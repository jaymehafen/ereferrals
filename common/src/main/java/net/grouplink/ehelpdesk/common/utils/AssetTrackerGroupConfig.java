package net.grouplink.ehelpdesk.common.utils;

public class AssetTrackerGroupConfig {

    public static final Integer ASSETTRACKER_INTERNAL = new Integer(1);
    public static final Integer ASSETTRACKER_ZEN = new Integer(2);

    private Boolean enableAssetTracker;
    private Integer assetTrackerType;
    private String zenBaseUrl;
    private String zenJdbcDriver;
    private String zenHibernateDialect;
    private String zenJdbcUrl;
    private String zenDbUsername;
    private String zenDbPassword;

    public Boolean getEnableAssetTracker() {
        return enableAssetTracker;
    }

    public void setEnableAssetTracker(Boolean enableAssetTracker) {
        this.enableAssetTracker = enableAssetTracker;
    }

    public Integer getAssetTrackerType() {
        return assetTrackerType;
    }

    public void setAssetTrackerType(Integer assetTrackerType) {
        this.assetTrackerType = assetTrackerType;
    }

    public String getZenBaseUrl() {
        return zenBaseUrl;
    }

    public void setZenBaseUrl(String zenBaseUrl) {
        this.zenBaseUrl = zenBaseUrl;
    }

    public String getZenJdbcDriver() {
        return zenJdbcDriver;
    }

    public void setZenJdbcDriver(String zenJdbcDriver) {
        this.zenJdbcDriver = zenJdbcDriver;
    }

    public String getZenJdbcUrl() {
        return zenJdbcUrl;
    }

    public void setZenJdbcUrl(String zenJdbcUrl) {
        this.zenJdbcUrl = zenJdbcUrl;
    }

    public String getZenDbUsername() {
        return zenDbUsername;
    }

    public void setZenDbUsername(String zenDbUsername) {
        this.zenDbUsername = zenDbUsername;
    }

    public String getZenDbPassword() {
        return zenDbPassword;
    }

    public void setZenDbPassword(String zenDbPassword) {
        this.zenDbPassword = zenDbPassword;
    }

    public String getZenHibernateDialect() {
        return zenHibernateDialect;
    }

    public void setZenHibernateDialect(String zenHibernateDialect) {
        this.zenHibernateDialect = zenHibernateDialect;
    }
}