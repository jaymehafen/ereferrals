package net.grouplink.ehelpdesk.common.utils;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {
    
    public static final String EMAIL_LIST_REGEX = "(\\s+)?;(\\s+)?|(\\s+)?,(\\s+)?|\\s+";

    public static String regexAsSqlMatch(String str) {
        if (str == null) return null;
		str = StringUtils.replace(str, "\\", "\\\\");
		str = StringUtils.replace(str, "[", "\\[");
		str = StringUtils.replace(str, "^", "\\^");
		str = StringUtils.replace(str, "$", "\\$");
		str = StringUtils.replace(str, ".", "\\.");
		str = StringUtils.replace(str, "|", "\\|");
		str = StringUtils.replace(str, "+", "\\+");
		str = StringUtils.replace(str, "(", "\\(");
		str = StringUtils.replace(str, ")", "\\)");
		str = StringUtils.replace(str, "%", ".*");
		str = StringUtils.replace(str, "*", ".*");
		str = StringUtils.replace(str, "?", ".?");
		str = StringUtils.replace(str, "_", ".?");
		str = ".*" + str + ".*";
		return str;
	}

    /**
     * Simply removes any html tags (or anything matching the pattern <*> from
     * the given String.
     * @param htmlContent the string to remove html tags
     * @return the string removed of any html tags
     */
    public static String stripHtmlTags(String htmlContent) {
        if (htmlContent == null) return null;
        Pattern p = Pattern.compile("<(.)+?>", Pattern.DOTALL);
        Matcher m = p.matcher(htmlContent);
        htmlContent = m.replaceAll(" ");
        htmlContent = htmlContent.replaceAll("\\p{Z}{2,}", " ");
        htmlContent = htmlContent.replaceAll("\\t+", " ");
        return htmlContent;
    }

    /**
     * Removes the <style type="text/css"> opening and closing tags, along with
     * the contents of it.
     * @param htmlContent text which may contain <style type="text/css"> tag
     * @return the text without the <style> tags and its contents
     */
    public static String stripCss(String htmlContent) {
        if (htmlContent == null) return null;
        String exp = "<style(\\s+type=\"text/css\")?\\s*>.*?</style>";
        Pattern p = Pattern.compile(exp, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(htmlContent);
        htmlContent = m.replaceAll("");
        return htmlContent;
    }

    /**
     * Replaces <br> or <br/> tags in the given String with CR LF characters.
     * @param htmlContent the html string
     * @return the string with <br>'s replaced with newlines
     */
    public static String replaceBRTagsWithNewLine(String htmlContent) {
        if (htmlContent == null) return null;
        htmlContent = htmlContent.replaceAll("<br(/)?>", "\r\n");
        return htmlContent;
    }

    /**
     * Replaces CR LF characters with a <br> tags.
     * @param text the text containing newlines
     * @return the text with newlines replaced with <br>'s
     */
    public static String replaceNewLinesWithBRTags(String text) {
        if (text == null) return null;
        text = text.replaceAll("(\\r)?(\\n)", "<br/>\r\n");
        return text;
    }

    /**
     * Replaces <p> tags with newlines.
     * @param text the text to replace
     * @return the text with <p> tags replaced by newlines
     */
    public static String replacePTagsWithNewLine(String text) {
        if (text == null) return null;
        text = text.replaceAll("<(/)?p(.|\\n)*?>", "\r\n");
        return text;
    }

    /**
     * Removes newlines from the given text.
     * @param text the text to replace
     * @return the text with new lines stripped
     */
    public static String stripNewLines(String text) {
        if (text == null) return null;
        text = text.replaceAll("(\\r)?(\\n)", "");
        return text;
    }
}