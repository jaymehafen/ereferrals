package net.grouplink.ehelpdesk.common.utils;


/**
 * @author mrollins
 * @version 1.0
 */
public class Constants {
    public static final int THISTTYPE_COMMENT = 0;
    public static final int THISTTYPE_APPOINTMENT = 1;
    public static final int THISTTYPE_TASK = 2;
    public static final int THISTTYPE_PHONERECD = 3;
    public static final int THISTTYPE_PHONEMADE = 4;
    
    public static final int STANDARD_MAX_GROUPS = 2;
    public static final int ENTERPRISE_MAX_GROUPS = 100;

    public static int ATTACHMENTS_FILE_SIZE = 1000000;

    public static final int ALL_CLEAR = 0;
    public static final int TECHS_EXCEEDED = 1;
    public static final int STANDARD_GROUPS_EXCEEDED = 2;
    public static final int ENTERPRISE_GROUPS_EXCEEDED = 3;
    public static final int WFPARTS_EXCEEDED = 4;


    // prevent instantiation
    private Constants(){
//        ArrayList<Integer> list = new ArrayList<Integer>();
    }
}