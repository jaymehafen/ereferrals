package net.grouplink.ehelpdesk.common.collaboration;

import java.util.ArrayList;
import java.util.List;

public class AddressBookContactsResult extends Result {

    private List<NameAndEmail> addressBookContacts = new ArrayList<NameAndEmail>();

	public List<NameAndEmail> getAddressBookContacts() {
		return addressBookContacts;
	}

	public void setAddressBookContacts(List<NameAndEmail> addressBookContacts) {
		this.addressBookContacts = addressBookContacts;
	}
	
	public void addContact(String displayName, String email) {
		addressBookContacts.add(new NameAndEmail(displayName, email));
	}
}
