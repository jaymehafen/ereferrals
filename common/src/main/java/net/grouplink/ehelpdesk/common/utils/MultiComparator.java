package net.grouplink.ehelpdesk.common.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Author: zpearce
 * Date: 4/13/12
 * Time: 12:10 PM
 */
public class MultiComparator implements Comparator<Object> {
    private List<Comparator> comparators = new ArrayList<Comparator>();

    public void addComparator(Comparator c) {
        comparators.add(c);
    }

    @SuppressWarnings("unchecked")
    public int compare(Object o, Object o1) {
        int result = 0;
        for(Comparator comparator : this.comparators) {
            result = comparator.compare(o, o1);
            if(result != 0) break;
        }
        return result;
    }
}
