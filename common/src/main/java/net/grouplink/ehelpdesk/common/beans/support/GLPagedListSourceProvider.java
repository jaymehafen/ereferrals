package net.grouplink.ehelpdesk.common.beans.support;

import java.util.List;
import java.util.Locale;

/**
 * Copy of Spring framework's PagedListSourceProvider in preparation for it's
 * deprecation and subsequent removal.
 * <p/>
 * Callback that provides the source for a reloadable List.
 * Used by {@link net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder}.
 *
 * @author Mark Rollins
 *         Date: Sep 17, 2008
 *         Time: 1:54:32 PM
 */
public interface GLPagedListSourceProvider {

    /**
     * Load the List for the given Locale and filter settings.
     * The filter object can be of any custom class, preferably a bean
     * for easy data binding from a request. An instance will simply
     * get passed through to this callback method.
     *
     * @param locale Locale that the List should be loaded for,
     *               or <code>null</code> if not locale-specific
     * @param filter object representing filter settings,
     *               or <code>null</code> if no filter options are used
     * @return the loaded List
     * @see org.springframework.beans.support.RefreshablePagedListHolder#setLocale
     * @see org.springframework.beans.support.RefreshablePagedListHolder#setFilter
     */
    List loadList(Locale locale, Object filter);

}
