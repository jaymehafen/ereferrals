package net.grouplink.ehelpdesk.common.collaboration;

import java.util.List;
import java.util.Vector;
import java.util.ArrayList;

public class FreeBusyInfo extends NameAndEmail {
	private List<FreeBusyBlock> freeBusyBlocks = new ArrayList<FreeBusyBlock>();

	public FreeBusyInfo() {
		
	}
	public void addFreeBusyBlock(FreeBusyBlock freeBusyBlock) {
		getFreeBusyBlocks().add(freeBusyBlock);
	}
	
	public List<FreeBusyBlock> getFreeBusyBlocks() {
		if (freeBusyBlocks == null) {
			freeBusyBlocks = new Vector<FreeBusyBlock>();
		}
		return freeBusyBlocks;
	}

	public void setFreeBusyBlocks(List<FreeBusyBlock> freeBusyBlocks) {
		this.freeBusyBlocks = freeBusyBlocks;
	}
}
