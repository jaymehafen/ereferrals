package net.grouplink.ehelpdesk.common.utils;

/**
 * Created by IntelliJ IDEA.
 * User: nrollins
 * Date: Aug 29, 2007
 * Time: 10:23:26 AM
 */
public class UserSearch {

    private String firstName;
    private String lastName;
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
