package net.grouplink.ehelpdesk.common.collaboration;

import java.util.List;
import java.util.Vector;
import java.util.ArrayList;

public class FreeBusyResult extends Result {
	private List<FreeBusyInfo> freeBusyInfo = new ArrayList<FreeBusyInfo>();

	public List<FreeBusyInfo> getFreeBusyInfo() {
		if (freeBusyInfo == null) {
			freeBusyInfo = new Vector<FreeBusyInfo>();
		}
		return freeBusyInfo;
	}

	public void setFreeBusyInfo(List<FreeBusyInfo> freeBusyInfo) {
		this.freeBusyInfo = freeBusyInfo;
	}
	
}
