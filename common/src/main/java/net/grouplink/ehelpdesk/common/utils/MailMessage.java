package net.grouplink.ehelpdesk.common.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

public class MailMessage {

    private MimeMessage mimeMessage;
    private MimeMessageHelper helper;

    protected final Log logger = LogFactory.getLog(getClass());
    private boolean isToSet = false;
    private String emailSplitRegex = ";\\s*|,\\s*|\\s+";

    public MailMessage(JavaMailSenderImpl mailSender) throws MessagingException {
        mimeMessage = mailSender.createMimeMessage();
        helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
    }

    public void addCc(String cc) throws MessagingException {
	    if(StringUtils.isNotBlank(cc)){
            helper.addCc(cc);
            isToSet = true;
        }
    }

    public void addBcc(String bcc) throws MessagingException {
        if(StringUtils.isNotBlank(bcc)){
            	helper.addBcc(bcc);
            isToSet = true;
        }
    }

    public void addTo(String to) throws MessagingException {
        if(StringUtils.isNotBlank(to)){
            helper.addTo(to);
            isToSet = true;
        }
    }

    public void setCc(String cc) throws MessagingException {
        if(StringUtils.isNotBlank(cc)){
            helper.setCc(cc.split(emailSplitRegex));
            isToSet = true;
        }
    }

    public void setCc(String[] cc) throws MessagingException {
        if (cc.length > 0) {
            helper.setCc(cc);
            isToSet = true;
        }
    }

    public void setBcc(String bcc) throws MessagingException {
        if(StringUtils.isNotBlank(bcc)){
            helper.setBcc(bcc.split(emailSplitRegex));
            isToSet = true;
        }
    }

    public void setBcc(String[] bcc) throws MessagingException {
        if (bcc.length > 0) {
            helper.setBcc(bcc);
            isToSet = true;
        }
    }

    public void setTo(String to) throws MessagingException {
        if(StringUtils.isNotBlank(to)){
            helper.setTo(to.split(emailSplitRegex));
            isToSet = true;
        }
    }

    public void setTo(String[] to) throws MessagingException {
        if (to.length > 0) {
            helper.setTo(to);
            isToSet = true;
        }
    }

    public void setFrom(String from) throws MessagingException {
        if(StringUtils.isNotBlank(from)){
            helper.setFrom(from);
        }
    }

    public void setFrom(String from, String personal) throws UnsupportedEncodingException,
	    MessagingException {
        if(StringUtils.isNotBlank(from)){
            helper.setFrom(from, personal);
        }
    }

    public void setSubject(String subject) throws MessagingException {
	    helper.setSubject(subject);
    }

    public void setText(String text) throws MessagingException {
	    helper.setText(text);
    }

    public void setText(String text, boolean html) throws MessagingException {
	    helper.setText(text, html);
    }

    public void setText(String plainText, String htmlText) throws MessagingException {
	    helper.setText(plainText, htmlText);
    }

    public MimeMessage getMimeMessage() {
	    return mimeMessage;
    }

    public boolean isMessageReady() {
        return isToSet;
    }
}
