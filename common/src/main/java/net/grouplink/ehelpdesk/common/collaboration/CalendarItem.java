/**
 * @author derickson
 * Date: 4/23/2007
 */
package net.grouplink.ehelpdesk.common.collaboration;

import net.grouplink.ehelpdesk.domain.Attachment;

import java.util.List;
import java.util.Vector;

/**
 * CalendarItem 
 * @author derickson
 *
 */
public class CalendarItem {
	private NameAndEmail from = null;
	private String subject = null;
	private String message = null;
	private String contentType = "text/plain";
	private List<Recipient> recipients = null;
	private List<Attachment> attachments = null;

	public void addRecipient(Recipient recipient) {
		getRecipients().add(recipient);
	}
	
	public void addRecipient(String displayName) {
		addRecipient(new Recipient(displayName));
	}
	
	public void addRecipient(String displayName, String emailAddress) {
		addRecipient(new Recipient(displayName, emailAddress));
	}
	
	public void addRecipient(String displayName, String emailAddress, String targetType) {
		addRecipient(new Recipient(displayName, emailAddress, targetType));
	}
	
	public void addRecipient(String displayName, String emailAddress, TargetType targetType) {
		addRecipient(new Recipient(displayName, emailAddress, targetType));
	}
	
	public void addAttachment(Attachment attachment) {
		getAttachments().add(attachment);
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public List<Recipient> getRecipients() {
		if (recipients == null) {
			recipients = new Vector<Recipient>();
		}
		return recipients;
	}
	
	public void setRecipients(List<Recipient> recipients) {
		this.recipients = recipients;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<Attachment> getAttachments() {
		if (attachments == null) {
			attachments = new Vector<Attachment>();
		}
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public NameAndEmail getFrom() {
		return from;
	}

	public void setFrom(NameAndEmail from) {
		this.from = from;
	}
}
