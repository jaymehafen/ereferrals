package net.grouplink.ehelpdesk.common.utils;

import org.apache.commons.lang.time.DateFormatUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtils {
	
	public static final DateFormat ISO_DATE_FORMAT 
		= new SimpleDateFormat(DateFormatUtils.ISO_DATE_FORMAT.getPattern());
	public static final DateFormat ISO_DATETIME_TIME_ZONE_FORMAT 
		= new SimpleDateFormat(DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.getPattern());
    public static final DateFormat ISO_TIME_NO_T_TIME_ZONE_FORMAT
            = new SimpleDateFormat(DateFormatUtils.ISO_TIME_NO_T_TIME_ZONE_FORMAT.getPattern());
    public static final DateFormat STANDARD_DATE_TIME
        = new SimpleDateFormat("MM/dd/yyyy HH:mm");
    public static final DateFormat STANDARD_DATE
        = new SimpleDateFormat("MM/dd/yyyy");
    public static final DateFormat DB_STRING_DATE
        = new SimpleDateFormat("yyyy-MM-dd");

    static {
		ISO_DATE_FORMAT.setLenient(true);
		ISO_DATETIME_TIME_ZONE_FORMAT.setLenient(true);
        ISO_TIME_NO_T_TIME_ZONE_FORMAT.setLenient(true);
        STANDARD_DATE_TIME.setLenient(true);
        STANDARD_DATE.setLenient(true);
    }

    public static Date firstDayOfWeek() {
        Calendar now_date = Calendar.getInstance();
        now_date.add(Calendar.DAY_OF_MONTH, -(now_date.get(Calendar.DAY_OF_WEEK) - 1));
        return now_date.getTime();
    }

    /**
     * @return A (String) date for the last day of the current week - formatted by the input mask.
     */
    public static Date lastDayOfWeek() {
        Calendar now_date = Calendar.getInstance();
        now_date.add(Calendar.DAY_OF_MONTH, -(now_date.get(Calendar.DAY_OF_WEEK) - 1));
        now_date.add(Calendar.DAY_OF_MONTH, 6);
        return now_date.getTime();
    }

    /**
     * @return A (String) date for the first day of the current month - formatted by the input mask.
     */

    public static Date firstDayOfMonth() {
        Calendar now_date = Calendar.getInstance();
        now_date.add(Calendar.DAY_OF_MONTH,-(now_date.get(Calendar.DATE) - 1));
        return now_date.getTime();
    }

    /**
     * @return A (String) date for the last day of the current month - formatted by the input mask.
     */

    public static Date lastDayOfMonth() {
        Calendar now_date = Calendar.getInstance();
        now_date.set(Calendar.DAY_OF_MONTH, getmaxday(now_date.get(Calendar.MONTH), now_date.get(Calendar.YEAR)));
        return now_date.getTime();
    }
    
    /**
     * Is this a leap year?
     * @param year Year to check.
     * @return true if that year is a leap year.
     */
    public static boolean leapyear(int year) {
	    return (year % 400 == 0) || (year % 4 == 0) && (year % 100 != 0);
    }


    /**
     * Get last day of the month.
     * @param month Month of year.
     * @param year The year.
     * @return Last day of that month.
     */
    public static int getmaxday(int month, int year) {
	    int monthlen[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	    int maxday = monthlen[month];
	    if (month == 2 && leapyear(year))
	    ++maxday;
	    return maxday;
    }

    public static Date getDateFromISOFormat(String dateString) throws ParseException {
        return ISO_DATE_FORMAT.parse(dateString);
    }

    public static Date getDateFromStandardDateTimeFormat(String dateString) throws ParseException {
        return STANDARD_DATE_TIME.parse(dateString);
    }

    public static Date getDateFromStandardDateFormat(String dateString) throws ParseException {
        return STANDARD_DATE.parse(dateString);
    }
}
