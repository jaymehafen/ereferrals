/**
 *	Class:		TargetType
 *	Package:	net.grouplink.collaboration
 *	Project:	ContactWise Web
 *	Date:		Jan 4, 2007
 *	Purpose:	Define an email target nType (to, cc, or bc)
 *	Description:
 *
 *	Notes:		Internal integer values correspond with the GroupWise constant
 *				values.  When dealing with other types of collaboration clients,
 *				do not depend on the constant values to match.
 *
 *	@author		derickson
 *	@version	1.0
 */
package net.grouplink.ehelpdesk.common.collaboration;

import java.io.Serializable;

public class TargetType implements Serializable {
	
	public static final int nTO = 0;
	public static final int nCC = 1;
	public static final int nBC = 2;
	public static final int nUNKNOWN = -1;
	
	public static final String sTO = "TO";
	public static final String sCC = "CC";
	public static final String sBC = "BC";
	public static final String sUNKNOWN = "UNKNOWN";
	
	public static final TargetType TO = new TargetType(nTO, sTO);
	public static final TargetType CC = new TargetType(nCC, sCC);
	public static final TargetType BC = new TargetType(nBC, sBC);
	public static final TargetType UNKNOWN = new TargetType(nUNKNOWN, sUNKNOWN);
	
	private final int nType;
	private final String sType;
	
	private TargetType(int type, String sType) {
		this.nType = type;
		this.sType = sType;
	}
	
	public static TargetType valueOf(int type) {
		TargetType value;
		switch (type)
		{
		case nTO:
			value = TO;
			break;
		case nCC:
			value = CC;
			break;
		case nBC:
			value = BC;
			break;
		default:
			value = UNKNOWN;
			break; 
		}
		return value;
	}
	
	public static TargetType valueOf(String type) {
		TargetType value;
		if (sTO.equalsIgnoreCase(type)) {
			value = TO;
		} else if (sCC.equalsIgnoreCase(type)) {
			value = CC;
		} else if (sBC.equalsIgnoreCase(type)) {
			value = BC;
		} else {
			value = UNKNOWN;
		}
		return value;
	}
	
	public int getType() {
		return nType;
	}
	
	public String toString() {
		return sType;
	}
}