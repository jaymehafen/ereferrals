/**
 * Abstract class that defines a collaboration Session object used to track a 
 * users session.  Extending classes are intended to contain implementation
 * specific information for tracking the session.
 * 
 * @author derickson
 */
package net.grouplink.ehelpdesk.common.collaboration;

import java.io.Serializable;

public abstract class Session implements Serializable {

	public String userName = null;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
