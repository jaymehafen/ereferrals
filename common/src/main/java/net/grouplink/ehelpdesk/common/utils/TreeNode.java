package net.grouplink.ehelpdesk.common.utils;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TreeNode implements Comparable {

    private int index;
    private int parent;
    private String name;
    private String text;
    private String icon;
    private String iconOpen;
    private boolean isFolder;

    public boolean isFolder() {
        return isFolder;
    }

    public void setFolder(boolean folder) {
        isFolder = folder;
    }

    public TreeNode() {
    }

    public TreeNode(int index, int parent, String name, String text) {
        this.index = index;
        this.parent = parent;
        this.name = name;
        this.text = text;
    }

    public TreeNode(int index, int parent, String name, String text, String icon, String iconOpen) {
        this.index = index;
        this.parent = parent;
        this.name = name;
        this.text = text;
        this.icon = icon;
        this.iconOpen = iconOpen;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconOpen() {
        return iconOpen;
    }

    public void setIconOpen(String iconOpen) {
        this.iconOpen = iconOpen;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getText() {
        return text;
    }

    public void setText(String dn) {
        this.text = dn;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.DEFAULT_STYLE);
    }

    public int compareTo(Object o) {
        if(index > ((TreeNode) o).getIndex()){
            return 1;
        }
        if(index < ((TreeNode) o).getIndex()){
            return -1;
        }
        else{
            return 0;
        }
    }
}
