package net.grouplink.ehelpdesk.common.utils;

import net.grouplink.ehelpdesk.domain.DatabaseConfig;

import javax.validation.constraints.Pattern;

public class AssetTrackerZen10Config {
    private DatabaseConfig zenDatabaseConfig = new DatabaseConfig();
    private String zenBaseUrl;
    private Boolean enableZen10;
    private Boolean enableEhdVnc;

    public String getZenBaseUrl() {
        return zenBaseUrl;
    }

    public void setZenBaseUrl(String zenBaseUrl) {
        this.zenBaseUrl = zenBaseUrl;
    }

    public DatabaseConfig getZenDatabaseConfig() {
        return zenDatabaseConfig;
    }

    public void setZenDatabaseConfig(DatabaseConfig zenDatabaseConfig) {
        this.zenDatabaseConfig = zenDatabaseConfig;
    }

    public Boolean getEnableZen10() {
        return enableZen10;
    }

    public void setEnableZen10(Boolean enableZen10) {
        this.enableZen10 = enableZen10;
    }

    public Boolean getEnableEhdVnc() {
        return enableEhdVnc;
    }

    public void setEnableEhdVnc(Boolean enableEhdVnc) {
        this.enableEhdVnc = enableEhdVnc;
    }
}
