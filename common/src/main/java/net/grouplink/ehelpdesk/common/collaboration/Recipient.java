/**
 *	Class:		Recipient
 *	Package:	net.grouplink.collaboration
 *	Project:	GroupWise Integration
 *	Date:		Oct 31, 2006
 *	Purpose:	
 *	Description:
 *				
 *
 *	@author		derickson
 *	@version	1.0
 */
package net.grouplink.ehelpdesk.common.collaboration;

import java.io.Serializable;

/**
 * @author derickson
 *
 */
public class Recipient extends NameAndEmail implements Serializable {
	
	private TargetType targetType = TargetType.TO;

	public Recipient(String displayName, String emailAddress, TargetType targetType) {
		super(displayName, emailAddress);
		setTargetType(targetType);
	}
	
	public Recipient(String displayName, String emailAddress) {
		super(displayName, emailAddress);
	}
	
	public Recipient(String displayName, String emailAddress, String targetType) {
		this(displayName, emailAddress, TargetType.valueOf(targetType));
	}
	
	public Recipient(String displayName) {
		super(displayName);
	}
	
	public TargetType getTargetType() {
		return targetType;
	}

	public void setTargetType(TargetType targetType) {
		this.targetType = targetType;
	}
}
