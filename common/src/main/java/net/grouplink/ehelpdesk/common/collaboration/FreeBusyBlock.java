package net.grouplink.ehelpdesk.common.collaboration;

import java.util.Calendar;

public class FreeBusyBlock {
	private String acceptLevel = null;
	private Calendar startDate = null;
	private Calendar endDate = null;
	private String subject = null;

	public FreeBusyBlock() {
		
	}
	
	public FreeBusyBlock(String acceptLevel, Calendar startDate, Calendar endDate, String subject) {
		setAcceptLevel(acceptLevel);
		setStartDate(startDate);
		setEndDate(endDate);
		setSubject(subject);
	}
	
	public String getAcceptLevel() {
		return acceptLevel;
	}
	public void setAcceptLevel(String acceptLevel) {
		this.acceptLevel = acceptLevel;
	}
	public Calendar getEndDate() {
		return endDate;
	}
	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}
	public Calendar getStartDate() {
		return startDate;
	}
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
}
