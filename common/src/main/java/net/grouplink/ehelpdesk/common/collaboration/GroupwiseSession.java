/**
 * @author derickson
 * Date: 4/23/2007
 */
package net.grouplink.ehelpdesk.common.collaboration;

import java.io.Serializable;

/**
 * Groupwise implementation of the collaboration Session class.
 *
 *	Contains the session string returned from the Groupwise Post Office Agent
 *	after logging in.
 */
public class GroupwiseSession extends Session implements Serializable {
	private String session = null;

    public GroupwiseSession(String userName, String session) {
		setUserName(userName);
		setSession(session);
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
}
