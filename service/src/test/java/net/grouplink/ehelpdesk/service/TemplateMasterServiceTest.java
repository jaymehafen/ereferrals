package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.*;

import java.util.List;

public class TemplateMasterServiceTest extends AbstractServiceIntegrationTest {
    private TemplateMasterService templateMasterService;
    private UserService userService;
    private TicketService ticketService;
    private UserRoleGroupService userRoleGroupService;
    private GroupService groupService;
    private LocationService locationService;

    public void testLaunchTicketTemplateMaster() {
        TicketTemplateMaster ttm = new TicketTemplateMaster();
        ttm.setName("test");
        TicketTemplate tt = new TicketTemplate();
        tt.setSubject("test tt 1 subject");
        tt.setNote("test tt 1 note");
        tt.setTemplateName("test tt 1");

        Location location = locationService.getLocationByName("High School 1");
        Group group = groupService.getGroupByName("Facilities");
        List urgs = userRoleGroupService.getUserRoleGroup();

        tt.setGroup(group);
        tt.setLocation(location);
        tt.setAssignedTo((UserRoleGroup) urgs.get(0));

        Attachment a = new Attachment();
        a.setFileName("test");
        tt.addAttachment(a);

        ttm.addTemplate(tt);

        templateMasterService.save(ttm);
        User user = userService.getUserById(User.ADMIN_ID);
        TicketTemplateLaunch launch = templateMasterService.launchTicketTemplateMaster(ttm, user);
        ticketService.saveTickets(launch.getTickets(), user);
        assertTrue(launch.getTickets().size() == 1);
    }

    public void testLaunchWorkflow() {
        TicketTemplateMaster ttm = new TicketTemplateMaster();
        ttm.setName("test");
        TicketTemplate tt = new TicketTemplate();
        tt.setSubject("test tt 1 subject");
        tt.setNote("test tt 1 note");
        tt.setTemplateName("test tt 1");

        Location location = locationService.getLocationByName("High School 1");
        Group group = groupService.getGroupByName("Facilities");
        List urgs = userRoleGroupService.getUserRoleGroup();

        tt.setGroup(group);
        tt.setLocation(location);
        tt.setAssignedTo((UserRoleGroup) urgs.get(0));

        Attachment a = new Attachment();
        a.setFileName("test");
        tt.addAttachment(a);

        ttm.addTemplate(tt);

        WorkflowStep wfs = new WorkflowStep();
        wfs.setStepNumber(1);
        wfs.setTicketTemplateMaster(ttm);
        ttm.addWorkflowStep(wfs);
        wfs.addTicketTemplate(tt);

        templateMasterService.save(ttm);
        User user = userService.getUserById(User.ADMIN_ID);
        TicketTemplateLaunch launch = templateMasterService.launchTicketTemplateMaster(ttm, user);
        ticketService.saveTickets(launch.getTickets(), user);
        assertTrue(launch.getTickets().size() == 1);
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
}
