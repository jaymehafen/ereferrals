package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;

import java.util.Date;
import java.util.Calendar;

public class RecurrenceScheduleServiceTest extends AbstractServiceIntegrationTest {
    private RecurrenceScheduleService recurrenceScheduleService;

    public void testMeetsScheduleCriteriaDaily() {
        RecurrenceSchedule r = new RecurrenceSchedule();
        r.setPattern(RecurrenceSchedule.PATTERN_DAILY);
        r.setDailyInterval(1);
        boolean meets = recurrenceScheduleService.meetsScheduleCriteria(r, new Date(), null, 0);
        assertTrue(meets);
    }

    public void testMeetsScheduleCriteriaWeekly() {
        RecurrenceSchedule r = new RecurrenceSchedule();
        r.setPattern(RecurrenceSchedule.PATTERN_WEEKLY);
        r.setWeeklyInterval(1);
        r.setOnSunday(true);
        r.setOnMonday(true);
        r.setOnTuesday(true);
        r.setOnWednesday(true);
        r.setOnThursday(true);
        r.setOnFriday(true);
        r.setOnSaturday(true);
        
        boolean meets = recurrenceScheduleService.meetsScheduleCriteria(r, new Date(), null, 0);
        assertTrue(meets);
    }

    public void testMeetsScheduleCriteriaMonthlyDayOfMonth() {
        RecurrenceSchedule r = new RecurrenceSchedule();
        r.setPattern(RecurrenceSchedule.PATTERN_MONTHLY);
        r.setMonthlySchedule(RecurrenceSchedule.MONTHLY_DAY_OF_MONTH);
        Calendar cal = Calendar.getInstance();
        r.setMonthlyDayOfMonth(cal.get(Calendar.DAY_OF_MONTH));
        r.setMonthlyDayOfMonthOccurrence(1);
        
        boolean meets = recurrenceScheduleService.meetsScheduleCriteria(r, new Date(), null, 0);
        assertTrue(meets);
    }

    public void testMeetsScheduleCriteriaMonthlyDayOfWeek() {
        RecurrenceSchedule r = new RecurrenceSchedule();
        r.setPattern(RecurrenceSchedule.PATTERN_MONTHLY);
        r.setMonthlySchedule(RecurrenceSchedule.MONTHLY_DAY_OF_WEEK);
        Calendar cal = Calendar.getInstance();

        int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        double week = dayOfMonth / 7D;
        int interval = (int)Math.ceil(week);
        
        r.setMonthlyDayOfWeekInterval(interval);
        r.setMonthlyDayOfWeekDay(cal.get(Calendar.DAY_OF_WEEK));
        r.setMonthlyDayOfWeekOccurrence(1);

        boolean meets = recurrenceScheduleService.meetsScheduleCriteria(r, new Date(), null, 0);
        assertTrue(meets);
    }

    public void testMeetsScheduleCriteriaYearlyDayOfYear() {
        RecurrenceSchedule r = new RecurrenceSchedule();
        r.setPattern(RecurrenceSchedule.PATTERN_YEARLY);
        Calendar cal = Calendar.getInstance();
        r.setYearlySchedule(RecurrenceSchedule.YEARLY_DAY_OF_YEAR);
        r.setYearlyDayOfYearDay(cal.get(Calendar.DAY_OF_MONTH));
        r.setYearlyDayOfYearMonth(cal.get(Calendar.MONTH) + 1);

        boolean meets = recurrenceScheduleService.meetsScheduleCriteria(r, new Date(), null, 0);
        assertTrue(meets);
    }

    public void testMeetsScheduleCriteriaYearlyDayOfMonth() {
        RecurrenceSchedule r = new RecurrenceSchedule();
        r.setPattern(RecurrenceSchedule.PATTERN_YEARLY);
        Calendar cal = Calendar.getInstance();
        r.setYearlySchedule(RecurrenceSchedule.YEARLY_DAY_OF_MONTH);

        r.setYearlyDayOfMonthInterval(cal.get(Calendar.WEEK_OF_MONTH));
        r.setYearlyDayOfMonthDay(cal.get(Calendar.DAY_OF_WEEK));
        r.setYearlyDayOfMonthMonth(cal.get(Calendar.MONTH) + 1);

        boolean meets = recurrenceScheduleService.meetsScheduleCriteria(r, new Date(), null, 0);
        assertTrue(meets);
    }


    public void setRecurrenceScheduleService(RecurrenceScheduleService recurrenceScheduleService) {
        this.recurrenceScheduleService = recurrenceScheduleService;
    }
}
