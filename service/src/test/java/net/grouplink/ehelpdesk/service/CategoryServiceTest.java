package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractTransactionalSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.Group;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 8, 2008
 * Time: 2:58:03 PM
 */
public class CategoryServiceTest extends AbstractTransactionalSpringContextTests {

    private CategoryService categoryService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }


    public void testGetCategories() {

        List categories = categoryService.getCategories();
        assertNotNull(categories);
        if (categories.size() > 0) {
            Category category = (Category) categories.get(0);
            assertNotNull(category);
            Group group = category.getGroup();
            assertNotNull(group);
            Integer groupid = group.getId();
            assertNotNull(groupid);
            assertNotNull(categoryService.getCategoryByGroupId(groupid));
            Integer catid = category.getId();
            assertNotNull(catid);
            assertNotNull(categoryService.getCategoryById(catid));
            String catname = category.getName();
            assertNotNull(catname);
            assertNotNull(categoryService.getCategoryByName(catname));
            assertNotNull(categoryService.getCategoryByNameAndGroupId(catname, groupid));
            assertNotNull(categoryService.getCategoryTreeByGroupId(groupid));
        }
    }



    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
