package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;

/**
 * User: jaymehafen
 * Date: Jul 18, 2008
 * Time: 1:00:00 PM
 */
public class AssetCustomFieldServiceTest extends AbstractServiceIntegrationTest {
    private AssetService assetService;
    private AssetFieldGroupService assetFieldGroupService;
    private AssetCustomFieldService assetCustomFieldService;

    public void testAddAssetCustomField() {
        AssetCustomField acf = new AssetCustomField();

        Asset asset = assetService.getAssetById(new Integer(1));
        acf.setName("test asset custom field 2");
        acf.setDescription("description of test asset custom field 2");
        acf.setRequired(Boolean.FALSE);

        AssetFieldGroup afg = assetFieldGroupService.getById(new Integer(1));
        acf.setFieldGroup(afg);
        acf.setFieldOrder(new Integer(10));

        acf.setCustomFieldType(AssetCustomField.VALUETYPE_TEXT);

        assetCustomFieldService.saveAssetCustomField(acf);
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setAssetFieldGroupService(AssetFieldGroupService assetFieldGroupService) {
        this.assetFieldGroupService = assetFieldGroupService;
    }

    public void setAssetCustomFieldService(AssetCustomFieldService assetCustomFieldService) {
        this.assetCustomFieldService = assetCustomFieldService;
    }
}
