package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Status;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 8, 2008
 * Time: 3:50:55 PM
 */
public class StatusServiceTest extends AbstractServiceIntegrationTest {

    private StatusService statusService;

    public void testGetStatus() {
        List statusList = statusService.getStatus();
        assertNotNull(statusList);
        if (statusList.size() > 0) {
            Status status = (Status) statusList.get(0);
            assertNotNull(status);
            Integer statusid = status.getId();
            assertNotNull(statusid);
            assertNotNull(statusService.getStatusById(statusid));
            String statusname = status.getName();
            assertNotNull(statusname);
            assertNotNull(statusService.getStatusByName(statusname));
        }

        Status testStatus = new Status();
        testStatus.setName("test");
        statusService.saveStatus(testStatus);

        statusList = statusService.getStatus();
        assertTrue(statusList.contains(testStatus));

        statusService.deleteStatus(testStatus);
        statusList = statusService.getStatus();
        assertFalse(statusList.contains(testStatus));
    }

    public void testDeleteStatus() {
        Status newStatus = new Status();
        statusService.saveStatus(newStatus);
        assertNotNull(newStatus.getId());
        statusService.deleteStatus(newStatus);
        Status status2 = statusService.getStatusById(newStatus.getId());
        assertNotNull(status2);
        assertFalse(status2.getActive().booleanValue());
    }

    public void testGetStatusByName() {
        Status closedStatus = statusService.getStatusByName("Closed");
        assertNotNull(closedStatus);

        Status testStatus = new Status();
        testStatus.setName("test");
        statusService.saveStatus(testStatus);
        statusService.deleteStatus(testStatus);
        testStatus = statusService.getStatusByName("test");
        assertNull(testStatus);
    }

    public void testGetClosedStatus() {
        assertNotNull(statusService.getClosedStatus());
    }

    public void testGetCustomStatus() {
        assertNotNull(statusService.getCustomStatus());
    }

    public void testGetInitialStatus() {
        assertNotNull(statusService.getInitialStatus());
    }

    public void testGetTicketPoolStatus() {
        assertNotNull(statusService.getTicketPoolStatus());
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }
}
