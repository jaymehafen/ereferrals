package net.grouplink.ehelpdesk.service;

import com.google.gson.Gson;
import junit.framework.TestCase;
import net.grouplink.ehelpdesk.domain.acl.Permission;
import net.grouplink.ehelpdesk.service.acl.dataobj.PermSchemesDataObj;
import net.grouplink.ehelpdesk.service.acl.dataobj.PermissionDataObj;
import net.grouplink.ehelpdesk.service.acl.PermissionInitializerServiceImpl;
import net.grouplink.ehelpdesk.service.acl.PermissionInitializerService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PermissionJsonTest extends TestCase {
    public void testParseJson() {
        BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader()
                .getResourceAsStream("net/grouplink/ehelpdesk/service/acl/permissions.json")));

        Gson gson = new Gson();
        DataObj obj = gson.fromJson(br, DataObj.class);

        System.out.println(obj);

        PermissionInitializerService pi = new PermissionInitializerServiceImpl();
        PermissionDataObj permDataObj = pi.parsePermissions();
        PermSchemesDataObj permSchemesDataObj = pi.parsePermissionSchemes();


        System.out.println(obj);

    }


}

class DataObj {
    private List<Permission> globalPermissions = new ArrayList<Permission>();
    private List<Permission> groupPermissions = new ArrayList<Permission>();
    private List<Permission> fieldPermissions = new ArrayList<Permission>();
}
