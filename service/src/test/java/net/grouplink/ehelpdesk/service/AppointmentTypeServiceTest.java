package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.AppointmentType;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 8, 2008
 * Time: 2:14:28 PM
 */
public class AppointmentTypeServiceTest extends AbstractDependencyInjectionSpringContextTests {

    private AppointmentTypeService appointmentTypeService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }

    public void testGetApptTypes() {
        List types = appointmentTypeService.getApptTypes();
        assertNotNull(types);

        if (types.size() != 0) {
            AppointmentType type = (AppointmentType) types.get(0);
            assertNotNull(type);
            assertNotNull(type.getId());
            assertNotNull(type.getName());
            AppointmentType betty = appointmentTypeService.getApptTypeById(type.getId());
            assertNotNull(betty);
            AppointmentType wilma = appointmentTypeService.getApptTypeByName(type.getName());
            assertNotNull(wilma);
        }

    }

    public void setAppointmentTypeService(AppointmentTypeService appointmentTypeService) {
        this.appointmentTypeService = appointmentTypeService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
