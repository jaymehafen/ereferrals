package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.User;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class UserServiceTest extends AbstractServiceIntegrationTest {

    private UserService userService;

    public void testGetUsers() {
        List users = userService.getUsers();
        assertNotNull(users);

        if (users.size() != 0) {
            User user = (User) users.get(0);
            assertNotNull(user);
            assertNotNull(user.getFirstName());
            assertNotNull(user.getLastName());
            User barney = userService.getUserById(user.getId());
            assertNotNull(barney);
        }

        /*Set userRoles = barney.getUserRoles();
        for (Iterator iterator = userRoles.iterator(); iterator.hasNext();) {
            UserRole userRole = (UserRole) iterator.next();
            Set urls = userRole.getUserRoleLocation();
            for (Iterator iterator1 = urls.iterator(); iterator1.hasNext();) {
                UserRoleGroup userRoleGroups = (UserRoleGroup) iterator1.next();
                Location loc = userRoleGroups.getLocation();
                assertNotNull(loc);
            }
        }*/
    }

    public void testGetUserByEmail() {
        String email = "ckent@grouplink.net";
        User u = userService.getUserByEmail(email);
        assertNotNull(u);
        assertEquals(u.getLoginId(), "user");

        u = userService.getUserByEmail(email.toUpperCase());
        assertNotNull(u);
        assertEquals(u.getLoginId(), "user");
    }


    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
