package net.grouplink.ehelpdesk.service;

import junit.framework.TestCase;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class JasperReportsTest extends TestCase {

    /**
     * Uncomment to compile .jrxml files to .jasper files
     */
    /*
    public void testCompileReports() throws JRException {
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/resources/net/grouplink/ehelpdesk/service/reports/MainReport.jrxml");
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/resources/net/grouplink/ehelpdesk/service/reports/ReportByDate.jrxml");
    }
    */

    public void testDummy() {
        // do nothing
    }

    private String getBaseDir() {
        Properties p = new Properties();
        InputStream is = null;
        try {
            is = getClass().getClassLoader().getResourceAsStream("jaspertest.properties");
            if (is != null)
                p.load(is);

            return (StringUtils.isNotBlank(p.getProperty("basedir")) ? p.getProperty("basedir") + File.separator : "");
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
