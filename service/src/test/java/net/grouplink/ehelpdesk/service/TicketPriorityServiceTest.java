package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.TicketPriority;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 9, 2008
 * Time: 10:50:16 AM
 */
public class TicketPriorityServiceTest extends AbstractDependencyInjectionSpringContextTests {

    private TicketPriorityService ticketPriorityService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }

    public void test() {
        List priorities = ticketPriorityService.getPriorities();
        assertNotNull(priorities);
        if (priorities.size() > 0) {
            TicketPriority priority = (TicketPriority) priorities.get(0);
            assertNotNull(priority);
            Integer pid = priority.getId();
            assertNotNull(pid);
            assertNotNull(ticketPriorityService.getPriorityById(pid));
            String pname = priority.getName();
            assertNotNull(pname);
            assertNotNull(ticketPriorityService.getPriorityByName(pname));
        }
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
