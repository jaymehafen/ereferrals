package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.service.AbstractServiceIntegrationTest;
import net.grouplink.ehelpdesk.domain.asset.AssetType;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 4:49:44 PM
 */
public class AssetTypeServiceTest extends AbstractServiceIntegrationTest {
    private AssetTypeService assetTypeService;

    public void testGetAssetType() {
        AssetType at = assetTypeService.getById(new Integer(1));
        assertNotNull(at);
        assertEquals("test asset type 1", at.getName());
        assertEquals("description of test asset type 1", at.getNotes());
    }

    public void testAddAssetType() {
        AssetType at = new AssetType();
        at.setName("test asset type 2");
        at.setNotes("description of test asset type 2");
        assetTypeService.saveAssetType(at);
        assertNotNull(at.getId());
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }
}
