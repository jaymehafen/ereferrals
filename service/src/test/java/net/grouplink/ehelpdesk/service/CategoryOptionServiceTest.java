package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractTransactionalSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.Group;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 8, 2008
 * Time: 2:39:38 PM
 */
public class CategoryOptionServiceTest extends AbstractTransactionalSpringContextTests {

    CategoryOptionService categoryOptionService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }

    public void testGetCategoryOptions() {
        List opts = categoryOptionService.getCategoryOptions();
        assertNotNull(opts);
        if (opts.size() > 0) {
            CategoryOption opt = (CategoryOption) opts.get(0);
            Category cat = opt.getCategory();
            assertNotNull(cat);
            Integer catid = cat.getId();
            assertNotNull(catid);
            assertNotNull(categoryOptionService.getCategoryOptionByCategoryId(catid));
            Group grp = cat.getGroup();
            assertNotNull(grp);
            Integer grpid = grp.getId();
            assertNotNull(grpid);
            assertNotNull(categoryOptionService.getCategoryOptionByGroupId(grpid));
            assertNotNull(opt.getId());
            assertNotNull(categoryOptionService.getCategoryOptionById(opt.getId()));
            String catname = opt.getName();
            assertNotNull(catname);
            assertNotNull(categoryOptionService.getCategoryOptionByName(catname));
            assertNotNull(categoryOptionService.getCategoryOptionByNameAndCategoryId(catname, cat));
            assertNotNull(categoryOptionService.getCategoryOptionByNameAndGroupId(catname, grpid));
            assertNotNull(categoryOptionService.getDuplicateCategoryOption(opt));
        }

    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
