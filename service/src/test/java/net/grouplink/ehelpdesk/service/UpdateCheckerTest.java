package net.grouplink.ehelpdesk.service;

import junit.framework.TestCase;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

public class UpdateCheckerTest extends TestCase {
    public void testArtifactVersions() {
        DefaultArtifactVersion v934 = new DefaultArtifactVersion("9.3.4");
        DefaultArtifactVersion v934SS = new DefaultArtifactVersion("9.3.4-SNAPSHOT");
        DefaultArtifactVersion v933 = new DefaultArtifactVersion("9.3.3");
        DefaultArtifactVersion v10 = new DefaultArtifactVersion("10.0");

        assertTrue(v933.compareTo(v934) == -1);
        assertTrue(v934SS.compareTo(v934) == -1);
        assertTrue(v934.compareTo(v933) == 1);
        assertTrue(v10.compareTo(v934) == 1);
    }
}
