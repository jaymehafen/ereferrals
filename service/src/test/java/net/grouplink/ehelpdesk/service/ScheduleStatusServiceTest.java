package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractDependencyInjectionSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import net.grouplink.ehelpdesk.domain.Group;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 8, 2008
 * Time: 3:40:46 PM
 */
public class ScheduleStatusServiceTest extends AbstractDependencyInjectionSpringContextTests {

    private ScheduleStatusService scheduleStatusService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }

    public void testGetScheduleStatus() {
        List scheduleStatusList = scheduleStatusService.getScheduleStatus();
        assertNotNull(scheduleStatusList);
        if (scheduleStatusList.size() > 0) {
            ScheduleStatus status = (ScheduleStatus) scheduleStatusList.get(0);
            assertNotNull(status);
            Integer statusid = status.getId();
            assertNotNull(statusid);
            assertNotNull(scheduleStatusService.getScheduleStatusById(statusid));
            Group group = status.getGroup();
            assertNotNull(group);
            Integer groupid = group.getId();
            assertNotNull(groupid);
            List list2 = scheduleStatusService.getScheduleStatusByGroupId(groupid);
            assertNotNull(list2);
        }
    }


    public void setScheduleStatusService(ScheduleStatusService scheduleStatusService) {
        this.scheduleStatusService = scheduleStatusService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
