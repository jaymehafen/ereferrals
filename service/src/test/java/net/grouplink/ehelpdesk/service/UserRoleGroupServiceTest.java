package net.grouplink.ehelpdesk.service;

import org.springframework.test.AbstractTransactionalSpringContextTests;
import org.hibernate.SessionFactory;

import java.util.List;

import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.User;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jan 9, 2008
 * Time: 11:01:40 AM
 */
public class UserRoleGroupServiceTest extends AbstractTransactionalSpringContextTests {

    private UserRoleGroupService userRoleGroupService;
    private SessionFactory sessionFactory;

    protected String[] getConfigLocations() {
        return new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                            "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                            "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                            "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"};
    }

    public void testGetUserRoleGroups() {
        List groups = userRoleGroupService.getUserRoleGroup();
        assertNotNull(groups);
        if (groups.size() > 0) {
            UserRoleGroup urg = (UserRoleGroup) groups.get(3);
            assertNotNull(urg);
            Integer urgid = urg.getId();
            assertNotNull(urgid);
            assertNotNull(userRoleGroupService.getUserRoleGroupById(urgid));
            Group group = urg.getGroup();
            assertNotNull(group);
            Integer gid = urg.getId();
            assertNotNull(gid);
            assertNotNull(userRoleGroupService.getUserRoleGroupByGroupId(gid));
            assertNotNull(userRoleGroupService.getUserRoleGroupSelectionByGroupId(gid));
            assertNotNull(userRoleGroupService.getTechnicianByGroupId(gid));
            UserRole role = urg.getUserRole();
            assertNotNull(role);
            User user = role.getUser();
            assertNotNull(user);
            Integer uid = user.getId();
            assertNotNull(uid);
            assertNotNull(userRoleGroupService.getUserRoleGroupByUserId(uid));

        }
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
