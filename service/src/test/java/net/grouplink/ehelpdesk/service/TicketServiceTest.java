package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.service.asset.AssetService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketServiceTest extends AbstractServiceIntegrationTest {

    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private LocationService locationService;
    private StatusService statusService;
    private TicketService ticketService;
    private UserService userService;
    private UserRoleGroupService userRoleGroupService;
    private GroupService groupService;
    private TicketPriorityService ticketPriorityService;
    private AssetService assetService;

    public void testGetTickets() {
        List tickets = ticketService.getTickets();
        assertNotNull(tickets);
        if (tickets.size() != 0) {
            Ticket ticket = (Ticket) tickets.get(1);
            assertNotNull(ticket);
            assertNotNull(ticket.getCategoryOption());
            assertNotNull(ticket.getAssignedTo());
            assertNotNull(ticket.getStatus());
            assertNotNull(ticket.getCreatedDate());
        }
    }

    public void testSaveTicket() {
        // get relational objects for creating a new ticket
        Category category = categoryService.getCategoryByName("Hardware");
        CategoryOption catOption = categoryOptionService.getCategoryOptionByName("Mouse");
        Location location = locationService.getLocationByName("High School 1");
        Group group = groupService.getGroupByName("Facilities");
        Status status = statusService.getStatusByName("onHold");
        
        User barn = userService.getUserById(new Integer(3));
        List urgs = userRoleGroupService.getUserRoleGroup();

        // Create a new ticket
        Ticket ticket = new Ticket();
        ticket.setLocation(location);
        ticket.setCategory(category);
        ticket.setCategoryOption(catOption);
        ticket.setSubmittedBy(barn);
        ticket.setAssignedTo((UserRoleGroup) urgs.get(0));
        ticket.setGroup(group);
        ticket.setStatus(status);
        ticket.setCreatedDate(new Date());

        // attempt the save
        ticketService.saveTicket(ticket, null);

        // if ticket is saved it should at least have an id.
        assertNotNull(ticket.getId());
    }

    public void testDeleteTicket() {
        Ticket t = ticketService.getTicketById(new Integer(42));
        assertNotNull(t);
        User u = userService.getUserById(new Integer(3));
        ticketService.deleteTicket(t, u);
        t = ticketService.getTicketById(new Integer(42));
        assertEquals(Status.DELETED, t.getStatus().getId());
    }

    public void testTicketAudit() {
        Ticket t = ticketService.getTicketById(new Integer(42));
        assertNotNull(t);
        User user = userService.getUserById(new Integer(3));
        t.setContact(user);
        ticketService.saveTicket(t, user);

        t = ticketService.getTicketById(new Integer(42));
        Location l = locationService.getLocationById(new Integer(2));
        t.setLocation(l);
        ticketService.saveTicket(t, user);

        t = ticketService.getTicketById(new Integer(42));
        Group group = groupService.getGroupById(new Integer(2));
        t.setGroup(group);
        ticketService.saveTicket(t, user);

        t = ticketService.getTicketById(new Integer(42));
        Category cat = categoryService.getCategoryById(new Integer(1));
        t.setCategory(cat);
        ticketService.saveTicket(t, user);

        t.getOldTicket().setCategory(null);
        ticketService.saveTicket(t, user);

        CategoryOption catOpt = categoryOptionService.getCategoryOptionById(new Integer(1));
        t.setCategoryOption(catOpt);
        ticketService.saveTicket(t, user);

        t.getOldTicket().setCategoryOption(null);
        ticketService.saveTicket(t, user);

        UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(new Integer(5));
        t.setAssignedTo(urg);
        ticketService.saveTicket(t, user);

        t.getOldTicket().setAssignedTo(null);
        ticketService.saveTicket(t, user);

        TicketPriority pri = ticketPriorityService.getPriorityById(new Integer(-2));
        t.setPriority(pri);
        ticketService.saveTicket(t, user);

        User submittedBy = userService.getUserById(new Integer(6));
        t.setSubmittedBy(submittedBy);
        ticketService.saveTicket(t, user);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2008);
        cal.set(Calendar.MONTH, 3);
        cal.set(Calendar.DAY_OF_MONTH, 9);
        t.setEstimatedDate(DateUtils.truncate(cal.getTime(), Calendar.DAY_OF_MONTH));

        ticketService.saveTicket(t, user);

        t.setAsset(null);
        t.getOldTicket().setAsset(null);
        ticketService.saveTicket(t, user);

        t = ticketService.getTicketById(t.getId());
        Asset asset = assetService.getAssetById(new Integer(2));
        t.setAsset(asset);
        ticketService.saveTicket(t, user);
        t.getOldTicket().setAsset(null);
        ticketService.saveTicket(t, user);
    }

    public void testGetTicketById() {
        Ticket t = ticketService.getTicketById(new Integer(-9999));
        assertNull(t);
    }

    public void testGetTicketsByAdvancedSearch() {
        AdvancedTicketsFilter atf = new AdvancedTicketsFilter();
        atf.setTicketNumber("42");
        List tickets = ticketService.getTicketsByAdvancedSearch(atf);
        assertNotNull(tickets);
        assertTrue(tickets.size() > 0); 
    }

    public void testGetTicketsByOwner() {
        User user = userService.getUserById(new Integer(1));
        List tickets = ticketService.getTicketsByOwner(user, Boolean.TRUE);
        assertNotNull(tickets);
        assertTrue(tickets.size() > 0);
    }

    public void testGetTicketsByOwnerCount() {
        User user = userService.getUserById(new Integer(1));
        Integer count = ticketService.getTicketsByOwnerCount(user);
        assertNotNull(count);
        assertTrue(count.intValue() > 0);
    }

    public void testGetTicketsByUrgIdCount() {
        Integer count = ticketService.getTicketsByUrgIdCount(new Integer(3));
        assertNotNull(count);
        assertTrue(count.intValue() > 0);
    }

    public void testGetTicketsAssignedToByUserId() {
        List tickets = ticketService.getTicketsAssignedToByUserId(new Integer(3));
        assertNotNull(tickets);
        assertTrue(tickets.size() > 0);
    }

    /*public void testGetFilteredTickets() {
        List tickets = ticketService.getFilteredTickets("owner", null, null, null, null, null, null, null, null, null,
                null, null, null, null);

        tickets = ticketService.getFilteredTickets(null, null, null, null, null, null, null, null, null, null,
                null, null, null, null);
    }*/
    
    

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
