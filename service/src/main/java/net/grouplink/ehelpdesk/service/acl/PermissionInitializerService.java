package net.grouplink.ehelpdesk.service.acl;

import net.grouplink.ehelpdesk.domain.acl.PermissionScheme;
import net.grouplink.ehelpdesk.service.acl.dataobj.GlobalPermEntriesDataObj;
import net.grouplink.ehelpdesk.service.acl.dataobj.PermSchemesDataObj;
import net.grouplink.ehelpdesk.service.acl.dataobj.PermissionDataObj;

public interface PermissionInitializerService {

    void init();

    PermissionDataObj parsePermissions();

    GlobalPermEntriesDataObj parseGlobalPermEntries();

    PermSchemesDataObj parsePermissionSchemes();

    void loadDefaultPermissionScheme(PermissionScheme permissionScheme);
}
