package net.grouplink.ehelpdesk.service.mail;

import com.sun.mail.imap.IMAPMessage;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.domain.mail.EmailAttachment;
import net.grouplink.ehelpdesk.domain.mail.EmailMessage;
import net.grouplink.ehelpdesk.service.PropertiesService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeUtility;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * User: jaymehafen
 * Date: Jan 23, 2008
 * Time: 4:13:20 PM
 */
public abstract class AbstractMailMonitor implements MailMonitor {
    private Log log = LogFactory.getLog(AbstractMailMonitor.class);
    private PropertiesService propertiesService;

//    public List<EmailMessage> getMessages(String host, int port, String user, String password, String mailBox) {
//        Folder folder = null;
//        try {
//            log.debug("connecting to mail folder: " + user + "@" + host + ":" + port + "/" + mailBox);
//            folder = connectToFolder(host, port, user, password, mailBox);
//            Message[] messages;
//            try {
//                log.debug("getting messages from folder");
//                messages = folder.getMessages();
//            } catch (MessagingException e) {
//                log.error("error getting messages from folder", e);
//                throw new EmailToTicketRuntimeException(e);
//            }
//
//            return processMessages(messages);
//        } finally {
//            try {
//                if (folder != null) folder.close(true);
//            } catch (MessagingException e) {
//                log.error("error closing folder", e);
//            }
//        }
//    }

    public MailMonitorFolder connectToFolder(String host, int port, String user, String password, String mailBox) {
        MailMonitorFolder mmf = new MailMonitorFolder();

        Properties props = (Properties) System.getProperties().clone();
        customizeSessionProperties(props);
        Session session = Session.getInstance(props, null);
        Store store = null;
        Folder folder = null;
        try {
            try {
                store = session.getStore(getStoreProtocol());
                mmf.setStore(store);
            } catch (NoSuchProviderException e) {
                log.error("error getting store", e);
                throw new EmailToTicketRuntimeException(e);
            }

            try {
                store.connect(host, port, user, password);
                folder = store.getFolder(mailBox);
                if (folder == null || !folder.exists()) {
                    throw new EmailToTicketRuntimeException("invalid folder: " + mailBox);
                }

                folder.open(Folder.READ_WRITE);
                mmf.setFolder(folder);

            } catch (MessagingException e) {
                log.error("error getting folder", e);
                throw new EmailToTicketRuntimeException(e);
            }
        } catch (EmailToTicketRuntimeException e) {
            // close the folder
            if (folder != null) {
                try {
                    folder.close(false);
                } catch (Exception e2) {
                    log.error("error closing mail folder", e);
                }
            }

            // close the store
            if (store != null) {
                try {
                    store.close();
                } catch (Exception e2) {
                    log.error("error closing mail store", e);
                }
            }

            throw e;
        }

        return mmf;
    }

    protected void customizeSessionProperties(Properties props) {
    }

    protected abstract String getStoreProtocol();

//    private List<EmailMessage> processMessages(Message[] messages) {
//        log.debug("processing " + messages.length + " messages");
//        List<EmailMessage> emailMessages = new ArrayList<EmailMessage>();
//        for (int i = 0; i < messages.length; i++) {
//            Message message = messages[i];
//            log.debug("processing message " + (i + 1));
//            if (isValidMessage(message)) {
//                EmailMessage emailMessage = processMessage(message);
//                if (emailMessage != null) {
//                    emailMessages.add(emailMessage);
//                }
//            }
//        }
//        log.debug("successfully processed " + emailMessages.size() + " messages");
//        return emailMessages;
//    }

    public boolean isValidMessage(Message message) {
        try {
            log.debug("message from: " + ArrayUtils.toString(message.getFrom()));
            log.debug("message subject: " + message.getSubject());
            log.debug("message number: " + message.getMessageNumber());
            log.debug("message contentType: " + message.getContentType());

            if (ArrayUtils.isEmpty(message.getFrom())) {
                log.error("message from field is empty, will not process");
                return false;
            }

            if (!isSupportedContentType(message)) {
                log.error("message contains unsupported contentType: " + message.getContentType());
                return false;
            }

            return true;

        } catch (Exception e) {
            log.error("message validation error", e);
            return false;
        }
    }

    public EmailMessage createEmailWrapperForMessage(Message message) {
        try {
            Address from = message.getFrom()[0];
            EmailMessage emailMessage = new EmailMessage();
            emailMessage.setFrom(MimeUtility.decodeText(from.toString()));
            if (from instanceof InternetAddress) {
                InternetAddress fromIA = (InternetAddress) from;
                emailMessage.setFromAddress(fromIA.getAddress());
                emailMessage.setFromPersonalName(fromIA.getPersonal());
            }

            Address[] ccAddressses = message.getRecipients(Message.RecipientType.CC);
            if (ccAddressses != null) {
                for (Address ccAddresss : ccAddressses) {
                    if (ccAddresss instanceof InternetAddress) {
                        emailMessage.addCcAddress(((InternetAddress)ccAddresss).getAddress());
                    }
                }
            }
            
            emailMessage.setSentDate(message.getSentDate());
            //in case the subject > 255 chars, truncate it
            String subject = StringUtils.substring(message.getSubject(), 0, 255);
            if (StringUtils.length(message.getSubject()) > 255) {
                log.warn("message " + message.getMessageNumber() + " from " + message.getFrom()[0] +
                        ", with subject " + subject + "... will have its subject line truncated to 255 characters");
            }

            emailMessage.setSubject(subject);
            emailMessage.setMessageNumber(message.getMessageNumber());
            handleContentType(message, emailMessage);
            return emailMessage;
        } catch (MessagingException e) {
            log.error("caught MessagingException in createEmailWrapperForMessage", e);
            throw new EmailToTicketRuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            log.error("caught UnsupportedEncodingException in createEmailWrapperForMessage", e);
            throw new EmailToTicketRuntimeException(e);
        }
    }

    private boolean isSupportedContentType(Part p) throws MessagingException {
        return (p.isMimeType("text/plain") ||
                p.isMimeType("text/html") ||
                p.isMimeType("multipart/*") ||
                p.isMimeType("application/*") ||
                p.isMimeType("image/*")) ||
                p.isMimeType("audio/wav");
    }

    private void handleContentType(Part part, EmailMessage emailMessage) {
        try {
            String contentType = part.getContentType();
            log.debug("handleContentType.contentType = " + contentType);
            log.debug("part.getFileName() = " + part.getFileName());
            log.debug("part.getDisposition() = " + part.getDisposition());

            if (StringUtils.containsIgnoreCase(part.getDisposition(), "attachment")) {
                storeBinaryAttachment(emailMessage, part);
            } else if (part.isMimeType("multipart/*")) {
                recurseMultipart((Multipart) part.getContent(), emailMessage);
            } else if (part.isMimeType("text/plain")) {
                emailMessage.setTextContent((String) part.getContent());
            } else if (part.isMimeType("text/html")) {
                setHtmlText(emailMessage, part);
            } else {
                log.error("unsupported contentType: " + contentType);
            }
        } catch (MessagingException e) {
            log.error("error determining content type", e);
            throw new EmailToTicketRuntimeException(e);
        } catch (IOException e) {
            log.error("error determining content type", e);
            throw new EmailToTicketRuntimeException(e);
        }
    }

    private void storeBinaryAttachment(EmailMessage emailMessage, Part part) {

        try {
            // get the attachment filename
            String filename = part.getFileName();
            filename = MimeUtility.decodeText(filename);
            if (StringUtils.isBlank(filename)) {
                filename = randomFilename(part.getContentType());
            }

            // check attachment size
            int attachmentSize = getAttachmentSize(part);
            String maxVal = propertiesService.getPropertiesValueByName(PropertiesConstants.MAX_UPLOAD_SIZE);
            // maxUpload is size in MB
            Integer maxUpload;
            if (StringUtils.isBlank(maxVal)) {
                maxUpload = 1;
                propertiesService.saveProperties(PropertiesConstants.MAX_UPLOAD_SIZE, String.valueOf(maxUpload));
            } else {
                maxUpload = Integer.valueOf(maxVal);
            }

            if (attachmentSize > (maxUpload * 1024 * 1024)) {
                throw new EmailToTicketRuntimeException("attachment size is too large");
            }

            EmailAttachment ea = new EmailAttachment();
            ea.setFilename(filename);

            String attachmentTmpPath = System.getProperty("java.io.tmpdir") + File.separatorChar + "ehelpdesk" +
                    File.separatorChar + "e2tattach" + File.separatorChar + emailMessage.getMessageNumber();
            File attachmentTmpDir = new File(attachmentTmpPath);
            if (!attachmentTmpDir.exists()) {
                if (!attachmentTmpDir.mkdirs())
                    throw new IOException("could not create attachment temp dir: " + attachmentTmpPath);
            }

            File file = new File(attachmentTmpPath + File.separatorChar + filename);
            ea.setFile(file);
            ea.setContentType(part.getContentType());

            readAttachmentData(part, file);

            emailMessage.addAttachment(ea);
            
        } catch (MessagingException e) {
            log.error("error storing attachment", e);
            throw new EmailToTicketRuntimeException(e);
        } catch (IOException e) {
            log.error("error storing attachment", e);
            throw new EmailToTicketRuntimeException(e);
        }
    }

    private void readAttachmentData(Part part, File file) throws IOException, MessagingException {
        if (part instanceof MimeBodyPart) {
            // test for javamail 1.4 - MimeBodyPart.saveFile is not available
            // in earlier versions
            boolean methodExists = false;
            try {
                Method saveFileMethod = MimeBodyPart.class.getMethod("saveFile", File.class);
                if (saveFileMethod != null) {
                    methodExists = true;
                }
            } catch (NoSuchMethodException e) {
                methodExists = false;
            }
            if (methodExists) {
                ((MimeBodyPart) part).saveFile(file);
            } else {
                saveMimeBodyPartFile((MimeBodyPart) part, file);
            }
        } else if (part instanceof IMAPMessage) {
            log.debug("part is an IMAPMessage");
            InputStream is = null;
            FileOutputStream fos = null;
            try {
                is = part.getDataHandler().getInputStream();
                fos = new FileOutputStream(file);
                int b;
                long starttime = System.currentTimeMillis();
                while ((b = is.read()) != -1) {
                    fos.write(b);
                }

                long endtime = System.currentTimeMillis();
                log.debug("took " + (endtime - starttime) + " ms to write attachment from IMAPMessage");
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        log.debug("error closing fileoutputstream", e);
                    }
                }

                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        log.debug("error closing inputstream", e);
                    }
                }
            }
        } else {
            log.error("part is not a MimeBodyPart or IMAPMessage");
            throw new EmailToTicketRuntimeException("cannot retrieve attachment, part is not a MimeBodyPart or " +
                    "IMAPMessage");
        }
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    private static class DataHandlerCounter extends OutputStream {
        int count = 0;
        public void write(int b) throws IOException {
            count++;
        }

        int getCount() {
            return count;
        }
    }

    private int getAttachmentSize(Part part) {
        int size = -1;
        try {
            DataHandler dataHandler = part.getDataHandler();
            DataHandlerCounter dhc = new DataHandlerCounter();
            dataHandler.writeTo(dhc);
            size = dhc.getCount();
        } catch (MessagingException e) {
            log.error("errong getting attachment size", e);
        } catch (IOException e) {
            log.error("errong getting attachment size", e);
        }

        return size;
    }

    private String randomFilename(String contentType) {
        String name = RandomStringUtils.randomAlphanumeric(8);
        String extension = null;
        if (StringUtils.isNotBlank(contentType)) {
            Map<String, List<String>> mimeTypes = parseMimeTypes();
            for (Map.Entry<String, List<String>> entry : mimeTypes.entrySet()) {
                if (contentType.toLowerCase().contains(entry.getKey().toLowerCase())) {
                    // found match, get first file extension
                    extension = entry.getValue().get(0);
                    break;
                }
            }
        }

        if (extension != null) {
            name += "." + extension;
        } else {
            name += "." + RandomStringUtils.random(3);
        }
        
        return name;
    }

    private Map<String, List<String>> parseMimeTypes() {
        Map<String, List<String>> mimeTypes = new HashMap<String, List<String>>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/org/springframework/mail/javamail/mime.types"), "UTF-8"));
            String line;
            while ((line = br.readLine()) != null) {
                if (StringUtils.isBlank(line) || line.trim().startsWith("#")) {
                    continue;
                }

                String[] tokens = line.split("\\s+");
                // first token is mime type, the rest are possible file extensions
                String mimeType = tokens[0];
                List<String> extensions = new ArrayList<String>();
                for (int i = 1; i < tokens.length; i++) {
                    String token = tokens[i];
                    extensions.add(token);
                }

                mimeTypes.put(mimeType,  extensions);
            }

            return mimeTypes;
        } catch (UnsupportedEncodingException e) {
            log.error("error", e);
        } catch (IOException e) {
            log.error("error", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception ignored) {
                }
            }
        }

        return null;
    }

    public void saveMimeBodyPartFile(MimeBodyPart mimeBodyPart, File file) throws IOException, MessagingException {
        OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
        InputStream in = mimeBodyPart.getInputStream();
        try {
            byte[] buf = new byte[8192];
            int len;
            while ((len = in.read(buf)) > 0)
                out.write(buf, 0, len);
        } finally {
            // close streams, but don't mask original exception, if any
            try {
                if (in != null)
                    in.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                out.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void setHtmlText(EmailMessage emailMessage, Part part) {
        try {
            emailMessage.setHtmlContent((String) part.getContent());
        } catch (IOException e) {
            log.error("error getting html text", e);
            throw new EmailToTicketRuntimeException(e);
        } catch (MessagingException e) {
            log.error("error getting html text", e);
            throw new EmailToTicketRuntimeException(e);
        }
    }

    private void recurseMultipart(Multipart multipart, EmailMessage emailMessage) {
        log.debug("multipart.getContentType() = " + multipart.getContentType());
        try {
            for (int i = 0; i < multipart.getCount(); i++) {
                BodyPart bodyPart = multipart.getBodyPart(i);
                String bodyPartContentType = bodyPart.getContentType();
                if (!isSupportedContentType(bodyPart) &&
                        !StringUtils.containsIgnoreCase(bodyPart.getDisposition(), "attachment")) {
                    log.error("unsupported contentType: " + bodyPartContentType);
                    continue;
                }

                handleContentType(bodyPart, emailMessage);
            }
        } catch (MessagingException e) {
            log.error("error recursing multipart", e);
            throw new EmailToTicketRuntimeException(e);
        }
    }
}
