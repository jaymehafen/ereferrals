package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TicketTemplateDao;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateCustomFieldValue;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class TicketTemplateServiceImpl implements TicketTemplateService {
    
    private TicketTemplateDao ticketTemplateDao;
    private StatusService statusService;
    private AttachmentService attachmentService;
    private TicketService ticketService;

    public Ticket createTicket(TicketTemplate ticketTemplate, Ticket parentTicket, Map<WorkflowStep, WorkflowStepStatus> workflowStepsMap) {
        Ticket ticket = new Ticket();
        String[] ignoreProps = new String[]{"id", "createdDate", "modifiedDate", "parent", "subtickets",
                "subTicketsList", "customeFieldValues", "attachments", "attchmnt"};

        // copy the properties from the template to the ticket
        BeanUtils.copyProperties(ticketTemplate, ticket, ignoreProps);

        ticket.setCreatedDate(new Date());

        // set the template on the ticket
        ticket.setTicketTemplate(ticketTemplate);
        ticket.setTicketTemplateName(ticketTemplate.getTemplateName());

        // set the parent if it exists
        if (parentTicket != null) {
            parentTicket.addSubTicket(ticket);
        }

        // status
        if (ticket.getStatus() == null) {
            ticket.setStatus(statusService.getInitialStatus());
        }

        // custom fields
        for (TicketTemplateCustomFieldValue ttCustomFieldValue : ticketTemplate.getCustomeFieldValues()) {
            CustomFieldValues cfv = new CustomFieldValues();
            cfv.setCustomField(ttCustomFieldValue.getCustomField());
            cfv.setFieldValue(ttCustomFieldValue.getFieldValue());
            ticket.addCustomeFieldValues(cfv);
        }

        // attachments
        for (Attachment attachment : ticketTemplate.getAttachments()) {
            // reinitialize attachment to avoid lazy property exception
            attachment = attachmentService.getById(attachment.getId());

            Attachment atchCopy = new Attachment();
            atchCopy.setContentType(attachment.getContentType());
            atchCopy.setFileName(attachment.getFileName());
            if (attachment.getFileData() != null) {
                byte[] fileDataCopy = new byte[attachment.getFileData().length];
                System.arraycopy(attachment.getFileData(), 0, fileDataCopy, 0, fileDataCopy.length);
                atchCopy.setFileData(fileDataCopy);
            }
            
            ticket.addAttachment(atchCopy);
            
            // clone the attachment
            /*try {
                Attachment atchClone = attachment.clone();
                ticket.addAttachment(atchClone);
            } catch (CloneNotSupportedException e) {
                log.error("error", e);
            }*/
        }

        // set the workflowStepStatus if one exists
        if (workflowStepsMap != null && ticketTemplate.getWorkflowStep() != null) {
            WorkflowStepStatus wss = workflowStepsMap.get(ticketTemplate.getWorkflowStep());
            if (wss != null) {
                ticket.setWorkflowStepStatus(wss);
                wss.addTicket(ticket);
            }
        }

        // create subtickets
        for (TicketTemplate subTicketTemplate : ticketTemplate.getSubtickets()) {
            createTicket(subTicketTemplate, ticket, workflowStepsMap);
        }

        return ticket;
    }

    public void flush() {
        ticketTemplateDao.flush();
    }

    public int getCountByGroup(Group group) {
        return ticketTemplateDao.getCountByGroup(group);
    }

    public int getCountByCategory(Category category) {
        return ticketTemplateDao.getCountByCategory(category);
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return ticketTemplateDao.getCountByCategoryOption(categoryOption);
    }

    public TicketTemplate getById(Integer id) {
        return ticketTemplateDao.getById(id);
    }

    public List<TicketTemplate> getByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        return ticketTemplateDao.getByTicketTemplateMaster(ticketTemplateMaster);
    }

    public int getTicketTemplateCountByPriorityId(Integer priorityId) {
        return ticketTemplateDao.getTicketTemplateCountByPriorityId(priorityId);
    }

    public List<TicketTemplate> getTopLevelByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        return ticketTemplateDao.getTopLevelByTicketTemplateMaster(ticketTemplateMaster);
    }

    public void save(TicketTemplate ticketTemplate) {
        ticketTemplateDao.save(ticketTemplate);
    }

    public void delete(TicketTemplate ticketTemplate) {
        // check if tt or any subtt's are in use by any tickets
        List<Ticket> tickets = ticketService.getTicketsByTicketTemplate(ticketTemplate);

        if (CollectionUtils.isEmpty(tickets)) {
            ticketTemplateDao.delete(ticketTemplate);
            flush();
        } else {
            setTTInactive(ticketTemplate);
            save(ticketTemplate);
        }
    }

    private void setTTInactive(TicketTemplate tt) {
        tt.setActive(false);

        for (TicketTemplate subtt : tt.getSubtickets()) {
            setTTInactive(subtt);
        }
    }

    public void setTicketTemplateDao(TicketTemplateDao ticketTemplateDao) {
        this.ticketTemplateDao = ticketTemplateDao;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }
}
