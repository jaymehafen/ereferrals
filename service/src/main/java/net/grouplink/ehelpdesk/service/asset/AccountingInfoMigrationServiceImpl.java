package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.dao.asset.AssetDao;
import net.grouplink.ehelpdesk.service.PropertiesService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AccountingInfoMigrationServiceImpl implements AccountingInfoMigrationService {
    private final Log log = LogFactory.getLog(getClass());

    private AccountingInfoMigrationProcessingService accountingInfoMigrationProcessingService;
    private AssetService assetService;
    private PropertiesService propertiesService;
    private AssetDao assetDao;

    public void convertAllAccountingInfo() {

        final int accountInfoCount = assetService.getAccountingInfoCount();
        if (accountInfoCount > 0) {
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    doMigration(accountInfoCount);
                }
            });

            thread.start();
        }
    }

    private void doMigration(int accountInfoCount) {
        int batchSize = 500;
        int numberProcessed = 0;

        log.info("migrating " + accountInfoCount + " AccountingInfo records...");
        long startTime = System.currentTimeMillis();
        while (numberProcessed < accountInfoCount) {
            log.info("migrating AccountingInfo records " + numberProcessed + " - " + (numberProcessed + batchSize));
            accountingInfoMigrationProcessingService.convertAccountInfos(batchSize);
            log.info("done migrating " + numberProcessed + " - " + (numberProcessed + batchSize));
            numberProcessed += batchSize;
        }

        long endTime = System.currentTimeMillis();
        log.info("done migrating AccountingInfo - completed in " + (endTime - startTime) + " ms");
    }

    public void removeAssetAccountingInfoForeignKey() {
        try {
            log.info("removing foreign key constraint on asset.id -> accountinginfo.id");
            assetDao.removeAssetAccountingInfoForeignKey();
        } catch (Exception e) {
            log.error("error trying to remove foreign key constraint on asset.id -> accountinginfo.id", e);
        }
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setAssetDao(AssetDao assetDao) {
        this.assetDao = assetDao;
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setAccountingInfoMigrationProcessingService(AccountingInfoMigrationProcessingService accountingInfoMigrationProcessingService) {
        this.accountingInfoMigrationProcessingService = accountingInfoMigrationProcessingService;
    }
}
