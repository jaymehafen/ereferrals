package net.grouplink.ehelpdesk.service;

import java.util.ArrayList;
import java.util.List;

import net.grouplink.ehelpdesk.dao.PropertiesDao;
import net.grouplink.ehelpdesk.domain.Properties;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import org.apache.commons.lang.StringUtils;

public class PropertiesServiceImpl implements PropertiesService {

	private PropertiesDao properties;
	
	/**
	 * This function will return the value of the properties base name.
	 * This function will only return the first value found for the given name
	 * or an empty string is the value doesn't excist
	 * If the name contains multiple values use getPropertiesValueListByName.
	 * @param name - the name of the properties 
	 */
	public String getPropertiesValueByName(String name) {
		
		String result = "";
		Properties prop = properties.getPropertiesByName(name);
		if(prop != null){
			result = prop.getValue();
		}
		return result;
	}

    public Boolean getBoolValueByName(String name, boolean defaultValue) {
//        Boolean result = Boolean.valueOf(defaultValue);
		Properties prop = properties.getPropertiesByName(name);
		if(prop!= null && !StringUtils.isBlank(prop.getValue())){
			if(prop.getValue().equals("0") || prop.getValue().equals("false")){
                return Boolean.FALSE;
            }
            if(prop.getValue().equals("1") || prop.getValue().equals("true")){
                return Boolean.TRUE;
            }
        }
		return defaultValue;
    }

    /**
	 * This function will return a list of values for the given name.
	 */
	public List<String> getPropertiesValueListByName(String name) {
		List<Properties> aList = properties.getPropertiesListByName(name);
		ArrayList<String> valueList = new ArrayList<String>();
        for (Object anAList : aList) {
            valueList.add(((Properties) anAList).getValue());
        }
		return valueList;
	}

	/**
	 * This function will replace all the value of the name with the given valueList
	 * @param name - the name of the properties
	 * @param valueList - the new list of values for the given name
	 */
	public void saveProperties(String name, List<String> valueList) {
		List<Properties> propList = this.properties.getPropertiesListByName(name);
        this.properties.deletePropertiesList(propList);
        if(valueList.size()>0){
            for (String aValueList : valueList) {
                Properties prop = new Properties();
                prop.setName(name);
                prop.setValue(aValueList);
                this.properties.saveProperties(prop);
            }
        }
	}

    public void flush() {
        this.properties.flush();
    }

    /**
	 * This function will replace the value of an excisting properties name.
	 * If the name is not found a new properties will be created.
	 * @param name The name
	 * @param value The value
	 */
	public void saveProperties(String name, String value) {
		Properties prop = this.properties.getPropertiesByName(name);
		if(prop == null){
			prop = new Properties();
			prop.setName(name);
		}
		prop.setValue(value);
		this.properties.saveProperties(prop);		
	}

	public void setPropertiesDao(PropertiesDao propertiesDao) {
		this.properties = propertiesDao;
	}
	
}
