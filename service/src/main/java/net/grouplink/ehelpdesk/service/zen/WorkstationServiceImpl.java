package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.dao.zen.WorkstationDao;
import net.grouplink.ehelpdesk.domain.zen.Workstation;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;
import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Mar 31, 2009
 * Time: 8:51:56 PM
 */
public class WorkstationServiceImpl implements WorkstationService {
    private WorkstationDao workstationDao;

    public List<Workstation> getAll() {
        return workstationDao.getAll();
    }

    public List<Workstation> search(ZenAssetSearch zas) {
        return workstationDao.search(zas);
    }

    public Workstation getById(byte[] id) {
        return workstationDao.getById(id);
    }

    public Workstation getByIdString(String id) {
        return workstationDao.getByIdString(id);
    }

    public List<Workstation> getByPrimaryUser(String primaryUserGuid) {
        return workstationDao.getByPrimaryUser(primaryUserGuid);
    }

    public List<Workstation> getByUserPrimaryDeviceInformation(List<UserPrimaryDeviceInformation> userPrimaryDeviceInformations) {
        return workstationDao.getByUserPrimaryDeviceInformation(userPrimaryDeviceInformations);
    }

    public void setWorkstationDao(WorkstationDao workstationDao) {
        this.workstationDao = workstationDao;
    }
}
