package net.grouplink.ehelpdesk.service.mail;

/**
 * User: jaymehafen
 * Date: Jan 31, 2008
 * Time: 11:01:41 AM
 */
public class EmailToTicketRuntimeException extends RuntimeException {
    public EmailToTicketRuntimeException() {
        super();
    }

    public EmailToTicketRuntimeException(String message) {
        super(message);
    }

    public EmailToTicketRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmailToTicketRuntimeException(Throwable cause) {
        super(cause);
    }
}
