package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TicketPriorityDao;
import net.grouplink.ehelpdesk.domain.TicketPriority;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketPriorityServiceImpl implements TicketPriorityService {

    private TicketPriorityDao ticketPriorityDao;

    public List<TicketPriority> getPriorities() {
        return ticketPriorityDao.getPriorities();
    }

    public TicketPriority getPriorityById(Integer id) {
        return ticketPriorityDao.getPriorityById(id);
    }

    public TicketPriority getPriorityByName(String name) {
        return ticketPriorityDao.getPriorityByName(name);
    }

    public void savePriority(TicketPriority priority) {
        ticketPriorityDao.savePriority(priority);
    }

    public void deletePriority(TicketPriority priority) {
        ticketPriorityDao.deletePriority(priority);
    }

    public TicketPriority getInitialPriority() {
        return getPriorities().get(0);
    }

    public void flush() {
        this.ticketPriorityDao.flush();
    }

    public void setTicketPriorityDao(TicketPriorityDao ticketPriorityDao) {
        this.ticketPriorityDao = ticketPriorityDao;
    }
}
