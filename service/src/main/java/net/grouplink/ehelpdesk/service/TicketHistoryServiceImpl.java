package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.Constants;
import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.grouplink.ehelpdesk.dao.TicketHistoryDao;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.lang.StringUtils;
import org.hibernate.ScrollableResults;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.context.support.MessageSourceAccessor;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketHistoryServiceImpl extends ApplicationObjectSupport implements TicketHistoryService {

    private TicketHistoryDao ticketHistoryDao;
    private MailService mailService;
    private EmailToTicketConfigService emailToTicketConfigService;
    private TicketService ticketService;

    public void saveTicketHistory(TicketHistory ticketHistory, User user) {
        Ticket ticket = ticketHistory.getTicket();
        if (!ticket.getTicketHistory().contains(ticketHistory)) {
            ticket.addTicketHistory(ticketHistory);
        }

        ticket.setModifiedDate(new Date());
        ticketService.saveTicket(ticket, user);
        flush();
    }

    public void sendEmailNotification(TicketHistory ticketHistory) throws MessagingException, UnsupportedEncodingException {
        Boolean sendLinkToUser;
        String emailDisplayName;
        String emailFromAddress;
        Ticket ticket = ticketHistory.getTicket();
        EmailToTicketConfig etc1;
        MailConfig mc = mailService.getMailConfig();
        if (mailService.isMailConfigured()) {
            if (ticket.getEmailToTicketConfig() != null) {
                etc1 = ticket.getEmailToTicketConfig();
                emailFromAddress = etc1.getReplyEmailAddress();
                emailDisplayName = etc1.getReplyDisplayName();
            } else if ((etc1 = emailToTicketConfigService.getByGroup(ticket.getGroup())) != null) {
                emailFromAddress = etc1.getReplyEmailAddress();
                emailDisplayName = etc1.getReplyDisplayName();
            } else {
                emailFromAddress = mc.getEmailFromAddress();
                emailDisplayName = mc.getEmailDisplayName();
            }

            sendLinkToUser = mc.getSendLinkOnNotification();

            // if notify flags are null set to defaults
            if (ticketHistory.getNotifyTech() == null) ticketHistory.setNotifyTech(mc.getNotifyTechnician());
            if (ticketHistory.getNotifyUser() == null) ticketHistory.setNotifyUser(mc.getNotifyUser());

            //Creating ticket comments
            String htmlMessage = createTicketHistoryHTMLComments(ticketHistory);
            String textMessage = createTicketHistoryTextComments(ticketHistory);
            mailService.notifyPartiesTicketHistory(ticketHistory.getUser(), htmlMessage, textMessage, ticketHistory, emailFromAddress,
                                                   emailDisplayName, sendLinkToUser);
        }
    }

    public String createTicketHistoryTextComments(TicketHistory ticketHistory) {
        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();

        // Check to see if this ticket has a locale definition
        EmailToTicketConfig emailToTicketConfig = ticketHistory.getTicket().getEmailToTicketConfig();
        if(emailToTicketConfig != null){
            String locString = emailToTicketConfig.getLocaleString();
            if(StringUtils.isNotBlank(locString)) locale = new Locale(locString);
        }

        StringBuilder text = new StringBuilder();
        MessageSourceAccessor accessor = getMessageSourceAccessor();
        text.append(accessor.getMessage("ticket.subject", locale)).append(": ");
        text.append(ticketHistory.getSubject());
        text.append("\n");
        if(StringUtils.isNotBlank(ticketHistory.getNote())){
            text.append(ticketHistory.getNote()).append("\n");
        }

        text.append(accessor.getMessage("ticket.commentsby", locale)).append(": ");
        text.append(ticketHistory.getUser().getFirstName());
        text.append(" ");
        text.append(ticketHistory.getUser().getLastName());
        text.append("\n");

        int size;
        size = ticketHistory.getAttachments()==null?0:ticketHistory.getAttachments().size();
        if(size == 0){
        	text.append(accessor.getMessage("ticket.noattachments", locale));
        }
        else if(size == 1){
        	text.append(accessor.getMessage("ticket.oneattachment", locale));
        }
        else{
        	text.append(accessor.getMessage("ticket.multipleattachments", new Object[]{size}, locale));
        }
        text.append("\n");

        return text.toString();
    }

    public String createTicketHistoryHTMLComments(TicketHistory ticketHistory) {
        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();

        // Check to see if this ticket has a locale definition
        EmailToTicketConfig emailToTicketConfig = ticketHistory.getTicket().getEmailToTicketConfig();
        if (emailToTicketConfig != null) {
            String locString = emailToTicketConfig.getLocaleString();
            if (StringUtils.isNotBlank(locString)) locale = new Locale(locString);
        }

        StringBuilder text = new StringBuilder();
        MessageSourceAccessor accessor = getMessageSourceAccessor();
        text.append("<p><strong>").append(accessor.getMessage("ticket.subject", locale)).append(":</strong> ");
        text.append(ticketHistory.getSubject());
        text.append(" </p>");
        if (StringUtils.isNotBlank(ticketHistory.getNote())) {
            text.append(RegexUtils.replaceNewLinesWithBRTags(ticketHistory.getNote()));
        }

        text.append("<p><strong>").append(accessor.getMessage("ticket.commentsby", locale)).append(":</strong> ");
        text.append(ticketHistory.getUser().getFirstName());
        text.append(" ");
        text.append(ticketHistory.getUser().getLastName());
        text.append("</p>");

        int size;
        size = ticketHistory.getAttachments()==null?0:ticketHistory.getAttachments().size();
        text.append("<p><small><i>");
        if(size == 0) {
        	text.append(accessor.getMessage("ticket.noattachments", locale));
        } else if(size == 1) {
        	text.append(accessor.getMessage("ticket.oneattachment", locale));
        } else {
            text.append(accessor.getMessage("ticket.multipleattachments", new Object[]{size}, locale));
        }
        
        text.append("</i></small></p>");

        return text.toString();
    }

    public TicketHistory getTicketHistoryById(Integer id) {
        return ticketHistoryDao.getTicketHistoryById(id);
    }

    public List<TicketHistory> getTicketHistoriesByTicket(Ticket ticket) {
        return ticketHistoryDao.getTicketHistoriesByTicket(ticket);
    }

    public List<TicketHistory> getTicketHistoriesByTicket(Ticket ticket, boolean sortDesc) {
        return ticketHistoryDao.getTicketHistoriesByTicket(ticket, sortDesc);
    }

    public List<TicketHistory> getEventTicketHistoriesByUser(User user) {
        return ticketHistoryDao.getEventTicketHistoriesByUser(user);
    }

    public void setTicketHistoryDao(TicketHistoryDao ticketHistoryDao) {
        this.ticketHistoryDao = ticketHistoryDao;
    }

    public TicketHistory createNewTicketHistory(User submittedByUser, Ticket ticket, int thtype, Locale locale) {
        TicketHistory ticketHistory = initializedTicketHistory(submittedByUser, ticket, thtype);
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, locale);
        String now = dateFormat.format(new Date());
        switch (thtype) {
            case Constants.THISTTYPE_PHONEMADE:
                ticketHistory.setSubject(now + " - " +
                        getMessageSourceAccessor().getMessage("phone.callPlaced", "Placed a phone call"));
                break;
            case Constants.THISTTYPE_PHONERECD:
                ticketHistory.setSubject(now + " - " +
                        getMessageSourceAccessor().getMessage("phone.callReceived", "Received a phone call"));
                break;
            default:

                break;
        }

        return ticketHistory;
    }

    public TicketHistory initializedTicketHistory(User submittedByUser, Ticket ticket, int thtype){
        TicketHistory ticketHistory = new TicketHistory();
        ticketHistory.setUser(submittedByUser);
        ticketHistory.setTicket(ticket);
        ticketHistory.setCommentType(thtype);
        ticketHistory.setCreatedDate(new Date());
        ticketHistory.setActive(true);
        return ticketHistory;
    }

    public void saveComments(User user, String updateType, Ticket ticket) {
        String comments = ticketService.createHTMLComments(ticket, ticket.getOldTicket());
        TicketHistory ticketHistory = initializedTicketHistory(user, ticket, Constants.THISTTYPE_COMMENT);
        String subject = getMessageSourceAccessor().getMessage(updateType, "Ticket Update");
        ticketHistory.setSubject(subject);
        ticketHistory.setNote(comments);
        saveTicketHistory(ticketHistory, user);
    }

    public ScrollableResults getAllTicketHistoryByGroupId(Integer groupId) {
        return this.ticketHistoryDao.getAllTicketHistoryByGroupId(groupId);
    }

    public int getHistoryCount(Integer groupId) {
        return this.ticketHistoryDao.getHistoryCount(groupId);
    }

    public void flush(){
        this.ticketHistoryDao.flush();
    }

    public void clear() {
        this.ticketHistoryDao.clear();
    }

    public void setEmailToTicketConfigService(EmailToTicketConfigService emailToTicketConfigService) {
        this.emailToTicketConfigService = emailToTicketConfigService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }
}
