package net.grouplink.ehelpdesk.service.mail;

import java.util.Properties;

/**
 * User: jaymehafen
 * Date: Jan 25, 2008
 * Time: 3:59:59 PM
 */
public class ImapSslMailMonitor extends AbstractMailMonitor {
    protected String getStoreProtocol() {
        return "imaps";
    }

    protected void customizeSessionProperties(Properties props) {
        super.customizeSessionProperties(props);
        props.setProperty("mail.imaps.partialfetch", "false");
        props.setProperty("mail.imaps.connectiontimeout", "60000");
    }
}
