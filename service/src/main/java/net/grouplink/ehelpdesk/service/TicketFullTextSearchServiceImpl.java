package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TicketFullTextSearchDao;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;

import java.util.List;

/**
 * Author: zpearce
 * Date: 4/27/11
 */
public class TicketFullTextSearchServiceImpl implements TicketFullTextSearchService {
    private TicketFullTextSearchDao ticketFullTextSearchDao;
    private String[] noteAndHistoryFields = new String[] {"subject", "id", "note", "ticketHistory.subject", "ticketHistory.note", "group.name", "location.name", "category.name", "categoryOption.name"};

    public void reindexTickets() {
        ticketFullTextSearchDao.reindexTickets();
    }

    public int getReindexStatusPercent() {
        return ticketFullTextSearchDao.getReindexStatusPercent();
    }

    public List<Ticket> search(String query, int startPage, int pageSize, User user) {
        return ticketFullTextSearchDao.fullTextSearch(query, noteAndHistoryFields, startPage, pageSize, user);
    }

    public int searchCount(String query, User user) {
        return ticketFullTextSearchDao.fullTextSearchCount(query, noteAndHistoryFields, user);
    }

    public void setTicketFullTextSearchDao(TicketFullTextSearchDao ticketFullTextSearchDao) {
        this.ticketFullTextSearchDao = ticketFullTextSearchDao;
    }
}
