package net.grouplink.ehelpdesk.service.acl;

import net.grouplink.ehelpdesk.dao.acl.PermissionDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.acl.*;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PermissionServiceImpl implements PermissionService {

    private final Log log = LogFactory.getLog(getClass());
    
    private PermissionDao permissionDao;
    private RoleService roleService;
    private UserRoleGroupService userRoleGroupService;
    private UserService userService;

    public boolean hasGlobalPermission(String permissionKey) {
        User user = userService.getLoggedInUser();
        return hasGlobalPermission(user, permissionKey);
    }

    public boolean hasGlobalPermission(User user, String permissionKey) {
        return hasGlobalPermission(user, getPermissionByKey(permissionKey));
    }

    public boolean hasGlobalPermission(User user, Permission p) {
        user = userService.getUserById(user.getId());
        if(user.isAdmin()) return true;
        boolean hasPermission;
        GlobalPermissionEntry gp = getGlobalPermissionEntry(p);
        if (gp == null) {
            log.debug("no globalPermissionEntry found for permission key " + p.getKey());
            return false;
        }
        // check if the user exists in either the user set or role set
        hasPermission = checkPermissionActorsGlobal(user, gp.getPermissionActors());
        return hasPermission;
    }

    public boolean hasGroupPermission(String permissionKey, Group group) {
        User user = userService.getLoggedInUser();
        return hasGroupPermission(user, permissionKey, group);
    }

    public boolean hasGroupPermission(User user, String permissionKey, Group group) {
        return hasGroupPermission(user, getPermissionByKey(permissionKey), group);
    }

    public boolean hasGroupPermission(User user, Permission permission, Group group) {
        if (user == null) return false;

        user = userService.getUserById(user.getId());
        if(user.isAdmin()) return true;

        if (group == null) {
            log.warn("group is null");
            return true;
        }

        if (group.getPermissionScheme() == null) {
            log.error("permission scheme is null");
            return false;
        }

        PermissionSchemeEntry pse = group.getPermissionScheme().getPermissionSchemeEntry(permission);

        if (pse == null) {
            log.error("permission scheme entry is null");
            return false;
        }

        boolean hasPermission = checkPermissionActorsScheme(user, group, pse.getPermissionActors());

        // check if the permission is for view on a field. if they don't have
        // view permission, but do have change permission on the field,
        // permission should be granted
        if (!hasPermission) {
            if (permission.getKey().contains(".view.")) {
                String key = StringUtils.replace(permission.getKey(), ".view.", ".change.");
                Permission changePerm = getPermissionByKey(key);
                if (changePerm != null) {
                    return hasGroupPermission(user, key, group);
                }
            }
        }

        return hasPermission;
    }

    public boolean canUserViewTicket(User user, Ticket ticket) {
        boolean canView = false;

        if (user.getId().equals(User.ADMIN_ID)) {
            return true;
        }

         /*
            user can view ticket if any of the following are true:
             - user is contact
             - user is submittedby
             - user is assignedto
             - user is manager of tickets group
             - user is technician of tickets group and ticket is assigned to ticket pool
             - user has permission to 'viewAllGroupTickets' for tickets group
        */

        if (ticket.getContact().equals(user) || ticket.getSubmittedBy().equals(user)) {
            // user same as ticket contact or submittedby
            canView = true;
        } else if (ticket.getAssignedTo() != null && ticket.getAssignedTo().getUserRole() != null &&
                ticket.getAssignedTo().getUserRole().getUser().equals(user)) {
            // user same as assignedTo
            canView = true;
        } else if (isUserInRole(user, ticket.getGroup(), Role.ROLE_MANAGER_ID)) {
            // user is manager
            canView = true;
        } else if (isUserInRole(user, ticket.getGroup(), Role.ROLE_TECHNICIAN_ID) && (ticket.getAssignedTo() == null || ticket.getAssignedTo().getUserRole() == null)) {
            // user is tech and ticket assigned to ticket pool
            canView = true;
        } else if (hasGroupPermission(user, "group.viewAllGroupTickets", ticket.getGroup())) {
            // user has viewAllGroupTickets permission
            canView = true;
        }

        return canView;
    }

    private boolean isUserInRole(User user, Group group, Integer role) {
        for (UserRole userRole : user.getUserRoles()) {
            if (userRole.getRole() != null && userRole.getRole().getId().equals(role)) {
                for (UserRoleGroup userRoleGroup : userRole.getUserRoleGroup()) {
                    if (userRoleGroup.getGroup().getId().equals(group.getId())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public GlobalPermissionEntry getGlobalPermissionEntry(Permission permission) {
        return permissionDao.getGlobalPermissionEntry(permission);
    }

    public Permission getPermissionByKey(String key) {
        return permissionDao.getPermissionByKey(key);
    }

    public List<PermissionSubject> getPermissionSubjects() {
        return permissionDao.getPermissionSubjects();
    }

    public PermissionSubject getPermissionSubjectByModelClass(String modelClass) {
        return permissionDao.getPermissionSubjectByModelClass(modelClass);
    }

    public PermissionSubject getPermissionSubjectByModelClassAndField(String modelClass, String fieldName) {
        return permissionDao.getPermissionSubjectByModelClassAndField(modelClass, fieldName);
    }

    public PermissionSubject getPermissionSubjectByModelNameAndField(String modelName, String fieldName) {
        return permissionDao.getPermissionSubjectByModelNameAndField(modelName, fieldName);
    }

    public void savePermissionSubject(PermissionSubject permissionSubject) {
        permissionDao.savePermissionSubject(permissionSubject);
    }

    private boolean checkPermissionActorsGlobal(User user, PermissionActors pc) {
        boolean hasPermission = false;

        if (pc.getUsers().contains(user)) {
            hasPermission = true;
        } else {
            // check if any of the users roles are in the role set
            for (UserRole userRole : user.getUserRoles()) {
                if (!userRole.isActive()) continue;
                hasPermission = checkRoles(pc.getRoles(), userRole.getRole());
                if (hasPermission) break;

            }
        }

        return hasPermission;
    }

    private boolean checkPermissionActorsScheme(User user, Group group, PermissionActors pa) {
        boolean hasPermission;

        if (pa.getUsers().contains(user)) {
            hasPermission = true;
        } else {
            Role role;
            // get the user role for the group
            UserRoleGroup urg = userRoleGroupService.getUserRoleGroupByUserIdAndGroupId(user.getId(), group.getId());
            if (urg != null) {
                role = urg.getUserRole().getRole();
            } else {
                role = getGlobalUserRole(user, Role.ROLE_ADMINISTRATOR_ID);
                if (role == null) {
                    role = getGlobalUserRole(user, Role.ROLE_USER_ID);
                }
            }

            if (role == null) {
                return false;
            }

            // if roles doesn't contain users' role, check if roles contains a lesser role
            // (e.g. if users role is manager, but roles only contains technician, user should still be authorized)
            hasPermission = checkRoles(pa.getRoles(), role);
        }

        return hasPermission;
    }

    private Role getGlobalUserRole(User user, Integer roleId) {
        for (UserRole userRole : user.getUserRoles()) {
            if (!userRole.isActive()) continue;
            if (userRole.getRole().getId().equals(roleId)) {
                return userRole.getRole();
            }
        }

        return null;
    }

    private boolean checkRoles(Set<Role> roles, Role role) {
        boolean hasPermission = false;
        List<Role> rolesToCheck = getEffectiveRolesFromHierarchy(role);
        for (Role rtc : rolesToCheck) {
            if (roles.contains(rtc)) {
                hasPermission = true;
                break;
            }
        }
        return hasPermission;
    }

    private List<Role> getEffectiveRolesFromHierarchy(Role r) {
        List<Role> rolesToCheck = new ArrayList<Role>();
        if (r.getId().equals(Role.ROLE_ADMINISTRATOR_ID)) {
            rolesToCheck.add(r);
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_MANAGER_ID));
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_TECHNICIAN_ID));
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_MEMBER_ID));
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_USER_ID));
        } else if (r.getId().equals(Role.ROLE_MANAGER_ID)) {
            rolesToCheck.add(r);
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_TECHNICIAN_ID));
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_MEMBER_ID));
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_USER_ID));
        } else if (r.getId().equals(Role.ROLE_TECHNICIAN_ID)) {
            rolesToCheck.add(r);
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_MEMBER_ID));
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_USER_ID));
        } else if (r.getId().equals(Role.ROLE_MEMBER_ID)) {
            rolesToCheck.add(r);
            rolesToCheck.add(roleService.getRoleById(Role.ROLE_USER_ID));
        } else {
            rolesToCheck.add(r);
        }
        return rolesToCheck;
    }

    public List<Permission> getGlobalPermissions() {
        return permissionDao.getGlobalPermissions();
    }

    public List<Permission> getSchemePermissions() {
        return permissionDao.getSchemePermissions();
    }

    public List<Permission> getFieldPermissionsByModelName(String modelName) {
        return permissionDao.getFieldPermissionsByModelName(modelName);
    }

    public void savePermission(Permission permission) {
        permissionDao.savePermission(permission);
    }

    public void deletePermission(Permission permission) {
        permissionDao.deletePermission(permission);
    }

    public List<PermissionScheme> getPermissionSchemes() {
        return permissionDao.getPermissionSchemes();
    }

    public void savePermissionScheme(PermissionScheme permissionScheme) {
        permissionDao.savePermissionScheme(permissionScheme);
    }

    public void deletePermissionScheme(PermissionScheme permissionScheme) {
        permissionDao.deletePermissionScheme(permissionScheme);
    }

    public void saveGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry) {
        permissionDao.saveGlobalPermissionEntry(globalPermissionEntry);
    }

    public void deleteGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry) {
        permissionDao.deleteGlobalPermissionEntry(globalPermissionEntry);
    }

    public void savePermissionSchemeEntry(PermissionSchemeEntry permissionSchemeEntry) {
        permissionDao.savePermissionSchemeEntry(permissionSchemeEntry);
    }

    public List<GlobalPermissionEntry> getGlobalPermissionEntries() {
        return permissionDao.getGlobalPermissionEntries();
    }

    public Permission getPermissionById(Integer gpId) {
        return permissionDao.getPermissionById(gpId);
    }

    public PermissionScheme getPermissionSchemeById(Integer id) {
        return permissionDao.getPermissionSchemeById(id);
    }

    public List<Permission> getGroupPermissions() {
        return permissionDao.getGroupPermissions();
    }

    public PermissionSchemeEntry getPermissionSchemeEntry(PermissionScheme permissionScheme, Permission permission) {
        return permissionDao.getPermissionSchemeEntry(permissionScheme, permission);
    }

    public void setPermissionDao(PermissionDao permissionDao) {
        this.permissionDao = permissionDao;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
