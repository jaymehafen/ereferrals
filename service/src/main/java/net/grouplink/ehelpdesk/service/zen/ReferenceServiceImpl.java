package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.dao.zen.ReferenceDao;

public class ReferenceServiceImpl implements ReferenceService {
    private ReferenceDao referenceDao;

    public byte[] getZuidForObjectUid(String objectUid) {
        return referenceDao.getZuidForObjectUid(objectUid);
    }

    public void setReferenceDao(ReferenceDao referenceDao) {
        this.referenceDao = referenceDao;
    }
}
