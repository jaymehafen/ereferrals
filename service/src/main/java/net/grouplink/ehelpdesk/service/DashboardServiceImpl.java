package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.DashboardDao;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.constants.JasperReportsConstants;
import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import javax.jws.WebService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebService(endpointInterface = "net.grouplink.ehelpdesk.service.DashboardService")
public class DashboardServiceImpl implements DashboardService {

    DashboardDao dashboardDao;
    TicketFilterService ticketFilterService;
    UserService userService;
    private WidgetService widgetService;

    public List<Dashboard> getAll() {
        return dashboardDao.getAll();
    }

    public Dashboard getById(Integer id) {
        return dashboardDao.getById(id);
    }

    public void save(Dashboard dashboard) {
        dashboardDao.save(dashboard);
    }

    public void delete(Dashboard dashboard) {
        dashboardDao.delete(dashboard);
    }

    public Integer getTicketCount(Integer ticketFilterId, Integer userId) {
        return ticketFilterService.getTicketCount(ticketFilterId, userId);
    }

    public HashMap<String, Integer> getTicketCountOneGroup(Integer widgetId, Integer userId) {
        HashMap<String, Integer> groupByMap = new HashMap<String, Integer>();

//        TicketFilter ticketFilter = ticketFilterService.getById(ticketFilterId);
        Widget widget = widgetService.getById(widgetId);
//        List<Ticket> tickets = ticketFilterService.getTickets(ticketFilter, userService.getUserById(userId));
        List<Ticket> tickets = ticketFilterService.getTickets(widget.getReport().getTicketFilter(), userService.getUserById(userId));
        for (Ticket ticket : tickets) {
            String name;

//            switch (ticketFilter.getGroupBy()) {
            switch (widget.getReport().getGroupBy()) {
                case JasperReportsConstants.GROUP:
                    name = ticket.getGroup().getName();
                    break;
                case JasperReportsConstants.LOCATION:
                    name = ticket.getLocation().getName();
                    break;
                case JasperReportsConstants.CATEGORY:
                    name = (ticket.getCategory() != null) ? ticket.getCategory().getName() : "";
                    break;
                case JasperReportsConstants.CATEGORY_OPTION:
                    name = (ticket.getCategoryOption() != null) ? ticket.getCategoryOption().getName() : "";
                    break;
                case JasperReportsConstants.TECHNICIAN:
                    UserRoleGroup urg = ticket.getAssignedTo();
                    if (urg.getUserRole() == null) {
                        name = urg.getGroup().getName();
                    } else {
                        name = urg.getUserRole().getUser().getFirstName() + " " +
                                urg.getUserRole().getUser().getLastName();
                    }
                    break;
                case JasperReportsConstants.PRIORITY:
                    name = ticket.getPriority().getName();
                    break;
                case JasperReportsConstants.STATUS:
                    name = ticket.getStatus().getName();
                    break;
                default:
                    name = null;
                    break;
            }

            if (groupByMap.containsKey(name)) groupByMap.put(name, groupByMap.get(name) + 1);
            else groupByMap.put(name, 1);
        }
        
        return groupByMap;
    }

    public HashMap<String, Map<String, Integer>> getTicketCountTwoGroups(Integer widgetId, Integer userId) {

        HashMap<String, Map<String, Integer>> groupBy = new HashMap<String, Map<String, Integer>>();
//        TicketFilter ticketFilter = ticketFilterService.getById(ticketFilterId);
        Widget widget = widgetService.getById(widgetId);

//        List<Ticket> tickets = ticketFilterService.getTickets(ticketFilter, userService.getUserById(userId));
        List<Ticket> tickets = ticketFilterService.getTickets(widget.getReport().getTicketFilter(), userService.getUserById(userId));

        for (Ticket ticket : tickets) {
            String byName = null;
//            switch (ticketFilter.getGroupBy()) {
            switch (widget.getReport().getGroupBy()) {
                case JasperReportsConstants.GROUP:
                    byName = ticket.getGroup().getName();
                    break;
                case JasperReportsConstants.LOCATION:
                    byName = ticket.getLocation().getName();
                    break;
                case JasperReportsConstants.CATEGORY:
                    byName = (ticket.getCategory() != null) ? ticket.getCategory().getName() : "";
                    break;
                case JasperReportsConstants.CATEGORY_OPTION:
                    byName = (ticket.getCategoryOption() != null) ? ticket.getCategoryOption().getName() : "";
                    break;
                case JasperReportsConstants.TECHNICIAN:
                    UserRoleGroup urg = ticket.getAssignedTo();
                    if (urg.getUserRole() == null) {
                        byName = urg.getGroup().getName();
                    } else {
                        byName = urg.getUserRole().getUser().getFirstName() + " " +
                                urg.getUserRole().getUser().getLastName();
                    }
                    break;
                case JasperReportsConstants.PRIORITY:
                    byName = ticket.getPriority().getName();
                    break;
                case JasperReportsConstants.STATUS:
                    byName = ticket.getStatus().getName();
                    break;
            }

            String bybyName = null;
//            switch (ticketFilter.getGroupByBy()) {
            switch (widget.getReport().getGroupByBy()) {
                case JasperReportsConstants.GROUP:
                    bybyName = ticket.getGroup().getName();
                    break;
                case JasperReportsConstants.LOCATION:
                    bybyName = ticket.getLocation().getName();
                    break;
                case JasperReportsConstants.CATEGORY:
                    bybyName = (ticket.getCategory() != null) ? ticket.getCategory().getName() : "";
                    break;
                case JasperReportsConstants.CATEGORY_OPTION:
                    bybyName = (ticket.getCategoryOption() != null) ? ticket.getCategoryOption().getName() : "";
                    break;
                case JasperReportsConstants.TECHNICIAN:
                    UserRoleGroup urg = ticket.getAssignedTo();
                    if (urg.getUserRole() == null) {
                        bybyName = urg.getGroup().getName();
                    } else {
                        bybyName = urg.getUserRole().getUser().getFirstName() + " " +
                                urg.getUserRole().getUser().getLastName();
                    }
                    break;
                case JasperReportsConstants.PRIORITY:
                    bybyName = ticket.getPriority().getName();
                    break;
                case JasperReportsConstants.STATUS:
                    bybyName = ticket.getStatus().getName();
                    break;
            }

            Map<String, Integer> tmpGroupByBy = new HashMap<String, Integer>();
            if(groupBy.containsKey(byName)){
                tmpGroupByBy = groupBy.get(byName);
            }
            
            if (tmpGroupByBy.containsKey(bybyName)) {
                tmpGroupByBy.put(bybyName, tmpGroupByBy.get(bybyName) + 1);
            } else {
                tmpGroupByBy.put(bybyName, 1);
            }

            groupBy.put(byName, tmpGroupByBy);
        }
        
        return groupBy;
    }

    public void flush() {
        dashboardDao.flush();
    }

    public void setDashboardDao(DashboardDao dashboardDao) {
        this.dashboardDao = dashboardDao;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setWidgetService(WidgetService widgetService) {
        this.widgetService = widgetService;
    }
}
