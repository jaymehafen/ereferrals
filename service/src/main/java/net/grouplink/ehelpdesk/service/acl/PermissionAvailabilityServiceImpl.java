package net.grouplink.ehelpdesk.service.acl;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.acl.Permission;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.List;

public class PermissionAvailabilityServiceImpl implements PermissionAvailabilityService {

    private Log log = LogFactory.getLog(getClass());
    private UserService userService;
    private RoleService roleService;
    private PermissionService permissionService;
    private LicenseService licenseService;

    public List<Role> getAvailableRolesForPermission(Permission permission) {
        PermissionAvailability permissionAvailability = getPermissionAvailability();
        List<Role> roles = permissionAvailability.getRoles(permission);
        if (roles == null) {
            log.error("no list of available roles for permission " + permission.getKey());
        }
        return roles;
    }

    public List<User> getAvailableUsersForPermission(Permission permission) {
        List<Role> roles = getAvailableRolesForPermission(permission);
        List<User> users = userService.getUsersWithAnyRole(roles);
        return users;
    }

    private PermissionAvailability getPermissionAvailability() {
        PermissionAvailabilityImpl pa = new PermissionAvailabilityImpl();

        Role adminRole = roleService.getRoleById(Role.ROLE_ADMINISTRATOR_ID);
        Role managerRole = roleService.getRoleById(Role.ROLE_MANAGER_ID);
        Role techRole = roleService.getRoleById(Role.ROLE_TECHNICIAN_ID);
        Role wfpRole = roleService.getRoleById(Role.ROLE_WFPARTICIPANT_ID);
        Role memberRole = roleService.getRoleById(Role.ROLE_MEMBER_ID);
        Role userRole = roleService.getRoleById(Role.ROLE_USER_ID);

        List<Role> techAndHigherRoles = Arrays.asList(adminRole, managerRole, techRole);
        List<Role> wfpAndHigherRoles;
        if (licenseService.getWFParticpantCount() > 0) {
            wfpAndHigherRoles = Arrays.asList(adminRole, managerRole, techRole, wfpRole);
        } else {
            wfpAndHigherRoles = Arrays.asList(adminRole, managerRole, techRole);
        }
        List<Role> userAndHigherRoles;
        if (licenseService.getWFParticpantCount() > 0) {
            userAndHigherRoles = Arrays.asList(adminRole, managerRole, techRole, wfpRole, memberRole, userRole);
        } else {
            userAndHigherRoles = Arrays.asList(adminRole, managerRole, techRole, memberRole, userRole);
        }

        pa.addPermission(permissionService.getPermissionByKey("global.systemConfig"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("global.userManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("global.userComments"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("global.assetTracker"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("global.customFieldManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("global.surveyManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("global.dashboardManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("global.publicTicketFilterManagement"), techAndHigherRoles);

        pa.addPermission(permissionService.getPermissionByKey("group.manageGroupSettings"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.scheduledTaskManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.ticketTemplateManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.launchNonPublicTicketTemplates"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.assignmentManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.customFieldManagement"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.viewAllGroupTickets"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.deleteTicketAttachments"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.scheduleApptsAndTasks"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.createTicket"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.kbView"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.kbManage"), techAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.findUser"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.massEdit"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("group.deleteTicket"), techAndHigherRoles);

        pa.addPermission(permissionService.getPermissionByKey("ticket.view.contact"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.contact"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.submittedBy"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.assignedTo"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.assignedTo"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.priority"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.priority"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.status"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.status"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.location"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.location"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.group"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.group"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.category"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.category"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.categoryOption"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.categoryOption"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.subject"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.subject"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.note"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.note"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.createdDate"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.modifiedDate"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.estimatedDate"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.estimatedDate"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.workTime"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.workTime"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.cc"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.cc"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.bc"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.bc"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.parent"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.knowledgeBase"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.knowledgeBase"), wfpAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.zenInformation"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.ticketHistory"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.ticketHistory"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.attachments"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.attachments"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.subtickets"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.subtickets"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.ticketAudit"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.asset"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.asset"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.surveyData"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.zenAsset"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.zenAsset"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.notifyTech"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.notifyTech"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.view.notifyUser"), userAndHigherRoles);
        pa.addPermission(permissionService.getPermissionByKey("ticket.change.notifyUser"), userAndHigherRoles);

        return pa;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }
}
