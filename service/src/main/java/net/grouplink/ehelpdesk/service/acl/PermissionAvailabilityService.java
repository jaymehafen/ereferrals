package net.grouplink.ehelpdesk.service.acl;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.acl.Permission;

import java.util.List;

public interface PermissionAvailabilityService {

    List<Role> getAvailableRolesForPermission(Permission permission);

    List<User> getAvailableUsersForPermission(Permission permission);
}
