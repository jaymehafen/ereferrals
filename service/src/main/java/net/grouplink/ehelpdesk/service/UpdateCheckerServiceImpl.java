package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class UpdateCheckerServiceImpl implements UpdateCheckerService {
    private final Log log = LogFactory.getLog(getClass());

    private PropertiesService propertiesService;
    private String currentVersion;
    private String updateUrl = "http://www.grouplink.net/updatechecker/ehd.txt";

    public boolean isUpdateAvailable() {
        return propertiesService.getBoolValueByName(PropertiesConstants.UPDATE_AVAILABLE, false);
    }

    public String getDownloadUrl() {
        return propertiesService.getPropertiesValueByName(PropertiesConstants.UPDATE_DOWNLOAD_URL);
    }

    public void checkForUpdate() {
        log.debug("checking for update...");
        boolean updateAvailable = false;
        String downloadUrl;

        BufferedReader br = null;
        try {
            URL url = new URL(updateUrl);

            log.debug("open url: " + updateUrl);
            br = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while ((str = br.readLine()) != null) {
                log.debug("read line: " + str);
                // format is: version|downloadUrl
                String[] parts = str.split("\\|");
                String latestVersion = parts[0];
                downloadUrl = parts[1];

                // compare versions using maven-artifact class
                DefaultArtifactVersion lastestVer = new DefaultArtifactVersion(latestVersion);
                DefaultArtifactVersion currentVer = new DefaultArtifactVersion(currentVersion);

                log.debug("comparing current version: " + currentVersion + " to latest version: " + latestVersion);
                if (currentVer.compareTo(lastestVer) < 0) {
                    updateAvailable = true;
                }

                log.debug("updateAvailable: " + updateAvailable);

                log.debug("saving properties");
                propertiesService.saveProperties(PropertiesConstants.UPDATE_AVAILABLE, String.valueOf(updateAvailable));
                propertiesService.saveProperties(PropertiesConstants.UPDATE_DOWNLOAD_URL, downloadUrl);
            }

        } catch (Exception e) {
            log.error("error checking for update", e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ignore) { }
            }
        }
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }
}
