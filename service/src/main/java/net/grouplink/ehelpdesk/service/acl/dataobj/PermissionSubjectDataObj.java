package net.grouplink.ehelpdesk.service.acl.dataobj;

import net.grouplink.ehelpdesk.domain.acl.PermissionSubject;

import java.util.ArrayList;
import java.util.List;

/**
 * author: zpearce
 * Date: 1/12/12
 * Time: 3:44 PM
 */
public class PermissionSubjectDataObj {
    private List<FieldPermissionSubjectDataObj> fieldPermissionSubjects = new ArrayList<FieldPermissionSubjectDataObj>();
    private List<PermissionSubject> permissionSubjects = new ArrayList<PermissionSubject>();

    public List<FieldPermissionSubjectDataObj> getFieldPermissionSubjects() {
        return fieldPermissionSubjects;
    }

    public void setFieldPermissionSubjects(List<FieldPermissionSubjectDataObj> fieldPermissionSubjects) {
        this.fieldPermissionSubjects = fieldPermissionSubjects;
    }

    public List<PermissionSubject> getPermissionSubjects() {
        return permissionSubjects;
    }

    public void setPermissionSubjects(List<PermissionSubject> permissionSubjects) {
        this.permissionSubjects = permissionSubjects;
    }
}
