package net.grouplink.ehelpdesk.service.mail;

import java.util.Properties;

public class Pop3StartTlsRequiredMailMonitor extends Pop3StartTlsMailMonitor {
    @Override
    protected void customizeSessionProperties(Properties props) {
        super.customizeSessionProperties(props);
        props.setProperty("mail.pop3.starttls.required", "true");
    }
}
