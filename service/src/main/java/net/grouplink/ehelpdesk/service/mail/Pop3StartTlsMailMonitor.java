package net.grouplink.ehelpdesk.service.mail;

import java.util.Properties;

public class Pop3StartTlsMailMonitor extends Pop3MailMonitor {
    @Override
    protected void customizeSessionProperties(Properties props) {
        super.customizeSessionProperties(props);
        props.setProperty("mail.pop3.starttls.enable", "true");
    }
}
