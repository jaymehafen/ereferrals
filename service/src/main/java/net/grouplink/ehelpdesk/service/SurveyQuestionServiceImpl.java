package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.SurveyQuestionDao;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 6, 2008
 * Time: 9:53:29 AM
 */
public class SurveyQuestionServiceImpl implements SurveyQuestionService {

    private SurveyQuestionDao surveyQuestionDao;

    public List<SurveyQuestion> getDefaultSurveyQuestions() {
        return surveyQuestionDao.getDefaultSurveyQuestions();
    }

    public List<SurveyQuestion> getSurveyQuestionsBySurveyId(Integer surveyId) {
        return surveyQuestionDao.getSurveyQuestionsBySurveyId(surveyId);
    }

    public SurveyQuestion getSurveyQuestionById(Integer id) {
        return surveyQuestionDao.getSurveyQuestionById(id);
    }

    public void saveSurveyQuestion(SurveyQuestion surveyQuestion) {
        surveyQuestionDao.saveSurveyQuestion(surveyQuestion);
    }

    public void deleteSurveyQuestion(SurveyQuestion surveyQuestion) {
        surveyQuestionDao.deleteSurveyQuestion(surveyQuestion);
    }

    public boolean hasDuplicateSurveyQuestionText(SurveyQuestion surveyQuestion) {
        return surveyQuestionDao.hasDuplicateSurveyQuestionText(surveyQuestion);
    }

    public void flush() {
        surveyQuestionDao.flush();
    }

    public List<String> getByUniqueQuestionText() {
        return surveyQuestionDao.getByUniqueQuestionText();
    }

    // Spring injection
    public void setSurveyQuestionDao(SurveyQuestionDao surveyQuestionDao) {
        this.surveyQuestionDao = surveyQuestionDao;
    }
}
