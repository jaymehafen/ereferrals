package net.grouplink.ehelpdesk.service.mail;

import java.util.Properties;

public class ImapStartTlsIfAvailMailMonitor extends ImapMailMonitor {

    @Override
    protected void customizeSessionProperties(Properties props) {
        super.customizeSessionProperties(props);

        // set tls enabled property
        props.setProperty("mail.imap.starttls.enable", "true");
    }
}
