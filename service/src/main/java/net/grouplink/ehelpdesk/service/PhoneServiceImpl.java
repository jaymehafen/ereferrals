package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.PhoneDao;
import net.grouplink.ehelpdesk.domain.Phone;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 5:15 PM
 */
public class PhoneServiceImpl implements PhoneService{
    private PhoneDao phoneDao;
    private Log log = LogFactory.getLog(getClass());

    public void savePhone(Phone phone) {
        phoneDao.savePhone(phone);
    }

    public boolean deletePhone(Phone phone) {
        try{
            phoneDao.deletePhone(phone);
            phoneDao.flush();
        } catch (Exception e){
            log.info("Unable to delete phone");
            return false;
        }
        return true;
    }

    public Phone getById(int phoneId) {
        return phoneDao.getById(phoneId);
    }

    public void setPhoneDao(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }
}
