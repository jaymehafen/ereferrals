package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TicketTemplateRecurrenceScheduleDao;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.TicketTemplateRecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.List;

public class TicketTemplateRecurrenceScheduleServiceImpl implements TicketTemplateRecurrenceScheduleService {
    private Log log = LogFactory.getLog(getClass());
    
    private TicketTemplateRecurrenceScheduleDao ticketTemplateRecurrenceScheduleDao;
    private RecurrenceScheduleService recurrenceScheduleService;
    private TemplateMasterService templateMasterService;
    private UserService userService;
    private TicketTemplateLaunchService ticketTemplateLaunchService;

    public void processSchedules() {
        log.debug("checking for recurring ticket templates");
        List<RecurrenceSchedule> schedules = recurrenceScheduleService.getRunningTicketTemplateSchedules();
        log.debug("found " + schedules.size() + " recurring ticket template schedules");
        Date currentDate = new Date();
        for (RecurrenceSchedule schedule : schedules) {
            log.debug("processing recurrenceSchedule: " + schedule.getId());
            log.debug("getting ticketTemplateRecurrenceSchedule");
            TicketTemplateRecurrenceSchedule ttrs = getByTicketTemplateMasterAndSchedule(schedule.getTicketTemplateMaster(), schedule);
            log.debug("ttrs == null: " + (ttrs == null));
            Date lastActionDate = null;
            int occurrenceCount = 0;
            if (ttrs != null) {
                lastActionDate = ttrs.getLastAction();
                occurrenceCount = ttrs.getOccurrenceCount();
                log.debug("ttrs lastAction: " + lastActionDate + ", occurrenceCount: " + occurrenceCount);
            }

            log.debug("checking if schedule meets schedule criteria");
            boolean processSchedule = recurrenceScheduleService.meetsScheduleCriteria(schedule, currentDate, lastActionDate, occurrenceCount);
            log.debug("meets schedule criteria: " + processSchedule);
            if (processSchedule) {
                log.debug("calling createTickets");
                createTickets(schedule);
            }
        }
    }

    public void cleanupOrphanedPending() {
        log.debug("called cleanupOrphanedPending ticket templates");
        // find ticket template launch's older than 1 day that have not been activated
        Date activeDate = new Date();
        activeDate = DateUtils.addDays(activeDate, -1);

        List<TicketTemplateLaunch> pendingLaunches = ticketTemplateLaunchService.getPendingOlderThan(activeDate);
        log.debug("found " + pendingLaunches.size() + " orphaned ticket templates");
        for (TicketTemplateLaunch pendingLaunch : pendingLaunches) {
            log.debug("cleaning up ticket template launch " + pendingLaunch.getId());
            ticketTemplateLaunchService.delete(pendingLaunch);
        }
    }

    private void createTickets(RecurrenceSchedule schedule) {
        User systemUser = userService.getUserById(User.ADMIN_ID);
        log.debug("calling templateMasterService.launchTicketTemplateMaster");
        TicketTemplateLaunch ttLaunch = templateMasterService.launchTicketTemplateMaster(schedule.getTicketTemplateMaster(), systemUser);
        List<Ticket> errorTickets = ticketTemplateLaunchService.readyForActivation(ttLaunch);
        if (CollectionUtils.isNotEmpty(errorTickets)) {
            log.error("cannot activate launch id " + ttLaunch.getId() + " because of invalid tickets");
            log.info("aborting launch");
            ticketTemplateLaunchService.abort(ttLaunch);
            return;
        }

        log.debug("calling ticketTemplateLaunchService.activate()");
        ticketTemplateLaunchService.activate(ttLaunch, systemUser);

        log.debug("got ticketTemplateLaunch, # of tickets: " + ttLaunch.getTickets().size());

        log.debug("updating ticketTemplateRecurrenceSchedule");
        TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule = getByTicketTemplateMasterAndSchedule(schedule.getTicketTemplateMaster(), schedule);
        if (ticketTemplateRecurrenceSchedule == null) {
            ticketTemplateRecurrenceSchedule = new TicketTemplateRecurrenceSchedule();
            ticketTemplateRecurrenceSchedule.setOccurrenceCount(1);
            ticketTemplateRecurrenceSchedule.setRecurrenceSchedule(schedule);
            ticketTemplateRecurrenceSchedule.setTicketTemplateMaster(schedule.getTicketTemplateMaster());
        } else {
            ticketTemplateRecurrenceSchedule.setOccurrenceCount(ticketTemplateRecurrenceSchedule.getOccurrenceCount() + 1);
        }

        ticketTemplateRecurrenceSchedule.setLastAction(new Date());
        save(ticketTemplateRecurrenceSchedule);
    }

    private TicketTemplateRecurrenceSchedule getByTicketTemplateMasterAndSchedule(TicketTemplateMaster ticketTemplateMaster, RecurrenceSchedule schedule) {
        return ticketTemplateRecurrenceScheduleDao.getByTicketTemplateMasterAndSchedule(ticketTemplateMaster,  schedule);
    }

    public void save(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule) {
        ticketTemplateRecurrenceScheduleDao.save(ticketTemplateRecurrenceSchedule);
    }

    public void delete(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule) {
        ticketTemplateRecurrenceScheduleDao.delete(ticketTemplateRecurrenceSchedule);
    }

    public void setTicketTemplateRecurrenceScheduleDao(TicketTemplateRecurrenceScheduleDao ticketTemplateRecurrenceScheduleDao) {
        this.ticketTemplateRecurrenceScheduleDao = ticketTemplateRecurrenceScheduleDao;
    }

    public void setRecurrenceScheduleService(RecurrenceScheduleService recurrenceScheduleService) {
        this.recurrenceScheduleService = recurrenceScheduleService;
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTicketTemplateLaunchService(TicketTemplateLaunchService ticketTemplateLaunchService) {
        this.ticketTemplateLaunchService = ticketTemplateLaunchService;
    }
}
