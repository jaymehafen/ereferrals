package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.RoleDao;
import net.grouplink.ehelpdesk.domain.Role;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RoleServiceImpl implements RoleService {

	private RoleDao roleDao;
    private ResourceBundleMessageSource resourceBundleMessageSource;

    public Role getRoleById(Integer id) {
		return roleDao.getRoleById(id);
	}
	
	public Role getRoleByName(String roleName){
		return roleDao.getRoleByName(roleName);
	}

	public List<Role> getRoles() {
		return roleDao.getRoles();
	}

    public List<Role> getMngAndTechRoles() {
        return roleDao.getMngAndTechRoles();
    }

    public List<Role> getNonAdminRoles() {
        return roleDao.getNonAdminRoles();
    }

    public List<Role> getGroupRoles() {
        return roleDao.getGroupRoles();
    }

    public List<String> getRoleNames(Locale locale) {
        List<Role> roles = getRoles();
        List<String> retList = new ArrayList<String>();
        for (Object role1 : roles) {
            Role role;
            role = (Role) role1;
            retList.add(resourceBundleMessageSource.getMessage(role.getName(), null, locale));
        }
        return retList;
    }

    public void init() {
        Role groupMember = roleDao.getRoleById(Role.ROLE_MEMBER_ID);
        if (groupMember == null) {
            groupMember = new Role();
            groupMember.setId(Role.ROLE_MEMBER_ID);
            groupMember.setName("ROLE_MEMBER");
            roleDao.saveOnly(groupMember);
        }

        Role wfParticipant = roleDao.getRoleById(Role.ROLE_WFPARTICIPANT_ID);
        if(wfParticipant == null){
            wfParticipant = new Role();
            wfParticipant.setId(Role.ROLE_WFPARTICIPANT_ID);
            wfParticipant.setName("ROLE_WFPARTICIPANT");
            roleDao.saveOnly(wfParticipant);
        }
    }

    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public void setResourceBundleMessageSource(ResourceBundleMessageSource resourceBundleMessageSource) {
        this.resourceBundleMessageSource = resourceBundleMessageSource;
    }
}
