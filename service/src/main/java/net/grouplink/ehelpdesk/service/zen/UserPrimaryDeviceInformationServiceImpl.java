package net.grouplink.ehelpdesk.service.zen;

import net.grouplink.ehelpdesk.dao.zen.UserPrimaryDeviceInformationDao;
import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;

import java.util.List;

public class UserPrimaryDeviceInformationServiceImpl implements UserPrimaryDeviceInformationService {
    private UserPrimaryDeviceInformationDao userPrimaryDeviceInformationDao;

    public List<UserPrimaryDeviceInformation> getByZuid(byte[] zuid) {
        return userPrimaryDeviceInformationDao.getByZuid(zuid);
    }

    public void setUserPrimaryDeviceInformationDao(UserPrimaryDeviceInformationDao userPrimaryDeviceInformationDao) {
        this.userPrimaryDeviceInformationDao = userPrimaryDeviceInformationDao;
    }
}
