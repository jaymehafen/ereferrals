package net.grouplink.ehelpdesk.service;

import org.springframework.security.core.AuthenticationException;

import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Provider;
import java.security.Security;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mrollins
 * @version 1.0
 */
public class LicenseServiceImpl implements LicenseService {

    private static final Pattern SECTION_PATTERN = Pattern.compile("^\\s*\\[([^\\]]*)\\].*$");
    private static final Pattern ENTRY_PATTERN = Pattern.compile("^(.*)=(.*)$");

    private String realLicensePath;
    private static Map<String, String> licenseProps;
    private boolean imhere;

    public void init(String realPath) {
        realLicensePath = realPath;
        try {
            licenseProps = getLicenseProperties();
            imhere = true;
        } catch (Exception e) {
            imhere = false;
        }
    }

    public Map<String, String> getLicenseProperties() throws Exception {
        File file = new File(realLicensePath);
        FileInputStream fis = new FileInputStream(file);
        InputStream stream = getEncStream(fis);

        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String sectionName = null;

        Map<String, String> retMap = new HashMap<String, String>();
        while (true) {
            String line = in.readLine();
            if (line == null)
                break;
            Matcher matcher = SECTION_PATTERN.matcher(line);
            if (matcher.matches()) {
                sectionName = matcher.group(1);
            } else {
                matcher = ENTRY_PATTERN.matcher(line);
                if (matcher.matches()) {
                    String name = matcher.group(1).trim();
                    String value = matcher.group(2).trim();
                    retMap.put(sectionName + "." + name, value);
                }
            }
        }
        in.close();
        return retMap;
    }

    /**
     * @param stream the stream to decode
     * @return decoded inputstream
     * @throws Exception if there is an exception
     * @noinspection ResultOfMethodCallIgnored
     */
    private InputStream getEncStream(InputStream stream) throws Exception {
        ByteArrayInputStream is;
        BufferedInputStream bs = new BufferedInputStream(stream);
        byte encode[] = new byte[bs.available()];
        bs.read(encode);
        Provider sunJce = new com.sun.crypto.provider.SunJCE();
        Security.addProvider(sunJce);
        byte[] raw = "XKteBAoK".getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "DES");
        javax.crypto.Cipher dcipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
        dcipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = dcipher.doFinal(encode);
        is = new ByteArrayInputStream(decrypted);
        return is;
    }

    public void uploadLicenseFile(String path, byte[] bytes) throws IOException {
        FileOutputStream fos;
        DataOutputStream dos;
        File file = new File(path);
        fos = new FileOutputStream(file);
        dos = new DataOutputStream(fos);
        dos.write(bytes, 0, bytes.length);
        init(path);
    }

    public String getDescription() {
        if (licenseProps == null) return null;
        String retVal = null;
        if (licenseProps.containsKey(HELPDESK_TECHNICIANS)) {
            retVal = licenseProps.get(HELPDESK_DESC);
        }
        return retVal;
    }

    public Integer getTechnicianCount() throws AuthenticationException {
        if (licenseProps == null) return null;
        int retVal = 0;
        if (licenseProps.containsKey(HELPDESK_TECHNICIANS)) {
            retVal = Integer.parseInt(licenseProps.get(HELPDESK_TECHNICIANS));
        }
        return retVal;
    }

    public Integer getWFParticpantCount() throws AuthenticationException {
        if (licenseProps == null) return null;
        int retVal = 0;
        if (licenseProps.containsKey(HELPDESK_WF_PARTICIPANTS)) {
            retVal = Integer.parseInt(licenseProps.get(HELPDESK_WF_PARTICIPANTS));
        }
        return retVal;
    }

    public Integer getGroupCount() throws AuthenticationException {
        if (licenseProps == null) return null;

        int retVal = 2;
        if (licenseProps.containsKey(HELPDESK_GROUPS)) {
            retVal = Integer.parseInt(licenseProps.get(HELPDESK_GROUPS));
        }

        return retVal;
    }

    public Date getGlobalBombDate() throws AuthenticationException {
        if (licenseProps == null) return null;
        Date retDate = null;
        if(licenseProps.containsKey(HELPDESK_BOMB)){
            String hbomb = licenseProps.get(HELPDESK_BOMB);
            long dd = getDelphiDate(Integer.parseInt(hbomb));
            retDate = new Date(dd);
        }
        return retDate;
    }

    public Date getUPDate() {
        if (licenseProps == null) return null;
        Date retDate = null;
        if(licenseProps.containsKey(HELPDESK_UPGRADE_DATE)){
            int iUpDate = Integer.parseInt(licenseProps.get(HELPDESK_UPGRADE_DATE));
            if(iUpDate == 0) return null;
            long dd = getDelphiDate(iUpDate);
            retDate = new Date(dd);
        }
        return retDate;
    }

    public Boolean isEnterprise() throws AuthenticationException {
    	if (licenseProps == null) return Boolean.FALSE;
    	Boolean enterprise = Boolean.FALSE;
    	if (licenseProps.containsKey(HELPDESK_ENTERPRISE)){
    		enterprise = Boolean.valueOf(licenseProps.get(HELPDESK_ENTERPRISE));
    	}
    	return enterprise;
    }

    public boolean areYouThereLicense() {
        return imhere;
    }

    private static long getDelphiDate(int date) {
        long ms = 1000 * 60 * 60 * 24; // milliseconds in a day
        long td = (date - 25569) * ms;  // TDate - JavaDate diff * milliseconds
        Calendar c = Calendar.getInstance();
        java.util.TimeZone tz = c.getTimeZone();
        int mod = tz.getOffset(td); // timezone compensation
        return (td - mod);
    }

    public Date getReleaseDate() {
        String releaseDate = "11/14/2013";
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            return sdf.parse(releaseDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isValidEHDLicense(String testFilePath) throws Exception {
        File file = new File(testFilePath);
        Map<String, String> retMap = new HashMap<String, String>();
        FileInputStream fis = new FileInputStream(file);
        InputStream stream;
        try{
            stream = getEncStream(fis);
        } catch (IllegalBlockSizeException ibse){
            return false;
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String sectionName = null;
        while (true) {
            String line = in.readLine();
            if (line == null)
                break;
            Matcher matcher = SECTION_PATTERN.matcher(line);
            if (matcher.matches()) {
                sectionName = matcher.group(1);
            } else {
                matcher = ENTRY_PATTERN.matcher(line);
                if (matcher.matches()) {
                    String name = matcher.group(1).trim();
                    String value = matcher.group(2).trim();
                    retMap.put(sectionName + "." + name, value);
                }
            }
        }
        in.close();
        return retMap.containsKey(HELPDESK_TECHNICIANS);
    }

    public boolean isDashboard() {
        if(licenseProps == null) return Boolean.FALSE;
        Boolean dashboard = true;
        if(licenseProps.containsKey((HELPDESK_DASHBOARD))){
            dashboard = Boolean.valueOf(licenseProps.get(HELPDESK_DASHBOARD));
        }
        return dashboard;
    }

    public boolean isTicketTemplates() {
        if(licenseProps == null) return Boolean.FALSE;
        Boolean tts = true;
        if(licenseProps.containsKey((HELPDESK_TICKETTEMPLATES))){
            tts = Boolean.valueOf(licenseProps.get(HELPDESK_TICKETTEMPLATES));
        }
        return tts;
    }

    public boolean isScheduledReports() {
        if(licenseProps == null) return Boolean.FALSE;
        Boolean schedReps = true;
        if(licenseProps.containsKey((HELPDESK_SCHEDULEDREPORTS))){
            schedReps = Boolean.valueOf(licenseProps.get(HELPDESK_SCHEDULEDREPORTS));
        }
        return schedReps;
    }

    public boolean isAssetTracker() {
        if(licenseProps == null) return Boolean.FALSE;
        Boolean at = true;
        if(licenseProps.containsKey((HELPDESK_ASSETTRACKER))){
            at = Boolean.valueOf(licenseProps.get(HELPDESK_ASSETTRACKER));
        }
        return at;
    }

    public boolean isZen10Integration() {
        if(licenseProps == null) return Boolean.FALSE;
        Boolean zen10 = true;
        if(licenseProps.containsKey((HELPDESK_ZEN10INTEGRATION))){
            zen10 = Boolean.valueOf(licenseProps.get(HELPDESK_ZEN10INTEGRATION));
        }
        return zen10;
    }

    public boolean isMaintenanceExpired() {
        Date upgradeDate = getUPDate();
        return (upgradeDate != null && upgradeDate.before(new Date()));
    }
}
