package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.CustomFieldsDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldOption;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Ticket;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 3:14:41 PM
 */
public class CustomFieldsServiceImpl implements CustomFieldsService {

    public CustomFieldsDao customFieldsDao;
    public CategoryOptionService categoryOptionService;

    /**
     * Calls the customFieldsDao in the persistent layer to save the customField object to the database.
     *
     * @param customField the custom field
     */
    public void saveCustomField(CustomField customField) {
        customFieldsDao.saveCustomField(customField);
    }

    /**
     * Gets a list of custom field pojos from a give category option id.
     *
     * @param id the category option id
     * @return list of custom field pojos
     */
    public List<CustomField> getCustomFieldsByCategoryOptionId(Integer id) {
        List<CustomField> customFields = new ArrayList<CustomField>();
        CategoryOption categoryOption = categoryOptionService.getCategoryOptionById(id);
        if (categoryOption != null) {
            // filter out the inactive custom fields
            Set<CustomField> cfs = categoryOption.getCustomFields();
            for (Object cf : cfs) {
                CustomField customField = (CustomField) cf;
                if (customField.isActive()) customFields.add(customField);
            }
            Collections.sort(customFields);
        }
        return customFields;
    }


    public CustomField getCustomFieldById(Integer id) {
        return customFieldsDao.getCustomFieldById(id);
    }

    public void deleteCustomField(CustomField customField) {
        customFieldsDao.deleteCustomField(customField);
    }

    public void setCustomFieldsDao(CustomFieldsDao customFieldsDao) {
        this.customFieldsDao = customFieldsDao;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    /**
     * Returns a map where the key is the label of the custom field to be displayed and the value is the html with value
     * set if any.
     *
     * @param ticket the ticket
     * @return a map of the customfields for the category option id and the ticket id
     */
    public Map<String, String> getCustomFieldsInHtml(Ticket ticket) {
        if (ticket.getCategoryOption() == null) return new HashMap<String, String>();
        Map<String, String> retMap = new LinkedHashMap<String, String>();
        List<CustomField> cfields = getCustomFieldsByCategoryOptionId(ticket.getCategoryOption().getId());

        for (CustomField cfield : cfields) {
            CustomFieldValues cfv = null;
            for (Object o : ticket.getCustomeFieldValues()) {
                CustomFieldValues cfvFound = (CustomFieldValues) o;
                if (cfvFound.getCustomField().getId().equals(cfield.getId())) {
                    cfv = cfvFound;
                    break;
                }
            }//done persisting -->>
            String html = getHtml(cfield, cfv, ticket.getId());
            retMap.put(colonize(cfield.getName()), html);
        }
        return retMap;
    }

    public CustomField getCustomFieldByOldIdAndCatOptId(Integer oldId, Integer catOptId) {
        return this.customFieldsDao.getCustomFieldByOldIdAndCatOptId(oldId, catOptId);
    }

    private String colonize(String name) {
        if (name.trim().endsWith(":" )) {
            return name.trim();
        } else {
            return name + " : ";
        }
    }

    private String getHtml(CustomField customField, CustomFieldValues cfv, Integer ticketId) {
        String tid = ticketId == null ? "new" : ticketId.toString();
        String name = "cf_" + customField.getId() + "_" + tid;
        String value = (cfv == null ||  StringUtils.isBlank(cfv.getFieldValue())) ? "" : cfv.getFieldValue();

        if (customField.getFieldType().equals(CustomField.VALUETYPE_TEXT)) {
            return "<input type=\"text\" name=\"" + name + "\" id=\"" + name + "\" value=\"" + value + "\" />";
        } else if (customField.getFieldType().equals(CustomField.VALUETYPE_TEXTAREA)) {
            return "<textarea name=\"" + name + "\" id=\"" + name + "\">" + value + "</textarea>";
        } else if (customField.getFieldType().equals(CustomField.VALUETYPE_RADIO)) {
            StringBuilder sbdd = new StringBuilder();
                            for (Object o : customField.getOptions()) {
                                CustomFieldOption customFieldOption = (CustomFieldOption) o;
                                sbdd.append("<input type=\"radio\" name=\"").append(name).append("\" value=\"")
                                        .append(customFieldOption.getValue()).append("\" ");
                                if (value != null && value.equals(customFieldOption.getValue())) sbdd.append("checked");
                                sbdd.append(" />").append(customFieldOption.getDisplayValue());
                            }
                            return sbdd.toString();
        } else if (customField.getFieldType().equals(CustomField.VALUETYPE_CHECKBOX)) {
            StringBuilder sbdd = new StringBuilder("<input type=\"hidden\" name=\"_" + name + "\" />");
                            sbdd.append("<input type=\"checkbox\" name=\"").append(name).append("\"");
                            if (value != null && value.equals("true" )) sbdd.append(" checked=\"checked\"");
                            sbdd.append(" />" );
                            return sbdd.toString();
        } else if (customField.getFieldType().equals(CustomField.VALUETYPE_SELECT)) {
            StringBuilder sbdd = new StringBuilder("<select name=\"" + name + "\" id=\"" + name + "\"><option></option>" );
                            for (Object o : customField.getOptions()) {
                                CustomFieldOption customFieldOption = (CustomFieldOption) o;
                                sbdd.append("<option value=\"").append(customFieldOption.getValue()).append("\" ");
                                if (value != null && value.equals(customFieldOption.getValue())) sbdd.append("selected");
                                sbdd.append(">").append(customFieldOption.getDisplayValue()).append("</option>");
                            }
                            sbdd.append("</select>" );
                            return sbdd.toString();
        } else {
            return "";
        }
    }

    public List<CustomField> getAll() {
        return customFieldsDao.getAll();
    }

    public List<String> getByUniqueName() {
        return customFieldsDao.getByUniqueName();
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return customFieldsDao.getCountByCategoryOption(categoryOption);
    }

    public List<CustomField> getGlobalCustomFields() {
        return customFieldsDao.getGlobalCustomFields();
    }

    public List<CustomField> getByLocation() {
        return customFieldsDao.getByLocation();
    }

    public void migrateFieldTypes() {

        // Get the count of all custom fields where customFieldType is not blank
        if(customFieldsDao.getWithCustomFieldTypeCount() == 0) return;

        // fill in the fieldType property with appropriate value from customFieldType
        List<CustomField> customFields = customFieldsDao.getWithCustomFieldType();
        for (CustomField customField : customFields) {
            switch(customField.getCustomFieldType().getId()){
                case 1: // text
                    customField.setFieldType(CustomField.VALUETYPE_TEXT);
                    break;
                case 2: // textarea
                    customField.setFieldType(CustomField.VALUETYPE_TEXTAREA);
                    break;
                case 3: // radio
                    customField.setFieldType(CustomField.VALUETYPE_RADIO);
                    break;
                case 4: // checkbox
                    customField.setFieldType(CustomField.VALUETYPE_CHECKBOX);
                    break;
                case 5: // select
                    customField.setFieldType(CustomField.VALUETYPE_SELECT);
                    break;
                default:
                    break;
            }
            customField.setCustomFieldType(null);
            saveCustomField(customField);
        }
    }

    public List<CustomField> getCustomFields(Location location, Group group, Category category, CategoryOption categoryOption) {
        return customFieldsDao.getCustomFields(location, group, category, categoryOption);
    }

    /**
     * Gets all CustomFields for a Group, including CustomFields tied to
     * Categories or CategoryOptions within the Group.
     * @param group the group
     * @return list of CustomFields
     */
    public List<CustomField> getCustomFieldsByGroup(Group group) {
        return customFieldsDao.getCustomFieldsByGroup(group);
    }

}
