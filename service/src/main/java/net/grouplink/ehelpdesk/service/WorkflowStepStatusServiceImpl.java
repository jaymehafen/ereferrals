package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.WorkflowStepStatusDao;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.io.UnsupportedEncodingException;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

import javax.mail.MessagingException;

public class WorkflowStepStatusServiceImpl implements WorkflowStepStatusService {
    private Log log = LogFactory.getLog(getClass());
    private WorkflowStepStatusDao workflowStepStatusDao;
    private TicketService ticketService;
    private UserService userService;

    public WorkflowStepStatus getById(Integer id) {
        return workflowStepStatusDao.getById(id);
    }

    public void save(WorkflowStepStatus workflowStepStatus) {
        workflowStepStatusDao.save(workflowStepStatus);
    }

    public void delete(WorkflowStepStatus workflowStepStatus) {
        workflowStepStatusDao.delete(workflowStepStatus);
    }

    public List<WorkflowStepStatus> getIncompleteSteps() {
        return workflowStepStatusDao.getIncompleteSteps();
    }

    public void checkForCompletedSteps() {
        log.debug("checking for completed ticket template workflow steps");
        try {
            // get a list of all workflowstepstatus's where completeddate is null
            // and wfss.wfs.completedstatus is not null
            List<WorkflowStepStatus> incompleteSteps = getIncompleteSteps();
            log.debug("found " + incompleteSteps.size() + " incomplete workflow steps");
            for (WorkflowStepStatus incompleteStep : incompleteSteps) {
                try {
                    log.debug("checking if tickets have been completed for workflowStepStatus " + incompleteStep.getId());
                    // check if each ticket matches the completedStatus
                    boolean matches = true;
                    for (Ticket ticket : incompleteStep.getTickets()) {
                        if (!ticket.getStatus().equals(incompleteStep.getWorkflowStep().getStepCompletedStatus())) {
                            matches = false;
                            log.debug("ticket " + ticket.getId() + " has not yet been completed, workflow step not complete");
                            break;
                        }
                    }

                    if (matches) {
                        log.debug("workflowStep has been completed, launching next workflow step");
                        incompleteStep.setCompletedDate(new Date());
                        save(incompleteStep);
                        // launch next workflowstep
                        launchNextWorkflowStep(incompleteStep);
                    }
                } catch (Exception e) { 
                    log.error("error", e);
                }
            }
        } catch (Exception e) {
            log.error("error", e);
        }
    }

    public int getCountByWorkflowStep(WorkflowStep workflowStep) {
        return workflowStepStatusDao.getCountByWorkflowStep(workflowStep);
    }

    private void launchNextWorkflowStep(WorkflowStepStatus lastCompletedStepStatus) {
        log.debug("called launchNextWorkflowStep, lastCompletedStepStatus: " + lastCompletedStepStatus.getId());
        List<WorkflowStep> workflowStepList = lastCompletedStepStatus.getWorkflowStep().getTicketTemplateMaster().getWorkflowSteps();
        // if there is a workflow step after the lastCompletedStep launch it
        int lastIndex = workflowStepList.indexOf(lastCompletedStepStatus.getWorkflowStep());
        log.debug("lastStepIndex: " + lastIndex + ", workflowStepList size: " + workflowStepList.size());
        if (workflowStepList.size() > lastIndex) {
            log.debug("determining next workflow step");
            // get the next work flow set
            WorkflowStep nextStep = workflowStepList.get(lastIndex + 1);
            log.debug("next workflow step: " + nextStep.getId());
            // get the workflowstepstatus for this workflowstep
            WorkflowStepStatus nextStepStatus = null;
            Set<WorkflowStepStatus> workflowStepStatuses = lastCompletedStepStatus.getTicketTemplateLaunch().getWorkflowStepStatuses();
            for (WorkflowStepStatus wss : workflowStepStatuses) {
                if (wss.getWorkflowStep().equals(nextStep)) {
                    nextStepStatus = wss;
                    break;
                }
            }

            assert nextStepStatus != null;

            log.debug("nextStepStatus id: " + nextStepStatus.getId());
            Set<Ticket> tickets = nextStepStatus.getTickets();
            log.debug("nextStep contains " + tickets.size() + " tickets");
            for (Ticket ticket : tickets) {
                log.debug("setting ticket " + ticket.getId() + " to active status");
                ticket.setTicketTemplateStatus(Ticket.TT_STATUS_ACTIVE);
                ticket.setCreatedDate(null);
                ticketService.saveTicket(ticket, userService.getUserById(User.ADMIN_ID));
                ticketService.flush();
                String htmlComments = ticketService.createHTMLComments(ticket, null);
                String textComments = ticketService.createTextComments(ticket, null);
                // setting the notify flags to null will cause them to use the default notifiation rules
                ticket.setNotifyTech(null);
                ticket.setNotifyUser(null);
                log.debug("sending email notification");
                try {
                    ticketService.sendEmailNotification(ticket, userService.getUserById(User.ADMIN_ID), htmlComments, textComments);
                } catch (MessagingException e) {
                    log.error("error sending notification for email " + ticket.getId(), e);
                } catch (UnsupportedEncodingException e) {
                    log.error("error sending notification for email " + ticket.getId(), e);
                }
            }
        }
    }

    public void setWorkflowStepStatusDao(WorkflowStepStatusDao workflowStepStatusDao) {
        this.workflowStepStatusDao = workflowStepStatusDao;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
