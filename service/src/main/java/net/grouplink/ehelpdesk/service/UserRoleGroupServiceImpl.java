package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.UserRoleGroupDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Implements a User service.
 *
 * @author mrollins
 * @version 1.0
 * @see UserService
 */
public class UserRoleGroupServiceImpl implements UserRoleGroupService {

    private Log log = LogFactory.getLog(getClass());

    private UserRoleGroupDao userRoleGroupDao;


    public void setUserRoleGroupDao(UserRoleGroupDao userRoleGroupDao) {
        this.userRoleGroupDao = userRoleGroupDao;
    }

    public List<UserRoleGroup> getUserRoleGroup() {
        return userRoleGroupDao.getUserRoleGroup();
    }

    public List<UserRoleGroup> getAllUserRoleGroup() {
        return userRoleGroupDao.getAllUserRoleGroup();
    }

    public List<UserRoleGroup> getUserRoleGroupByGroupId(Integer id) {
        return userRoleGroupDao.getUserRoleGroupByGroupId(id);
    }

    public List<UserRoleGroup> getManagerTechnicianUserRoleGroupByGroupId(Group group) {
        return userRoleGroupDao.getManagerTechnicianUserRoleGroupByGroupId(group);
    }

    public UserRoleGroup getUserRoleGroupById(Integer id) {
        return userRoleGroupDao.getUserRoleGroupById(id);
    }

    public void deleteUserRoleGroup(UserRoleGroup userRoleGroup) {
        userRoleGroupDao.deleteUserRoleGroup(userRoleGroup);
    }

    public void saveUserRoleGroup(UserRoleGroup userRoleGroup) {
        userRoleGroupDao.saveUserRoleGroup(userRoleGroup);
    }

    public UserRole getUserRoleByUserIdRoleId(Integer userId, Integer roleId) {
        return userRoleGroupDao.getUserRoleByUserIdRoleId(userId, roleId);
    }

    public List<UserRoleGroup> getUserRoleGroupByUserId(Integer id) {
        return userRoleGroupDao.getUserRoleGroupByUserId(id);
    }

    public List<UserRoleGroup> getUserRoleGroupByUserIdRoleId(Integer userId, Integer roleId) {
        return userRoleGroupDao.getUserRoleGroupByUserIdRoleId(userId, roleId);
    }

    public List<User> getTechnicianByGroupId(Integer id) {
        return userRoleGroupDao.getTechnicianByGroupId(id);
    }

    public UserRoleGroup getUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId) {
	return this.userRoleGroupDao.getUserRoleGroupByUserIdAndGroupId(userId, groupId);
    }

    public List<UserRoleGroup> getAllTicketPool(String queryParam, String translatedPoolName) {
        return this.userRoleGroupDao.getAllTicketPools(queryParam, translatedPoolName);
    }

    public UserRoleGroup getTicketPoolByGroupId(Integer groupId) {
	return this.userRoleGroupDao.getTicketPoolByGroupId(groupId);
    }

    public UserRoleGroup getAnyUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId) {
        return this.userRoleGroupDao.getAnyUserRoleGroupByUserIdAndGroupId(userId, groupId);
    }

    public Integer getTechnicianCount() {
        return this.userRoleGroupDao.getTechnicianCount();
    }

    public Integer getWFParticipantCount() {
        return this.userRoleGroupDao.getWFParticpantCount();
    }

    public List<User> getTechnicianAsUsers(String queryParam) {
        return this.userRoleGroupDao.getTechnicianAsUsers(queryParam);
    }

    public void flush() {
        this.userRoleGroupDao.flush();
    }

    public UserRoleGroup getUserRoleGroupByUserIdGroupIdRoleId(Integer userId, Integer groupId, Integer roleId) {
        return this.userRoleGroupDao.getUserRoleGroupByUserIdGroupIdRoleId(userId, groupId, roleId);
    }

    public List<UserRoleGroup> getAssignableUserRoleGroups(String queryParam, Integer start, Integer count) {
        return userRoleGroupDao.getAssignableUserRoleGroups(queryParam, start, count);
    }

    public int getAssignableUserRoleGroupCount(String queryParam) {
        return userRoleGroupDao.getAssignableUserRoleGroupCount(queryParam);
    }

    public List<UserRoleGroup> getAllUserRoleGroupByUserId(Integer userId) {
        return userRoleGroupDao.getAllUserRoleGroupByUserId(userId);
    }

    public void cleanupUserRoleGroup() {
        userRoleGroupDao.cleanupUserRoleGroup();
    }

    public boolean delete(UserRoleGroup userRoleGroup) {
        try{
            userRoleGroupDao.deleteUserRoleGroup(userRoleGroup);
            userRoleGroupDao.flush();
        } catch(DataIntegrityViolationException e){
            log.info("Unable to delete userrolegroup, setting as inactive");
            userRoleGroup.setActive(false);
            userRoleGroupDao.saveUserRoleGroup(userRoleGroup);
            return false;
        }
        return true;
    }

    public void clearNotificationsForUser(User user) {
        List<UserRoleGroup> urgs = getAllUserRoleGroup();
        for (UserRoleGroup urg : urgs) {
            Set<User> noteUsers = urg.getNotificationUsers();
            for (Iterator<User> userIterator = noteUsers.iterator(); userIterator.hasNext(); ) {
                User noteUser = userIterator.next();
                if (noteUser.getId().equals(user.getId())) {
                    userIterator.remove();
                    saveUserRoleGroup(urg);
                }
            }
        }
        flush();
    }

    public List<UserRoleGroup> getAssignableUserRoleGroupByGroupId(Integer groupId) {
        return userRoleGroupDao.getAssignableUserRoleGroupByGroupId(groupId);
    }

    public List<UserRoleGroup> getAllAssignableUserRoleGroups() {
        return userRoleGroupDao.getAllAssignableUserRoleGroups();
    }

    public List<UserRoleGroup> getUserRoleGroupSelectionByGroupId(Integer id) {
	    return this.userRoleGroupDao.getUserRoleGroupSelectionByGroupId(id);
    }
}
