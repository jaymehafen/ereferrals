package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TicketScheduleStatusDao;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketScheduleStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketScheduleStatusServiceImpl implements TicketScheduleStatusService {
    private TicketScheduleStatusDao ticketScheduleStatusDao;

    public TicketScheduleStatus getTSSById(Integer id) {
        return ticketScheduleStatusDao.getTSSById(id);
    }

    public TicketScheduleStatus getTSSByTicket(Ticket ticket, ScheduleStatus scheduleStatus) {
        return ticketScheduleStatusDao.getTSSByTicket(ticket, scheduleStatus);
    }

    public void saveTSS(TicketScheduleStatus ticketScheduleStatus) {
        ticketScheduleStatusDao.saveTSS(ticketScheduleStatus);
    }

    public void deleteTSS(TicketScheduleStatus ticketScheduleStatus) {
        ticketScheduleStatusDao.deleteTSS(ticketScheduleStatus);
    }

    public void cleanHouse(List ticketIdsToStay, ScheduleStatus scheduleStatus) {
        List<TicketScheduleStatus> allTss = ticketScheduleStatusDao.getTicketScheduleStatus();
        List<TicketScheduleStatus> tss2del = new ArrayList<TicketScheduleStatus>();
        for (TicketScheduleStatus allTs : allTss) {
            if (allTs.getScheduleStatus().equals(scheduleStatus) &&
                    !ticketIdsToStay.contains(allTs.getTicket().getId())) {
                tss2del.add(allTs);
            }
        }
        for (TicketScheduleStatus aTss2del : tss2del) {
            deleteTSS(aTss2del);
        }
    }

    public void flush() {
        ticketScheduleStatusDao.flush();
    }

    public void setTicketScheduleStatusDao(TicketScheduleStatusDao ticketScheduleStatusDao) {
        this.ticketScheduleStatusDao = ticketScheduleStatusDao;
    }
}
