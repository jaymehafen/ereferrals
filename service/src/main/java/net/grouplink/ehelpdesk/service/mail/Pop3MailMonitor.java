package net.grouplink.ehelpdesk.service.mail;

import java.util.Properties;

/**
 * User: jaymehafen
 * Date: Jan 23, 2008
 * Time: 3:08:04 PM
 */
public class Pop3MailMonitor extends AbstractMailMonitor {
    protected String getStoreProtocol() {
        return "pop3";
    }

    @Override
    protected void customizeSessionProperties(Properties props) {
        super.customizeSessionProperties(props);
        props.setProperty("mail.pop3.connectiontimeout", "60000");
    }
}
