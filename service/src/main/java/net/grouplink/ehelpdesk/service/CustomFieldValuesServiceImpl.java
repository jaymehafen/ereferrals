package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.CustomFieldValuesDao;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 7, 2007
 * Time: 9:57:38 PM
 */
public class CustomFieldValuesServiceImpl implements CustomFieldValuesService {

    CustomFieldValuesDao customFieldValuesDao;

    public void saveCustomFieldValues(CustomFieldValues customFieldValues) {
        customFieldValuesDao.saveCustomFieldValues(customFieldValues);
    }

    public boolean customFieldHasValues(CustomField customField) {
        return customFieldValuesDao.customFieldHasValues(customField);
    }

    public CustomFieldValues getValuesByTicketCustomField(Integer ticketId, CustomField customField) {
        return customFieldValuesDao.getValuesByTicketCustomField(ticketId, customField);
    }

    public void setCustomFieldValuesDao(CustomFieldValuesDao customFieldValuesDao) {
        this.customFieldValuesDao = customFieldValuesDao;
    }
}
