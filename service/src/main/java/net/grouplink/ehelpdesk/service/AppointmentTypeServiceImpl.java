package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.AppointmentType;
import net.grouplink.ehelpdesk.dao.AppointmentTypeDao;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class AppointmentTypeServiceImpl implements AppointmentTypeService {

    private AppointmentTypeDao appointmentTypeDao;
    
    public List<AppointmentType> getApptTypes() {
        return appointmentTypeDao.getApptTypes();
    }

    public AppointmentType getApptTypeById(Integer id) {
        return appointmentTypeDao.getApptTypeById(id);
    }

    public AppointmentType getApptTypeByName(String name) {
        return appointmentTypeDao.getApptTypeByName(name);
    }

    public void saveApptType(AppointmentType apptType) {
        appointmentTypeDao.saveApptType(apptType);
    }

    public void deleteApptType(AppointmentType apptType) {
        appointmentTypeDao.deleteApptType(apptType);
    }


    public void setAppointmentTypeDao(AppointmentTypeDao appointmentTypeDao) {
        this.appointmentTypeDao = appointmentTypeDao;
    }
}
