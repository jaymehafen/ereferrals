package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.DatabaseHandlerDao;
import net.grouplink.ehelpdesk.dao.TicketDao;
import net.grouplink.ehelpdesk.dao.UserDao;
import net.grouplink.ehelpdesk.domain.DatabaseConfig;
import net.grouplink.ehelpdesk.domain.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Aug 8, 2007
 * Time: 4:36:32 PM
 */
public class DatabaseHandlerServiceImpl implements DatabaseHandlerService {

    private DatabaseHandlerDao databaseHandlerDao;
    private UserDao userDao;
    private TicketDao ticketDao;

    public DatabaseConfig uploadJdbcProperties(String path) throws IOException {
        Properties prop = new Properties();
        prop.load(new FileInputStream(path + File.separator + "WEB-INF" + File.separator + "classes" + File.separator + "conf/jdbc.properties"));

        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setServerName((String) prop.get("server.name"));
        dbConfig.setDatabaseName((String) prop.get("database.name"));
        dbConfig.setUsername((String) prop.get("jdbc.username"));
        dbConfig.setPassword((String) prop.get("jdbc.password"));
        dbConfig.setDbType((String) prop.get("database.type"));

        return dbConfig;
    }

    public void updateJdbcProperties(DatabaseConfig dbConfig, String path) throws IOException {

        Properties prop = new Properties();
        prop.load(new FileInputStream(path + File.separator + "WEB-INF" + File.separator + "classes" + File.separator + "conf/jdbc.properties"));

        prop.setProperty("jdbc.driverClassName", dbConfig.getDriver());
        prop.setProperty("jdbc.url", dbConfig.getUrl());
        prop.setProperty("jdbc.username", dbConfig.getUsername());
        prop.setProperty("jdbc.password", dbConfig.getPassword());
        prop.setProperty("hibernate.dialect", dbConfig.getDialect());
        prop.setProperty("server.name", dbConfig.getServerName());
        prop.setProperty("database.name", dbConfig.getDatabaseName());
        prop.setProperty("database.type", dbConfig.getDbType());

        prop.store(new FileOutputStream(path + File.separator + "WEB-INF" + File.separator + "classes" + File.separator + "conf/jdbc.properties"), "Database Config");
    }

    public void init(){
        databaseInit();
//        ticketCorrection();
    }

    public void ticketCorrection(){
        int result = ticketDao.cleanUpDummyTickets();
        if(result > 0){
            databaseHandlerDao.ticketCorrection(++result);
        }
    }

    public void databaseInit() {
        User admin = userDao.getUserById(User.ADMIN_ID);
        if(admin == null){
            databaseHandlerDao.databaseInit();
        }
    }

    public void setDatabaseHandlerDao(DatabaseHandlerDao databaseHandlerDao) {
        this.databaseHandlerDao = databaseHandlerDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setTicketDao(TicketDao ticketDao) {
        this.ticketDao = ticketDao;
    }
}
