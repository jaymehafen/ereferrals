package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetFilterDao;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 5, 2008
 * Time: 6:50:06 AM
 */
public class AssetFilterServiceImpl implements AssetFilterService {

    private AssetFilterDao assetFilterDao;
    private UserService userService;

    /**
     * Returns a list of all the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}s in the system.
     *
     * @return a {@link java.util.List} of all the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}s in the system.
     */
    public List<AssetFilter> getAll() {
        return assetFilterDao.getAll();
    }

    /**
     * Returns a list of all the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}s in the system that are
     * marked public.
     *
     * @return a {@link java.util.List} of all the public {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}s.
     */
    public List<AssetFilter> getAllPublic() {
        return assetFilterDao.getAllPublic();
    }

    /**
     * Returns a list of all the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}s in the system that are
     * marked public but don't belong to <code>user</code>.
     *
     * @param user the {@link net.grouplink.ehelpdesk.domain.User} whose filters will be excluded
     * @return a {@link java.util.List} of all the public {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}s.
     */
    public List<AssetFilter> getPublicMinusUser(User user) {
        List<AssetFilter> allPubs = getAllPublic();
        List<AssetFilter> retList = new ArrayList<AssetFilter>();
        for (Object allPub : allPubs) {
            AssetFilter assetFilter = (AssetFilter) allPub;
            if (!assetFilter.getUser().equals(user)) retList.add(assetFilter);
        }
        return retList;
    }

    /**
     * Gets the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} that were created by <code>user</code>
     *
     * @param user the {@link net.grouplink.ehelpdesk.domain.User} that owns the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}
     * @return a {@link java.util.List} of all {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter}s  owned by <code>user</code>
     */
    public List<AssetFilter> getByUser(User user) {
        return assetFilterDao.getByUser(user);
    }

    /**
     * Gets a {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} by id
     *
     * @param id the id of the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} to return
     * @return the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} with matching <code>id</code>
     */
    public AssetFilter getById(Integer id) {
        return assetFilterDao.getById(id);
    }

    /**
     * Saves a {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} to the database.
     *
     * @param assetFilter the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} to save
     */
    public void save(AssetFilter assetFilter) {
        assetFilterDao.save(assetFilter);
    }

    /**
     * Deletes a {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} from the database.
     *
     * @param assetFilter the {@link net.grouplink.ehelpdesk.domain.asset.AssetFilter} to delete
     */
    public void delete(AssetFilter assetFilter) {
        assetFilterDao.delete(assetFilter);
    }

    public Integer getAssetCount(AssetFilter filter, User user) {
        return assetFilterDao.getAssetCount(filter, user);
    }

    /**
     * Searches for Tickets matching the AssetFilter criteria.
     *
     * @param assetFilter the AssetFilter
     * @param user        the User performing the search
     * @return the Tickets matching the search criteria
     */
    public List<Asset> getAssets(AssetFilter assetFilter, User user) {
        user = userService.getUserById(user.getId());
        return assetFilterDao.getAssets(assetFilter, user);
    }

    public List<Asset> getAssets(AssetFilter filter, User user, Integer firstResult, Integer maxResults, String sort) {
        return assetFilterDao.getAssets(filter, user, firstResult, maxResults, sort);
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setAssetFilterDao(AssetFilterDao assetFilterDao) {
        this.assetFilterDao = assetFilterDao;
    }

}
