package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.AddressDao;
import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.Phone;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 5:15 PM
 */
public class AddressServiceImpl implements AddressService{
    private AddressDao addressDao;
    private Log log = LogFactory.getLog(getClass());

    public void saveAddress(Address address) {
        addressDao.saveAddress(address);
    }

    public boolean deleteAddress(Address address) {
        try{
            addressDao.deleteAddress(address);
            addressDao.flush();
        } catch (Exception e){
            log.info("Unable to delete address");
            return false;
        }
        return true;
    }

    public Address getById(int addressId) {
        return addressDao.getById(addressId);
    }


    public void setAddressDao(AddressDao addressDao) {
        this.addressDao = addressDao;
    }
}
