package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.ReportDao;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.constants.JasperReportsConstants;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ReportServiceImpl implements ReportService {
    private final Log log = LogFactory.getLog(getClass());
    private ReportDao reportDao;
    private UserRoleGroupService userRoleGroupService;
    private TicketFilterService ticketFilterService;

    public Report getById(Integer id) {
        return reportDao.getById(id);
    }

    public List<Report> getByUser(User user) {
        return reportDao.getByUser(user);
    }

    public List<Report> getPublicMinusUser(User user) {
        return reportDao.getPublicMinusUser(user);
    }

    public void delete(Report report) {
        reportDao.delete(report);
    }

    public void flush() {
        reportDao.flush();
    }

    public void save(Report report) {
        reportDao.save(report);
    }

    public int getUsageCount(Report report) {
        return reportDao.getUsageCount(report);
    }

    public List<Report> getByTicketFilter(TicketFilter ticketFilter) {
        return reportDao.getByTicketFilter(ticketFilter);
    }

    public JasperPrint generateReport(Report report, User user, String drillDownValue) {
        return generateReport(report, user, drillDownValue, 0, 0);
    }

    public JasperPrint generateReport(Report report, User user, String drillDownValue, int drillDownLevel, Integer firstDDValue) {
        TicketFilter tf = report.getTicketFilter();
        String ticketProperty = null;
        String sortProperty = null;
        switch (report.getGroupBy()) {
            case JasperReportsConstants.GROUP:
                ticketProperty = "group";
                sortProperty = TicketFilter.COLUMN_GROUP;
                if (StringUtils.isNotBlank(drillDownValue)) {
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setGroupIds(new Integer[] {firstDDValue});
                        tf.setGroupIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    else {
                        tf.setGroupIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setGroupIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                }
                break;
            case JasperReportsConstants.LOCATION:
                ticketProperty = "location";
                sortProperty = TicketFilter.COLUMN_LOCATION;
                if (StringUtils.isNotBlank(drillDownValue)) {
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setLocationIds(new Integer[] {firstDDValue});
                        tf.setLocationIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    else {
                        tf.setLocationIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setLocationIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                }
                break;
            case JasperReportsConstants.CATEGORY:
                ticketProperty = "category";
                sortProperty = TicketFilter.COLUMN_CATEGORY;
                if (StringUtils.isNotBlank(drillDownValue)) {
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setCategoryIds(new Integer[] {firstDDValue});
                        tf.setCategoryIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    else {
                        tf.setCategoryIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setCategoryIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                }
                break;
            case JasperReportsConstants.CATEGORY_OPTION:
                ticketProperty = "categoryOption";
                sortProperty = TicketFilter.COLUMN_CATOPT;
                if (StringUtils.isNotBlank(drillDownValue)) {
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setCategoryOptionIds(new Integer[] {firstDDValue});
                        tf.setCategoryOptionIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    else {
                        tf.setCategoryOptionIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setCategoryOptionIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                }
                break;
            case JasperReportsConstants.TECHNICIAN:
                ticketProperty = "assignedTo";
                sortProperty = TicketFilter.COLUMN_ASSIGNED;

                if (StringUtils.isNotBlank(drillDownValue)) {
                    // in this case the drillDownValue is a UserRoleGroup id and we need
                    // to get all URG id's for the user associated with it
                    UserRoleGroup urg;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        urg = userRoleGroupService.getUserRoleGroupById(firstDDValue);
                    } else {
                        urg = userRoleGroupService.getUserRoleGroupById(Integer.valueOf(drillDownValue));
                    }

                    // check if it's a user or a ticketpool urg
                    if (urg.getUserRole() != null && urg.getUserRole().getUser() != null) {
                        // get all urgs for user
//                        List<UserRoleGroup> urgs = userRoleGroupService.getUserRoleGroupByUserId(urg.getUserRole().getUser().getId());
                        // Creating a new service method that includes inactive users to get around a report giving erroneous results
                        List<UserRoleGroup> urgs = userRoleGroupService.getAllUserRoleGroupByUserId(urg.getUserRole().getUser().getId());
                        Integer[] urgArray = new Integer[urgs.size()];
                        for (int i = 0; i < urgs.size(); i++) {
                            UserRoleGroup userRoleGroup = urgs.get(i);
                            urgArray[i] = userRoleGroup.getId();
                        }

                        tf.setAssignedToIds(urgArray);
                    } else {
                        if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                            tf.setAssignedToIds(new Integer[] {firstDDValue});
                        } else {
                            tf.setAssignedToIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        }
                    }
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        switch(report.getGroupByBy()) {
                            case JasperReportsConstants.GROUP:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setGroupIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setGroupIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;
                            case JasperReportsConstants.LOCATION:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setLocationIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setLocationIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;
                            case JasperReportsConstants.CATEGORY:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setCategoryIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setCategoryIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;
                            case JasperReportsConstants.CATEGORY_OPTION:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setCategoryOptionIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setCategoryOptionIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;
                            case JasperReportsConstants.TECHNICIAN:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setAssignedToIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setAssignedToIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;                            
                            case JasperReportsConstants.PRIORITY:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setPriorityIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setPriorityIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;
                            case JasperReportsConstants.STATUS:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setStatusIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setStatusIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;
                            case JasperReportsConstants.ZENASSET:
                                if (StringUtils.isNotBlank(drillDownValue)) {
                                    tf.setZenAssetIds(new Integer[] {Integer.valueOf(drillDownValue)});
                                    tf.setZenAssetIdOps(new Integer[] {TicketFilterOperators.EQUALS});
                                }
                                break;
                            default:
                                break;
                        }

                    }

                    // set the operator array
                    tf.setAssignedToIdsOps(new Integer[tf.getAssignedToIds().length]);
                    for (int i = 0; i < tf.getAssignedToIds().length; i++) {
                        tf.getAssignedToIdsOps()[i] = TicketFilterOperators.EQUALS;
                    }
                }
                break;
            case JasperReportsConstants.PRIORITY:
                ticketProperty = "priority";
                sortProperty = TicketFilter.COLUMN_PRIORITY;
                if (StringUtils.isNotBlank(drillDownValue)) {
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setPriorityIds(new Integer[] {firstDDValue});
                        tf.setPriorityIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    else {
                        tf.setPriorityIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setPriorityIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                }
                break;
            case JasperReportsConstants.STATUS:
                ticketProperty = "status";
                sortProperty = TicketFilter.COLUMN_STATUS;
                if (StringUtils.isNotBlank(drillDownValue)) {
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setStatusIds(new Integer[] {firstDDValue});
                        tf.setStatusIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    else {
                        tf.setStatusIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setStatusIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                }
                break;
            case JasperReportsConstants.ZENASSET:
                ticketProperty = "zenAsset";
                sortProperty = TicketFilter.COLUMN_ZENWORKS10ASSET;
                if (StringUtils.isNotBlank(drillDownValue)) {
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setZenAssetIds(new Integer[] {firstDDValue});
                        tf.setZenAssetIdOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    else {
                        tf.setZenAssetIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setZenAssetIdOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                }
                break;
            default:
                break;
        }

        if (StringUtils.isNotBlank(drillDownValue)) {
            switch (report.getGroupByBy()) {
                case JasperReportsConstants.GROUP:
                    ticketProperty = "group";
                    sortProperty = TicketFilter.COLUMN_GROUP;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setGroupIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setGroupIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
                case JasperReportsConstants.LOCATION:
                    ticketProperty = "location";
                    sortProperty = TicketFilter.COLUMN_LOCATION;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setLocationIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setLocationIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
                case JasperReportsConstants.CATEGORY:
                    ticketProperty = "category";
                    sortProperty = TicketFilter.COLUMN_CATEGORY;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setCategoryIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setCategoryIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
                case JasperReportsConstants.CATEGORY_OPTION:
                    ticketProperty = "categoryOption";
                    sortProperty = TicketFilter.COLUMN_CATOPT;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setCategoryOptionIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setCategoryOptionIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
                case JasperReportsConstants.TECHNICIAN:
                    ticketProperty = "assignedTo";
                    sortProperty = TicketFilter.COLUMN_ASSIGNED;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setAssignedToIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setAssignedToIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
                case JasperReportsConstants.PRIORITY:
                    ticketProperty = "priority";
                    sortProperty = TicketFilter.COLUMN_PRIORITY;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setPriorityIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setPriorityIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
                case JasperReportsConstants.STATUS:
                    ticketProperty = "status";
                    sortProperty = TicketFilter.COLUMN_STATUS;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setStatusIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setStatusIdsOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
                case JasperReportsConstants.ZENASSET:
                    ticketProperty = "zenAsset";
                    sortProperty = TicketFilter.COLUMN_ZENWORKS10ASSET;
                    if(drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                        tf.setZenAssetIds(new Integer[] {Integer.valueOf(drillDownValue)});
                        tf.setZenAssetIdOps(new Integer[] {TicketFilterOperators.EQUALS});
                    }
                    break;
            }

//            if (report.getGroupBy().intValue() == report.getGroupByBy().intValue()) {
            if (drillDownLevel == JasperReportsConstants.THIRD_DRILL_DOWN_LEVEL) {
                report.setShowDetail(Boolean.TRUE);
                report.setShowLink(Boolean.FALSE);
            } else {
                report.setShowLink(Boolean.TRUE);
            }

            report.setGroupBy(report.getGroupByBy());
            report.setMainReport(Boolean.FALSE);
        }

        final List<Ticket> tickets = ticketFilterService.getTickets(tf, user, sortProperty, -1, -1);

        // The ticket filter retrieves both active and inactive categories with same name
        if(report.getGroupBy() == JasperReportsConstants.CATEGORY) {
            int i = 0;
            int length = tickets.size();
            do {
               Ticket ticket = tickets.get(i);
                if(ticket.getCategory() != null && !ticket.getCategory().isActive()){
                    tickets.remove(i);
                    length--;
                }
                else {
                    i++;
                }
            } while(i < length);
        }

        // resort tickets by report group by order
        final String ticketProp2 = ticketProperty;
        if (report.getGroupByOrder() == JasperReportsConstants.GROUP_BY_ORDER_TICKETS) {

            final Map<Object, Integer> groupTickets = new HashMap<Object, Integer>();
            // loop through tickets and count number of tickets for each group
            for (Ticket ticket : tickets) {
                try {
                    Object key = PropertyUtils.getProperty(ticket, ticketProp2);
                    if (groupTickets.containsKey(key)) {
                        Integer gc = groupTickets.get(key);
                        groupTickets.put(key, gc + 1);
                    } else {
                        groupTickets.put(key, 1);
                    }
                } catch (IllegalAccessException e) {
                    log.error("error", e);
                } catch (InvocationTargetException e) {
                    log.error("error", e);
                } catch (NoSuchMethodException e) {
                    log.error("error", e);
                }
            }

            if (report.getShowTop() != null) {
                List<Map.Entry<Object, Integer>> groupEntriesList = new ArrayList<Map.Entry<Object, Integer>>(groupTickets.entrySet());
                Collections.sort(groupEntriesList, new Comparator<Map.Entry<Object, Integer>>() {
                    public int compare(Map.Entry<Object, Integer> o1, Map.Entry<Object, Integer> o2) {
                        Integer count1 = o1.getValue();
                        Integer count2 = o2.getValue();
                        return count2.compareTo(count1);
                    }
                });

                Map<Object, Integer> linkedSortedGroups = new LinkedHashMap<Object, Integer>();
                for (Map.Entry<Object, Integer> gel : groupEntriesList) {
                    linkedSortedGroups.put(gel.getKey(), gel.getValue());
                }

                List<Ticket> removeTickets = new ArrayList<Ticket>();
                Iterator<Map.Entry<Object, Integer>> sortedGroupsIterator = linkedSortedGroups.entrySet().iterator();
                for (int i = 0; i < linkedSortedGroups.size(); i++) {
                    Map.Entry<Object, Integer> entry = sortedGroupsIterator.next();
                    if (i >= report.getShowTop()) {
                        // get all tickets where the key property matches
                        removeTickets.addAll(getTicketsWithPropertyValue(tickets, sortProperty, entry.getKey()));
                    }
                }

                tickets.removeAll(removeTickets);
            }

            if(report.getGroupBy() == JasperReportsConstants.TECHNICIAN) {
                Collections.sort(tickets, new Comparator<Ticket>() {
                    public int compare(Ticket t1, Ticket t2) {
                        Integer t1UserCount = 0;
                        Integer t2UserCount = 0;
                        for (Object next : groupTickets.keySet()) {
                            UserRoleGroup urg = null;
                            if (next != null) {
                                urg = (UserRoleGroup) next;
                            }
                            if (t1.getAssignedTo() == null) {
                                if (urg == null) {
                                    t1UserCount++;
                                }
                            } else {
                                if (urg != null && (t1.getAssignedTo().toString()).equals(urg.toString())) {
                                    t1UserCount += groupTickets.get(urg);
                                }
                            }
                            if (t2.getAssignedTo() == null) {
                                if (urg == null) {
                                    t2UserCount++;
                                }
                            } else {
                                if (urg != null && (t2.getAssignedTo().toString()).equals(urg.toString())) {
                                    t2UserCount += groupTickets.get(urg);
                                }
                            }
                        }
                        return t2UserCount.compareTo(t1UserCount);
                    }
                });

            } else {
                Collections.sort(tickets, new Comparator<Ticket>() {
                    public int compare(Ticket t1, Ticket t2) {
                        // sort the tickets so the group with the most tickets is first
                        try {
                            Object t1Key = PropertyUtils.getProperty(t1, ticketProp2);
                            Object t2Key = PropertyUtils.getProperty(t2, ticketProp2);
                            Integer t1GroupCount = groupTickets.get(t1Key);
                            Integer t2GroupCount = groupTickets.get(t2Key);
                            return t2GroupCount.compareTo(t1GroupCount);
                        } catch (IllegalAccessException e) {
                            log.error("error", e);
                        } catch (InvocationTargetException e) {
                            log.error("error", e);
                        } catch (NoSuchMethodException e) {
                            log.error("error", e);
                        }

                        return 0;
                    }
                });
            }
        }
        else {
//            if(report.getGroupBy() == JasperReportsConstants.TECHNICIAN && drillDownLevel == 1) {
            if(report.getGroupBy() == JasperReportsConstants.TECHNICIAN) {
                Collections.sort(tickets, new Comparator<Ticket>() {
                    public int compare(Ticket t1, Ticket t2) {
                        String t1Key = t1.getAssignedTo() == null ? "No Assigned To" : StringUtils.isBlank(t1.getAssignedTo().toString()) ? "No AssignedTo" : t1.getAssignedTo().toString();
                        String t2Key = t2.getAssignedTo() == null ? "No Assigned To" : StringUtils.isBlank(t2.getAssignedTo().toString()) ? "No AssignedTo" : t2.getAssignedTo().toString();
                        return t1Key.compareTo(t2Key);

                    }
                });
            }
            if(report.getGroupBy() == JasperReportsConstants.ZENASSET || report.getGroupByBy() == JasperReportsConstants.ZENASSET) {
                boolean groupByZen = report.getGroupBy() == JasperReportsConstants.ZENASSET && drillDownLevel == 0;
                boolean groupByByZen = report.getGroupByBy() == JasperReportsConstants.ZENASSET
                                       && drillDownLevel == 1 && StringUtils.isNotBlank(drillDownValue);
                if(groupByZen || groupByByZen) {
                    Collections.sort(tickets, new Comparator<Ticket>() {
                        public int compare(Ticket t1, Ticket t2) {
                            String t1Key = t1.getZenAsset() == null ? "" : t1.getZenAsset().getAssetName() == null ? "" : t1.getZenAsset().getAssetName();
                            String t2Key = t2.getZenAsset() == null ? "" : t2.getZenAsset().getAssetName() == null ? "" : t2.getZenAsset().getAssetName();
                            return t1Key.compareTo(t2Key);

                        }
                    });
                }
            }
            if(report.getGroupByBy() == JasperReportsConstants.CATEGORY_OPTION && drillDownLevel == 2) {
                Collections.sort(tickets, new Comparator<Ticket>() {
                    public int compare(Ticket t1, Ticket t2) {
                        String t1Key = t1.getCategoryOption() == null ? "" : t1.getCategoryOption().getName();
                        String t2Key = t1.getCategoryOption() == null ? "" : t2.getCategoryOption().getName();
                        return t1Key.compareTo(t2Key);

                    }
                });
            }
        }

        JRXmlDataSource xml;
        try {

            Document document = getXml(tickets, report);
            xml = new JRXmlDataSource(document, "/ROOT/TICKET");
            xml.setDatePattern("dd/MM/yyyy HH.mm.ss");
            xml.setNumberPattern("###0");
        }
        catch (JRException e) {
            log.error("error creating datasource", e);
            throw new RuntimeException(e);
        }
        catch (ParserConfigurationException e) {
            log.error("error creating datasource", e);
            throw new RuntimeException(e);
        }

        Map<Object, Object> params = new HashMap<Object, Object>();

        params.put("showWorkTime", Boolean.TRUE);
        params.put("groupBy", report.getGroupBy());
        params.put("showDetail", report.getShowDetail());
        params.put("showGraph", report.getShowGraph());
        params.put("isMainReport", report.getMainReport());
        params.put("reportId", report.getId());
        params.put("showLink", report.getShowLink());
        params.put("showPieChart", report.getShowPieChart());
        params.put("reportTitle", report.getName());
        params.put("format", report.getFormat());
        params.put("myparams", xml);

        Map<JRHtmlExporterParameter, Object> imagesMap = new HashMap<JRHtmlExporterParameter, Object>();
        imagesMap.put(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
//        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath() + "/images/report?image=");

        params.putAll(imagesMap);
//        mainReport.setContentType("text/html");
        JasperPrint jp;
        try {
            jp = JasperFillManager.fillReport(getSourceFileName(report), params, xml);
        } catch (JRException e) {
            log.error("error filling report");
            throw new RuntimeException(e);
        }

        return jp;
    }

    public JasperPrint generateReportByDate(Report report, User user, String drillDownValue, int byDate) {
        TicketFilter tf = report.getTicketFilter();

        boolean showDetailVal = report.getShowDetail();
        boolean showLinkVal = report.getShowLink();
        
        if (StringUtils.isNotBlank(drillDownValue)) {
            SimpleDateFormat df = new SimpleDateFormat("EEE, d MMM yyyy");
            Date date1;
            Date date2;
            try {
                if (byDate == Report.REPORT_BY_DATE_DAY) {
                    date1 = df.parse(drillDownValue);
                    date1 = DateUtils.truncate(date1, Calendar.DATE);
                    date2 = DateUtils.addDays(date1, 1);
                    date2 = DateUtils.addMilliseconds(date2, -1);
                    showDetailVal = true;
                    showLinkVal = false;
                } else if (byDate == Report.REPORT_BY_DATE_WEEK) {
                    date1 = df.parse(drillDownValue);
                    date1 = DateUtils.truncate(date1, Calendar.DATE);
                    date2 = DateUtils.addWeeks(date1, 1);
                    date2 = DateUtils.addMilliseconds(date2, -1);
                    byDate = Report.REPORT_BY_DATE_DAY;
                } else if (byDate == Report.REPORT_BY_DATE_MONTH) {
                    df = new SimpleDateFormat("MMM yyyy");
                    date1 = df.parse(drillDownValue);
                    date1 = DateUtils.truncate(date1, Calendar.MONTH);
                    date2 = DateUtils.addMonths(date1, 1);
                    date2 = DateUtils.addMilliseconds(date2, -1);
                    byDate = Report.REPORT_BY_DATE_WEEK;
                } else if (byDate == Report.REPORT_BY_DATE_YEAR) {
                    df = new SimpleDateFormat("yyyy");
                    date1 = df.parse(drillDownValue);
                    date1 = DateUtils.truncate(date1, Calendar.YEAR);
                    date2 = DateUtils.addYears(date1, 1);
                    date2 = DateUtils.addMilliseconds(date2, -1);
                    byDate = Report.REPORT_BY_DATE_MONTH;
                } else {
                    log.error("invalid report.byDate: " + report.getByDate());
                    throw new RuntimeException("invalid report.byDate: " + report.getByDate());
                }
            } catch (ParseException e) {
                log.error("error parsing dates", e);
                throw new RuntimeException(e);
            }

            df = new SimpleDateFormat("yyyy-MM-dd");
            tf.setCreatedDateOperator(TicketFilterOperators.DATE_RANGE);
            tf.setCreatedDate(df.format(date1));
            tf.setCreatedDate2(df.format(date2));
        }

        final List<Ticket> tickets = ticketFilterService.getTickets(tf, user, TicketFilter.COLUMN_CREATED, -1, -1);

        JRXmlDataSource xml;
        try {
            Document document = getXmlByDate(tickets, byDate);
            xml = new JRXmlDataSource(document, "/ROOT/TICKET");
            xml.setDatePattern("dd/MM/yyyy HH.mm.ss");
            xml.setNumberPattern("###0");
        }
        catch (JRException e) {
            log.error("error creating datasource", e);
            throw new RuntimeException(e);
        }
        catch (ParserConfigurationException e) {
            log.error("error creating datasource", e);
            throw new RuntimeException(e);
        }

        Map<Object, Object> params = new HashMap<Object, Object>();
        params.put("byDate", byDate);
        params.put("showDetail", showDetailVal);
        params.put("showGraph", report.getShowGraph());
        params.put("isMainReport", Boolean.FALSE);
        params.put("showLink", showLinkVal);
        params.put("showPieChart", report.getShowPieChart());
        params.put("reportTitle", report.getName());
        params.put("format", report.getFormat());
        params.put("myData", xml);
        params.put("reportId", report.getId());

        Map<JRHtmlExporterParameter, Object> imagesMap = new HashMap<JRHtmlExporterParameter, Object>();
        imagesMap.put(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
//        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath() + "/images/report?image=");

        params.putAll(imagesMap);

        JasperPrint jp;
        try {
            jp = JasperFillManager.fillReport(getSourceFileName(report), params, xml);
        } catch (JRException e) {
            log.error("error filling report");
            throw new RuntimeException(e);
        }

        return jp;
    }

    private InputStream getSourceFileName(Report report) {
        if (report.getShowReportByDate()) {
            return this.getClass().getClassLoader().getResourceAsStream("net/grouplink/ehelpdesk/service/reports/ReportByDate.jasper");
        } else {
            return this.getClass().getClassLoader().getResourceAsStream("net/grouplink/ehelpdesk/service/reports/MainReport.jasper");
        }
    }

    private List<Ticket> getTicketsWithPropertyValue(List<Ticket> tickets, String sortProperty, Object propertyValue) {
        List<Ticket> found = new ArrayList<Ticket>();
        for (Ticket ticket : tickets) {
            try {
                if (PropertyUtils.getProperty(ticket, sortProperty).equals(propertyValue)) {
                    found.add(ticket);
                }
            } catch (IllegalAccessException e) {
                log.error("error", e);
            } catch (InvocationTargetException e) {
                log.error("error", e);
            } catch (NoSuchMethodException e) {
                log.error("error", e);
            }
        }

        return found;
    }

    private static Document getXml(List<Ticket> tickets, Report report) throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document document = parser.newDocument();
        Element root = document.createElement("ROOT");
        document.appendChild(root);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH.mm.ss");
        String firstName, lastName;
        String contactFName, contactLName;
        String email = "";
        List<Element> elementList = new ArrayList<Element>(tickets.size());
        for (Ticket ticket : tickets) {
            if (ticket.getAssignedTo() != null && ticket.getAssignedTo().getUserRole() != null) {
                firstName = ticket.getAssignedTo().getUserRole().getUser().getFirstName();
                lastName = ticket.getAssignedTo().getUserRole().getUser().getLastName();
                email = ticket.getAssignedTo().getUserRole().getUser().getEmail();
            } else {
                if(ticket.getAssignedTo() == null) {
                    firstName = ticket.getGroup().getName();
                }
                else {
                    firstName = ticket.getAssignedTo().getGroup().getName();
                }
                lastName = "Ticket Pool";
            }

            User contact = ticket.getContact();
            if (contact != null) {
                contactFName = contact.getFirstName();
                contactLName = contact.getLastName();
            } else {
                contactFName = "";
                contactLName = "";
            }

            Element ticketElement = document.createElement("TICKET");
            ticketElement.setAttribute("tId", ticket.getId().toString());
            ticketElement.setAttribute("ticketId", ticket.getTicketId().toString());
            ticketElement.setAttribute("ticketCreatedDate", sdf.format(ticket.getCreatedDate()));
            ticketElement.setAttribute("ticketModifiedDate", sdf.format(ticket.getModifiedDate()));
            ticketElement.setAttribute("wT", ticket.getWorkTime().toString());
            ticketElement.setAttribute("sub", ticket.getSubject());
            ticketElement.setAttribute("assignedTo", ticket.getAssignedTo() == null ? "No Assigned To" : lastName + ", " + firstName);
            ticketElement.setAttribute("aId", ticket.getAssignedTo() == null ? "" : ticket.getAssignedTo().getId().toString());
            ticketElement.setAttribute("email", email);
            ticketElement.setAttribute("contact", contactLName + ", " + contactFName);
            ticketElement.setAttribute("cId", ticket.getContact().getId().toString());
            ticketElement.setAttribute("group", ticket.getGroup().getName());
            ticketElement.setAttribute("gId", ticket.getGroup().getId().toString());
            ticketElement.setAttribute("location", ticket.getLocation().getName());
            ticketElement.setAttribute("lId", ticket.getLocation().getId().toString());
            ticketElement.setAttribute("status", ticket.getStatus().getName());
            ticketElement.setAttribute("sId", ticket.getStatus().getId().toString());
            ticketElement.setAttribute("priority", ticket.getPriority().getName());
            ticketElement.setAttribute("pId", ticket.getPriority().getId().toString());
            ticketElement.setAttribute("category", ticket.getCategory() == null ? "No Category" : ticket.getCategory().getName());
            ticketElement.setAttribute("caId", ticket.getCategory() == null ? "" : ticket.getCategory().getId().toString());
            ticketElement.setAttribute("categoryOption", ticket.getCategoryOption() == null ? "No Category Option" : ticket.getCategoryOption().getName());
            ticketElement.setAttribute("coId", ticket.getCategoryOption() == null ? "" : ticket.getCategoryOption().getId().toString());
            ticketElement.setAttribute("zenAssetName", ticket.getZenAsset() == null ? "No Asset" : ticket.getZenAsset().getAssetName());
            ticketElement.setAttribute("zenAssetId", ticket.getZenAsset() == null ? "" : ticket.getZenAsset().getId().toString());
            elementList.add(ticketElement);
        }
        if(report.getGroupByOrder() == JasperReportsConstants.GROUP_BY_ORDER_ALPHA) {
            if(report.getGroupBy() == JasperReportsConstants.TECHNICIAN) {
                Collections.sort(elementList, new Comparator<Element>() {
                    public int compare(Element e1, Element e2) {
                        String e1Key = e1.getAttribute("assignedTo");
                        String e2Key = e2.getAttribute("assignedTo");
                        return e1Key.compareTo(e2Key);

                    }
                });
            }
        }

        for (Element anElementList : elementList) {
            root.appendChild(anElementList);
        }
        
        return document;
    }

    private static Document getXmlByDate(List<Ticket> tickets, int byDate) throws ParserConfigurationException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document document = parser.newDocument();
        Element root = document.createElement("ROOT");
        document.appendChild(root);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH.mm.ss");
        String firstName, lastName;
        String contactFName, contactLName;
        String email = "";
        SimpleDateFormat df = new SimpleDateFormat("EEE, d MMM yyyy");
        Calendar cal = Calendar.getInstance();

        for (Ticket ticket : tickets) {
            cal.clear();
            cal.setTime(ticket.getCreatedDate());
            if (byDate == 0) {
            } else if (byDate == 1) {
                cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
            } else if (byDate == 2) {
                df = new SimpleDateFormat("MMM yyyy");
            } else {
                df = new SimpleDateFormat("yyyy");
            }

            if (ticket.getAssignedTo().getUserRole() != null) {
                firstName = ticket.getAssignedTo().getUserRole().getUser().getFirstName();
                lastName = ticket.getAssignedTo().getUserRole().getUser().getLastName();
                email = ticket.getAssignedTo().getUserRole().getUser().getEmail();
            } else {
                firstName = ticket.getAssignedTo().getGroup().getName();
                lastName = "Ticket Pool";
            }

            User contact = ticket.getContact();
            if (contact != null) {
                contactFName = contact.getFirstName();
                contactLName = contact.getLastName();
            } else {
                contactFName = "";
                contactLName = "";
            }

            Element ticketElement = document.createElement("TICKET");
            ticketElement.setAttribute("tId", ticket.getId().toString());
            ticketElement.setAttribute("ticketId", ticket.getTicketId().toString());
            ticketElement.setAttribute("ticketCreatedDate", sdf.format(ticket.getCreatedDate()));
            ticketElement.setAttribute("date", df.format(cal.getTime()));
            ticketElement.setAttribute("ticketModifiedDate", sdf.format(ticket.getModifiedDate()));
            ticketElement.setAttribute("wT", ticket.getWorkTime().toString());
            ticketElement.setAttribute("sub", ticket.getSubject());
            ticketElement.setAttribute("assignedTo", lastName + ", " + firstName);
            ticketElement.setAttribute("email", email);
            ticketElement.setAttribute("contact", contactLName + ", " + contactFName);

            root.appendChild(ticketElement);
        }
        
        return document;
    }

    private Object buildAssignToKey(Ticket ticket, String ticketProperty) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object key = PropertyUtils.getProperty(ticket, ticketProperty);
        if (ticket.getAssignedTo().getUserRole() != null) {
            key = key + ". " + ticket.getAssignedTo().getUserRole().getUser().getFirstName();
        } else {
            key = key + ". " + ticket.getAssignedTo().getGroup().getName();
        }
        return key;
    }

    private String buildUserRoleGroupKey(UserRoleGroup urg, String ticketProperty) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        String key = urg.toString();
        if (urg.getUserRole() != null) {
            key = key + ". " + urg.getUserRole().getUser().getFirstName();
        } else {
            key = key + ". " + urg.getGroup().getName();
        }
        return key;
    }

    public void setReportDao(ReportDao reportDao) {
        this.reportDao = reportDao;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }
}
