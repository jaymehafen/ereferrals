package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.dao.LdapFieldDao;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Apr 13, 2007
 * Time: 4:49:29 PM
 */
public class LdapFieldServiceImpl implements LdapFieldService {

    private LdapFieldDao ldapFieldDao;

    public List<LdapField> getLdapFields() {
        return this.ldapFieldDao.getLdapFields();
    }

    public LdapField getLdapFieldByAttributeID(String fieldName) {
        return this.ldapFieldDao.getLdapFieldByAttributeID(fieldName);
    }

    public LdapField getLdapFieldById(Integer fieldId) {
        return this.ldapFieldDao.getLdapFieldById(fieldId);
    }

    public void saveLdapField(LdapField ldapField) {
        this.ldapFieldDao.saveLdapField(ldapField);
    }

    public void saveLdapFields(List ldapFieldList) {
        this.ldapFieldDao.saveLdapFields(ldapFieldList);
    }

    public void deleteLdapField(LdapField ldapField) {
        this.ldapFieldDao.deleteLdapField(ldapField);
    }

    public void deleteLdapFieldList(List list) {
        this.ldapFieldDao.deleteLdapFieldList(list);
    }

    public void setLdapFieldDao(LdapFieldDao ldapFieldDao) {
            this.ldapFieldDao = ldapFieldDao;
    }    
}
