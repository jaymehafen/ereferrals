package net.grouplink.ehelpdesk.service.mail;


import com.sun.mail.smtp.SMTPTransport;
import org.apache.commons.lang.StringUtils;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Jun 5, 2008
 * Time: 2:39:07 PM
 */



/**
 * Sends multipart emails.  Multipart emails are normally "mixed", or "alternative".  When "mixed" is used, the first part should
 * be text, and the additional parts will appear as attachments in the email.  When "alternative" is used, the first part
 * should be text, and if there are additional parts, the email client will display the part that is most appropriate.  The
 * additional parts will not be visible as attachments when using "alternative".
 *
 * @author Jayme Hafen
 */
public class MultipartMailer
{

    public static final String MULTIPART_ALTERNATIVE = "alternative";
    public static final String MULTIPART_MIXED = "mixed";

    private JavaMailSenderImpl sender = null;

    private MimeMessage msg = null;
    private MimeMessageHelper helper = null;
    private Multipart mp = null;

    private int recipientCount = 0;

    private String charset = "iso-8859-1";

    private Session session = null;


    public MultipartMailer(JavaMailSenderImpl mailSender, String multipartType) throws MessagingException
    {
        recipientCount = 0;
        sender = mailSender;
        session = sender.getSession();
        session.setDebug(false);
        msg = new MimeMessage(session);
        helper = new MimeMessageHelper(msg, true);
        if (multipartType == null)
            mp = new MimeMultipart();
        else
            mp = new MimeMultipart(multipartType);
    }

    public void setRecipientCount(int recipientCount) {
        this.recipientCount = recipientCount;
    }

    public void setMailSender(JavaMailSenderImpl mailSender) {
        this.sender = mailSender;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void setFrom(String fromAddress) throws MessagingException
    {
        if (StringUtils.isNotBlank(fromAddress))
            helper.setFrom(fromAddress);
    }

    public void setFrom(String fromAddress, String displayEmail)
        throws MessagingException, UnsupportedEncodingException
    {
        if(StringUtils.isNotBlank(fromAddress))
            helper.setFrom(fromAddress, displayEmail);
    }

    public void addToRecipient(String address)
        throws MessagingException
    {
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
        recipientCount++;
    }

    public void addCCRecipient(String address)
        throws MessagingException
    {
        msg.addRecipient(Message.RecipientType.CC, new InternetAddress(address));
        recipientCount++;
    }

    public void addBCCRecipient(String address)
        throws MessagingException
    {
        msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(address));
        recipientCount++;
    }

    //private String removeMailToTag(String s)
    //{
    //    return s.replaceAll("[mM][aA][iI][lL][tT][oO]:", "");
    //}

    public void setSubject(String subject) throws MessagingException
    {
        helper.setSubject(subject);
    }

    /**
     * Adds a text part to the message.  The text part should always be added first.
     *
     * @param text plain text message
     * @throws MessagingException an exception of type MessagingException
     */
    public void addTextPart(String text)
        throws MessagingException
    {
        MimeBodyPart mbp = new MimeBodyPart();
        mbp.setContent(text, "text/plain; charset=\"" + charset + "\"");
        mp.addBodyPart(mbp);
    }

    public void addHTMLPart(String html)
        throws MessagingException
    {
        MimeBodyPart mbp = new MimeBodyPart();
        try {
            mbp.setDataHandler(new DataHandler(new ByteArrayDataSource(html, "text/html", charset)));
        }
        catch(UnsupportedEncodingException uee) {
            throw new MessagingException("Unsupported encoding in ByteArrayDataSource.");
        }
        mp.addBodyPart(mbp);
    }

    /**
     * Adds a file part to the message.
     *
     * @param filename The name of the file
     * @throws MessagingException An exception of type MessagingException
     */
    public void addFilePart(String filename)
        throws MessagingException
    {
        MimeBodyPart mbp = new MimeBodyPart();

        FileDataSource fds = new FileDataSource(filename);
        mbp.setDataHandler(new DataHandler(fds));
        mbp.setFileName(fds.getName());

        mp.addBodyPart(mbp);
    }

    /**
     * get the charset this message is using.
     * @return String
     */
    public String getCharset()
    {
        return charset;
    }

    /**
     * Set the charset value
     * <p/>
     * ISO-8859-1
     * ISO-8859-15
     * US-ASCII
     * UTF-16
     * UTF-16BE
     * UTF-16LE
     * UTF-8
     * windows-1252
     *
     * @param charset A String with a valid java charset
     */
    public void setCharset(String charset)
    {
        this.charset = charset;
    }

    public void send()
        throws MessagingException
    {
        msg.setContent(mp);
        msg.setSentDate(new Date());
        URLName urlName = new URLName(sender.getProtocol(), sender.getHost(), sender.getPort(), null, sender.getUsername(), sender.getPassword());
        SMTPTransport transport = new SMTPTransport(session, urlName);
        transport.connect();
        transport.sendMessage(msg, msg.getAllRecipients());
    }

    public JavaMailSenderImpl getMailSender() {
        return sender;
    }

    public int getRecipientCount() {
        return recipientCount;
    }

    class ByteArrayDataSource implements DataSource
    {
        private byte[] data;	// data
        private String type;	// content-type

        /* Create a DataSource from an input stream */
//		public ByteArrayDataSource(InputStream is, String type)
//		{
//			this.type = type;
//			try
//			{
//				ByteArrayOutputStream os = new ByteArrayOutputStream();
//				int ch;
//
//				while ((ch = is.read()) != -1)
//				  // XXX - must be made more efficient by
//				  // doing buffered reads, rather than one byte reads
//					os.write(ch);
//				data = os.toByteArray();
//
//			}
//			catch (IOException ioex)
//			{
//			}
//		}

        /* Create a DataSource from a byte array */
        public ByteArrayDataSource(byte[] data, String type)
        {
            this.data = data;
            this.type = type;
        }

        /* Create a DataSource from a String */
        public ByteArrayDataSource(String data, String type, String charset) throws UnsupportedEncodingException
        {
            // Assumption that the string contains only ASCII
            // characters!  Otherwise just pass a charset into this
            // constructor and use it in getBytes()
            this.data = data.getBytes(charset);
            this.type = type + "; charset=" + charset;
        }

        /**
         * Return an InputStream for the data.
         * Note - a new stream must be returned each time.
         */
        public InputStream getInputStream() throws IOException
        {
            if (data == null)
                throw new IOException("no data");

            return new ByteArrayInputStream(data);
        }

        public OutputStream getOutputStream() throws IOException
        {
            throw new IOException("cannot do this");
        }

        public String getContentType()
        {
            return type;
        }

        public String getName()
        {
            return "dummy";
        }
    }
}