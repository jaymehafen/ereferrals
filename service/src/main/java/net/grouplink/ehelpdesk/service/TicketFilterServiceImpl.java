package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TicketFilterDao;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterCriteriaHelper;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * User: mark
 * Date: Feb 20, 2008
 * Time: 7:07:04 PM
 */
//@WebService(endpointInterface = "net.grouplink.ehelpdesk.service.TicketFilterService")
public class TicketFilterServiceImpl implements TicketFilterService {

    TicketFilterDao ticketFilterDao;
    private UserService userService;
    private SurveyQuestionService surveyQuestionService;
    private TicketService ticketService;
    private ReportService reportService;
    private WidgetService widgetService;
    private PermissionService permissionService;
    private GroupService groupService;

    public List<TicketFilter> getAll() {
        return ticketFilterDao.getAll();
    }

    public List<TicketFilter> getAllPublic() {
        return ticketFilterDao.getAllPublic();
    }

    public List<TicketFilter> getPublicMinusUser(User user) {
        List<TicketFilter> allPubs = getAllPublic();
        List<TicketFilter> retList = new ArrayList<TicketFilter>();
        for (TicketFilter allPub : allPubs) {
            if (!allPub.getUser().equals(user)) retList.add(allPub);
        }
        return retList;
    }

    public List<TicketFilter> getPublicPlusUser(User user) {
        return ticketFilterDao.getPublicPlusUser(user);
    }

    public List<TicketFilter> getByUser(User user) {
        return ticketFilterDao.getByUser(user);
    }

    public TicketFilter getById(Integer id) {
        //        if (filter.getTfVersion() == null || filter.getTfVersion() < 2) {
//            migrateTicketFilter(filter);
//            filter = ticketFilterDao.getById(id);
//        }

        return ticketFilterDao.getById(id);
    }

    public void migrateTicketFilter(TicketFilter filter) {
        if (filter.getTfVersion() == null || filter.getTfVersion() < TicketFilter.CURRENT_TF_VERSION) {

            // assignedToIds
            if (!ArrayUtils.isEmpty(filter.getAssignedToIds())) {
                filter.setAssignedToIdsOps(new Integer[filter.getAssignedToIds().length]);
                for (int i = 0; i < filter.getAssignedToIds().length; i++) {
                    filter.getAssignedToIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // categoryIds
            if (!ArrayUtils.isEmpty(filter.getCategoryIds())) {
                filter.setCategoryIdsOps(new Integer[filter.getCategoryIds().length]);
                for (int i = 0; i < filter.getCategoryIds().length; i++) {
                    filter.getCategoryIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // categoryOptionIds
            if (!ArrayUtils.isEmpty(filter.getCategoryOptionIds())) {
                filter.setCategoryOptionIdsOps(new Integer[filter.getCategoryOptionIds().length]);
                for (int i = 0; i < filter.getCategoryOptionIds().length; i++) {
                    filter.getCategoryOptionIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // contactIds
            if (!ArrayUtils.isEmpty(filter.getContactIds())) {
                filter.setContactIdsOps(new Integer[filter.getContactIds().length]);
                for (int i = 0; i < filter.getContactIds().length; i++) {
                    filter.getContactIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // groupIds
            if (!ArrayUtils.isEmpty(filter.getGroupIds())) {
                filter.setGroupIdsOps(new Integer[filter.getGroupIds().length]);
                for (int i = 0; i < filter.getGroupIds().length; i++) {
                    filter.getGroupIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // historyNote
            if (!ArrayUtils.isEmpty(filter.getHistoryNote())) {
                filter.setHistoryNoteOps(new Integer[filter.getHistoryNote().length]);
                for (int i = 0; i < filter.getHistoryNote().length; i++) {
                    filter.getHistoryNoteOps()[i] = TicketFilterOperators.CONTAINS;
                }
            }

            // historySubject
            if (!ArrayUtils.isEmpty(filter.getHistorySubject())) {
                filter.setHistorySubjectOps(new Integer[filter.getHistorySubject().length]);
                for (int i = 0; i < filter.getHistorySubject().length; i++) {
                    filter.getHistorySubjectOps()[i] = TicketFilterOperators.CONTAINS;
                }
            }

            // locationIds
            if (!ArrayUtils.isEmpty(filter.getLocationIds())) {
                filter.setLocationIdsOps(new Integer[filter.getLocationIds().length]);
                for (int i = 0; i < filter.getLocationIds().length; i++) {
                    filter.getLocationIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // note
            if (!ArrayUtils.isEmpty(filter.getNote())) {
                filter.setNoteOps(new Integer[filter.getNote().length]);
                for (int i = 0; i < filter.getNote().length; i++) {
                    filter.getNoteOps()[i] = TicketFilterOperators.CONTAINS;
                }
            }

            // priorityIds
            if (!ArrayUtils.isEmpty(filter.getPriorityIds())) {
                filter.setPriorityIdsOps(new Integer[filter.getPriorityIds().length]);
                for (int i = 0; i < filter.getPriorityIds().length; i++) {
                    filter.getPriorityIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // statusIds
            if (!ArrayUtils.isEmpty(filter.getStatusIds())) {
                filter.setStatusIdsOps(new Integer[filter.getStatusIds().length]);
                for (int i = 0; i < filter.getStatusIds().length; i++) {
                    filter.getStatusIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // subject
            if (!ArrayUtils.isEmpty(filter.getSubject())) {
                filter.setSubjectOps(new Integer[filter.getSubject().length]);
                for (int i = 0; i < filter.getSubject().length; i++) {
                    filter.getSubjectOps()[i] = TicketFilterOperators.CONTAINS;
                }
            }

            // sumittedbyIds
            if (!ArrayUtils.isEmpty(filter.getSubmittedByIds())) {
                filter.setSubmittedByIdsOps(new Integer[filter.getSubmittedByIds().length]);
                for (int i = 0; i < filter.getSubmittedByIds().length; i++) {
                    filter.getSubmittedByIdsOps()[i] = TicketFilterOperators.EQUALS;
                }
            }

            // ticketNumbers
            if (!ArrayUtils.isEmpty(filter.getTicketNumbers())) {
                filter.setTicketNumbersOps(new Integer[filter.getTicketNumbers().length]);
                for (int i = 0; i < filter.getTicketNumbers().length; i++) {
                    filter.getTicketNumbersOps()[i] = TicketFilterOperators.EQUALS;
                }
            }


            // custom fields
            if (filter.getCustomFields() == null) {
                filter.setCustomFields(new HashMap<String, List<TicketFilterCriteriaHelper>>());
            }
            
            if (!ArrayUtils.isEmpty(filter.getCustomFieldName())) {
                for (int i = 0; i < filter.getCustomFieldName().length; i++) {
                    String name = filter.getCustomFieldName()[i];
                    String value = filter.getCustomFieldValue()[i];
                    List<TicketFilterCriteriaHelper> newValues;
                    newValues = filter.getCustomFields().get(name);
                    if (newValues == null) {
                        newValues = new ArrayList<TicketFilterCriteriaHelper>();
                        filter.getCustomFields().put(name, newValues);
                    }
                    TicketFilterCriteriaHelper cfHelper = new TicketFilterCriteriaHelper();
                    cfHelper.setValue(value);
                    cfHelper.setOperatorId(TicketFilterOperators.CONTAINS);
                    newValues.add(cfHelper);
                }

                filter.setCustomFieldName(null);
                filter.setCustomFieldValue(null);
            }

            // surveys
            if (filter.getSurveys() == null) {
                filter.setSurveys(new HashMap<String, List<TicketFilterCriteriaHelper>>());
            }
            
            if (!ArrayUtils.isEmpty(filter.getSurveyQuestionId())) {
                for (int i = 0; i < filter.getSurveyQuestionId().length; i++) {
                    String questionId = filter.getSurveyQuestionId()[i];
                    String value = filter.getSurveyQuestionValue()[i];

                    SurveyQuestion question = surveyQuestionService.getSurveyQuestionById(Integer.valueOf(questionId));
                    List<TicketFilterCriteriaHelper> newValues;
                    newValues = filter.getSurveys().get(question.getSurveyQuestionText());
                    if (newValues == null) {
                        newValues = new ArrayList<TicketFilterCriteriaHelper>();
                        filter.getSurveys().put(question.getSurveyQuestionText(), newValues);
                    }
                    TicketFilterCriteriaHelper helper = new TicketFilterCriteriaHelper();
                    helper.setValue(value);
                    helper.setOperatorId(TicketFilterOperators.CONTAINS);
                    newValues.add(helper);
                }
                filter.setSurveyQuestionId(null);
                filter.setSurveyQuestionValue(null);
            }

            filter.setTfVersion(TicketFilter.CURRENT_TF_VERSION);
            save(filter);

            // reports
            if (filter.getReport()) {
                // create report
                Report report = new Report();
                report.setByDate(filter.getByDate());
                report.setFormat(filter.getFormat());
                report.setGroupBy(filter.getGroupBy());
                report.setGroupByBy(filter.getGroupByBy());
                report.setGroupByOrder(filter.getGroupByOrder());
                report.setMainReport(filter.getMainReport());
                report.setName(filter.getName());
                report.setPrivateReport(filter.getPrivateFilter());
                report.setReGroup(filter.getReGroup());
                report.setShowDetail(filter.getShowDetail());
                report.setShowGraph(filter.getShowGraph());
                report.setShowLink(filter.getShowLink());
                report.setShowPieChart(filter.getShowPieChart());
                report.setShowReportByDate(filter.getShowReportByDate());
                report.setShowTop(filter.getShowTop());
                report.setTicketFilter(filter);
                report.setUser(filter.getUser());
                reportService.save(report);

                // check for dashboard widgets using ticket filter and associate them to the report
                List<Widget> widgets = widgetService.getByTicketFilter(filter);
                for (Widget widget : widgets) {
                    widget.setReport(report);
                    widget.setTicketFilter(null);
                    widgetService.save(widget);
                }
            }

            // check for any remaining dashboards that were using a ticket filter that was not a report
            List<Widget> widgets = widgetService.getByTicketFilter(filter);
            if (widgets.size() > 0) {
                // create report from ticket filter
                Report report = new Report();
                report.setName(filter.getName());
                report.setUser(filter.getUser());
                report.setPrivateReport(filter.getPrivateFilter());
                report.setTicketFilter(filter);
                reportService.save(report);

                for (Widget widget : widgets) {
                    widget.setReport(report);
                    widget.setTicketFilter(null);
                    widgetService.save(widget);
                }
            }
        }
    }

    public void init() {
        List<TicketFilter> filters = getByVersionLessThan(TicketFilter.CURRENT_TF_VERSION);
        for (TicketFilter filter : filters) {
            migrateTicketFilter(filter);
        }
    }

    public TicketFilter getFindUserTicketFilter(int userId) {
        TicketFilter ticketFilter = new TicketFilter();
        ticketFilter.setContactIds(new Integer[]{userId});
        ticketFilter.setContactIdsOps(new Integer[]{TicketFilterOperators.EQUALS});
        ticketFilter.setColumnOrder(new String[]{TicketFilter.COLUMN_PRIORITY, TicketFilter.COLUMN_TICKETNUMBER,
                TicketFilter.COLUMN_CONTACT, TicketFilter.COLUMN_LOCATION, TicketFilter.COLUMN_SUBJECT,
                TicketFilter.COLUMN_CATOPT, TicketFilter.COLUMN_ASSIGNED, TicketFilter.COLUMN_STATUS,
                TicketFilter.COLUMN_CREATED});
        ticketFilter.setStatusIds(new Integer[]{Status.CLOSED});
        ticketFilter.setStatusIdsOps(new Integer[]{TicketFilterOperators.NOT_EQUALS});
        return ticketFilter;
    }

    public void save(TicketFilter ticketFilter) {
        if (ticketFilter.getId() == null && ticketFilter.getTfVersion() == null) {
            ticketFilter.setTfVersion(TicketFilter.CURRENT_TF_VERSION);
        }
        ticketFilterDao.save(ticketFilter);
    }

    public void delete(TicketFilter ticketFilter) {
        // check usages of ticketfilter
        int count = getUsageCount(ticketFilter);
        if (count > 0) {
            // don't delete
            throw new RuntimeException("TicketFilter id " + ticketFilter.getId() + " is in use and cannot be deleted");
        } else {
            // delete
            ticketFilterDao.delete(ticketFilter);
        }
    }

    public List<Ticket> getTickets(TicketFilter ticketFilter, User user) {
        return getTickets(ticketFilter, user, null, -1, -1);
    }

    public List<Ticket> getTickets(TicketFilter ticketFilter, User user, String sort, int start, int count) {
        if (ticketFilter == null) throw new IllegalArgumentException("ticketFilter cannot be null");
        if (user == null) throw new IllegalArgumentException("user cannot be null");

        // check if ticketfilter is db only
        boolean dbOnly = true;
        if (requiresServiceFiltering(ticketFilter, sort)) {
            dbOnly = false;
        }

        user = userService.getUserById(user.getId());

        Map<Group, Boolean> viewTicketsGroupPermissions = getViewTicketsGroupPermissions(user);

        List<Ticket> tickets;
        if (dbOnly) {
            tickets = ticketFilterDao.getTickets(ticketFilter, user, start, count, sort, viewTicketsGroupPermissions);
        } else {
            tickets = ticketFilterDao.getTickets(ticketFilter, user, viewTicketsGroupPermissions);
            applyServiceFiltering(tickets, ticketFilter);
            // apply sort if necessary
            handleSort(tickets, ticketFilter, sort);

            if (start > -1 && count > -1) {
                tickets = tickets.subList(start, Math.min(start + count, tickets.size()));
            } else if (start > -1) {
                tickets = tickets.subList(start, tickets.size());
            } else if (count > -1) {
                tickets = tickets.subList(0, Math.min(start + count, tickets.size()));
            }
        }

        return tickets;
    }

    private Map<Group, Boolean> getViewTicketsGroupPermissions(User user) {
        List<Group> allGroups = groupService.getGroups();
        Map<Group, Boolean> viewTicketsGroupPermissions = new HashMap<Group, Boolean>();
        for (Group group : allGroups) {
            boolean hasViewTicketsPerm = permissionService.hasGroupPermission(user, "group.viewAllGroupTickets", group);
            viewTicketsGroupPermissions.put(group, hasViewTicketsPerm);
        }
        return viewTicketsGroupPermissions;
    }

    public void flush() {
        ticketFilterDao.flush();
    }

    public List<TicketFilter> getByVersionLessThan(Integer version) {
        return ticketFilterDao.getByVersionLessThan(version);
    }

    private void applyServiceFiltering(List<Ticket> tickets, TicketFilter ticketFilter) {
        if (MapUtils.isNotEmpty(ticketFilter.getSurveys())) {
            for (Iterator i = tickets.iterator(); i.hasNext();) {
                Ticket ticket = (Ticket) i.next();
                if (!ticketMatchesSurveyCriteria(ticket, ticketFilter)) {
                    i.remove();
                }
            }
        }

        if (!ArrayUtils.isEmpty(ticketFilter.getNote())) {
            for (Iterator i = tickets.iterator(); i.hasNext();) {
                Ticket ticket = (Ticket) i.next();
                if (!ticketMatchesNoteCriteria(ticket, ticketFilter.getNote(), ticketFilter.getNoteOps())) {
                    i.remove();
                }
            }
        }

        if (!ArrayUtils.isEmpty(ticketFilter.getHistoryNote())) {
            for (Iterator i = tickets.iterator(); i.hasNext();) {
                Ticket ticket = (Ticket) i.next();
                if (!ticketMatchesHistoryNoteCriteria(ticket, ticketFilter.getHistoryNote(), ticketFilter.getHistoryNoteOps())) {
                    i.remove();
                }
            }
        }

        if (MapUtils.isNotEmpty(ticketFilter.getCustomFields())) {
            for (Iterator i = tickets.iterator(); i.hasNext();) {
                Ticket ticket = (Ticket) i.next();
                if (!ticketMatchesCustomFieldCriteria(ticket, ticketFilter)) {
                    i.remove();
                }
            }
        }
    }

    private boolean requiresServiceFiltering(TicketFilter ticketFilter, String sort) {
        return MapUtils.isNotEmpty(ticketFilter.getSurveys()) ||
                !ArrayUtils.isEmpty(ticketFilter.getNote()) ||
                !ArrayUtils.isEmpty(ticketFilter.getHistoryNote()) ||
                MapUtils.isNotEmpty(ticketFilter.getCustomFields()) ||
                isCustomFieldProperty(sort) ||
                isSurveyProperty(sort) ||
                isHistoryCommentSort(sort) ||
                isNoteSort(sort);
    }

    private void handleSort(List<Ticket> tickets, TicketFilter ticketFilter, String sort) {
        if (isCustomFieldProperty(sort)) {
            handleCustomFieldSort(tickets, sort);
        } else if (isSurveyProperty(sort)) {
            handleSurveySort(tickets, sort);
        } else if (isHistoryCommentSort(sort)) {
            handleHistoryCommentSort(tickets, sort);
        } else {
            // regular ticket field sort
            String sortProperty = null;
            if (StringUtils.isNotBlank(sort)) {
                sortProperty = getPropertyNameForSortProperty(sort);
            }

            if (StringUtils.isBlank(sortProperty)) {
                sortProperty = getPropertyNameForSortProperty(ticketFilter.getColumnOrder()[0]);
            }

            MutableSortDefinition sd = new MutableSortDefinition();
            sd.setProperty(sortProperty);
            sd.setAscending(!StringUtils.startsWith(sort, "-"));
            sd.setIgnoreCase(true);

            PropertyComparator.sort(tickets, sd);
        }
    }

    private void handleHistoryCommentSort(List<Ticket> tickets, String sort) {
        final boolean desc = StringUtils.startsWith(sort, "-");
        final String thProp = desc ? sort.substring(1, sort.length()) : sort;
        Collections.sort(tickets, new Comparator<Ticket>() {
            public int compare(Ticket o1, Ticket o2) {
                int result;
                // get the latest tickethistory for each ticket
                TicketHistory th1 = ticketService.getMostRecentTicketHistory(o1);
                TicketHistory th2 = ticketService.getMostRecentTicketHistory(o2);
                if (th1 == null && th2 == null) {
                    result = 0;
                } else if (th1 != null && th2 == null) {
                    result = -1;
                } else if (th1 == null) {
                    result = 1;
                } else {
                    String s1 = null;
                    String s2 = null;
                    if (thProp.equals(TicketFilter.COLUMN_HISTORYSUBJECT)) {
                        s1 = th1.getSubject();
                        s2 = th2.getSubject();
                    } else if (thProp.equals(TicketFilter.COLUMN_HISTORYNOTE)) {
                        s1 = th1.getNote();
                        s2 = th2.getNote();
                    } else {
                        // invalid sort prop
                    }

                    if (s1 == null && s2 == null) {
                        result = 0;
                    } else if (s1 != null && s2 == null) {
                        result = -1;
                    } else if (s1 == null) {
                        result = 1;
                    } else {
                        result = s1.compareToIgnoreCase(s2);
                    }
                }
                if (desc) {
                    if (result > 0) {
                        result = result - result * 2;
                    } else if (result < 0) {
                        result = Math.abs(result);
                    }
                }
                return result;

            }
        });
    }

    private void handleSurveySort(List<Ticket> tickets, String sort) {
        final boolean desc = StringUtils.startsWith(sort, "-");
        String surveyProp = desc ? sort.substring(1, sort.length()) : sort;
        final String surveyQuestionText = StringUtils.removeStart(surveyProp, "survey.");
        Collections.sort(tickets, new Comparator<Ticket>() {
            public int compare(Ticket o1, Ticket o2) {
                int result;
                // get the surveydata value that matches the sort string
                SurveyData val1 = getSuveyDataValue(o1, surveyQuestionText);
                SurveyData val2 = getSuveyDataValue(o2, surveyQuestionText);
                if (val1 == null && val2 == null) {
                    result = 0;
                } else if (val1 != null && val2 == null) {
                    result = -1;
                } else if (val1 == null) {
                    result = 1;
                } else {
                    String s1 = val1.getValue();
                    String s2 = val2.getValue();
                    if (s1 == null && s2 == null) {
                        result = 0;
                    } else if (s1 != null && s2 == null) {
                        result = -1;
                    } else if (s1 == null) {
                        result = 1;
                    } else {
                        result = s1.compareToIgnoreCase(s2);
                    }
                }
                if (desc) {
                    if (result > 0) {
                        result = result - result * 2;
                    } else if (result < 0) {
                        result = Math.abs(result);
                    }
                }
                return result;
            }

            private SurveyData getSuveyDataValue(Ticket ticket, String questionText) {
                for (SurveyData sd : ticket.getSurveyData()) {
                    if (sd.getSurveyQuestion().getSurveyQuestionText().equals(questionText)) {
                        return sd;
                    }
                }
                return null;
            }
        });
    }

    private void handleCustomFieldSort(List<Ticket> tickets, String sort) {
        final boolean desc = StringUtils.startsWith(sort, "-");
        String cfProp = desc ? sort.substring(1, sort.length()) : sort;
        final String cfName = StringUtils.removeStart(cfProp, "customField.");
        Collections.sort(tickets, new Comparator<Ticket>() {
            public int compare(Ticket o1, Ticket o2) {
                int result;
                // get the custom field value that matches the sort string
                CustomFieldValues val1 = getCustomFieldValue(o1, cfName);
                CustomFieldValues val2 = getCustomFieldValue(o2, cfName);
                if (val1 == null && val2 == null) {
                    result = 0;
                } else if (val1 != null && val2 == null) {
                    result = -1;
                } else if (val1 == null) {
                    result = 1;
                } else {
                    String s1 = val1.getFieldValue();
                    String s2 = val2.getFieldValue();
                    if (s1 == null && s2 == null) {
                        result = 0;
                    } else if (s1 != null && s2 == null) {
                        result = -1;
                    } else if (s1 == null) {
                        result = 1;
                    } else {
                        result = s1.compareToIgnoreCase(s2);
                    }
                }
                if (desc) {
                    if (result > 0) {
                        result = result - result * 2;
                    } else if (result < 0) {
                        result = Math.abs(result);
                    }
                }
                return result;
            }

            private CustomFieldValues getCustomFieldValue(Ticket ticket, String customFieldName) {
                for (CustomFieldValues cfValue : ticket.getCustomeFieldValues()) {
                    if (cfValue.getCustomField().getName().equals(customFieldName)) {
                        return cfValue;
                    }
                }
                return null;
            }
        });
    }

    private boolean isSurveyProperty(String sort) {
        return StringUtils.startsWith(sort, "survey.") || StringUtils.startsWith(sort, "-survey.");
    }

    private boolean isCustomFieldProperty(String sort) {
        return StringUtils.startsWith(sort, "customField.") || StringUtils.startsWith(sort, "-customField.");
    }

    private boolean isHistoryCommentSort(String sort) {
        return StringUtils.startsWith(sort, TicketFilter.COLUMN_HISTORYSUBJECT) || StringUtils.startsWith(sort, "-" + TicketFilter.COLUMN_HISTORYSUBJECT) ||
                StringUtils.startsWith(sort, TicketFilter.COLUMN_HISTORYNOTE) || StringUtils.startsWith(sort, "-" + TicketFilter.COLUMN_HISTORYNOTE);
    }

    private boolean isNoteSort(String sort) {
        return StringUtils.startsWith(sort, TicketFilter.COLUMN_NOTE) || StringUtils.startsWith(sort, "-" + TicketFilter.COLUMN_NOTE);
    }

    private String getPropertyNameForSortProperty(String sort) {
        String sortProp = null;
        if (StringUtils.contains(sort, TicketFilter.COLUMN_TICKETNUMBER)) {
            sortProp = "ticketId";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_PRIORITY)) {
            sortProp = "priority";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_STATUS)) {
            sortProp = "status";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_SUBJECT)) {
            sortProp = "subject";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_NOTE)) {
            sortProp = "note";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_WORKTIME)) {
            sortProp = "workTime";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CREATED)) {
            sortProp = "createdDate";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_MODIFIED)) {
            sortProp = "modifiedDate";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_ESTIMATED)) {
            sortProp = "estimatedDate";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CONTACT)) {
            sortProp = "contact";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_SUBMITTED)) {
            sortProp = "submittedBy";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_ASSIGNED)) {
            sortProp = "assignedTo";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_LOCATION)) {
            sortProp = "location";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_GROUP)) {
            sortProp = "group";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CATEGORY)) {
            sortProp = "category";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_CATOPT)) {
            sortProp = "categoryOption";
        } else if (StringUtils.contains(sort, TicketFilter.COLUMN_ZENWORKS10ASSET)) {
            sortProp = "zenAsset";
        }

        return sortProp;
    }

    public Integer getTicketCount(Integer ticketFilterId, Integer userId) {
        TicketFilter ticketFilter = getById(ticketFilterId);
        User user = userService.getUserById(userId);
        return getTicketCount(ticketFilter, user);
    }

    public Integer getTicketCount(TicketFilter ticketFilter, User user) {
        // if survey, note, historynote, or customfield values exist on the
        // ticket filter, the filter must be processed with custom logic in the
        // service method. otherwise, we can just do a hibernate projection to
        // select the count directly, which will perform much better.
        if (MapUtils.isNotEmpty(ticketFilter.getSurveys()) ||
                !ArrayUtils.isEmpty(ticketFilter.getNote()) ||
                !ArrayUtils.isEmpty(ticketFilter.getHistoryNote()) ||
                MapUtils.isNotEmpty(ticketFilter.getCustomFields())) {
            return getTickets(ticketFilter, user).size();
        } else {
            return ticketFilterDao.getTicketCount(ticketFilter, user, getViewTicketsGroupPermissions(user));
        }
    }

    public int getUsageCount(TicketFilter ticketFilter) {
        return ticketFilterDao.getUsageCount(ticketFilter);
    }

    private boolean ticketMatchesCustomFieldCriteria(Ticket ticket, TicketFilter ticketFilter) {
        boolean matchesAll = true;
        Map<String, List<TicketFilterCriteriaHelper>> customFields = ticketFilter.getCustomFields();
        for (Map.Entry<String, List<TicketFilterCriteriaHelper>> cfEntry : customFields.entrySet()) {
            String cfName = StringUtils.removeStart(cfEntry.getKey(), TicketFilter.COLUMN_CUSTOMFIELDPREFIX);
            List<TicketFilterCriteriaHelper> cfCrit = cfEntry.getValue();
            boolean matchesField = customFieldValueMatchesCriteria(ticket, cfName, cfCrit);
            if (!matchesField) {
                matchesAll = false;
                break;
            }
        }
        return matchesAll;
    }

    private boolean customFieldValueMatchesCriteria(Ticket ticket, String cfName, List<TicketFilterCriteriaHelper> cfCrit) {
        boolean matchesAny = false;
        for (TicketFilterCriteriaHelper ticketFilterCFHelper : cfCrit) {
            Integer cfOp = ticketFilterCFHelper.getOperatorId();
            String cfValue = ticketFilterCFHelper.getValue();

            // check if ticket has a custom field value matching cfName with value matching cfOp and cfValue
            matchesAny = ticketCustomFieldValueMatchesOpAndValue(ticket, cfName, cfOp, cfValue);
            if (matchesAny) break;
        }

        return matchesAny;
    }

    private boolean ticketCustomFieldValueMatchesOpAndValue(Ticket ticket, String cfName, Integer cfOp, String cfValue) {
        boolean matchesAny = false;
        for (CustomFieldValues customFieldValue : ticket.getCustomeFieldValues()) {
            if (customFieldValue.getCustomField().getName().equals(cfName)) {
                if (cfOp.equals(TicketFilterOperators.EQUALS)) {
                    matchesAny = StringUtils.equalsIgnoreCase(customFieldValue.getFieldValue(), cfValue);
                } else if (cfOp.equals(TicketFilterOperators.NOT_EQUALS)) {
                    matchesAny = !StringUtils.equalsIgnoreCase(customFieldValue.getFieldValue(), cfValue);
                } else if (cfOp.equals(TicketFilterOperators.CONTAINS)) {
                    matchesAny = StringUtils.containsIgnoreCase(customFieldValue.getFieldValue(), cfValue);
                } else if (cfOp.equals(TicketFilterOperators.ENDS_WIDTH)) {
                    matchesAny = StringUtils.endsWithIgnoreCase(customFieldValue.getFieldValue(), cfValue);
                } else if (cfOp.equals(TicketFilterOperators.NOT_CONTAINS)) {
                    matchesAny = !StringUtils.containsIgnoreCase(customFieldValue.getFieldValue(), cfValue);
                } else if (cfOp.equals(TicketFilterOperators.STARTS_WITH)) {
                    matchesAny = StringUtils.startsWithIgnoreCase(customFieldValue.getFieldValue(), cfValue);
                }

                if (matchesAny) break;
            }
        }

        return matchesAny;
    }

    private boolean ticketMatchesHistoryNoteCriteria(Ticket ticket, String[] historyNote, Integer[] historyNoteOps) {
        for (TicketHistory ticketHistory : ticket.getTicketHistory()) {
            for (int i = 0; i < historyNote.length; i++) {
                String s = historyNote[i];
                Integer op = historyNoteOps[i];     

                if (op.equals(TicketFilterOperators.EQUALS)) {
                    if (StringUtils.equalsIgnoreCase(ticketHistory.getNote(), s)) {
                        return true;
                    }
                } else if (op.equals(TicketFilterOperators.NOT_EQUALS)) {
                    if (!StringUtils.equalsIgnoreCase(ticketHistory.getNote(), s)) {
                        return true;
                    }
                } else if (op.equals(TicketFilterOperators.CONTAINS)) {
                    if (StringUtils.containsIgnoreCase(ticketHistory.getNote(), s)) {
                        return true;
                    }
                } else if (op.equals(TicketFilterOperators.NOT_CONTAINS)) {
                    if (!StringUtils.containsIgnoreCase(ticketHistory.getNote(), s)) {
                        return true;
                    }
                } else if (op.equals(TicketFilterOperators.STARTS_WITH)) {
                    if (StringUtils.startsWithIgnoreCase(ticketHistory.getNote(), s)) {
                        return true;
                    }
                } else if (op.equals(TicketFilterOperators.ENDS_WIDTH)) {
                    if (StringUtils.endsWithIgnoreCase(ticketHistory.getNote(), s)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean ticketMatchesNoteCriteria(Ticket ticket, String[] note, Integer[] noteOps) {
        for (int i = 0; i < note.length; i++) {
            String s = note[i];
            Integer op = noteOps[i];
            if (op.equals(TicketFilterOperators.EQUALS)) {
                if (StringUtils.equalsIgnoreCase(ticket.getNote(), s)) {
                    return true;
                }
            } else if (op.equals(TicketFilterOperators.NOT_EQUALS)) {
                if (!StringUtils.equalsIgnoreCase(ticket.getNote(), s)) {
                    return true;
                }
            } else if (op.equals(TicketFilterOperators.CONTAINS)) {
                if (StringUtils.containsIgnoreCase(ticket.getNote(), s)) {
                    return true;
                }
            } else if (op.equals(TicketFilterOperators.NOT_CONTAINS)) {
                if (!StringUtils.containsIgnoreCase(ticket.getNote(), s)) {
                    return true;
                }
            } else if (op.equals(TicketFilterOperators.STARTS_WITH)) {
                if (StringUtils.startsWithIgnoreCase(ticket.getNote(), s)) {
                    return true;
                }
            } else if (op.equals(TicketFilterOperators.ENDS_WIDTH)) {
                if (StringUtils.endsWithIgnoreCase(ticket.getNote(), s)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean ticketMatchesSurveyCriteria(Ticket ticket, TicketFilter ticketFilter) {
        boolean matchesAll = true;

        if (CollectionUtils.isEmpty(ticket.getSurveyData())) {
            return false;
        }

        for (Map.Entry<String, List<TicketFilterCriteriaHelper>> surveyEntry : ticketFilter.getSurveys().entrySet()) {
            String questionText = StringUtils.removeStart(surveyEntry.getKey(), TicketFilter.COLUMN_SURVEYPREFIX);
            List<TicketFilterCriteriaHelper> surveyCrit = surveyEntry.getValue();
            boolean matches = surveyValueMatchesCriteria(ticket, questionText, surveyCrit);
            if (!matches) {
                matchesAll = false;
                break;
            }

        }

        return matchesAll;
    }

    private boolean surveyValueMatchesCriteria(Ticket ticket, String questionText, List<TicketFilterCriteriaHelper> surveyCrit) {
        boolean matchesAny = false;
        for (TicketFilterCriteriaHelper ticketFilterCFHelper : surveyCrit) {
            Integer op = ticketFilterCFHelper.getOperatorId();
            String value = ticketFilterCFHelper.getValue();

            // check if ticket has a survey value matching questiontext with value matching op and value
            for (SurveyData surveyData : ticket.getSurveyData()) {
                if (surveyData.getSurveyQuestion().getSurveyQuestionText().equals(questionText)) {
                    if (op.equals(TicketFilterOperators.EQUALS)) {
                        matchesAny = StringUtils.equalsIgnoreCase(surveyData.getValue(), value);
                    } else if (op.equals(TicketFilterOperators.NOT_EQUALS)) {
                        matchesAny = !StringUtils.equalsIgnoreCase(surveyData.getValue(), value);
                    } else if (op.equals(TicketFilterOperators.CONTAINS)) {
                        matchesAny = StringUtils.containsIgnoreCase(surveyData.getValue(), value);
                    } else if (op.equals(TicketFilterOperators.ENDS_WIDTH)) {
                        matchesAny = StringUtils.endsWithIgnoreCase(surveyData.getValue(), value);
                    } else if (op.equals(TicketFilterOperators.NOT_CONTAINS)) {
                        matchesAny = !StringUtils.containsIgnoreCase(surveyData.getValue(), value);
                    } else if (op.equals(TicketFilterOperators.STARTS_WITH)) {
                        matchesAny = StringUtils.startsWithIgnoreCase(surveyData.getValue(), value);
                    }

                    if (matchesAny) break;
                }
            }
        }
        return matchesAny;
    }

    public void setTicketFilterDao(TicketFilterDao ticketFilterDao) {
        this.ticketFilterDao = ticketFilterDao;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setWidgetService(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }
}
