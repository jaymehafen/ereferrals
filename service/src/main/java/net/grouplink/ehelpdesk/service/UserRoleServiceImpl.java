package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.UserRoleDao;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.User;

public class UserRoleServiceImpl implements UserRoleService {
	
	private UserRoleDao userRoleDao = null;
	
	public UserRole getUserRoleById(Integer id) {
		return userRoleDao.getUserRoleById(id);
	}
	
	public void deleteUserRole(UserRole userRole) {
		userRoleDao.deleteUserRole(userRole);
	}

    public UserRole getUserRoleByUserIdAndRoleId(Integer userId, Integer roleId) {
        return userRoleDao.getUserRoleByUserIdAndRoleId(userId, roleId);
    }

    public User getUserByUserNameAndRoleId(String userName, Integer roleId) {
        return userRoleDao.getUserByUserNameAndRoleId(userName, roleId);
    }

    public User getUserByEmailAndRoleId(String email, Integer roleId) {
        return userRoleDao.getUserByEmailAndRoleId(email, roleId);
    }

    public void flush() {
        this.userRoleDao.flush();
    }

    public void setUserRoleDao(UserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}
}
