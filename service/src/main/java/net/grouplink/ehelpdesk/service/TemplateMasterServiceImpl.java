package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.TemplateMasterDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.HashSet;

public class TemplateMasterServiceImpl implements TemplateMasterService {
    private final Log log = LogFactory.getLog(TemplateMasterServiceImpl.class);
    private TemplateMasterDao templateMasterDao;
    private TicketTemplateService ticketTemplateService;
    private WorkflowStepStatusService workflowStepStatusService;
    private TicketService ticketService;
    private TicketTemplateLaunchService ticketTemplateLaunchService;
    private UserService userService;

    public List<TicketTemplateMaster> getAll() {
        return templateMasterDao.getAll();
    }

    public List<TicketTemplateMaster> getAllPublic() {
        return templateMasterDao.getAllPublic();
    }

    public void flush() {
        templateMasterDao.flush();
    }

    public TicketTemplateMaster getById(Integer id) {
        return templateMasterDao.getById(id);
    }

    public void save(TicketTemplateMaster master) {
        templateMasterDao.save(master);
    }

    public void delete(TicketTemplateMaster master) {
        // check if template master has been launched
        if (hasBeenLaunched(master)) {
            // if launched, set active to false and save
            master.setActive(false);
            save(master);
        } else {
            // if not launched just delete
            templateMasterDao.delete(master);
        }
    }

    private boolean hasBeenLaunched(TicketTemplateMaster ttm) {
        return (ticketTemplateLaunchService.getLaunchCountByTemplateMaster(ttm) > 0);
    }

    public TicketTemplateLaunch launchTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster, User user) {
        ticketTemplateMaster = templateMasterDao.getById(ticketTemplateMaster.getId());
        user = userService.getUserById(user.getId());
        TicketTemplateLaunch ttl = new TicketTemplateLaunch();
        ttl.setCreatedBy(user);
        ttl.setCreatedDate(new Date());
        ttl.setTicketTemplateMaster(ticketTemplateMaster);
        ttl.setStatus(TicketTemplateLaunch.STATUS_PENDING);
        ticketTemplateLaunchService.save(ttl);

        List<Ticket> tickets;
        if (ticketTemplateMaster.getEnableWorkflow()) {
            tickets = launchWorkflow(ticketTemplateMaster, ttl);
        } else {
            tickets = launchTickets(ticketTemplateMaster);
        }

        // set additional properties on the tickets
        for (Ticket ticket : tickets) {
            setTTLaunch(ticket, ttl);
            initContact(ticket, user);
            initLocation(ticket);
            initSubmittedBy(ticket, user);
        }

        // save ttlaunch
        ticketTemplateLaunchService.save(ttl);

        // save tickets
        ticketService.saveTickets(tickets, user);
        
        return ttl;
    }

    private void initContact(Ticket ticket, User user) {
        if (ticket.getContact() == null) {
            ticket.setContact(user);
        }

        for (Ticket st : ticket.getSubtickets()) {
            initContact(st, user);
        }
    }

    private void initLocation(Ticket ticket) {
        if (ticket.getLocation() == null && ticket.getContact().getLocation() != null) {
            ticket.setLocation(ticket.getContact().getLocation());
        }

        for (Ticket st : ticket.getSubtickets()) {
            initLocation(st);
        }
    }

    private void initSubmittedBy(Ticket ticket, User submittedBy) {
        ticket.setSubmittedBy(submittedBy);

        for (Ticket st : ticket.getSubtickets()) {
            initSubmittedBy(st, submittedBy);
        }
    }

    private void setTTLaunch(Ticket ticket, TicketTemplateLaunch ttl) {
        ttl.addTicket(ticket);
        for (Ticket st : ticket.getSubtickets()) {
            setTTLaunch(st, ttl);
        }
    }

    public TicketTemplateMaster cloneTicketTemplateMaster(TicketTemplateMaster master) {
        TicketTemplateMaster newTTM;
        try {
            newTTM = master.clone();
        } catch (CloneNotSupportedException e) {
            log.error("error", e);
            throw new RuntimeException(e);
        }

        if (newTTM.getRecurrenceSchedule() != null) {
            try {
                RecurrenceSchedule rsClone = newTTM.getRecurrenceSchedule().clone();
                newTTM.setRecurrenceSchedule(rsClone);
                rsClone.setTicketTemplateMaster(newTTM);
            } catch (CloneNotSupportedException e) {
                log.error("error", e);
                throw new RuntimeException(e);
            }
        }

        newTTM.setWorkflowSteps(new ArrayList<WorkflowStep>());
        Map<WorkflowStep, WorkflowStep> workflowStepMap = new HashMap<WorkflowStep, WorkflowStep>();
        for (WorkflowStep workflowStep : master.getWorkflowSteps()) {
            try {
                WorkflowStep newWfs = workflowStep.clone();
                newTTM.addWorkflowStep(newWfs);
                workflowStepMap.put(workflowStep,  newWfs);
            } catch (CloneNotSupportedException e) {
                log.error("error", e);
                throw new RuntimeException(e);
            }
        }

        // clone top level ticket templates
        newTTM.setTicketTemplates(new HashSet<TicketTemplate>());
        List<TicketTemplate> topLevelTickets = ticketTemplateService.getTopLevelByTicketTemplateMaster(master);
        List<TicketTemplate> topLevelClones = new ArrayList<TicketTemplate>();
        for (TicketTemplate topLevelTicket : topLevelTickets) {
            TicketTemplate tt;
            try {
                tt = topLevelTicket.clone();
            } catch (CloneNotSupportedException e) {
                log.error("error", e);
                throw new RuntimeException(e);
            }

            topLevelClones.add(tt);
        }

        for (TicketTemplate topLevelClone : topLevelClones) {
            setTicketTemplateMaster(newTTM, topLevelClone);
            if (!workflowStepMap.isEmpty()) {
                setWorkflowSteps(workflowStepMap, topLevelClone);
            }

        }

        return newTTM;
    }

    public int getCountByGroup(Group group) {
        return templateMasterDao.getCountByGroup(group);
    }

    public void clear() {
        templateMasterDao.clear();
    }

    private List<Ticket> launchWorkflow(TicketTemplateMaster ticketTemplateMaster, TicketTemplateLaunch ttLaunch) {
        Map<WorkflowStep, WorkflowStepStatus> workflowStepsMap = new HashMap<WorkflowStep, WorkflowStepStatus>();
        for (WorkflowStep workflowStep : ticketTemplateMaster.getWorkflowSteps()) {
            WorkflowStepStatus wss = new WorkflowStepStatus();
            wss.setCreatedDate(new Date());
            wss.setWorkflowStep(workflowStep);
            wss.setTicketTemplateLaunch(ttLaunch);
            workflowStepStatusService.save(wss);
            workflowStepsMap.put(workflowStep, wss);
            ttLaunch.addWorkflowStepStatus(wss);
        }

        List<TicketTemplate> topLevelTickets = ticketTemplateService.getTopLevelByTicketTemplateMaster(ticketTemplateMaster);
        List<Ticket> tickets = new ArrayList<Ticket>();
        for (TicketTemplate tt : topLevelTickets) {
            Ticket ticket = ticketTemplateService.createTicket(tt, null, workflowStepsMap);
            tickets.add(ticket);
        }
        
        for (Ticket ticket : tickets) {
            initTicketStatus(ticket);
        }

        return tickets;
    }

    private List<Ticket> launchTickets(TicketTemplateMaster ticketTemplateMaster) {
        List<Ticket> topLevelTickets = new ArrayList<Ticket>();
        List<TicketTemplate> ticketTemplates = ticketTemplateService.getTopLevelByTicketTemplateMaster(ticketTemplateMaster);
        for (TicketTemplate ticketTemplate : ticketTemplates) {
            Ticket ticket = ticketTemplateService.createTicket(ticketTemplate, null, null);
            topLevelTickets.add(ticket);
        }

        for (Ticket ticket : topLevelTickets) {
            initTicketStatus(ticket);
        }

        return topLevelTickets;
    }

    private void setWorkflowSteps(Map<WorkflowStep, WorkflowStep> workflowStepMap, TicketTemplate tt) {
        if (tt.getWorkflowStep() != null) {
            WorkflowStep newWfs = workflowStepMap.get(tt.getWorkflowStep());
            newWfs.addTicketTemplate(tt);
        }

        for (TicketTemplate ticketTemplate : tt.getSubtickets()) {
            setWorkflowSteps(workflowStepMap, ticketTemplate);
        }
    }

    private void initTicketStatus(Ticket ticket) {
        ticket.setTicketTemplateStatus(Ticket.TT_STATUS_PENDING);

        for (Ticket subTicket : ticket.getSubtickets()) {
            initTicketStatus(subTicket);
        }
    }

    private void setTicketTemplateMaster(TicketTemplateMaster ttm, TicketTemplate tt) {
        ttm.addTemplate(tt);
        for (TicketTemplate ticketTemplate : tt.getSubtickets()) {
            setTicketTemplateMaster(ttm, ticketTemplate);
        }
    }

    public void setTemplateMasterDao(TemplateMasterDao templateMasterDao) {
        this.templateMasterDao = templateMasterDao;
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setWorkflowStepStatusService(WorkflowStepStatusService workflowStepStatusService) {
        this.workflowStepStatusService = workflowStepStatusService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setTicketTemplateLaunchService(TicketTemplateLaunchService ticketTemplateLaunchService) {
        this.ticketTemplateLaunchService = ticketTemplateLaunchService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
