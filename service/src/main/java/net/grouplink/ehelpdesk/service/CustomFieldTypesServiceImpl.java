package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.CustomFieldTypesDao;
import net.grouplink.ehelpdesk.domain.CustomFieldType;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 7, 2007
 * Time: 9:56:15 PM
 */
public class CustomFieldTypesServiceImpl implements CustomFieldTypesService {

    public CustomFieldTypesDao customFieldTypesDao;

    public List<CustomFieldType> getCustomFieldTypes() {
        return customFieldTypesDao.getCustomFieldTypes();
    }

    public CustomFieldType getCustomFieldType(Integer typeId) {
        return customFieldTypesDao.getCustomFieldType(typeId);
    }

    public void setCustomFieldTypesDao(CustomFieldTypesDao customFieldTypesDao) {
        this.customFieldTypesDao = customFieldTypesDao;
    }
}
