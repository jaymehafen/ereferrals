package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.GroupSearch;
import net.grouplink.ehelpdesk.dao.GroupDao;
import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.util.List;

public class GroupServiceImpl implements GroupService {

    private Log log = LogFactory.getLog(getClass());

    private GroupDao groupDao;
    private UserRoleGroupService userRoleGroupService;
    private ScheduleStatusService scheduleStatusService;
    private TicketScheduleStatusService ticketScheduleStatusService;
    private TicketService ticketService;
    private EmailToTicketConfigService emailToTicketConfigService;
    private KnowledgeBaseService knowledgeBaseService;
    private TemplateMasterService templateMasterService;
    private TicketTemplateService ticketTemplateService;
    private AssetService assetService;
    private AssignmentService assignmentService;

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public void deleteGroup(Group group) {
        //For now we will only deactivate domain objects
        /*if(group.getTickets().size() > 0
        || group.getKnowledgebases().size() > 0
        || group.getCategories().size() > 0
        || group.getScheduleStatus().size() > 0
        || group.getUserRoleGroups().size() > 1) {*/

        group.setActive(false);
        groupDao.saveGroup(group);

        List scheduleStatuss = scheduleStatusService.getScheduleStatusByGroupId(group.getId());
        for (Object scheduleStatus : scheduleStatuss) {
            ScheduleStatus ss = (ScheduleStatus) scheduleStatus;
            ss.setRunning(false);
            scheduleStatusService.saveScheduleStatus(ss);
            ticketScheduleStatusService.cleanHouse(new ArrayList(), ss);
        }

        /*}
        else {
            UserRoleGroup urg = (UserRoleGroup) group.getUserRoleGroups().toArray()[0];
            this.groupDao.deleteGroup(urg);
            this.groupDao.deleteGroup(group);
        }*/
    }

    public Group getGroupById(Integer id) {
        return this.groupDao.getGroupById(id);
    }

    public List<Group> getGroupsByTechId(Integer userId) {
        return this.groupDao.getGroupsByTechId(userId);
    }

    public List<Group> getGroupsByManagerId(Integer userId) {
        return this.groupDao.getGroupsByManagerId(userId);
    }

    public List<Group> getGroups() {
        return this.groupDao.getGroups();
    }

    public void saveGroup(Group group) {
        boolean newGroup = group.getId()==null;
        this.groupDao.saveGroup(group);
        // Create a default UserRoleGroup entry for this groups ticket pool
        UserRoleGroup urg = new UserRoleGroup();
        if (userRoleGroupService.getTicketPoolByGroupId(group.getId()) == null) {
            urg.setGroup(group);
            userRoleGroupService.saveUserRoleGroup(urg);
        }
        // Create a default assignment if group is new
        if(newGroup){
            Assignment assignment = new Assignment();
            assignment.setGroup(group);
            assignment.setUserRoleGroup(urg);
            assignmentService.save(assignment);
        }

    }

    public List<Group> searchGroup(GroupSearch groupSearch) {
        return this.groupDao.searchGroup(groupSearch);
    }

    public boolean hasDuplicateGroupName(Group group) {
        return this.groupDao.hasDuplicateGroupName(group);
    }

    public Group getGroupByName(String groupName) {
        return this.groupDao.getGroupByName(groupName);
    }

    public void flush() {
        this.groupDao.flush();
    }

    public List<Group> getAllGroups() {
        return groupDao.getAllGroups();
    }

    public boolean deleteGroupIfUnused(Group group) {
        boolean retVal;
        if (groupCanBeDeleted(group)) {
            try {
                groupDao.deleteGroup(group);
                groupDao.flush();
                retVal = true;
            } catch (DataIntegrityViolationException e) {
                retVal = false;
            }
        } else {
            retVal = false;
        }

        if(!retVal){
            log.info("Unable to delete group: '" + group.getName() + "'. Setting as inactive");
            group.setActive(false);
            saveGroup(group);
        }

        return retVal;
    }

    public boolean groupCanBeDeleted(Group group) {
        if (ticketService.getTicketCountByGroupId(group.getId()) > 0) return false;
        if (emailToTicketConfigService.getCountByGroup(group) > 0) return false;
        if (knowledgeBaseService.getCountByGroup(group) > 0) return false;
        if (scheduleStatusService.getCountByGroup(group) > 0) return false;
        if (templateMasterService.getCountByGroup(group) > 0) return false;
        if (ticketTemplateService.getCountByGroup(group) > 0) return false;
        if (assetService.getCountByGroup(group) > 0) return false;
        // Seems like group isn't being used, so it can be deleted
        return true;
    }

    public Integer getGroupCount() {
        return this.groupDao.getGroupCount();
    }

    public List<Group> getGroupsByAssetTrackerType(String assetTrackerType) {
        return groupDao.getGroupsByAssetTrackerType(assetTrackerType);
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setScheduleStatusService(ScheduleStatusService scheduleStatusService) {
        this.scheduleStatusService = scheduleStatusService;
    }

    public void setTicketScheduleStatusService(TicketScheduleStatusService ticketScheduleStatusService) {
        this.ticketScheduleStatusService = ticketScheduleStatusService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setEmailToTicketConfigService(EmailToTicketConfigService emailToTicketConfigService) {
        this.emailToTicketConfigService = emailToTicketConfigService;
    }

    public void setKnowledgeBaseService(KnowledgeBaseService knowledgeBaseService) {
        this.knowledgeBaseService = knowledgeBaseService;
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setAssignmentService(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }
}
