package net.grouplink.ehelpdesk.service.acl;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.acl.Permission;

import java.util.List;

public interface PermissionAvailability {
    void addPermission(Permission permission, List<Role> roles);

    List<Role> getRoles(Permission permission);
}
