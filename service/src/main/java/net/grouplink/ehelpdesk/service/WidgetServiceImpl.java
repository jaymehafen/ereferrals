package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.dao.WidgetDao;

import java.util.List;

public class WidgetServiceImpl implements WidgetService {

    private WidgetDao widgetDao;

    public List<Widget> getAll() {
        return widgetDao.getAll();
    }

    public Widget getById(Integer id) {
        return widgetDao.getById(id);
    }

    public void save(Widget widget) {
        widgetDao.save(widget);
    }

    public void delete(Widget widget) {
        widgetDao.delete(widget);
    }

    public List<Widget> getByTicketFilter(TicketFilter filter) {
        return widgetDao.getByTicketFilter(filter);
    }

    public void setWidgetDao(WidgetDao widgetDao) {
        this.widgetDao = widgetDao;
    }
}
