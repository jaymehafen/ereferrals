package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.dao.LocationDao;
import net.grouplink.ehelpdesk.domain.Location;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.bytecode.buildtime.Logger;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class LocationServiceImpl implements LocationService {

    private Log log = LogFactory.getLog(getClass());
    private LocationDao locationDao;

    public List<Location> getLocations() {
        return locationDao.getLocations();
    }

    public List<Location> getAllLocations() {
        return locationDao.getAllLocations();
    }

    public List<Location> getLocations(String queryParam, int start, int count) {
        return locationDao.getLocations(queryParam, start, count);
    }

    public Location getLocationById(Integer id) {
        return locationDao.getLocationById(id);
    }

    public Location getLocationByName(String name) {
        return locationDao.getLocationByName(name);
    }

    public void saveLocation(Location location) {
        locationDao.saveLocation(location);
    }

    public boolean deleteLocation(Location location) {
        try{
            locationDao.deleteLocation(location);
            locationDao.flush();
        } catch(DataIntegrityViolationException e){
            log.info("Unable to delete location, setting as inactive");
            location.setActive(false);
            locationDao.saveLocation(location);
            return false;
        }
        return true;
    }

    public boolean hasDuplicateLocationName(Location location) {
        return this.locationDao.hasDuplicateLocationName(location);
    }

    public void flush() {
        this.locationDao.flush();
    }

    public int getLocationCount(String queryParam) {
        return locationDao.getLocationCount(queryParam);
    }

    public void setLocationDao(LocationDao locationDao) {
        this.locationDao = locationDao;
    }
}
