package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserDetailsImpl implements UserDetails {
    private User user;

    public Collection<? extends GrantedAuthority> getAuthorities() {
        // initialize user.userRoles.role collection to prevent lazyinitialization error
        faultUserRoles(user);
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>();

        for (final UserRole userRole : user.getUserRoles()) {
            if (userRole.getRole() != null) {
                authorityList.add(new GrantedAuthority() {
                    public String getAuthority() {
                        return userRole.getRole().getName();
                    }
                });
            }
        }

        return authorityList;
    }

    private void faultUserRoles(User user) {
        for (UserRole userRole : user.getUserRoles()) {
            if (userRole.getRole() != null)
                userRole.getRole().getName();
        }
    }

    public String getPassword() {
        return user == null ? null : user.getPassword();
    }

    public String getUsername() {
        return user.getLoginId();
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return user == null || user.isActive();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
