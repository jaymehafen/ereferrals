package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.AssetTrackerZen10Config;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * User: jaymehafen
 * Date: Mar 8, 2009
 * Time: 8:29:50 PM
 */
public class AssetTrackerZen10ServiceImpl implements AssetTrackerZen10Service {
    private Log log = LogFactory.getLog(getClass());
    private PropertiesService propertiesService;

    public void saveAssetTrackerZen10Config(AssetTrackerZen10Config zen10Config, String path) {
        // write to zen10config.properties
        writeZen10ConfigProperties(zen10Config, path);

        // write zen10-jdbc.properties
        writeZen10JdbcProperties(zen10Config, path);

        // save ehd properties
        saveEhdProperties(zen10Config);
    }

    public AssetTrackerZen10Config getAssetTrackerZen10Config(String path) {
        AssetTrackerZen10Config zen10Config = new AssetTrackerZen10Config();

        // load from zen10config.properties
        Properties prop = loadProps(path, "zen10config.properties");
        zen10Config.setEnableZen10(Boolean.valueOf(prop.getProperty("zen10.enabled", "false")));

        if(zen10Config.getEnableZen10()) {
            // load from zen10-jdbc.properties
            prop = loadProps(path, "zen10-jdbc.properties");
            zen10Config.getZenDatabaseConfig().setDatabaseName(prop.getProperty("database.name"));
            zen10Config.getZenDatabaseConfig().setDbType(prop.getProperty("database.type"));
            zen10Config.getZenDatabaseConfig().setPassword(prop.getProperty("jdbc.password"));
            zen10Config.getZenDatabaseConfig().setServerName(prop.getProperty("server.name"));
            zen10Config.getZenDatabaseConfig().setUsername(prop.getProperty("jdbc.username"));
            zen10Config.getZenDatabaseConfig().setDsn(prop.getProperty("database.odbcDsn"));

            // load base url
            zen10Config.setZenBaseUrl(getZen10BaseUrl());

            // ehd vnc
            zen10Config.setEnableEhdVnc(isEhdVncEnabled());
        }

        return zen10Config;
    }

    public String getZen10BaseUrl() {
        return propertiesService.getPropertiesValueByName(PropertiesConstants.ZEN10_BASEURL);
    }

    public boolean isZen10Enabled() {
        String enabled = propertiesService.getPropertiesValueByName(PropertiesConstants.ZEN10_ENABLED);
        return Boolean.valueOf(enabled);
    }

    public boolean isEhdVncEnabled() {
        String enabled = propertiesService.getPropertiesValueByName(PropertiesConstants.ZEN10_EHDVNC_ENABLED);
        return Boolean.valueOf(enabled);
    }

    public void saveEhdProperties(AssetTrackerZen10Config zen10Config) {
        propertiesService.saveProperties(PropertiesConstants.ZEN10_ENABLED, zen10Config.getEnableZen10().toString());
        if(zen10Config.getEnableZen10()) {
            propertiesService.saveProperties(PropertiesConstants.ZEN10_EHDVNC_ENABLED, zen10Config.getEnableEhdVnc().toString());
            propertiesService.saveProperties(PropertiesConstants.ZEN10_BASEURL, zen10Config.getZenBaseUrl());
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBTYPE, zen10Config.getZenDatabaseConfig().getDbType());
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBSERVER, zen10Config.getZenDatabaseConfig().getServerName());
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBNAME, zen10Config.getZenDatabaseConfig().getDatabaseName());
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBUSER, zen10Config.getZenDatabaseConfig().getUsername());
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBPASSWORD, zen10Config.getZenDatabaseConfig().getPassword());
        } else {
            propertiesService.saveProperties(PropertiesConstants.ZEN10_EHDVNC_ENABLED, "false");
            propertiesService.saveProperties(PropertiesConstants.ZEN10_BASEURL, "");
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBTYPE, "");
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBSERVER, "");
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBNAME, "");
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBUSER, "");
            propertiesService.saveProperties(PropertiesConstants.ZEN10_DBPASSWORD, "");
        }
    }

    private void writeZen10JdbcProperties(AssetTrackerZen10Config zen10Config, String path) {
        if(zen10Config.getEnableZen10()) {
            Properties prop = loadProps(path, "zen10-jdbc.properties");
            prop.setProperty("jdbc.driverClassName", zen10Config.getZenDatabaseConfig().getDriver());
            prop.setProperty("jdbc.url", zen10Config.getZenDatabaseConfig().getUrl());
            prop.setProperty("jdbc.username", zen10Config.getZenDatabaseConfig().getUsername());
            prop.setProperty("jdbc.password", zen10Config.getZenDatabaseConfig().getPassword());
            prop.setProperty("hibernate.dialect", zen10Config.getZenDatabaseConfig().getDialect());
            prop.setProperty("server.name", zen10Config.getZenDatabaseConfig().getServerName());
            prop.setProperty("database.name", zen10Config.getZenDatabaseConfig().getDatabaseName());
            prop.setProperty("database.type", zen10Config.getZenDatabaseConfig().getDbType());
            prop.setProperty("database.odbcDsn", zen10Config.getZenDatabaseConfig().getDsn());
            storeProps(path, prop, "zen10-jdbc.properties", "Zen10 Database Config");
        }
    }

    private void writeZen10ConfigProperties(AssetTrackerZen10Config zen10Config, String path) {
        Properties prop = loadProps(path, "zen10config.properties");
        prop.setProperty("zen10.enabled", zen10Config.getEnableZen10().toString());
        storeProps(path, prop, "zen10config.properties", "Zen10 Config");
    }

    private Properties loadProps(String path, String propFileName) {
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(path + File.separator + "WEB-INF" + File.separator + "classes" + File.separator +
                    "conf" + File.separator + propFileName));
        } catch (IOException e) {
            log.error("unable to load " + propFileName + ": " + e.getMessage(), e);
            throw new RuntimeException(e);
        }
        return prop;
    }

    private void storeProps(String path, Properties prop, String propFileName, String comments) {
        try {
            prop.store(new FileOutputStream(path + File.separator + "WEB-INF" + File.separator + "classes" + File.separator +
                    "conf" + File.separator + propFileName), comments);
        } catch (IOException e) {
            log.error("unable to load " + propFileName + ": " + e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }
}
