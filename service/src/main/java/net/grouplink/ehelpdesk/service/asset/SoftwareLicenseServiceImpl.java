package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.dao.asset.SoftwareLicenseDao;
import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.Collections;
import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 8, 2008
 * Time: 4:33:00 PM
 */
public class SoftwareLicenseServiceImpl implements SoftwareLicenseService {
    private SoftwareLicenseDao softwareLicenseDao;

    public SoftwareLicense getById(Integer id) {
        return softwareLicenseDao.getById(id);
    }

    public List<SoftwareLicense> getAllSoftwareLicenses() {
        List<SoftwareLicense> allSoftwareLicenses = softwareLicenseDao.getAllSoftwareLicenses();
        Collections.sort(allSoftwareLicenses);
        return allSoftwareLicenses;
    }

    public List<Vendor> getUniqueVendors() {
        return softwareLicenseDao.getUniqueVendors();
    }

    public void saveSoftwareLicense(SoftwareLicense softwareLicense) {
        softwareLicenseDao.saveSoftwareLicense(softwareLicense);
    }

    public void delete(SoftwareLicense softwareLicense) {
        softwareLicenseDao.delete(softwareLicense);
    }

    public void flush() {
        softwareLicenseDao.flush();
    }

    public void setSoftwareLicenseDao(SoftwareLicenseDao softwareLicenseDao) {
        this.softwareLicenseDao = softwareLicenseDao;
    }
}
