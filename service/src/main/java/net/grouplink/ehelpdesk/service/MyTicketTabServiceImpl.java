package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.dao.MyTicketTabDao;

import java.util.List;
import java.util.ArrayList;
import java.util.Locale;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.context.i18n.LocaleContextHolder;

public class MyTicketTabServiceImpl extends ApplicationObjectSupport implements MyTicketTabService {
    private MyTicketTabDao myTicketTabDao;
    private TicketFilterService ticketFilterService;
    private UserService userService;

    public MyTicketTab getById(Integer id) {
        return myTicketTabDao.getById(id);
    }

    public List<MyTicketTab> getByUser(User user) {
        return myTicketTabDao.getByUser(user);
    }

    public void save(MyTicketTab myTicketTab) {
        myTicketTabDao.save(myTicketTab);
    }

    public void delete(MyTicketTab myTicketTab) {
        myTicketTabDao.delete(myTicketTab);
    }

    public void saveAll(List<MyTicketTab> myTicketTabs) {
        myTicketTabDao.saveAll(myTicketTabs);
    }

    public List<MyTicketTab> initializeTabs(User user) {
        if (user.isAdmin()) {
            return new ArrayList<MyTicketTab>();
        }
        
        if (userService.isEndUser(user)) {
            return initializeTabsForUser(user);
        } else {
            return initializeTabsForTech(user);
        }
    }

    public List<MyTicketTab> getByTicketFilter(TicketFilter tf) {
        return myTicketTabDao.getByTicketFilter(tf);
    }

    private List<MyTicketTab> initializeTabsForUser(User user) {
        List<MyTicketTab> tabs = new ArrayList<MyTicketTab>();
        
        // create owned by me tab
        MyTicketTab obmTab = createOwnedByMeTab(user);
        save(obmTab);
        tabs.add(obmTab);

        // closed tickets tab
        MyTicketTab closedTab = createdClosedTicketsTab(user);
        save(closedTab);
        tabs.add(closedTab);

        return tabs;
    }

    private List<MyTicketTab> initializeTabsForTech(User user) {
        List<MyTicketTab> tabs = new ArrayList<MyTicketTab>();
        // create tab for each group the user is a tech or manager of
        user = userService.getUserById(user.getId());
        List<Group> groups = user.getGroups();
        for (Group group : groups) {
            MyTicketTab m = new MyTicketTab();
            m.setUser(user);
            m.setActive(true);
            m.setName(group.getName());
            TicketFilter tf = getTicketFilter(user, group);
            m.setTicketFilter(tf);
            save(m);
            tabs.add(m);
        }

        // create owned by me tab
        MyTicketTab obmTab = createOwnedByMeTab(user);
        save(obmTab);
        tabs.add(obmTab);

        // closed tickets tab
        MyTicketTab closedTab = createdClosedTicketsTab(user);
        save(closedTab);
        tabs.add(closedTab);
        
        return tabs;
    }

    private MyTicketTab createdClosedTicketsTab(User user) {
        MyTicketTab closedTab = new MyTicketTab();
        closedTab.setActive(true);
        closedTab.setName(getMessage("ticketList.myClosedTickets"));
        closedTab.setTicketFilter(getMyClosedTicketsFilter(user));
        closedTab.setUser(user);
        return closedTab;
    }

    private MyTicketTab createOwnedByMeTab(User user) {
        MyTicketTab obmTab = new MyTicketTab();
        obmTab.setActive(true);
        obmTab.setName(getMessage("ticketList.ownedByMe"));
        obmTab.setTicketFilter(getOwnedByMeTicketFilter(user));
        obmTab.setUser(user);
        return obmTab;
    }

    private TicketFilter getOwnedByMeTicketFilter(User user) {
        TicketFilter tf = new TicketFilter();
        tf.setUser(user);
        tf.setName(getMessage("ticketList.ownedByMe"));
        tf.setPrivateFilter(true);
        tf.setContactIds(new Integer[]{user.getId()});
        tf.setContactIdsOps(new Integer[]{TicketFilterOperators.EQUALS});
        tf.setStatusIds(new Integer[]{Status.CLOSED});
        tf.setStatusIdsOps(new Integer[]{TicketFilterOperators.NOT_EQUALS});
        tf.setColumnOrder(new String[]{
                TicketFilter.COLUMN_PRIORITY,
                TicketFilter.COLUMN_TICKETNUMBER,
                TicketFilter.COLUMN_LOCATION,
                TicketFilter.COLUMN_SUBJECT,
                TicketFilter.COLUMN_CATOPT,
                TicketFilter.COLUMN_ASSIGNED,
                TicketFilter.COLUMN_STATUS,
                TicketFilter.COLUMN_CREATED
        });

        tf.setTfVersion(TicketFilter.CURRENT_TF_VERSION);
        ticketFilterService.save(tf);
        return tf;
    }

    private TicketFilter getMyClosedTicketsFilter(User user) {
        TicketFilter tf = new TicketFilter();
        tf.setUser(user);
        tf.setName(getMessage("ticketList.myClosedTickets"));
        tf.setPrivateFilter(true);
        tf.setContactIds(new Integer[]{user.getId()});
        tf.setContactIdsOps(new Integer[]{TicketFilterOperators.EQUALS});
        tf.setStatusIds(new Integer[]{Status.CLOSED});
        tf.setStatusIdsOps(new Integer[]{TicketFilterOperators.EQUALS});
        tf.setColumnOrder(new String[]{
                TicketFilter.COLUMN_PRIORITY,
                TicketFilter.COLUMN_TICKETNUMBER,
                TicketFilter.COLUMN_LOCATION,
                TicketFilter.COLUMN_SUBJECT,
                TicketFilter.COLUMN_CATOPT,
                TicketFilter.COLUMN_ASSIGNED,
                TicketFilter.COLUMN_STATUS,
                TicketFilter.COLUMN_CREATED
        });

        tf.setTfVersion(TicketFilter.CURRENT_TF_VERSION);
        ticketFilterService.save(tf);
        return tf;
    }

    private TicketFilter getTicketFilter(User user, Group group) {
        TicketFilter tf = new TicketFilter();
        tf.setUser(user);
        tf.setGroupIds(new Integer[]{group.getId()});
        tf.setGroupIdsOps(new Integer[]{TicketFilterOperators.EQUALS});
        tf.setName(group.getName());
        tf.setPrivateFilter(true);
        tf.setStatusIds(new Integer[]{Status.CLOSED});
        tf.setStatusIdsOps(new Integer[]{TicketFilterOperators.NOT_EQUALS});
        tf.setColumnOrder(new String[]{
                TicketFilter.COLUMN_PRIORITY,
                TicketFilter.COLUMN_TICKETNUMBER,
                TicketFilter.COLUMN_CONTACT,
                TicketFilter.COLUMN_LOCATION,
                TicketFilter.COLUMN_SUBJECT,
                TicketFilter.COLUMN_CATOPT,
                TicketFilter.COLUMN_ASSIGNED,
                TicketFilter.COLUMN_STATUS,
                TicketFilter.COLUMN_CREATED
        });

        tf.setTfVersion(TicketFilter.CURRENT_TF_VERSION);
        ticketFilterService.save(tf);
        return tf;
    }

    private String getMessage(String code) {
        // get messagesourceaccessor
        MessageSourceAccessor accessor = getMessageSourceAccessor();

        // Get default locale
        Locale locale = LocaleContextHolder.getLocale();

        return accessor.getMessage(code, locale);
    }

    public void setMyTicketTabDao(MyTicketTabDao myTicketTabDao) {
        this.myTicketTabDao = myTicketTabDao;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
