package net.grouplink.ehelpdesk.domain;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Date: Aug 9, 2007
 * Time: 1:10:44 AM
 */
public class DatabaseConfig implements Serializable {

    public static final String MySQL = "MySQL";
    public static final String Oracle = "Oracle";
    public static final String Oracle8i = "Oracle8i";
    public static final String Oracle9i = "Oracle9i";
    public static final String Oracle10g = "Oracle10g";
    public static final String Oracle11g = "Oracle11g";
    public static final String PostgreSQL = "PostgreSQL";
    public static final String SQLServer = "SQLServer";
    public static final String SQLServerExpress = "SQLServerExpress";
    public static final String Sybase = "Sybase";
    public static final String SybaseODBC = "Sybase ODBC";
    public static final String HSQL = "HSQL";

    public static final String MySQLDialect = "org.hibernate.dialect.MySQL5InnoDBDialect";
    public static final String OracleDialect = "org.hibernate.dialect.OracleDialect";
    public static final String Oracle8iDialect = "org.hibernate.dialect.Oracle8iDialect";
    public static final String Oracle9iDialect = "org.hibernate.dialect.Oracle9iDialect";
    public static final String Oracle10gDialect = "org.hibernate.dialect.Oracle10gDialect";
    public static final String PostgreSQLDialect = "org.hibernate.dialect.PostgreSQLDialect";
    public static final String SQLServerDialect = "org.hibernate.dialect.SQLServerDialect";
    public static final String SybaseDialect = "org.hibernate.dialect.SybaseDialect";
    public static final String HSQLDialect = "org.hibernate.dialect.HSQLDialect";

    private Log log = LogFactory.getLog(getClass());
    
    private String dbType;
    private String serverName;
    private String databaseName;
    private String username;
    private String password;
    private String changePassword;
    private String dsn;

    public String getDbType() {
        return this.dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getServerName() {
        if (StringUtils.isNotBlank(dbType) && dbType.equals(HSQL)) {
            try {
                return new File(serverName).getCanonicalPath();
            } catch (IOException e) {
                log.error("error getting canonical path for " + serverName, e);
                throw new RuntimeException(e);
            }
        } else {
            return serverName;
        }
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getDatabaseName() {
        return this.databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getDriver() {
        if (dbType.equals(MySQL)) {
            return "org.gjt.mm.mysql.Driver";
        } else if (dbType.equals(Oracle) ||
                dbType.equals(Oracle8i) ||
                dbType.equals(Oracle9i) ||
                dbType.equals(Oracle10g) ||
                dbType.equals(Oracle11g)) {
            return "oracle.jdbc.OracleDriver";
        } else if (dbType.equals(PostgreSQL)) {
            return "org.postgresql.Driver";
        } else if (dbType.equals(SQLServer) || dbType.equals(SQLServerExpress)) {
            return "net.sourceforge.jtds.jdbc.Driver";
        } else if (dbType.equals(Sybase)) {
            return "com.sybase.jdbc4.jdbc.SybDriver";
        } else if (dbType.equals(SybaseODBC)) {
//            return "ianywhere.ml.jdbcodbc.IDriver";
            return "ianywhere.ml.jdbcodbc.jdbc3.IDriver";   
        } else {
            return "org.hsqldb.jdbcDriver";
        }
    }

    public String getUrl() {
        String url;
        if (dbType.equals(MySQL)) {
            url = "jdbc:mysql://" + serverName+ "/" + databaseName;
        } else if (dbType.equals(Oracle) ||
                dbType.equals(Oracle8i) ||
                dbType.equals(Oracle9i) ||
                dbType.equals(Oracle10g) ||
                dbType.equals(Oracle11g)) {
            url = "jdbc:oracle:thin:@" + serverName + ":" + databaseName;
        } else if (dbType.equals(PostgreSQL)) {
            url = "jdbc:postgresql://" + serverName + "/" + databaseName;
        } else if (dbType.equals(SQLServer) || dbType.equals(SQLServerExpress)) {
            url = "jdbc:jtds:sqlserver://" + serverName + "/" + databaseName;
        } else if (dbType.equals(Sybase)) {
            url = "jdbc:sybase:Tds:" + serverName + "/" + databaseName;
        } else if (dbType.equals(SybaseODBC)) {
//            url = "jdbc:joinodbc:DSN=" + dsn;
//            url = "jdbc:ianywhere:Driver=SQL Anywhere 10;DSN=" + dsn;
            url = "jdbc:ianywhere:DSN=" + dsn;
//            url = "jdbc:odbc:DSN=" + dsn;
//            url = "jdbc:odbc:Driver=SQL Anywhere 10;DSN=" + dsn;
        } else {
            url = "jdbc:hsqldb:file:" + getServerName();
        }
        
        return url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(String password) {
        this.changePassword = password;
    }

    public String getDsn() {
        return dsn;
    }

    public void setDsn(String dsn) {
        this.dsn = dsn;
    }

    public String getDialect() {
        if (dbType.equals(MySQL)) {
            return MySQLDialect;
        } else if (dbType.equals(Oracle)) {
            return OracleDialect;
        } else if (dbType.equals(Oracle8i)) {
            return Oracle8iDialect;
        } else if (dbType.equals(Oracle9i)) {
            return Oracle9iDialect;
        } else if (dbType.equals(Oracle10g) || dbType.equals(Oracle11g)) {
            return Oracle10gDialect;
        } else if(dbType.equals(PostgreSQL)){
            return PostgreSQLDialect;
        } else if(dbType.equals(Sybase) || dbType.equals(SybaseODBC)){
            return SybaseDialect;
        } else if(dbType.equals(SQLServer) || dbType.equals(SQLServerExpress)){
            return SQLServerDialect;
        } else {
            return HSQLDialect;
        }
    }

    public String canConnect() {
        Connection conn = null;
        try {
            // debugging
//            DriverManager.setLogWriter(new PrintWriter(System.out));
            
            String driverClassName = getDriver();
            log.debug("calling Class.forName for driver class: " + driverClassName);
            Class driverClass = Class.forName(driverClassName);
            log.debug("calling driverClass.newInstance()");
            Object driverClassInstance = driverClass.newInstance();
            log.debug("driverClassInstance: " + driverClassInstance);
            
            if (dbType.equals(HSQL)) {
                username = "sa";
                password = "";
            }

            String jdbcUrl = getUrl();
            log.debug("jdbc url: " + jdbcUrl);
            if (dbType.equals(SybaseODBC)) {
                log.debug("dbtype is sybaseOdbc, calling driverManager.getConnection");
                conn = DriverManager.getConnection(jdbcUrl);
            } else {
                conn = DriverManager.getConnection(jdbcUrl, username, password);
            }
            
            return "";
        } catch (SQLException e) {
            log.error("error connecting to database", e);
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            log.error("error connecting to database", e);
            return e.getMessage();
        } catch (Exception e) {
            log.error("error connecting to database", e);
            return e.getMessage();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception ignored) {
                }
            }
        }
    }
}
