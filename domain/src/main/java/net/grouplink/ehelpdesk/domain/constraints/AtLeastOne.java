package net.grouplink.ehelpdesk.domain.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * author: zpearce
 * Date: 12/9/11
 * Time: 10:36 AM
 */
@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = AtLeastOneValidator.class)
public @interface AtLeastOne {
    String properties() default "";

    Class<?>[] constraints() default {};

    Class<?>[] groups() default {};

    String message() default "{net.grouplink.ehelpdesk.domain.constraints.AtLeastOne.message}";

    Class<? extends Payload>[] payload() default {};
}
