package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Date: May 4, 2007
 * Time: 11:08:30 AM
 */
@Entity
@Table(name = "CUSTOMFIELDTYPES")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class CustomFieldType implements Serializable, Comparable{

    public static final int TEXT_FIELD = 1;
    public static final int TEXT_AREA = 2;
    public static final int RADIO = 3;
    public static final int CHECKBOX = 4;
    public static final int DROPDOWN = 5;

    private Integer id;
    private String name;
    private String htmlType;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME", length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "HTMLTYPE", length = 50)
    public String getHtmlType() {
        return htmlType;
    }

    public void setHtmlType(String htmlType) {
        this.htmlType = htmlType;
    }

    public int compareTo(Object o) {
        return this.id.compareTo(((CustomFieldType)o).getId());
    }
}
