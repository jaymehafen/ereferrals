package net.grouplink.ehelpdesk.domain;

import java.io.Serializable;

/**
 * @author dgarcia
 * @version 1.0
 */
public class AssigmentFilter implements Serializable {

    private Integer technician;
    private Integer location;
    private Integer category;
    private Integer categoryOption;

    public boolean equals(Object obj) {
        return (obj instanceof AssigmentFilter && equals((AssigmentFilter) obj));
    }

    public boolean equals(AssigmentFilter cf) {
        if (cf == this) return true;

        // Check each field for equality
        boolean result = (technician == null || technician.equals(cf.getTechnician()));
        if (result) result = (location == null || location.equals(cf.getLocation()));
        if (result) result = (category == null || category.equals(cf.getCategory()));
        if (result) result = (categoryOption == null || categoryOption.equals(cf.getCategoryOption()));

        return result;
    }

    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + (technician == null ? 0 : technician.hashCode());
        hash = 37 * hash + (location == null ? 0 : location.hashCode());
        hash = 37 * hash + (category == null ? 0 : category.hashCode());
        hash = 37 * hash + (categoryOption == null ? 0 : categoryOption.hashCode());

        return hash;
    }

    public Integer getTechnician() {
        return technician;
    }

    public void setTechnician(Integer technician) {
        this.technician = technician;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(Integer categoryOption) {
        this.categoryOption = categoryOption;
    }
}
