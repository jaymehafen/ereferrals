package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TECHCATOPTASSIGNMENT")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Assignment implements Serializable, Comparable {

    private Integer id;
    private UserRoleGroup userRoleGroup;
    private Location location;
    private CategoryOption categoryOption;
    private Group group;
    private Category category;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYOPTIONID")
    public CategoryOption getCategoryOption() {
        return categoryOption;
    }
    
    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATIONID")
    public Location getLocation() {
        return location;
    }
    
    public void setLocation(Location location) {
        this.location = location;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERROLEGROUPID")
    public UserRoleGroup getUserRoleGroup() {
        return userRoleGroup;
    }
    
    public void setUserRoleGroup(UserRoleGroup userRoleGroup) {
        this.userRoleGroup = userRoleGroup;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYID")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int compareTo(Object o) {
	
        int result = location.getName().compareTo(((Assignment) o).getLocation().getName());

        if (result != 0) {
            return result;
        }
        else if(userRoleGroup.getUserRole() == null) {
            return 1;
        }
        else if(((Assignment) o).getUserRoleGroup().getUserRole() == null) {
            return -1;
        }
        else if((result = userRoleGroup.getUserRole().getUser().getId().compareTo(((Assignment) o).getUserRoleGroup().getUserRole().getUser().getId())) != 0) {
            return result;
        }
        else if((result = categoryOption.getCategory().getName().compareTo(((Assignment) o).getCategoryOption().getCategory().getName())) != 0){
            return result;
        }
        else if((result = categoryOption.getName().compareTo(((Assignment) o).getCategoryOption().getName())) != 0){
            return result;
        }
        return result;
    }

}
