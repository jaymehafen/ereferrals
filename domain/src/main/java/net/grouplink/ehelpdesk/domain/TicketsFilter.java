package net.grouplink.ehelpdesk.domain;

import java.io.Serializable;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketsFilter implements Serializable {

    private String ticketNumber;
    private String subject;
    private String categoryOption;
    private String assignedTo;
    private String contact;
    private String status;
    private String creationDate;
    private String location;
    private String locs;
    private String priority;
    private String grouping;
    private Boolean showClosed = Boolean.FALSE;

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof TicketsFilter && equals((TicketsFilter) obj));
    }

    public boolean equals(TicketsFilter cf) {
        if (cf == this) return true;

        // Check each field for equality
        boolean result =(ticketNumber == null ? cf.ticketNumber == null : ticketNumber.equals(cf.ticketNumber));
        if (result) result = (subject == null ? cf.subject == null : subject.equals(cf.subject));
        if (result) result = (categoryOption == null ? cf.categoryOption == null : categoryOption.equals(cf.categoryOption));
        if (result) result = (assignedTo == null ? cf.assignedTo == null : assignedTo.equals(cf.assignedTo));
        if (result) result = (contact == null ? cf.contact == null : contact.equals(cf.contact));
        if (result) result = (status == null ? cf.status == null : status.equals(cf.status));
        if (result) result = (creationDate == null ? cf.creationDate == null : creationDate.equals(cf.creationDate));
        if (result) result = (location == null ? cf.location == null : location.equals(cf.location));
        if (result) result = (locs == null ? cf.locs == null : locs.equals(cf.locs));
        if (result) result = (priority == null ? cf.priority == null : priority.equals(cf.priority));
        if (result) result = (grouping == null ? cf.grouping == null : grouping.equals(cf.grouping));
        if (result) result = (showClosed == null ? cf.showClosed == null : showClosed.equals(cf.showClosed));

        return result;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + (ticketNumber == null ? 0 : ticketNumber.hashCode());
        hash = 37 * hash + (subject == null ? 0 : subject.hashCode());
        hash = 37 * hash + (categoryOption == null ? 0 : categoryOption.hashCode());
        hash = 37 * hash + (assignedTo == null ? 0 : assignedTo.hashCode());
        hash = 37 * hash + (contact == null ? 0 : contact.hashCode());
        hash = 37 * hash + (status == null ? 0 : status.hashCode());
        hash = 37 * hash + (creationDate == null ? 0 : creationDate.hashCode());
        hash = 37 * hash + (location == null ? 0 : location.hashCode());
        hash = 37 * hash + (locs == null ? 0 : locs.hashCode());
        hash = 37 * hash + (priority == null ? 0 : priority.hashCode());
        hash = 37 * hash + (grouping == null ? 0 : grouping.hashCode());
        hash = 37 * hash + (showClosed == null ? 0 : showClosed.hashCode());

        return hash;
    }


    public Boolean getShowClosed() {
        return showClosed;
    }

    public void setShowClosed(Boolean showClosed) {
        this.showClosed = showClosed;
    }

    public String getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(String categoryOption) {
        this.categoryOption = categoryOption;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getContact() {
        return contact;
    }

    public void setContact (String contact) {
        this.contact = contact;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLocs() {
        return locs;
    }

    public void setLocs(String locs) {
        this.locs = locs;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getGrouping() {
        return grouping;
    }

    public void setGrouping(String grouping) {
        this.grouping = grouping;
    }
}
