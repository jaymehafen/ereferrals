package net.grouplink.ehelpdesk.domain;

import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Feb 13, 2008
 * Time: 4:05:49 PM
 */
public class GeneralConfig implements Serializable {
    private String baseUrl;
    private Integer maxUploadSize;
    private String marqueeText;
    private Integer sessionTimeoutLength;
    private Boolean allowUserRegistration;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Integer getMaxUploadSize() {
        return maxUploadSize;
    }

    public void setMaxUploadSize(Integer maxUploadSize) {
        this.maxUploadSize = maxUploadSize;
    }

    public String getMarqueeText() {
        return marqueeText;
    }

    public void setMarqueeText(String marqueeText) {
        this.marqueeText = marqueeText;
    }

    public Integer getSessionTimeoutLength() {
        return sessionTimeoutLength;
    }

    public void setSessionTimeoutLength(Integer sessionTimeoutLength) {
        this.sessionTimeoutLength = sessionTimeoutLength;
    }

    public Boolean getAllowUserRegistration() {
        return allowUserRegistration;
    }

    public void setAllowUserRegistration(Boolean allowUserRegistration) {
        this.allowUserRegistration = allowUserRegistration;
    }
}
