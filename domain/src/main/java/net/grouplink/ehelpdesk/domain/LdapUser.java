/*
 * Copyright 2002-2005 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.grouplink.ehelpdesk.domain;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Simple class representing a single person.
 *
 * @author Mattias Arthursson
 * @author Ulrik Sandberg
 */
public class LdapUser implements Serializable, Comparable {

    private String givenName;
    private String sn;
    private String cn;
    private String mail;
    private String dn;
    private Boolean added;

    private Map<String, String> attributeMap;


    public String getURLEncodeDN() {
        try {
            return URLEncoder.encode(dn, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            System.err.println(e.getCause());
            return dn;
        }
    }

    public Boolean getAdded() {
        return added;
    }

    public void setAdded(Boolean added) {
        this.added = added;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public Map<String, String> getAttributeMap() {
        return attributeMap;
    }

    public void setAttributeMap(Map<String, String> attributeMap) {
        this.attributeMap = attributeMap;
    }

    @Override
    public String toString() {
        return "LdapUser{" +
                "givenName='" + givenName + '\'' +
                ", sn='" + sn + '\'' +
                ", cn='" + cn + '\'' +
                ", mail='" + mail + '\'' +
                ", dn='" + dn + '\'' +
                ", added=" + added +
                ", attributeMap=" + attributeMap +
                '}';
    }

    public int compareTo(Object o) {
        int result;

        if (sn == null || givenName == null) {
            return 1;
        } else if (((LdapUser) o).getSn() == null || ((LdapUser) o).getGivenName() == null) {
            return -1;
        } else if ((result = sn.compareTo(((LdapUser) o).getSn())) != 0) {
            return result;
        } else {
            return givenName.compareTo(((LdapUser) o).getGivenName());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LdapUser)) return false;

        LdapUser ldapUser = (LdapUser) o;

        if (dn != null ? !dn.equals(ldapUser.dn) : ldapUser.dn != null) return false;
        else assert true;

        return true;
    }

    @Override
    public int hashCode() {
        return dn != null ? dn.hashCode() : 0;
    }
}
