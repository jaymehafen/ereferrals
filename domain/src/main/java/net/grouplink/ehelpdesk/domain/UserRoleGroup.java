package net.grouplink.ehelpdesk.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USERROLEGROUPS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserRoleGroup implements Serializable, Comparable {

    private Integer id;
    private UserRole userRole;    
    private Group group;
    private boolean active = true;
    private UserRoleGroup backUpTech;

    private Set<Ticket> tickets = new HashSet<Ticket>();
    private Set<Assignment> assignments = new HashSet<Assignment>();
    private Set<User> notificationUsers = new HashSet<User>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BACKUPTECHID")
    public UserRoleGroup getBackUpTech() {
        return backUpTech;
    }

    public void setBackUpTech(UserRoleGroup backUpTech) {
        this.backUpTech = backUpTech;
    }

    @ManyToMany
    @JoinTable(name = "URGNOTIFCATION",
            joinColumns = @JoinColumn(name = "URGID"),
            inverseJoinColumns = @JoinColumn(name = "USERID"))
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Set<User> getNotificationUsers() {
        return notificationUsers;
    }

    public void setNotificationUsers(Set<User> notificationUsers) {
        this.notificationUsers = notificationUsers;
    }

    @OneToMany(mappedBy = "userRoleGroup")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<Assignment> getAssignments() {
        return assignments;
    }

    public void setAssignments(Set<Assignment> techLocationCategoryOptions) {
        this.assignments = techLocationCategoryOptions;
    }

    @Column(name = "ACTIVE")
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERROLEID")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public UserRole getUserRole() {
        return userRole;
    }
    
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @OneToMany(mappedBy = "assignedTo")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public int compareTo(Object o) {
        int result;

        String ufirst = "";
        String ulast = "";
        String ofirst = "";
        String olast = "";

        try{
            if(userRole != null && userRole.getUser() != null){
                ufirst = userRole.getUser().getFirstName();
                ulast = userRole.getUser().getLastName();
            }

            if(((UserRoleGroup) o).getUserRole() != null && ((UserRoleGroup) o).getUserRole().getUser() != null){
                ofirst =((UserRoleGroup) o).getUserRole().getUser().getFirstName();
                olast = ((UserRoleGroup) o).getUserRole().getUser().getLastName();
            }

            if(StringUtils.isBlank(ulast) || StringUtils.isBlank(ufirst)){
                return 1;
            }
            else if(StringUtils.isBlank(olast) || StringUtils.isBlank(ofirst)) {
                return -1;
            }
            else if ((result = ulast.compareTo(olast)) != 0) {
                    return result;
            }
            else if ((result = ufirst.compareTo(ofirst)) != 0) {
                return result;
            }
            else{
                return group.getName().compareTo(((UserRoleGroup) o).getGroup().getName());
            }
        }
        catch(Exception e){
            if(StringUtils.isBlank(ulast) || StringUtils.isBlank(ufirst)){
                return 1;
            }
            else if(StringUtils.isBlank(olast) || StringUtils.isBlank(ofirst)) {
                return -1;
            }
            else if ((result = ulast.compareTo(olast)) != 0) {
                    return result;
            }
            else if ((result = ufirst.compareTo(ofirst)) != 0) {
                return result;
            }
            else{
                return group.getName().compareTo(((UserRoleGroup) o).getGroup().getName());
            }
        }
    }

    @Override
    public String toString() {
        return "UserRoleGroup{" +
                "id=" + id +
                ", active=" + active +
                ", userRole=" + userRole +
                ", group=" + group +
                '}';
    }
}
