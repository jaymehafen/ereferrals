package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Date;

@Entity
@Table(name = "WORKFLOWSTEPSTATUS")
public class WorkflowStepStatus implements Serializable {
    private Integer id;
    private WorkflowStep workflowStep;
    private Set<Ticket> tickets = new HashSet<Ticket>();
    private Date createdDate;
    private Date completedDate;
    private TicketTemplateLaunch ticketTemplateLaunch;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "WORKFLOWSTEPID")
    public WorkflowStep getWorkflowStep() {
        return workflowStep;
    }

    public void setWorkflowStep(WorkflowStep workflowStep) {
        this.workflowStep = workflowStep;
    }

    @OneToMany(mappedBy = "workflowStepStatus")
    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(Ticket t) {
        t.setWorkflowStepStatus(this);
        tickets.add(t);
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDDATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "COMPLETEDDATE")
    public Date getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Date completedDate) {
        this.completedDate = completedDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TTLAUNCHID")
    public TicketTemplateLaunch getTicketTemplateLaunch() {
        return ticketTemplateLaunch;
    }

    public void setTicketTemplateLaunch(TicketTemplateLaunch ticketTemplateLaunch) {
        this.ticketTemplateLaunch = ticketTemplateLaunch;
    }
}
