package net.grouplink.ehelpdesk.domain;


import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Aug 12, 2008
 * Time: 2:25:16 PM
 */
public class DashBoard implements Serializable {

    private String name;
    private int total;
    private boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
