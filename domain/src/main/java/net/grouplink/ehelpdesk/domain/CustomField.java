package net.grouplink.ehelpdesk.domain;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.ParamDef;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CUSTOMFIELDS")
@FilterDef(name = "deletedObject", parameters = @ParamDef(name = "isActive", type = "boolean"))
@Filter(name = "deletedObject", condition = "ACTIVE = :isActive")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CustomField implements Serializable, Comparable {

    private Integer id;
    private String name;
    private boolean required;
    private boolean active = true;
    private Integer order = 0;
    private Integer oldCustonFieldId;
    private CustomFieldType customFieldType;
    private CategoryOption categoryOption;
    private String fieldType;
    private Location location;
    private Group group;
    private Category category;

    public static final String VALUETYPE_TEXT = "text";
    public static final String VALUETYPE_TEXTAREA = "textarea";
    public static final String VALUETYPE_SELECT = "select";
    public static final String VALUETYPE_CHECKBOX = "checkbox";
    public static final String VALUETYPE_RADIO = "radio";
    public static final String VALUETYPE_DATE = "date";
    public static final String VALUETYPE_NUMBER = "number";
    public static final String VALUETYPE_DATETIME = "datetime";
    public static final String VALUETYPE_TIME = "time";

    @SuppressWarnings("unchecked")
    private List<CustomFieldOption> options = LazyList.decorate(new ArrayList(), FactoryUtils.instantiateFactory(CustomFieldOption.class));

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME", length = 255)
    @NotEmpty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TYPE")
    public CustomFieldType getCustomFieldType() {
        return customFieldType;
    }

    public void setCustomFieldType(CustomFieldType customFieldType) {
        this.customFieldType = customFieldType;
    }

    @Column(name = "REQUIRED")
    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "CUSTOMFIELDSID", nullable = false)
    @IndexColumn(name = "OPTORDER")
    @Cascade(CascadeType.ALL)
    public List<CustomFieldOption> getOptions() {
        return options;
    }

    public void setOptions(List<CustomFieldOption> options) {
        this.options = options;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYOPTIONID")
    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    @Column(name = "CFORDER")
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Column(name = "ACTIVE")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Column(name = "OLDCFID")
    public Integer getOldCustonFieldId() {
        return oldCustonFieldId;
    }

    public void setOldCustonFieldId(Integer oldCustonFieldId) {
        this.oldCustonFieldId = oldCustonFieldId;
    }

    @Column(name = "FIELDTYPE")
    @NotEmpty
    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATIONID")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYID")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int compareTo(Object o) {
        if (this.order == null) setOrder(0);
        if (((CustomField) o).getOrder() == null) ((CustomField) o).setOrder(0);
        int result = this.order.compareTo(((CustomField) o).getOrder());
        if (result == 0) {
            return this.id.compareTo(((CustomField) o).getId());
        } else {
            return result;
        }
    }

}
