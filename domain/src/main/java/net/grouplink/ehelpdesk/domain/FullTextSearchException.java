package net.grouplink.ehelpdesk.domain;

public class FullTextSearchException extends RuntimeException {
    public FullTextSearchException(String message, Throwable cause) {
        super(message, cause);
    }
}
