package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Date: May 3, 2007
 * Time: 12:15:25 AM
 */
@Entity
@Table(name = "TTCFVALUE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TicketTemplateCustomFieldValue implements Serializable, Cloneable {

    private Integer id;
    private TicketTemplate ticketTemplate;
    private String fieldValue;
    private CustomField customField;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Lob
    @Column(name = "FIELDVALUE")
    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMFIELDSID")
    public CustomField getCustomField() {
        return customField;
    }

    public void setCustomField(CustomField customField) {
        this.customField = customField;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEMPLATEID")
    public TicketTemplate getTicketTemplate() {
        return ticketTemplate;
    }

    public void setTicketTemplate(TicketTemplate ticketTemplate) {
        this.ticketTemplate = ticketTemplate;
    }

    @Override
    protected TicketTemplateCustomFieldValue clone() throws CloneNotSupportedException {
        TicketTemplateCustomFieldValue cfv = (TicketTemplateCustomFieldValue) super.clone();
        cfv.setId(null);
        return cfv;
    }
}