package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.FetchType;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Jan 4, 2008
 * Time: 9:45:36 AM
 */
@Entity
@Table(name = "EMAILTOTICKET")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EmailToTicketConfig implements Serializable {

    public static final int SERVER_TYPE_IMAP = 1;
    public static final int SERVER_TYPE_POP = 2;
//    public static final int SERVER_TYPE_GROUPWISE = 3;

    public static final Integer DEF_PORT_IMAP = 143;
    public static final Integer DEF_PORT_IMAP_SSL = 993;
    public static final Integer DEF_PORT_POP = 110;
    public static final Integer DEF_PORT_POP_SSL = 995;

    public static final String SECURITY_NONE = "none";
    public static final String SECURITY_TLSIFAVAILABLE = "tlsIfAvailable";
    public static final String SECURITY_TLS = "tls";
    public static final String SECURITY_SSL = "ssl";

    public static final Integer NONEHDUSER_REJECT = 1;
    public static final Integer NONEHDUSER_ANONYMOUS = 2;
    public static final Integer NONEHDUSER_CREATE = 3;

    private Integer id;
    private String serverName;
    private Integer port;
    private String userName;
    private String password;
    private String newPassword;
    private String verifyPassword;
    /** Deprecated. Use security property instead. */
    private boolean useSsl;
    private int serverType;
    private String imapFolder;
    private boolean allowNonUsers;
    private Integer nonEhdUserHandlingMethod;
    private boolean enabled;
    private String replyDisplayName;
    private String replyEmailAddress;
    private Group group;
    private Location location;
    private TicketPriority ticketPriority;
    private String localeString;
    private Boolean sendDefaultEmail = true;
    private String security = SECURITY_NONE;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "SERVERNAME")
    @NotEmpty
    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    @Column(name = "PORT")
    @Range(min = 1, max = 65535)
    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Column(name = "USERNAME")
    @NotEmpty
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Deprecated. Use security property instead.
     * @return boolean ssl enabled
     */
    @Column(name = "USESSL")
    public boolean isUseSsl() {
        return useSsl;
    }

    /**
     * Deprecated. Use security property instead.
     * @param useSsl ssl enabled
     */
    public void setUseSsl(boolean useSsl) {
        this.useSsl = useSsl;
    }

    @Column(name = "SERVERTYPE")
    public int getServerType() {
        return serverType;
    }

    public void setServerType(int serverType) {
        this.serverType = serverType;
    }

    @Column(name = "IMAPFOLDER")
    public String getImapFolder() {
        return imapFolder;
    }

    public void setImapFolder(String imapFolder) {
        this.imapFolder = imapFolder;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    @NotNull
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Column(name = "ALLOWNONUSERS")
    public boolean isAllowNonUsers() {
        return allowNonUsers;
    }

    public void setAllowNonUsers(boolean allowNonUsers) {
        this.allowNonUsers = allowNonUsers;
    }

    @Column(name = "ENABLED")
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATIONID")
    @NotNull
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETPRIORITYID")
    public TicketPriority getTicketPriority() {
        return ticketPriority;
    }

    public void setTicketPriority(TicketPriority ticketPriority) {
        this.ticketPriority = ticketPriority;
    }

    @Column(name = "REPLYDISPLAYNAME")
    public String getReplyDisplayName() {
        return replyDisplayName;
    }

    public void setReplyDisplayName(String replyDisplayName) {
        this.replyDisplayName = replyDisplayName;
    }

    @Column(name = "REPLYEMAILADDRESS")
    @NotEmpty
    @Email
    public String getReplyEmailAddress() {
        return replyEmailAddress;
    }

    public void setReplyEmailAddress(String replyEmailAddress) {
        this.replyEmailAddress = replyEmailAddress;
    }

    @Column(name = "LOCALESTRING")
    public String getLocaleString() {
        return localeString;
    }

    public void setLocaleString(String localeString) {
        this.localeString = localeString;
    }

    @Column(name = "SENDDEFAULTEMAIL")
    public Boolean getSendDefaultEmail() {
        return sendDefaultEmail;
    }

    public void setSendDefaultEmail(Boolean sendDefaultEmail) {
        this.sendDefaultEmail = sendDefaultEmail==null?Boolean.TRUE:sendDefaultEmail;
    }

    @Column(name = "SECURITY")
    public String getSecurity() {
        return security;
    }

    public void setSecurity(String security) {
        this.security = security;
    }

    public Integer getNonEhdUserHandlingMethod() {
        return nonEhdUserHandlingMethod;
    }

    public void setNonEhdUserHandlingMethod(Integer nonEhdUserHandlingMethod) {
        this.nonEhdUserHandlingMethod = nonEhdUserHandlingMethod;
    }
}
