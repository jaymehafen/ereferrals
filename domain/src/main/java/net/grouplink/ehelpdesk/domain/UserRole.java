package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "USERROLE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserRole implements Serializable {

    private Integer id;
    private User user;
    private Role role;
    private boolean active = true;

    private Set<UserRoleGroup> userRoleGroup = new HashSet<UserRoleGroup>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "ACTIVE")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROLEID")
    public Role getRole() {
        return role;
    }
    
    public void setRole(Role role) {
        this.role = role;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public UserRoleGroup getUserRoleGroupByGroupId(Integer groupId) {
    	UserRoleGroup userRoleGroup = null;
        for (Object anUserRoleGroup : this.userRoleGroup) {
            UserRoleGroup urg = (UserRoleGroup) anUserRoleGroup;
            if (urg.getGroup() != null && urg.getGroup().getId().equals(groupId)) {
                userRoleGroup = urg;
                break;
            }
        }
    	
    	return userRoleGroup;
    }

    @OneToMany(mappedBy = "userRole")
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    public Set<UserRoleGroup> getUserRoleGroup() {
        return userRoleGroup;
    }

    public void setUserRoleGroup(Set<UserRoleGroup> userRoleGroup) {
        this.userRoleGroup = userRoleGroup;
    }

    @Transient
    public List<UserRoleGroup> getSortedURG(){
        List<UserRoleGroup> list = new ArrayList<UserRoleGroup>(userRoleGroup);
        Collections.sort(list);
        return list;
    }
}
