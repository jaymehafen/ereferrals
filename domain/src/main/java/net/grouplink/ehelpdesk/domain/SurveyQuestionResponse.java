package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Date: Mar 18, 2008
 * Time: 10:05:21 AM
 */
@Entity
@Table(name = "SURVEY_QUESTION_RESPONSE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SurveyQuestionResponse implements Serializable {

    private Integer id;

    private SurveyQuestion surveyQuestion;
    private String surveyQuestionResponseText;
    private Boolean selected;
    private Date responseDate;

    // Getters
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")    
    public Integer getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEYQUESTIONID")
    public SurveyQuestion getSurveyQuestion() {
        return surveyQuestion;
    }

    @Column(name = "SURVEYQUESTIONRESPONSETEXT")
    public String getSurveyQuestionResponseText() {
        return surveyQuestionResponseText;
    }

    @Column(name = "SELECTED")
    public Boolean getSelected() {
        return selected;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "responsedate")
    public Date getResponseDate() {
        return responseDate;
    }

    // Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setSurveyQuestion(SurveyQuestion surveyQuestion) {
        this.surveyQuestion = surveyQuestion;
    }

    public void setSurveyQuestionResponseText(String surveyQuestionResponseText) {
        this.surveyQuestionResponseText = surveyQuestionResponseText;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public void setResponseDate(Date responseDate) {
        this.responseDate = responseDate;
    }
}
