package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Filter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "TTMASTER")
@FilterDef(name = "deletedObject", parameters = @ParamDef(name = "isActive", type = "boolean"))
@Filter(name = "deletedObject", condition = "(ACTIVE is null or ACTIVE = :isActive)")
public class TicketTemplateMaster implements Serializable, Cloneable {

    private Integer id;
    private String name;
    private boolean privified;
    private Group group;
    private RecurrenceSchedule recurrenceSchedule;
    private Set<TicketTemplate> ticketTemplates = new HashSet<TicketTemplate>();
    private List<WorkflowStep> workflowSteps = new ArrayList<WorkflowStep>();
    private Boolean enableWorkflow = Boolean.FALSE;
    private Boolean active = Boolean.TRUE;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "master")
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @Filter(name = "deletedObject", condition = "(ACTIVE is null or ACTIVE = :isActive)")
    public Set<TicketTemplate> getTicketTemplates() {
        return ticketTemplates;
    }

    public void setTicketTemplates(Set<TicketTemplate> ticketTemplates) {
        this.ticketTemplates = ticketTemplates;
    }

    public void addTemplate(TicketTemplate ticketTemplate) {
        ticketTemplate.setMaster(this);
        ticketTemplates.add(ticketTemplate);
    }

    public void removeTemplate(TicketTemplate ticketTemplate){
        ticketTemplates.remove(ticketTemplate);
    }

    @OneToOne(mappedBy = "ticketTemplateMaster", fetch = FetchType.LAZY)
    @Cascade(CascadeType.ALL)
    public RecurrenceSchedule getRecurrenceSchedule() {
        return recurrenceSchedule;
    }

    public void setRecurrenceSchedule(RecurrenceSchedule recurrenceSchedule) {
        this.recurrenceSchedule = recurrenceSchedule;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }


    public boolean isPrivified() {
        return privified;
    }

    public void setPrivified(boolean privified) {
        this.privified = privified;
    }

    @OneToMany(mappedBy = "ticketTemplateMaster")
    @IndexColumn(name = "STEPNUMBER", base = 1)
    @Cascade(CascadeType.ALL)
    public List<WorkflowStep> getWorkflowSteps() {
        return workflowSteps;
    }

    public void setWorkflowSteps(List<WorkflowStep> workflowSteps) {
        this.workflowSteps = workflowSteps;
    }

    public void addWorkflowStep(WorkflowStep wfs) {
        wfs.setTicketTemplateMaster(this);
        workflowSteps.add(wfs);
    }

    @Column(name = "ENABLEWORKFLOW")
    public Boolean getEnableWorkflow() {
        if (enableWorkflow == null) {
            this.enableWorkflow = false;
        }
        
        return enableWorkflow;
    }

    public void setEnableWorkflow(Boolean enableWorkflow) {
        if (enableWorkflow == null) {
            this.enableWorkflow = Boolean.FALSE;
        } else {
            this.enableWorkflow = enableWorkflow;
        }
    }

    @Column(name = "ACTIVE")
    public Boolean getActive() {
        if (active == null) {
            active = Boolean.TRUE;
        }
        
        return active;
    }

    public void setActive(Boolean active) {
        if (active == null) {
            this.active = Boolean.TRUE;
        } else {
            this.active = active;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TicketTemplateMaster)) return false;

        TicketTemplateMaster that = (TicketTemplateMaster) o;

        if (privified != that.privified) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        else assert true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (privified ? 1 : 0);
        return result;
    }

    @Override
    public TicketTemplateMaster clone() throws CloneNotSupportedException {
        TicketTemplateMaster templateMaster = (TicketTemplateMaster) super.clone();
        templateMaster.setId(null);
        return templateMaster;
    }
}
