package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.acl.SecurityLevel;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * Represents a <code>KnowledgeBase</code> article in the eHelpDesk system.
 */
@Entity
@Table(name = "KNOWLEDGEBASE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class KnowledgeBase implements Serializable {

	private Integer id;
	private String subject;
	private String problem;
	private String resolution;
	private Date date;
	private Boolean isPrivate;
    private Group group;
    private Integer timesViewed = 0;

    private Set<Ticket> tickets = new HashSet<Ticket>();
    private Set<Attachment> attachments = new HashSet<Attachment>();
    /** Used in spring binding. Runtime type is org.springframework.web.multipart.MultipartFile */
    private List<Object> attchmnt = new ArrayList<Object>();
    private SecurityLevel securityLevel;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToMany(mappedBy = "knowledgeBase")
    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    @Transient
    public int getTrueOrFalse(){
        if(isPrivate){
            return 1;
        }
        else{
            return 0;
        }
    }

    @ManyToMany
    @Cascade(value = {org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @JoinTable(name = "KNOWLEDGEBASEATTACHMENTS",
            joinColumns = @JoinColumn(name = "KNOWLEDGEBASEID"),
            inverseJoinColumns = @JoinColumn(name = "ATTACHMENTID"))
    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    /**
     * Used in spring binding. Runtime type is
     * org.springframework.web.multipart.MultipartFile, which is located in the
     * spring-webmvc module. The domain layer is not allowed to depend on the
     * spring-webmvc module.
     * @return list of MultipartFile
     */
    @Transient
    public List<Object> getAttchmnt() {
        return attchmnt;
    }

    public void setAttchmnt(List<Object> attchmnt) {
        this.attchmnt = attchmnt;
    }

    /**
	 * @return the timesView
	 */
    @Column(name = "TIMESVIEWED")
	public Integer getTimesViewed() {
	    return timesViewed;
	}

	/**
	 * @param timesViewed the timesView to set
	 */
	public void setTimesViewed(Integer timesViewed) {
	    this.timesViewed = timesViewed;
	}

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDDATE")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

    @Lob
    @Column(name = "PROBLEM")
	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

    @Lob
    @Column(name = "RESOLUTION")
	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

    @Column(name = "SUBJECT")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

    @Column(name = "ISPRIVATE")
	public Boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

    @Transient
    public String getResolutionText() {
        String text = null;
        if (resolution != null) {
            text = resolution.replaceAll("<.*?>", "");
            text = text.replaceAll("&.*?;", " ");
            text = text.replaceAll("      ", "\n");
            text = text.trim();
        }

	    return text;
	}

    @Transient
    public String getProblemText() {
        String text = null;
        if (problem != null) {
            text = problem.replaceAll("<.*?>", "");
            text = text.replaceAll("&.*?;", " ");
            text = text.replaceAll("      ", "\n");
            text = text.trim();
        }

	    return text;
	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SECURITYLEVELID")
    public SecurityLevel getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(SecurityLevel securityLevel) {
        this.securityLevel = securityLevel;
    }
}
