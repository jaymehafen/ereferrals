package net.grouplink.ehelpdesk.domain.dashboard;

import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.Report;
import org.apache.cxf.aegis.type.java5.IgnoreProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "DBWIDGET")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Widget implements Comparable, Serializable, Cloneable {

    public static final int DIAL = 1;
    public static final int HORIZONTAL_ARC_DIAL = 2;
    public static final int VERTICAL_ARC_DIAL = 3;
    public static final int THERMOMETER = 4;
    public static final int BAR_CHART = 5;
    public static final int STACKED_BAR_CHART = 6;
    public static final int PIE_CHART = 7;
    public static final int MULTI_PIE_CHART = 8;

    private Integer id;
    private String name;
    private TicketFilter ticketFilter;
    private Report report;
    private Integer row;
    private Integer rowspan;
    private Integer column;
    private Integer colspan;
    private Dashboard dashboard;
    private Integer type;

    // Thresholds
    private Integer minimum;
    private Integer maximum;
    private Integer critical;

    private Integer greenRangeBegin;
    private Integer greenRangeEnd;
    private Integer yellowRangeBegin;
    private Integer yellowRangeEnd;
    private Integer redRangeBegin;
    private Integer redRangeEnd;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETFILTERID")
    public TicketFilter getTicketFilter() {
        return ticketFilter;
    }

    public void setTicketFilter(TicketFilter ticketFilter) {
        this.ticketFilter = ticketFilter;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPORTID")
    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    @Column(name = "DBROW")
    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    @Column(name = "ROWSPAN")
    public Integer getRowspan() {
        return rowspan==null?1:rowspan;
    }

    public void setRowspan(Integer rowspan) {
        this.rowspan = rowspan;
    }

    @Column(name = "DBCOLUMN")
    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    @Column(name = "COLSPAN")
    public Integer getColspan() {
        return colspan==null?1:colspan;
    }

    public void setColspan(Integer colspan) {
        this.colspan = colspan;
    }

    @IgnoreProperty
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DASHBOARDID")
    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    @Column(name = "TYPE")
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Column(name = "MINIMUM")
    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    @Column(name = "MAXIMUM")
    public Integer getMaximum() {
        return maximum;
    }

    public void setMaximum(Integer maximum) {
        this.maximum = maximum;
    }

    @Column(name = "CRITICAL")
    public Integer getCritical() {
        return critical;
    }

    public void setCritical(Integer critical) {
        this.critical = critical;
    }

    @Column(name = "GREENRANGEBEGIN")
    public Integer getGreenRangeBegin() {
        return greenRangeBegin;
    }

    public void setGreenRangeBegin(Integer greenRangeBegin) {
        this.greenRangeBegin = greenRangeBegin;
    }

    @Column(name = "GREENRANGEEND")
    public Integer getGreenRangeEnd() {
        return greenRangeEnd;
    }

    public void setGreenRangeEnd(Integer greenRangeEnd) {
        this.greenRangeEnd = greenRangeEnd;
    }

    @Column(name = "YELLOWRANGEBEGIN")
    public Integer getYellowRangeBegin() {
        return yellowRangeBegin;
    }

    public void setYellowRangeBegin(Integer yellowRangeBegin) {
        this.yellowRangeBegin = yellowRangeBegin;
    }

    @Column(name = "YELLOWRANGEEND")
    public Integer getYellowRangeEnd() {
        return yellowRangeEnd;
    }

    public void setYellowRangeEnd(Integer yellowRangeEnd) {
        this.yellowRangeEnd = yellowRangeEnd;
    }

    @Column(name = "REDRANGEBEGIN")
    public Integer getRedRangeBegin() {
        return redRangeBegin;
    }

    public void setRedRangeBegin(Integer redRangeBegin) {
        this.redRangeBegin = redRangeBegin;
    }

    @Column(name = "REDRANGEEND")
    public Integer getRedRangeEnd() {
        return redRangeEnd;
    }

    public void setRedRangeEnd(Integer redRangeEnd) {
        this.redRangeEnd = redRangeEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Widget)) return false;

        Widget widget = (Widget) o;

        if (column != null ? !column.equals(widget.column) : widget.column != null) return false;
        if (critical != null ? !critical.equals(widget.critical) : widget.critical != null) return false;
        if (greenRangeBegin != null ? !greenRangeBegin.equals(widget.greenRangeBegin) : widget.greenRangeBegin != null)
            return false;
        if (greenRangeEnd != null ? !greenRangeEnd.equals(widget.greenRangeEnd) : widget.greenRangeEnd != null)
            return false;
        if (maximum != null ? !maximum.equals(widget.maximum) : widget.maximum != null) return false;
        if (minimum != null ? !minimum.equals(widget.minimum) : widget.minimum != null) return false;
        if (name != null ? !name.equals(widget.name) : widget.name != null) return false;
        if (redRangeBegin != null ? !redRangeBegin.equals(widget.redRangeBegin) : widget.redRangeBegin != null)
            return false;
        if (redRangeEnd != null ? !redRangeEnd.equals(widget.redRangeEnd) : widget.redRangeEnd != null) return false;
        if (row != null ? !row.equals(widget.row) : widget.row != null) return false;
        if (type != null ? !type.equals(widget.type) : widget.type != null) return false;
        if (yellowRangeBegin != null ? !yellowRangeBegin.equals(widget.yellowRangeBegin) : widget.yellowRangeBegin != null)
            return false;
        if (yellowRangeEnd != null ? !yellowRangeEnd.equals(widget.yellowRangeEnd) : widget.yellowRangeEnd != null)
            return false;
        else assert true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (row != null ? row.hashCode() : 0);
        result = 31 * result + (column != null ? column.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (minimum != null ? minimum.hashCode() : 0);
        result = 31 * result + (maximum != null ? maximum.hashCode() : 0);
        result = 31 * result + (critical != null ? critical.hashCode() : 0);
        result = 31 * result + (greenRangeBegin != null ? greenRangeBegin.hashCode() : 0);
        result = 31 * result + (greenRangeEnd != null ? greenRangeEnd.hashCode() : 0);
        result = 31 * result + (yellowRangeBegin != null ? yellowRangeBegin.hashCode() : 0);
        result = 31 * result + (yellowRangeEnd != null ? yellowRangeEnd.hashCode() : 0);
        result = 31 * result + (redRangeBegin != null ? redRangeBegin.hashCode() : 0);
        result = 31 * result + (redRangeEnd != null ? redRangeEnd.hashCode() : 0);
        return result;
    }

    @Override
    public Widget clone() throws CloneNotSupportedException {
        Widget clone = (Widget)super.clone();
        clone.setId(null);
        return clone;
    }

    public int compareTo(Object o) {
        int rowCompare = this.row.compareTo(((Widget) o).getRow());
        int colCompare = this.column.compareTo(((Widget) o).getColumn());

        return rowCompare==0?colCompare:rowCompare;
    }

}
