package net.grouplink.ehelpdesk.domain.filter;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;

import java.io.Serializable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 3/15/12
 * Time: 6:17 PM
 */
public class AssignmentFilter implements Serializable {
    private Group group;
    private Location location;
    private Category category;
    private CategoryOption categoryOption;
    private String assignment;
    private List<UserRoleGroup> userRoleGroupList;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public List<UserRoleGroup> getUserRoleGroupList() {
        return userRoleGroupList;
    }

    public void setUserRoleGroupList(List<UserRoleGroup> userRoleGroupList) {
        this.userRoleGroupList = userRoleGroupList;
    }
}
