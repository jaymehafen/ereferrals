package net.grouplink.ehelpdesk.domain;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a <code>User</code> in the eHelpDesk system.
 *
 * @author Mark Rollins
 */
@Entity
@Table(name = "USERS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User implements Serializable, Comparable, Cloneable {

    public static final Integer ADMIN_ID = 0;
    public static final String ADMIN_LOGIN = "admin";
    public static final Integer ANONYMOUS_ID = -1;

    private Integer id;
    private String salutation;
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private String loginId;
    private String password;
    private String changePassword;
    private String retypePassword;
    private boolean active = true;
    private boolean onVacation = false;
    private String gender;
    private Date creationDate = new Date();
    private String creator = "SYSTEM";
    private String comments;
    private String colLoginId = null;
    private String colPassword = null;
    private String ldapDN;

    private Set<Address> address = new HashSet<Address>();
    private Set<Phone> phone = new HashSet<Phone>();
    private Set<UserRole> userRoles = new HashSet<UserRole>();
    private Set<UserRoleGroup> notificationSupport = new HashSet<UserRoleGroup>();

    @SuppressWarnings("unchecked")
    private List<Address> addresses = LazyList.decorate(new ArrayList<Address>(), FactoryUtils.instantiateFactory(Address.class));
    @SuppressWarnings("unchecked")  
    private List<Phone> phones = LazyList.decorate(new ArrayList<Phone>(), FactoryUtils.instantiateFactory(Phone.class));

    private User oldUser;

    private Location location;


    /**
     * Default constructor. Creates a new <code>User</code>
     */
    public User() {
    }

    /**
     * Minimal constructor specifying the most pertinent properties to be set.
     *
     * @param loginId      the identifier this <code>User</code> will use to login to
     *                     the system.
     * @param password     the password this <code>User</code> will use to login to the
     *                     system.
     * @param active       <code>true</code> if this <code>User</code> is active in
     *                     the system; <code>false</code> otherwise.
     * @param creationDate the time that this <code>User</code> was created.
     */
    public User(String loginId, String password, boolean active, Date creationDate) {
        this.loginId = loginId;
        this.password = password;
        this.active = active;
        this.creationDate = creationDate;
    }

    /**
     * Full constructor specifying all possible properties of a
     * <code>User</code>
     *
     * @param salutation   this <code>User</code>'s preferred salutation. (Mr. Mrs.
     *                     Ms. Dr. etc.)
     * @param firstname    the first name of this <code>User</code>
     * @param lastname     the last or surname of this <code>User</code>
     * @param middlename   this <code>User</code>'s middle name or initial
     * @param email        this <code>User</code>'s email address.
     * @param loginId      the identifier this <code>User</code> will use to login to the system.
     * @param password     the password this <code>User</code> will use to login to the system.
     * @param active       <code>true</code> if this <code>User</code> is active in
     *                     the system; <code>false</code> otherwise.
     * @param onVacation   flag to denote that this <code>User</code> is on vacation or
     *                     otherwise temporarily unavailable. <code>true</code> if this
     *                     <code>User</code> is unavailable; <code>false</code> otherwise.
     * @param gender       "M" if this <code>User</code> is Male, "F" if Female and "?"
     *                     if unknown or confused
     * @param creationDate the time that this <code>User</code> was created.
     * @param creator      the name or other identifier of the entity that created this
     *                     <code>User</code>
     * @param comments     general comments about or description of this
     *                     <code>User</code>
     * @see Role
     */
    public User(String salutation, String firstname, String lastname, String middlename, String email, String loginId,
                String password, boolean active, boolean onVacation, String gender, Date creationDate, String creator,
                String comments) {
        this.salutation = salutation;
        this.firstName = firstname;
        this.lastName = lastname;
        this.middleName = middlename;
        this.email = email;
        this.loginId = loginId;
        this.password = password;
        this.active = active;
        this.onVacation = onVacation;
        this.gender = gender;
        this.creationDate = creationDate;
        this.creator = creator;
        this.comments = comments;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "COLLOGINID")
    public String getColLoginId() {
        return colLoginId;
    }

    public void setColLoginId(String colLoginId) {
        this.colLoginId = colLoginId;
    }

    @Column(name = "COLPASSWORD")
    public String getColPassword() {
        return colPassword;
    }

    public void setColPassword(String colPassword) {
        this.colPassword = colPassword;
    }

    @Column(name = "SALUTATION")
    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    @Column(name = "FIRSTNAME")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    @Column(name = "LASTNAME")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    @Column(name = "MIDDLENAME")
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middlename) {
        this.middleName = middlename;
    }

    @Column(name = "EMAIL")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "LOGINID")
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Column(name = "PASSWORD", length = 32)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "ACTIVE")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Transient
    public boolean isAdmin() {
        return this.id.equals(ADMIN_ID) && this.loginId.equals(ADMIN_LOGIN);
    }

    @Column(name = "ONVACATION")
    public boolean isOnVacation() {
        return onVacation;
    }

    public void setOnVacation(boolean onVacation) {
        this.onVacation = onVacation;
    }

    @Column(name = "GENDER")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATIONDATE")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "CREATOR")
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Lob
    @Column(name = "COMMENTS")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * Returns all the <code>Address</code>'s that this <code>User</code>
     * has
     *
     * @return the set of <code>Address</code>'s for this <code>User</code>
     * @see Address
     */
    @Transient
    public List<Address> getAddresses() {
        return addresses;
    }

    /**
     * Assigns a group of <code>Address</code>'s to this <code>User</code>
     *
     * @param addresses the <code>List</code> of addresses
     * @see Address
     */
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    /**
     * Returns all the <code>Phone</code>'s that this <code>User</code> has
     *
     * @return the list of <code>Phone</code>'s for this <code>User</code>
     * @see Phone
     */
    @Transient
    public List<Phone> getPhones() {
        return phones;
    }

    /**
     * Assigns a group of <code>Phone</code>'s to this <code>User</code>
     *
     * @param phones the <code>List</code> of phones
     * @see Phone
     */
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @OneToMany(mappedBy = "user")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public UserRole getUserRoleByRoleId(Integer roleId) {
        UserRole userRole = null;
        for (UserRole userRole1 : userRoles) {
            if (userRole1.getRole() != null && userRole1.getRole().getId().equals(roleId)) {
                userRole = userRole1;
                break;
            }
        }
        
        return userRole;
    }

    @Transient
    public boolean getConsumesTechLicense() {
        boolean consumesTechLicense = false;
        Set<UserRole> roles = getUserRoles();
        for (Object role : roles) {
            UserRole userRole = (UserRole) role;
            if (userRole.getRole() != null && !userRole.getRole().getId().equals(Role.ROLE_USER_ID)
                    && !userRole.getRole().getId().equals(Role.ROLE_ADMINISTRATOR_ID)) {
                consumesTechLicense = true;
                break;
            }
        }

        return consumesTechLicense;
    }

    public String toJSONString() throws JSONException {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("id", this.id);
        jsonObj.put("firstName", this.firstName);
        jsonObj.put("lastName", this.lastName);
        jsonObj.put("loginId", this.loginId);
        jsonObj.put("email", this.email);
        return jsonObj.toString();
    }

    @OneToMany(mappedBy = "user")
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    public Set<Address> getAddress() {
        return address;
    }

    public void setAddress(Set<Address> address) {
        this.address = address;
    }

    public void addAddress(Address a) {
        a.setUser(this);
        address.add(a);
    }

    @OneToMany(mappedBy = "user")
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    public Set<Phone> getPhone() {
        return phone;
    }

    public void setPhone(Set<Phone> phone) {
        this.phone = phone;
    }

    public void addPhone(Phone p) {
        p.setUser(this);
        phone.add(p);
    }

    @Transient
    public Phone getPrimaryPhone() {
        if (phone == null)
            return null;

        Phone first = null;
        for (Phone p : phone) {
            if (first == null) {
                first = p;
            }

            if (Boolean.TRUE.equals(p.getPrimaryPhone())) {
                return p;
            }
        }

        return first;
    }

    @Transient
    public List<Phone> getPhoneList() {
        if (phone == null) {
            return new ArrayList<Phone>();
        }
        return new ArrayList<Phone>(phone);
    }

    @Transient
    public List<Address> getAddressList() {
        if (address == null) {
            return new ArrayList<Address>();
        }
        return new ArrayList<Address>(address);
    }

    @Transient
    public String getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(String changePassword) {
        this.changePassword = changePassword;
    }

    @Transient
    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    @Column(name = "LDAPDN")
    public String getLdapDN() {
        return ldapDN;
    }

    public void setLdapDN(String ldapDN) {
        this.ldapDN = ldapDN;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATIONID")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Transient
    public String getJsEscapeFirstName() {
        return StringEscapeUtils.escapeJavaScript(firstName);
    }

    @Transient
    public String getJsEscapeLastName() {
        return StringEscapeUtils.escapeJavaScript(lastName);
    }

    /**
     * Gets the Set of Role Names that this user belongs to.
     *
     * @return the Set of Role Names
     */
    @Transient
    public Set<String> getRoles() {
        Set<String> result = new HashSet<String>();
        for (UserRole userRole : userRoles) {
            if (userRole.isActive()) {
                result.add(userRole.getRole().getName());
            }
        }

        return result;
    }

    /**
     * Gets a List of the Group objects that this user belongs to.
     *
     * @return the List of Groups
     */
    @Transient
    public List<Group> getGroups() {
        Set<Group> result = new HashSet<Group>();
        for (UserRole userRole : userRoles) {
            if (userRole.isActive()) {
                Set<UserRoleGroup> urgs = userRole.getUserRoleGroup();
                for (UserRoleGroup urg1 : urgs) {
                    result.add(urg1.getGroup());
                }
            }
        }

        return new ArrayList<Group>(result);
    }

    @Transient
    public String getActiveString() {
        if (active) {
            return "global.active";
        } else {
            return "global.inactive";
        }
    }

    @ManyToMany(mappedBy = "notificationUsers")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Set<UserRoleGroup> getNotificationSupport() {
        return notificationSupport;
    }

    public void setNotificationSupport(Set<UserRoleGroup> notificationSupport) {
        this.notificationSupport = notificationSupport;
    }

    @Transient
    public User getOldUser() {
        return oldUser;
    }

    public void setOldUser(User oldUser) {
        this.oldUser = oldUser;
    }

    @Transient
    public String getDisplayName(){
        return lastName + ", " + firstName + " ("+loginId+")";
    }

//    public boolean equals(Object object){
//        return object instanceof User && id.equals(((User) object).getId());
//    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (getEmail() != null ? !getEmail().equals(user.getEmail()) : user.getEmail() != null)
            return false;
        if (getFirstName() != null ? !getFirstName().equals(user.getFirstName()) : user.getFirstName() != null)
            return false;
        if (getId() != null ? !getId().equals(user.getId()) : user.getId() != null) return false;
        if (getLastName() != null ? !getLastName().equals(user.getLastName()) : user.getLastName() != null)
            return false;
        if (getLoginId() != null ? !getLoginId().equals(user.getLoginId()) : user.getLoginId() != null)
            return false;
        if (getMiddleName() != null ? !getMiddleName().equals(user.getMiddleName()) : user.getMiddleName() != null)
            return false;
        else assert true;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = (getId() != null ? getId().hashCode() : 0);
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getMiddleName() != null ? getMiddleName().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getLoginId() != null ? getLoginId().hashCode() : 0);
        return result;
    }

    public int compareTo(Object o) {
        int result = 0;
        String f2 = ((User) o).getFirstName();
        String l2 = ((User) o).getLastName();
        if (StringUtils.isNotBlank(lastName) && StringUtils.isNotBlank(l2)) {
            result = String.CASE_INSENSITIVE_ORDER.compare(lastName, l2);
        } else if (StringUtils.isBlank(lastName)) {
            result = -1;
        } else if (StringUtils.isBlank(l2)) {
            result = 1;
        }

        if (result == 0) {
            if (StringUtils.isNotBlank(firstName) && StringUtils.isNotBlank(f2)) {
                result = String.CASE_INSENSITIVE_ORDER.compare(firstName, ((User) o).getFirstName());
            } else if (StringUtils.isBlank(firstName)) {
                result = -1;
            } else if (StringUtils.isBlank(f2)) {
                result = 1;
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", salutation='" + salutation + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", email='" + email + '\'' +
                ", loginId='" + loginId + '\'' +
                ", password='" + password + '\'' +
                ", changePassword='" + changePassword + '\'' +
                ", retypePassword='" + retypePassword + '\'' +
                ", active=" + active +
                ", onVacation=" + onVacation +
                ", gender='" + gender + '\'' +
                ", creationDate=" + creationDate +
                ", creator='" + creator + '\'' +
                ", comments='" + comments + '\'' +
                ", colLoginId='" + colLoginId + '\'' +
                ", colPassword='" + colPassword + '\'' +
                ", ldapDN='" + ldapDN + '\'' +
                '}';
    }

    @Override
    public User clone() throws CloneNotSupportedException {
        User u = (User) super.clone();
        u.setId(null);
        return u;
    }
}
