package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Date: Feb 27, 2008
 * Time: 7:25:07 AM
 */
@Entity
@Table(name = "SURVEY_DATA")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class SurveyData implements Serializable {

    private Integer id;

    private Survey survey;
    private SurveyQuestion surveyQuestion;
    private Ticket ticket;
    private String value;

    // Getters
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEYID")
    public Survey getSurvey() {
        return survey;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEYQUESTIONID")
    public SurveyQuestion getSurveyQuestion() {
        return surveyQuestion;
    }

    @Column(name = "VALUE")
    public String getValue() {
        return value;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETID")
    public Ticket getTicket() {
        return ticket;
    }

    // Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public void setSurveyQuestion(SurveyQuestion surveyQuestion) {
        this.surveyQuestion = surveyQuestion;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    // Misc
    @Transient
    public String getSurveyQuestionText() {
        return surveyQuestion.getSurveyQuestionText();
    }

    @Transient
    public String getTruncatedSurveyQuestionText() {
        String question = surveyQuestion.getSurveyQuestionText();
        if (question.length() > 45)
            question = question.substring(0, 44).trim() + "...";
        return question;
    }
}
