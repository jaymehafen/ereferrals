package net.grouplink.ehelpdesk.domain.utils;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.Group;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Nov 14, 2008
 * Time: 10:42:13 AM
 */
public class CategoryComparator implements Comparator<Category> {

    public int compare(Category cat1, Category cat2) {
        String c1Name = cat1.getName();
        String c2Name = cat2.getName();
        Group group1 = cat1.getGroup();
        Group group2 = cat2.getGroup();
        String g1Name = group1.getName();
        String g2Name = group2.getName();
        int comp = g1Name.compareTo(g2Name);
        if (comp != 0)
            return comp;
        else {
            return c1Name.compareTo(c2Name);    
        }
    }
}
