package net.grouplink.ehelpdesk.domain.filter;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.TermsFilter;
import org.hibernate.search.annotations.Factory;
import org.hibernate.search.annotations.Key;
import org.hibernate.search.filter.FilterKey;
import org.hibernate.search.filter.StandardFilterKey;

/**
 * Author: zpearce
 * Date: 6/10/11
 * Time: 12:36 PM
 */
public class UserVisibleTicketFilterFactory {
    private User user;

    @Key
    public FilterKey getKey() {
        StandardFilterKey key = new StandardFilterKey();
        key.addParameter(user);
        return key;
    }

    @Factory
    public Filter getFilter() {
        TermsFilter filter = new TermsFilter();
        for(Group g : user.getGroups()) {
            filter.addTerm(new Term("group.id", g.getId().toString()));
        }
        return filter;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
