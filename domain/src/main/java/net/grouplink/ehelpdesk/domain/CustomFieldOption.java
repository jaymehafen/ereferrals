package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Date: May 3, 2007
 * Time: 12:19:25 AM
 */
@Entity
@Table(name = "CUSTOMFIELDOPTIONS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CustomFieldOption implements Serializable, Comparable{

    private Integer id;
    private String value;
    private String displayValue;
    private Integer order;
    
    private CustomField customField;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "VALUE")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "DISPLAYVALUE")
    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    @Column(name = "OPTORDER", insertable = false, updatable = false)
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMFIELDSID", nullable = false, updatable = false, insertable = false)
    public CustomField getCustomField() {
        return customField;
    }

    public void setCustomField(CustomField customField) {
        this.customField = customField;
    }

    public int compareTo(Object o) {
        if(this.order == null) setOrder(0);
        if(((CustomField)o).getOrder() == null) ((CustomField)o).setOrder(0);
        return this.order.compareTo(((CustomField) o).getOrder());
    }
}
