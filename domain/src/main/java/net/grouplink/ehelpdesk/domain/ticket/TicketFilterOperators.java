package net.grouplink.ehelpdesk.domain.ticket;

import java.io.Serializable;

/**
 * Date: Mar 3, 2008
 * Time: 11:57:24 AM
 */
public class TicketFilterOperators implements Serializable {
    public static final int DATE_ANY = 0;
    public static final int DATE_TODAY = 1;
    public static final int DATE_WEEK = 2;
    public static final int DATE_MONTH = 3;
    public static final int DATE_ON = 4;
    public static final int DATE_BEFORE = 5;
    public static final int DATE_AFTER = 6;
    public static final int DATE_RANGE = 7;
    public static final int DATE_LAST_X_HOURS = 8;
    public static final int DATE_LAST_X_DAYS = 17;
    public static final int DATE_LAST_X_WEEKS = 18;
    public static final int DATE_LAST_X_MONTHS = 19;
    public static final int DATE_LAST_X_YEARS = 20;

    public static final int EQUALS = 9;
    public static final int NOT_EQUALS = 10;
    public static final int GREATER_THAN = 11;
    public static final int LESS_THAN = 12;
    public static final int CONTAINS = 13;
    public static final int NOT_CONTAINS = 14;
    public static final int STARTS_WITH = 15;
    public static final int ENDS_WIDTH = 16;

    private Integer value;
    private String display;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public static String getMessageCode(int operator){
        String retVal = "";
        switch(operator){
            case DATE_ANY: retVal = "ticketSearch.any"; break;
            case DATE_TODAY: retVal = "date.today"; break;
            case DATE_WEEK: retVal = "date.thisweek"; break;
            case DATE_MONTH: retVal = "date.thismonth"; break;
            case DATE_ON: retVal = "date.on"; break;
            case DATE_BEFORE: retVal = "date.before"; break;
            case DATE_AFTER: retVal = "date.after"; break;
            case DATE_RANGE: retVal = "date.range"; break;
            case DATE_LAST_X_HOURS: retVal = "date.lastx.hours"; break;
            case DATE_LAST_X_DAYS: retVal = "date.lastx.days"; break;
            case DATE_LAST_X_WEEKS: retVal = "date.lastx.weeks"; break;
            case DATE_LAST_X_MONTHS: retVal = "date.lastx.months"; break;
            case DATE_LAST_X_YEARS: retVal = "date.lastx.years"; break;
            case EQUALS: retVal = "ticketFilter.op.equals"; break;
            case NOT_EQUALS: retVal = "ticketFilter.op.notEquals"; break;
            case GREATER_THAN: retVal = "ticketFilter.op.greaterThan"; break;
            case LESS_THAN: retVal = "ticketFilter.op.lessThan"; break;
            case CONTAINS: retVal = "ticketFilter.op.contains"; break;
            case NOT_CONTAINS: retVal = "ticketFilter.op.notContains"; break;
            case STARTS_WITH: retVal = "ticketFilter.op.startsWith"; break;
            case ENDS_WIDTH: retVal = "ticketFilter.op.endsWith"; break;
        }
        return retVal;
    }
}
