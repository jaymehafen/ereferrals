package net.grouplink.ehelpdesk.domain;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Jun 21, 2007
 * Time: 2:39:56 PM
 */
public class UrgSetter implements Serializable {

    private UserRoleGroup userRoleGroup;
    private Group group;
    private User user;
    private Role role;

    public UserRoleGroup getUserRoleGroup() {
        return userRoleGroup;
    }

    public void setUserRoleGroup(UserRoleGroup userRoleGroup) {
        this.userRoleGroup = userRoleGroup;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
