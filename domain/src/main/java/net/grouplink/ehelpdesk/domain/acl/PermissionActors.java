package net.grouplink.ehelpdesk.domain.acl;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ACL_PERMISSIONACTORS")
public class PermissionActors implements Serializable {
    private Integer id;
    private Set<User> users = new HashSet<User>();
    private Set<Role> roles = new HashSet<Role>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToMany
    @JoinTable(name = "ACL_PERMISSIONACTORS_USERS",
                joinColumns = @JoinColumn(name = "PERMISSIONACTORSID"),
                inverseJoinColumns = @JoinColumn(name = "USERID"))
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        users.add(user);
    }

    @ManyToMany
    @JoinTable(name = "ACL_PERMISSIONACTORS_ROLL",
                joinColumns = @JoinColumn(name = "PERMISSIONACTORSID"),
                inverseJoinColumns = @JoinColumn(name = "ROLEID"))
    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        roles.add(role);
    }
}
