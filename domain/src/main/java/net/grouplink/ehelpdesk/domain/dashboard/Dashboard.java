package net.grouplink.ehelpdesk.domain.dashboard;

import org.apache.cxf.aegis.type.java5.IgnoreProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;

@Entity
@Table(name = "DASHBOARD")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Dashboard implements Serializable, Cloneable {
    private Integer id;
    private String name;
    private Set<Widget> widgets = new TreeSet<Widget>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "dashboard", fetch = FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    public Set<Widget> getWidgets() {
        return widgets;
    }

    public void setWidgets(Set<Widget> widgets) {
        this.widgets = widgets;
    }

    @IgnoreProperty
    @Transient
    public List<Widget> getSortedWidgets() {
        List<Widget> sortedWidgets = new ArrayList<Widget>(widgets);
        Collections.sort(sortedWidgets, new Comparator<Widget>() {
            public int compare(Widget o1, Widget o2) {
                int rowCompare = o1.getRow().compareTo(o2.getRow());
                int colCompare = o1.getColumn().compareTo(o2.getColumn());
                return rowCompare == 0 ? colCompare : rowCompare;
            }
        });

        return sortedWidgets;
    }

    public void addWidget(Widget widget) {
        widget.setDashboard(this);
        widgets.add(widget);
    }

    public void removeWidget(Widget widget) {
        widgets.remove(widget);
    }

    @Override
    public Dashboard clone() throws CloneNotSupportedException {
        Dashboard dashboard = (Dashboard)super.clone();
        dashboard.setId(null);
        
        // clear existing widgets and build a list of cloned widgets
        // for the dashboard clone
        dashboard.setWidgets(new TreeSet<Widget>());
        for (Widget widget : widgets) {
            dashboard.addWidget(widget.clone());
        }

        return dashboard;
    }

}
