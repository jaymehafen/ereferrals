package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Date;

import net.grouplink.ehelpdesk.domain.mail.EmailMessageTemplate;

@Entity
@Table(name = "REPORTRECURSCHED")
public class ReportRecurrenceSchedule implements Serializable {

    private Integer id;
    private Report report;
    private RecurrenceSchedule recurrenceSchedule;
    private Date lastAction;
    private Integer occurrenceCount;
    private EmailMessageTemplate emailMessageTemplate;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REPORTID")
    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RECURRENCESCHEDULEID")
    @Cascade(CascadeType.ALL)
    public RecurrenceSchedule getRecurrenceSchedule() {
        return recurrenceSchedule;
    }

    public void setRecurrenceSchedule(RecurrenceSchedule recurrenceSchedule) {
        this.recurrenceSchedule = recurrenceSchedule;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getLastAction() {
        return lastAction;
    }

    public void setLastAction(Date lastAction) {
        this.lastAction = lastAction;
    }

    public Integer getOccurrenceCount() {
        return occurrenceCount;
    }

    public void setOccurrenceCount(Integer occurrenceCount) {
        this.occurrenceCount = occurrenceCount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMAILTEMPLATEID")
    @Cascade(CascadeType.ALL)
    public EmailMessageTemplate getEmailMessageTemplate() {
        return emailMessageTemplate;
    }

    public void setEmailMessageTemplate(EmailMessageTemplate emailMessageTemplate) {
        this.emailMessageTemplate = emailMessageTemplate;
    }
}
