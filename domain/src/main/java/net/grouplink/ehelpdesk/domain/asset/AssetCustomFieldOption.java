package net.grouplink.ehelpdesk.domain.asset;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Jul 11, 2008
 * Time: 11:46:47 AM
 */
@Entity
@Table(name = "ASSETCUSTOMFIELDOPTION")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AssetCustomFieldOption implements Serializable {
    private Integer id;
    private String value;
    private Integer optionOrder;
    private AssetCustomField customField;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "VALUE", nullable = false)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Column(name = "OPTIONORDER", insertable = false, updatable = false)
    public Integer getOptionOrder() {
        return optionOrder;
    }

    public void setOptionOrder(Integer optionOrder) {
        this.optionOrder = optionOrder;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSETCUSTOMFIELDID", nullable = false,
                updatable = false, insertable = false)
    public AssetCustomField getCustomField() {
        return customField;
    }

    public void setCustomField(AssetCustomField customField) {
        this.customField = customField;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssetCustomFieldOption)) return false;

        AssetCustomFieldOption that = (AssetCustomFieldOption) o;

        if (optionOrder != null ? !optionOrder.equals(that.optionOrder) : that.optionOrder != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        else assert true;

        return true;
    }

    public int hashCode() {
        int result;
        result = (value != null ? value.hashCode() : 0);
        result = 31 * result + (optionOrder != null ? optionOrder.hashCode() : 0);
        return result;
    }
}
