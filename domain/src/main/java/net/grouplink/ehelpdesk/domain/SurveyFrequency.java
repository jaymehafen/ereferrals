package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Date: Feb 27, 2008
 * Time: 6:22:40 AM
 */
@Entity
@Table(name = "SURVEY_FREQUENCY")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SurveyFrequency implements Serializable {

    private Integer id;
    private String value;
    private Set<Survey> surveys = new HashSet<Survey>();


    // Getters
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    @Column(name = "VALUE")
    public String getValue() {
        return value;
    }

    @OneToMany(mappedBy = "surveyFrequency")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<Survey> getSurveys() {
        return surveys;
    }

    // Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setSurveys(Set<Survey> surveys) {
        this.surveys = surveys;
    }
}
