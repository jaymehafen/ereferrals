package net.grouplink.ehelpdesk.domain.asset;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 3, 2008
 * Time: 10:41:49 PM
 */
@Entity
@Table(name = "ASSETCUSTOMFIELD")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AssetCustomField implements Serializable {
    private Integer id;
    private String name;
    private String description;
    private String customFieldType;
    private AssetFieldGroup fieldGroup;
    private Boolean required = Boolean.FALSE;
    private Integer fieldOrder = 0;

    @SuppressWarnings("unchecked")
    private List<AssetCustomFieldOption> customFieldOptions = LazyList.decorate(new ArrayList(), FactoryUtils.instantiateFactory(AssetCustomFieldOption.class));

    public static final String VALUETYPE_TEXT = "text";
    public static final String VALUETYPE_TEXTAREA = "textarea";
    public static final String VALUETYPE_SELECT = "select";
    public static final String VALUETYPE_CHECKBOX = "checkbox";
    public static final String VALUETYPE_RADIO = "radio";
    public static final String VALUETYPE_DATE = "date";

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "ASSETCUSTOMFIELDTYPE", nullable = false)
    public String getCustomFieldType() {
        return customFieldType;
    }

    public void setCustomFieldType(String customFieldType) {
        this.customFieldType = customFieldType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSETFIELDGROUPID", nullable = false)
    public AssetFieldGroup getFieldGroup() {
        return fieldGroup;
    }

    public void setFieldGroup(AssetFieldGroup fieldGroup) {
        this.fieldGroup = fieldGroup;
    }

    @Column(name = "REQUIRED")
    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    @Column(name = "FIELDORDER")
    public Integer getFieldOrder() {
        return fieldOrder;
    }

    public void setFieldOrder(Integer fieldOrder) {
        this.fieldOrder = fieldOrder;
    }

    @OneToMany
    @JoinColumn(name = "ASSETCUSTOMFIELDID", nullable = false)
    @IndexColumn(name = "OPTIONORDER")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public List<AssetCustomFieldOption> getCustomFieldOptions() {
        return customFieldOptions;
    }

    public void setCustomFieldOptions(List<AssetCustomFieldOption> customFieldOptions) {
        this.customFieldOptions = customFieldOptions;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AssetCustomField)) return false; 

        AssetCustomField that = (AssetCustomField) o;

        if (customFieldType != null ? !customFieldType.equals(that.customFieldType) : that.customFieldType != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (fieldOrder != null ? !fieldOrder.equals(that.fieldOrder) : that.fieldOrder != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (required != null ? !required.equals(that.required) : that.required != null) return false;
        else assert true;

        return true;
    }

    public int hashCode() {
        int result;
        result = (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (customFieldType != null ? customFieldType.hashCode() : 0);
        result = 31 * result + (required != null ? required.hashCode() : 0);
        result = 31 * result + (fieldOrder != null ? fieldOrder.hashCode() : 0);
        return result;
    }
}
