package net.grouplink.ehelpdesk.domain.asset;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.apache.commons.lang.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * User: jaymehafen
 * Date: Jul 3, 2008
 * Time: 10:55:23 PM
 */
@Entity
@Table(name = "SOFTWARELICENSE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SoftwareLicense implements Serializable, Comparable {
    private Integer id;
    private String productName;
    private String productVersion;
    private Vendor vendor;
    private Integer numberOfLicenses;
    private Date purchaseDate;
    private Date supportExpirationDate;
    private String note;
    private Set<Asset> installedOnAssets = new HashSet<Asset>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "PRODUCTNAME")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Column(name = "PRODUCTVERSION")
    public String getProductVersion() {
        return productVersion;
    }

    public void setProductVersion(String productVersion) {
        this.productVersion = productVersion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VENDORID")
    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Column(name = "NUMBEROFLICENSES")
    public Integer getNumberOfLicenses() {
        return numberOfLicenses;
    }

    public void setNumberOfLicenses(Integer numberOfLicenses) {
        this.numberOfLicenses = numberOfLicenses;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "PURCHASEDATE")
    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "SUPPORTEXPDATE")
    public Date getSupportExpirationDate() {
        return supportExpirationDate;
    }

    public void setSupportExpirationDate(Date supportExpirationDate) {
        this.supportExpirationDate = supportExpirationDate;
    }

    @Lob
    @Column(name = "NOTE")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @ManyToMany(mappedBy = "softwareLicenses")
    public Set<Asset> getInstalledOnAssets() {
        return installedOnAssets;
    }

    public void setInstalledOnAssets(Set<Asset> installedOnAssets) {
        this.installedOnAssets = installedOnAssets;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SoftwareLicense)) return false;

        SoftwareLicense that = (SoftwareLicense) o;

        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (numberOfLicenses != null ? !numberOfLicenses.equals(that.numberOfLicenses) : that.numberOfLicenses != null)
            return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        if (productVersion != null ? !productVersion.equals(that.productVersion) : that.productVersion != null)
            return false;
        if (purchaseDate != null ? !purchaseDate.equals(that.purchaseDate) : that.purchaseDate != null) return false;
        if (supportExpirationDate != null ? !supportExpirationDate.equals(that.supportExpirationDate) : that.supportExpirationDate != null)
            return false;
        else
            assert true;

        return true;
    }

    public int hashCode() {
        int result;
        result = (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (productVersion != null ? productVersion.hashCode() : 0);
        result = 31 * result + (numberOfLicenses != null ? numberOfLicenses.hashCode() : 0);
        result = 31 * result + (purchaseDate != null ? purchaseDate.hashCode() : 0);
        result = 31 * result + (supportExpirationDate != null ? supportExpirationDate.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }

    public int compareTo(Object o) {
        int result = 0;
        String name2 = ((SoftwareLicense)o).getProductName();
        if(StringUtils.isNotBlank(productName) && StringUtils.isNotBlank(name2)) {
            result = String.CASE_INSENSITIVE_ORDER.compare(productName, name2);
        } else if(StringUtils.isBlank(productName)){
            result = -1;
        } else {
            result = 1;
        }
        return result;
    }
}
