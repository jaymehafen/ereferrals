package net.grouplink.ehelpdesk.domain.ticket;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.constants.JasperReportsConstants;
import org.apache.cxf.aegis.type.java5.IgnoreProperty;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Feb 18, 2008
 * Time: 4:36:28 PM
 */
@Entity
@Table(name = "TICKETFILTER")
public class TicketFilter implements Serializable, Cloneable {

    public static final String COLUMN_TICKETNUMBER = "ticketFilter.ticketNumber";
    public static final String COLUMN_PRIORITY = "ticketFilter.priority";
    public static final String COLUMN_STATUS = "ticketFilter.status";
    public static final String COLUMN_SUBJECT = "ticketFilter.subject";
    public static final String COLUMN_NOTE = "ticketFilter.note";
    public static final String COLUMN_WORKTIME = "ticketFilter.worktime";
    public static final String COLUMN_CREATED = "ticketFilter.created";
    public static final String COLUMN_MODIFIED = "ticketFilter.modified";
    public static final String COLUMN_ESTIMATED = "ticketFilter.estimated";
    public static final String COLUMN_CONTACT = "ticketFilter.contact";
    public static final String COLUMN_SUBMITTED = "ticketFilter.submitted";
    public static final String COLUMN_ASSIGNED = "ticketFilter.assigned";
    public static final String COLUMN_LOCATION = "ticketFilter.location";
    public static final String COLUMN_GROUP = "ticketFilter.group";
    public static final String COLUMN_CATEGORY = "ticketFilter.category";
    public static final String COLUMN_CATOPT = "ticketFilter.catopt";
    public static final String COLUMN_CUSTOMFIELD = "ticketFilter.customField";
    public static final String COLUMN_SURVEYS = "ticketFilter.surveys";
    public static final String COLUMN_ZENWORKS10ASSET = "ticketFilter.zenworks10Asset";
    public static final String COLUMN_INTERNALASSET = "ticketFilter.internalAsset";
    public static final String COLUMN_HISTORYSUBJECT = "ticketFilter.historySubject";
    public static final String COLUMN_HISTORYNOTE = "ticketFilter.historyNote";

    public static final String[] ALL_COLUMNS = new String[]{
            COLUMN_TICKETNUMBER,
            COLUMN_PRIORITY,
            COLUMN_STATUS,
            COLUMN_SUBJECT,
            COLUMN_NOTE,
            COLUMN_WORKTIME,
            COLUMN_CREATED,
            COLUMN_MODIFIED,
            COLUMN_ESTIMATED,
            COLUMN_CONTACT,
            COLUMN_SUBMITTED,
            COLUMN_ASSIGNED,
            COLUMN_LOCATION,
            COLUMN_GROUP,
            COLUMN_CATEGORY,
            COLUMN_CATOPT,
//            COLUMN_ZENWORKS10ASSET,
            COLUMN_INTERNALASSET,
            COLUMN_HISTORYSUBJECT,
            COLUMN_HISTORYNOTE
    };

    public static final String COLUMN_CUSTOMFIELDPREFIX = "customField.";
    public static final String COLUMN_SURVEYPREFIX = "survey.";

    public static final Integer CURRENT_TF_VERSION = 2;

    private Integer id;

    private User user;
    private String name;
    private Boolean privateFilter = Boolean.TRUE;

    //Report variables
    private Boolean report = Boolean.FALSE;
    private Boolean mainReport = Boolean.TRUE;
    private Boolean showLink = Boolean.TRUE;
    private Integer groupBy = JasperReportsConstants.TECHNICIAN;
    private Boolean reGroup = Boolean.FALSE;
    private Integer groupByBy = JasperReportsConstants.TECHNICIAN;
    private Boolean showDetail = Boolean.FALSE;
    private Boolean showGraph = Boolean.TRUE;
    private Boolean showPieChart = Boolean.TRUE;
    private String format = "html";
    private Integer groupByOrder = JasperReportsConstants.GROUP_BY_ORDER_ALPHA;
    private Integer showTop;
    private Boolean showReportByDate = Boolean.FALSE;
    private Integer byDate = 2;

    private Integer[] ticketNumbers;
    private Integer[] ticketNumbersOps;

    private Integer[] priorityIds;
    private Integer[] priorityIdsOps;

    private Integer[] statusIds;
    private Integer[] statusIdsOps;

    private Integer[] contactIds;
    private Integer[] contactIdsOps;
    private Integer[] submittedByIds;
    private Integer[] submittedByIdsOps;
    private Integer[] assignedToIds;
    private Integer[] assignedToIdsOps;

    // Adding additional fields for new assigned to drop downs
    private Integer[] assignedToUserIds;
    private Integer[] assignedToUserIdsOps;

    private Integer[] locationIds;
    private Integer[] locationIdsOps;
    private Integer[] groupIds;
    private Integer[] groupIdsOps;
    private Integer[] categoryIds;
    private Integer[] categoryIdsOps;
    private Integer[] categoryOptionIds;
    private Integer[] categoryOptionIdsOps;

    private String[] subject;
    private Integer[] subjectOps;
    private String[] note;
    private Integer[] noteOps;

    private String[] historySubject;
    private Integer[] historySubjectOps;
    private String[] historyNote;
    private Integer[] historyNoteOps;

    private Integer createdDateOperator;
    private String createdDate;
    private String createdDate2;
    private Integer createdDateLastX;

    private Integer modifiedDateOperator;
    private String modifiedDate;
    private String modifiedDate2;
    private Integer modifiedDateLastX;

    private Integer estimatedDateOperator;
    private String estimatedDate;
    private String estimatedDate2;
    private Integer estimatedDateLastX;

    private String workTimeOperator;
    private String workTimeHours;
    private String workTimeMinutes;

    private String[] columnOrder;

    private String[] customFieldName;
    private String[] customFieldValue;

    private String[] surveyQuestionId;
    private String[] surveyQuestionValue;

    private Map<String, List<TicketFilterCriteriaHelper>> surveys = new HashMap<String, List<TicketFilterCriteriaHelper>>();

    private Integer[] zenAssetIds;
    private Integer[] zenAssetIdOps;

    private Integer[] internalAssetIds;
    private Integer[] internalAssetIdOps;

    private Integer tfVersion;

    private Map<String, List<TicketFilterCriteriaHelper>> customFields = new  HashMap<String, List<TicketFilterCriteriaHelper>>();


    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @IgnoreProperty
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "PRIVATE")
    public Boolean getPrivateFilter() {
        if (privateFilter == null) {
            privateFilter = Boolean.TRUE;
        }
        
        return privateFilter;
    }

    public void setPrivateFilter(Boolean privateFilter) {
        if (privateFilter == null) {
            this.privateFilter = Boolean.TRUE;
        } else {
            this.privateFilter = privateFilter;
        }
    }

    @Column(name = "TICKETNUMBERS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getTicketNumbers() {
        return ticketNumbers;
    }

    public void setTicketNumbers(Integer[] ticketNumbers) {
        this.ticketNumbers = ticketNumbers;
    }

    @Column(name = "PRIORITYIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getPriorityIds() {
        return priorityIds;
    }

    public void setPriorityIds(Integer[] priorityIds) {
        this.priorityIds = priorityIds;
    }

    @Column(name = "STATUSIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getStatusIds() {
        return statusIds;
    }

    public void setStatusIds(Integer[] statusIds) {
        this.statusIds = statusIds;
    }

    @Column(name = "CONTACTIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getContactIds() {
        return contactIds;
    }

    public void setContactIds(Integer[] contactIds) {
        this.contactIds = contactIds;
    }

    @Column(name = "SUBBYIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getSubmittedByIds() {
        return submittedByIds;
    }

    public void setSubmittedByIds(Integer[] submittedByIds) {
        this.submittedByIds = submittedByIds;
    }

    @Column(name = "ASSTOIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getAssignedToIds() {
        return assignedToIds;
    }

    public void setAssignedToIds(Integer[] assignedToIds) {
        this.assignedToIds = assignedToIds;
    }

    @Column(name = "LOCIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getLocationIds() {
        return locationIds;
    }

    public void setLocationIds(Integer[] locationIds) {
        this.locationIds = locationIds;
    }

    @Column(name = "GROUPIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(Integer[] groupIds) {
        this.groupIds = groupIds;
    }

    @Column(name =  "CATIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Integer[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    @Column(name = "CATOPTIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getCategoryOptionIds() {
        return categoryOptionIds;
    }

    public void setCategoryOptionIds(Integer[] categoryOptionIds) {
        this.categoryOptionIds = categoryOptionIds;
    }

    @Column(name = "SUBJECT", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getSubject() {
        return subject;
    }

    public void setSubject(String[] subject) {
        this.subject = subject;
    }

    @Column(name = "NOTE", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getNote() {
        return note;
    }

    public void setNote(String[] note) {
        this.note = note;
    }

    @Column(name = "CREATEDDATEOP")
    public Integer getCreatedDateOperator() {
        return createdDateOperator;
    }

    public void setCreatedDateOperator(Integer createdDateOperator) {
        this.createdDateOperator = createdDateOperator;
    }

    @Column(name = "CREATIONDATE")
    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "CREATIONDATE2")
    public String getCreatedDate2() {
        return createdDate2;
    }

    public void setCreatedDate2(String createdDate2) {
        this.createdDate2 = createdDate2;
    }

    @Column(name = "MODIFIEDDATEOP")
    public Integer getModifiedDateOperator() {
        return modifiedDateOperator;
    }

    public void setModifiedDateOperator(Integer modifiedDateOperator) {
        this.modifiedDateOperator = modifiedDateOperator;
    }

    @Column(name = "MODIFIEDDATE")
    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Column(name = "MODIFIEDDATE2")
    public String getModifiedDate2() {
        return modifiedDate2;
    }

    public void setModifiedDate2(String modifiedDate2) {
        this.modifiedDate2 = modifiedDate2;
    }

    @Column(name = "ESTIMATEDDATEOP")
    public Integer getEstimatedDateOperator() {
        return estimatedDateOperator;
    }

    public void setEstimatedDateOperator(Integer estimatedDateOperator) {
        this.estimatedDateOperator = estimatedDateOperator;
    }

    @Column(name = "ESTIMATEDDATE")
    public String getEstimatedDate() {
        return estimatedDate;
    }

    public void setEstimatedDate(String estimatedDate) {
        this.estimatedDate = estimatedDate;
    }

    @Column(name = "ESTIMATEDDATE2")
    public String getEstimatedDate2() {
        return estimatedDate2;
    }

    public void setEstimatedDate2(String estimatedDate2) {
        this.estimatedDate2 = estimatedDate2;
    }

    @Column(name = "WORKTIMEOP")
    public String getWorkTimeOperator() {
        return workTimeOperator;
    }

    public void setWorkTimeOperator(String workTimeOperator) {
        this.workTimeOperator = workTimeOperator;
    }

    @Column(name = "WORKTIMEHOURS")
    public String getWorkTimeHours() {
        return workTimeHours;
    }

    public void setWorkTimeHours(String workTimeHours) {
        this.workTimeHours = workTimeHours;
    }

    @Column(name = "WORKTIMEMINUTES")
    public String getWorkTimeMinutes() {
        return workTimeMinutes;
    }

    public void setWorkTimeMinutes(String workTimeMinutes) {
        this.workTimeMinutes = workTimeMinutes;
    }

    @Column(name = "COLORDER", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getColumnOrder() {
        return columnOrder;
    }

    public void setColumnOrder(String[] columnOrder) {
        this.columnOrder = columnOrder;
    }

    @Column(name = "CUSTFIELDNAME", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getCustomFieldName() {
        return customFieldName;
    }

    public void setCustomFieldName(String[] customFieldName) {
        this.customFieldName = customFieldName;
    }

    @Column(name = "CUSTFIELDVALUE", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getCustomFieldValue() {
        return customFieldValue;
    }

    public void setCustomFieldValue(String[] customFieldValue) {
        this.customFieldValue = customFieldValue;
    }

    @Column(name = "HISTORYSUBJECT", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getHistorySubject() {
        return historySubject;
    }

    public void setHistorySubject(String[] historySubject) {
        this.historySubject = historySubject;
    }

    @Column(name = "HISTORYNOTE", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getHistoryNote() {
        return historyNote;
    }

    public void setHistoryNote(String[] historyNote) {
        this.historyNote = historyNote;
    }

    @Column(name = "SURVEYQUESTIONID", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getSurveyQuestionId() {
        return surveyQuestionId;
    }

    public void setSurveyQuestionId(String[] surveyQuestionId) {
        this.surveyQuestionId = surveyQuestionId;
    }

    @Column(name = "SURVEYQUESTIONVALUE", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public String[] getSurveyQuestionValue() {
        return surveyQuestionValue;
    }

    public void setSurveyQuestionValue(String[] surveyQuestionValue) {
        this.surveyQuestionValue = surveyQuestionValue;
    }

    public TicketFilter clone() throws CloneNotSupportedException {
        return (TicketFilter)super.clone();
    }

    @Column(name = "ISREPORT")
    public Boolean getReport() {
        if (report == null) {
            report = Boolean.FALSE;
        }

        return report;
    }

    public void setReport(Boolean report) {
        if (report == null) {
            this.report = Boolean.FALSE;
        } else {
            this.report = report;
        }
    }

    @Column(name = "ISMAINREPORT")
    public Boolean getMainReport() {
        if (mainReport == null) {
            mainReport = Boolean.TRUE;
        }

        return mainReport;
    }

    public void setMainReport(Boolean mainReport) {
        if (mainReport == null) {
            this.mainReport = Boolean.TRUE;
        } else {
            this.mainReport = mainReport;
        }
    }

    @Column(name = "SHOWLINK")
    public Boolean getShowLink() {
        if (showLink == null) {
            showLink = Boolean.TRUE;
        }

        return showLink;
    }

    public void setShowLink(Boolean showLink) {
        if (showLink == null) {
            this.showLink = Boolean.TRUE;
        } else {
            this.showLink = showLink;
        }
    }

    @Column(name = "GROUPBY")
    public Integer getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(Integer groupBy) {
        this.groupBy = groupBy;
    }

    @Column(name = "REGROUP")
    public Boolean getReGroup() {
        if (reGroup == null) {
            reGroup = Boolean.FALSE;
        }

        return reGroup;
    }

    public void setReGroup(Boolean reGroup) {
        if (reGroup == null) {
            this.reGroup = Boolean.FALSE;
        } else {
            this.reGroup = reGroup;
        }
    }

    @Column(name = "GROUPBYBY")
    public Integer getGroupByBy() {
        return groupByBy;
    }

    public void setGroupByBy(Integer groupByBy) {
        this.groupByBy = groupByBy;
    }

    @Column(name = "SHOWDETAIL")
    public Boolean getShowDetail() {
        if (showDetail == null) {
            showDetail = Boolean.FALSE;
        }

        return showDetail;
    }

    public void setShowDetail(Boolean showDetail) {
        if (showDetail == null) {
            this.showDetail = Boolean.FALSE;
        } else {
            this.showDetail = showDetail;
        }
    }

    @Column(name = "SHOWGRAPH")
    public Boolean getShowGraph() {
        if (showGraph == null) {
            showGraph = Boolean.TRUE;
        }

        return showGraph;
    }

    public void setShowGraph(Boolean showGraph) {
        if (showGraph == null) {
            this.showGraph = Boolean.TRUE;
        } else {
            this.showGraph = showGraph;
        }
    }

    @Column(name = "SHOWPIECHART")
    public Boolean getShowPieChart() {
        if (showPieChart == null) {
            showPieChart = Boolean.TRUE;
        }

        return showPieChart;
    }

    public void setShowPieChart(Boolean showPieChart) {
        if (showPieChart == null) {
            this.showPieChart = Boolean.TRUE;
        } else {
            this.showPieChart = showPieChart;
        }
    }

    @Column(name = "FORMAT")
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Transient
    public Boolean getShowReportByDate() {
        if (showReportByDate == null) {
            showReportByDate = Boolean.FALSE;
        }

        return showReportByDate;
    }

    public void setShowReportByDate(Boolean showReportByDate) {
        if (showReportByDate == null) {
            this.showReportByDate = Boolean.FALSE;
        } else {
            this.showReportByDate = showReportByDate;
        }
    }

    @Transient
    public Integer getByDate() {
        return byDate;
    }

    public void setByDate(Integer byDate) {
        this.byDate = byDate;
    }

    @Column(name = "ZENASSETIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getZenAssetIds() {
        return zenAssetIds;
    }

    public void setZenAssetIds(Integer[] zenAssetIds) {
        this.zenAssetIds = zenAssetIds;
    }

    @Column(name = "ZENASSETOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getZenAssetIdOps() {
        return zenAssetIdOps;
    }

    public void setZenAssetIdOps(Integer[] zenAssetIdOps) {
        this.zenAssetIdOps = zenAssetIdOps;
    }

    @Column(name = "INTERNALASSETIDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getInternalAssetIds() {
        return internalAssetIds;
    }

    public void setInternalAssetIds(Integer[] internalAssetIds) {
        this.internalAssetIds = internalAssetIds;
    }

    @Column(name = "INTERNALASSETOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getInternalAssetIdOps() {
        return internalAssetIdOps;
    }

    public void setInternalAssetIdOps(Integer[] internalAssetIdOps) {
        this.internalAssetIdOps = internalAssetIdOps;
    }

    @Column(name = "GROUPBYORDER")
    public Integer getGroupByOrder() {
        return groupByOrder;
    }

    public void setGroupByOrder(Integer groupByOrder) {
        this.groupByOrder = groupByOrder;
    }

    @Column(name = "SHOWTOP")
    public Integer getShowTop() {
        return showTop;
    }

    public void setShowTop(Integer showTop) {
        this.showTop = showTop;
    }

    @Column(name = "TICKETNUMBEROPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getTicketNumbersOps() {
        return ticketNumbersOps;
    }

    public void setTicketNumbersOps(Integer[] ticketNumbersOps) {
        this.ticketNumbersOps = ticketNumbersOps;
    }

    @Column(name = "PRIORITYOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getPriorityIdsOps() {
        return priorityIdsOps;
    }

    public void setPriorityIdsOps(Integer[] priorityIdsOps) {
        this.priorityIdsOps = priorityIdsOps;
    }

    @Column(name = "STATUSOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getStatusIdsOps() {
        return statusIdsOps;
    }

    public void setStatusIdsOps(Integer[] statusIdsOps) {
        this.statusIdsOps = statusIdsOps;
    }

    @Column(name = "CONTACTOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getContactIdsOps() {
        return contactIdsOps;
    }

    public void setContactIdsOps(Integer[] contactIdsOps) {
        this.contactIdsOps = contactIdsOps;
    }

    @Column(name = "SUBMITTEDBYOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getSubmittedByIdsOps() {
        return submittedByIdsOps;
    }

    public void setSubmittedByIdsOps(Integer[] submittedByIdsOps) {
        this.submittedByIdsOps = submittedByIdsOps;
    }

    @Column(name = "ASSIGNEDTOOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getAssignedToIdsOps() {
        return assignedToIdsOps;
    }

    public void setAssignedToIdsOps(Integer[] assignedToIdsOps) {
        this.assignedToIdsOps = assignedToIdsOps;
    }

    @Column(name = "LOCATIONOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getLocationIdsOps() {
        return locationIdsOps;
    }

    public void setLocationIdsOps(Integer[] locationIdsOps) {
        this.locationIdsOps = locationIdsOps;
    }

    @Column(name = "GROUPOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getGroupIdsOps() {
        return groupIdsOps;
    }

    public void setGroupIdsOps(Integer[] groupIdsOps) {
        this.groupIdsOps = groupIdsOps;
    }

    @Column(name = "CATEGORYOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getCategoryIdsOps() {
        return categoryIdsOps;
    }

    public void setCategoryIdsOps(Integer[] categoryIdsOps) {
        this.categoryIdsOps = categoryIdsOps;
    }

    @Column(name = "CATOPTSOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getCategoryOptionIdsOps() {
        return categoryOptionIdsOps;
    }

    public void setCategoryOptionIdsOps(Integer[] categoryOptionIdsOps) {
        this.categoryOptionIdsOps = categoryOptionIdsOps;
    }

    @Column(name = "SUBJECTOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getSubjectOps() {
        return subjectOps;
    }

    public void setSubjectOps(Integer[] subjectOps) {
        this.subjectOps = subjectOps;
    }

    @Column(name = "NOTEOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getNoteOps() {
        return noteOps;
    }

    public void setNoteOps(Integer[] noteOps) {
        this.noteOps = noteOps;
    }

    @Column(name = "HISTSUBJOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getHistorySubjectOps() {
        return historySubjectOps;
    }

    public void setHistorySubjectOps(Integer[] historySubjectOps) {
        this.historySubjectOps = historySubjectOps;
    }

    @Column(name = "HISTNOTEOPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getHistoryNoteOps() {
        return historyNoteOps;
    }

    public void setHistoryNoteOps(Integer[] historyNoteOps) {
        this.historyNoteOps = historyNoteOps;
    }

    @Column(name = "CREATEDDATELASTX")
    public Integer getCreatedDateLastX() {
        return createdDateLastX;
    }

    public void setCreatedDateLastX(Integer createdDateLastX) {
        this.createdDateLastX = createdDateLastX;
    }

    @Column(name = "MODDATELASTX")
    public Integer getModifiedDateLastX() {
        return modifiedDateLastX;
    }

    public void setModifiedDateLastX(Integer modifiedDateLastX) {
        this.modifiedDateLastX = modifiedDateLastX;
    }

    @Column(name = "ESTDATELASTX")
    public Integer getEstimatedDateLastX() {
        return estimatedDateLastX;
    }

    public void setEstimatedDateLastX(Integer estimatedDateLastX) {
        this.estimatedDateLastX = estimatedDateLastX;
    }

    @Column(name = "CUSTOMFIELDS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Map<String, List<TicketFilterCriteriaHelper>> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<String, List<TicketFilterCriteriaHelper>> customFields) {
        this.customFields = customFields;
    }

    @Column(name = "SURVEYS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Map<String, List<TicketFilterCriteriaHelper>> getSurveys() {
        return surveys;
    }

    public void setSurveys(Map<String, List<TicketFilterCriteriaHelper>> surveys) {
        this.surveys = surveys;
    }

    @Column(name = "TFVERSION")
    public Integer getTfVersion() {
        return tfVersion;
    }

    public void setTfVersion(Integer tfVersion) {
        this.tfVersion = tfVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketFilter that = (TicketFilter) o;

        if (!Arrays.equals(assignedToIds, that.assignedToIds)) return false;
        if (!Arrays.equals(assignedToIdsOps, that.assignedToIdsOps)) return false;
        if (!Arrays.equals(categoryIds, that.categoryIds)) return false;
        if (!Arrays.equals(categoryIdsOps, that.categoryIdsOps)) return false;
        if (!Arrays.equals(categoryOptionIds, that.categoryOptionIds)) return false;
        if (!Arrays.equals(categoryOptionIdsOps, that.categoryOptionIdsOps)) return false;
        if (!Arrays.equals(columnOrder, that.columnOrder)) return false;
        if (!Arrays.equals(contactIds, that.contactIds)) return false;
        if (!Arrays.equals(contactIdsOps, that.contactIdsOps)) return false;
        if (createdDate != null ? !createdDate.equals(that.createdDate) : that.createdDate != null) return false;
        if (createdDate2 != null ? !createdDate2.equals(that.createdDate2) : that.createdDate2 != null) return false;
        if (createdDateLastX != null ? !createdDateLastX.equals(that.createdDateLastX) : that.createdDateLastX != null)
            return false;
        if (createdDateOperator != null ? !createdDateOperator.equals(that.createdDateOperator) : that.createdDateOperator != null)
            return false;
        if (!Arrays.equals(customFieldName, that.customFieldName)) return false;
        if (!Arrays.equals(customFieldValue, that.customFieldValue)) return false;
        if (estimatedDate != null ? !estimatedDate.equals(that.estimatedDate) : that.estimatedDate != null)
            return false;
        if (estimatedDate2 != null ? !estimatedDate2.equals(that.estimatedDate2) : that.estimatedDate2 != null)
            return false;
        if (estimatedDateLastX != null ? !estimatedDateLastX.equals(that.estimatedDateLastX) : that.estimatedDateLastX != null)
            return false;
        if (estimatedDateOperator != null ? !estimatedDateOperator.equals(that.estimatedDateOperator) : that.estimatedDateOperator != null)
            return false;
        if (!Arrays.equals(groupIds, that.groupIds)) return false;
        if (!Arrays.equals(groupIdsOps, that.groupIdsOps)) return false;
        if (!Arrays.equals(historyNote, that.historyNote)) return false;
        if (!Arrays.equals(historyNoteOps, that.historyNoteOps)) return false;
        if (!Arrays.equals(historySubject, that.historySubject)) return false;
        if (!Arrays.equals(historySubjectOps, that.historySubjectOps)) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!Arrays.equals(locationIds, that.locationIds)) return false;
        if (!Arrays.equals(locationIdsOps, that.locationIdsOps)) return false;
        if (modifiedDate != null ? !modifiedDate.equals(that.modifiedDate) : that.modifiedDate != null) return false;
        if (modifiedDate2 != null ? !modifiedDate2.equals(that.modifiedDate2) : that.modifiedDate2 != null)
            return false;
        if (modifiedDateLastX != null ? !modifiedDateLastX.equals(that.modifiedDateLastX) : that.modifiedDateLastX != null)
            return false;
        if (modifiedDateOperator != null ? !modifiedDateOperator.equals(that.modifiedDateOperator) : that.modifiedDateOperator != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (!Arrays.equals(note, that.note)) return false;
        if (!Arrays.equals(noteOps, that.noteOps)) return false;
        if (!Arrays.equals(priorityIds, that.priorityIds)) return false;
        if (!Arrays.equals(priorityIdsOps, that.priorityIdsOps)) return false;
        if (privateFilter != null ? !privateFilter.equals(that.privateFilter) : that.privateFilter != null)
            return false;
        if (!Arrays.equals(statusIds, that.statusIds)) return false;
        if (!Arrays.equals(statusIdsOps, that.statusIdsOps)) return false;
        if (!Arrays.equals(subject, that.subject)) return false;
        if (!Arrays.equals(subjectOps, that.subjectOps)) return false;
        if (!Arrays.equals(submittedByIds, that.submittedByIds)) return false;
        if (!Arrays.equals(submittedByIdsOps, that.submittedByIdsOps)) return false;
        if (!Arrays.equals(surveyQuestionId, that.surveyQuestionId)) return false;
        if (!Arrays.equals(surveyQuestionValue, that.surveyQuestionValue)) return false;
        if (tfVersion != null ? !tfVersion.equals(that.tfVersion) : that.tfVersion != null) return false;
        if (!Arrays.equals(ticketNumbers, that.ticketNumbers)) return false;
        if (!Arrays.equals(ticketNumbersOps, that.ticketNumbersOps)) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        if (workTimeHours != null ? !workTimeHours.equals(that.workTimeHours) : that.workTimeHours != null)
            return false;
        if (workTimeMinutes != null ? !workTimeMinutes.equals(that.workTimeMinutes) : that.workTimeMinutes != null)
            return false;
        if (workTimeOperator != null ? !workTimeOperator.equals(that.workTimeOperator) : that.workTimeOperator != null)
            return false;
        if (!Arrays.equals(zenAssetIdOps, that.zenAssetIdOps)) return false;
        if (!Arrays.equals(zenAssetIds, that.zenAssetIds)) return false;

        return true;
    }

    @Column(name = "ASSIGNEDTOUSER", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getAssignedToUserIds() {
        return assignedToUserIds;
    }

    public void setAssignedToUserIds(Integer[] assignedToUserIds) {
        this.assignedToUserIds = assignedToUserIds;
    }

    @Column(name = "ASSIGNEDTOUSEROPS", length = 2000)
    @Type(type = "serializable", parameters = @Parameter(name = "length", value = "2000"))
    public Integer[] getAssignedToUserIdsOps() {
        return assignedToUserIdsOps;
    }

    public void setAssignedToUserIdsOps(Integer[] assignedToUserIdsOps) {
        this.assignedToUserIdsOps = assignedToUserIdsOps;
    }
}
