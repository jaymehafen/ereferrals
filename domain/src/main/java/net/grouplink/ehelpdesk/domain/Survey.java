package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Date: Feb 21, 2008
 * Time: 5:52:56 PM
 */
@Entity
@Table(name = "SURVEYS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Survey implements Serializable {

    private Integer id;
    private String name;
    private Set<CategoryOption> categoryOptions = new HashSet<CategoryOption>();
    private SurveyFrequency surveyFrequency;
    private Boolean active;
    private Set<SurveyQuestion> surveyQuestions = new HashSet<SurveyQuestion>();
    
    public static final String DEFAULT = "Default Survey";

    // Getters
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "survey")
    public Set<CategoryOption> getCategoryOptions() {
        return categoryOptions;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEYFREQUENCYID")
    public SurveyFrequency getSurveyFrequency() {
        return surveyFrequency;
    }

    @Column(name = "ACTIVE")
    public Boolean getActive() {
        return active;
    }

    @OneToMany(mappedBy = "survey")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<SurveyQuestion> getSurveyQuestions() {
        return surveyQuestions;
    }

    // Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategoryOptions(Set<CategoryOption> categoryOptions) {
        this.categoryOptions = categoryOptions;
    }

    public void setSurveyFrequency(SurveyFrequency surveyFrequency) {
        this.surveyFrequency = surveyFrequency;
    }

    public void setActive(Boolean isActive) {
        this.active = isActive;
    }

    public void setSurveyQuestions(Set<SurveyQuestion> surveyQuestions) {
        this.surveyQuestions = surveyQuestions;
    }


}
