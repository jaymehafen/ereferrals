package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a <code>Role</code> in the eHelpDesk system.
 *
 * @author Mark Rollins
 */
@Entity
@Table(name = "ROLL")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Role implements Serializable {

    public static final Integer ROLE_ADMINISTRATOR_ID = 1;
    public static final Integer ROLE_MANAGER_ID = 2;
    public static final Integer ROLE_TECHNICIAN_ID = 3;
    public static final Integer ROLE_USER_ID = 4;
    public static final Integer ROLE_MEMBER_ID = 5;
    public static final Integer ROLE_WFPARTICIPANT_ID = 6;

    private Integer id;
    private String name;

    private Set<UserRole> userRoles = new HashSet<UserRole>();

    /**
     * Default constructor. Creates a new <code>Role</code>
     */
    public Role() {
    }

    /**
     * Constructor specifying name of role
     *
     * @param name the name or description of this role
     */
    public Role(String name) {
        this.name = name;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "role")
    @Cascade(value = {CascadeType.SAVE_UPDATE, CascadeType.PERSIST})
    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    @Column(name = "NAME", length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
