package net.grouplink.ehelpdesk.domain.asset;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * User: jaymehafen
 * Date: Jul 3, 2008
 * Time: 10:52:57 PM
 */
@Entity
@Table(name = "ASSETCUSTOMFIELDVALUE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AssetCustomFieldValue implements Serializable {
    private Integer id;
    private AssetCustomField customField;
    private String fieldValue;
    private String fieldValueText;
    private Asset asset;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMFIELDID", nullable = false)
    public AssetCustomField getCustomField() {
        return customField;
    }

    public void setCustomField(AssetCustomField customField) {
        this.customField = customField;
    }

    @Column(name = "FIELDVALUE")
    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Lob
    @Column(name = "FIELDVALUETEXT")
    public String getFieldValueText() {
        return fieldValueText;
    }

    public void setFieldValueText(String fieldValueText) {
        this.fieldValueText = fieldValueText;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSETID", nullable = false)
    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }
}
