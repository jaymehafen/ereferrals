package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.acl.PermissionScheme;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.OrderBy;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a <code>Group</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "GROUPS")
@FilterDef(name = "deletedObject", parameters = @ParamDef(name = "isActive", type = "boolean"))
@Filter(name = "deletedObject", condition = "ACTIVE = :isActive")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Group implements Serializable, Comparable {

    public static final String ASSETTRACKERTYPE_EHD = "EHD";
    public static final String ASSETTRACKERTYPE_ZEN10 = "ZEN10";
    public static final String ASSETTRACKERTYPE_NONE = "NONE";

    private Integer id;
    private String name;
    private String comments;
    private boolean active = true;
    private boolean showAllGroupTickets = true;
    private String assetTrackerType = ASSETTRACKERTYPE_NONE;

    private Set<Category> categories = new HashSet<Category>();
    private Set<KnowledgeBase> knowledgebases = new HashSet<KnowledgeBase>();
    private Set<ScheduleStatus> scheduleStatus = new HashSet<ScheduleStatus>();
    private Set<UserRoleGroup> userRoleGroups = new HashSet<UserRoleGroup>();
    private Set<TicketTemplateMaster> templateMasters = new HashSet<TicketTemplateMaster>();
    private PermissionScheme permissionScheme;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    @Field(index = Index.TOKENIZED, store = Store.NO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Transient
    public String getJsEscapeName(){
        return StringEscapeUtils.escapeJavaScript(name);
    }

    @OneToMany(mappedBy = "group")
    public Set<KnowledgeBase> getKnowledgebases() {
        return knowledgebases;
    }

    public void setKnowledgebases(Set<KnowledgeBase> knowledgebases) {
        this.knowledgebases = knowledgebases;
    }

    @OneToMany(mappedBy = "group")
    public Set<ScheduleStatus> getScheduleStatus() {
        return scheduleStatus;
    }

    public void setScheduleStatus(Set<ScheduleStatus> scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    @OneToMany(mappedBy = "group")
    @Cascade(CascadeType.ALL)
    public Set<UserRoleGroup> getUserRoleGroups() {
        return userRoleGroups;
    }

    public void setUserRoleGroups(Set<UserRoleGroup> userRoleGroups) {
        this.userRoleGroups = userRoleGroups;
    }

    @Transient
    public List<UserRoleGroup> getUserRoleGroupList(){
        List<UserRoleGroup> urgList = new ArrayList<UserRoleGroup>(userRoleGroups);
        Collections.sort(urgList);
        return urgList;
    }

    @Column(name = "NAME")
    @Field(index = Index.TOKENIZED, store = Store.NO)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "group")
    @Cascade(value = CascadeType.ALL)
    @OrderBy(clause = "NAME asc")
    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    @Transient
    public List<Category> getCategoryList() {
        List<Category> cat = new ArrayList<Category>(categories);
        Collections.sort(cat);
        return cat;
    }

    @Column(name = "COMMENTS")
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name = "ACTIVE")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @deprecated use permission 'group.viewAllGroupTickets' instead
     */
    @Column(name = "SHOWALLGROUPTICKETS")
    public boolean isShowAllGroupTickets() {
        return showAllGroupTickets;
    }

    public void setShowAllGroupTickets(boolean showAllGroupTickets) {
        this.showAllGroupTickets = showAllGroupTickets;
    }

    @Column(name = "ASSETTRACKERTYPE")
    public String getAssetTrackerType() {
        return assetTrackerType;
    }

    public void setAssetTrackerType(String assetTrackerType) {
        this.assetTrackerType = assetTrackerType;
    }

    @OneToMany(mappedBy = "group")
    @Filter(name = "deletedObject", condition = "(ACTIVE is null or ACTIVE = :isActive)")
    @OrderBy(clause = "NAME asc")
    public Set<TicketTemplateMaster> getTemplateMasters() {
        return templateMasters;
    }

    public void setTemplateMasters(Set<TicketTemplateMaster> templateMasters) {
        this.templateMasters = templateMasters;
    }

    @ManyToOne
    @JoinColumn(name = "PERMISSIONSCHEMEID")
    public PermissionScheme getPermissionScheme() {
        return permissionScheme;
    }

    public void setPermissionScheme(PermissionScheme permissionScheme) {
        this.permissionScheme = permissionScheme;
    }

    public int compareTo(Object o) {
        return String.CASE_INSENSITIVE_ORDER.compare(name, ((Group) o).getName());
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", comments='" + comments + '\'' +
                ", active=" + active +
                ", showAllGroupTickets=" + showAllGroupTickets +
                ", assetTrackerType='" + assetTrackerType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        if (active != group.active) return false;
        if (showAllGroupTickets != group.showAllGroupTickets) return false;
        if (assetTrackerType != null ? !assetTrackerType.equals(group.assetTrackerType) : group.assetTrackerType != null)
            return false;
        if (comments != null ? !comments.equals(group.comments) : group.comments != null) return false;
        if (name != null ? !name.equals(group.name) : group.name != null) return false;
        else assert true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        result = 31 * result + (showAllGroupTickets ? 1 : 0);
        result = 31 * result + (assetTrackerType != null ? assetTrackerType.hashCode() : 0);
        return result;
    }
}
