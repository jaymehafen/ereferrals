package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Date: Feb 27, 2008
 * Time: 7:32:11 AM
 */
@Entity
@Table(name = "SURVEY_QUESTION")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SurveyQuestion implements Serializable {

    private Integer id;

    private int displayOrder;
    private int surveyQuestionTypeId;
    private Survey survey;
    private String surveyQuestionText;
    private Set<SurveyQuestionResponse> surveyQuestionResponses = new HashSet<SurveyQuestionResponse>();

    // Getters
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    @Column(name = "SURVEYTYPEID")
    public int getSurveyQuestionTypeId() {
        return surveyQuestionTypeId;
    }

    @Column(name = "DISPLAYORDER")
    public int getDisplayOrder() {
        return displayOrder;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SURVEYID")
    public Survey getSurvey() {
        return survey;
    }

    @Column(name = "SURVEYQUESTIONTEXT")
    public String getSurveyQuestionText() {
        return surveyQuestionText;
    }

    @OneToMany(mappedBy = "surveyQuestion")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<SurveyQuestionResponse> getSurveyQuestionResponses() {
        return surveyQuestionResponses;
    }

    // Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setSurveyQuestionTypeId(int surveyQuestionTypeId) {
        this.surveyQuestionTypeId = surveyQuestionTypeId;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public void setSurveyQuestionText(String surveyQuestionText) {
        this.surveyQuestionText = surveyQuestionText;
    }

    public void setSurveyQuestionResponses(Set<SurveyQuestionResponse> surveyQuestionResponses) {
        this.surveyQuestionResponses = surveyQuestionResponses;
    }
}
