package net.grouplink.ehelpdesk.domain.utils;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Comparator;

/**
 * Date: Aug 24, 2008
 * Time: 5:47:16 PM
 */
public class UserRoleGroupComparator implements Comparator<UserRoleGroup> {

    protected final Log logger = LogFactory.getLog(getClass());

    public int compare(UserRoleGroup urg1, UserRoleGroup urg2) {
        // Get the Group for each UserRoleGroup
        Group grp1 = urg1.getGroup();
        Group grp2 = urg2.getGroup();

        // Get the name for each Group
        String gName1 = grp1.getName();
        String gName2 = grp2.getName();

//        logger.debug("Group Names:  " + gName1 + ", " + gName2);

        // Compare the group names
        int compare1 = gName1.compareTo(gName2);
//        logger.debug("Comp1: " + compare1);
        if (compare1 != 0)
            return compare1;
        else {
            // Group names are the same, so now get UserRoles
            UserRole ur1 = urg1.getUserRole();
            UserRole ur2 = urg2.getUserRole();

//            logger.debug("UR1: " + ur1);
//            logger.debug("UR2: " + ur2);

            if (ur1 == null) {
                if (ur2 == null)
                    return 0;
                else {
//                    logger.debug("The first is null.");
                    return -1;
                }
            }

            if (ur2 == null) {
                return 1;
            }

            // Get the User for each UserRole
            User user1 = ur1.getUser();
            User user2 = ur2.getUser();

            // Get the last name for each user
            String ulName1 = user1.getLastName();
            String ulName2 = user2.getLastName();

//            logger.debug("Last Names: " + ulName1 + ", " + ulName2);

            // Compare the last names
            int compare2 = ulName1.compareTo(ulName2);
//            logger.debug("Comp2: " + compare2);
            if (compare2 != 0)
                return compare2;
            else {
                // User last names were the same, so now get first names
                String ufName1 = user1.getFirstName();
                String ufName2 = user2.getFirstName();

//                logger.debug("First Names: " + ufName1 + ", " + ufName2);

                return ufName1.compareTo(ufName2);
            }
        }
    }
}
