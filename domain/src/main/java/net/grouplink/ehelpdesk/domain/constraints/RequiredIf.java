package net.grouplink.ehelpdesk.domain.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * author: zpearce
 * Date: 12/9/11
 * Time: 1:44 PM
 */
@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = RequiredIfValidator.class)
public @interface RequiredIf {
    String property() default "";

    String dependentProperty() default "";

    Class<?>[] constraints() default {};

    Class<?>[] groups() default {};

    String message() default "{net.grouplink.ehelpdesk.domain.constraints.RequiredIf.message}";

    Class<? extends Payload>[] payload() default {};

    @Target({TYPE})
    @Retention(RUNTIME)
    public @interface List {
        RequiredIf[] value();
    }
}
