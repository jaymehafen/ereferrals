package net.grouplink.ehelpdesk.domain.utils;

import net.grouplink.ehelpdesk.domain.Group;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Nov 24, 2008
 * Time: 12:12:49 PM
 */
public class GroupComparator implements Comparator<Group> {

    public int compare(Group grp1, Group grp2) {
        return grp1.getName().compareTo(grp2.getName());
    }

}
