package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author mrollins
 * @version 1.0
 */
@Entity
@Table(name = "TICKETPRIORITY")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TicketPriority implements Serializable, Comparable {

    private Integer id;
    private String name;
    private String icon = "text";
    private Integer order;
    private Boolean active = Boolean.TRUE;
//    private Set<Ticket> tickets = new HashSet<Ticket>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "PORDER")
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Column(name = "NAME", length = 50)
    @NotEmpty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    @OneToMany(mappedBy = "priority")
//    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
//    public Set<Ticket> getTickets() {
//        return tickets;
//    }

//    public void setTickets(Set<Ticket> tickets) {
//        this.tickets = tickets;
//    }

    @Column(name = "ICON", length = 50)
    @NotEmpty
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "ACTIVE")
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public int compareTo(Object o) {
        return this.order.compareTo(((TicketPriority) o).getOrder());
//        return this.id.compareTo(((TicketPriority)o).getId());
    }

    @Override
    public String toString() {
        return "TicketPriority{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", icon='" + icon + '\'' +
                ", order=" + order +
                ", active=" + active +
                '}';
    }
}
