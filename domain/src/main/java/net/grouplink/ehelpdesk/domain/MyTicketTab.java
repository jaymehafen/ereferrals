package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import java.io.Serializable;

import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

@Entity
@Table(name = "MYTICKETTAB")
public class MyTicketTab implements Serializable {
    private Integer id;
    private String name;
    private String comments;
    private User user;
    private Integer order;
    private TicketFilter ticketFilter;

    private boolean active = true;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the comments
     */
    @Column(name = "COMMENTS")
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }


    /**
     * @return the active
     */
    @Column(name = "ACTIVE")
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "SORTORDER")
    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETFILTERID")
    public TicketFilter getTicketFilter() {
        return ticketFilter;
    }

    public void setTicketFilter(TicketFilter ticketFilter) {
        this.ticketFilter = ticketFilter;
    }
}
