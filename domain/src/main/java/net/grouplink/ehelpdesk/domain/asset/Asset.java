package net.grouplink.ehelpdesk.domain.asset;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a <code>Asset</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "ASSET")
@FilterDef(name = "deletedObject", parameters = @ParamDef(name = "isActive", type = "boolean"))
@Filter(name = "deletedObject", condition = "ACTIVE = :isActive")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Asset implements Serializable {

    private Integer id;
    private String assetNumber;
    private String name;
    private AssetType assetType;
    private Vendor vendor;
    private String notes;
    private Date acquisitionDate;
    private Asset parentAsset;
    private String assetLocation;
    private Boolean active = Boolean.TRUE;
    private ImprovedAccountingInfo accountingInfo;
    private Set<Ticket> tickets = new HashSet<Ticket>();

    @SuppressWarnings("unchecked")
    private List<SoftwareLicense> softLicks = LazyList.decorate(new ArrayList(), FactoryUtils.instantiateFactory(SoftwareLicense.class));
    private Set<SoftwareLicense> softwareLicenses = new HashSet<SoftwareLicense>();
    private String manufacturer;
    private String modelNumber;
    private String serialNumber;
    private String description;
    private AssetStatus status;

    private Set<AssetCustomFieldValue> customFieldValues = new HashSet<AssetCustomFieldValue>();

    /** eHelpDesk properties */
    private User owner;
    private Location location;
    private Group group;
    private Category category;
    private CategoryOption categoryOption;
    private String hostname;
    private Integer vncPort;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "ACTIVE", nullable = false)
    public Boolean getActive() {
        if (active == null) return Boolean.TRUE;
        else
            return active;
    }

    public void setActive(Boolean active) {
        if (active == null)
            this.active = Boolean.TRUE;
        else
            this.active = active;
    }

    @Transient
    public List<Ticket> getOrderList() {
        List<Ticket> list = new ArrayList<Ticket>(tickets);
        Collections.sort(list);
        return list;
    }

    @OneToMany(mappedBy = "asset")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Set<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(Set<Ticket> tickets) {
        this.tickets = tickets;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IMPROVEDACCOUNTINGINFOID")
    @Cascade(CascadeType.ALL)
    public ImprovedAccountingInfo getAccountingInfo() {
        return accountingInfo;
    }

    public void setAccountingInfo(ImprovedAccountingInfo improvedAccountingInfo) {
        this.accountingInfo = improvedAccountingInfo;
    }

    @Column(name = "ASSETNUMBER", nullable = false)
    public String getAssetNumber() {
        return assetNumber;
    }

    public void setAssetNumber(String assetNumber) {
        this.assetNumber = assetNumber;
    }

    @Column(name = "NAME", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Transient
    public String getJSEscapeAssetName() {
        return StringEscapeUtils.escapeJavaScript(this.name);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSETTYPEID", nullable = false)
    public AssetType getAssetType() {
        return assetType;
    }

    public void setAssetType(AssetType assetType) {
        this.assetType = assetType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OWNERID")
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VENDORID")
    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    @Column(name = "NOTES")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACQUISITIONDATE")
    public Date getAcquisitionDate() {
        return acquisitionDate;
    }

    public void setAcquisitionDate(Date acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATIONID")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENTASSETID")
    public Asset getParentAsset() {
        return parentAsset;
    }

    public void setParentAsset(Asset parentAsset) {
        this.parentAsset = parentAsset;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYID")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYOPTIONID")
    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    @Column(name = "ASSETLOCATION")
    public String getAssetLocation() {
        return assetLocation;
    }

    public void setAssetLocation(String assetLocation) {
        this.assetLocation = assetLocation;
    }

    @Column(name = "MANUFACTURER")
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Column(name = "MODELNUMBER")
    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    @Column(name = "SERIALNUMBER")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSETSTATUSID")
    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    @OneToMany(mappedBy = "asset")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    public Set<AssetCustomFieldValue> getCustomFieldValues() {
        return customFieldValues;
    }

    public void setCustomFieldValues(Set<AssetCustomFieldValue> customFieldValues) {
        this.customFieldValues = customFieldValues;
    }

    @Transient
    public List<SoftwareLicense> getSoftLicks() {
        return softLicks;
    }

    public void setSoftLicks(List<SoftwareLicense> softLicks) {
        this.softLicks = softLicks;
    }

    @ManyToMany
    @JoinTable(name = "ASSETSOFTWARELICENSE",
            joinColumns = @JoinColumn(name = "ASSETID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "SOFTWARELICENSEID", referencedColumnName = "ID"))
    public Set<SoftwareLicense> getSoftwareLicenses() {
        return softwareLicenses;
    }

    public void setSoftwareLicenses(Set<SoftwareLicense> softwareLicenses) {
        this.softwareLicenses = softwareLicenses;
    }

    @Column(name = "HOSTNAME")
    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    @Column(name = "VNCPORT")
    public Integer getVncPort() {
        return vncPort;
    }

    public void setVncPort(Integer vncPort) {
        this.vncPort = vncPort;
    }

    @Override
    public String toString() {
        return "Asset{" +
                "id=" + id +
                ", assetNumber='" + assetNumber + '\'' +
                ", name='" + name + '\'' +
                ", notes='" + notes + '\'' +
                ", acquisitionDate=" + acquisitionDate +
                ", assetLocation='" + assetLocation + '\'' +
                ", active=" + active +
                ", manufacturer='" + manufacturer + '\'' +
                ", modelNumber='" + modelNumber + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", description='" + description + '\'' +
                ", hostname='" + hostname + '\'' +
                ", vncPort=" + vncPort +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Asset)) return false;

        Asset asset = (Asset) o;

        if (acquisitionDate != null ? !acquisitionDate.equals(asset.acquisitionDate) : asset.acquisitionDate != null)
            return false;
        if (active != null ? !active.equals(asset.active) : asset.active != null) return false;
        if (assetLocation != null ? !assetLocation.equals(asset.assetLocation) : asset.assetLocation != null)
            return false;
        if (assetNumber != null ? !assetNumber.equals(asset.assetNumber) : asset.assetNumber != null) return false;
        if (description != null ? !description.equals(asset.description) : asset.description != null) return false;
        if (manufacturer != null ? !manufacturer.equals(asset.manufacturer) : asset.manufacturer != null) return false;
        if (modelNumber != null ? !modelNumber.equals(asset.modelNumber) : asset.modelNumber != null) return false;
        if (name != null ? !name.equals(asset.name) : asset.name != null) return false;
        if (notes != null ? !notes.equals(asset.notes) : asset.notes != null) return false;
        if (serialNumber != null ? !serialNumber.equals(asset.serialNumber) : asset.serialNumber != null) return false;
        if (hostname != null ? !hostname.equals(asset.hostname) : asset.hostname != null) return false;
        if (vncPort != null ? !vncPort.equals(asset.vncPort) : asset.vncPort != null) return false;
        else assert true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = assetNumber != null ? assetNumber.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (acquisitionDate != null ? acquisitionDate.hashCode() : 0);
        result = 31 * result + (assetLocation != null ? assetLocation.hashCode() : 0);
        result = 31 * result + (manufacturer != null ? manufacturer.hashCode() : 0);
        result = 31 * result + (modelNumber != null ? modelNumber.hashCode() : 0);
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (hostname != null ? hostname.hashCode() : 0);
        result = 31 * result + (vncPort != null ? vncPort.hashCode() : 0);
        return result;
    }
}
