package net.grouplink.ehelpdesk.domain;

import net.grouplink.ehelpdesk.domain.acl.SecurityLevel;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.filter.UserVisibleTicketFilterFactory;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ParamDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FullTextFilterDef;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents a <code>Ticket</code> in the eHelpDesk system.
 */
@Entity
@Indexed
@Table(name = "TICKET")
@FilterDefs({@FilterDef(name = "deletedTickets", parameters = @ParamDef(name = "deletedId", type = "integer")),
             @FilterDef(name = "ttPendingTickets", parameters = @ParamDef(name = "ttPendingId", type = "integer"))})
@Filters({@Filter(name = "deletedTickets", condition = "STATUSID != :deletedId"),
          @Filter(name = "ttPendingTickets", condition = "(TTSTATUS is null or TTSTATUS != :ttPendingId)")})
@FullTextFilterDef(name = "userVisibleTickets", impl = UserVisibleTicketFilterFactory.class)
@BatchSize(size = 100)
public class Ticket implements Serializable, Comparable, Cloneable {
    public static final Integer TT_STATUS_PENDING = -1;
    public static final Integer TT_STATUS_ACTIVE = 1;

    private Integer id;

    private User contact;
    private User submittedBy;
    private UserRoleGroup assignedTo;
    private TicketPriority priority;
    private Status status;
    private Location location;
    private Group group;
    private Category category;
    private CategoryOption categoryOption;

    private String subject;
    private String note;

    private Date createdDate;
    private Date modifiedDate;
    private Date estimatedDate;
    private Integer workTime = 0;

    private String cc;
    private String bc;
    private Ticket parent;
    private Integer oldTicketId;
    private EmailToTicketConfig emailToTicketConfig;
    private String anonymousEmailAddress;

    private Set<KnowledgeBase> knowledgeBase = new HashSet<KnowledgeBase>();
    private Set<ZenInformation> zenInformation = new HashSet<ZenInformation>();
    private Set<TicketHistory> ticketHistory = new HashSet<TicketHistory>();
    private Set<Attachment> attachments = new HashSet<Attachment>();
    private Set<TicketScheduleStatus> ticketScheduleStatus = new HashSet<TicketScheduleStatus>();
    private Set<Ticket> subtickets = new HashSet<Ticket>();
    private Set<CustomFieldValues> customeFieldValues = new HashSet<CustomFieldValues>();
    private List<TicketAudit> ticketAudit = new ArrayList<TicketAudit>();
    /* Used for spring binding. Runtime type is MultipartFile. */
    private List<Object> attchmnt = new ArrayList<Object>();
    private Ticket oldTicket;
    private Boolean notifyUser = Boolean.FALSE;
    private Boolean notifyTech = Boolean.FALSE;
    private Asset asset;
    private Set<SurveyData> surveyData = new HashSet<SurveyData>();
    private ZenAsset zenAsset;
    private TicketTemplate ticketTemplate;
    private String ticketTemplateName;
    private TicketTemplateLaunch ticketTemplateLaunch;
    private WorkflowStepStatus workflowStepStatus;
    private Integer ticketTemplateStatus;
    private SecurityLevel securityLevel;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    @Field(index = Index.TOKENIZED, store = Store.NO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "ticket")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @OrderBy("id asc")
    public List<TicketAudit> getTicketAudit() {
        return ticketAudit;
    }

    public void setTicketAudit(List<TicketAudit> ticketAudit) {
        this.ticketAudit = ticketAudit;
    }

    @Column(name = "OLDTID")
    public Integer getOldTicketId() {
        return oldTicketId;
    }

    public void setOldTicketId(Integer oldTicketId) {
        this.oldTicketId = oldTicketId;
    }

    @OneToMany(mappedBy = "ticket")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<CustomFieldValues> getCustomeFieldValues() {
        return customeFieldValues;
    }

    public void setCustomeFieldValues(Set<CustomFieldValues> customeFieldValues) {
        this.customeFieldValues = customeFieldValues;
    }

    public void addCustomeFieldValues(CustomFieldValues cfv) {
        cfv.setTicket(this);
        customeFieldValues.add(cfv);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSETID")
    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    @OneToMany(mappedBy = "ticket")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<TicketScheduleStatus> getTicketScheduleStatus() {
        return ticketScheduleStatus;
    }

    public void setTicketScheduleStatus(Set<TicketScheduleStatus> ticketScheduleStatus) {
        this.ticketScheduleStatus = ticketScheduleStatus;
    }

    @Transient
    public Boolean getNotifyUser() {
        return notifyUser;
    }

    public void setNotifyUser(Boolean notifyUser) {
        this.notifyUser = notifyUser;
    }

    @Transient
    public Boolean getNotifyTech() {
        return notifyTech;
    }

    public void setNotifyTech(Boolean notifyTech) {
        this.notifyTech = notifyTech;
    }

    @Transient
    public Ticket getOldTicket() {
        return oldTicket;
    }

    public void setOldTicket(Ticket oldTicket) {
        this.oldTicket = oldTicket;
    }


    @ManyToMany
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    @JoinTable(name = "TICKETATTACHMENTS",
         joinColumns = @JoinColumn(name = "TICKETID"),
         inverseJoinColumns = @JoinColumn(name = "ATTACHMENTID"))
    public Set<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void addAttachment(Attachment attachment) {
        attachments.add(attachment);
    }

    /**
     * Used for spring binding. Runtime type is MultipartFile.
     * @return List of MultipartFile
     */
    @Transient
    public List<Object> getAttchmnt() {
        return attchmnt;
    }

    public void setAttchmnt(List<Object> attchmnt) {
        this.attchmnt = attchmnt;
    }

    @IndexedEmbedded
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GROUPID")
    @NotNull
    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Column(name = "CC")
    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    @Column(name = "BCC")
    public String getBc() {
        return bc;
    }

    public void setBc(String bc) {
        this.bc = bc;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASSIGNEDTO")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    @NotNull
    public UserRoleGroup getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(UserRoleGroup assignedTo) {
        this.assignedTo = assignedTo;
    }

    @IndexedEmbedded
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYID")
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @IndexedEmbedded
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORYOPTIONID")
    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDDATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ESTIMATEDDATE")
    public Date getEstimatedDate() {
        return estimatedDate;
    }

    public void setEstimatedDate(Date estimatedDate) {
        this.estimatedDate = estimatedDate;
    }

    @IndexedEmbedded
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOCATIONID")
    @NotNull
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFIEDDATE")
    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRIORITYID")
    public TicketPriority getPriority() {
        return priority;
    }

    public void setPriority(TicketPriority priority) {
        this.priority = priority;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUSID")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUBMITTEDBY")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    public User getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(User submitedBy) {
        this.submittedBy = submitedBy;
    }

    @Column(name = "WORKTIME")
    public Integer getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Integer workTime) {
        this.workTime = workTime;
    }

    @Transient
    public String getDisplayWorkTime() {
        int time = workTime;
        String secString;
        String minString;
        String hrsString;

        int sec = time % 60;
        secString = sec + "";
        if (sec < 10) {
            secString = "0" + sec;
        }
        int min = (time / 60) % 60;
        minString = min + "";
        if (min < 10) {
            minString = "0" + min;
        }
        int hrs = time / 60 / 60;
        hrsString = hrs + "";
        if (hrs < 10) {
            hrsString = "0" + hrs;
        }
        return hrsString + ":" + minString + ":" + secString;
    }

    public void setDisplayWorkTime(String workTime) {
        String[] timeArray = workTime.split(":");
        int sec = 0;
        int min = 0;
        int hrs = 0;
        if (timeArray.length > 2) {
            sec = Integer.parseInt(timeArray[2]);
        }
        if (timeArray.length > 1) {
            min = Integer.parseInt(timeArray[1]);
        }
        if (timeArray.length > 0) {
            hrs = Integer.parseInt(timeArray[0]);
        }
        this.workTime = hrs * 60 * 60 + min * 60 + sec;
    }

    @IndexedEmbedded
    @OneToMany(mappedBy = "ticket")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<TicketHistory> getTicketHistory() {
        return ticketHistory;
    }

    public void setTicketHistory(Set<TicketHistory> ticketHistory) {
        this.ticketHistory = ticketHistory;
    }

    public void addTicketHistory(TicketHistory ticketHistory) {
        ticketHistory.setTicket(this);
        this.ticketHistory.add(ticketHistory);
    }

    @Transient
    public List<TicketHistory> getTicketHistoryList() {
        List<TicketHistory> list = new ArrayList<TicketHistory>(ticketHistory);
        Collections.sort(list);
        return list;
    }

    @ManyToMany
    @JoinTable(name = "TICKETKB",
                joinColumns = @JoinColumn(name = "TICKETID"),
                inverseJoinColumns = @JoinColumn(name = "KBID"))
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    public Set<KnowledgeBase> getKnowledgeBase() {
        return knowledgeBase;
    }

    public void setKnowledgeBase(Set<KnowledgeBase> knowledgeBase) {
        this.knowledgeBase = knowledgeBase;
    }

    public void addKnowledgeBase(KnowledgeBase kb) {
        knowledgeBase.add(kb);
    }

    @OneToMany(mappedBy = "ticket")
    @Cascade({org.hibernate.annotations.CascadeType.ALL, org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    public Set<ZenInformation> getZenInformation() {
        return zenInformation;
    }

    public void setZenInformation(Set<ZenInformation> zenInformation) {
        this.zenInformation = zenInformation;
    }

    @Field(index = Index.TOKENIZED, store = Store.NO)
    @Column(name = "SUBJECT")
    @NotBlank
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Field(index = Index.TOKENIZED, store = Store.NO)
    @Lob
    @Column(name = "NOTE", length = 2147483647)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    @NotNull
    public User getContact() {
        return contact;
    }

    public void setContact(User contact) {
        this.contact = contact;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENTID")
    public Ticket getParent() {
        return parent;
    }

    public void setParent(Ticket parent) {
        this.parent = parent;
    }

    @OneToMany(mappedBy = "parent")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, org.hibernate.annotations.CascadeType.PERSIST})
    public Set<Ticket> getSubtickets() {
        return subtickets;
    }

    public void setSubtickets(Set<Ticket> subtickets) {
        this.subtickets = subtickets;
    }

    public void addSubTicket(Ticket ticket) {
        ticket.setParent(this);
        getSubtickets().add(ticket);
    }

    @Transient
    public List<Ticket> getSubTicketsList() {
        List<Ticket> list = new ArrayList<Ticket>();
        if (subtickets != null) {
            list.addAll(subtickets);
            Collections.sort(list);
        }
        return list;
    }

    @Transient
    public Integer getTicketId() {
        if (oldTicketId == null) {
            return id;
        } else {
            return oldTicketId;
        }
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMAILTOTICKETCONFIGID")
    public EmailToTicketConfig getEmailToTicketConfig() {
        return emailToTicketConfig;
    }

    public void setEmailToTicketConfig(EmailToTicketConfig emailToTicketConfig) {
        this.emailToTicketConfig = emailToTicketConfig;
    }

    @Column(name = "ANONYMOUSEMAILADDRESS")
    public String getAnonymousEmailAddress() {
        return anonymousEmailAddress;
    }

    public void setAnonymousEmailAddress(String anonymousEmailAddress) {
        this.anonymousEmailAddress = anonymousEmailAddress;
    }

    @OneToMany(mappedBy = "ticket")
    public Set<SurveyData> getSurveyData() {
        return surveyData;
    }

    public void setSurveyData(Set<SurveyData> surveyData) {
        this.surveyData = surveyData;
    }

    public void addSurveyData(SurveyData surveyData) {
        surveyData.setTicket(this);
        this.surveyData.add(surveyData);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ZENASSETID")
    public ZenAsset getZenAsset() {
        return zenAsset;
    }

    public void setZenAsset(ZenAsset zenAsset) {
        this.zenAsset = zenAsset;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETTEMPLATEID")
    public TicketTemplate getTicketTemplate() {
        return ticketTemplate;
    }

    public void setTicketTemplate(TicketTemplate ticketTemplate) {
        this.ticketTemplate = ticketTemplate;
    }

    @Column(name = "TICKETTEMPLATENAME")
    public String getTicketTemplateName() {
        return ticketTemplateName;
    }

    public void setTicketTemplateName(String ticketTemplateName) {
        this.ticketTemplateName = ticketTemplateName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TTLAUNCHID")
    public TicketTemplateLaunch getTicketTemplateLaunch() {
        return ticketTemplateLaunch;
    }

    public void setTicketTemplateLaunch(TicketTemplateLaunch ticketTemplateLaunch) {
        this.ticketTemplateLaunch = ticketTemplateLaunch;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "WORKFLOWSTEPSTATUS")
    public WorkflowStepStatus getWorkflowStepStatus() {
        return workflowStepStatus;
    }

    public void setWorkflowStepStatus(WorkflowStepStatus workflowStepStatus) {
        this.workflowStepStatus = workflowStepStatus;
    }

    @Column(name = "TTSTATUS")
    public Integer getTicketTemplateStatus() {
        return ticketTemplateStatus;
    }

    public void setTicketTemplateStatus(Integer ticketTemplateStatus) {
        this.ticketTemplateStatus = ticketTemplateStatus;
    }

    @ManyToOne
    @JoinColumn(name = "SECURITYLEVELID")
    public SecurityLevel getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(SecurityLevel securityLevel) {
        this.securityLevel = securityLevel;
    }

    public int compareTo(Object o) {
        return id.compareTo(((Ticket) o).getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ticket)) return false;

        Ticket ticket = (Ticket) o;

        if (anonymousEmailAddress != null ? !anonymousEmailAddress.equals(ticket.anonymousEmailAddress) : ticket.anonymousEmailAddress != null)
            return false;
        if (bc != null ? !bc.equals(ticket.bc) : ticket.bc != null) return false;
        if (cc != null ? !cc.equals(ticket.cc) : ticket.cc != null) return false;
        if (createdDate != null ? !createdDate.equals(ticket.createdDate) : ticket.createdDate != null) return false;
        if (emailToTicketConfig != null ? !emailToTicketConfig.equals(ticket.emailToTicketConfig) : ticket.emailToTicketConfig != null)
            return false;
        if (estimatedDate != null ? !estimatedDate.equals(ticket.estimatedDate) : ticket.estimatedDate != null)
            return false;
        if (modifiedDate != null ? !modifiedDate.equals(ticket.modifiedDate) : ticket.modifiedDate != null)
            return false;
        if (note != null ? !note.equals(ticket.note) : ticket.note != null) return false;
        if (notifyTech != null ? !notifyTech.equals(ticket.notifyTech) : ticket.notifyTech != null) return false;
        if (notifyUser != null ? !notifyUser.equals(ticket.notifyUser) : ticket.notifyUser != null) return false;
        if (oldTicketId != null ? !oldTicketId.equals(ticket.oldTicketId) : ticket.oldTicketId != null) return false;
        if (subject != null ? !subject.equals(ticket.subject) : ticket.subject != null) return false;
        if (ticketTemplateName != null ? !ticketTemplateName.equals(ticket.ticketTemplateName) : ticket.ticketTemplateName != null)
            return false;
        if (ticketTemplateStatus != null ? !ticketTemplateStatus.equals(ticket.ticketTemplateStatus) : ticket.ticketTemplateStatus != null)
            return false;
        if (workTime != null ? !workTime.equals(ticket.workTime) : ticket.workTime != null) return false;
        else assert true;

        return true;
    }

    @Override
    public int hashCode() {
        int result = subject != null ? subject.hashCode() : 0;
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (modifiedDate != null ? modifiedDate.hashCode() : 0);
        result = 31 * result + (estimatedDate != null ? estimatedDate.hashCode() : 0);
        result = 31 * result + (workTime != null ? workTime.hashCode() : 0);
        result = 31 * result + (cc != null ? cc.hashCode() : 0);
        result = 31 * result + (bc != null ? bc.hashCode() : 0);
        result = 31 * result + (oldTicketId != null ? oldTicketId.hashCode() : 0);
        result = 31 * result + (anonymousEmailAddress != null ? anonymousEmailAddress.hashCode() : 0);
        result = 31 * result + (notifyUser != null ? notifyUser.hashCode() : 0);
        result = 31 * result + (notifyTech != null ? notifyTech.hashCode() : 0);
        result = 31 * result + (ticketTemplateName != null ? ticketTemplateName.hashCode() : 0);
        result = 31 * result + (ticketTemplateStatus != null ? ticketTemplateStatus.hashCode() : 0);
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Ticket t = (Ticket) super.clone();
        t.setId(null);

        // Do a deep clone on customfieldvalues
        Set<CustomFieldValues> clonedCfvs = new HashSet<CustomFieldValues>();
        Set<CustomFieldValues> cfvs = this.getCustomeFieldValues();
        for (CustomFieldValues cfv : cfvs) {
            clonedCfvs.add(cfv.clone());
        }
        t.setCustomeFieldValues(clonedCfvs);
        
        return t;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("Ticket -----\n")
                .append("ID: ").append(getId()).append("\n")
                .append("Contact: ").append(getContact()).append("\n")
                .append("Group: ").append(getGroup() != null ? getGroup().getName() : "").append("\n")
                .append("Category: ").append(getCategory() != null ? getCategory().getName() : "").append("\n")
                .append("CatOpt: ").append(getCategoryOption() != null ? getCategoryOption().getName() : "").append("\n")
                .append("Subject: ").append(getSubject()).append("\n")
                .append("Note: ").append(getNote()).append("\n");
        return s.toString();
    }
}
