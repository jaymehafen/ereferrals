package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * Represents a <code>Ticket Status Time Table</code> in the eHelpDesk system.
 */
@Entity
@Table(name = "TICKETAUDIT")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class TicketAudit implements Serializable {

    public static final String PROPERTY_CONTACT = "ticket.contact";
    public static final String PROPERTY_LOCATION = "ticket.location";
    public static final String PROPERTY_GROUP = "ticket.group";
    public static final String PROPERTY_CATEGORY = "ticket.category";
    public static final String PROPERTY_CATEGORYOPTION = "ticket.categoryOption";
    public static final String PROPERTY_ASSIGNEDTO = "ticket.assignedTo";
    public static final String PROPERTY_TICKETPRIORITY = "ticket.priority";
    public static final String PROPERTY_STATUS = "ticket.status";
    public static final String PROPERTY_SUBMITTEDBY = "ticket.submittedBy";
    public static final String PROPERTY_ESTIMATEDDATE = "ticket.estimatedDate";
    public static final String PROPERTY_ASSET = "ticket.asset.name";
    public static final String PROPERTY_SUBJECT = "ticket.subject";
    public static final String PROPERTY_NOTE = "ticket.note";
    public static final String PROPERTY_WORKTIME = "ticket.workTime";

    private Integer id;
    private Ticket ticket;
    private String property;
    private String beforeValue;
    private String afterValue;
    private String beforeText;
    private String afterText;
    private Date dateStamp;
    private User user;
    private Object beforeValueObject;
    private Object afterValueObject;
    private String beforeValueDescription;
    private String afterValueDescription;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Transient
    public Object getBeforeValueObject() {
        return beforeValueObject;
    }

    public void setBeforeValueObject(Object beforeValueObject) {
        this.beforeValueObject = beforeValueObject;
    }

    @Transient
    public Object getAfterValueObject() {
        return afterValueObject;
    }

    public void setAfterValueObject(Object afterValueObject) {
        this.afterValueObject = afterValueObject;
    }

    @Transient
    public String getBeforeValueDescription() {
        return beforeValueDescription;
    }

    public void setBeforeValueDescription(String beforeValueDescription) {
        this.beforeValueDescription = beforeValueDescription;
    }

    @Transient
    public String getAfterValueDescription() {
        return afterValueDescription;
    }

    public void setAfterValueDescription(String afterValueDescription) {
        this.afterValueDescription = afterValueDescription;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TICKETID")
    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @Column(name = "PROPERTY")
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Column(name = "BEFOREVALUE")
    public String getBeforeValue() {
        return beforeValue;
    }

    public void setBeforeValue(String beforeValue) {
        this.beforeValue = beforeValue;
    }

    @Column(name = "AFTERVALUE")
    public String getAfterValue() {
        return afterValue;
    }

    public void setAfterValue(String afterValue) {
        this.afterValue = afterValue;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATESTAMP")
    public Date getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(Date dateStamp) {
        this.dateStamp = dateStamp;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USERID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Lob
    @Column(name = "BEFORETEXT", length = 2147483647)
    public String getBeforeText() {
        return beforeText;
    }

    public void setBeforeText(String beforeText) {
        this.beforeText = beforeText;
    }

    @Lob
    @Column(name = "AFTERTEXT", length = 2147483647)
    public String getAfterText() {
        return afterText;
    }

    public void setAfterText(String afterText) {
        this.afterText = afterText;
    }
}
