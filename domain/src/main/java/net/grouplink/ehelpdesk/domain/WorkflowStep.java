package net.grouplink.ehelpdesk.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "WORKFLOWSTEP")
public class WorkflowStep implements Serializable, Cloneable {
    private Integer id;
    private TicketTemplateMaster ticketTemplateMaster;
    private Integer stepNumber;
    private Status stepCompletedStatus;
    private Set<TicketTemplate> ticketTemplates = new HashSet<TicketTemplate>();

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TTMASTERID")
    public TicketTemplateMaster getTicketTemplateMaster() {
        return ticketTemplateMaster;
    }

    public void setTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        this.ticketTemplateMaster = ticketTemplateMaster;
    }

    @Column(name = "STEPNUMBER")
    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUSID")
    public Status getStepCompletedStatus() {
        return stepCompletedStatus;
    }

    public void setStepCompletedStatus(Status stepCompletedStatus) {
        this.stepCompletedStatus = stepCompletedStatus;
    }

    @OneToMany(mappedBy = "workflowStep")
    public Set<TicketTemplate> getTicketTemplates() {
        return ticketTemplates;
    }

    public void setTicketTemplates(Set<TicketTemplate> ticketTemplates) {
        this.ticketTemplates = ticketTemplates;
    }

    public void addTicketTemplate(TicketTemplate ticketTemplate) {
        ticketTemplate.setWorkflowStep(this);
        ticketTemplates.add(ticketTemplate);
    }

    @Override
    public WorkflowStep clone() throws CloneNotSupportedException {
        WorkflowStep wfs = (WorkflowStep) super.clone();
        wfs.setId(null);
        wfs.setTicketTemplates(new HashSet<TicketTemplate>());
        return wfs;
    }
}
