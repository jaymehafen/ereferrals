package net.grouplink.ehelpdesk.domain.acl;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ACL_GLOBALPERMISSIONENTRY")
public class GlobalPermissionEntry implements Serializable {
    private Integer id;
    private Permission permission;
    private PermissionActors permissionActors;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "PERMISSIONID")
    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PERMISSIONACTORSID")
    public PermissionActors getPermissionActors() {
        return permissionActors;
    }

    public void setPermissionActors(PermissionActors permissionActors) {
        this.permissionActors = permissionActors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GlobalPermissionEntry)) return false;

        GlobalPermissionEntry that = (GlobalPermissionEntry) o;

        if (!permission.equals(that.permission)) return false;
        if (!permissionActors.equals(that.permissionActors)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = permission.hashCode();
        result = 31 * result + permissionActors.hashCode();
        return result;
    }
}
