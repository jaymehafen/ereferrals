package net.grouplink.ehelpdesk.domain.acl;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ACL_PERMISSIONSCHEMEENTRY")
public class PermissionSchemeEntry implements Serializable {
    private Integer id;
    private Permission permission;
    private PermissionActors permissionActors;
    private PermissionScheme permissionScheme;

    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "org.hibernate.id.IncrementGenerator")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "PERMISSIONID")
    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PERMISSIONACTORSID")
    public PermissionActors getPermissionActors() {
        return permissionActors;
    }

    public void setPermissionActors(PermissionActors permissionActors) {
        this.permissionActors = permissionActors;
    }

    @ManyToOne
    @JoinColumn(name = "PERMISSIONSCHEMEID")
    public PermissionScheme getPermissionScheme() {
        return permissionScheme;
    }

    public void setPermissionScheme(PermissionScheme permissionScheme) {
        this.permissionScheme = permissionScheme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PermissionSchemeEntry)) return false;

        PermissionSchemeEntry entry = (PermissionSchemeEntry) o;

        if (!permission.equals(entry.permission)) return false;
        if (!permissionActors.equals(entry.permissionActors)) return false;
        if (!permissionScheme.equals(entry.permissionScheme)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = permission.hashCode();
        result = 31 * result + permissionActors.hashCode();
        result = 31 * result + permissionScheme.hashCode();
        return result;
    }
}
