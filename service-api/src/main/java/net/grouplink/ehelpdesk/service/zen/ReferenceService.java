package net.grouplink.ehelpdesk.service.zen;

public interface ReferenceService {
    byte[] getZuidForObjectUid(String objectUid);
}
