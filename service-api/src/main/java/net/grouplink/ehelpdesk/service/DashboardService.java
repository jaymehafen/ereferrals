package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;

import javax.jws.WebService;
import java.util.List;
import java.util.Map;

@WebService(serviceName = "DashboardService")
public interface DashboardService {

    /**
     * Retrieves a list of all the {@link Dashboard}s in the system.
     * @return a {@link List} of all the {@link Dashboard}s in the system.
     */
    List<Dashboard> getAll();

    /**
     * Gets a {@link Dashboard} by id
     * @param id the id of the {@link Dashboard} to return
     * @return the {@link Dashboard} with matching <code>id</code>
     */
    Dashboard getById(Integer id);

    /**
     * Saves a {@link Dashboard} to the database.
     * @param dashboard the {@link Dashboard} to save
     */
    void save(Dashboard dashboard);

    /**
     * Deletes a {@link Dashboard} to the database
     * @param dashboard the {@link Dashboard} to delete
     */
    void delete(Dashboard dashboard);

    Integer getTicketCount(Integer ticketFilterId, Integer userId);

    Map<String, Integer> getTicketCountOneGroup(Integer widgetId, Integer userId);

    Map<String, Map<String, Integer>> getTicketCountTwoGroups(Integer widgetId, Integer userId);

    void flush();
}
