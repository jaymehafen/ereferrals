package net.grouplink.ehelpdesk.service.asset;

public interface AccountingInfoMigrationProcessingService {
    void convertAccountInfos(int batchSize);
}
