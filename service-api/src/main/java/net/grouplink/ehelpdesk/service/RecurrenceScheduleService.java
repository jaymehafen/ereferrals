package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;

import java.util.Date;
import java.util.List;

public interface RecurrenceScheduleService {

    RecurrenceSchedule getById(Integer id);

    void save(RecurrenceSchedule recurrenceSchedule);

    boolean meetsScheduleCriteria(RecurrenceSchedule schedule, Date processDate, Date lastActionDate, int occurrenceCount);

    List<RecurrenceSchedule> getAllRunning();

    List<RecurrenceSchedule> getRunningTicketTemplateSchedules();

    List<RecurrenceSchedule> getRunningScheduleStatusSchedules();

    List<RecurrenceSchedule> getRunningReportSchedules();

    void delete(RecurrenceSchedule recurrenceSchedule);
}
