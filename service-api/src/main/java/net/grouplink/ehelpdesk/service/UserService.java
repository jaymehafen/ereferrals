package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.common.utils.UserSearch;

import java.util.List;
import java.util.Map;

/**
 * Identifies service classes for accessing and modifying a User
 *
 * @see net.grouplink.ehelpdesk.domain.User
 *
 * @version 1.0
 * @author mrollins
 */
//@WebService(serviceName = "UserService")
public interface UserService {
    public User getUserById(Integer id);
    public User getUserByUsername(String username);
    public User getUserByUsernameAndPassword(String username, String password);
    public User getUserByEmail(String email);
    public String resetUserPassword(User user);
    public List<User> getUsers();
    public List<User> getActiveUsers();
    public void saveUser(User user);

    /**
     * Deletes a User, or inactivates the User if it's in use.
     * @param user User to delete
     * @return true if User was deleted, false if User was inactivated
     */
    boolean deleteUser(User user);

    public boolean isEndUser(User user);
    public List<User> searchUser(UserSearch userSearch);
    public void removePhone(Phone phone);
    public void removeAddress(Address address);
    public User synchronizedUserWithLdap(User user, String password, LdapUser ldapUser);

    public void inActivateUser(User user);

    public boolean userInUse(User user);

    public void flush();

    public User getUserByFirstAndLastName(String firstName, String lastName);

    public List<User> getFilteredUsers(String sort, boolean ascending, String group, String firstName, String lastName, String role, Boolean status, String username, String email);

    public boolean isManagerForGroup(User user, UserRoleGroup urg);

    void clear();

    List<User> getUsers(String queryParam, int start, int count, Map<String, Boolean> sortFieldsOrder, boolean includeInactive);

    int getUserCount(String queryParam, boolean includeInactive);

    User getLoggedInUser();

    List<User> getUsersWithAnyRole(List<Role> roles);
}
