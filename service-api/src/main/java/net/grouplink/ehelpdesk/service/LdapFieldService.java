package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.LdapField;

import java.util.List;

public interface LdapFieldService {

	public List<LdapField> getLdapFields();
    public LdapField getLdapFieldByAttributeID(String fieldName);
    public LdapField getLdapFieldById(Integer fieldId);    
    public void saveLdapField(LdapField ldapField);
    public void saveLdapFields(List ldapFieldList);
    public void deleteLdapField(LdapField ldapField);
    public void deleteLdapFieldList(List list);
}
