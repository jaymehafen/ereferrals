package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import org.hibernate.ScrollableResults;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Locale;
import java.io.UnsupportedEncodingException;

/**
 * @author mrollins
 * @version 1.0
 */
public interface TicketHistoryService {

    void saveTicketHistory(TicketHistory ticketHistory, User user);

    void sendEmailNotification(TicketHistory ticketHistory)
            throws MessagingException, UnsupportedEncodingException;

    String createTicketHistoryHTMLComments(TicketHistory ticketHistory);

    String createTicketHistoryTextComments(TicketHistory ticketHistory);

    TicketHistory getTicketHistoryById(Integer id);

    List getTicketHistoriesByTicket(Ticket ticket);

    List<TicketHistory> getTicketHistoriesByTicket(Ticket ticket, boolean sortDesc);

    List<TicketHistory> getEventTicketHistoriesByUser(User user);

    TicketHistory createNewTicketHistory(User user, Ticket ticket, int thtype, Locale locale);

    TicketHistory initializedTicketHistory(User submittedByUser, Ticket ticket, int thtype);

    void saveComments(User user, String updateType, Ticket ticket);

    ScrollableResults getAllTicketHistoryByGroupId(Integer groupId);

    void flush();

    void clear();

    int getHistoryCount(Integer groupId);
}
