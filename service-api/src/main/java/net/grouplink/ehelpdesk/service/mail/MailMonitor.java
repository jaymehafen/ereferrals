package net.grouplink.ehelpdesk.service.mail;

import net.grouplink.ehelpdesk.domain.mail.EmailMessage;

import javax.mail.Message;

/**
 * User: jaymehafen
 * Date: Jan 25, 2008
 * Time: 4:42:46 PM
 */
public interface MailMonitor {

    /**
     * Connects to mail server using specfied parameters, reads all messages in
     * the mailbox, converts them messages to EmailMessage objects, and deletes
     * the messages from the mailbox.
     * @param host the hostname of the mail provider
     * @param port the port to connect to
     * @param user the user name
     * @param password the password for the specified user
     * @param mailBox the mailbox to connect to (POP3 is always "INBOX")
     * @return a List of EmailMessage instances
     * @throws EmailToTicketRuntimeException if an error occurs
     */
//    List<EmailMessage> getMessages(String host, int port, String user, String password, String mailBox);

    /**
     * Connects to the mail server using the specified parameters.
     *
     * @param host the hostname of the mail provider
     * @param port the port to connect to
     * @param user the user name
     * @param password the password for the specified user
     * @param mailBox the mailbox to connect to (POP3 is always "INBOX")
     * @return the javax.mail.Folder containing the messages
     */
    MailMonitorFolder connectToFolder(String host, int port, String user, String password, String mailBox);

    boolean isValidMessage(Message message);

    EmailMessage createEmailWrapperForMessage(Message message);
}
