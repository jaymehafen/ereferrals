package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.domain.Ticket;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 13, 2008
 * Time: 1:54:14 PM
 */
public interface SurveyService {

    public Survey getSurveyByName(String name);

    public Survey getSurveyById(int id);

    public Survey getSurveyById(Integer id);

    public List<Survey> getActiveSurveys();

    public List<Survey> getAllSurveys();

    public boolean getDefaultSurveyIsActivatedForAllGroups();

    public boolean getDefaultSurveyIsActivatedForGroup(int groupId);

    public boolean getDefaultSurveyIsActivatedForCategory(int categoryId);

    public void activateSurveyForAllGroups(int selSurveyId);

    public void deactivateSurveysForAllGroups();

    public void activateSurveyForAllCatOptsInGroup(int selSurveyId, int groupId);

    public void deactivateSurveysForAllCatOptsInGroup(int groupId);

    public void activateSurveyForCategoryOption(int surveyId, int catOptId);

    public void deactivateSurveyForCategoryOption(int catOptId);

    public void activateSurveyForCategory(int selSurveyId, int catId);

    public void deactivateSurveysForAllOptsInCategory(int catId);

    public void sendSurveyNotificationEmail(Survey survey, Ticket ticket) throws Exception;

    public void sendSurveyManagementNotificationEmail(Survey survey, Ticket ticket) throws Exception;

    public void saveSurvey(Survey survey);

    public void deleteSurvey(Survey survey);

    public boolean hasDuplicateSurveyName(Survey survey);

    public void flush();

    int getCountByCategoryOption(CategoryOption categoryOption);
}
