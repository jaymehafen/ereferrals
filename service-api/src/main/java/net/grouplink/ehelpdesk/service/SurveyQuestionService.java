package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.SurveyQuestion;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 6, 2008
 * Time: 9:52:05 AM
 */
public interface SurveyQuestionService {

    public List<SurveyQuestion> getDefaultSurveyQuestions();

    public List<SurveyQuestion> getSurveyQuestionsBySurveyId(Integer surveyId);

    public SurveyQuestion getSurveyQuestionById(Integer id);

    public void saveSurveyQuestion(SurveyQuestion surveyQuestion);

    public void deleteSurveyQuestion(SurveyQuestion surveyQuestion);

    public boolean hasDuplicateSurveyQuestionText(SurveyQuestion surveyQuestion);

    public void flush();

    List<String> getByUniqueQuestionText();
}
