package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 8, 2008
 * Time: 4:30:43 PM
 */
public interface SoftwareLicenseService {
    SoftwareLicense getById(Integer id);

    List<SoftwareLicense> getAllSoftwareLicenses();

    List<Vendor> getUniqueVendors();

    void saveSoftwareLicense(SoftwareLicense softwareLicense);

    void delete(SoftwareLicense softwareLicense);

    void flush();
}
