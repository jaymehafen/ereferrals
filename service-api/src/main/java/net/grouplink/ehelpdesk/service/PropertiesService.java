package net.grouplink.ehelpdesk.service;

import java.util.List;

public interface PropertiesService {

	String getPropertiesValueByName(String name);
    Boolean getBoolValueByName(String name, boolean defaultValue);
    List<String> getPropertiesValueListByName(String name);
	void saveProperties(String name, String value);
    void saveProperties(String name, List<String> valueList);
    void flush();
}
