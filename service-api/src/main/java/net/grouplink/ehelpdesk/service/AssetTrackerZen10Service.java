package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.common.utils.AssetTrackerZen10Config;

/**
 * Date: Mar 8, 2009
 * Time: 8:25:15 PM
 */
public interface AssetTrackerZen10Service {

    void saveAssetTrackerZen10Config(AssetTrackerZen10Config zen10Config, String path);
    
    AssetTrackerZen10Config getAssetTrackerZen10Config(String path);

    String getZen10BaseUrl();

    boolean isZen10Enabled();

    boolean isEhdVncEnabled();
}
