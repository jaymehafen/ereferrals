package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;

import java.util.List;

/**
 * Identifies service classes for accessing and modifying a UserRoleGroup
 *
 * @version 1.0
 * @author mrollins
 */
public interface UserRoleGroupService {

    List<UserRoleGroup> getUserRoleGroup();

    List<UserRoleGroup> getAllUserRoleGroup();

    List<UserRoleGroup> getUserRoleGroupByGroupId(Integer id);

    List<UserRoleGroup> getManagerTechnicianUserRoleGroupByGroupId(Group group);

    List<UserRoleGroup> getUserRoleGroupSelectionByGroupId(Integer id);

    List<User> getTechnicianByGroupId(Integer id);

    List<UserRoleGroup> getUserRoleGroupByUserId(Integer id);

    UserRoleGroup getUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId);

    List<UserRoleGroup> getUserRoleGroupByUserIdRoleId(Integer id, Integer roleId);

    UserRoleGroup getUserRoleGroupById(Integer id);

    UserRole getUserRoleByUserIdRoleId(Integer userId, Integer roleId);

    void deleteUserRoleGroup(UserRoleGroup userRoleGroup);

    void saveUserRoleGroup(UserRoleGroup userRoleGroup);

    UserRoleGroup getTicketPoolByGroupId(Integer groupId);

    List<UserRoleGroup> getAllTicketPool(String queryParam, String translatedPoolName);

    UserRoleGroup getAnyUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId);

    Integer getTechnicianCount();

    Integer getWFParticipantCount();

    List<User> getTechnicianAsUsers(String queryParam);

    void flush();

    UserRoleGroup getUserRoleGroupByUserIdGroupIdRoleId(Integer userId, Integer groupId, Integer roleId);

    List<UserRoleGroup> getAssignableUserRoleGroups(String queryParam, Integer start, Integer count);

    int getAssignableUserRoleGroupCount(String queryParam);

    /**
     * Returns all URGs regardless of the active flag
     * @param userId the id if the user URG to get
     * @return list of URG for the given user id
     */
    List<UserRoleGroup> getAllUserRoleGroupByUserId(Integer userId);

    void cleanupUserRoleGroup();

    boolean delete(UserRoleGroup userRoleGroup);

    void clearNotificationsForUser(User user);

    List<UserRoleGroup> getAssignableUserRoleGroupByGroupId(Integer groupId);

    List<UserRoleGroup> getAllAssignableUserRoleGroups();
}
