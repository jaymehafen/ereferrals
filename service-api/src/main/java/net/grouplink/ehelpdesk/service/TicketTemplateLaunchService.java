package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;

import java.util.List;
import java.util.Date;

public interface TicketTemplateLaunchService {

    TicketTemplateLaunch getById(Integer id);

    void save(TicketTemplateLaunch ticketTemplateLaunch);

    void delete(TicketTemplateLaunch ticketTemplateLaunch);

    /**
     * Checks if all the Tickets for this TicketTemplateLaunch are ready to be
     * launched.
     * @param ttLaunch the TicketTemplateLaunch
     * @return List of Tickets that are NOT ready for launch
     */
    List<Ticket> readyForActivation(TicketTemplateLaunch ttLaunch);

    void activate(TicketTemplateLaunch ticketTemplateLaunch, User user);

    void abort(TicketTemplateLaunch launch);

    int getLaunchCountByTemplateMaster(TicketTemplateMaster ttm);

    List<TicketTemplateLaunch> getPendingOlderThan(Date activeDate);

    boolean userHasLaunchPermission(User user, TicketTemplateMaster master);

}
