package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.common.utils.MailMessage;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public interface MailService {

    boolean init();

    void sendMail(MailMessage mailMessage);

    MailMessage newMessage();

    MailConfig getMailConfig();

    void saveMailConfig(MailConfig mailConfig);

    void sendSurveyNotificationEmail(Survey survey, Ticket ticket) throws Exception;

    void sendSurveyManagementNotificationEmail(Survey survey, Ticket ticket) throws Exception;

    void notifyParties(User user, String htmlMessage, String textMessage, Ticket newTicket, String fromEmail, String displayEmail,
                       Boolean sendLinkToUser)
            throws MessagingException, UnsupportedEncodingException;

    void notifyPartiesTicketHistory(User user, String htmlMessage, String txtMessage, TicketHistory ticketHistory, String fromEmail,
                                    String displayEmail, Boolean sendLinkToUser)
            throws MessagingException, UnsupportedEncodingException;

    void setMailConfigured(boolean mailConfigured);

    boolean isMailConfigured();

}
