package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.SurveyFrequency;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 6, 2008
 * Time: 9:37:45 AM
 */
public interface SurveyFrequencyService {

    public List<SurveyFrequency> getSurveyFrequencies();

    public SurveyFrequency getDefaultSurveyFrequency();

    public SurveyFrequency getSurveyFrequencyById(Integer id);

    public void save(SurveyFrequency surveyFrequency);

    public void flush();
}
