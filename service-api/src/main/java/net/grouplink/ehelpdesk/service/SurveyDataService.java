package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.SurveyData;

import java.util.List;

/**
 * Date: Apr 6, 2008
 * Time: 8:26:44 PM
 */
public interface SurveyDataService {

    void saveSurveyData(SurveyData surveyData);

    List<SurveyData> getByTicketId(Integer ticketId);

    int getCountByTicketId(Integer ticketId);

}
