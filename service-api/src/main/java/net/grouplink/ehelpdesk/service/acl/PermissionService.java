package net.grouplink.ehelpdesk.service.acl;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.acl.*;

import java.util.List;

public interface PermissionService {
    boolean hasGlobalPermission(String permissionKey);

    boolean hasGlobalPermission(User user, Permission permission);

    boolean hasGlobalPermission(User user, String permissionKey);

    boolean hasGroupPermission(User user, Permission permission, Group group);

    boolean hasGroupPermission(User user, String permissionKey, Group group);

    boolean canUserViewTicket(User user, Ticket ticket);

    Permission getPermissionByKey(String key);

    List<Permission> getGlobalPermissions();

    List<Permission> getSchemePermissions();
    
    List<Permission> getFieldPermissionsByModelName(String modelName);

    void savePermission(Permission permission);

    void deletePermission(Permission permission);

    List<PermissionScheme> getPermissionSchemes();

    void savePermissionScheme(PermissionScheme permissionScheme);

    void deletePermissionScheme(PermissionScheme permissionScheme);

    GlobalPermissionEntry getGlobalPermissionEntry(Permission permission);

    void saveGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry);

    void deleteGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry);

    void savePermissionSchemeEntry(PermissionSchemeEntry permissionSchemeEntry);

    List<GlobalPermissionEntry> getGlobalPermissionEntries();

    Permission getPermissionById(Integer gpId);

    PermissionScheme getPermissionSchemeById(Integer id);

    List<Permission> getGroupPermissions();

    PermissionSchemeEntry getPermissionSchemeEntry(PermissionScheme permissionScheme, Permission permission);

    boolean hasGroupPermission(String permissionKey, Group group);
    
    List<PermissionSubject> getPermissionSubjects();

    PermissionSubject getPermissionSubjectByModelClass(String modelClass);
    
    PermissionSubject getPermissionSubjectByModelClassAndField(String modelClass, String fieldName);
    
    PermissionSubject getPermissionSubjectByModelNameAndField(String modelName, String fieldName);

    void savePermissionSubject(PermissionSubject permissionSubject);
}
