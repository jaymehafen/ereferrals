package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.DatabaseConfig;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Aug 8, 2007
 * Time: 4:35:03 PM
 */
public interface DatabaseHandlerService {

    public DatabaseConfig uploadJdbcProperties(String path) throws IOException;
    public void updateJdbcProperties(DatabaseConfig dbConfig, String path) throws IOException;
    public void ticketCorrection();
    public void databaseInit();
//    public Object getObject();
}
