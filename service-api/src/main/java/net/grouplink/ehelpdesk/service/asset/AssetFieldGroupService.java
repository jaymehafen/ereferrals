package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 5:37:32 PM
 */
public interface AssetFieldGroupService {
    AssetFieldGroup getById(Integer id);
    List<AssetFieldGroup> getByAssetType(AssetType assetType);
    void saveAssetFieldGroup(AssetFieldGroup assetFieldGroup);
    void deleteAssetFieldGroup(AssetFieldGroup assetFieldGroup);

    List<AssetFieldGroup> getAllAssetFieldGroups();

    void flush();

    AssetFieldGroup getByName(String name);
}
