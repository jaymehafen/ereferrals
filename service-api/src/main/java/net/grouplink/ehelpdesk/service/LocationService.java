package net.grouplink.ehelpdesk.service;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Location;

/**
 * @author mrollins
 * @version 1.0
 */
public interface LocationService {
    public List<Location> getLocations();
    public List<Location> getAllLocations();
    public List<Location> getLocations(String queryParam, int start, int count);
    public Location getLocationById(Integer id);
    public Location getLocationByName(String name);
    public void saveLocation(Location location);
    public boolean deleteLocation(Location location);

    public boolean hasDuplicateLocationName(Location location);

    public void flush();

    int getLocationCount(String queryParam);
}
