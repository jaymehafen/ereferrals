package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Feb 20, 2008
 * Time: 6:48:03 PM
 */
//@WebService(serviceName = "TicketFilterService")
public interface TicketFilterService {
    /**
     * Returns a list of all the {@link TicketFilter}s in the system.
     * @return a {@link List} of all the {@link TicketFilter}s in the system.
     */
    List<TicketFilter> getAll();

    /**
     * Returns a list of all the {@link TicketFilter}s in the system that are
     * marked public.
     * @return a {@link List} of all the public {@link TicketFilter}s.
     */
    List<TicketFilter> getAllPublic();

    /**
     * Returns a list of all the {@link TicketFilter}s in the system that are
     * marked public but don't belong to <code>user</code>.
     * @param user the {@link User} to exclude from the list of filters
     * @return a {@link List} of all the public {@link TicketFilter}s.
     */
    List<TicketFilter> getPublicMinusUser(User user);

    /**
     * Get a list of TicketFilters that are either public, or owned by the
     * user.
     * @param user user to include
     * @return list of TicketFilters
     */
    List<TicketFilter> getPublicPlusUser(User user);

    /**
     * Gets the {@link TicketFilter} that were created by <code>user</code>
     * @param user the {@link User} that owns the {@link TicketFilter}
     * @return a {@link List} of all {@link TicketFilter}s  owned by <code>user</code>
     */
    List<TicketFilter> getByUser(User user);

    /**
     * Gets a {@link TicketFilter} by id
     * @param id the id of the {@link TicketFilter} to return
     * @return the {@link TicketFilter} with matching <code>id</code>
     */
    TicketFilter getById(Integer id);

    /**
     * Saves a {@link TicketFilter} to the database.
     * @param ticketFilter the {@link TicketFilter} to save
     */
    void save(TicketFilter ticketFilter);

    /**
     * Deletes a {@link TicketFilter} from the database.
     * @param ticketFilter the {@link TicketFilter} to delete
     */
    void delete(TicketFilter ticketFilter);

    /**
     * Searches for Tickets matching the TicketFilter criteria.
     * @param ticketFilter the TicketFilter
     * @param user the User performing the search
     * @return the Tickets matching the search criteria
     */
    List<Ticket> getTickets(TicketFilter ticketFilter, User user);

    /**
     * Retrieve the number of <code>Ticket</code>s for the <code>TicketFilter</code>
     * with the provided <code>id</code>
     * @param ticketFilterId the id of the <code>TicketFilter</code>
     * @param userId the id of the User
     * @return an <code>Integer</code> representing the count of Tickets
     */
    Integer getTicketCount(Integer ticketFilterId, Integer userId);

    /**
     * Gets the number of tickets for the given TicketFilter and User.
     * @param ticketFilter the TicketFilter
     * @param user the User
     * @return the number of matching tickets
     */
    Integer getTicketCount(TicketFilter ticketFilter, User user);

    /**
     * Gets the number of times this TicketFilter is referenced by other domain
     * objects. Usages include MyTicketTab, Report, and Widget.
     * @param ticketFilter the TicketFilter
     * @return the number of usages
     */
    int getUsageCount(TicketFilter ticketFilter);

    /**
     * Gets a List of Tickets matching the given TicketFilter and User, with
     * sort, start position, and number of records to return.
     * @param ticketFilter the TicketFilter
     * @param user the User
     * @param sort the sort criteria, should match (or start with, in the case
     *          of custom fields) one of TicketFilter.COLUMN_X constants
     * @param start the start position, zero based
     * @param count the number of records to return
     * @return the List of Ticekts
     */
    List<Ticket> getTickets(TicketFilter ticketFilter, User user, String sort, int start, int count);

    /**
     * Performs a hibernate flush.
     */
    void flush();

    /**
     * Gets a List of TicketFilters where the version property is less than
     * the given version Integer.
     * @param version the version
     * @return a List of matching TicketFilters
     */
    List<TicketFilter> getByVersionLessThan(Integer version);

    /**
     * Migrate the TicketFilter from a previous version to the current version.
     * @param filter the TicketFilter
     */
    void migrateTicketFilter(TicketFilter filter);

    /**
     * Initializes TicketFilters to current version. This happens on startup,
     * so all TicketFilters from an old version with be migrated to the new
     * version before any user interaction.
     */
    void init();


    TicketFilter getFindUserTicketFilter(int userId);
}
