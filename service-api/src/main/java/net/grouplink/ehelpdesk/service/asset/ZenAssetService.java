package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Apr 5, 2009
 * Time: 10:59:52 PM
 */
public interface ZenAssetService {

    ZenAsset getById(Integer id);

    void save(ZenAsset zenAsset);

    List<ZenAsset> getAll();

    ZenAsset getByWorkstationId(byte[] workstationId);

    void delete(ZenAsset zenAsset);

    List<ZenAsset> search(ZenAssetSearch zenAssetSearch);
}
