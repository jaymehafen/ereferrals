package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.TicketScheduleStatus;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface TicketScheduleStatusService {
    public TicketScheduleStatus getTSSById(Integer id);
    public TicketScheduleStatus getTSSByTicket(Ticket ticket, ScheduleStatus scheduleStatus);
    public void saveTSS(TicketScheduleStatus ticketScheduleStatus);
    public void deleteTSS(TicketScheduleStatus ticketScheduleStatus);

    /**
     * Removes any TicketScheduleStatus with a reference to the given
     * ScheduleStatus, unless the TicketScheduleStatus is for one of the
     * Tickets in the tickedIdsToStay List.
     * @param ticketIdsToStay the list of TicketIds to keep
     * @param scheduleStatus the ScheduleStatus to use when finding
     *          TicketScheduleStatus's
     */
    public void cleanHouse(List ticketIdsToStay, ScheduleStatus scheduleStatus);

    void flush();
}
