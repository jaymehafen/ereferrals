package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Phone;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 5:13 PM
 */
public interface PhoneService {
    void savePhone(Phone phone);
    boolean deletePhone(Phone phone);
    Phone getById(int phoneId);
}
