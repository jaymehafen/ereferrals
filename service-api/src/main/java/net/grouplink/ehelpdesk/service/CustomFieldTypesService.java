package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.CustomFieldType;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 7, 2007
 * Time: 9:55:56 PM
 */
public interface CustomFieldTypesService {
    public List<CustomFieldType> getCustomFieldTypes();
    public CustomFieldType getCustomFieldType(Integer typeId);
}
