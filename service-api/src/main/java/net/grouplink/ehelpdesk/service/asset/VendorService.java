package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 14, 2008
 * Time: 11:53:35 AM
 */
public interface VendorService {
    Vendor getById(Integer id);

    Vendor getByName(String name);

    List<Vendor> getAllVendors();

    List<Vendor> getUndeletableVendors();

    void saveVendor(Vendor vendor);

    void deleteVendor(Vendor vendor);

    void flush();
}
