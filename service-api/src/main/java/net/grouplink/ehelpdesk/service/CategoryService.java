package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.common.utils.TreeNode;
import net.grouplink.ehelpdesk.domain.Group;

import java.util.List;

/**
 * @author david garcia
 * @version 1.0
 */
public interface CategoryService {
    public List<Category> getCategories();

    public Category getCategoryById(Integer id);

    public Category getCategoryByName(String name);

    public boolean isDuplicateCategory(Category cat);

    public List<Category> getCategoryByGroupId(Integer id);

    public void saveCategory(Category category);

    public void deleteCategory(Category category);

    public List<TreeNode> getCategoryTreeByGroupId(Integer id);

    public List<TreeNode> getCategoryTreeByUrgId(Integer urgId);

    public void flush();

    public Category getCategoryByNameAndGroupId(String categoryName, Integer groupId);

    boolean deleteCategoryIfUnused(Category category);

    boolean categoryCanBeDeleted(Category category);

    List<Category> getAllCategoriesByGroup(Group group);
}
