package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.SurveyQuestionResponse;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 24, 2008
 * Time: 5:32:57 PM
 */
public interface SurveyQuestionResponseService {

    public SurveyQuestionResponse getSurveyQuestionResponseById(Integer id);

    public List<SurveyQuestionResponse> getSurveyQuestionResponsesByQuestionId(Integer id);

    public void saveSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse);

    public void deleteSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse);

    public boolean hasDuplicateSurveyQuestionResponseText(SurveyQuestionResponse surveyQuestionResponse);

    public void flush();
}
