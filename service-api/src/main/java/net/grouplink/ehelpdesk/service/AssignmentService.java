package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.filter.AssignmentFilter;

import java.util.List;
import java.util.Map;

public interface AssignmentService {

    Assignment getById(Integer id);

    List<Group> getGroupListByLocationId(Integer id);

    List<Category> getCategoryListByLocationIdAndGroupId(Integer locationId, Integer groupId);

    void save(Assignment assignment);

    boolean delete(Assignment assignment);

    int getCountByCategoryOption(CategoryOption categoryOption);

    List<Assignment> getAssignments(AssignmentFilter assignmentFilter, String queryParam, Integer start, Integer count, Map<String, Boolean> sortFieldMap);

    void saveUniqueAssignment(Assignment assignment);

    int getAssignmentCount(AssignmentFilter assignmentFilter, String queryParam);

    List<UserRoleGroup> getAssignments(Location location, Group group, Category category, CategoryOption categoryOption);

    List<UserRoleGroup> getMoreBetterAssignments(Location location, Group group, Category category, CategoryOption categoryOption);

    List<Assignment> getByCatOptAndLocation(CategoryOption categoryOption, Location location);

    void flush();

    void clear();
}
