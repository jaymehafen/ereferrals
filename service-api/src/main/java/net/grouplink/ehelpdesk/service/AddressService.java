package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.Phone;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 5:13 PM
 */
public interface AddressService {
    void saveAddress(Address address);
    boolean deleteAddress(Address address);
    Address getById(int phoneId);
}
