package net.grouplink.ehelpdesk.service.asset;

public interface AccountingInfoMigrationService {
    /**
     * Converts AccountingInfo instances to ImprovedAccountingInfo instances.
     * This should only happen once during app initialization. The first time
     * this method is invoked it will perform the following steps:
     *  - look up all AccountInfo instances
     *  - create a new ImprovedAccountingInfo instance for each one, mirroring the
     *    properties of the original
     *  - set the ImprovedAccountingInfo on the corresponding Asset
     *  - save the Asset, which cascades to ImprovedAccountingInfo
     *  - delete all of the original AccountingInfo instances from the database
     */
    void convertAllAccountingInfo();

    void removeAssetAccountingInfoForeignKey();
}
