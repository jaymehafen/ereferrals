package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.sf.jasperreports.engine.JasperPrint;

import java.util.List;

public interface ReportService {
    Report getById(Integer id);

    List<Report> getByUser(User user);

    List<Report> getPublicMinusUser(User user);

    void delete(Report report);

    void flush();

    void save(Report report);

    int getUsageCount(Report report);

    List<Report> getByTicketFilter(TicketFilter ticketFilter);

    JasperPrint generateReport(Report report, User user, String drillDownValue);

    JasperPrint generateReport(Report report, User user, String drillDownValue, int drillDownLevel, Integer assignedToId);

    JasperPrint generateReportByDate(Report report, User user, String value, int byDate);
}
