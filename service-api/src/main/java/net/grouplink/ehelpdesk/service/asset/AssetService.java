package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.common.utils.AssetSearch;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.asset.AccountingInfo;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.Vendor;

import java.util.List;
import java.util.Map;

/**
 * @author david garcia
 * @version 1.0
 */
public interface AssetService {
    Asset getAssetById(Integer id);

    Asset getAssetByAssetNumber(String assetNumber);

    List<Asset> getAllAssets();

    List<Asset> searchAsset(AssetSearch assetSearch);

    int getAssetCount(String searchText);

    List<Asset> searchAsset(String searchText, int start, int count, Map<String, Boolean> sortFieldMap);

    List<AssetType> getUniqueTypes();

    List<AssetStatus> getUniqueStatus();

    List<Vendor> getUniqueVendors();

    void saveAsset(Asset asset);

    void deleteAsset(Asset asset);

    Asset getByAssetNumber(String assetNumber);

    void flush();

    List<Asset> getAssetsWithLeaseExpirationDate();

    List<Asset> getAssetsWithWarrantyExpirationDate();

    List<Asset> getAssetsWithAcquisitionDate();

    int getCountByGroup(Group group);

    int getCountByCategory(Category category);

    int getCountByCategoryOption(CategoryOption categoryOption);

    List<AccountingInfo> getAllAccountingInfo(int start, int count);

    int getAccountingInfoCount();

    void deleteAccountingInfoList(List<AccountingInfo> accountingInfos);
}
