package net.grouplink.ehelpdesk.service.asset;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 3, 2008
 * Time: 7:08:42 PM
 */
public interface AssetFilterService {
    /**
     * Returns a list of all the {@link AssetFilter}s in the system.
     *
     * @return a {@link java.util.List} of all the {@link AssetFilter}s in the system.
     */
    List<AssetFilter> getAll();

    /**
     * Returns a list of all the {@link AssetFilter}s in the system that are
     * marked public.
     *
     * @return a {@link List} of all the public {@link AssetFilter}s.
     */
    List<AssetFilter> getAllPublic();

    /**
     * Returns a list of all the {@link AssetFilter}s in the system that are
     * marked public but don't belong to <code>user</code>.
     *
     * @param user the {@link User} whose filters will be excluded
     * @return a {@link List} of all the public {@link AssetFilter}s.
     */
    List<AssetFilter> getPublicMinusUser(User user);

    /**
     * Gets the {@link AssetFilter} that were created by <code>user</code>
     *
     * @param user the {@link User} that owns the {@link AssetFilter}
     * @return a {@link List} of all {@link AssetFilter}s  owned by <code>user</code>
     */
    List<AssetFilter> getByUser(User user);

    /**
     * Gets a {@link AssetFilter} by id
     *
     * @param id the id of the {@link AssetFilter} to return
     * @return the {@link AssetFilter} with matching <code>id</code>
     */
    AssetFilter getById(Integer id);

    /**
     * Saves a {@link AssetFilter} to the database.
     *
     * @param assetFilter the {@link AssetFilter} to save
     */
    void save(AssetFilter assetFilter);

    /**
     * Deletes a {@link AssetFilter} from the database.
     *
     * @param assetFilter the {@link AssetFilter} to delete
     */
    void delete(AssetFilter assetFilter);


    Integer getAssetCount(AssetFilter assetFilter, User user);
    /**
     * Searches for Tickets matching the AssetFilter criteria.
     *
     * @param assetFilter the AssetFilter
     * @param user        the User performing the search
     * @return the Tickets matching the search criteria
     */
    List<Asset> getAssets(AssetFilter assetFilter, User user);

    List<Asset> getAssets(AssetFilter assetFilter, User user, Integer firstResult, Integer maxResult, String sort);
}
