package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.WorkflowStep;
import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;

import java.util.List;
import java.util.Map;

public interface TicketTemplateService {

    Ticket createTicket(TicketTemplate ticketTemplate, Ticket parentTicket, Map<WorkflowStep, WorkflowStepStatus> workflowStepsMap);

    TicketTemplate getById(Integer id);

    List<TicketTemplate> getByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster);

    int getTicketTemplateCountByPriorityId(Integer priorityId);    

    List<TicketTemplate> getTopLevelByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster);

    void save(TicketTemplate ticketTemplate);

    void delete(TicketTemplate ticketTemplate);

    void flush();

    int getCountByGroup(Group group);

    int getCountByCategory(Category category);

    int getCountByCategoryOption(CategoryOption categoryOption);
}
