package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.Attachment;

public interface AttachmentService {

    Attachment getById(Integer id);
    
    void delete(Attachment attachment);
}
