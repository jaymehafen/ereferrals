package net.grouplink.ehelpdesk.service;

import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.List;

public interface WidgetService {

    /**
     * Retrieves a list of all the {@link Widget}s in the system.
     * @return a {@link List} of all the {@link Widget}s in the system.
     */
    List<Widget> getAll();

    /**
     * Gets a {@link Widget} by id
     * @param id the id of the {@link Widget} to return
     * @return the {@link Widget} with matching <code>id</code>
     */
    Widget getById(Integer id);

    /**
     * Saves a {@link Widget} to the database.
     * @param widget the {@link Widget} to save
     */
    void save(Widget widget);

    /**
     * Deletes a {@link Widget} to the database
     * @param widget the {@link Widget} to delete
     */
    void delete(Widget widget);

    List<Widget> getByTicketFilter(TicketFilter filter);
}
