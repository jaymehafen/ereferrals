package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.dial.DialBackground;
import org.jfree.chart.plot.dial.DialCap;
import org.jfree.chart.plot.dial.DialPlot;
import org.jfree.chart.plot.dial.DialPointer;
import org.jfree.chart.plot.dial.DialTextAnnotation;
import org.jfree.chart.plot.dial.DialValueIndicator;
import org.jfree.chart.plot.dial.StandardDialFrame;
import org.jfree.chart.plot.dial.StandardDialRange;
import org.jfree.chart.plot.dial.StandardDialScale;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.StandardGradientPaintTransformer;

import java.awt.*;

public class Dial {

    public static JFreeChart getChart(Widget widget, Integer userId) {
//        TicketFilter ticketFilter = widget.getTicketFilter();
        TicketFilter ticketFilter = widget.getReport().getTicketFilter();

        Integer count = DashboardAppUtils.getDashboardService().getTicketCount(ticketFilter.getId(), userId);

        return getCustomChart(count, widget);
    }

    public static JFreeChart getCustomChart(Integer count, Widget widget) {

        DialPlot dialPlot = new DialPlot();
        dialPlot.setDataset(new DefaultValueDataset(count));
        dialPlot.setDialFrame(new StandardDialFrame());

        DialTextAnnotation dialTextAnnotation = new DialTextAnnotation(DashboardAppUtils.getMessage("ticket.header"));
        dialTextAnnotation.setFont(new Font("Dialog", 1, 14));
        dialTextAnnotation.setRadius(0.69999999999999996D);
        dialPlot.addLayer(dialTextAnnotation);

        dialPlot.addLayer(new DialValueIndicator(0));

        StandardDialScale standardDialScale;
        if (widget.getMinimum() != null && widget.getMaximum() != null) {
            standardDialScale = new StandardDialScale(widget.getMinimum(), widget.getMaximum(), -120D, -300D, 10D, 4);
        } else {
            standardDialScale = new StandardDialScale();
            standardDialScale.setStartAngle(-120D);
            standardDialScale.setExtent(-300D);
            standardDialScale.setMajorTickIncrement(10D);
            standardDialScale.setMinorTickCount(4);

        }
        standardDialScale.setTickRadius(0.88D);
        standardDialScale.setTickLabelOffset(0.14999999999999999D);
        standardDialScale.setTickLabelFont(new Font("Dialog", 0, 14));
        dialPlot.addScale(0, standardDialScale);

        DialCap dialCap = new DialCap();
        dialPlot.setCap(dialCap);

        if (widget.getRedRangeBegin() != null && widget.getRedRangeEnd() != null) {
            StandardDialRange standarddialrange = new StandardDialRange(widget.getRedRangeBegin(), widget.getRedRangeEnd(), Color.red);
            standarddialrange.setInnerRadius(0.52000000000000002D);
            standarddialrange.setOuterRadius(0.55000000000000004D);
            dialPlot.addLayer(standarddialrange);
        }

        if (widget.getYellowRangeBegin() != null && widget.getYellowRangeEnd() != null) {
            StandardDialRange standarddialrange1 = new StandardDialRange(widget.getYellowRangeBegin(), widget.getYellowRangeEnd(), Color.orange);
            standarddialrange1.setInnerRadius(0.52000000000000002D);
            standarddialrange1.setOuterRadius(0.55000000000000004D);
            dialPlot.addLayer(standarddialrange1);
        }

        if (widget.getGreenRangeBegin() != null && widget.getGreenRangeEnd() != null) {
            StandardDialRange standarddialrange2 = new StandardDialRange(widget.getGreenRangeBegin(), widget.getGreenRangeEnd(), Color.green);
            standarddialrange2.setInnerRadius(0.52000000000000002D);
            standarddialrange2.setOuterRadius(0.55000000000000004D);
            dialPlot.addLayer(standarddialrange2);
        }

        GradientPaint gradientpaint = new GradientPaint(new Point(), new Color(255, 255, 255), new Point(), new Color(170, 170, 220));
        DialBackground dialbackground = new DialBackground(gradientpaint);
        dialbackground.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.VERTICAL));
        dialPlot.setBackground(dialbackground);

        DialPointer.Pointer pointer = new DialPointer.Pointer();
        pointer.setFillPaint(Color.yellow);
        dialPlot.addPointer(pointer);

        JFreeChart jFreeChart = new JFreeChart(dialPlot);
        jFreeChart.setTitle(widget.getName());
        return jFreeChart;
    }

}
