package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.constants.JasperReportsConstants;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.*;
import java.util.Map;

public class StackedBar {

    public static JFreeChart getChart(Widget widget, Integer userId) {

//        TicketFilter ticketFilter = widget.getTicketFilter();
        TicketFilter ticketFilter = widget.getReport().getTicketFilter();

        String groupByName;
//        switch (ticketFilter.getGroupBy()) {
        switch (widget.getReport().getGroupBy()) {
            case JasperReportsConstants.GROUP:
                groupByName = DashboardAppUtils.getMessage("ticket.group");
                break;
            case JasperReportsConstants.LOCATION:
                groupByName = DashboardAppUtils.getMessage("ticket.location");
                break;
            case JasperReportsConstants.CATEGORY:
                groupByName = DashboardAppUtils.getMessage("ticket.category");
                break;
            case JasperReportsConstants.CATEGORY_OPTION:
                groupByName = DashboardAppUtils.getMessage("ticket.categoryOption");
                break;
            case JasperReportsConstants.TECHNICIAN:
                groupByName = DashboardAppUtils.getMessage("ROLE_TECHNICIAN");
                break;
            case JasperReportsConstants.PRIORITY:
                groupByName = DashboardAppUtils.getMessage("ticket.priority");
                break;
            case JasperReportsConstants.STATUS:
                groupByName = DashboardAppUtils.getMessage("ticket.status");
                break;
            default:
                groupByName = null;
                break;
        }

//        Map<String, Map<String, Integer>> groupByByMap = DashboardAppUtils.getDashboardService().getTicketCountTwoGroups(ticketFilter.getId(), userId);
        Map<String, Map<String, Integer>> groupByByMap = DashboardAppUtils.getDashboardService().getTicketCountTwoGroups(widget.getId(), userId);

        return getCustomChart(widget, groupByByMap, groupByName);
    }

    private static JFreeChart getCustomChart(Widget widget, Map<String, Map<String, Integer>> groupByByMap, String groupByName) {

        CategoryDataset categoryDataset = createDataset(groupByByMap);

        JFreeChart jfreechart = ChartFactory.createStackedBarChart(widget.getName(), groupByName,
                DashboardAppUtils.getMessage("ticket.header"), categoryDataset, PlotOrientation.VERTICAL, true, true, false);

        CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();
        categoryplot.setBackgroundPaint(null);
        categoryplot.setRangeGridlinePaint(Color.gray);
        categoryplot.setDataset(categoryDataset);

        StackedBarRenderer stackedbarrenderer = (StackedBarRenderer) categoryplot.getRenderer();
        stackedbarrenderer.setDrawBarOutline(false);
        stackedbarrenderer.setBaseItemLabelsVisible(true);
        stackedbarrenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());

        NumberAxis numberaxis = (NumberAxis) categoryplot.getRangeAxis();
        numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        if (widget.getMinimum() != null && widget.getMaximum() != null) {
            numberaxis.setRange(widget.getMinimum(), widget.getMaximum());
        }
        
        numberaxis.setTickMarkPaint(Color.black);

        return jfreechart;
    }

    private static CategoryDataset createDataset(Map<String, Map<String, Integer>> groupByByMap) {

        DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();
        for (Map.Entry<String, Map<String, Integer>> entry : groupByByMap.entrySet()) {
            for (Map.Entry<String, Integer> byEntry : entry.getValue().entrySet()) {
                defaultCategoryDataset.addValue(byEntry.getValue(), byEntry.getKey(), entry.getKey());
            }
        }
        return defaultCategoryDataset;
    }
}
