package net.grouplink.ehelpdesk.dashboard.charts;

import net.grouplink.ehelpdesk.dashboard.DashboardAppUtils;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.constants.JasperReportsConstants;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.StandardGradientPaintTransformer;

import java.awt.*;
import java.util.Map;

public class Bar {

    static class CustomBarRenderer extends BarRenderer {

        public Paint getItemPaint(int i, int j) {
            return colors[j % colors.length];
        }

        private Paint colors[];

        public CustomBarRenderer(Paint apaint[]) {
            colors = apaint;
        }
    }

    public static JFreeChart getChart(Widget widget, Integer userId) {

//        TicketFilter ticketFilter = widget.getReport().getTicketFilter();

        Map<String, Integer> groupByMap = DashboardAppUtils.getDashboardService().getTicketCountOneGroup(widget.getId(), userId);

        String groupByName;
//        switch (ticketFilter.getGroupBy()) {
        switch (widget.getReport().getGroupBy()) {
            case JasperReportsConstants.GROUP:
                groupByName = DashboardAppUtils.getMessage("ticket.group");
                break;
            case JasperReportsConstants.LOCATION:
                groupByName = DashboardAppUtils.getMessage("ticket.location");
                break;
            case JasperReportsConstants.CATEGORY:
                groupByName = DashboardAppUtils.getMessage("ticket.category");
                break;
            case JasperReportsConstants.CATEGORY_OPTION:
                groupByName = DashboardAppUtils.getMessage("ticket.categoryOption");
                break;
            case JasperReportsConstants.TECHNICIAN:
                groupByName = DashboardAppUtils.getMessage("ROLE_TECHNICIAN");
                break;
            case JasperReportsConstants.PRIORITY:
                groupByName = DashboardAppUtils.getMessage("ticket.priority");
                break;
            case JasperReportsConstants.STATUS:
                groupByName = DashboardAppUtils.getMessage("ticket.status");
                break;
            default:
                groupByName = null;
                break;
        }

        return getCustomChart(widget, groupByMap, groupByName);
    }

    private static JFreeChart getCustomChart(Widget widget, Map<String, Integer> groupByMap, String groupByName) {
        CategoryDataset categoryDataset = createDataset(groupByMap);

        JFreeChart jfreechart = ChartFactory.createBarChart(widget.getName(), groupByName, DashboardAppUtils.getMessage("ticket.header"), categoryDataset, PlotOrientation.VERTICAL, false, true, false);

        CategoryPlot categoryPlot = (CategoryPlot) jfreechart.getPlot();
        categoryPlot.setNoDataMessage(DashboardAppUtils.getMessage("dashboard.widget.noData"));
        categoryPlot.setBackgroundPaint(null);
        categoryPlot.setInsets(new RectangleInsets(10D, 5D, 5D, 5D));
        categoryPlot.setOutlinePaint(Color.black);
        categoryPlot.setRangeGridlinePaint(Color.gray);
        categoryPlot.setRangeGridlineStroke(new BasicStroke(1.0F));

        Paint paintArray[] = createPaint();
        CustomBarRenderer custombarrenderer = new CustomBarRenderer(paintArray);
        custombarrenderer.setDrawBarOutline(true);
        custombarrenderer.setGradientPaintTransformer(new StandardGradientPaintTransformer(GradientPaintTransformType.CENTER_HORIZONTAL));
        categoryPlot.setRenderer(custombarrenderer);

        NumberAxis numberAxis = (NumberAxis) categoryPlot.getRangeAxis();
        numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        if (widget.getMinimum() != null && widget.getMaximum() != null) {
            numberAxis.setRange(widget.getMinimum(), widget.getMaximum());
        }
        
        numberAxis.setTickMarkPaint(Color.black);

        return jfreechart;
    }

    private static Paint[] createPaint() {

        Paint paintArray[] = new Paint[12];

        paintArray[0] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.red);
        paintArray[1] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.green);
        paintArray[2] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.blue);
        paintArray[3] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.orange);
        paintArray[4] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.magenta);
        paintArray[5] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.lightGray);
        paintArray[6] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.gray);
        paintArray[7] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.darkGray);
        paintArray[8] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.black);
        paintArray[9] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.pink);
        paintArray[10] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.yellow);
        paintArray[11] = new GradientPaint(0.0F, 0.0F, Color.white, 0.0F, 0.0F, Color.cyan);

        return paintArray;
    }

    private static CategoryDataset createDataset(Map<String, Integer> groupByMap) {

        DefaultCategoryDataset defaultCategoryDataset = new DefaultCategoryDataset();
        for (Map.Entry<String, Integer> entry : groupByMap.entrySet()) {
            defaultCategoryDataset.addValue(entry.getValue(), DashboardAppUtils.getMessage("ticket.header"), entry.getKey());
        }
        return defaultCategoryDataset;
    }
}
