package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwOpportunityFilterPropertyDetail generated by hbm2java
 */
public class CwOpportunityFilterPropertyDetail implements java.io.Serializable {

	// Fields    

	private int olKey;

	private int olPiKey;

	private int olOfKey;

	private String olOperator;

	// Constructors

	/** default constructor */
	public CwOpportunityFilterPropertyDetail() {
	}

	/** full constructor */
	public CwOpportunityFilterPropertyDetail(int olKey, int olPiKey,
			int olOfKey, String olOperator) {
		this.olKey = olKey;
		this.olPiKey = olPiKey;
		this.olOfKey = olOfKey;
		this.olOperator = olOperator;
	}

	// Property accessors
	public int getOlKey() {
		return this.olKey;
	}

	public void setOlKey(int olKey) {
		this.olKey = olKey;
	}

	public int getOlPiKey() {
		return this.olPiKey;
	}

	public void setOlPiKey(int olPiKey) {
		this.olPiKey = olPiKey;
	}

	public int getOlOfKey() {
		return this.olOfKey;
	}

	public void setOlOfKey(int olOfKey) {
		this.olOfKey = olOfKey;
	}

	public String getOlOperator() {
		return this.olOperator;
	}

	public void setOlOperator(String olOperator) {
		this.olOperator = olOperator;
	}

}
