package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwRbTable generated by hbm2java
 */
public class CwRbTable implements java.io.Serializable {

	// Fields    

	private String taTablename;

	private String taTablealias;

	// Constructors

	/** default constructor */
	public CwRbTable() {
	}

	/** full constructor */
	public CwRbTable(String taTablename, String taTablealias) {
		this.taTablename = taTablename;
		this.taTablealias = taTablealias;
	}

	// Property accessors
	public String getTaTablename() {
		return this.taTablename;
	}

	public void setTaTablename(String taTablename) {
		this.taTablename = taTablename;
	}

	public String getTaTablealias() {
		return this.taTablealias;
	}

	public void setTaTablealias(String taTablealias) {
		this.taTablealias = taTablealias;
	}

}
