package net.grouplink.ehelpdesk.migration.dao;

import org.hibernate.ScrollableResults;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 2, 2007
 * Time: 9:28:57 PM
 */
public interface OldCwHdDao  {
    ScrollableResults getOldTickets();
    void flushAndReset();
    int getTicketCount();
}
