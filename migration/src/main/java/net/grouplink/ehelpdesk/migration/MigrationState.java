package net.grouplink.ehelpdesk.migration;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeListener;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: Nov 19, 2007
 * Time: 4:17:03 PM
 */
public class MigrationState {

    public static final int ASSIGNMENTS_MESSAGE = 0;
    public static final int POLISHING_MESSAGE = 1;
    public static final int USERS_MESSAGE = 2;
    public static final int KBA_MESSAGE = 3;
    public static final int TICKETS_MESSAGE = 4;
    public static final int USERS_ADMIN_MESSAGE = 5;
    public static final int USERS_TECHS_MESSAGE = 6;
    public static final int NEWGROUP_MESSAGE = 7;
    public static final int LOCATIONS_MESSAGE = 8;
    public static final int CATEGORIES_MESSAGE = 9;
    public static final int CATOPTS_MESSAGE = 10;
    public static final int ATTACHMENTS_MESSAGE = 11;

    // the index of the strings in this array must match the int
    // value of the ints above for the corresponding message
    private static String[] messages = {
            "Migrating Assignments", "Polishing Ticket History",
            "Migrating Users", "Migration Knowledge Base Articles",
            "Migrating Tickets", "Migrating Administrators",
            "Migrating Technicians", "Creating new group",
            "Migrating Locations", "Migrating Categories",
            "Migrating Category Options", "Uploading attachments"
    };

    private PropertyChangeSupport support = new PropertyChangeSupport(this);

    private int message = -1;
    private Integer itemCount;
    private Integer totalItems;
    private boolean finished;

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        int oldVal = this.message;
        this.message = message;
        support.firePropertyChange("message", oldVal, message);
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        Integer oldVal = this.itemCount;
        this.itemCount = itemCount;
        support.firePropertyChange("itemCount", oldVal, itemCount);
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        Integer oldVal = this.totalItems;
        this.totalItems = totalItems;
        support.firePropertyChange("totalItems", oldVal, totalItems);
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        boolean oldVal = this.finished;
        this.finished = finished;
        support.firePropertyChange("finished", oldVal, finished);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    public static String getMessageString(Integer index){
        return messages[index.intValue()];
    }
}
