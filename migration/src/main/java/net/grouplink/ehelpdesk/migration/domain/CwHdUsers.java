package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwHdUsers generated by hbm2java
 */
public class CwHdUsers implements java.io.Serializable {

	// Fields    

	private CwHdUsersId id;

	// Constructors

	/** default constructor */
	public CwHdUsers() {
	}

	/** full constructor */
	public CwHdUsers(CwHdUsersId id) {
		this.id = id;
	}

	// Property accessors
	public CwHdUsersId getId() {
		return this.id;
	}

	public void setId(CwHdUsersId id) {
		this.id = id;
	}

}
