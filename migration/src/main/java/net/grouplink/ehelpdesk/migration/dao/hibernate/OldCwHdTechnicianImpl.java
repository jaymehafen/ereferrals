package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldCwHdTechnician;
import net.grouplink.ehelpdesk.migration.domain.CwHdTechnician;
import org.apache.commons.lang.StringUtils;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 4, 2007
 * Time: 12:15:06 AM
 */
public class OldCwHdTechnicianImpl extends HibernateDaoSupport implements OldCwHdTechnician {

    public CwHdTechnician getCwHdTechnicianByEmail(String email) {
        List list = getHibernateTemplate().findByNamedParam("select t from CwHdTechnician as t where t.hteEmail = :email and t.hteKey > 0", "email", email);
        if(list.size() > 0){
            return (CwHdTechnician) list.get(0);    
        }
        return null;
    }

    public CwHdTechnician getCwHdTechnicianByName(String name) {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwHdTechnician.class);
        cri.add(Restrictions.like("hteName", name));
        List list = cri.list();
        if(list.isEmpty()){
            String[] temp = StringUtils.stripAll(StringUtils.split(name), ",.-");
            String tempText = "";
            if(temp != null && temp.length > 0){
                for(int i=0;i<temp.length;i++){
                    tempText += "%" + temp[i] + "%";
                }
            }
            cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwHdTechnician.class);
            cri.add(Restrictions.like("hteName", tempText));
            list = cri.list();
        }
        else{
            CwHdTechnician cwTech = (CwHdTechnician) list.get(0);
            return cwTech;
        }
        return null;
    }

    public ScrollableResults getAllTechs() {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwHdTechnician.class);
        cri.add(Restrictions.gt("hteKey", new Integer(0)));
        cri.setCacheMode(CacheMode.IGNORE);
        return cri.scroll(ScrollMode.FORWARD_ONLY);
//        return getHibernateTemplate().find("select t from CwHdTechnician as t where t.hteKey > 0");
    }

    public int getAllTechCount() {
        return DataAccessUtils.intResult(getHibernateTemplate().find("select count(*) from CwHdTechnician"));
    }

    public void flushAndReset() {
        getHibernateTemplate().flush();
        getHibernateTemplate().clear();
    }
}
