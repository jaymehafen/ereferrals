/*
 * MigrationDBPanel.java
 *
 * Created on November 13, 2007, 11:10 PM
 */

package net.grouplink.ehelpdesk.migration.gui;

import net.grouplink.ehelpdesk.migration.gui.wizard.Wizard;
import org.apache.commons.lang.StringUtils;

import java.awt.*;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author mrollins
 */
public class MigrationDBPanel extends javax.swing.JPanel {

    private boolean connectionSuccessful = false;
    private Wizard wizard;

    /**
     * Creates new form MigrationDBPanel
     */
    public MigrationDBPanel() {
        initComponents();
    }

    public void addTestButtonActionListener(ActionListener l) {
        btnTestConnection.addActionListener(l);
    }

    public javax.swing.JButton getTestButton() {
        return btnTestConnection;
    }

    private void resetDefaultPort() {
        int cbChoice = cboDBPlatform.getSelectedIndex();
        switch (cbChoice) {
            case DBSelectionPanel.MYSQL:
                lblDefPort.setText("3306");
                break;
            case DBSelectionPanel.POSTGRESQL:
                lblDefPort.setText("5432");
                break;
            case DBSelectionPanel.SYBASE:
                lblDefPort.setText("5000");
                break;
            case DBSelectionPanel.ORACLE:
            case DBSelectionPanel.ORACLE8I:
            case DBSelectionPanel.ORACLE9I:
            case DBSelectionPanel.ORACLE10G:
            case DBSelectionPanel.ORACLE11G:
                lblDefPort.setText("1521");
                break;
            case DBSelectionPanel.MSSQL:
                lblDefPort.setText("1433");
                break;
            case DBSelectionPanel.HYPERSONIC:
                lblDefPort.setText("");
                break;
            default:
                lblDefPort.setText("");
                break;
        }

        txtFldPort.setText(lblDefPort.getText());
    }

    private void writeURL() {
        writeURL(null, null, null, null);
    }

    private void writeURL(Character h, Character p, Character d, Character u) {
        setConnectionSuccessful(false);
        wizard.setNextFinishButtonEnabled(false);

        lblConnStatus.setText(null);
        lblDBName.setText("Database:");

        if (h != null && h.charValue() == '\u0008') {
            h = new Character(' ');
        }
        if (p != null && p.charValue() == '\u0008') {
            p = new Character(' ');
        }
        if (d != null && d.charValue() == '\u0008') {
            d = new Character(' ');
        }
        if (u != null && u.charValue() == '\u0008') {
            u = new Character(' ');
        }
        String host = ((StringUtils.isBlank(txtFldHost.getText()) && h == null) ? "<host>" : txtFldHost.getText() + (h == null ? "" : h.toString())).trim();
        String port = ((StringUtils.isBlank(txtFldPort.getText()) && p == null) ? "<port>" : txtFldPort.getText() + (p == null ? "" : p.toString())).trim();
        String db = ((StringUtils.isBlank(txtFldSchema.getText()) && d == null) ? "<database>" : txtFldSchema.getText() + (d == null ? "" : d.toString())).trim();

        int cbChoice = cboDBPlatform.getSelectedIndex();
        StringBuilder sb = new StringBuilder();
        switch (cbChoice) {
            case DBSelectionPanel.MYSQL:
                lblDefPort.setText("3306");
                sb.append("jdbc:mysql://").append(host).append(":").append(port).append("/").append(db);
                break;
            case DBSelectionPanel.POSTGRESQL:
                lblDefPort.setText("5432");
                sb.append("jdbc:postgresql://").append(host).append(":").append(port).append("/").append(db);
                break;
            case DBSelectionPanel.SYBASE:
                lblDefPort.setText("5000");
                sb.append("jdbc:jtds:sybase://").append(host).append(":").append(port).append(";DatabaseName=").append(db);
                break;
            case DBSelectionPanel.ORACLE:
            case DBSelectionPanel.ORACLE8I:
            case DBSelectionPanel.ORACLE9I:
            case DBSelectionPanel.ORACLE10G:
            case DBSelectionPanel.ORACLE11G:
                lblDBName.setText("SID:");
                lblDefPort.setText("1521");
                sb.append("jdbc:oracle:thin:@").append(host).append(":").append(port).append(":").append(db.equals("<database>") ? "<sid>" : db);
                break;
            case DBSelectionPanel.MSSQL:
                lblDefPort.setText("1433");
                sb.append("jdbc:jtds:sqlserver://").append(host).append(":").append(port).append("/").append(db);
                break;
            case DBSelectionPanel.HYPERSONIC:
                lblDefPort.setText("");
                lblDBName.setText("HSQL File:");
                sb.append("jdbc:hsqldb:file:").append(db.equals("<database>") ? "<hsql file>" : db);
                break;
            default:
                lblDefPort.setText("");
                this.btnTestConnection.setEnabled(false);
                break;
        }

        lblURLTemplate.setText(sb.toString().trim());

        // Check that the test button can be enabled
        btnTestConnection.setEnabled(StringUtils.isNotBlank(txtFldHost.getText()) && StringUtils.isNotBlank(txtFldPort.getText()) && StringUtils.isNotBlank(txtFldSchema.getText()) && (StringUtils.isNotBlank(txtFldUser.getText()) || (u != null && u.charValue() != ' ')));
    }


    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        lblURLTemplate = new javax.swing.JLabel();
        lblChooseDB = new javax.swing.JLabel();
        cboDBPlatform = new javax.swing.JComboBox();
        lblHost = new javax.swing.JLabel();
        txtFldHost = new javax.swing.JTextField();
        lblPort = new javax.swing.JLabel();
        txtFldPort = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        lblDefPort = new javax.swing.JLabel();
        lblDBName = new javax.swing.JLabel();
        txtFldSchema = new javax.swing.JTextField();
        lblUser = new javax.swing.JLabel();
        txtFldUser = new javax.swing.JTextField();
        lblPassword = new javax.swing.JLabel();
        txtFldPassword = new javax.swing.JPasswordField();
        btnTestConnection = new javax.swing.JButton();
        lblConnStatus = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setText("JDBC URL:");

        lblURLTemplate.setForeground(new java.awt.Color(0, 102, 204));
        lblURLTemplate.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblChooseDB.setText("Platform:");

        cboDBPlatform.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " ", "MySQL", "Oracle", "Oracle 8i", "Oracle 9i", "Oracle 10g", "Oracle 11g", "PostgreSQL", "Microsoft SQL Server", "Sybase" }));
        cboDBPlatform.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboDBPlatformActionPerformed(evt);
            }
        });

        lblHost.setText("Host:");

        txtFldHost.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFldHostKeyTyped(evt);
            }
        });

        lblPort.setText("Port:");

        txtFldPort.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFldPortKeyTyped(evt);
            }
        });

        jLabel3.setForeground(new java.awt.Color(153, 153, 153));
        jLabel3.setText("Default:");

        lblDefPort.setForeground(new java.awt.Color(153, 153, 153));

        lblDBName.setText("Database:");

        txtFldSchema.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFldSchemaKeyTyped(evt);
            }
        });

        lblUser.setText("Username:");

        txtFldUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFldUserKeyTyped(evt);
            }
        });

        lblPassword.setText("Password:");

        btnTestConnection.setText("Test Connection");
        btnTestConnection.setEnabled(false);
        btnTestConnection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestConnectionActionPerformed(evt);
            }
        });

        lblConnStatus.setForeground(new java.awt.Color(204, 0, 0));

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(lblURLTemplate, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE))
                    .add(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jLabel9))
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(63, 63, 63)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, lblPassword)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, lblUser)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, lblDBName)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, lblPort)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, lblHost)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, lblChooseDB))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(txtFldPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 55, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jLabel3)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(lblDefPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 32, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(txtFldSchema)
                            .add(txtFldPassword)
                            .add(txtFldUser, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                            .add(cboDBPlatform, 0, 181, Short.MAX_VALUE)
                            .add(txtFldHost)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(lblConnStatus, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
                        .add(10, 10, 10)
                        .add(btnTestConnection)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblChooseDB)
                    .add(cboDBPlatform, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblHost)
                    .add(txtFldHost, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblPort)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(txtFldPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel3)
                        .add(lblDefPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblDBName)
                    .add(txtFldSchema, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblUser)
                    .add(txtFldUser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblPassword)
                    .add(txtFldPassword, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(22, 22, 22)
                .add(jLabel9)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(lblURLTemplate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnTestConnection)
                    .add(lblConnStatus, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 55, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setText("Provide the database connection information for the previous version.");

        jLabel2.setText("This utility migrates version 6 databases to version 7 and above.");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel2)
                    .add(jLabel1)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel2)
                .add(18, 18, 18)
                .add(jLabel1)
                .add(18, 18, 18)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cboDBPlatformActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboDBPlatformActionPerformed
        resetDefaultPort();
        writeURL();
        txtFldPort.setText(lblDefPort.getText());
    }//GEN-LAST:event_cboDBPlatformActionPerformed

    private void txtFldHostKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFldHostKeyTyped
        writeURL(new Character(evt.getKeyChar()), null, null, null);
    }//GEN-LAST:event_txtFldHostKeyTyped

    private void txtFldPortKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFldPortKeyTyped
        writeURL(null, new Character(evt.getKeyChar()), null, null);
    }//GEN-LAST:event_txtFldPortKeyTyped

    private void txtFldSchemaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFldSchemaKeyTyped
        writeURL(null, null, new Character(evt.getKeyChar()), null);
    }//GEN-LAST:event_txtFldSchemaKeyTyped

    private void txtFldUserKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFldUserKeyTyped
        writeURL(null, null, null, new Character(evt.getKeyChar()));
    }//GEN-LAST:event_txtFldUserKeyTyped

    private void btnTestConnectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestConnectionActionPerformed
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        lblConnStatus.setText(null);

        // Register JDBC drivers
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("oracle.jdbc.OracleDriver");
            Class.forName("org.postgresql.Driver");
            Class.forName("com.sybase.jdbc4.jdbc.SybDriver");
//            Class.forName("org.hsqldb.jdbcDriver");
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        String url = lblURLTemplate.getText();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, txtFldUser.getText(),
                    new String(txtFldPassword.getPassword()));
        } catch (SQLException sex) {
            StringBuilder errMsg = new StringBuilder("<html>Unable to connect: ");
            String sexMsg = sex.getMessage();
            if (sexMsg.indexOf("UnknownHostException") != -1) {
                errMsg.append("Host not found. (").append(txtFldHost.getText())
                        .append(")");
            } else if (sexMsg.indexOf("SocketException") != -1) {
                errMsg.append("Couldn't connect to port ").append(txtFldPort.getText());
            } else {
                errMsg.append(sex.getMessage());
            }
            errMsg.append("</html>");
            lblConnStatus.setText(errMsg.toString());
            setConnectionSuccessful(false);
            return;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sex) {
                sex.printStackTrace();
            }
            // always set the cursor back to default
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        lblConnStatus.setText("Connection successful.");
        setConnectionSuccessful(true);
    }//GEN-LAST:event_btnTestConnectionActionPerformed

    public boolean isConnectionSuccessful() {
        return connectionSuccessful;
    }

    public void setConnectionSuccessful(boolean connectionSuccessful) {
        this.connectionSuccessful = connectionSuccessful;
    }

    public Wizard getWizard() {
        return wizard;
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnTestConnection;
    public javax.swing.JComboBox cboDBPlatform;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblChooseDB;
    private javax.swing.JLabel lblConnStatus;
    private javax.swing.JLabel lblDBName;
    private javax.swing.JLabel lblDefPort;
    private javax.swing.JLabel lblHost;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblPort;
    public javax.swing.JLabel lblURLTemplate;
    private javax.swing.JLabel lblUser;
    public javax.swing.JTextField txtFldHost;
    public javax.swing.JPasswordField txtFldPassword;
    public javax.swing.JTextField txtFldPort;
    public javax.swing.JTextField txtFldSchema;
    public javax.swing.JTextField txtFldUser;
    // End of variables declaration//GEN-END:variables

}
