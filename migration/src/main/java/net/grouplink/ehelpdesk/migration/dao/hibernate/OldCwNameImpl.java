package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldCwName;
import net.grouplink.ehelpdesk.migration.domain.CwName;

import java.util.List;
import java.util.ArrayList;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.apache.commons.lang.StringUtils;


/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 4, 2007
 * Time: 12:31:08 AM
 */
public class OldCwNameImpl extends HibernateDaoSupport implements OldCwName {

    public List getCwNameByDisplayName(String displayName) {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwName.class);
        cri.add(Restrictions.eq("naDisplayname", displayName));
        cri.createAlias("cwAddressList", "cwAddress").add(Restrictions.isNotNull("cwAddress.adEmail"));
        return cri.list();
    }

    public List getCwNameByFirstAndLastName(String firstName, String lastName) {
        if(StringUtils.isBlank(firstName) && StringUtils.isBlank(firstName)){
            return new ArrayList();    
        }
        else{
            Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwName.class);
            cri.add(Restrictions.like("naFname", "%" + firstName + "%"));
            cri.add(Restrictions.like("naLname", "%" + lastName + "%"));
            cri.createAlias("cwAddressList", "cwAddress").add(Restrictions.isNotNull("cwAddress.adEmail"));
            return cri.list();
        }

    }

    public void flushAndReset() {
        getHibernateTemplate().flush();
        getHibernateTemplate().clear();
    }
}
