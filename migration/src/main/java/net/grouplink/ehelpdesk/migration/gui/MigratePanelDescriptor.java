/*
 * MigratePanelDescriptor.java
 * 
 * Created on Nov 14, 2007, 12:12:40 AM
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.grouplink.ehelpdesk.migration.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import net.grouplink.ehelpdesk.migration.gui.wizard.WizardPanelDescriptor;

/**
 *
 * @author mrollins
 */
public class MigratePanelDescriptor extends WizardPanelDescriptor implements ActionListener {
    public static final String IDENTIFIER = "MIGRATE_PANEL";
    MigratePanel migratePanel;
    
    public MigratePanelDescriptor(){
        migratePanel = new MigratePanel();
        
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(migratePanel);
    }
    
    public Object getNextPanelDescriptor() {
        return FINISH;
    }

    public Object getBackPanelDescriptor() {
        return DBSelectionPanelDescriptor.IDENTIFIER;
    }

    public void aboutToDisplayPanel() {
        if(migratePanel.getWizard() == null) 
            migratePanel.setWizard(getWizard());
        setNextButtonIfMigrateOK();
        migratePanel.resetPanel();
    }

    public void actionPerformed(ActionEvent e) {
        setNextButtonIfMigrateOK();
    }

    private void setNextButtonIfMigrateOK(){
        getWizard().setNextFinishButtonEnabled(migratePanel.isMigrationSuccessful());
    }
}
