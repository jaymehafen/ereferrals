package net.grouplink.ehelpdesk.migration.dao;

import java.util.List;

public interface CwHdLcLinkDao {
	
	public List getLocationIdByCatId(int id);
	public List getCategoryIdByLocId(int id);
}
