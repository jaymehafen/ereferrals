package net.grouplink.ehelpdesk.migration.dao;

import java.util.List;

import net.grouplink.ehelpdesk.migration.domain.CwHdCategory;

public interface OldCategoryDao {

	public List getCategory();
	public CwHdCategory getCategoryById(Integer id);
}
