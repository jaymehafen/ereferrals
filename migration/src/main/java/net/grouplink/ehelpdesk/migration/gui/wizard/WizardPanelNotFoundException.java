/*
 * WizardPanelNotFoundException.java
 *
 * Created on Oct 1, 2007, 1:24:47 PM
 *
 */

package net.grouplink.ehelpdesk.migration.gui.wizard;

/**
 *
 * @author mark
 */
public class WizardPanelNotFoundException extends RuntimeException {

    public WizardPanelNotFoundException() {
        super();
    }
}