package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldCwHdDao;
import net.grouplink.ehelpdesk.migration.domain.CwHd;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Order;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 2, 2007
 * Time: 9:31:26 PM
 */
public class OldCwHdDaoImpl extends HibernateDaoSupport implements OldCwHdDao {

    public int getTicketCount() {
        List list = getHibernateTemplate().find("select count(*) from CwHd");
        return ((Long)list.get(0)).intValue();
    }

    public ScrollableResults getOldTickets() {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwHd.class);
//        cri.addOrder(Order.asc("htiKey"));
        cri.setCacheMode(CacheMode.IGNORE);
        return cri.scroll(ScrollMode.FORWARD_ONLY);
    }

    public void flushAndReset() {
        getHibernateTemplate().flush();
        getHibernateTemplate().clear();
    }
}
