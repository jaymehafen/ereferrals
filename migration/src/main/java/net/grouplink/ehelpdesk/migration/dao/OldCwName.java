package net.grouplink.ehelpdesk.migration.dao;

import net.grouplink.ehelpdesk.migration.domain.CwName;

import java.util.List;

import org.hibernate.ScrollableResults;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 4, 2007
 * Time: 12:28:35 AM
 */
public interface OldCwName {

    public List getCwNameByDisplayName(String displayName);

    public List getCwNameByFirstAndLastName(String firstName, String lastName);

    void flushAndReset();
}
