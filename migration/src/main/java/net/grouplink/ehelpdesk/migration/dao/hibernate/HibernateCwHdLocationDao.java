package net.grouplink.ehelpdesk.migration.dao.hibernate;

import java.util.List;

import net.grouplink.ehelpdesk.migration.dao.CwHdLocationDao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateCwHdLocationDao extends HibernateDaoSupport implements
        CwHdLocationDao {

	public List getLocations() {
		return getHibernateTemplate().find("select c from CwHdLocation as c");
	}

}
