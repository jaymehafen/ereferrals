package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwHdReports generated by hbm2java
 */
public class CwHdReports implements java.io.Serializable {

	// Fields    

	private int hrpKey;

	private String hrpType;

	private String hrpDatetype;

	private String hrpName;

	private String hrpLink;

	// Constructors

	/** default constructor */
	public CwHdReports() {
	}

	/** minimal constructor */
	public CwHdReports(int hrpKey) {
		this.hrpKey = hrpKey;
	}

	/** full constructor */
	public CwHdReports(int hrpKey, String hrpType, String hrpDatetype,
			String hrpName, String hrpLink) {
		this.hrpKey = hrpKey;
		this.hrpType = hrpType;
		this.hrpDatetype = hrpDatetype;
		this.hrpName = hrpName;
		this.hrpLink = hrpLink;
	}

	// Property accessors
	public int getHrpKey() {
		return this.hrpKey;
	}

	public void setHrpKey(int hrpKey) {
		this.hrpKey = hrpKey;
	}

	public String getHrpType() {
		return this.hrpType;
	}

	public void setHrpType(String hrpType) {
		this.hrpType = hrpType;
	}

	public String getHrpDatetype() {
		return this.hrpDatetype;
	}

	public void setHrpDatetype(String hrpDatetype) {
		this.hrpDatetype = hrpDatetype;
	}

	public String getHrpName() {
		return this.hrpName;
	}

	public void setHrpName(String hrpName) {
		this.hrpName = hrpName;
	}

	public String getHrpLink() {
		return this.hrpLink;
	}

	public void setHrpLink(String hrpLink) {
		this.hrpLink = hrpLink;
	}

}
