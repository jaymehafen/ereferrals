package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwHdKwLink generated by hbm2java
 */
public class CwHdKwLink implements java.io.Serializable {

	// Fields    

	private int hklKey;

	private int hklHkwKey;

	private int hklHkbKey;

	// Constructors

	/** default constructor */
	public CwHdKwLink() {
	}

	/** full constructor */
	public CwHdKwLink(int hklKey, int hklHkwKey, int hklHkbKey) {
		this.hklKey = hklKey;
		this.hklHkwKey = hklHkwKey;
		this.hklHkbKey = hklHkbKey;
	}

	// Property accessors
	public int getHklKey() {
		return this.hklKey;
	}

	public void setHklKey(int hklKey) {
		this.hklKey = hklKey;
	}

	public int getHklHkwKey() {
		return this.hklHkwKey;
	}

	public void setHklHkwKey(int hklHkwKey) {
		this.hklHkwKey = hklHkwKey;
	}

	public int getHklHkbKey() {
		return this.hklHkbKey;
	}

	public void setHklHkbKey(int hklHkbKey) {
		this.hklHkbKey = hklHkbKey;
	}

}
