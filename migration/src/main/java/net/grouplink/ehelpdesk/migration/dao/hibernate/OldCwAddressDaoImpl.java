package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldCwAddressDao;
import net.grouplink.ehelpdesk.migration.domain.CwAddress;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.hibernate.ScrollableResults;
import org.hibernate.Criteria;
import org.hibernate.CacheMode;
import org.hibernate.ScrollMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 2, 2007
 * Time: 2:35:14 PM
 */
public class OldCwAddressDaoImpl extends HibernateDaoSupport implements OldCwAddressDao {

    public CwAddress getCwAddressByEmail(String email) {
        List list = getHibernateTemplate().findByNamedParam("select a from CwAddress as a where a.adEmail = :userEmail", "userEmail", email);
        if(list.size() > 0){
            return (CwAddress) list.get(0);            
        }
        return null;
    }

    public CwAddress getCwAddressByUserId(Integer userId) {
        List list = getHibernateTemplate().findByNamedParam("select a from CwAddress as a where a.cwName.id = :userId", "userId", userId);
        if(list.size() > 0){
            return (CwAddress) list.get(0);
        }
        return null;
    }

    public ScrollableResults getAllCwAddress(String addressDescription){
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwAddress.class);
        cri.createAlias("cwName", "cwName").add(Restrictions.eq("cwName.naContype", new Integer(1)));
        cri.add(Restrictions.eq("adDescription", addressDescription));
        cri.add(Restrictions.isNotNull("adEmail"));
        // oracle doesn't like empty string ('') comparisons
//        cri.add(Restrictions.ne("adEmail", ""));
        cri.setCacheMode(CacheMode.IGNORE);
        return cri.scroll(ScrollMode.FORWARD_ONLY);
    }

    public void flushAndReset() {
        getHibernateTemplate().flush();
        getHibernateTemplate().clear();
    }

    public int getAddressCount() {
        List list = getHibernateTemplate().find("select count(*) from CwAddress");
        return ((Long)list.get(0)).intValue();
    }
}
