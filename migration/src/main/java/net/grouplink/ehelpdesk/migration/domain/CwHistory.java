package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

import java.util.Date;

/**
 * CwHistory generated by hbm2java
 */
public class CwHistory implements java.io.Serializable {

	// Fields    

	private int hiKey;

	private int hiNaKey;

	private int hiNoKey;

	private int hiType;

	private Date hiDate;

	private String hiSubject;

	private String hiCreator;

	private String hiMsginfo;

	private String hiControl;

	private Date hiTimestamp;

	// Constructors

	/** default constructor */
	public CwHistory() {
	}

	/** minimal constructor */
	public CwHistory(int hiKey, int hiNaKey, int hiNoKey, int hiType,
			Date hiDate, String hiControl) {
		this.hiKey = hiKey;
		this.hiNaKey = hiNaKey;
		this.hiNoKey = hiNoKey;
		this.hiType = hiType;
		this.hiDate = hiDate;
		this.hiControl = hiControl;
	}

	/** full constructor */
	public CwHistory(int hiKey, int hiNaKey, int hiNoKey, int hiType,
			Date hiDate, String hiSubject, String hiCreator, String hiMsginfo,
			String hiControl, Date hiTimestamp) {
		this.hiKey = hiKey;
		this.hiNaKey = hiNaKey;
		this.hiNoKey = hiNoKey;
		this.hiType = hiType;
		this.hiDate = hiDate;
		this.hiSubject = hiSubject;
		this.hiCreator = hiCreator;
		this.hiMsginfo = hiMsginfo;
		this.hiControl = hiControl;
		this.hiTimestamp = hiTimestamp;
	}

	// Property accessors
	public int getHiKey() {
		return this.hiKey;
	}

	public void setHiKey(int hiKey) {
		this.hiKey = hiKey;
	}

	public int getHiNaKey() {
		return this.hiNaKey;
	}

	public void setHiNaKey(int hiNaKey) {
		this.hiNaKey = hiNaKey;
	}

	public int getHiNoKey() {
		return this.hiNoKey;
	}

	public void setHiNoKey(int hiNoKey) {
		this.hiNoKey = hiNoKey;
	}

	public int getHiType() {
		return this.hiType;
	}

	public void setHiType(int hiType) {
		this.hiType = hiType;
	}

	public Date getHiDate() {
		return this.hiDate;
	}

	public void setHiDate(Date hiDate) {
		this.hiDate = hiDate;
	}

	public String getHiSubject() {
		return this.hiSubject;
	}

	public void setHiSubject(String hiSubject) {
		this.hiSubject = hiSubject;
	}

	public String getHiCreator() {
		return this.hiCreator;
	}

	public void setHiCreator(String hiCreator) {
		this.hiCreator = hiCreator;
	}

	public String getHiMsginfo() {
		return this.hiMsginfo;
	}

	public void setHiMsginfo(String hiMsginfo) {
		this.hiMsginfo = hiMsginfo;
	}

	public String getHiControl() {
		return this.hiControl;
	}

	public void setHiControl(String hiControl) {
		this.hiControl = hiControl;
	}

	public Date getHiTimestamp() {
		return this.hiTimestamp;
	}

	public void setHiTimestamp(Date hiTimestamp) {
		this.hiTimestamp = hiTimestamp;
	}

}
