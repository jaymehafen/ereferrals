package net.grouplink.ehelpdesk.migration.dao.hibernate;

import java.util.List;

import net.grouplink.ehelpdesk.migration.dao.CwHdLcLinkDao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateCwHdLcLinkDao extends HibernateDaoSupport implements
        CwHdLcLinkDao {

	public List getLocationIdByCatId(int id) {
		return getHibernateTemplate().findByNamedParam("select c from CwHdLcLink as c where c.hlcHcaKey=:id", "id", new Integer(id));
	}
	
	public List getCategoryIdByLocId(int id) {
		return getHibernateTemplate().findByNamedParam("select c from CwHdLcLink as c where c.hlcHloKey=:id", "id", new Integer(id));
	}

}
