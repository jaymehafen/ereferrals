package net.grouplink.ehelpdesk.migration.dao.hibernate;

import java.util.List;

import net.grouplink.ehelpdesk.migration.dao.CwHdPropertiesDao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateCwHdPropertiesDao extends HibernateDaoSupport implements
        CwHdPropertiesDao {

	public List getProperties() {
		return getHibernateTemplate().find("select p from CwHdProperties as p");
	}

}
