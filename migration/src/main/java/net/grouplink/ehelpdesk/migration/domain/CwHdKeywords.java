package net.grouplink.ehelpdesk.migration.domain;

// Generated Jan 26, 2007 12:50:45 PM by Hibernate Tools 3.2.0.beta8

/**
 * CwHdKeywords generated by hbm2java
 */
public class CwHdKeywords implements java.io.Serializable {

	// Fields    

	private int hkwKey;

	private String hkwKeyword;

	// Constructors

	/** default constructor */
	public CwHdKeywords() {
	}

	/** minimal constructor */
	public CwHdKeywords(int hkwKey) {
		this.hkwKey = hkwKey;
	}

	/** full constructor */
	public CwHdKeywords(int hkwKey, String hkwKeyword) {
		this.hkwKey = hkwKey;
		this.hkwKeyword = hkwKeyword;
	}

	// Property accessors
	public int getHkwKey() {
		return this.hkwKey;
	}

	public void setHkwKey(int hkwKey) {
		this.hkwKey = hkwKey;
	}

	public String getHkwKeyword() {
		return this.hkwKeyword;
	}

	public void setHkwKeyword(String hkwKeyword) {
		this.hkwKeyword = hkwKeyword;
	}

}
