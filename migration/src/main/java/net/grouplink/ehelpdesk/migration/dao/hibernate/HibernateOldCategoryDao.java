package net.grouplink.ehelpdesk.migration.dao.hibernate;

import java.util.List;

import net.grouplink.ehelpdesk.migration.domain.CwHdCategory;
import net.grouplink.ehelpdesk.migration.dao.OldCategoryDao;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernateOldCategoryDao extends HibernateDaoSupport implements
        OldCategoryDao {

	public List getCategory() {
		return getHibernateTemplate().find("select c from CwHdCategory as c");
	}

	public CwHdCategory getCategoryById(Integer id) {
		List list = getHibernateTemplate().findByNamedParam("select c from CwHdCategory as c where c.hcaKey=:catId", "catId", id);
		if(list.size() > 0){
			return (CwHdCategory) list.get(0);
		}
		return null;
	}

}
