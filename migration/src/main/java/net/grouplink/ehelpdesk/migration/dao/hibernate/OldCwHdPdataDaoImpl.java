package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldCwHdPdataDao;

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 30, 2007
 * Time: 3:17:12 PM
 */
public class OldCwHdPdataDaoImpl extends HibernateDaoSupport implements OldCwHdPdataDao {
    public List getUniqueCwPid() {
        Set set = new HashSet(getHibernateTemplate().find("select p.hpdPiKey from CwHdPdata as p"));
        return new ArrayList(set);
    }
}
