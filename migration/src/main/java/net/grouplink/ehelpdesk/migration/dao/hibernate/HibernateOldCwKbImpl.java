package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldCwKbDao;
import net.grouplink.ehelpdesk.migration.domain.CwHdKbase;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;
import org.hibernate.Criteria;
import org.hibernate.CacheMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 23, 2007
 * Time: 4:23:50 PM
 */
public class HibernateOldCwKbImpl extends HibernateDaoSupport implements OldCwKbDao {
    public ScrollableResults getCwKb() {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(CwHdKbase.class);
        cri.setCacheMode(CacheMode.IGNORE);
        return cri.scroll(ScrollMode.FORWARD_ONLY);
    }

    public int getCwKbCount() {
        return DataAccessUtils.intResult(getHibernateTemplate().find("select count(*) from CwHdKbase"));
    }
}
