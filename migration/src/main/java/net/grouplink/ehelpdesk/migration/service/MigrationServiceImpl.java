package net.grouplink.ehelpdesk.migration.service;

import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.migration.MigrationState;
import net.grouplink.ehelpdesk.migration.dao.*;
import net.grouplink.ehelpdesk.migration.domain.*;
import net.grouplink.ehelpdesk.service.*;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.ScrollableResults;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.*;

public class MigrationServiceImpl implements MigrationService, InitializingBean {

    protected final Log logger = LogFactory.getLog(getClass());

    private LocationService locationService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private GroupService groupService;
    private UserService userService;
    private RoleService roleService;
    private UserRoleGroupService userRoleGroupService;
    private StatusService statusService;
    private TicketPriorityService ticketPriorityService;
    private TicketService ticketService;
    private UserRoleService userRoleService;
    private KnowledgeBaseService knowledgeBaseService;
    private TicketHistoryService ticketHistoryService;
    private CustomFieldTypesService customFieldTypesService;
    private CustomFieldsService customFieldService;
    private AssignmentService assignmentService;


    private CwHdLocationDao cwHdLocationDao;
    private OldCategoryDao oldCategoryDao;
    private CwHdCatoptDao cwHdCatoptDao;
    private OldUserDao oldUserDao;
    private OldCwAddressDao oldCwAddressDao;
    private OldCwHdDao oldCwHdDao;
    private OldCwHdTechnician oldCwHdTechnician;
    private OldCwName oldCwName;
    private OldCwKbDao oldCwKbDao;
    private OldCwHdCdLinkDao oldCwHdCdLinkDao;
    private OldCwHdPdataDao oldCwHdPdataDao;

    private Group newGroup;
    private User anon;
    private Map techMap = new HashMap();
    private Set emaiList = new HashSet();
    private Map userMap = new HashMap();

    private MigrationState migrationState;

    public void startMigration(String groupName, String addressDesription, String uploadFolder) {

        long startTime = System.currentTimeMillis();

        logger.debug("Migration started");

        createNewGroup(groupName);
        migrateLocation();
        migrateCategory();
        migrateCategoryOption();
        migrateAllUsers(addressDesription);
        migrateUsers();
        migrateAssigments();
        migrateTickets();
        if (StringUtils.isNotBlank(uploadFolder)) {
            uploadAttachments(uploadFolder);
        }
        migrateKB();

        logger.debug("Migration completed - " + (System.currentTimeMillis() - startTime) / 1000 / 60 + " minutes.");
        migrationState.setFinished(true);
    }

    private void uploadAttachments(String uploadFolder) {
        migrationState.setMessage(MigrationState.ATTACHMENTS_MESSAGE);
        File folder = new File(uploadFolder);
        File[] fileList = folder.listFiles();
        migrationState.setTotalItems(new Integer(fileList.length));
        for (int i = 0; i < fileList.length; i++) {
            try {
                if (fileList[i].isDirectory()) {
                    String folderName = fileList[i].getName();
                    File[] files = fileList[i].listFiles();
                    Ticket ticket = ticketService.getTicketByOldTicketIdAndGroupId(new Integer(folderName), newGroup.getId());
                    if (ticket != null) {
                        for (int j = 0; j < files.length; j++) {
                            File aFile = files[j];
                            Attachment attch;
                            String mime;
                            byte[] bytes;
                            if (aFile.length() <= 5000000) {
                                try {
                                    mime = new MimetypesFileTypeMap().getContentType(aFile);
                                    InputStream is = new FileInputStream(aFile);
                                    bytes = new byte[(int) aFile.length()];
                                    int offset = 0;
                                    int numRead;
                                    while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                                        offset += numRead;
                                    }

                                    attch = new Attachment();
                                    attch.setContentType(mime);
                                    attch.setFileData(bytes);
                                    attch.setFileName(aFile.getName());

                                    ticket.getAttachments().add(attch);
                                }
                                catch (Exception e) {
                                    logger.debug(e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        }
                        ticketService.saveMigrationTicket(ticket);
                        ticketService.flush();
                        ticketService.clear();

                    }
                }
            }
            catch (Exception e) {
                logger.debug(e.getMessage());
                e.printStackTrace();
            }
            ticketService.flush();
            ticketService.clear();
            migrationState.setItemCount(new Integer(i));
        }
    }

    private void migrateAssigments() {
        migrationState.setMessage(MigrationState.ASSIGNMENTS_MESSAGE);

        ScrollableResults allTechs = oldCwHdTechnician.getAllTechs();
        ScrollableResults cwHdCdList = oldCwHdCdLinkDao.getAllCwHdCdLink();

        migrationState.setTotalItems(new Integer(oldCwHdTechnician.getAllTechCount()));

        int i = 0;
        while (allTechs.next()) {
            CwHdTechnician cwHdTech = (CwHdTechnician) allTechs.get(0);
            UserRoleGroup urg = (UserRoleGroup) techMap.get(StringUtils.lowerCase(cwHdTech.getHteEmail()));
            if (urg != null) {
                List techLoc = new ArrayList(cwHdTech.getHdLocationSet());
                List hdLink = new ArrayList(cwHdTech.getHdCatOptSet());
                for (int j = 0; j < techLoc.size(); j++) {
                    CwHdToLink toLink = (CwHdToLink) techLoc.get(j);
                    for (int k = 0; k < hdLink.size(); k++) {
                        CwHdLink hdCLink = (CwHdLink) hdLink.get(k);
                        Assignment tlco = new Assignment();
                        tlco.setCategoryOption(categoryOptionService.getCategoryOptionById(hdCLink.getHtlHcoKey().getNewCatOpt()));
                        tlco.setLocation(locationService.getLocationByName(toLink.getHtoHloKey().getHloName()));
                        tlco.setUserRoleGroup(urg);
                        assignmentService.save(tlco);
                        if (k % 20 == 0) {
                            assignmentService.flush();
                            assignmentService.clear();
                        }
                    }
                    assignmentService.flush();
                    assignmentService.clear();
                }
            }
            assignmentService.flush();
            assignmentService.clear();
            oldCwHdTechnician.flushAndReset();

            i++;
            migrationState.setItemCount(new Integer(i));
        }
        allTechs.close();
        techMap.clear();

        migrationState.setTotalItems(new Integer(oldCwHdCdLinkDao.getAllCwHdCdLinkCount()));
        UserRoleGroup ticketPool = userRoleGroupService.getTicketPoolByGroupId(newGroup.getId());

        i = 0;
        while (cwHdCdList.next()) {
            CwHdCdLink cdLink = (CwHdCdLink) cwHdCdList.get(0);
            Assignment tlco = new Assignment();
            tlco.setCategoryOption(categoryOptionService.getCategoryOptionById(cdLink.getHcdHcoKey().getNewCatOpt()));
            tlco.setLocation(locationService.getLocationByName(cdLink.getHcdHloKey().getHloName()));
            tlco.setUserRoleGroup(ticketPool);
            assignmentService.save(tlco);

            i++;
            if (i % 20 == 0) {
                assignmentService.flush();
                assignmentService.clear();
                oldCwHdTechnician.flushAndReset();
            }
            migrationState.setItemCount(new Integer(i));
        }
        cwHdCdList.close();
        assignmentService.flush();
        assignmentService.clear();
        oldCwHdTechnician.flushAndReset();
    }

    private String stripHtml(String stringWithHtml) {
        String result = "";
        if (StringUtils.isNotBlank(stringWithHtml)) {
            result = StringUtils.replace(stringWithHtml, "<br>", "\r\n");
            result = result.replaceAll("<.*?>", "");
        }
        return result;
    }

    private String massageNote(String note) {
        String result = "";
        if (StringUtils.isNotBlank(note)) {
            try {
                result = URLDecoder.decode(note, "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
                return stripHtml(note);
            }
        }
        return stripHtml(result);
    }

    private void migrateAllUsers(String addressDescription) {
        migrationState.setMessage(MigrationState.USERS_MESSAGE);
        Role roleUser = roleService.getRoleById(Role.ROLE_USER_ID);
        migrationState.setTotalItems(new Integer(oldCwAddressDao.getAddressCount()));

        ScrollableResults cwAddressList = oldCwAddressDao.getAllCwAddress(addressDescription);
        int i = 0;
        User user = null;
        String email;
        while (cwAddressList.next()) {
            migrationState.setItemCount(new Integer(i));
            CwAddress cwAddress = (CwAddress) cwAddressList.get(0);

            // skip addresses with empty emails
            if (StringUtils.isBlank(cwAddress.getAdEmail())) {
                continue;
            }

            email = StringUtils.lowerCase(cwAddress.getAdEmail());

            if (!emaiList.contains(email)) {
                emaiList.add(email);
                user = userService.getUserByEmail(email);

                if (user == null) {
                    CwName cwName = cwAddress.getCwName();
                    user = new User();
                    user.setCreationDate(new Date());
                    user.setCreator("MIGRATION-UTIL");

                    user.setFirstName(cwName.getNaFname());
                    user.setLastName(cwName.getNaLname());

                    user.setEmail(StringUtils.lowerCase(email));
                    user.setLoginId(StringUtils.lowerCase(email));
                    if (cwName.getNaPassword() != null && StringUtils.isNotBlank(cwName.getNaPassword().getHpwPw())) {
                        user.setPassword(StringUtils.lowerCase(cwName.getNaPassword().getHpwPw()));
                    }
                    user.setActive(true);

                    user.setComments(createCommentsFromCustomField(cwName));

                    //adding user role to user
                    UserRole userRole = new UserRole();
                    userRole.setUser(user);
                    userRole.setRole(roleUser);

                    user.getUserRoles().add(userRole);
                }
            }

            if (user != null) {
                Address newAddress = new Address();
                newAddress.setCity(cwAddress.getAdCity());
                newAddress.setLine1(cwAddress.getAdAddr());
                newAddress.setPostalCode(cwAddress.getAdZip());
                newAddress.setPrimaryAddress(Boolean.TRUE);
                newAddress.setAddressType(cwAddress.getAdDescription());
                newAddress.setState(cwAddress.getAdState());
                newAddress.setUser(user);
                user.getAddress().add(newAddress);

                List phList = new ArrayList(cwAddress.getCwName().getCwPhoneList());
                boolean primary = true;
                for (int j = 0; j < phList.size(); j++) {
                    CwPhone oldPhone = (CwPhone) phList.get(j);
                    if (StringUtils.isNotBlank(oldPhone.getPhPhone())) {
                        Phone newPhone = new Phone();
                        String area = StringUtils.stripToEmpty(oldPhone.getPhAreacode()) + " " + StringUtils.stripToEmpty(oldPhone.getPhPhone());
                        newPhone.setNumber(StringUtils.stripToEmpty(area));
                        newPhone.setExtension(oldPhone.getPhExtension());
                        newPhone.setPhoneType(oldPhone.getPhDescription());
                        newPhone.setUser(user);
                        newPhone.setPrimaryPhone(Boolean.valueOf(primary));
                        primary = false;
                        user.getPhone().add(newPhone);
                    }
                }
                userService.saveUser(user);
            }
            i++;
            if (i % 20 == 0) {
                userService.flush();
                userService.clear();
                oldCwAddressDao.flushAndReset();
            }
        }
        cwAddressList.close();
        userService.flush();
        userService.clear();
        oldCwAddressDao.flushAndReset();
        emaiList.clear();
    }

    private void migrateKB() {
        migrationState.setMessage(MigrationState.KBA_MESSAGE);
        ScrollableResults oldKbList = oldCwKbDao.getCwKb();
        migrationState.setTotalItems(new Integer(oldCwKbDao.getCwKbCount()));
        int i = 0;
        while (oldKbList.next()) {
            CwHdKbase oldKb = (CwHdKbase) oldKbList.get(0);
            KnowledgeBase kb = new KnowledgeBase();
            kb.setDate(oldKb.getHkbDate());
            kb.setGroup(newGroup);
            kb.setIsPrivate(oldKb.getHkbTechArticle() == null || oldKb.getHkbTechArticle().intValue() == 0 ? Boolean.FALSE : Boolean.TRUE);
            kb.setSubject(oldKb.getHkbSubject());
            kb.setProblem(oldKb.getHkbProblem());
            kb.setResolution(oldKb.getHkbResolution());
            knowledgeBaseService.saveKnowledgeBaseMigration(kb);
            if (i % 20 == 0) {
                knowledgeBaseService.flush();
                knowledgeBaseService.clear();
            }
            i++;
            migrationState.setItemCount(new Integer(i));
        }
        oldKbList.close();
        knowledgeBaseService.flush();
        knowledgeBaseService.clear();
    }

    private void migrateTickets() {
        migrationState.setMessage(MigrationState.TICKETS_MESSAGE);
        migrationState.setTotalItems(new Integer(oldCwHdDao.getTicketCount()));
        Map locMap = new HashMap();
        Map catMap = new HashMap();
        Map catOptMap = new HashMap();
        Map staMap = new HashMap();
        Map priMap = new HashMap();

        ScrollableResults tickets = oldCwHdDao.getOldTickets();
        anon = userService.getUserById(new Integer(-1));
        int i = 0;
        while (tickets.next()) {
            migrationState.setItemCount(new Integer(i));
            try {
                CwHd oldTicket = (CwHd) tickets.get(0);

                User contact = getUserFromOldContact(oldTicket.getCwName());

                User submittedBy = getUserBySubmittedBy(oldTicket.getHtiSubmittedBy());

                User assignedTo = null;
                UserRoleGroup urg;
                if (!("Ticket Pool").equals(oldTicket.getHtiAssignedTo())) {
                    if (oldTicket.getHtiSubmittedBy() != null && oldTicket.getHtiSubmittedBy().equals(oldTicket.getHtiAssignedTo())) {
                        assignedTo = submittedBy;
                    } else {
                        assignedTo = getUserByAssignedTo(oldTicket.getHtiAssignedTo());
                    }

                    urg = getUserRoleGroupFromUser(assignedTo);
                } else {
                    urg = userRoleGroupService.getTicketPoolByGroupId(newGroup.getId());
                }

                CategoryOption catOpt = (CategoryOption) catOptMap.get(StringUtils.isBlank(oldTicket.getHtiCatopt()) ? "No Category Option Name" : oldTicket.getHtiCatopt());
                if (catOpt == null) {
                    catOpt = categoryOptionService.getCategoryOptionByNameAndGroupId(StringUtils.isBlank(oldTicket.getHtiCatopt()) ? "No Category Option Name" : oldTicket.getHtiCatopt(), newGroup.getId());
                    catOptMap.put(oldTicket.getHtiCatopt(), catOpt);
                }
                if (catOpt == null) {
                    catOpt = new CategoryOption();
                    catOpt.setActive(false);
                    catOpt.setName(StringUtils.isBlank(oldTicket.getHtiCatopt()) ? "No Category Option Name" : oldTicket.getHtiCatopt());

                    Category cat = (Category) catMap.get(StringUtils.isBlank(oldTicket.getHtiCategory()) ? "No Category Name" : oldTicket.getHtiCategory());
                    if (cat == null) {
                        cat = categoryService.getCategoryByNameAndGroupId(StringUtils.isBlank(oldTicket.getHtiCategory()) ? "No Category Name" : oldTicket.getHtiCategory(), newGroup.getId());
                        catMap.put(oldTicket.getHtiCategory(), cat);
                    }
                    if (cat == null) {
                        cat = new Category();
                        cat.setActive(false);
                        cat.setName(StringUtils.isBlank(oldTicket.getHtiCategory()) ? "No Category Name" : oldTicket.getHtiCategory());
                        cat.setGroup(newGroup);
                        categoryService.saveCategory(cat);
                    }
                    catOpt.setCategory(cat);
                    categoryOptionService.saveCategoryOption(catOpt);
                    categoryOptionService.flush();
                }

                Location loc = (Location) locMap.get(oldTicket.getCwHdLocation().getHloName());
                if (loc == null) {
                    loc = locationService.getLocationByName(oldTicket.getCwHdLocation().getHloName());
                    locMap.put(oldTicket.getCwHdLocation().getHloName(), loc);
                }
                if (loc == null) {
                    loc = new Location();
                    loc.setName(oldTicket.getCwHdLocation().getHloName());
                    locationService.saveLocation(loc);
                    locationService.flush();
                }

                Status status = (Status) staMap.get(oldTicket.getHtiStatus());
                if (status == null) {
                    status = statusService.getStatusByName(oldTicket.getHtiStatus());
                    staMap.put(oldTicket.getHtiStatus(), status);
                }
                if (status == null) {
                    status = new Status();
                    status.setName(oldTicket.getHtiStatus());
                    statusService.saveStatus(status);
                    statusService.flush();
                }

                TicketPriority priority = (TicketPriority) priMap.get(oldTicket.getHtiPriority());
                if (priority == null) {
                    priority = ticketPriorityService.getPriorityByName(oldTicket.getHtiPriority());
                    priMap.put(oldTicket.getHtiPriority(), priority);
                }
                if (priority == null) {
                    if (StringUtils.isNotBlank(oldTicket.getHtiPriority())) {
                        priority = new TicketPriority();
                        priority.setName(oldTicket.getHtiPriority());
                        ticketPriorityService.savePriority(priority);
                        ticketPriorityService.flush();
                    } else {
                        priority = ticketPriorityService.getPriorityByName("No Priority");
                        if (priority == null) {
                            priority = new TicketPriority();
                            priority.setName("No Priority");
                            priority.setIcon("blank");
                            priority.setOrder(new Integer(0));
                            ticketPriorityService.savePriority(priority);
                            ticketPriorityService.flush();
                        }
                    }
                }

                Integer workTime = new Integer(0);
                if (oldTicket.getHtiWorkTime() != null) {
                    workTime = new Integer(new Double(Math.ceil(oldTicket.getHtiWorkTime().doubleValue() / 0.000012)).intValue());
                }

                //Creating new Ticket
                Ticket newTicket = new Ticket();
                newTicket.setContact(contact);
                newTicket.setSubmittedBy(submittedBy);
                newTicket.setAssignedTo(urg);
                newTicket.setCategoryOption(catOpt);
                newTicket.setCategory(catOpt.getCategory());
                newTicket.setGroup(catOpt.getCategory().getGroup());
                newTicket.setLocation(loc);
                newTicket.setStatus(status);
                newTicket.setCreatedDate(validateDate(oldTicket, oldTicket.getHtiCreatedDate()));
                newTicket.setModifiedDate(validateDate(oldTicket, oldTicket.getHtiModifiedDate()));
                newTicket.setEstimatedDate(validateDate(oldTicket, oldTicket.getHtiEstDate()));
                newTicket.setPriority(priority);
                newTicket.setWorkTime(workTime);
                newTicket.setOldTicketId(oldTicket.getHtiKey());

                //Custom Field
                if (oldTicket.getCwHdPdataSet().size() > 0) {
                    for (Iterator iterator = oldTicket.getCwHdPdataSet().iterator(); iterator.hasNext();) {

                        CwHdPdata cwHdPdata = (CwHdPdata) iterator.next();

                        try {
                            CustomField custField = customFieldService.getCustomFieldByOldIdAndCatOptId(cwHdPdata.getHpdPiKey().getPiKey(), catOpt.getId());

                            //if needed create a custom field
                            if (custField == null) {
                                CwPid oldCwPid = cwHdPdata.getHpdPiKey();

                                CustomFieldType cusType;
                                if (oldCwPid.getPiView().intValue() == 22) {
                                    cusType = customFieldTypesService.getCustomFieldType(new Integer(5));
                                } else {
                                    cusType = customFieldTypesService.getCustomFieldType(new Integer(1));
                                }

                                String[] oldOptions = StringUtils.split(oldCwPid.getPiList(), "\r\n");

                                custField = new CustomField();
                                custField.setCategoryOption(catOpt);
                                custField.setCustomFieldType(cusType);
                                custField.setName(oldCwPid.getPiName());
                                custField.setOldCustonFieldId(oldCwPid.getPiKey());

                                if (oldOptions != null) {
                                    for (int k = 0; k < oldOptions.length; k++) {
                                        String oldOption = oldOptions[k];
                                        CustomFieldOption cutOption = new CustomFieldOption();
                                        cutOption.setCustomField(custField);
                                        cutOption.setDisplayValue(oldOption);
                                        cutOption.setOrder(new Integer(0));
                                        cutOption.setValue(oldOption);

                                        custField.getOptions().add(cutOption);
                                    }
                                }

                                custField.setRequired(oldCwPid.getPiReq().intValue() > 0);

                                catOpt.getCustomFields().add(custField);
                                categoryOptionService.saveCategoryOption(catOpt);
                                categoryOptionService.flush();
                            }

                            CustomFieldValues cfv = new CustomFieldValues();
                            cfv.setCustomField(custField);
                            cfv.setTicket(newTicket);

                            if (cwHdPdata.getHpdPiKey().getPiType().intValue() == 2 && cwHdPdata.getHpdInt() != null) {
                                cfv.setFieldValue(StringUtils.isBlank(cwHdPdata.getHpdInt().toString()) ? "" : cwHdPdata.getHpdInt().toString());
                            } else
                            if (cwHdPdata.getHpdPiKey().getPiType().intValue() == 3 && cwHdPdata.getHpdDate() != null) {
                                cfv.setFieldValue(StringUtils.isBlank(cwHdPdata.getHpdDate().toString()) ? "" : cwHdPdata.getHpdDate().toString());
                            } else
                            if (cwHdPdata.getHpdPiKey().getPiType().intValue() == 4 && cwHdPdata.getHpdFloat() != null) {
                                cfv.setFieldValue(StringUtils.isBlank(cwHdPdata.getHpdFloat().toString()) ? "" : cwHdPdata.getHpdFloat().toString());
                            } else {
                                if (StringUtils.isNotBlank(cwHdPdata.getHpdVarchar())) {
                                    cfv.setFieldValue(cwHdPdata.getHpdVarchar());
                                }
                            }

                            newTicket.getCustomeFieldValues().add(cfv);
                        }
                        catch (ObjectNotFoundException e) {
                            logger.debug("Unable to find custom field for CW_HD_PDATA#" + cwHdPdata.getHpdKey());
                            e.printStackTrace();
                        }
                        catch (Exception e) {
                            logger.debug("Migration error: migrateTickets()" + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }

                //Initial Comments
                List histList = new ArrayList(oldTicket.getCwHistorySet());
                Collections.sort(histList);
                CwHdHistory hist;

                if (!histList.isEmpty()) {
                    hist = (CwHdHistory) histList.get(0);

                    newTicket.setSubject(StringUtils.isBlank(hist.getHthSubject()) ? "No Subject" : hist.getHthSubject());
                    newTicket.setNote(massageNote(hist.getHthNote()));
                }

                //History Notes
                if (!histList.isEmpty()) {
                    for (int j = 1; j < histList.size(); j++) {
                        hist = (CwHdHistory) histList.get(j);
                        TicketHistory tHist = new TicketHistory();
                        tHist.setSubject(stripHtml(hist.getHthSubject()));
                        tHist.setNote(massageNote(hist.getHthNote()));
                        tHist.setTicket(newTicket);
                        tHist.setCreatedDate(hist.getHthDate());
                        User userHist;
                        if (StringUtils.isNotBlank(hist.getHthCreator())) {
                            if (hist.getHthCreator().equals(oldTicket.getHtiSubmittedBy())) {
                                userHist = submittedBy;
                            } else if (hist.getHthCreator().equals(oldTicket.getHtiAssignedTo())) {
                                userHist = assignedTo;
                            } else {
                                userHist = getUserBySubmittedBy(hist.getHthCreator());
                            }
                        } else {
                            userHist = anon;
                        }
                        tHist.setUser(userHist);
                        newTicket.getTicketHistory().add(tHist);
                    }
                }

                //Zen Information
                if (oldTicket.getCwHdNdsSet() != null && oldTicket.getCwHdNdsSet().size() > 0) {
                    CwHdNds oldZen = (CwHdNds) oldTicket.getCwHdNdsSet().toArray()[0];
                    ZenInformation zen = new ZenInformation();
                    zen.setAssetTag(oldZen.getHdsAssettag());
                    zen.setBiosType(oldZen.getHdsBiostype());
                    zen.setComputerModel(oldZen.getHdsComputermodel());
                    zen.setComputerName(oldZen.getHdsComputerName());
                    zen.setComputerType(oldZen.getHdsComputertype());
                    zen.setDiskInfo(oldZen.getHdsDiskinfo());
                    zen.setDn(oldZen.getHdsDn());
                    zen.setIpAddress(oldZen.getHdsIpaddress());
                    zen.setLastUser(oldZen.getHdsLastuser());
                    zen.setLoggedInWorkstation(oldZen.getHdsLoggedinworkstation());
                    zen.setMacAddress(oldZen.getHdsMacaddress());
                    zen.setMemorySize(oldZen.getHdsMemorysize());
                    zen.setModelNumber(oldZen.getHdsModelnumber());
                    zen.setNcv(oldZen.getHdsNcv());
                    zen.setNicType(oldZen.getHdsNictype());
                    zen.setOperator(oldZen.getHdsOperator());
                    zen.setOsRevision(oldZen.getHdsOsrevision());
                    zen.setOsType(oldZen.getHdsOstype());
                    zen.setProcessorType(oldZen.getHdsProcessortype());
                    zen.setSerialNumber(oldZen.getHdsSerialnumber());
                    zen.setSubnetAddress(oldZen.getHdsSubnetaddress());
                    zen.setTicket(newTicket);
                    zen.setUserHistory(oldZen.getHdsUserhistory());
                    zen.setVideoType(oldZen.getHdsVideotype());
                    zen.setWorkstation(oldZen.getHdsWorkstation());
                    zen.setWsdn(oldZen.getHdsWsdn());

                    newTicket.getZenInformation().add(zen);
                }

                //Saving Ticket
                ticketService.saveMigrationTicket(newTicket);

                if (i % 20 == 0) {
                    oldCwHdDao.flushAndReset();
                }
                if (i % 30 == 0) {
                    locMap.clear();
                    catMap.clear();
                    catOptMap.clear();
                    staMap.clear();
                    priMap.clear();
                    userMap.clear();
                    anon = null;

                    ticketService.flush();
                    ticketService.clear();

                    anon = userService.getUserById(new Integer(-1));
                }
                i++;
            }
            catch (Exception e) {
                logger.debug(e.getMessage());
                e.printStackTrace();
            }
        }
        tickets.close();
        ticketService.flush();
        ticketService.clear();
        oldCwHdDao.flushAndReset();
        userMap.clear();
    }

    private Date validateDate(CwHd oldTicket, Date date) {
        if (date == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        if (1753 <= year && year <= 9999) {
            return cal.getTime();
        } else {
            logger.debug("Unable to resolve date for ticket " + oldTicket.getHtiKey() + " [" + date + "]");
            return null;
        }
    }

    private User getUserByAssignedTo(String htiAssignedTo) {
        User user = (User) userMap.get(htiAssignedTo);

        if (user == null) {

            CwHdTechnician cwHdTech = oldCwHdTechnician.getCwHdTechnicianByName(htiAssignedTo);
            if (cwHdTech != null) {
                user = userService.getUserByEmail(cwHdTech.getHteEmail());
            }

            if (user == null) {
                String[] temp = new String[]{};
                if (StringUtils.isNotBlank(htiAssignedTo)) {
                    temp = StringUtils.split(htiAssignedTo);
                }

                List cwNameList = new ArrayList();
                if (temp.length == 1) {
                    cwNameList = oldCwName.getCwNameByDisplayName(temp[0]);
                }
                if (temp.length == 2) {
                    cwNameList = oldCwName.getCwNameByFirstAndLastName(temp[0], temp[1]);
                }
                if (temp.length == 3) {
                    cwNameList = oldCwName.getCwNameByFirstAndLastName(temp[0], temp[2]);
                }

                if (cwNameList.size() > 0) {
                    for (int i = 0; i < cwNameList.size(); i++) {
                        CwName oldUser = (CwName) cwNameList.get(i);
                        List cwAddressList = new ArrayList(oldUser.getCwAddressList());
                        for (int j = 0; j < cwAddressList.size(); j++) {
                            CwAddress cwAddress = (CwAddress) cwAddressList.get(j);
                            if (StringUtils.isNotBlank(cwAddress.getAdEmail())) {
                                user = userService.getUserByEmail(StringUtils.lowerCase(cwAddress.getAdEmail()));
                                if (user != null) {
                                    break;
                                }
                            }
                        }
                        if (user != null) {
                            break;
                        }
                    }
                }
            }
            if (user == null) {
                user = anon;
            }
            userMap.put(htiAssignedTo, user);
        }

        return user;
    }

    private User getUserFromOldContact(CwName cwName) {
        User user = null;

        try {
            List cwAddressList = new ArrayList(cwName.getCwAddressList());
            for (int i = 0; i < cwAddressList.size(); i++) {
                CwAddress cwAddress = (CwAddress) cwAddressList.get(i);
                if (StringUtils.isNotBlank(cwAddress.getAdEmail())) {
                    user = (User) userMap.get(StringUtils.lowerCase(cwAddress.getAdEmail()));
                    if (user == null) {
                        user = userService.getUserByEmail(StringUtils.lowerCase(cwAddress.getAdEmail()));
                        if (user != null) {
                            userMap.put(StringUtils.lowerCase(cwAddress.getAdEmail()), user);
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return anon;
        }

        if (user == null) {
            user = anon;
        }

        return user;
    }

    private User getUserBySubmittedBy(String oldName) {
        return getUserByAssignedTo(oldName);
    }

    private UserRoleGroup getUserRoleGroupFromUser(User user) {
        UserRoleGroup urg = userRoleGroupService.getAnyUserRoleGroupByUserIdAndGroupId(user.getId(), newGroup.getId());
        if (urg == null) {
            urg = new UserRoleGroup();

            UserRole ur = new UserRole();
            ur.setUser(user);
            ur.setActive(false);

            urg.setUserRole(ur);
            urg.setGroup(newGroup);
            urg.setActive(false);

            ur.getUserRoleGroup().add(urg);
            user.getUserRoles().add(ur);
            userService.saveUser(user);
            userService.flush();
        }

        return urg;
    }

    private void migrateUsers() {
        migrationState.setMessage(MigrationState.USERS_ADMIN_MESSAGE);

        List oldUsers = oldUserDao.getAdministrators();
        migrationState.setTotalItems(new Integer(oldUsers.size()));
        for (int i = 0; i < oldUsers.size(); i++) {
            try {
                synchUser((CwHdUsersId) oldUsers.get(i), Role.ROLE_MANAGER_ID);
            }
            catch (Exception e) {
                logger.debug(e.getMessage());
                e.printStackTrace();
            }
            migrationState.setItemCount(new Integer(i));
        }

        migrationState.setMessage(MigrationState.USERS_TECHS_MESSAGE);
        oldUsers = oldUserDao.getTechnicians();
        migrationState.setTotalItems(new Integer(oldUsers.size()));
        for (int i = 0; i < oldUsers.size(); i++) {
            try {
                synchUser((CwHdUsersId) oldUsers.get(i), Role.ROLE_TECHNICIAN_ID);
            }
            catch (Exception e) {
                logger.debug(e.getMessage());
                e.printStackTrace();
            }
            migrationState.setItemCount(new Integer(i));
        }
        oldUserDao.flush();
    }

    private void synchUser(CwHdUsersId oldUser, Integer roleId) {


        Role roleUser = roleService.getRoleById(Role.ROLE_USER_ID);
        Role roleTech = roleService.getRoleById(Role.ROLE_TECHNICIAN_ID);
        Role roleMgr = roleService.getRoleById(Role.ROLE_MANAGER_ID);

        User user = userService.getUserByEmail(StringUtils.lowerCase(oldUser.getHusEmail()));

        if (user == null) {
            user = new User();
            user.setCreationDate(new Date());
            user.setCreator("MIGRATION-UTIL");

            CwAddress oldAddress = oldCwAddressDao.getCwAddressByEmail(oldUser.getHusEmail());
            CwName oldName = null;
            if (oldAddress != null) {
                oldName = oldAddress.getCwName();
                if (StringUtils.isBlank(oldName.getNaFname()) && StringUtils.isBlank(oldName.getNaLname())) {
                    String[] name = StringUtils.split(oldUser.getHusName());
                    user.setFirstName(name.length > 0 ? name[0] : "");
                    user.setLastName(name.length > 1 ? name[1] : "");
                } else {
                    user.setFirstName(oldName.getNaFname());
                    user.setLastName(oldName.getNaLname());
                }
            } else {
                String[] name = StringUtils.split(oldUser.getHusName());
                user.setFirstName(name.length > 0 ? name[0] : "");
                user.setLastName(name.length > 1 ? name[1] : "");
            }
            user.setEmail(StringUtils.lowerCase(oldUser.getHusEmail()));
            user.setLoginId(StringUtils.lowerCase(oldUser.getHusUserid()));
            user.setPassword(StringUtils.lowerCase(oldUser.getHusPassword()));
            user.setActive(BooleanUtils.toBoolean(oldUser.getHusActive()));

            //Adding custom fields to user comments
            if (oldName != null) {
                user.setComments(createCommentsFromCustomField(oldName));
            }

            //adding user role to user
            UserRole userRole = new UserRole();
            userRole.setUser(user);
            userRole.setRole(roleUser);

            user.getUserRoles().add(userRole);
            userService.saveUser(user);
        }

        if (roleId.equals(Role.ROLE_TECHNICIAN_ID)) {
            UserRole techRole = userRoleService.getUserRoleByUserIdAndRoleId(user.getId(), roleId);

            if (techRole == null) {
                techRole = new UserRole();
                techRole.setUser(user);
                techRole.setRole(roleTech);
            }

            UserRoleGroup techUrg = userRoleGroupService.getUserRoleGroupByUserIdAndGroupId(user.getId(), newGroup.getId());
            if (techUrg == null) {
                techUrg = new UserRoleGroup();
                techUrg.setGroup(newGroup);
                techUrg.setUserRole(techRole);
                techRole.getUserRoleGroup().add(techUrg);

                user.getUserRoles().add(techRole);
            }
            userService.saveUser(user);

            techMap.put(user.getEmail(), techUrg);
        }

        if (roleId.equals(Role.ROLE_MANAGER_ID)) {
            UserRole mgrRole = userRoleService.getUserRoleByUserIdAndRoleId(user.getId(), roleId);

            if (mgrRole == null) {
                mgrRole = new UserRole();
                mgrRole.setUser(user);
                mgrRole.setRole(roleMgr);
            }

            UserRoleGroup mgrUrg = userRoleGroupService.getUserRoleGroupByUserIdAndGroupId(user.getId(), newGroup.getId());
            if (mgrUrg == null) {
                mgrUrg = new UserRoleGroup();
                mgrUrg.setGroup(newGroup);
                mgrUrg.setUserRole(mgrRole);
                mgrRole.getUserRoleGroup().add(mgrUrg);

                user.getUserRoles().add(mgrRole);
            }
            userService.saveUser(user);
        }
        userService.flush();
    }

    private String createCommentsFromCustomField(CwName oldName) {
        String comments = "";
        List cwPdataList = new ArrayList(oldName.getCwPdataSet());
        for (int j = 0; j < cwPdataList.size(); j++) {
            CwPdata cwPdata = (CwPdata) cwPdataList.get(j);
            try {
                if (cwPdata.getPdPiKey().getPiType().intValue() == 2) {
                    if (cwPdata.getPdInt() != null) {
                        comments += cwPdata.getPdPiKey().getPiName() + ": " + cwPdata.getPdInt().toString() + "\r\n";
                    }
                } else if (cwPdata.getPdPiKey().getPiType().intValue() == 3) {
                    if (cwPdata.getPdDate() != null) {
                        comments += cwPdata.getPdPiKey().getPiName() + ": " + cwPdata.getPdDate().toString() + "\r\n";
                    }
                } else if (cwPdata.getPdPiKey().getPiType().intValue() == 4) {
                    if (cwPdata.getPdFloat() != null) {
                        comments += cwPdata.getPdPiKey().getPiName() + ": " + cwPdata.getPdFloat().toString() + "\r\n";
                    }
                } else {
                    if (StringUtils.isNotBlank(cwPdata.getPdVarchar())) {
                        comments += cwPdata.getPdPiKey().getPiName() + ": " + cwPdata.getPdVarchar() + "\r\n";
                    }
                }
            }
            catch (ObjectNotFoundException e) {
                logger.debug("Unable to find custom field for CW_PDATA#" + cwPdata.getPdKey());
                e.printStackTrace();
            }
            catch (Exception e) {
                logger.debug("Migration error: createCommentsFromCustomField()" + e.getMessage());
                e.printStackTrace();
            }
        }
        return comments;
    }

    private void createNewGroup(String groupName) {
        migrationState.setItemCount(new Integer(0));
        migrationState.setTotalItems(new Integer(100));
        migrationState.setMessage(MigrationState.NEWGROUP_MESSAGE);
        Group group = groupService.getGroupByName(groupName);
        if (group == null) {
            group = new Group();
            group.setName(groupName);
            group.setComments("New group created while doing migration");
            groupService.saveGroup(group);
        }
        newGroup = group;
        groupService.flush();
        migrationState.setItemCount(new Integer(100));
    }

    private void migrateLocation() {
        migrationState.setMessage(MigrationState.LOCATIONS_MESSAGE);
        List list = cwHdLocationDao.getLocations();
        migrationState.setTotalItems(new Integer(list.size()));
        for (int i = 0; i < list.size(); i++) {
            CwHdLocation oldLoc = (CwHdLocation) list.get(i);
            Location loc = locationService.getLocationByName(oldLoc.getHloName());
            if (loc == null) {
                loc = new Location();
                loc.setName(oldLoc.getHloName());
                locationService.saveLocation(loc);
            }
            migrationState.setItemCount(new Integer(i));
        }
        locationService.flush();
    }

    private void migrateCategory() {
        migrationState.setMessage(MigrationState.CATEGORIES_MESSAGE);
        List list = oldCategoryDao.getCategory();
        migrationState.setTotalItems(new Integer(list.size()));
        for (int i = 0; i < list.size(); i++) {
            CwHdCategory oldCat = (CwHdCategory) list.get(i);
            Category cat = categoryService.getCategoryByNameAndGroupId(StringUtils.isBlank(oldCat.getHcaName()) ? "No Category Name" : oldCat.getHcaName(), newGroup.getId());
            if (cat == null) {
                cat = new Category();
                cat.setName(StringUtils.isBlank(oldCat.getHcaName()) ? "No Category Name" : oldCat.getHcaName());
                cat.setGroup(newGroup);
                categoryService.saveCategory(cat);
            }
            migrationState.setItemCount(new Integer(i));
        }
        categoryService.flush();
    }

    private void migrateCategoryOption() {
        migrationState.setMessage(MigrationState.CATOPTS_MESSAGE);
        List list = cwHdCatoptDao.getCwHdCatopt();
        migrationState.setTotalItems(new Integer(list.size()));
        for (int i = 0; i < list.size(); i++) {
            CwHdCatopt oldCatOpt = (CwHdCatopt) list.get(i);
            try {
                CategoryOption newCategoryOption = categoryOptionService.getCategoryOptionByNameAndGroupId(StringUtils.isBlank(oldCatOpt.getHcoName()) ? "No Category Option Name" : oldCatOpt.getHcoName(), newGroup.getId());
                if (newCategoryOption == null) {
                    newCategoryOption = new CategoryOption();
                    newCategoryOption.setName(StringUtils.isBlank(oldCatOpt.getHcoName()) ? "No Category Option Name" : oldCatOpt.getHcoName());
                    newCategoryOption.setCategory(categoryService.getCategoryByNameAndGroupId(StringUtils.isBlank(oldCategoryDao.getCategoryById(oldCatOpt.getHcoHcaKey()).getHcaName()) ? "No Category Name" : oldCategoryDao.getCategoryById(oldCatOpt.getHcoHcaKey()).getHcaName(), newGroup.getId()));
                    categoryOptionService.saveCategoryOption(newCategoryOption);
                    oldCatOpt.setNewCatOpt(newCategoryOption.getId());
                }
            }
            catch (Exception e) {
                logger.debug("Unable to resolve category for: '" + oldCatOpt.getHcoName() + "' - Will try to migrate option during Ticket Migration");
                e.printStackTrace();
            }
            migrationState.setItemCount(new Integer(i));
        }
        categoryOptionService.flush();
    }


    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserRoleService(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    public void setKnowledgeBaseService(KnowledgeBaseService knowledgeBaseService) {
        this.knowledgeBaseService = knowledgeBaseService;
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setCustomFieldTypesService(CustomFieldTypesService customFieldTypesService) {
        this.customFieldTypesService = customFieldTypesService;
    }

    public void setCustomFieldService(CustomFieldsService customFieldService) {
        this.customFieldService = customFieldService;
    }

    public void setAssignmentService(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    ///////////////////////////////////////Old Stuff/////////////////////////

    public void setCwHdLocationDao(CwHdLocationDao cwHdLocationDao) {
        this.cwHdLocationDao = cwHdLocationDao;
    }

    public void setOldCategoryDao(OldCategoryDao oldCategoryDao) {
        this.oldCategoryDao = oldCategoryDao;
    }

    public void setCwHdCatoptDao(CwHdCatoptDao cwHdCatoptDao) {
        this.cwHdCatoptDao = cwHdCatoptDao;
    }


    public void setOldUserDao(OldUserDao oldUserDao) {
        this.oldUserDao = oldUserDao;
    }

    public void setOldCwAddressDao(OldCwAddressDao oldCwAddressDao) {
        this.oldCwAddressDao = oldCwAddressDao;
    }

    public void setOldCwHdDao(OldCwHdDao oldCwHdDao) {
        this.oldCwHdDao = oldCwHdDao;
    }

    public void setOldCwHdTechnician(OldCwHdTechnician oldCwHdTechnician) {
        this.oldCwHdTechnician = oldCwHdTechnician;
    }

    public void setOldCwName(OldCwName oldCwName) {
        this.oldCwName = oldCwName;
    }

    public void setOldCwKbDao(OldCwKbDao oldCwKbDao) {
        this.oldCwKbDao = oldCwKbDao;
    }

    public void setOldCwHdCdLinkDao(OldCwHdCdLinkDao oldCwHdCdLinkDao) {
        this.oldCwHdCdLinkDao = oldCwHdCdLinkDao;
    }

    public void setOldCwHdPdataDao(OldCwHdPdataDao oldCwHdPdataDao) {
        this.oldCwHdPdataDao = oldCwHdPdataDao;
    }

    public void afterPropertiesSet() throws Exception {
        if (oldCwKbDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCwKbDao");
        if (oldCwHdCdLinkDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCwHdCdLinkDao");
        if (oldCwHdPdataDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCwHdPdataDao");
        if (cwHdLocationDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: cwHdLocationDao");
        if (oldCategoryDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCategoryDao");
        if (cwHdCatoptDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: cwHdCatoptDao");
        if (oldUserDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldUserDao");
        if (oldCwAddressDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCwAddressDao");
        if (oldCwHdDao == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCwHdDao");
        if (oldCwHdTechnician == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCwHdTechnician");
        if (oldCwName == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: oldCwName");
        if (assignmentService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: assignmentService");
        if (customFieldService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: customFieldService");
        if (customFieldTypesService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: customFieldTypesService");
        if (ticketHistoryService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketHistoryService");
        if (knowledgeBaseService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: knowledgeBaseService");
        if (userRoleService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userRoleService");
        if (ticketService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketService");
        if (ticketPriorityService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketPriorityService");
        if (statusService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: statusService");
        if (userRoleGroupService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userRoleGroupService");
        if (roleService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: roleService");
        if (userService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userService");
        if (groupService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: groupService");
        if (categoryOptionService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryOptionService");
        if (categoryService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryService");
        if (locationService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: locationService");
    }

    public void setMigrationState(MigrationState migrationState) {
        this.migrationState = migrationState;
    }
}
