package net.grouplink.ehelpdesk.migration.dao;

import net.grouplink.ehelpdesk.migration.domain.CwAddress;
import org.hibernate.ScrollableResults;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 2, 2007
 * Time: 2:24:14 PM
 */
public interface OldCwAddressDao {
    CwAddress getCwAddressByEmail(String email);
    CwAddress getCwAddressByUserId(Integer userId);
    ScrollableResults getAllCwAddress(String addressDescription);
    void flushAndReset();
    int getAddressCount();
}
