package net.grouplink.ehelpdesk.migration.dao.hibernate;

import net.grouplink.ehelpdesk.migration.dao.OldUserDao;
import net.grouplink.ehelpdesk.migration.domain.CwHdUsersId;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 1, 2007
 * Time: 11:07:33 PM
 */
public class OldUserDaoImpl extends HibernateDaoSupport implements OldUserDao {
    public List getAdministrators() {
        return getHibernateTemplate().find("select u from CwHdUsersId as u where u.husRole = 'Administrator' or u.husRole = 'Manager' or u.husRole = 'Super User' and u.husUserid != 'User'");
    }

    public List getTechnicians() {
        return getHibernateTemplate().find("select u from CwHdUsersId as u where u.husRole = 'Technician' and u.husUserid != 'User'");
    }

    public List getUsers() {
        return getHibernateTemplate().find("select u from CwHdUsersId as u where u.husRole = 'User' and u.husUserid != 'User'");
    }

    public CwHdUsersId getCwHdUserByName(String htiAssignedTo) {
        List list = getHibernateTemplate().findByNamedParam("select t from CwHdUsersId as t where t.husName = :userName", "userName", htiAssignedTo);
        if(list.size() > 0){
            return (CwHdUsersId) list.get(0);
        }
        return null;
    }

    public void flush() {
        getHibernateTemplate().flush();
        getHibernateTemplate().clear();
    }
}
