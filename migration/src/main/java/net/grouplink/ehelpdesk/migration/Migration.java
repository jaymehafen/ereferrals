package net.grouplink.ehelpdesk.migration;

import net.grouplink.ehelpdesk.migration.service.MigrationService;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.apache.commons.lang.StringUtils;
import net.grouplink.ehelpdesk.migration.gui.DBSelectionPanelDescriptor;
import net.grouplink.ehelpdesk.migration.gui.MigratePanelDescriptor;
import net.grouplink.ehelpdesk.migration.gui.MigrationDBPanelDescriptor;
import net.grouplink.ehelpdesk.migration.gui.wizard.Wizard;
import net.grouplink.ehelpdesk.migration.gui.wizard.WizardPanelDescriptor;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Oct 15, 2007
 * Time: 2:08:03 PM
 */
public class Migration {

    private String imagePath = "net/grouplink/ehelpdesk/common/images/";
    private String ehdLogo = "ehd_logo.gif";
    
    private String ehelpdeskHome = System.getProperty("ehelpdesk.home");
    private static GenericApplicationContext ctx;

    public void setMigrationProps(Properties migrationProps) {
        this.migrationProps = migrationProps;
    }

    private Properties migrationProps = new Properties();

    public Migration() {}

    public void init() throws Exception {

        checkMigrationProps();

        String[] configLocation =
                new String[]{"classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                        "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                        "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml",
                        "classpath:net/grouplink/ehelpdesk/migration/applicationContext-migration-db.xml",
                        "classpath:net/grouplink/ehelpdesk/migration/dao/hibernate/applicationContext-migration-dao.xml",
                        "classpath:net/grouplink/ehelpdesk/migration/service/applicationContext-migration-service.xml"};

        ctx = new GenericApplicationContext();
        XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(ctx);
        xmlReader.loadBeanDefinitions(configLocation);

        //Hibernate Dialect From
        Properties fromProp = (Properties) ctx.getBean("hibernatePropertiesMigration");
        fromProp.setProperty("hibernate.dialect", migrationProps.getProperty("from.hibernate.dialect"));

        //From dataSource
        DriverManagerDataSource migrationDataSource = (DriverManagerDataSource) ctx.getBean("dataSourceMigration");
        migrationDataSource.setDriverClassName(migrationProps.getProperty("from.classname"));
        migrationDataSource.setUrl(migrationProps.getProperty("from.url"));
        migrationDataSource.setUsername(migrationProps.getProperty("from.username"));
        migrationDataSource.setPassword(migrationProps.getProperty("from.password"));

        //Hibernate Dialect To
        Properties toProp = (Properties) ctx.getBean("hibernateProperties");
        toProp.setProperty("hibernate.dialect", migrationProps.getProperty("to.hibernate.dialect"));

        //To dataSource
        DriverManagerDataSource dataSource = (DriverManagerDataSource) ctx.getBean("dataSource");
        dataSource.setDriverClassName(migrationProps.getProperty("to.classname"));
        dataSource.setUrl(migrationProps.getProperty("to.url"));
        dataSource.setUsername(migrationProps.getProperty("to.username"));
        dataSource.setPassword(migrationProps.getProperty("to.password"));

        ctx.refresh();
    }

    private void checkMigrationProps() throws Exception {
        //TODO wtf is this checking anyway?
        String[] props = new String[]{
                "to.hibernate.dialect", "to.classname", "to.url", "to.username",
                "from.hibernate.dialect", "from.classname", "from.url", "from.username",
                "groupName", "addressDescription"};
        StringBuilder errMsg = new StringBuilder();
        for(int i=0; i<props.length; i++){
            if(StringUtils.isBlank(props[i])) errMsg.append(props[i]).append(" ");
        }

        if(StringUtils.isNotBlank(errMsg.toString()))
            throw new Exception("Required migration properties not set: " + errMsg);
    }

    public void startMigration() {
        MigrationService migrationService = (MigrationService) ctx.getBean("migrationService");
        migrationService.startMigration(migrationProps.getProperty("groupName"), migrationProps.getProperty("addressDescription"), migrationProps.getProperty("uploadFolder"));
    }

    public MigrationState getMigrationState() {
        return (MigrationState) ctx.getBean("migrationState");
    }

    private void doHeadlessMigration() throws Exception {
        // try to load the install properties
        try {
            migrationProps.load(
                    new FileInputStream(ehelpdeskHome + File.separatorChar + "migration.properties")
            );
        } catch (IOException e) {
            throw new Exception("Could not load headless.properties: " + e.getMessage());
        }

        // Check that the required properties are set
        StringBuilder missing = new StringBuilder();
        if (StringUtils.isBlank(migrationProps.getProperty("to.hibernate.dialect")))
            missing.append("to.hibernate.dialect ");
        if (StringUtils.isBlank(migrationProps.getProperty("to.classname")))
            missing.append("to.classname ");
        if (StringUtils.isBlank(migrationProps.getProperty("to.url")))
            missing.append("to.url ");
        if (StringUtils.isBlank(migrationProps.getProperty("to.username")))
            missing.append("to.username");

        if (StringUtils.isBlank(migrationProps.getProperty("from.hibernate.dialect")))
            missing.append("from.hibernate.dialect");
        if (StringUtils.isBlank(migrationProps.getProperty("from.classname")))
            missing.append("from.classname");
        if (StringUtils.isBlank(migrationProps.getProperty("from.url")))
            missing.append("from.url");
        if (StringUtils.isBlank(migrationProps.getProperty("from.username")))
            missing.append("from.username");

        if (StringUtils.isNotBlank(missing.toString()))
            throw new Exception("Required properties not set: " + missing);

        if (!checkConnections()) {
            throw new Exception("Could not make database connections. Check your settings.");
        }

        init();
        startMigration();
    }

    private boolean checkConnections() {
        // Check that both TO and FROM connections are OK
        if (!isConnectionGood(migrationProps.getProperty("to.classname"),
                migrationProps.getProperty("to.url"), migrationProps.getProperty("to.username"),
                migrationProps.getProperty("to.password")))
            return false;
        if (!isConnectionGood(migrationProps.getProperty("from.classname"),
                migrationProps.getProperty("from.url"), migrationProps.getProperty("from.username"),
                migrationProps.getProperty("from.password")))
            return false;
        return true;
    }

    private boolean isConnectionGood(String className, String url, String userName, String password) {
        Connection conn = null;
        try {
            Class.forName(className);
            conn = DriverManager.getConnection(url, userName, password);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return false;
        } finally {
            try {
                if (conn != null) conn.close();
            } catch (SQLException sex) {
                sex.printStackTrace();
            }
        }
        return true;
    }

    private void startGUIInstaller() {
        // Display a splash screen
        Frame splashFrame;
        URL imageURL = Wizard.class.getClassLoader().getResource(imagePath + ehdLogo);
        splashFrame = net.grouplink.ehelpdesk.common.gui.SplashScreen.splash(Toolkit.getDefaultToolkit().createImage(imageURL));
        try {
            // let's pause for a second to annoy those splash screen haters
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Get the real stuff ready
        JFrame frame = new JFrame("eReferrals\u2122");
        frame.setUndecorated(true);
        frame.setVisible(true);

        Wizard wizard = new Wizard(frame);
        wizard.getDialog().setTitle(frame.getTitle());
        
        // Add each "page" to the wizard
        WizardPanelDescriptor migDBDescriptor = new MigrationDBPanelDescriptor();
        wizard.registerWizardPanel(MigrationDBPanelDescriptor.IDENTIFIER, migDBDescriptor);

        WizardPanelDescriptor dbDescriptor = new DBSelectionPanelDescriptor();
        wizard.registerWizardPanel(DBSelectionPanelDescriptor.IDENTIFIER, dbDescriptor);

        WizardPanelDescriptor migrationDescriptor = new MigratePanelDescriptor();
        wizard.registerWizardPanel(MigratePanelDescriptor.IDENTIFIER, migrationDescriptor);

        // Set the starting panel
        wizard.setCurrentPanel(MigrationDBPanelDescriptor.IDENTIFIER);

        // Hide the splash screen and show the wizard
        splashFrame.dispose();
        int returnCode = wizard.showModalDialog();

        switch (returnCode) {
            case Wizard.FINISH_RETURN_CODE:
                // Process wizard data
                JOptionPane.showMessageDialog(null, "Migration Complete", "Migration Complete",
                        JOptionPane.INFORMATION_MESSAGE);

                break;
            case Wizard.CANCEL_RETURN_CODE:
                System.out.println("Migration cancelled by user.");
                break;
            case Wizard.ERROR_RETURN_CODE:
                System.out.println("Migration aborted due to internal error.");
                break;
            default:
                break;
        }
        frame.dispose();
        System.exit(0);
    }
   
    public static void main(String[] args) throws Exception {
        Migration migration = new Migration();

        // check that ehelpdesk.home is set
        if (StringUtils.isBlank(System.getProperty("ehelpdesk.home")))
            throw new Exception("System property ehelpdesk.home not set.");

        if (GraphicsEnvironment.isHeadless()) {
            migration.doHeadlessMigration();
        } else {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            migration.startGUIInstaller();
        }
    }
}
