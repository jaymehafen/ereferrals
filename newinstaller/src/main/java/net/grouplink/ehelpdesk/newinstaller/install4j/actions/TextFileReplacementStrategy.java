package net.grouplink.ehelpdesk.newinstaller.install4j.actions;

import com.install4j.api.Util;
import com.install4j.api.beans.AbstractBean;
import com.install4j.api.context.InstallerContext;
import de.schlichtherle.io.File;
import de.schlichtherle.io.FileReader;
import de.schlichtherle.io.FileWriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 * @author zpearce
 */
public class TextFileReplacementStrategy extends AbstractBean implements ReplacementStrategy {
    public boolean execute(InstallerContext context, File file) {
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = null;
        BufferedWriter writer = null;

        try {
            reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null) {
                buffer.append(line).append("\n");
            }
            reader.close();

            String replacedVars = replaceVariables(buffer.toString());

            writer = new BufferedWriter(new FileWriter(file));
            writer.write(replacedVars);
            writer.close();

            File.umount();
        } catch (Exception e) {
            Util.logError(this, e.getMessage());
            return false;
        } finally {
            try {
                reader.close();
                writer.close();
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }
}
