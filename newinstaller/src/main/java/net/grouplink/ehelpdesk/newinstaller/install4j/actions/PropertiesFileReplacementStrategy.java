package net.grouplink.ehelpdesk.newinstaller.install4j.actions;

import com.install4j.api.beans.AbstractBean;
import com.install4j.api.context.InstallerContext;
import de.schlichtherle.io.File;
import de.schlichtherle.io.FileInputStream;
import de.schlichtherle.io.FileOutputStream;

import java.util.Properties;

/**
 * @author zpearce
 */
public class PropertiesFileReplacementStrategy extends AbstractBean implements ReplacementStrategy {
    public boolean execute(InstallerContext context, File file) {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(file));

            for(Object prop : props.keySet()) {
                String propKey = (String)prop;
                String propVal = replaceVariables(props.getProperty(propKey));
                props.setProperty(propKey, propVal);
            }

            props.store(new FileOutputStream(file), null);
            File.umount();
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
