package net.grouplink.ehelpdesk.newinstaller.install4j.actions;

import com.install4j.api.context.InstallerContext;
import de.schlichtherle.io.File;

/**
 * @author zpearce
 */
public interface ReplacementStrategy {
    boolean execute(InstallerContext context, File file);
}
