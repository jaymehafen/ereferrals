package net.grouplink.ehelpdesk.newinstaller.install4j.actions;

import com.install4j.api.Util;
import com.install4j.api.actions.AbstractInstallAction;
import com.install4j.api.context.InstallerContext;
import de.schlichtherle.io.File;

/**
 * @author zpearce
 */
public class CopyFileOrDirIntoZipFile extends AbstractInstallAction {
    private String sourceFileOrDir;
    private String destFileOrDir;

    public String getSourceFileOrDir() {
        return replaceVariables(sourceFileOrDir);
    }

    public void setSourceFileOrDir(String sourceFileOrDir) {
        this.sourceFileOrDir = sourceFileOrDir;
    }

    public String getDestFileOrDir() {
        return replaceVariables(destFileOrDir);
    }

    public void setDestFileOrDir(String destFileOrDir) {
        this.destFileOrDir = destFileOrDir;
    }

    public boolean install(InstallerContext context) {
        try {
            File source = new File(getSourceFileOrDir());
            File dest = new File(getDestFileOrDir());
            if (!source.copyAllTo(dest)) {
                return false;
            }
        } catch (Exception e) {
            Util.logError(this, e.getMessage());
            context.setErrorOccured(true);
            return false;
        }

        return true;
    }
}
