dependencies = {
    layers: [
        {
            name: "../grouplink/includes.js",
            dependencies: [
                "grouplink.includes"
            ]
        }
    ],
    prefixes: [
        [ "dijit", "../dijit" ],
        [ "dojox", "../dojox" ],
        [ "grouplink", "../grouplink"   ]
    ]
}
