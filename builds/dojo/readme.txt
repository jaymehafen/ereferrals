here are the steps to do a custom dojo build:

1. download dojo source, extract

2. place includes.js in dojo-src/grouplink

3. place grouplink.profile.js in dojo-src/util/buildscripts/profiles

4. open shell in dojo-src/util/buildscripts

5. run:  ./build.sh profile=grouplink action=clean,release copyTests=false optimize=shrinksafe localeList=en,en-au,en-bz,en-ca,en-ie,en-jm,en-nz,en-ph,en-za,en-tt,en-gb,en-us,en-zw,fi-fi,zh-cn,zh-tw,nl-be,nl-nl,no,fr-be,fr-ca,fr-fr,fr-lu,fr-mc,fr-ch,de-at,de-de,de-li,de-lu,de-ch,pt-br,pt-pt,es,sv-se,sv-fi,tr-tr
The localeList should match the locales supported by ehd.

6. build output is created in dojo-src/release
