@echo off

rem  Database migration script for GroupLink eHelpDesk
set JAVA_OPTS=-Xmx256m -Dcom.sun.management.jmxremote

rem Make sure prerequisite environment variables are set
if not "%JAVA_HOME%" == "" goto gotJdkHome
echo JAVA_HOME environment variable is not defined.
echo This environment variable is needed to run this program.
goto end

:gotJdkHome
if not exist "%JAVA_HOME%\bin\java.exe" goto noJavaHome
if not exist "%JAVA_HOME%\bin\javaw.exe" goto noJavaHome
if not exist "%JAVA_HOME%\bin\jdb.exe" goto noJavaHome
if not exist "%JAVA_HOME%\bin\javac.exe" goto noJavaHome
if not "%JRE_HOME%" == "" goto okJavaHome
set JRE_HOME=%JAVA_HOME%
goto okJavaHome

:noJavaHome
echo The JAVA_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
echo NB: JAVA_HOME should point to a JDK not a JRE
goto end

:okJavaHome

rem  determine EHELPDESK_HOME
set CURRENT_DIR=%cd%
set EHELPDESK_HOME=%CURRENT_DIR%
if exist "%EHELPDESK_HOME%\migrate.bat" goto okHome
cd ..
set EHELPDESK_HOME=%cd%
cd %CURRENT_DIR%

:okHome

echo EHELPDESK_HOME:   %EHELPDESK_HOME%
echo JAVA_HOME:        %JAVA_HOME%
echo JAVA_OPTS:        %JAVA_OPTS%
echo CLASSPATH:        %CLASSPATH%

"%JAVA_HOME%\bin\java" %JAVA_OPTS% -Dehelpdesk.home="%EHELPDESK_HOME%" -jar "%EHELPDESK_HOME%/migration/ehd-migration-${project.version}.jar"

:end
