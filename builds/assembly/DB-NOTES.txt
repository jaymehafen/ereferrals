everything HelpDesk v10.1
All Rights Reserved GroupLink 2013
----------------------------------

Database Notes
--------------

Connection Pooling
------------------

The default maximum connection pool is now 20.  Customers are now allowed to
customize this by editing the jdbc.properties file located in the
WEB-INF/classes/conf directory.  Add the line:
jdbc.maxActive=20


MySQL
-----

Tables should be set up as INNODB rather than the default MyISAM so that
transactions and database constraints are available.

In your MySQL configuration file (example my.cnf), under the [mysqld] section,
put the following:
default-storage_engine=innodb

For more information visit:
http://dev.mysql.com/doc/refman/5.0/en/innodb.html


You may also want to change the default character set to support Unicode
characters.

In your MySQL configuration file (example my.cnf), under both the [client]
section and the [mysqld] section, put the following:
default-character-set=utf8

Oracle
------

You need to set the proper Oracle dialect depending on your version of Oracle.
Edit the jdbc.properties file (after running the installer), located in the
WEB-INF/classes/conf directory.  Edit the line that begins with "hibernate.dialect"
and set the value to be the following:

    Oracle 8i - org.hibernate.dialect.Oracle8iDialect
    Oracle 9i - org.hibernate.dialect.Oracle9iDialect
    Oracle 10g or higher - org.hibernate.dialect.Oracle10gDialect

For example, if using Oracle 10g:

hibernate.dialect=org.hibernate.dialect.Oracle10gDialect



