package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketPriorityDao;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateTicketPriorityDao extends HibernateDaoSupport implements TicketPriorityDao {

    @SuppressWarnings("unchecked")
    public List<TicketPriority> getPriorities() {
        return (List<TicketPriority>) getHibernateTemplate().find("select tp from TicketPriority as tp where (tp.active is null or tp.active = true) order by tp.order, tp.name");
    }

    @SuppressWarnings("unchecked")
    public TicketPriority getPriorityById(Integer id) {
        List<TicketPriority> priorities = (List<TicketPriority>) getHibernateTemplate().findByNamedParam("select tp from TicketPriority as tp where tp.id=:PRIORITYID", "PRIORITYID", id);
		if(priorities.size() > 0){
			return priorities.get(0);
		}
		return null;
    }

    @SuppressWarnings("unchecked")
    public TicketPriority getPriorityByName(String name) {
        List<TicketPriority> priorities = (List<TicketPriority>)getHibernateTemplate().findByNamedParam("select tp from TicketPriority as tp where tp.name=:NAME", "NAME", name);
		if(priorities.size() > 0){
			return priorities.get(0);
		}
		return null;
    }

    public void savePriority(TicketPriority priority) {
        getHibernateTemplate().saveOrUpdate(priority);
    }

    public void deletePriority(TicketPriority priority) {
        getHibernateTemplate().delete(priority);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

}
