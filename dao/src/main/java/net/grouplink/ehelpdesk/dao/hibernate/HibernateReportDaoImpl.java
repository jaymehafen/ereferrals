package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.ReportDao;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

public class HibernateReportDaoImpl extends HibernateDaoSupport implements ReportDao {
    public Report getById(Integer id) {
        return (Report) getHibernateTemplate().get(Report.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Report> getByUser(User user) {
        return getHibernateTemplate().findByNamedParam("from Report r where r.user = :user order by r.name", "user", user);
    }

    @SuppressWarnings("unchecked")
    public List<Report> getPublicMinusUser(User user) {
        return getHibernateTemplate().findByNamedParam("from Report r where r.privateReport = false and r.user != :user order by r.name", "user", user);
    }

    public void delete(Report report) {
        getHibernateTemplate().delete(report);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public void save(Report report) {
        getHibernateTemplate().save(report);
    }

    public int getUsageCount(Report report) {
        int count = 0;

        // check dashboard widgets
        count += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(w) from Widget w where w.report = :report", "report", report));


        return count;
    }

    @SuppressWarnings("unchecked")
    public List<Report> getByTicketFilter(TicketFilter ticketFilter) {
        return getHibernateTemplate().findByNamedParam("from Report r where r.ticketFilter = :tf", "tf", ticketFilter);
    }

}
