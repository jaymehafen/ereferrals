package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetCustomFieldDao;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomFieldValue;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

/**
 * User: jaymehafen
 * Date: Jul 18, 2008
 * Time: 1:29:40 PM
 */
public class HibernateAssetCustomFieldDao extends HibernateDaoSupport implements AssetCustomFieldDao {
    public AssetCustomField getById(Integer id) {
        return (AssetCustomField) getHibernateTemplate().get(AssetCustomField.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<AssetCustomField> getAll() {
        return (List<AssetCustomField>) getHibernateTemplate().loadAll(AssetCustomField.class);
    }

    @SuppressWarnings("unchecked")
    public List<AssetCustomField> getByAssetFieldGroup(AssetFieldGroup assetFieldGroup) {
        return (List<AssetCustomField>) getHibernateTemplate().findByNamedParam("from AssetCustomField acf where acf.assetFieldGroup = :ASSETFIELDGROUP", "ASSETFIELDGROUP", assetFieldGroup);
    }

    @SuppressWarnings("unchecked")
    public List<AssetCustomField> getByAsset(Asset asset) {
        return (List<AssetCustomField>) getHibernateTemplate().findByNamedParam("from AssetCustomField acf where acf.asset = :ASSET", "ASSET", asset);
    }

    public void saveAssetCustomField(AssetCustomField assetCustomField) {
        getHibernateTemplate().saveOrUpdate(assetCustomField);
    }

    public void deleteAssetCustomField(AssetCustomField assetCustomField) {
        getHibernateTemplate().delete(assetCustomField);
    }

    @SuppressWarnings("unchecked")
    public AssetCustomField getByName(String name) {
        List<AssetCustomField> l = (List<AssetCustomField>) getHibernateTemplate().findByNamedParam("from AssetCustomField acf where acf.name = :NAME", "NAME", name);
        return (l.size() > 0) ? l.get(0) : null;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public AssetCustomFieldValue getCustomFieldValue(Asset asset, AssetCustomField assetCustomField) {
        return (AssetCustomFieldValue) DataAccessUtils.singleResult(getHibernateTemplate()
                .findByNamedParam("from AssetCustomFieldValue acfl where acfl.asset = :asset and acfl.customField = :customField",
                new String[]{"asset", "customField"}, new Object[]{asset, assetCustomField}));
    }
}
