package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.AppointmentTypeDao;
import net.grouplink.ehelpdesk.domain.AppointmentType;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateAppointmentTypeDao extends HibernateDaoSupport implements AppointmentTypeDao {

    @SuppressWarnings("unchecked")
    public List<AppointmentType> getApptTypes() {
        return (List<AppointmentType>) getHibernateTemplate().find("from AppointmentType");
    }

    @SuppressWarnings("unchecked")
    public AppointmentType getApptTypeById(Integer id) {
        List<AppointmentType> apptTypes = (List<AppointmentType>) getHibernateTemplate().findByNamedParam("select at from AppointmentType as at where at.id=:APPTTYPEID", "APPTTYPEID", id);
        if (apptTypes.size() == 1) {
            return apptTypes.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public AppointmentType getApptTypeByName(String name) {
        List<AppointmentType> apptTypes = (List<AppointmentType>) getHibernateTemplate().findByNamedParam("select at from AppointmentType as at where at.name=:NAME", "NAME", name);
        if (apptTypes.size() == 1) {
            return apptTypes.get(0);
        }
        return null;
    }

    
    public void saveApptType(AppointmentType apptType) {
        getHibernateTemplate().saveOrUpdate(apptType);
    }

    public void deleteApptType(AppointmentType apptType) {
        getHibernateTemplate().delete(apptType);
    }
}
