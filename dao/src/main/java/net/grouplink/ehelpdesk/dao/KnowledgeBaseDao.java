package net.grouplink.ehelpdesk.dao;

import java.util.List;
import java.util.Map;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.KnowledgeBase;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.User;


public interface KnowledgeBaseDao {

    KnowledgeBase getKnowledgeBaseById(Integer id);

    List<KnowledgeBase> searchKnowLedgeBase(String searchText, Integer type, Integer groupId, User user);

    List<KnowledgeBase> searchKnowLedgeBaseByGroupId(String searchText, Integer type, Integer groupId, User user);

    List<KnowledgeBase> searchKnowledgeBaseByUser(String searchText, int start, int count, Map<String, Boolean> sortFieldsOrder, User user);

    int getKbArticleCountByUser(String searchText, User user);

    void saveKnowledgeBase(KnowledgeBase knowledgeBase);

    void deleteKnowledgeBase(KnowledgeBase knowledgeBase);

    void deleteKnowledgeBaseAttachments(Attachment attach);

    void flush();

    void clear();

    Attachment getNonPrivateAttachment(Integer attachmentId);

    int getCountByGroup(Group group);
}
