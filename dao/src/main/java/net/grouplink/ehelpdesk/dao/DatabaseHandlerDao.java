package net.grouplink.ehelpdesk.dao;

/**
 * Created by IntelliJ IDEA.
 * User: dgarcia
 * Date: Aug 8, 2007
 * Time: 4:31:29 PM
 */
public interface DatabaseHandlerDao {
    public void databaseInit();

    void ticketCorrection(int nextId);
}
