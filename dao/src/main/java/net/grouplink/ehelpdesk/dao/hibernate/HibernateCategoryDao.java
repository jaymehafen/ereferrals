package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.AssignmentDao;
import net.grouplink.ehelpdesk.dao.CategoryDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Assignment;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateCategoryDao extends HibernateDaoSupport implements CategoryDao {

    private AssignmentDao assignmentDao;

    @SuppressWarnings("unchecked")
    public List<Category> getCategories() {
        return (List<Category>) getHibernateTemplate().find("select c from Category as c where c.active = true order by c.name asc");
    }

    @SuppressWarnings("unchecked")
    public Category getCategoryById(Integer id) {
        List<Category> cats = (List<Category>) getHibernateTemplate().findByNamedParam("select c from Category as c where c.id=:ID", "ID", id);
        if (cats.size() > 0) {
            return cats.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public Category getCategoryByName(String name) {
        List<Category> cats = (List<Category>) getHibernateTemplate().findByNamedParam("select c from Category as c where c.name=:NAME", "NAME", name);
        if (cats.size() > 0) {
            return cats.get(0);
        }
        return null;
    }

    public void saveCategory(Category category) {
        getHibernateTemplate().saveOrUpdate(category);
    }

    @SuppressWarnings("unchecked")
    public void deleteCategory(Category category) {
//        getHibernateTemplate().delete(category);

        List<CategoryOption> catOpts;
        catOpts = category.getCategoryOptionsList();
        //Need to inactivate all category options beloging to this category
        //And delete all TechLocCatOpt assigments
        for (Object catOpt1 : catOpts) {
            CategoryOption catOpt = (CategoryOption) catOpt1;
            catOpt.setActive(false);
            List<Assignment> techLocCatOptList;
            techLocCatOptList = assignmentDao.getByCatOpt(catOpt);
            assignmentDao.deleteList(techLocCatOptList);
            getHibernateTemplate().saveOrUpdate(catOpt);
        }

        category.setActive(false);
        getHibernateTemplate().saveOrUpdate(category);
    }

    @SuppressWarnings("unchecked")
    public boolean isDuplicateCategory(Category cat) {
        List<Category> cats;
        cats = (List<Category>) getHibernateTemplate().findByNamedParam(
                "select c from Category as c where c.name=:name and c.group = :group and c.active = true",
                new String[]{"name", "group"}, new Object[]{cat.getName(), cat.getGroup()});
        if (cat.getId() == null) {
            if (cats.size() > 0) {
                return true;
            }
        } else if (cats.size() > 1) {
            return true;
        }
        return false;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public Category getCategoryByNameAndGroupId(String categoryName, Integer groupId) {
        List<Category> cats;
        cats = (List<Category>) getHibernateTemplate().findByNamedParam("select c from Category as c where c.name=:catName and c.group.id = :groupId and c.active=true", new String[]{"catName", "groupId"}, new Object[]{categoryName, groupId});
        if (cats.size() > 0) {
            return cats.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Category> getCategoryByGroupId(Integer id) {
        return (List<Category>) getHibernateTemplate().findByNamedParam("select c from Category as c where c.group.id = :groupId and c.active = true  order by c.name asc", "groupId", id);
    }

    @SuppressWarnings("unchecked")
    public List<Category> getAllCategoriesByGroup(Group group) {
        return (List<Category>) getHibernateTemplate().findByNamedParam(
                "from Category as c where c.group = :group order by c.name asc", "group", group);
    }

    public void justDeleteCategory(Category category) {
        getHibernateTemplate().delete(category);
    }

    public void setAssignmentDao(AssignmentDao techLocCatOptDao) {
        this.assignmentDao = techLocCatOptDao;
    }
}
