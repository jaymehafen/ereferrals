package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;

import java.util.List;

public interface TemplateMasterDao {
    List<TicketTemplateMaster> getAll();

    void flush();

    TicketTemplateMaster getById(Integer id);

    void save(TicketTemplateMaster master);

    void delete(TicketTemplateMaster master);

    List<TicketTemplateMaster> getAllPublic();

    int getCountByGroup(Group group);

    void clear();
}
