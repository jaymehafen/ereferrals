package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.ScheduleStatusDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;

import java.util.List;

import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateScheduleStatusDao extends HibernateDaoSupport implements ScheduleStatusDao {

    @SuppressWarnings("unchecked")
    public List<ScheduleStatus> getScheduleStatus() {
        return getHibernateTemplate().find("select ss from ScheduleStatus as ss");
    }

    public ScheduleStatus getScheduleStatusById(Integer id) {
        List scheduleStati = getHibernateTemplate().findByNamedParam(
                "select ss from ScheduleStatus as ss where ss.id=:SSID", "SSID", id);
        if (scheduleStati.size() == 1) {
            return (ScheduleStatus) scheduleStati.get(0);
        }
        return null;
    }

    public void saveScheduleStatus(ScheduleStatus scheduleStatus) {
        getHibernateTemplate().saveOrUpdate(scheduleStatus);
    }

    public void deleteScheduleStatus(ScheduleStatus scheduleStatus) {
        getHibernateTemplate().delete(scheduleStatus);
    }

    @SuppressWarnings("unchecked")
    public List<ScheduleStatus> getScheduleStatusByGroupId(Integer id) {
        return getHibernateTemplate().findByNamedParam(
                "select ss from ScheduleStatus as ss where ss.group.id=:ID", "ID", id);
    }

    public int getCountByGroup(Group group) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from ScheduleStatus as ss where ss.group = :group", "group", group));
    }

    public int getCountByCategory(Category category) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from ScheduleStatus as ss where ss.category = :category", "category", category));
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from ScheduleStatus as ss where ss.categoryOption = :categoryOption", "categoryOption", categoryOption));

    }
}
