package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.DatabaseHandlerDao;
import net.grouplink.ehelpdesk.common.utils.SqlSeed;
import org.hibernate.dialect.Dialect;
import org.hibernate.jdbc.Work;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Date: Aug 8, 2007
 * Time: 11:13:43 AM
 */
public class DatabaseHandlerImpl extends HibernateDaoSupport implements DatabaseHandlerDao {
    private Properties hibernateProperties;

    public void databaseInit() {
        logger.info("Initializing database information");

        getHibernateTemplate().setFlushMode(HibernateTemplate.FLUSH_NEVER);

        getSession().doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                Dialect dialect = Dialect.getDialect(hibernateProperties);
                String[] sql = getSqlSeed(dialect);


                if (sql != null && sql.length > 0) {
                    boolean oldAutoCommit = connection.getAutoCommit();
                    if (!oldAutoCommit) {
                        connection.setAutoCommit(true);
                    }
                    try {
                        Statement stmt = connection.createStatement();
                        try {
                            for (String aSql : sql) {
                                if (logger.isDebugEnabled()) {
                                    logger.debug("Executing schema statement: " + aSql);
                                }
                                try {
                                    stmt.executeUpdate(aSql);
                                } catch (SQLException ex) {
                                    if (logger.isWarnEnabled()) {
                                        logger.warn("Unsuccessful schema statement: " + aSql, ex);
                                    }
                                }
                            }
                        } finally {
                            if (stmt != null) {
                                try {
                                    stmt.close();
                                }
                                catch (SQLException ex) {
                                    logger.trace("Could not close JDBC Statement", ex);
                                }
                                catch (Throwable ex) {
                                    // We don't trust the JDBC driver: It might throw RuntimeException or Error.
                                    logger.trace("Unexpected exception on closing JDBC Statement", ex);
                                }
                            }
                        }
                    } finally {
                        if (!oldAutoCommit) {
                            connection.setAutoCommit(false);
                        }
                    }
                }
            }
        });
    }

    public void ticketCorrection(final int nextId) {
        logger.info("Initializing database information");

        getHibernateTemplate().setFlushMode(HibernateTemplate.FLUSH_NEVER);

        getSession().doWork(new Work() {
            public void execute(Connection connection) throws SQLException {

                boolean oldAutoCommit = connection.getAutoCommit();
                if (!oldAutoCommit) {
                    connection.setAutoCommit(true);
                }
                try {
                    String aSql = "INSERT INTO TICKET (id) VALUES (" + nextId + ")";
                    Statement stmt = connection.createStatement();
                    try {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Executing schema statement: " + aSql);
                        }
                        try {
                            stmt.executeUpdate(aSql);
                        } catch (SQLException ex) {
                            if (logger.isWarnEnabled()) {
                                logger.warn("Unsuccessful schema statement: " + aSql, ex);
                            }
                        }
                    } finally {
                        if (stmt != null) {
                            try {
                                stmt.close();
                            }
                            catch (SQLException ex) {
                                logger.trace("Could not close JDBC Statement", ex);
                            }
                            catch (Throwable ex) {
                                // We don't trust the JDBC driver: It might throw RuntimeException or Error.
                                logger.trace("Unexpected exception on closing JDBC Statement", ex);
                            }
                        }
                    }
                } finally {
                    if (!oldAutoCommit) {
                        connection.setAutoCommit(false);
                    }
                }
            }
        });
    }

    public String[] getSqlSeed(Dialect dialect) {
        return SqlSeed.getSqlSeed(dialect.toString());
    }

    public void setHibernateProperties(Properties hibernateProperties) {
        this.hibernateProperties = hibernateProperties;
    }

    public Properties getHibernateProperties() {
        return hibernateProperties;
    }
}
