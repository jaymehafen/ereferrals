package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.CustomFieldType;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 3:29:01 AM
 */
public interface CustomFieldTypesDao {

    /**
     * Gets a list of all custom field types from the database.
     * @return list of custom field types.
     */
    public List<CustomFieldType> getCustomFieldTypes();

    /**
     * Gets a custom field type from its id.
     * @param id The custom field type id
     * @return custom field type object
     */
    public CustomFieldType getCustomFieldType(Integer id);
    
}
