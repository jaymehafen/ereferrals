package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketDao;
import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

public class HibernateTicketDao extends HibernateDaoSupport implements TicketDao, Serializable {
    /**
     * The name of the deletedTickets filter that excludes tickets in deleted status from searches
     */
    private static final String DELETED_TICKETS_FILTER_NAME = "deletedTickets";
    private static final String TT_PENDING_FILTER_NAME = "ttPendingTickets";

    protected final Log logger = LogFactory.getLog(getClass());

    public void deleteTicket(Ticket ticket) {
        getHibernateTemplate().delete(ticket);
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getAllTicketsById(Integer ticketId) {
        getSession().disableFilter(TT_PENDING_FILTER_NAME);
        getSession().disableFilter(DELETED_TICKETS_FILTER_NAME);
        List<Ticket> list = (List<Ticket>) getHibernateTemplate().findByNamedParam("select t from Ticket as t where t.id=:ticketId and t.oldTicketId is null", "ticketId", ticketId);
        list.addAll(getHibernateTemplate().findByNamedParam("select t from Ticket as t where t.oldTicketId = :ticketId", "ticketId", ticketId));
        return list;
    }

    @SuppressWarnings("unchecked")
    public int cleanUpDummyTickets() {
        List<Ticket> ticketList = (List<Ticket>) getHibernateTemplate().find("select t from Ticket as t where t.group is null and t.location is null");
        getHibernateTemplate().deleteAll(ticketList);
        getHibernateTemplate().flush();

        int count = DataAccessUtils.intResult(getHibernateTemplate().find("select count(*) from Ticket as t"));
        int oldId = 0;
        int newId = 0;

        if (count > 0) {
            List maxOld = getHibernateTemplate().find("select max(oldTicketId) from Ticket as t");
            if (maxOld.get(0) != null) {
                oldId = DataAccessUtils.intResult(maxOld);
            }
            List maxId = getHibernateTemplate().find("select max(id) from Ticket as t");
            if (maxId.get(0) != null) {
                newId = DataAccessUtils.intResult(maxId);
            }
        }


        if (oldId > newId) {
            return oldId;
        } else {
            return 0;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsByEmailToTicketConfig(EmailToTicketConfig emailToTicketConfig) {
        // disable the deleted tickets filter to get all tickets, including deleted
        getSession().disableFilter(DELETED_TICKETS_FILTER_NAME);
        // re-enable the filter for this session
//        getSession().enableFilter(DELETED_TICKETS_FILTER_NAME);
        return (List<Ticket>) getHibernateTemplate().findByNamedParam("from Ticket as t where t.emailToTicketConfig = :ETTCONFIG", "" +
                "ETTCONFIG", emailToTicketConfig);
    }

    @SuppressWarnings("unchecked")
    public Ticket getTicketById(Integer id) {
        if (id != null) {
            getSession().disableFilter(TT_PENDING_FILTER_NAME);
            getSession().disableFilter(DELETED_TICKETS_FILTER_NAME);

            List<Ticket> tickets = (List<Ticket>) getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.id=:TICKETID", "TICKETID", id);
            if (tickets.size() > 0) {
                return tickets.get(0);
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTickets() {
        return (List<Ticket>) getHibernateTemplate().find("select t from Ticket as t");
    }

    /**
     * This method is only to be called by ticketService to make sure tickets get final modified date stamp
     * and status time table added to the ticket
     * If you need to save a ticket always call ticketService.saveTicket(Ticket ticket)
     *
     * @param ticket the ticket to save
     */
    public void saveServiceTicket(Ticket ticket) {
        getHibernateTemplate().saveOrUpdate(ticket);
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsByOwner(User user, Boolean showClosed) {
        if (showClosed) {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.contact=:CONTACT order by t.priority",
                    "CONTACT", user);
        } else {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.contact=:CONTACT and t.status.id != 6 order by t.priority",
                    "CONTACT", user);
        }
    }

    public Integer getTicketsByOwnerCount(User user) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Ticket as t where t.contact=:CONTACT and t.status.id != 6 and t.status.id != 5",
                "CONTACT", user));
    }

    public Integer getTicketsByUrgIdCount(Integer urgId) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Ticket as t where t.assignedTo.id =:urgId and t.status.id != 6 and t.status.id != 5",
                "urgId", urgId));
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsAssignedToByUserId(Integer userId) {
        return (List<Ticket>) getHibernateTemplate().findByNamedParam("select t from Ticket as t where t.assignedTo.userRole.user.id = :userId", "userId", userId);
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsAssignedToByUserRoleGroup(UserRoleGroup userRoleGroup, Boolean showClosed) {
        if (showClosed) {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.assignedTo=:assignedTo order by t.priority",
                    "assignedTo", userRoleGroup);
        } else {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.assignedTo=:assignedTo and t.status.id != 6 order by t.priority",
                    "assignedTo", userRoleGroup);
        }
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getAllGroupTicketsByGroupId(Integer groupId, Boolean showClosed) {
        if (showClosed) {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.assignedTo.group.id=:groupId order by t.priority",
                    "groupId", groupId);
        } else {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.assignedTo.group.id=:groupId and t.status.id != 6 order by t.priority",
                    "groupId", groupId);
        }
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsByCriteria(Group group, Set<Status> statuses, Set<TicketPriority> priorities, Category category,
                                             CategoryOption categoryOption) {
        Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(Ticket.class);

        c.createAlias("status", "status");
        c.add(Restrictions.ne("status.id", 6));
        c.add(Restrictions.ne("status.id", -1));

        // only tickets where ticketTemplateStatus is null or does not equal pending status
        Disjunction ttsDis = Restrictions.disjunction();
        ttsDis.add(Restrictions.isNull("ticketTemplateStatus"));
        ttsDis.add(Restrictions.ne("ticketTemplateStatus", Ticket.TT_STATUS_PENDING));
        c.add(ttsDis);

        c.add(Restrictions.eq("group", group));
        if (CollectionUtils.isNotEmpty(statuses)) {
            Disjunction dis = Restrictions.disjunction();
            for (Status status : statuses) {
                dis.add(Restrictions.eq("status", status));
            }

            c.add(dis);
        }

        if (CollectionUtils.isNotEmpty(priorities)) {
            Disjunction dis = Restrictions.disjunction();
            for (TicketPriority priority : priorities) {
                dis.add(Restrictions.eq("priority", priority));
            }

            c.add(dis);
        }

        if (category != null) {
            c.add(Restrictions.eq("category", category));
        }

        if (categoryOption != null) {
            c.add(Restrictions.eq("categoryOption", categoryOption));
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return c.list();
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsByAdvancedSearch(AdvancedTicketsFilter filter) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(Ticket.class);

        // priority
        if (filter.getPriority() != null) {
            cri.add(Restrictions.eq("priority.id", filter.getPriority().getId()));
        }

        // contact
        if (filter.getContactId() != null && filter.getContactId() > 0) {
            cri.add(Restrictions.eq("contact.id", filter.getContactId()));
        }

        // submitted by
        if (filter.getSubmittedById() != null && filter.getSubmittedById() > 0) {
            cri.add(Restrictions.eq("submittedBy.id", filter.getSubmittedById()));
        }

        // assigned to
        if (filter.getAssignedTo() != null) {
            if (Boolean.valueOf(filter.getAssignedToTicketPool()) == Boolean.TRUE) {
                cri.add(Restrictions.eq("assignedTo.id", filter.getAssignedTo()));
            } else {
                cri.createAlias("assignedTo", "assignedTo").createAlias("assignedTo.userRole", "userRole").createAlias("userRole.user", "user").add(Restrictions.eq("user.id", filter.getAssignedTo()));
            }
        }

        // subject
        Object[] subject = searchStringToList(new StringBuilder(filter.getSubject()));
        for (Object aSubject : subject) {
            cri.add(Restrictions.like("subject", "%" + aSubject + "%"));
        }

        Object[] note = searchStringToList(new StringBuilder(filter.getNote()));
        for (Object aNote : note) {
            cri.add(Restrictions.like("note", "%" + aNote + "%"));
        }

        Object[] comment = searchStringToList(new StringBuilder(filter.getComments()));
        if (comment.length > 0) {
            cri.createAlias("ticketHistory", "ticketHistory");
        }
        for (Object aComment : comment) {
            cri.add(Restrictions.like("ticketHistory.note", "%" + aComment + "%"));
        }

        // location
        if (filter.getLocation() != null) {
            cri.add(Restrictions.eq("location.id", filter.getLocation().getId()));
        }

        // group
        if (filter.getGroup() != null) {
            cri.add(Restrictions.eq("group.id", filter.getGroup().getId()));
        } else {
            cri.add(Restrictions.in("group.id", filter.getGroupList()));
        }

        // category
        if (filter.getCategory() != null) {
            cri.add(Restrictions.eq("category.id", filter.getCategory().getId()));
        }

        // category option
        if (filter.getCategoryOption() != null) {
            cri.add(Restrictions.eq("categoryOption.id", filter.getCategoryOption().getId()));
        }

        // status
        if (filter.getTicketStatus() != null) {
            cri.add(Restrictions.eq("status.id", filter.getTicketStatus().getId()));
        }

        // creation date
        String operator = filter.getCreateDateOperator();
        if (StringUtils.isNotBlank(operator)) {
            createDateCritiria(operator, "createdDate", cri, filter.getCreationDate(), filter.getCreationDate2());
        }

        // modify date
        operator = filter.getModifiedDateOperator();
        if (StringUtils.isNotBlank(operator)) {
            createDateCritiria(operator, "modifiedDate", cri, filter.getModifiedDate(), filter.getModifiedDate2());
        }

        // estimated completion date
        operator = filter.getEstimatedDateOperator();
        if (StringUtils.isNotBlank(filter.getEstimatedDateOperator())) {
            createDateCritiria(operator, "estimatedDate", cri, filter.getEstimatedDate(), filter.getEstimatedDate2());
        }

        // work time
        String workTimeOperator = filter.getWorkTimeOperator();
        if (StringUtils.isNotBlank(workTimeOperator)) {
            int hours = 0;
            if (StringUtils.isNotBlank(filter.getWorkTimeHours())) {
                hours = Integer.parseInt(filter.getWorkTimeHours());
            }
            int minutes = 0;
            if (StringUtils.isNotBlank(filter.getWorkTimeMinutes())) {
                minutes = Integer.parseInt(filter.getWorkTimeMinutes());
            }
            int seconds = (hours * 60 * 60) + (minutes * 60);

            if (workTimeOperator.equals("more")) {
                cri.add(Restrictions.ge("workTime", seconds));
            }
            if (workTimeOperator.equals("less")) {
                cri.add(Restrictions.lt("workTime", seconds));
            }
        }
        cri.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return (List<Ticket>) cri.list();
    }

    private void createDateCritiria(String operator, String field, Criteria cri, String fromDate, String toDate) {
        if (operator.equals("today")) {
            Calendar today = Calendar.getInstance();
            resetToDay(today);
            cri.add(Restrictions.ge(field, today.getTime()));
        }
        if (operator.equals("thisweek")) {
            Calendar weekToDate = Calendar.getInstance();
            weekToDate.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            resetToDay(weekToDate);
            cri.add(Restrictions.ge(field, weekToDate.getTime()));
        }
        if (operator.equals("thismonth")) {
            Calendar monthToDate = Calendar.getInstance();
            monthToDate.set(Calendar.DAY_OF_MONTH, 1);
            monthToDate.set(Calendar.HOUR_OF_DAY, 0);
            resetToDay(monthToDate);
            cri.add(Restrictions.ge(field, monthToDate.getTime()));
        }
        if (operator.equals("on")) {
            String[] creationArray = fromDate.split("-");
            Calendar creationDate = Calendar.getInstance();
            creationDate.set(Integer.parseInt(creationArray[0]), Integer.parseInt(creationArray[1]) - 1, Integer.parseInt(creationArray[2]), 0, 0, 0);
            Calendar creationDate2 = (Calendar) creationDate.clone();
            creationDate2.add(Calendar.DATE, 1);
            cri.add(Restrictions.between(field, creationDate.getTime(), creationDate2.getTime()));
        }
        if (operator.equals("before")) {
            String[] creationArray = fromDate.split("-");
            Calendar creationDate = Calendar.getInstance();
            creationDate.set(Integer.parseInt(creationArray[0]), Integer.parseInt(creationArray[1]) - 1, Integer.parseInt(creationArray[2]), 0, 0, 0);
            cri.add(Restrictions.lt(field, creationDate.getTime()));
        }
        if (operator.equals("after")) {
            String[] creationArray = fromDate.split("-");
            Calendar creationDate = Calendar.getInstance();
            creationDate.set(Integer.parseInt(creationArray[0]), Integer.parseInt(creationArray[1]) - 1, Integer.parseInt(creationArray[2]), 0, 0, 0);
            cri.add(Restrictions.ge(field, creationDate.getTime()));
        }
        if (operator.equals("range")) {
            String[] creationArray = fromDate.split("-");
            Calendar creationDate = Calendar.getInstance();
            creationDate.set(Integer.parseInt(creationArray[0]), Integer.parseInt(creationArray[1]) - 1, Integer.parseInt(creationArray[2]), 0, 0, 0);
            String[] creationArray2 = toDate.split("-");
            Calendar creationDate2 = (Calendar) creationDate.clone();
            creationDate2.set(Integer.parseInt(creationArray2[0]), Integer.parseInt(creationArray2[1]) - 1, Integer.parseInt(creationArray2[2]), 23, 59, 59);
            cri.add(Restrictions.between(field, creationDate.getTime(), creationDate2.getTime()));
        }
    }

    private void resetToDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsByTicketPool(Integer groupId, Boolean showClosed) {
        if (showClosed) {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam("select t from Ticket as t where t.assignedTo.group.id = :groupId and t.assignedTo.userRole is null", "groupId", groupId);
        } else {
            return (List<Ticket>) getHibernateTemplate().findByNamedParam("select t from Ticket as t where t.assignedTo.group.id = :groupId and t.assignedTo.userRole is null and t.status.id != 6", "groupId", groupId);
        }
    }

    public Ticket getTicketByOldTicketIdAndGroupId(Integer oldTicketId, Integer groupId) {
        if (oldTicketId != null) {
            List tickets = getHibernateTemplate().findByNamedParam(
                    "select t from Ticket as t where t.oldTicketId = :oldTicketId and t.group.id = :groupId", new String[]{"oldTicketId", "groupId"}, new Object[]{oldTicketId, groupId});
            if (tickets.size() > 0) {
                return (Ticket) tickets.get(0);
            }
        }
        return null;
    }

    public Ticket getTicketByOldTicketId(Integer ticketId) {
        getSession().disableFilter(TT_PENDING_FILTER_NAME);
        getSession().disableFilter(DELETED_TICKETS_FILTER_NAME);
        List tickets = getHibernateTemplate()
                .findByNamedParam("select t from Ticket as t where t.oldTicketId = :oldTicketId", "oldTicketId", ticketId);
        if (tickets.size() > 0) {
            return (Ticket) tickets.get(0);
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsByZenAsset(ZenAsset zenAsset) {
        return getHibernateTemplate().findByNamedParam("from Ticket t where t.zenAsset = :zenAsset", "zenAsset", zenAsset);
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTopLevelTicketsByTicketTemplateLaunch(TicketTemplateLaunch ttl) {
        getSession().disableFilter(TT_PENDING_FILTER_NAME);
        return getHibernateTemplate().findByNamedParam("from Ticket t where t.ticketTemplateLaunch = :ttl and t.parent is null", "ttl", ttl);
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> getTicketsByTicketTemplate(TicketTemplate ticketTemplate) {
        return getHibernateTemplate().findByNamedParam("from Ticket t where t.ticketTemplate = :tt", "tt", ticketTemplate);
    }

    public int getCountByCategory(Category category) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Ticket as t where t.category = :category", "category", category));
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Ticket as t where t.categoryOption = :categoryOption", "categoryOption", categoryOption));
    }

    public void evict(Ticket ticket) {
        getHibernateTemplate().evict(ticket);
    }

    public void clear() {
        getHibernateTemplate().clear();
    }

    public int getTicketCountByGroupId(Integer groupId) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Ticket as t where t.group.id = :groupId", "groupId", groupId));
    }

    public int getTicketCountByCatOptId(Integer catOptId) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Ticket as t where t.categoryOption.id = :catOptId", "catOptId", catOptId));
    }

    public int getTicketCountByStatusId(Integer statusId) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Ticket as t where t.status.id = :statusId", "statusId", statusId));
    }

    public int getTicketCountByPriorityId(Integer priorityId) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Ticket as t where t.priority.id = :priorityId and t.status.id != -1", "priorityId", priorityId));
    }

    public Object[] searchStringToList(StringBuilder text) {
        List<String> resultList = new ArrayList<String>();
        while (text.indexOf("\"") != -1 && text.indexOf("\"", text.indexOf("\"") + 1) != -1) {
            resultList.add(text.substring(text.indexOf("\"") + 1, text.indexOf("\"", text.indexOf("\"") + 1)));
            text.delete(text.indexOf("\""), text.indexOf("\"", text.indexOf("\"") + 1) + 1);
        }
        if (StringUtils.isNotBlank(text.toString().trim())) {
            resultList.addAll(Arrays.asList(text.toString().trim().split(" ")));
        }
        return resultList.toArray();
    }
}
