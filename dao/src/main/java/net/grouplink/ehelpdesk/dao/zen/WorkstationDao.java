package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.Workstation;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;
import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Mar 31, 2009
 * Time: 8:47:50 PM
 */
public interface WorkstationDao {
    List<Workstation> getAll();

    List<Workstation> search(ZenAssetSearch zas);

    Workstation getById(byte[] id);

    Workstation getByIdString(String id);

    List<Workstation> getByPrimaryUser(String primaryUserGuid);

    List<Workstation> getByUserPrimaryDeviceInformation(List<UserPrimaryDeviceInformation> userPrimaryDeviceInformations);
}
