package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.VendorDao;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 14, 2008
 * Time: 2:45:17 PM
 */
public class HibernateVendorDao extends HibernateDaoSupport implements VendorDao {
    public Vendor getById(Integer id) {
        return (Vendor) getHibernateTemplate().get(Vendor.class, id);
    }

    @SuppressWarnings("unchecked")
    public Vendor getByName(String name) {
        List<Vendor> list = (List<Vendor>) getHibernateTemplate().findByNamedParam("from Vendor v where v.name=:NAME", "NAME", name);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Vendor> getAllVendors() {
        return (List<Vendor>) getHibernateTemplate().find("from Vendor v order by v.name");
    }

    public void saveVendor(Vendor vendor) {
        getHibernateTemplate().saveOrUpdate(vendor);
    }

    public void deleteVendor(Vendor vendor) {
        getHibernateTemplate().delete(vendor);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
