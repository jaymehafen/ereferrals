package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.ScheduleStatus;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface ScheduleStatusDao {
    public List<ScheduleStatus> getScheduleStatus();
    public ScheduleStatus getScheduleStatusById(Integer id);
    public void saveScheduleStatus(ScheduleStatus scheduleStatus);
    public void deleteScheduleStatus(ScheduleStatus scheduleStatus);
    public List<ScheduleStatus> getScheduleStatusByGroupId(Integer id);

    int getCountByGroup(Group group);

    int getCountByCategory(Category category);

    int getCountByCategoryOption(CategoryOption categoryOption);
}
