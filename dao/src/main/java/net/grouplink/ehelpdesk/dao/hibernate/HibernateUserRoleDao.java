package net.grouplink.ehelpdesk.dao.hibernate;

import java.util.List;

import net.grouplink.ehelpdesk.dao.UserRoleDao;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.User;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.apache.commons.lang.StringUtils;

public class HibernateUserRoleDao extends HibernateDaoSupport implements UserRoleDao {

    @SuppressWarnings("unchecked")
	public UserRole getUserRoleById(Integer id) {
        List<UserRole> userRoles = (List<UserRole>) getHibernateTemplate().findByNamedParam("select ur from UserRole as ur where ur.id=:id", "id", id);
		if (userRoles.size() > 0) {
			return userRoles.get(0);
		}
		return null;
	}

	public void deleteUserRole(UserRole userRole) {
		getHibernateTemplate().delete(userRole);
    }

    @SuppressWarnings("unchecked")
    public UserRole getUserRoleByUserIdAndRoleId(Integer userId, Integer roleId) {
        List<UserRole> userRoles = (List<UserRole>) getHibernateTemplate().findByNamedParam("select ur from UserRole as ur where ur.user.id = :userId and ur.role.id = :roleId and ur.active = true", new String[]{"userId", "roleId"}, new Object[]{userId, roleId});
		if (userRoles.size() > 0) {
			return userRoles.get(0);
		}
		return null;
    }

    @SuppressWarnings("unchecked")
    public User getUserByUserNameAndRoleId(String userName, Integer roleId) {
        List<User> userList = (List<User>) getHibernateTemplate().findByNamedParam(
                "select ur.user from UserRole as ur where lower(ur.user.loginId) = :loginId and ur.role.id = :roleId and ur.active = true",
                new String[]{"loginId", "roleId"}, new Object[]{StringUtils.lowerCase(userName), roleId});
		if (userList.size() > 0) {
			return userList.get(0);
		}
		return null;
    }

    @SuppressWarnings("unchecked")
    public User getUserByEmailAndRoleId(String email, Integer roleId) {
        List<User> userList = (List<User>) getHibernateTemplate().findByNamedParam(
                "select ur.user from UserRole as ur where lower(ur.user.email) = :email and ur.role.id = :roleId and ur.active = true",
                new String[]{"email", "roleId"}, new Object[]{StringUtils.lowerCase(email), roleId});
		if (userList.size() > 0) {
			return userList.get(0);
		}
		return null;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public void inactivateUserRole(UserRole userRole) {
        userRole.setActive(false);
        userRole.setRole(null);
        getHibernateTemplate().saveOrUpdate(userRole);
    }

    public void saveUserRole(UserRole userRole) {
        getHibernateTemplate().saveOrUpdate(userRole);
    }
}
