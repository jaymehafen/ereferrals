package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TemplateMasterDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class HibernateTemplateMasterDao  extends HibernateDaoSupport implements TemplateMasterDao {

    @SuppressWarnings("unchecked")
    public List<TicketTemplateMaster> getAll() {
        return getHibernateTemplate().find("from TicketTemplateMaster ttm");
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public TicketTemplateMaster getById(Integer id) {
        return (TicketTemplateMaster) getHibernateTemplate().get(TicketTemplateMaster.class, id);
    }

    public void save(TicketTemplateMaster master) {
        getHibernateTemplate().saveOrUpdate(master);
    }

    public void delete(TicketTemplateMaster master) {
        getHibernateTemplate().delete(master);
    }

    @SuppressWarnings("unchecked")
    public List<TicketTemplateMaster> getAllPublic() {
        return getHibernateTemplate().find("from TicketTemplateMaster t where t.privified = false order by t.name");
    }

    public int getCountByGroup(Group group) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from TicketTemplateMaster as t where t.group = :group", "group", group));

    }

    public void clear() {
        getHibernateTemplate().clear();
    }
}
