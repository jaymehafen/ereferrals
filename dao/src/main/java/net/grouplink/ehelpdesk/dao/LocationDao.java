package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Location;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface LocationDao {
    public List<Location> getLocations();
    public List<Location> getAllLocations();
    public List<Location> getLocations(String queryParam, int start, int count);
    public Location getLocationById(Integer id);
    public Location getLocationByName(String name);
    public void saveLocation(Location location);
    public void deleteLocation(Location location);

    public boolean hasDuplicateLocationName(Location location);

    public void flush();

    int getLocationCount(String queryParam);
}
