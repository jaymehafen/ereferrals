package net.grouplink.ehelpdesk.dao;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.common.utils.GroupSearch;

public interface GroupDao {

    List<Group> getGroups();

    Group getGroupById(Integer id);

    List<Group> getGroupsByTechId(Integer userId);

    List<Group> getGroupsByManagerId(Integer userId);

    void saveGroup(Group group);

    void deleteGroup(Object group);

    List<Group> searchGroup(GroupSearch groupSearch);

    boolean hasDuplicateGroupName(Group group);

    Group getGroupByName(String groupName);

    void flush();

    List<Group> getGroupsByAssetTrackerType(String assetTrackerType);

    int getGroupCount();

    List<Group> getAllGroups();
}
