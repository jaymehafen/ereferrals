package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.common.utils.UserSearch;
import net.grouplink.ehelpdesk.dao.UserDao;
import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Hibernate implementation of UserDao
 *
 * @author mrollins
 * @version 1.0
 */
public class HibernateUserDao extends HibernateDaoSupport implements UserDao {

    private static final String DELETED_TICKETS_FILTER_NAME = "deletedTickets";

    protected final Log logger = LogFactory.getLog(getClass());

    /**
     * Queries the database for a User with <code>id</code>.
     *
     * @param id the integer identifying a user uniquely
     * @return the <code>User</code> associated with <code>hexId</code>
     *         or a blank <code>User</code> if one is not found
     */
    @SuppressWarnings({"unchecked"})
    public User getUserById(Integer id) {
        return getHibernateTemplate().get(User.class, id);
    }

    @SuppressWarnings({"unchecked"})
    public User getUserByUsername(String username) {
        List<User> users;
        users = (List<User>) getHibernateTemplate().findByNamedParam(
                "select u from User as u where lower(u.loginId) = :loginId", "loginId", StringUtils.lowerCase(username));

        if (users.size() > 0) {
            return users.get(0);
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    public User getUserByUsernameAndPassword(String username, String password) {
        List<User> users;
        users = (List<User>) getHibernateTemplate().findByNamedParam(
                "select u from User as u where lower(u.loginId) = :userId and u.password=:password",
                new String[]{"userId", "password"},
                new Object[]{StringUtils.lowerCase(username), password}
        );
        if (users.size() > 0) {
            return users.get(0);
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    public User getUserByEmail(String email) {
        if (StringUtils.isNotBlank(email)) {
            List<User> users = (List<User>) getHibernateTemplate().findByNamedParam("select u from User as u where lower(u.email) = :email", "email", email.trim().toLowerCase());
            if (users.size() > 0) {
                return users.get(0);
            }
        }
        return null;
    }

    @SuppressWarnings(value={"unchecked"})
    public List<User> getUsers() {
        return (List<User>) getHibernateTemplate().find("select u from User as u where u.id > 0 order by u.lastName, u.firstName asc");
    }

    @SuppressWarnings(value={"unchecked"})
    public List<User> getActiveUsers() {
        return (List<User>) getHibernateTemplate().find("select u from User as u where u.id > 0 and u.active = true order by u.lastName, u.firstName asc");
    }

    public void saveUser(Object user) {
        getHibernateTemplate().saveOrUpdate(user);
    }

    public void deleteUser(User user) {
        getHibernateTemplate().delete(user);
    }

    @SuppressWarnings({"unchecked"})
    public List<User> searchUser(UserSearch userSearch) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(User.class);

        cri.add(Restrictions.eq("active", Boolean.TRUE));

        if (StringUtils.isNotBlank(userSearch.getFirstName())) {
            cri.add(Restrictions.ilike("firstName", userSearch.getFirstName(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(userSearch.getLastName())) {
            cri.add(Restrictions.ilike("lastName", userSearch.getLastName(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(userSearch.getEmail())) {
            cri.add(Restrictions.ilike("email", userSearch.getEmail(), MatchMode.ANYWHERE));
        }

        //Making sure the admin account doesn't come through
        cri.add(Restrictions.ne("id", 0));
        //Making sure the anonymous account doesn't come through
        cri.add(Restrictions.ne("id", -1));

        cri.addOrder(Order.asc("lastName"));
        cri.addOrder(Order.asc("firstName"));
        cri.addOrder(Order.asc("email"));
        return (List<User>) cri.list();
    }

    public void removePhone(Phone phone) {
        getHibernateTemplate().delete(phone);
    }

    public void removeAddress(Address address) {
        getHibernateTemplate().delete(address);
    }

    public boolean userInUse(User user) {

        // check: tickets, ticketaudit, tickethistory, tickettemplate, tickettemplatelaunch, asset,
        // assetfilter, ticketfilter

        // disable deleted tickets filter
        getSession().disableFilter(DELETED_TICKETS_FILTER_NAME);

        // ticket
        int size = DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Ticket as t where t.assignedTo.userRole.user.id = :userId", "userId", user.getId()));
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Ticket as t where t.contact.id = :userId", "userId", user.getId()));
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Ticket as t where t.submittedBy.id = :userId", "userId", user.getId()));

        // tickethistory
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketHistory as th where th.user.id = :userId", "userId", user.getId()));

        // ticketaudit
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketAudit as ta where ta.user.id = :userId", "userId", user.getId()));

        // tickettemplate
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketTemplate as t where t.assignedTo.userRole.user.id = :userId", "userId", user.getId()));
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketTemplate as t where t.contact.id = :userId", "userId", user.getId()));
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketTemplate as t where t.submittedBy.id = :userId", "userId", user.getId()));

        // tickettemplatelaunch
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketTemplateLaunch as t where t.createdBy.id = :userId", "userId", user.getId()));

        // asset
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from Asset as a where a.owner.id = :userId", "userId", user.getId()));

        // assetfilter
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from AssetFilter as a where a.user.id = :userId", "userId", user.getId()));

        // ticketfilter
        size += DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketFilter as t where t.user.id = :userId", "userId", user.getId()));

        return size > 0;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings({"unchecked"})
    public User getUserByFirstAndLastName(String firstName, String lastName) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(User.class);
        cri.add(Restrictions.like("firstName", "%" + firstName + "%"));
        cri.add(Restrictions.like("lastName", "%" + lastName + "%"));

        List<User> list = (List<User>) cri.list();
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    public List<User> getFilteredUsers(String sort, boolean ascending, String group, String firstName, String lastName, String role, Boolean status, String username, String email) {
        return getUsersByCriteria(sort, ascending, group, firstName, lastName, role, status, username, email);
    }

    public void clear() {
        getHibernateTemplate().clear();
    }

    @SuppressWarnings("unchecked")
    public List<User> getUsers(String queryParam, int start, int count, Map<String, Boolean> sortFieldsOrder, boolean includeInactive) {
        DetachedCriteria dc = getUserQueryCriteria(queryParam, includeInactive);

        for(String sortField : sortFieldsOrder.keySet()) {
            if(sortFieldsOrder.get(sortField)) {
                dc.addOrder(Order.asc(sortField));
            } else {
                dc.addOrder(Order.desc(sortField));
            }
        }

        return getHibernateTemplate().findByCriteria(dc, start, count);
    }

    public int getUserCount(String queryParam, boolean includeInactive) {
        DetachedCriteria dc = getUserQueryCriteria(queryParam, includeInactive);
        dc.setProjection(Projections.rowCount());
        return DataAccessUtils.intResult(getHibernateTemplate().findByCriteria(dc));
    }

    public List<User> getUsersWithAnyRole(List<Role> roles) {
        // get users that have a user role matching one of the given roles
        return getHibernateTemplate().findByNamedParam("select distinct ur.user from UserRole ur where ur.role in :roles and ur.active = true", "roles", roles);
    }

    private DetachedCriteria getUserQueryCriteria(String queryParam, boolean includeInactive) {
        DetachedCriteria dc = DetachedCriteria.forClass(User.class);
        dc.createAlias("location", "location", Criteria.LEFT_JOIN);

        if(!includeInactive) {
            dc.add(Restrictions.eq("active", Boolean.TRUE));
        }

        // exclude admin and anonymous accounts
        dc.add(Restrictions.gt("id", 0));

        String loginQueryRegex = "(.*,.*)(\\((.*)\\))";
        if (StringUtils.isBlank(queryParam)) {
            // no restrictions to add
        } else if (queryParam.matches(loginQueryRegex)) {
            // if login id is present, just use that to query
            Pattern p = Pattern.compile(loginQueryRegex);
            Matcher m = p.matcher(queryParam);
            m.find();
            String loginId = m.group(3);
            dc.add(Restrictions.eq("loginId", loginId).ignoreCase());
        } else {
            // split on "," for the case when the queryParam comes in as Lastname, Firstname
            // and change the hql appropriately
            String[] splitParam = queryParam.split(",");
            if (splitParam.length == 2){
                // match the lastname and the firstname
                dc.add(Restrictions.ilike("lastName", splitParam[0].trim(), MatchMode.START));
                dc.add(Restrictions.ilike("firstName", splitParam[1].trim(), MatchMode.START));
            } else {
                Disjunction d = Restrictions.disjunction();
                d.add(Restrictions.ilike("lastName", queryParam, MatchMode.START));
                d.add(Restrictions.ilike("firstName", queryParam, MatchMode.START));
                d.add(Restrictions.ilike("loginId", queryParam, MatchMode.START));
                d.add(Restrictions.ilike("email", queryParam, MatchMode.ANYWHERE));
                d.add(Restrictions.ilike("location.name", queryParam, MatchMode.START));
                dc.add(d);
            }
        }

        return dc;
    }

    @SuppressWarnings({"unchecked"})
    private List<User> getUsersByCriteria(String sort, boolean ascending, String group, String firstName, String lastName, String role, Boolean status, String username, String email) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(User.class);
        cri.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        boolean urAlias = true;
        if (StringUtils.isNotBlank(group)) {
            cri.createAlias("userRoles", "userRole").createAlias("userRole.userRoleGroup", "userRoleGroup").createAlias("userRoleGroup.group", "group");
            cri.add(Restrictions.ilike("group.name", "%" + group + "%"));
            cri.add(Restrictions.eq("userRole.active", Boolean.TRUE));
            urAlias = false;
        }
        if (StringUtils.isNotBlank(firstName)) {
            cri.add(Restrictions.ilike("firstName", "%" + firstName + "%"));
        }
        if (StringUtils.isNotBlank(lastName)) {
            cri.add(Restrictions.ilike("lastName", "%" + lastName + "%"));
        }
        if (StringUtils.isNotBlank(email)) {
            cri.add(Restrictions.ilike("email", "%" + email + "%"));
        }
        if (StringUtils.isNotBlank(role)) {
            if (urAlias) {
                cri.createAlias("userRoles", "userRole");
            }
            cri.createAlias("userRole.role", "role");
            cri.add(Restrictions.ilike("role.name", "%" + role + "%"));
        }
        if (status != null) {
            cri.add(Restrictions.eq("active", status));
        }
        if (StringUtils.isNotBlank(username)) {
            cri.add(Restrictions.ilike("loginId", "%" + username + "%"));
        }

        if (StringUtils.isNotBlank(sort)) {
            if (ascending) {
                cri.addOrder(Order.asc(sort));
                if (!sort.equals("lastName")) {
                    cri.addOrder(Order.asc("lastName"));
                }
                if (!sort.equals("firstName")) {
                    cri.addOrder(Order.asc("firstName"));
                }
            } else {
                cri.addOrder(Order.desc(sort));
                if (!sort.equals("lastName")) {
                    cri.addOrder(Order.asc("lastName"));
                }
                if (!sort.equals("firstName")) {
                    cri.addOrder(Order.asc("firstName"));
                }
            }
        } else {
            cri.addOrder(Order.asc("lastName"));
            cri.addOrder(Order.asc("firstName"));
        }

        cri.add(Restrictions.gt("id", 0));
        return (List<User>) cri.list();
    }
}
