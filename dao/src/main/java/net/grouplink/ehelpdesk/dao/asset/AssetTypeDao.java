package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetType;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 17, 2008
 * Time: 1:36:57 PM
 */
public interface AssetTypeDao {
    AssetType getById(Integer id);
    AssetType getByName(String name);
    List<AssetType> getAllAssetTypes();
    void saveAssetType(AssetType assetType);
    void deleteAssetType(AssetType assetType);

    void flush();
}
