package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.WidgetDao;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class HibernateWidgetDao extends HibernateDaoSupport implements WidgetDao {

    @SuppressWarnings("unchecked")
    public List<Widget> getAll() {
        return getHibernateTemplate().find("from Widget w");
    }

    public Widget getById(Integer id) {
        return (Widget) getHibernateTemplate().get(Widget.class, id);
    }

    public void save(Widget widget) {
        getHibernateTemplate().saveOrUpdate(widget);
    }

    public void delete(Widget widget) {
        getHibernateTemplate().delete(widget);
    }

    @SuppressWarnings("unchecked")
    public List<Widget> getByTicketFilter(TicketFilter filter) {
        return getHibernateTemplate().findByNamedParam("from Widget w where w.ticketFilter = :tf", "tf", filter);
    }
}
