package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.SurveyQuestionResponseDao;
import net.grouplink.ehelpdesk.domain.SurveyQuestionResponse;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * User: pericope
 * Date: Mar 18, 2008
 * Time: 3:38:13 PM
 */
public class HibernateSurveyQuestionResponseDao extends HibernateDaoSupport implements SurveyQuestionResponseDao {

    @SuppressWarnings("unchecked")
    public SurveyQuestionResponse getSurveyQuestionResponseById(Integer id) {
        List<SurveyQuestionResponse> responses = (List<SurveyQuestionResponse>) getHibernateTemplate().findByNamedParam("select r from SurveyQuestionResponse as r where r.id=:ID", "ID", id);
        if (responses.size() == 1) {
            return responses.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<SurveyQuestionResponse> getSurveyQuestionResponsesByQuestionId(Integer id) {
        return (List<SurveyQuestionResponse>) getHibernateTemplate().findByNamedParam("select r from SurveyQuestionResponse as r where r.surveyQuestion.id=:ID", "ID", id);
    }

    public void saveSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse) {
        getHibernateTemplate().saveOrUpdate(surveyQuestionResponse);
    }

    public void deleteSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse) {
        getHibernateTemplate().delete(surveyQuestionResponse);
    }

    @SuppressWarnings("unchecked")
    public boolean hasDuplicateSurveyQuestionResponseText(SurveyQuestionResponse surveyQuestionResponse) {
        Integer id;
        id = Integer.valueOf("-1");
        if (surveyQuestionResponse.getId() != null) {
            id = surveyQuestionResponse.getId();
        }
        List<SurveyQuestionResponse> list = (List<SurveyQuestionResponse>) getHibernateTemplate().findByNamedParam("select s from SurveyQuestionResponse as s where s.id != :surveyQuestionResponseId and s.surveyQuestionResponseText =:surveyQuestionResponseText", new String[]{"surveyQuestionResponseId", "surveyQuestionResponseText"}, new Object[]{id, surveyQuestionResponse.getSurveyQuestionResponseText()});
        return list.size() > 0;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
