package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.CustomFieldsDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 4:07:05 AM
 */
public class HibernateCustomFieldsDao extends HibernateDaoSupport implements CustomFieldsDao {

    /**
     * Saves a new custom field object to or updates an already existing one in the database.
     * @param customField the custom field to save
     */
    public void saveCustomField(CustomField customField) {
        getHibernateTemplate().saveOrUpdate(customField);
    }

    /**
     * Gets the custom field object for a give id.
     * @param id the id of the custom field to get
     * @return custom field pojo
     */
    public CustomField getCustomFieldById(Integer id) {
        List customFields = getHibernateTemplate().findByNamedParam("select cf from CustomField as cf where cf.id=:ID", "ID", id);
        if (customFields.size() == 1) {
            return (CustomField) customFields.get(0);
        }
        return null;
    }

    /**
     * Deletes a customField from the database.
     * @param customField the custom field to delete
     */
    public void deleteCustomField(CustomField customField) {
        getHibernateTemplate().delete(customField);
    }

    public CustomField getCustomFieldByOldIdAndCatOptId(Integer oldId, Integer catOptId) {
        List customFields = getHibernateTemplate().findByNamedParam("select cf from CustomField as cf where cf.oldCustonFieldId = :oldId and cf.categoryOption.id = :catOptId", new String[]{"oldId", "catOptId"}, new Object[]{oldId, catOptId});
        if (customFields.size() > 0) {
            return (CustomField) customFields.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CustomField> getAll() {
        return getHibernateTemplate().loadAll(CustomField.class);
    }

    /**
     * @return a List of custom fields with a unique name
     */
    @SuppressWarnings("unchecked")
    public List<String> getByUniqueName() {
        return getHibernateTemplate().find("select distinct cf.name from CustomField as cf where cf.active = true order by cf.name asc");
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from CustomField as cf where cf.categoryOption = :categoryOption", "categoryOption", categoryOption));
    }

    @SuppressWarnings("unchecked")
    public List<CustomField> getGlobalCustomFields() {
        return getHibernateTemplate().find("from CustomField as cf where cf.group is null and cf.location is null and cf.category is null and cf.categoryOption is null");
    }

    @SuppressWarnings("unchecked")
    public List<CustomField> getByLocation() {
        return getHibernateTemplate().find("from CustomField as cf where cf.location is not null and cf.group is null and cf.category is null and cf.categoryOption is null");
    }

    @SuppressWarnings("unchecked")
    public List<CustomField> getWithCustomFieldType() {
        return getHibernateTemplate().find("from CustomField as cf where cf.customFieldType is not null");
    }

    public int getWithCustomFieldTypeCount() {
        return DataAccessUtils.intResult(getHibernateTemplate().find("select count(*) from CustomField as cf where cf.customFieldType is not null"));
    }

    @SuppressWarnings("unchecked")
    public List<CustomField> getCustomFields(Location location, Group group, Category category, CategoryOption categoryOption) {
        DetachedCriteria criteria = DetachedCriteria.forClass(CustomField.class);

        Disjunction disj = Restrictions.disjunction();

        // get custom fields that match any of the given values
        // if categoryoption != null, get customfields where catopt = catopt
        // if category != null, get customfields where cat = cat and catopt is null
        // if group != null, get customfields where group = group and cat is null and catopt is null
        // if loc != null, get customfields where location = location, group is null and cat is null and catopt is null

        if (categoryOption != null) {
            disj.add(Restrictions.eq("categoryOption", categoryOption));
        }

        if (category != null) {
            Conjunction conj = Restrictions.conjunction();
            conj.add(Restrictions.eq("category", category));
            conj.add(Restrictions.isNull("categoryOption"));
            disj.add(conj);
        }

        if (group != null) {
            Conjunction conj = Restrictions.conjunction();
            conj.add(Restrictions.eq("group", group));
            conj.add(Restrictions.isNull("category"));
            conj.add(Restrictions.isNull("categoryOption"));
            disj.add(conj);
        }

        if (location != null) {
            Conjunction conj = Restrictions.conjunction();
            conj.add(Restrictions.eq("location", location));
            conj.add(Restrictions.isNull("group"));
            conj.add(Restrictions.isNull("category"));
            conj.add(Restrictions.isNull("categoryOption"));
            disj.add(conj);
        }

        // check global custom fields
        Conjunction conj = Restrictions.conjunction();
        conj.add(Restrictions.isNull("location"));
        conj.add(Restrictions.isNull("group"));
        conj.add(Restrictions.isNull("category"));
        conj.add(Restrictions.isNull("categoryOption"));
        disj.add(conj);

        criteria.add(disj);

        criteria.add(Restrictions.eq("active", true));

        return getHibernateTemplate().findByCriteria(criteria);
    }

    @SuppressWarnings("unchecked")
    public List<CustomField> getCustomFieldsByGroup(Group group) {
        DetachedCriteria criteria = DetachedCriteria.forClass(CustomField.class);
        Disjunction disjunction = Restrictions.disjunction();

        // get custom fields where cf.group = group
        // or cf.category.group = group
        // or cf.category.categoryOption.group = group

        criteria.createAlias("category", "category", Criteria.LEFT_JOIN);
        criteria.createAlias("categoryOption", "categoryOption", Criteria.LEFT_JOIN);
        criteria.createAlias("categoryOption.category", "catOptCategory", Criteria.LEFT_JOIN);

        disjunction.add(Restrictions.eq("group", group));
        disjunction.add(Restrictions.eq("category.group", group));
        disjunction.add(Restrictions.eq("catOptCategory.group", group));

        criteria.add(disjunction);
        return getHibernateTemplate().findByCriteria(criteria);
    }
}
