package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Status;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public interface StatusDao {
    public List<Status> getStatus();
    public Status getStatusById(Integer id);
    public Status getStatusByName(String name);
    public void saveStatus(Status status);
    public void deleteStatus(Status status);
    public Status getInitialStatus();
    public Status getTicketPoolStatus();
    public Status getClosedStatus();
    List<Status> getAllStatuses();
    public List<Status> getCustomStatus();
    public List<Status> getNonClosedStatus();

    public void flush();

    void saveOnly(Status status);
}
