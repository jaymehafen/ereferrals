package net.grouplink.ehelpdesk.dao.ldap;

import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.lang.StringUtils;
import net.grouplink.ehelpdesk.dao.LdapFieldDao;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.LdapField;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.HashMap;

/**
 * This is the Context Mapper to map ldap result to LdapUsers
 * Default contructor will return only dn, sn, givenName and mail from LDAP
 * Constructor with type 1 will return all attributes for an LdapUser
 * Constructor with type 2 will return LdapUser with attributes selected by helpdesk administrator through the ldapConfig page
 * When selecting type 2 LdapFieldDao will have to be set through the setLdapFieldDao
 */
class PersonContextMapper implements ContextMapper {

    private Log log = LogFactory.getLog(PersonContextMapper.class);
    private int searchType = 0;
    private LdapFieldDao ldapFieldDao;
    private String usernameAttribute;

    public PersonContextMapper() {
    }

    public PersonContextMapper(int searchType) {
        this.searchType = searchType;
    }

    @SuppressWarnings("unchecked")
    public Object mapFromContext(Object ctx) {
        log.debug("called PersonContextMapper.mapFromContext");

        DirContextAdapter context = (DirContextAdapter) ctx;
        LdapUser person = new LdapUser();

        String contextGivenName = context.getStringAttribute("givenName");
        log.debug("contextGivenName = " + contextGivenName);
        person.setGivenName(contextGivenName);
        String contextSn = context.getStringAttribute("sn");
        log.debug("contextSn = " + contextSn);
        person.setSn(contextSn);

        String userName;
        // if ldapconfig.usernameAttribute is set, only set username to that,
        // otherwise set to the first non-blank value starting with uid,
        // then samaccountname, then cn
        if (StringUtils.isNotBlank(usernameAttribute)) {
            userName = context.getStringAttribute(usernameAttribute);
        } else {

            String contextUid = context.getStringAttribute("uid");
            log.debug("contextUid = " + contextUid);

            userName = contextUid;
            if (StringUtils.isBlank(userName)) {
                String contextSamAccountName = context.getStringAttribute("sAMAccountName");
                log.debug("contextSamAccountName = " + contextSamAccountName);
                userName = contextSamAccountName;
            }

            if (StringUtils.isBlank(userName)) {
                String contextCn = context.getStringAttribute("cn");
                log.debug("contextCn = " + contextCn);
                userName = contextCn;
            }
        }

        userName = StringUtils.lowerCase(userName);
        log.debug("setting person cn to " + userName);
        person.setCn(userName);

        String contextMail = context.getStringAttribute("mail");
        contextMail = StringUtils.lowerCase(contextMail);
        log.debug("contextMail = " + contextMail);
        person.setMail(contextMail);
        String contextDn = context.getDn().toString();
        log.debug("contextDn = " + contextDn);
        person.setDn(contextDn);

        if (searchType == 2) {
            Map attributeMap = new LinkedHashMap();
            List fieldList = this.ldapFieldDao.getLdapFields();
            for (Object aFieldList : fieldList) {
                String id = ((LdapField) aFieldList).getName();
                attributeMap.put(id, context.getStringAttribute(id));
            }

            person.setAttributeMap(attributeMap);
        } else if (searchType == 1) {
            Map attributeMap = new HashMap();
            NamingEnumeration namingEnum = context.getAttributes().getAll();
            try {
                while (namingEnum.hasMore()) {
                    BasicAttribute item = (BasicAttribute) namingEnum.next();
                    NamingEnumeration itemEnum = item.getAll();
                    StringBuilder answer = new StringBuilder();
                    boolean start = true;
                    while (itemEnum.hasMore()) {
                        Object valueItem = itemEnum.next();
                        if (!start) {
                            answer.append("; ");
                        }

                        answer.append(valueItem.toString());
                        start = false;
                    }

                    attributeMap.put(item.getID(), answer.toString());
                }
            } catch (NamingException e) {
                log.error("error mapping person from context", e);
            }

            person.setAttributeMap(attributeMap);
        }

        return person;
    }

    public void setLdapFieldDao(LdapFieldDao ldapFieldDao) {
        this.ldapFieldDao = ldapFieldDao;
    }

    public void setUsernameAttribute(String usernameAttribute) {
        this.usernameAttribute = usernameAttribute;
    }
}

