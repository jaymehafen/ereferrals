package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.common.utils.AssetSearch;
import net.grouplink.ehelpdesk.dao.asset.AssetDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.asset.AccountingInfo;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.dialect.Dialect;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Date: Aug 27, 2007
 * Time: 3:41:02 PM
 */
public class HibernateAssetDao extends HibernateDaoSupport implements AssetDao {

    private Properties hibernateProperties;

    @SuppressWarnings("unchecked")
    public List<Asset> getAllAssets() {
        return (List<Asset>) getHibernateTemplate().find("select g from Asset as g order by g.name asc");
    }

    public void delete(Object asset) {
        getHibernateTemplate().delete(asset);
    }

    @SuppressWarnings("unchecked")
    public List<AssetType> getUniqueTypes() {
        return (List<AssetType>) getHibernateTemplate().find("select distinct a.assetType from Asset as a");
    }

    @SuppressWarnings("unchecked")
    public List<AssetStatus> getUniqueStatus() {
        return (List<AssetStatus>) getHibernateTemplate().find("select distinct a.status from Asset as a");
    }

    @SuppressWarnings("unchecked")
    public List<Vendor> getUniqueVendors() {
        return (List<Vendor>) getHibernateTemplate().find("select distinct a.vendor from Asset as a");
    }

    @SuppressWarnings("unchecked")
    public Asset getByAssetNumber(String assetNumber) {
        List<Asset> l = (List<Asset>) getHibernateTemplate().findByNamedParam("from Asset a where a.assetNumber = :ASSETNUMBER", "ASSETNUMBER", assetNumber);
        return (l.size() > 0) ? l.get(0) : null;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public List<Asset> getAssetsWithLeaseExpirationDate() {
        return (List<Asset>) getHibernateTemplate().find("from Asset a where a.accountingInfo.leaseExpirationDate is not null");
    }

    @SuppressWarnings("unchecked")
    public List<Asset> getAssetsWithWarrantyExpirationDate() {
        return (List<Asset>) getHibernateTemplate().find("from Asset a where a.accountingInfo.warrantyDate is not null");
    }

    @SuppressWarnings("unchecked")
    public List<Asset> getAssetsWithAcquisitionDate() {
        return (List<Asset>) getHibernateTemplate().find("from Asset a where a.acquisitionDate is not null");
    }

    public int getCountByGroup(Group group) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Asset as a where a.group = :group", "group", group));
    }

    public int getCountByCategory(Category category) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Asset as a where a.category = :category", "category", category));
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from Asset as a where a.categoryOption = :categoryOption", "categoryOption", categoryOption));
    }

    @SuppressWarnings("unchecked")
    public List<AccountingInfo> getAllAccountingInfo(int start, int count) {
        DetachedCriteria criteria = DetachedCriteria.forClass(AccountingInfo.class);
        return getHibernateTemplate().findByCriteria(criteria, start, count);
    }

    public void removeAssetAccountingInfoForeignKey() {
        int modified = getHibernateTemplate().executeWithNewSession(new HibernateCallback<Integer>() {
            public Integer doInHibernate(Session session) throws HibernateException, SQLException {
                Dialect dialect = Dialect.getDialect(hibernateProperties);
                String foreignKeyName = "FK3BAF2D05934BD91";
                String sql = "alter table ASSET" + dialect.getDropForeignKeyString() + foreignKeyName;
                logger.info("executing sql: " + sql);
                SQLQuery sqlQuery = session.createSQLQuery(sql);
                try {
                    return sqlQuery.executeUpdate();
                } catch (Exception e) {
                    logger.info("could not remove constraint: " + e.getMessage() + " : " + ((e.getCause() != null) ? e.getCause().getMessage() : ""));
                    return 0;
                }
            }
        });

        logger.info("modified = " + modified);
    }

    public int getAccountingInfoCount() {
        DetachedCriteria dc = DetachedCriteria.forClass(AccountingInfo.class);
        dc.setProjection(Projections.rowCount());
        return DataAccessUtils.intResult(getHibernateTemplate().findByCriteria(dc));
    }

    public void deleteAccountingInfoList(List<AccountingInfo> accountingInfos) {
        getHibernateTemplate().deleteAll(accountingInfos);
    }

    @SuppressWarnings("unchecked")
    public List<Asset> searchAssets(AssetSearch assetSearch) {
        Criteria cri = getSessionFactory().getCurrentSession().createCriteria(Asset.class);
        if (StringUtils.isNotBlank(assetSearch.getAssetNumber())) {
            cri.add(Restrictions.ilike("assetNumber", assetSearch.getAssetNumber(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(assetSearch.getAssetName())) {
            cri.add(Restrictions.ilike("name", assetSearch.getAssetName(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(assetSearch.getAssetLocation())) {
            cri.add(Restrictions.ilike("assetLocation", assetSearch.getAssetLocation(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(assetSearch.getLocation())) {
            cri.createAlias("location", "location");
            cri.add(Restrictions.ilike("location.name", assetSearch.getLocation(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(assetSearch.getGroup())) {
            cri.createAlias("group", "group");
            cri.add(Restrictions.ilike("group.name", assetSearch.getGroup(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(assetSearch.getCategory())) {
            cri.createAlias("category", "category");
            cri.add(Restrictions.ilike("category.name", assetSearch.getCategory(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(assetSearch.getCategoryOption())) {
            cri.createAlias("categoryOption", "categoryOption");
            cri.add(Restrictions.ilike("categoryOption.name", assetSearch.getCategoryOption(), MatchMode.ANYWHERE));
        }

        if (StringUtils.isNotBlank(assetSearch.getOwner())) {
            cri.createAlias("owner", "owner");

            Disjunction disj = Restrictions.disjunction();
            disj.add(Restrictions.ilike("owner.loginId", assetSearch.getOwner(), MatchMode.ANYWHERE));
            disj.add(Restrictions.ilike("owner.email", assetSearch.getOwner(), MatchMode.ANYWHERE));
            disj.add(Restrictions.ilike("owner.firstName", assetSearch.getOwner(), MatchMode.ANYWHERE));
            disj.add(Restrictions.ilike("owner.lastName", assetSearch.getOwner(), MatchMode.ANYWHERE));

            cri.add(disj);
        }

        cri.addOrder(Order.asc("assetNumber"));
        cri.addOrder(Order.asc("name"));
        return (List<Asset>) cri.list();
    }

    public int getAssetCount(String searchText) {
        DetachedCriteria cri = getAssetQueryCriteria(searchText);
        cri.setProjection(Projections.rowCount());
        return DataAccessUtils.intResult(getHibernateTemplate().findByCriteria(cri));
    }

    @SuppressWarnings("unchecked")
    public List<Asset> searchAssets(String searchText, int start, int count, Map<String, Boolean> sortFieldMap) {
        DetachedCriteria cri = getAssetQueryCriteria(searchText);

        for(String sortField : sortFieldMap.keySet()) {
            if(sortFieldMap.get(sortField)) {
                cri.addOrder(Order.asc(sortField));
            } else {
                cri.addOrder(Order.desc(sortField));
            }
        }

        return getHibernateTemplate().findByCriteria(cri, start, count);
    }

    private DetachedCriteria getAssetQueryCriteria(String searchText) {
        DetachedCriteria cri = DetachedCriteria.forClass(Asset.class)
                .createAlias("location", "loc", CriteriaSpecification.LEFT_JOIN)
                .createAlias("group", "gr", CriteriaSpecification.LEFT_JOIN)
                .createAlias("category", "cat", CriteriaSpecification.LEFT_JOIN)
                .createAlias("categoryOption", "catopt", CriteriaSpecification.LEFT_JOIN)
                .createAlias("owner", "usr", CriteriaSpecification.LEFT_JOIN);

        Disjunction dis = Restrictions.disjunction();

        if(StringUtils.isNotBlank(searchText)) {
            dis.add(Restrictions.ilike("assetNumber", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("name", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("assetLocation", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("loc.name", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("gr.name", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("cat.name", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("catopt.name", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("usr.loginId", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("usr.firstName", searchText, MatchMode.ANYWHERE));
            dis.add(Restrictions.ilike("usr.lastName", searchText, MatchMode.ANYWHERE));
        }

        cri.add(dis);

        return cri;
    }

    @SuppressWarnings("unchecked")
    public Asset getAssetByAssetNumber(String assetNumber) {
        List<Asset> list = (List<Asset>) getHibernateTemplate().findByNamedParam("select g from Asset as g where g.assetNumber = :assetNumber", "assetNumber", assetNumber);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public Asset getAssetById(Integer id) {
        List<Asset> list = (List<Asset>) getHibernateTemplate().findByNamedParam("select g from Asset as g where g.id = :id", "id", id);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public void saveOrUpdate(Object o) {
        getHibernateTemplate().saveOrUpdate(o);
    }

    public void setHibernateProperties(Properties hibernateProperties) {
        this.hibernateProperties = hibernateProperties;
    }

}
