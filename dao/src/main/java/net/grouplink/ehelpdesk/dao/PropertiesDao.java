package net.grouplink.ehelpdesk.dao;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Properties;

public interface PropertiesDao {

	public Properties getPropertiesByName(String name);
	public Properties getPropertiesByValue(String value);
    public void saveProperties(Properties properties);
	public List<Properties> getPropertiesListByName(String name);
	public void deleteProperties(Properties properties);
	public void deletePropertiesList(List propertiesList);

    void flush();
}
