package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.SurveyFrequencyDao;
import net.grouplink.ehelpdesk.domain.SurveyFrequency;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 8:28:39 AM
 */
public class HibernateSurveyFrequencyDao extends HibernateDaoSupport implements SurveyFrequencyDao {

    @SuppressWarnings("unchecked")
    public List<SurveyFrequency> getSurveyFrequencies() {
        return (List<SurveyFrequency>) getHibernateTemplate().find("select s from SurveyFrequency as s");
    }

    @SuppressWarnings("unchecked")
    public SurveyFrequency getDefaultSurveyFrequency() {
        List<SurveyFrequency> surveyFrequencies = (List<SurveyFrequency>) getHibernateTemplate().findByNamedParam("select surveyFrequency from SurveyFrequency as surveyFrequency where" +
                " surveyFrequency.id=:ID", "ID", new Integer(1));
        if (surveyFrequencies.size() == 1) {
            return surveyFrequencies.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public SurveyFrequency getSurveyFrequencyById(Integer id) {
        List<SurveyFrequency> surveyFrequencies = (List<SurveyFrequency>) getHibernateTemplate().findByNamedParam("select surveyFrequency from SurveyFrequency as surveyFrequency where" +
                " surveyFrequency.id=:ID", "ID", id);
        if (surveyFrequencies.size() == 1) {
            return surveyFrequencies.get(0);
        }
        return null;
    }

    public void save(SurveyFrequency surveyFrequency){
        getHibernateTemplate().saveOrUpdate(surveyFrequency);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
