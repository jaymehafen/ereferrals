package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;

import java.util.List;

public interface RecurrenceScheduleDao {

    RecurrenceSchedule getById(Integer id);

    void save(RecurrenceSchedule recurrenceSchedule);

    List<RecurrenceSchedule> getAllRunning();

    List<RecurrenceSchedule> getRunningTicketTemplateSchedules();

    List<RecurrenceSchedule> getRunningScheduleStatusSchedules();

    List<RecurrenceSchedule> getRunningReportSchedules();

    void delete(RecurrenceSchedule recurrenceSchedule);
}
