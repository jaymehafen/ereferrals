package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.GroupDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.common.utils.GroupSearch;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

public class HibernateGroupDao extends HibernateDaoSupport implements GroupDao {

    @SuppressWarnings("unchecked")
    public List<Group> getGroups() {
        return getHibernateTemplate().find("select g from Group as g where g.active = true order by g.name asc");
    }

    public void deleteGroup(Object group) {
        getHibernateTemplate().delete(group);
    }

    public Group getGroupById(Integer id) {
        getSession().disableFilter("deletedObject");
        List list = getHibernateTemplate().findByNamedParam("select g from Group as g where g.id = :id", "id", id);
        if (list.size() > 0) {
            return (Group) list.get(0);
        }
        getSession().enableFilter("deletedObject");
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Group> getGroupsByTechId(Integer userId) {
        return getHibernateTemplate().findByNamedParam("select distinct urg.group from UserRoleGroup as urg where urg.userRole.user.id = :userId", "userId", userId);
    }

    @SuppressWarnings("unchecked")
    public List<Group> getGroupsByManagerId(Integer userId) {
        return getHibernateTemplate().findByNamedParam("select urg.group from UserRoleGroup as urg where urg.userRole.user.id = :userId and urg.userRole.role.id = :roleId",
                new String[]{"userId", "roleId"}, new Object[]{userId, Role.ROLE_MANAGER_ID});
    }

    public void saveGroup(Group group) {
        getHibernateTemplate().saveOrUpdate(group);
    }

    @SuppressWarnings("unchecked")
    public List<Group> searchGroup(GroupSearch groupSearch) {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(Group.class);
        if (!StringUtils.isBlank(groupSearch.getName())) {
            cri.add(Restrictions.like("name", groupSearch.getName().replace('*', '%') + "%"));
        }

        //Making sure the deleted groups doesn't come through
        cri.add(Restrictions.eq("active", Boolean.TRUE));

        cri.addOrder(Order.asc("name"));
        return cri.list();
    }

    @SuppressWarnings("unchecked")
    public List<Group> getGroupsByAssetTrackerType(String assetTrackerType) {
        return getHibernateTemplate().findByNamedParam("from Group as g where g.assetTrackerType = :assetTrackerType", "assetTrackerType", assetTrackerType);
    }

    public int getGroupCount() {
        return DataAccessUtils.intResult(getHibernateTemplate().find("select count(g) from Group g where g.active = true"));
    }

    @SuppressWarnings(value = "unchecked")
    public List<Group> getAllGroups() {
        getSession().disableFilter("deletedObject");
        List<Group> groups =  getHibernateTemplate().find("from Group");
        getSession().enableFilter("deletedObject").setParameter("isActive", Boolean.TRUE);
        return groups;
    }

    public boolean hasDuplicateGroupName(Group group) {
        Integer id = Integer.valueOf("-1");
        if (group.getId() != null) {
            id = group.getId();
        }

        int count = DataAccessUtils.intResult(getHibernateTemplate()
                .findByNamedParam("select count(g) from Group as g where g.id != :groupId and g.name = :groupName and g.active = true",
                new String[]{"groupId", "groupName"}, new Object[]{id, group.getName()}));
        return count > 0;
    }

    public Group getGroupByName(String groupName) {
        List list = getHibernateTemplate().findByNamedParam("select g from Group as g where g.name = :groupName", "groupName", groupName);
        if (list.size() > 0) {
            return (Group) list.get(0);
        }
        return null;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
