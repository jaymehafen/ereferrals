package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketTemplateDao;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

public class HibernateTicketTemplateDao extends HibernateDaoSupport implements TicketTemplateDao {
    public TicketTemplate getById(Integer id) {
        return (TicketTemplate) getHibernateTemplate().get(TicketTemplate.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<TicketTemplate> getByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        return getHibernateTemplate().findByNamedParam("from TicketTemplate t where t.master = :master", "master", ticketTemplateMaster);
    }

    @SuppressWarnings("unchecked")
    public int getTicketTemplateCountByPriorityId(Integer priorityId) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam("select count(*) from TicketTemplate tt where tt.priority.id = :priorityId and tt.status.id != -1", "priorityId", priorityId));
    }

    @SuppressWarnings("unchecked")
    public List<TicketTemplate> getTopLevelByTicketTemplateMaster(TicketTemplateMaster ticketTemplateMaster) {
        return getHibernateTemplate().findByNamedParam("from TicketTemplate t where t.master = :master and t.parent is null", "master", ticketTemplateMaster);
    }

    public void save(TicketTemplate ticketTemplate) {
        getHibernateTemplate().saveOrUpdate(ticketTemplate);
    }

    public void delete(TicketTemplate ticketTemplate) {
        getHibernateTemplate().delete(ticketTemplate);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public int getCountByGroup(Group group) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from TicketTemplate as tt where tt.group = :group", "group", group));
    }

    public int getCountByCategory(Category category) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from TicketTemplate as tt where tt.category = :category", "category", category));
    }

    public int getCountByCategoryOption(CategoryOption categoryOption) {
        return DataAccessUtils.intResult(getHibernateTemplate().findByNamedParam(
                "select count(*) from TicketTemplate as tt where tt.categoryOption = :categoryOption", "categoryOption", categoryOption));

    }
}
