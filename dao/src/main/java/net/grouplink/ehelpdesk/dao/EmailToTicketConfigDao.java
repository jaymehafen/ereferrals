package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Jan 4, 2008
 * Time: 10:47:09 AM
 */
public interface EmailToTicketConfigDao {
    List<EmailToTicketConfig> getAll();
    List<EmailToTicketConfig> getAllEnabled();
    int getEmailToTicketConfigCountByPriorityId(Integer priorityId);
    EmailToTicketConfig getById(Integer id);
    void save(EmailToTicketConfig emailToTicketConfig);
    void delete(EmailToTicketConfig emailToTicketConfig);
    EmailToTicketConfig getByGroup(Group group);

    int getCountByGroup(Group group);
}

