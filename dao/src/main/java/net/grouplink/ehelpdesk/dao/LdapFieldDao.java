package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.LdapField;

import java.util.List;

/**
 * @author dgarcia
 * @version 1.0
 */
public interface LdapFieldDao {
    public List<LdapField> getLdapFields();
    public LdapField getLdapFieldByAttributeID(String fieldName);
    public LdapField getLdapFieldById(Integer fieldId);
    public void saveLdapField(LdapField ldapField);
    public void saveLdapFields(List ldapFieldList);
    public void deleteLdapField(LdapField ldapField);
    public void deleteLdapFieldList(List list);
}
