package net.grouplink.ehelpdesk.dao.hibernate;

import java.util.List;

import net.grouplink.ehelpdesk.dao.PropertiesDao;
import net.grouplink.ehelpdesk.domain.Properties;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class HibernatePropertiesDao extends HibernateDaoSupport implements
		PropertiesDao {

	public Properties getPropertiesByName(String name) {
		List list = getHibernateTemplate().findByNamedParam("select p from Properties as p where p.name=:name", "name", name);
		if(list.size() > 0){
			return (Properties) list.get(0);
		}
		return null;
	}

    public Properties getPropertiesByValue(String value) {
        List list = getHibernateTemplate().findByNamedParam("select p from Properties as p where p.value=:value", "value", value);
		if(list.size() > 0){
			return (Properties) list.get(0);
		}
		return null;
    }

    @SuppressWarnings("unchecked")
    public List<Properties> getPropertiesListByName(String name) {
		return getHibernateTemplate().findByNamedParam("select p from Properties as p where p.name=:name", "name", name);
	}

	public void saveProperties(Properties properties) {
		getHibernateTemplate().saveOrUpdate(properties);
	}

	public void deleteProperties(Properties properties) {
		getHibernateTemplate().delete(properties);		
	}

    public void deletePropertiesList(List propertiesList) {
        getHibernateTemplate().deleteAll(propertiesList);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

}
