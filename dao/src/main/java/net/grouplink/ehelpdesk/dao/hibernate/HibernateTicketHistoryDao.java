package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketHistoryDao;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * @author mrollins
 * @version 1.0
 */
public class HibernateTicketHistoryDao extends HibernateDaoSupport implements TicketHistoryDao {

    public void saveTicketHistoryEntry(TicketHistory ticketHistory) {
        getHibernateTemplate().saveOrUpdate(ticketHistory);
    }

    @SuppressWarnings("unchecked")
    public TicketHistory getTicketHistoryById(Integer id) {
        List<TicketHistory> ticketHists = (List<TicketHistory>) getHibernateTemplate().findByNamedParam(
                "select th from TicketHistory as th where th.id=:THISTID", "THISTID", id);
        if (ticketHists.size() == 1) {
            return ticketHists.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<TicketHistory> getTicketHistoriesByTicket(Ticket ticket) {
        return (List<TicketHistory>) getHibernateTemplate().findByNamedParam(
                "select th from TicketHistory as th where th.ticket=:TICKETID",
                "TICKETID", ticket);
    }

    @SuppressWarnings("unchecked")
    public List<TicketHistory> getTicketHistoriesByTicket(Ticket ticket, boolean sortDesc) {
        StringBuilder qry = new StringBuilder("select th from TicketHistory as th where th.ticket=:TICKETID");
        if (sortDesc) {
            qry.append(" order by th.createdDate desc");
        } else {
            qry.append(" order by th.createdDate asc");
        }
        return (List<TicketHistory>) getHibernateTemplate().findByNamedParam(qry.toString(), "TICKETID", ticket);
    }

    @SuppressWarnings("unchecked")
    public List<TicketHistory> getEventTicketHistoriesByUser(User user) {
        return (List<TicketHistory>) getHibernateTemplate().findByNamedParam(
                "select th from TicketHistory as th where th.user=:USER and th.active = true",
                "USER", user);
    }

    public ScrollableResults getAllTicketHistoryByGroupId(Integer groupId) {
        Criteria cri = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(TicketHistory.class);
        cri.createAlias("ticket", "ticket").createAlias("ticket.group", "group");
        cri.add(Restrictions.eq("group.id", groupId));
//        cri.addOrder(Order.asc("id"));
        cri.setCacheMode(CacheMode.IGNORE);
        return cri.scroll(ScrollMode.FORWARD_ONLY);
//        return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("select t from TicketHistory as t where t.ticket.group.id = :groupId").setInteger("groupId", groupId.intValue()).setCacheMode(CacheMode.IGNORE).scroll(ScrollMode.FORWARD_ONLY);
    }


    public void flush() {
        getHibernateTemplate().flush();
    }

    public void clear() {
        getHibernateTemplate().clear();
    }

    @SuppressWarnings("unchecked")
    public int getHistoryCount(Integer groupId) {
        List<Long> list = (List<Long>) getHibernateTemplate().findByNamedParam("select count(*) from TicketHistory as th where th.ticket.group.id=:ID", "ID", groupId);
        return (list.get(0)).intValue();
    }
}
