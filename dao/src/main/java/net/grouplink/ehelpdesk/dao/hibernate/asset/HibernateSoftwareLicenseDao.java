package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.SoftwareLicenseDao;
import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 8, 2008
 * Time: 4:35:35 PM
 */
public class HibernateSoftwareLicenseDao extends HibernateDaoSupport implements SoftwareLicenseDao {
    public SoftwareLicense getById(Integer id) {
        return (SoftwareLicense) getHibernateTemplate().get(SoftwareLicense.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<SoftwareLicense> getAllSoftwareLicenses() {
        return (List<SoftwareLicense>) getHibernateTemplate().find("from SoftwareLicense s");
    }

    @SuppressWarnings("unchecked")
    public List<Vendor> getUniqueVendors() {
        return (List<Vendor>) getHibernateTemplate().find("select distinct s.vendor from SoftwareLicense as s");
    }

    public void saveSoftwareLicense(SoftwareLicense softwareLicense) {
        getHibernateTemplate().saveOrUpdate(softwareLicense);
    }

    public void delete(SoftwareLicense softwareLicense) {
        getHibernateTemplate().delete(softwareLicense);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
