package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.SurveyDataDao;
import net.grouplink.ehelpdesk.domain.SurveyData;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

import java.util.List;

/**
 * Date: Feb 27, 2008
 * Time: 8:28:14 AM
 */
public class HibernateSurveyDataDao extends HibernateDaoSupport implements SurveyDataDao {

    public void saveSurveyData(SurveyData surveyData) {
        getHibernateTemplate().save(surveyData);
    }

    @SuppressWarnings("unchecked")
    public List<SurveyData> getByTicketId(Integer ticketId) {
        return (List<SurveyData>) getHibernateTemplate()
                .findByNamedParam("from SurveyData as sd where sd.ticket.id = :TICKETID", "TICKETID",
                        ticketId);
    }

    @SuppressWarnings("unchecked")
    public int getCountByTicketId(Integer ticketId) {
        return DataAccessUtils.intResult(getHibernateTemplate()
                .findByNamedParam("select count(sd) from SurveyData as sd where sd.ticket.id = :TICKETID", "TICKETID",
                ticketId));
    }
}
