package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Attachment;

public interface AttachmentDao {

    Attachment getById(Integer id);

    void delete(Attachment attachment);
}
