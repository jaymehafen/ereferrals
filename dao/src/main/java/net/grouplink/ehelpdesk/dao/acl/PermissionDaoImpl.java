package net.grouplink.ehelpdesk.dao.acl;

import net.grouplink.ehelpdesk.domain.acl.GlobalPermissionEntry;
import net.grouplink.ehelpdesk.domain.acl.Permission;
import net.grouplink.ehelpdesk.domain.acl.PermissionScheme;
import net.grouplink.ehelpdesk.domain.acl.PermissionSchemeEntry;
import net.grouplink.ehelpdesk.domain.acl.PermissionSubject;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class PermissionDaoImpl extends HibernateDaoSupport implements PermissionDao {

    @SuppressWarnings("unchecked")
    public GlobalPermissionEntry getGlobalPermissionEntry(Permission permission) {
        HibernateTemplate hibernateTemplate = createHibernateTemplate(getSessionFactory());
        hibernateTemplate.setCacheQueries(true);
        return (GlobalPermissionEntry) DataAccessUtils.singleResult(hibernateTemplate.findByNamedParam("from GlobalPermissionEntry gp where gp.permission = :permission", "permission", permission));
    }

    @SuppressWarnings("unchecked")
    public Permission getPermissionByKey(String key) {
        HibernateTemplate hibernateTemplate = createHibernateTemplate(getSessionFactory());
        hibernateTemplate.setCacheQueries(true);
        return (Permission) DataAccessUtils.singleResult(hibernateTemplate.findByNamedParam("from Permission p where p.key = :key", "key", key));
    }

    @SuppressWarnings("unchecked")
    public List<Permission> getGlobalPermissions() {
        HibernateTemplate hibernateTemplate = createHibernateTemplate(getSessionFactory());
        hibernateTemplate.setCacheQueries(true);
        return hibernateTemplate.findByNamedParam("from Permission p where p.permissionType = :type", "type", Permission.PERMISSIONTYPE_GLOBAL);
    }

    @SuppressWarnings("unchecked")
    public List<Permission> getSchemePermissions() {
        HibernateTemplate hibernateTemplate = createHibernateTemplate(getSessionFactory());
        hibernateTemplate.setCacheQueries(true);
        return hibernateTemplate.findByNamedParam("from Permission p where p.permissionType = :type", "type", Permission.PERMISSIONTYPE_SCHEME);
    }

    public List<Permission> getFieldPermissionsByModelName(String modelName) {
        HibernateTemplate hibernateTemplate = createHibernateTemplate(getSessionFactory());
        hibernateTemplate.setCacheQueries(true);
        return hibernateTemplate.findByNamedParam("from Permission p where p.permissionSubject.modelName = :modelName and p.permissionSubject.modelField is not null", "modelName", modelName);
    }

    public void savePermission(Permission permission) {
        getHibernateTemplate().saveOrUpdate(permission);
    }

    public void deletePermission(Permission permission) {
        getHibernateTemplate().delete(permission);
    }

    @SuppressWarnings("unchecked")
    public List<PermissionScheme> getPermissionSchemes() {
        return getHibernateTemplate().find("from PermissionScheme ps");
    }

    public void savePermissionScheme(PermissionScheme permissionScheme) {
        getHibernateTemplate().saveOrUpdate(permissionScheme);
    }

    public void deletePermissionScheme(PermissionScheme permissionScheme) {
        getHibernateTemplate().delete(permissionScheme);
    }

    public void saveGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry) {
        getHibernateTemplate().saveOrUpdate(globalPermissionEntry);
    }

    public void deleteGlobalPermissionEntry(GlobalPermissionEntry globalPermissionEntry) {
        getHibernateTemplate().delete(globalPermissionEntry);
    }

    public void savePermissionSchemeEntry(PermissionSchemeEntry permissionSchemeEntry) {
        getHibernateTemplate().saveOrUpdate(permissionSchemeEntry);
    }

    @SuppressWarnings("unchecked")
    public List<GlobalPermissionEntry> getGlobalPermissionEntries() {
        return getHibernateTemplate().find("from GlobalPermissionEntry gpe");
    }

    public Permission getPermissionById(Integer gpId) {
        return getHibernateTemplate().get(Permission.class, gpId);
    }

    public PermissionScheme getPermissionSchemeById(Integer id) {
        return getHibernateTemplate().get(PermissionScheme.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Permission> getGroupPermissions() {
        HibernateTemplate hibernateTemplate = createHibernateTemplate(getSessionFactory());
        hibernateTemplate.setCacheQueries(true);
        return hibernateTemplate.findByNamedParam("from Permission p where p.permissionType = :type", "type", Permission.PERMISSIONTYPE_SCHEME);
    }

    @SuppressWarnings("unchecked")
    public PermissionSchemeEntry getPermissionSchemeEntry(PermissionScheme permissionScheme, Permission permission) {
        return (PermissionSchemeEntry) DataAccessUtils.singleResult(getHibernateTemplate().findByNamedParam("from PermissionSchemeEntry p where p.permissionScheme = :permScheme and p.permission = :perm", new String[]{"permScheme", "perm"}, new Object[]{permissionScheme, permission}));
    }

    @SuppressWarnings("unchecked")
    public List<PermissionSubject> getPermissionSubjects() {
        return getHibernateTemplate().find("from PermissionSubject ps");
    }

    @SuppressWarnings("unchecked")
    public PermissionSubject getPermissionSubjectByModelClass(String modelClass) {
        return (PermissionSubject) DataAccessUtils.singleResult(getHibernateTemplate().findByNamedParam("from PermissionSubject p where p.modelClass = :modelClass and p.modelField is null", "modelClass", modelClass));
    }

    @SuppressWarnings("unchecked")
    public PermissionSubject getPermissionSubjectByModelClassAndField(String modelClass, String fieldName) {
        return (PermissionSubject) DataAccessUtils.singleResult(getHibernateTemplate().findByNamedParam("from PermissionSubject p where p.modelClass = :modelClass and p.modelField = :fieldName", new String[] {"modelClass", "fieldName"}, new Object[] {modelClass, fieldName}));
    }

    @SuppressWarnings("unchecked")
    public PermissionSubject getPermissionSubjectByModelNameAndField(String modelName, String fieldName) {
        return (PermissionSubject) DataAccessUtils.singleResult(getHibernateTemplate().findByNamedParam("from PermissionSubject p where p.modelName = :modelName and p.modelField = :fieldName", new String[] {"modelName", "fieldName"}, new Object[] {modelName, fieldName}));
    }

    public void savePermissionSubject(PermissionSubject permissionSubject) {
        getHibernateTemplate().saveOrUpdate(permissionSubject);
    }
}
