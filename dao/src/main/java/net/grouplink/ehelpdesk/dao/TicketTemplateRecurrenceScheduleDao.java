package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.TicketTemplateRecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;

public interface TicketTemplateRecurrenceScheduleDao {

    void save(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule);

    void delete(TicketTemplateRecurrenceSchedule ticketTemplateRecurrenceSchedule);

    TicketTemplateRecurrenceSchedule getByTicketTemplateMasterAndSchedule(TicketTemplateMaster ticketTemplateMaster, RecurrenceSchedule schedule);
}
