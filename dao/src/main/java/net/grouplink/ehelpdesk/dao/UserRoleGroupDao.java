package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;

import java.util.List;

public interface UserRoleGroupDao {

	List<UserRoleGroup> getUserRoleGroup();

	UserRoleGroup getUserRoleGroupById(Integer id);

	List<UserRoleGroup> getUserRoleGroupByGroupId(Integer id);

    List<UserRoleGroup> getManagerTechnicianUserRoleGroupByGroupId(Group group);

	List<UserRoleGroup> getUserRoleGroupSelectionByGroupId(Integer id);

	UserRoleGroup getUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId);

	UserRoleGroup getAnyUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId);

	List<UserRoleGroup> getUserRoleGroupByUserId(Integer id);

	List<UserRoleGroup> getUserRoleGroupByUserIdRoleId(Integer userId, Integer roleId);

	void deleteUserRoleGroup(UserRoleGroup userRoleGroup);

	void saveUserRoleGroup(UserRoleGroup userRoleGroup);

	List<User> getTechnicianByGroupId(Integer groupId);

	UserRole getUserRoleByUserIdRoleId(Integer userId, Integer roleId); // Exception out of place

	UserRoleGroup getTicketPoolByGroupId(Integer groupId);

    Integer getTechnicianCount();

    void flush();

    UserRoleGroup getUserRoleGroupByUserIdGroupIdRoleId(Integer userId, Integer groupId, Integer roleId);

    List<UserRoleGroup> getAssignableUserRoleGroups(String queryParam, Integer start, Integer count);

    int getAssignableUserRoleGroupCount(String queryParam);

    List<User> getTechnicianAsUsers(String queryParam);

    List<UserRoleGroup> getAllTicketPools(String queryParam, String translatedPoolName);
    
     /**
     * Returns all URGs regardless of the active flag
     * @param userId the id if the user URG to get
     * @return list of URG for the given user id
     */
    List<UserRoleGroup> getAllUserRoleGroupByUserId(Integer userId);

    void cleanupUserRoleGroup();

    List<UserRoleGroup> getAllUserRoleGroup();

    List<UserRoleGroup> getAssignableUserRoleGroupByGroupId(Integer groupId);

    List<UserRoleGroup> getAllAssignableUserRoleGroups();

    Integer getWFParticpantCount();
}
