package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.UserRoleDao;
import net.grouplink.ehelpdesk.dao.UserRoleGroupDao;
import net.grouplink.ehelpdesk.domain.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.criterion.*;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class HibernateUserRoleGroupDao extends HibernateDaoSupport implements UserRoleGroupDao {

    private UserRoleDao userRoleDao;
    private String getUserRoleGroupByUserIdAndGroupId_queryCache = "getUserRoleGroupByUserIdAndGroupId";

    public void deleteUserRoleGroup(UserRoleGroup userRoleGroup) {

        //we need to delete all assigments
        List<Assignment> techLocCatOpt = new ArrayList<Assignment>(userRoleGroup.getAssignments());
        userRoleGroup.getAssignments().clear();

        saveUserRoleGroup(userRoleGroup);
        getHibernateTemplate().deleteAll(techLocCatOpt);

        //Remove all notification assistant to/from user
        if (userRoleGroup.getUserRole() != null) {
            userRoleGroup.getUserRole().getUser().getNotificationSupport().clear();
        }
        userRoleGroup.getNotificationUsers().clear();

        //Remove backups to/from tech if any
        userRoleGroup.setBackUpTech(null);
        List<UserRoleGroup> backUpList = getUserRoleGroupbyBackUpId(userRoleGroup.getId());
        for (Object aBackUpList : backUpList) {
            UserRoleGroup roleGroup = (UserRoleGroup) aBackUpList;
            roleGroup.setBackUpTech(null);
        }

        //If it is not the ticket pool then inactivate groups and roles from users
        if (userRoleGroup.getUserRole() != null) {
            if (userRoleGroup.getTickets().size() > 0) {
                //Inactivating the userRoleGroup
                userRoleGroup.setActive(false);
//                getHibernateTemplate().saveOrUpdate(userRoleGroup);
//                getHibernateTemplate().flush();

                UserRole userRole = userRoleGroup.getUserRole();

                if (userRole.getUserRoleGroup().size() <= 1) {
                    // this will delete the userRole too
                    userRoleDao.inactivateUserRole(userRole);
                } else {
                    //Deleting technician from group
//                    userRole.getUserRoleGroup().remove(userRoleGroup);

                    UserRole ur = new UserRole();
                    ur.setUser(userRole.getUser());
                    ur.setActive(false);
                    userRoleGroup.setUserRole(ur);
                    getHibernateTemplate().saveOrUpdate(userRoleGroup);
                }
            } else {
                UserRole userRole = userRoleGroup.getUserRole();

                if (userRole.getUserRoleGroup().size() <= 1) {
                    // this will delete the userRole too
                    userRoleGroup.setUserRole(null);
                    saveUserRoleGroup(userRoleGroup);
                    userRole.getRole().getUserRoles().remove(userRole);
                    userRole.getUser().getUserRoles().remove(userRole);
                    userRoleDao.deleteUserRole(userRole);
                } else {
                    userRole.getUserRoleGroup().remove(userRoleGroup);
                    userRoleGroup.setUserRole(null);
                    getHibernateTemplate().delete(userRoleGroup);
                    userRoleDao.saveUserRole(userRole);
                }
            }
        }

        // evict the query cache
        evictQueryCache(getUserRoleGroupByUserIdAndGroupId_queryCache);
    }

    public void justDeleteUserRoleGroup(UserRoleGroup userRoleGroup) {
        getHibernateTemplate().delete(userRoleGroup);
    }

    @SuppressWarnings("unchecked")
    private List<UserRoleGroup> getUserRoleGroupbyBackUpId(Integer urgId) {
        return (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.backUpTech.id  = :urgId", "urgId", urgId);
    }

    @SuppressWarnings("unchecked")
    public UserRole getUserRoleByUserIdRoleId(Integer userId, Integer roleId) {
        List<UserRole> list = (List<UserRole>) getHibernateTemplate().findByNamedParam("select ur from UserRole as ur where ur.user.id = :userId and ur.role.id = :roleId", new String[]{"userId", "roleId"}, new Object[]{userId, roleId});
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getUserRoleGroupByGroupId(Integer id) {
        return (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.group.id  = :groupId and urg.active = true", "groupId", id);
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getManagerTechnicianUserRoleGroupByGroupId(Group group) {
        // get manager, technician, and ticket pool urgs for this group
        DetachedCriteria dc = DetachedCriteria.forClass(UserRoleGroup.class);
        dc.createAlias("userRole", "userRole", CriteriaSpecification.LEFT_JOIN);
        dc.createAlias("userRole.user", "user", CriteriaSpecification.LEFT_JOIN);
        dc.createAlias("group", "group", CriteriaSpecification.LEFT_JOIN);
        dc.createAlias("userRole.role", "role", CriteriaSpecification.LEFT_JOIN);

        dc.add(Restrictions.ne("active", false));
        dc.add(Restrictions.eq("group", group));

        dc.add(Restrictions
                .disjunction().add(Restrictions.ne("user.active", false)).add(Restrictions.isNull("user.active")));

        dc.add(Restrictions.in("role.id", new Integer[]{Role.ROLE_MANAGER_ID, Role.ROLE_TECHNICIAN_ID}));

        // order by
//        dc.addOrder(Order.asc("group.name"));
        dc.addOrder(Order.asc("user.lastName"));
        dc.addOrder(Order.asc("user.firstName"));
        List userRoleGroups = getHibernateTemplate().findByCriteria(dc);
        // add ticket pool urg
        userRoleGroups.add(getTicketPoolByGroupId(group.getId()));
        return userRoleGroups;

//        List<UserRoleGroup> userRoleGroups = (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam(
//                "from UserRoleGroup as urg left outer join urg.userRole as ur where urg.group  = :group and urg.active = true and " +
//                        "(urg.userRole is null or ur.role.id = :managerId or ur.role.id = :techId)",
//                new String[]{"group", "managerId", "techId"}, new Object[]{group, Role.ROLE_MANAGER_ID, Role.ROLE_TECHNICIAN_ID});
//        return userRoleGroups;
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getUserRoleGroupSelectionByGroupId(Integer id) {
        return (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.group.id  = :groupId and urg.userRole is not null and urg.active = true", "groupId", id);
    }

    @SuppressWarnings("unchecked")
    public UserRoleGroup getUserRoleGroupById(Integer urgId) {
        List<UserRoleGroup> list = (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.id = :urgId", "urgId", urgId);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getUserRoleGroupByUserId(Integer id) {
        return (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.userRole.user.id=:userId and urg.active = true", "userId", id);
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getAllUserRoleGroupByUserId(Integer userId) {
        return (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.userRole.user.id=:userId", "userId", userId);
    }

    @SuppressWarnings("unchecked")
    public void cleanupUserRoleGroup() {
        List<UserRoleGroup> nulledUrgs = getHibernateTemplate().find("from UserRoleGroup as urg where urg.group is null");
        for (UserRoleGroup urg : nulledUrgs) {
            getHibernateTemplate().delete(urg);
        }
    }

    public List<UserRoleGroup> getAllUserRoleGroup() {
        return getHibernateTemplate().loadAll(UserRoleGroup.class);
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getAssignableUserRoleGroupByGroupId(Integer groupId) {
        List<Integer> assignableRoleIds = new ArrayList<Integer>();
        assignableRoleIds.add(Role.ROLE_MANAGER_ID);
        assignableRoleIds.add(Role.ROLE_TECHNICIAN_ID);
        Query q = this.getSession().createQuery("select urg from UserRoleGroup as urg where urg.group.id = :groupId and urg.userRole.role.id in (:assignableRoles) and urg.active = true");
        q.setParameter("groupId", groupId);
        q.setParameterList("assignableRoles", assignableRoleIds);
        Query q2 = this.getSession().createQuery("select urg from UserRoleGroup as urg where urg.group.id = :groupId and urg.userRole is null");
        q2.setParameter("groupId", groupId);
        List<UserRoleGroup> finalList = new ArrayList<UserRoleGroup>();
        finalList.addAll(q.list());
        finalList.addAll(q2.list());
        return finalList;
    }

    public List<UserRoleGroup> getAllAssignableUserRoleGroups() {
        Query q = getSession().createQuery("select urg from UserRoleGroup as urg where urg.active = true and urg.userRole.role.id in (:assignableRoleIds)");
        q.setParameterList("assignableRoleIds", new Integer[]{Role.ROLE_MANAGER_ID, Role.ROLE_TECHNICIAN_ID});
        return q.list();
//        return (List<UserRoleGroup>) getHibernateTemplate().find("select urg from UserRoleGroup as urg where urg.active = true and urg.userRole.role.id in (Role.ROLE_MANAGER_ID, Role.ROLE_TECHNICIAN_ID)");
    }

    public void evictQueryCache(String cacheRegion) {
        getSessionFactory().getCache().evictQueryRegion(cacheRegion);
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getUserRoleGroup() {
        return (List<UserRoleGroup>) getHibernateTemplate().find("select urg from UserRoleGroup as urg where urg.active = true");
    }

    public void saveUserRoleGroup(UserRoleGroup userRoleGroup) {
        getHibernateTemplate().saveOrUpdate(userRoleGroup);
        evictQueryCache(getUserRoleGroupByUserIdAndGroupId_queryCache);
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getUserRoleGroupByUserIdRoleId(Integer userId, Integer roleId) {
        return (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.userRole.user.id=:userId and urg.userRole.role.id = :roleId and urg.active = true", new String[]{"userId", "roleId"}, new Object[]{userId, roleId});
    }

    @SuppressWarnings("unchecked")
    public List<User> getTechnicianByGroupId(Integer groupId) {
        List<User> list = (List<User>) getHibernateTemplate().findByNamedParam("select urg.userRole.user from UserRoleGroup as urg where urg.group.id = :groupId and urg.active = true", "groupId", groupId);
        Collections.sort(list);
        return list;
    }

    @SuppressWarnings("unchecked")
    public UserRoleGroup getUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId) {
        HibernateTemplate hibernateTemplate = createHibernateTemplate(getSessionFactory());
        hibernateTemplate.setCacheQueries(true);
        hibernateTemplate.setQueryCacheRegion(getUserRoleGroupByUserIdAndGroupId_queryCache);
        List<UserRoleGroup> list = (List<UserRoleGroup>) hibernateTemplate.findByNamedParam("select urg from UserRoleGroup as urg where urg.userRole.user.id = :userId and urg.group.id = :groupId and urg.active = true", new String[]{"userId", "groupId"}, new Object[]{userId, groupId});
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public UserRoleGroup getAnyUserRoleGroupByUserIdAndGroupId(Integer userId, Integer groupId) {
        List<UserRoleGroup> list = (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.userRole.user.id = :userId and urg.group.id = :groupId", new String[]{"userId", "groupId"}, new Object[]{userId, groupId});
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public UserRoleGroup getTicketPoolByGroupId(Integer groupId) {
        List<UserRoleGroup> list = (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.userRole is null and urg.group.id = :groupId and urg.active = true", "groupId", groupId);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public Integer getTechnicianCount() {
        return DataAccessUtils.intResult(getHibernateTemplate().find("select count(distinct urg.userRole.user) from UserRoleGroup as urg where urg.userRole is not null and urg.userRole.role.id = 2 or urg.userRole.role.id = 3 and urg.active = true"));
    }

    public Integer getWFParticpantCount() {
        List<Integer> usersWithWFP = getHibernateTemplate().find("select distinct urg.userRole.user.id from UserRoleGroup as urg where urg.userRole is not null and urg.userRole.role.id = 6 and urg.active = true");
        List<Integer> usersWithTech = getHibernateTemplate().find("select distinct urg.userRole.user.id from UserRoleGroup as urg where urg.userRole is not null and urg.userRole.role.id = 2 or urg.userRole.role.id = 3 and urg.active = true");

        // Don't count the WFP if the user has a Technician role also
        usersWithWFP.removeAll(usersWithTech);

        return usersWithWFP.size();
    }


    @SuppressWarnings("unchecked")
    public List<User> getTechnicianAsUsers(String queryParam) {
        List<User> list = (List<User>) getHibernateTemplate().find("select urg.userRole.user from UserRoleGroup as urg where urg.userRole is not null and urg.userRole.role.id = 2 or urg.userRole.role.id = 3 and urg.active = true");

        HashSet set = new HashSet(list);
        list.clear();
        list.addAll(set);

        List<User> userList = new ArrayList<User>();
        for (User user : list) {
            if (StringUtils.isNotBlank(queryParam)) {
                if (StringUtils.containsIgnoreCase(user.getFirstName(), queryParam) || StringUtils.containsIgnoreCase(user.getLastName(), queryParam) ||
                        StringUtils.containsIgnoreCase(user.getLoginId(), queryParam)) {
                    userList.add(user);
                }
            } else {
                userList.add(user);
            }

        }

        Collections.sort(userList);
        return userList;
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getAllTicketPools(String queryParam, String translatedPoolName) {
        List<UserRoleGroup> list = getHibernateTemplate().find("from UserRoleGroup as urg where urg.userRole is null and urg.active = true and urg.group.active = true");

        List<UserRoleGroup> urgList = new ArrayList<UserRoleGroup>();
        for (UserRoleGroup urg : list) {
            if (StringUtils.isNotBlank(queryParam)) {
                if (StringUtils.containsIgnoreCase(translatedPoolName, queryParam) || StringUtils.containsIgnoreCase(urg.getGroup().getName(), queryParam)) {
                    urgList.add(urg);
                }
            } else {
                urgList.add(urg);
            }
        }

        return urgList;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public UserRoleGroup getUserRoleGroupByUserIdGroupIdRoleId(Integer userId, Integer groupId, Integer roleId) {
        List<UserRoleGroup> list = (List<UserRoleGroup>) getHibernateTemplate().findByNamedParam("select urg from UserRoleGroup as urg where urg.userRole.user.id = :userId and urg.userRole.role.id = :roleId and urg.group.id = :groupId", new String[]{"userId", "roleId", "groupId"}, new Object[]{userId, roleId, groupId});
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<UserRoleGroup> getAssignableUserRoleGroups(String queryParam, Integer start, Integer count) {
        DetachedCriteria dc = getAssignableUserRoleGroupCriteria(queryParam);
        // order by
        dc.addOrder(Order.asc("group.name"));
        dc.addOrder(Order.asc("user.lastName"));
        dc.addOrder(Order.asc("user.firstName"));
        return getHibernateTemplate().findByCriteria(dc, start, count);
    }

    public int getAssignableUserRoleGroupCount(String queryParam) {
        DetachedCriteria dc = getAssignableUserRoleGroupCriteria(queryParam);
        dc.setProjection(Projections.rowCount());
        return DataAccessUtils.intResult(getHibernateTemplate().findByCriteria(dc));
    }

    private DetachedCriteria getAssignableUserRoleGroupCriteria(String queryParam) {
        DetachedCriteria dc = DetachedCriteria.forClass(UserRoleGroup.class);
        dc.createAlias("userRole", "userRole", CriteriaSpecification.LEFT_JOIN);
        dc.createAlias("userRole.user", "user", CriteriaSpecification.LEFT_JOIN);
        dc.createAlias("group", "group", CriteriaSpecification.LEFT_JOIN);
        dc.createAlias("userRole.role", "role", CriteriaSpecification.LEFT_JOIN);

        dc.add(Restrictions.ne("active", false));
        dc.add(Restrictions.ne("group.active", false));
        dc.add(Restrictions.disjunction().add(Restrictions.ne("user.active", false)).add(Restrictions.isNull("user.active")));

        // check for urg's that are for manager or tech role, or ticket pool
        Disjunction disj1 = Restrictions.disjunction();
        disj1.add(Restrictions.in("role.id", new Integer[]{Role.ROLE_MANAGER_ID, Role.ROLE_TECHNICIAN_ID}));
        disj1.add(Restrictions.isNull("userRole.user"));
        dc.add(disj1);

        if (StringUtils.isNotBlank(queryParam)) {
            Disjunction disj2 = Restrictions.disjunction();
            String[] splitParam = queryParam.split(",");
            if (splitParam.length == 2) {
                disj2.add(Restrictions.ilike("user.lastName", splitParam[0].trim(), MatchMode.ANYWHERE));
                disj2.add(Restrictions.ilike("user.firstName", splitParam[1].trim(), MatchMode.ANYWHERE));
            } else {
                disj2.add(Restrictions.ilike("user.firstName", queryParam, MatchMode.ANYWHERE));
                disj2.add(Restrictions.ilike("user.lastName", queryParam, MatchMode.ANYWHERE));
                disj2.add(Restrictions.ilike("group.name", queryParam, MatchMode.ANYWHERE));
            }

            dc.add(disj2);
        }

        return dc;
    }

    public void setUserRoleDao(UserRoleDao userRoleDao) {
        this.userRoleDao = userRoleDao;
    }
}
