package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.domain.asset.Asset;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 5, 2008
 * Time: 6:52:26 AM
 */
public interface AssetFilterDao {
    List<AssetFilter> getAll();

    List<AssetFilter> getAllPublic();

    List<AssetFilter> getByUser(User user);

    AssetFilter getById(Integer id);

    void save(AssetFilter assetFilter);

    void delete(AssetFilter assetFilter);

    Integer getAssetCount(AssetFilter assetFilter, User user);

    List<Asset> getAssets(AssetFilter assetFilter, User user);

    List<Asset> getAssets(AssetFilter assetFilter, User user, Integer firstResult, Integer maxResults, String sort);
}
