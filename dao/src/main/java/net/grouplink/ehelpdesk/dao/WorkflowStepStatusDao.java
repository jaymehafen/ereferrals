package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.WorkflowStepStatus;
import net.grouplink.ehelpdesk.domain.WorkflowStep;

import java.util.List;

public interface WorkflowStepStatusDao {
    WorkflowStepStatus getById(Integer id);

    void save(WorkflowStepStatus workflowStepStatus);

    void delete(WorkflowStepStatus workflowStepStatus);

    List<WorkflowStepStatus> getIncompleteSteps();

    int getCountByWorkflowStep(WorkflowStep workflowStep);
}
