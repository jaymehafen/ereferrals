package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.SurveyData;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 8:24:20 AM
 */
public interface SurveyDataDao {

    public void saveSurveyData(SurveyData surveyData);

    List<SurveyData> getByTicketId(Integer ticketId);

    int getCountByTicketId(Integer ticketId);
}
