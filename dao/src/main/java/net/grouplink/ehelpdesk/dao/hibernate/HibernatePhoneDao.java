package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.PhoneDao;
import net.grouplink.ehelpdesk.domain.Phone;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 5:20 PM
 */
public class HibernatePhoneDao extends HibernateDaoSupport implements PhoneDao{
    public void savePhone(Phone phone) {
        getHibernateTemplate().saveOrUpdate(phone);
    }

    public void deletePhone(Phone phone) {
        getHibernateTemplate().delete(phone);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    public Phone getById(int phoneId) {
        return getHibernateTemplate().get(Phone.class, phoneId);
    }
}
