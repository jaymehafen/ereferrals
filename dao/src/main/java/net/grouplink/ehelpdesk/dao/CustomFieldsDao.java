package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 4:07:58 AM
 */
public interface CustomFieldsDao {

    /**
     * Saves a new custom field object to or updates an already existing one in the database.
     * @param customField the custom field to save
     */
    public void saveCustomField(CustomField customField);

    /**
     * Gets the custom field object for a give id.
     * @param id the id of the custom field
     * @return custom field pojo
     */
    public CustomField getCustomFieldById(Integer id);

    /**
     * Deletes a customField from the database.
     * @param customField the custom field to delete
     */
    public void deleteCustomField(CustomField customField);

    public CustomField getCustomFieldByOldIdAndCatOptId(Integer oldId, Integer catOptId);

    public List<CustomField> getAll();

    /**
     * @return a List of custom fields with a unique name
     */
    public List<String> getByUniqueName();

    int getCountByCategoryOption(CategoryOption categoryOption);

    List<CustomField> getGlobalCustomFields();

    List<CustomField> getByLocation();

    List<CustomField> getWithCustomFieldType();

    int getWithCustomFieldTypeCount();

    List<CustomField> getCustomFields(Location location, Group group, Category category, CategoryOption categoryOption);

    List<CustomField> getCustomFieldsByGroup(Group group);

}
