package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.SurveyQuestionDao;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 8:56:54 AM
 */
public class HibernateSurveyQuestionDao extends HibernateDaoSupport implements SurveyQuestionDao {

    @SuppressWarnings("unchecked")
    public List<SurveyQuestion> getDefaultSurveyQuestions() {
        return (List<SurveyQuestion>) getHibernateTemplate().findByNamedParam("select surveyQuestion from SurveyQuestion as surveyQuestion where" +
                " surveyQuestion.survey.id=:ID", "ID", new Integer(1));
    }

    @SuppressWarnings("unchecked")
    public List<SurveyQuestion> getSurveyQuestionsBySurveyId(Integer surveyId) {
        return (List<SurveyQuestion>) getHibernateTemplate().findByNamedParam("select surveyQuestion from SurveyQuestion as surveyQuestion where" +
                " surveyQuestion.survey.id=:ID", "ID", surveyId);
    }

    @SuppressWarnings("unchecked")
    public SurveyQuestion getSurveyQuestionById(Integer id) {
        List<SurveyQuestion> surveyQuestions = (List<SurveyQuestion>) getHibernateTemplate().findByNamedParam("select s from SurveyQuestion as s where s.id=:ID", "ID", id);
        if (surveyQuestions.size() == 1) {
            return surveyQuestions.get(0);
        }
        return null;
    }

    public void saveSurveyQuestion(SurveyQuestion surveyQuestion) {
        getHibernateTemplate().saveOrUpdate(surveyQuestion);
    }

    public void deleteSurveyQuestion(SurveyQuestion surveyQuestion) {
        getHibernateTemplate().delete(surveyQuestion);
    }

    @SuppressWarnings("unchecked")
    public boolean hasDuplicateSurveyQuestionText(SurveyQuestion surveyQuestion) {
        Integer id;
        id = Integer.valueOf("-1");
        if (null != surveyQuestion.getId()) {
            id = surveyQuestion.getId();
        }
        List<SurveyQuestion> list = (List<SurveyQuestion>) getHibernateTemplate().findByNamedParam("select s from SurveyQuestion as s where s.id != :surveyQuestionId and s.surveyQuestionText = :surveyQuestionText", new String[]{"surveyQuestionId", "surveyQuestionText"}, new Object[]{id, surveyQuestion.getSurveyQuestionText()});
        return list.size() > 0;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public List<String> getByUniqueQuestionText() {
        return getHibernateTemplate().find("select distinct sq.surveyQuestionText from SurveyQuestion as sq order by sq.surveyQuestionText asc");
    }
}
