package net.grouplink.ehelpdesk.dao.zen;

import net.grouplink.ehelpdesk.domain.zen.Device;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Apr 1, 2009
 * Time: 2:43:07 PM
 */
public interface DeviceDao {
    List<Device> getAll();
}
