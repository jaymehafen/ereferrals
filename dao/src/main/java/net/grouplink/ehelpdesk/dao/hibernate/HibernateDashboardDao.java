package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.DashboardDao;
import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class HibernateDashboardDao extends HibernateDaoSupport implements DashboardDao {

    @SuppressWarnings("unchecked")
    public List<Dashboard> getAll() {
        return getHibernateTemplate().loadAll(Dashboard.class);
    }

    public Dashboard getById(Integer id) {
        return (Dashboard) getHibernateTemplate().get(Dashboard.class, id);
    }

    public void save(Dashboard dashboard) {
        getHibernateTemplate().saveOrUpdate(dashboard);
    }

    public void delete(Dashboard dashboard) {
        getHibernateTemplate().delete(dashboard);
    }

    public void flush() {
        getHibernateTemplate().flush();
    }
}
