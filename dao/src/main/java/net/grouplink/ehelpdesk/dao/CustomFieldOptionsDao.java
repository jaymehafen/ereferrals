package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.CustomFieldOption;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 4:06:36 AM
 */
public interface CustomFieldOptionsDao {

    public void saveCustomFieldOption(CustomFieldOption customFieldOption);

    /**
     * Saves a list of customFieldOptions objects and all their attributes to the database.
     * @param customFieldOptions list
     */
    public void saveCustomFieldOptions(List<CustomFieldOption> customFieldOptions);

    /**
     * Gets a list of custom field option pojos for a specifc custom field id.
     * @param id The custom field ID of type {@link java.lang.Integer}
     * @return Custom field option pojo
     */
    public List<CustomFieldOption> getCustomFieldOptions(Integer id);

    /**
     * Gets a custom field option pojo for a specifc custom field id.
     * @param id The custom field option ID
     * @return Custom field option pojo
     */
    public CustomFieldOption getCustomFieldOptionByOptionId(Integer id);

    /**
     * Gets the custom field option pojo for a given id.
     * @param id The custom field option ID
     * @return CustomFieldOption pojo
     */
    public CustomFieldOption getCustomFieldOptionsById(Integer id);

    /**
     * Deletes a list of customFieldOptions from the database.
     * @param customFieldOptions The list of CustomFieldOption
     */
    public void deleteCustomFieldOptions(List<CustomFieldOption> customFieldOptions);

    /**
     * Deletes a customFieldOption item from the database.
     * @param customFieldOption The CustomFieldOption object
     */
    public void deleteCustomFieldOption(CustomFieldOption customFieldOption);
}
