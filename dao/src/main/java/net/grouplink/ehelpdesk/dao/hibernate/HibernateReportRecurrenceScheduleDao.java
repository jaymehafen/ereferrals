package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.ReportRecurrenceScheduleDao;
import net.grouplink.ehelpdesk.domain.ReportRecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.dao.support.DataAccessUtils;

public class HibernateReportRecurrenceScheduleDao extends HibernateDaoSupport implements ReportRecurrenceScheduleDao {

    public ReportRecurrenceSchedule getByReportAndSchedule(Report report, RecurrenceSchedule schedule) {
        return (ReportRecurrenceSchedule) DataAccessUtils.singleResult(getHibernateTemplate().findByNamedParam(
                "from ReportRecurrenceSchedule r where r.report = :report and r.recurrenceSchedule = :schedule",
                new String[]{"report", "schedule"}, new Object[]{report, schedule}));
    }

    public void save(ReportRecurrenceSchedule reportRecurrenceSchedule) {
        getHibernateTemplate().saveOrUpdate(reportRecurrenceSchedule);
    }

    public void delete(ReportRecurrenceSchedule reportRecurrenceSchedule) {
        getHibernateTemplate().delete(reportRecurrenceSchedule);
    }
}
