package net.grouplink.ehelpdesk.dao.zen;

public interface ReferenceDao {
    byte[] getZuidForObjectUid(String objectUid);
}
