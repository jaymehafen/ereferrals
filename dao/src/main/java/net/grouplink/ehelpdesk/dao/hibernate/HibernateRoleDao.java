package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.RoleDao;
import net.grouplink.ehelpdesk.domain.Role;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class HibernateRoleDao extends HibernateDaoSupport implements RoleDao {

    @Override
    protected void initDao() throws Exception {
        getHibernateTemplate().setCacheQueries(true);
    }

    @SuppressWarnings({"unchecked"})
	public Role getRoleById(Integer id) {
        return getHibernateTemplate().get(Role.class, id);
	}

    @SuppressWarnings({"unchecked"})
	public Role getRoleByName(String roleName) {
		List roles = getHibernateTemplate().findByNamedParam("select r from Role as r where r.name=:roleName", "roleName", roleName);
		if(roles.size() > 0){
			return (Role) roles.get(0);
		}
		return null;
	}

    @SuppressWarnings("unchecked")
    public List<Role> getNonAdminRoles() {
        return (List<Role>)getHibernateTemplate().find("select r from Role as r where r.id != 1 order by r.id asc");
    }

    public void saveOnly(Role groupMember) {
        // TODO: consider using separate domain object with 'assigned' id generator OR
        // TODO: consider custom id generator that allows the assinged id or other (increment)
        getHibernateTemplate().getSessionFactory().getCurrentSession().save(groupMember, groupMember.getId());
    }

    public List<Role> getGroupRoles() {
        Integer[] groupRoleIds = new Integer[]{Role.ROLE_TECHNICIAN_ID, Role.ROLE_MANAGER_ID, Role.ROLE_MEMBER_ID, Role.ROLE_WFPARTICIPANT_ID};
        List<Role> roles = getHibernateTemplate().findByNamedParam("select r from Role r where r.id in :groupRoleIds", "groupRoleIds", groupRoleIds);
        return roles;
    }

    @SuppressWarnings({"unchecked"})
	public List<Role> getRoles() {
		return (List<Role>)getHibernateTemplate().find("select r from Role as r order by r.id asc");
	}

    @SuppressWarnings({"unchecked"})
	public List<Role> getMngAndTechRoles() {
		return (List<Role>)getHibernateTemplate().find("select r from Role as r where r.id != 1 and r.id != 4 order by r.id asc");
	}

}
