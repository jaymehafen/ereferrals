package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.SurveyQuestionTypeDao;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 8:29:09 AM
 */
public class HibernateSurveyQuestionTypeDao extends HibernateDaoSupport implements SurveyQuestionTypeDao {
}
