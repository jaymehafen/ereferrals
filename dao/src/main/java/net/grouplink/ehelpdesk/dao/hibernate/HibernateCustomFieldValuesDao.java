package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.CustomFieldValuesDao;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 4:12:11 AM
 */
public class HibernateCustomFieldValuesDao extends HibernateDaoSupport implements CustomFieldValuesDao {

    public CustomFieldValues getValuesByTicketCustomField(Integer ticketId, CustomField customField) {
        List cfs = getHibernateTemplate().findByNamedParam(
                "select cfv from CustomFieldValues as cfv where cfv.ticket.id=:TICKETID and cfv.customField=:CUSTOMFIELD",
                new String[]{"TICKETID", "CUSTOMFIELD"}, new Object[]{ticketId, customField});

        if(cfs.size() > 0) return (CustomFieldValues) cfs.get(0);
        return null;
    }

    public void saveCustomFieldValues(CustomFieldValues customFieldValues) {
        getHibernateTemplate().saveOrUpdate(customFieldValues);
    }

    public boolean customFieldHasValues(CustomField customField) {
        List cfvs = getHibernateTemplate().findByNamedParam(
          "select cfv from CustomFieldValues as cfv where cfv.customField=:CUSTOMFIELD", "CUSTOMFIELD", customField
        );
        return cfvs.size() > 0;
    }
}
