package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketFullTextSearchDao;
import net.grouplink.ehelpdesk.domain.FullTextSearchException;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * Author: zpearce
 * Date: 4/11/11
 * Time: 11:52 AM
 */
public class HibernateTicketFullTextSearchDao extends HibernateDaoSupport implements TicketFullTextSearchDao {
    protected final Log log = LogFactory.getLog(HibernateTicketFullTextSearchDao.class);
    private int reindexStatusPercent = 0;

    public void reindexTickets() {
        int job_size = DataAccessUtils.intResult(getHibernateTemplate().find("select count(*) from Ticket as t"));
        int batch_size = 100;
        FullTextSession fullTextSession = Search.getFullTextSession(getSession());
        fullTextSession.setFlushMode(FlushMode.MANUAL);
        fullTextSession.setCacheMode(CacheMode.IGNORE);

        ScrollableResults results = fullTextSession.createCriteria(Ticket.class)
                .setFetchSize(batch_size)
                .scroll(ScrollMode.FORWARD_ONLY);

        int index = 0;
        while(results.next()) {
            index++;
            fullTextSession.index(results.get(0));
            if(index % batch_size == 0) {
                fullTextSession.flushToIndexes();
                fullTextSession.clear();
            }
            reindexStatusPercent = Math.round((index * 1.0f / job_size) * 100);
        }
        fullTextSession.flushToIndexes();
        fullTextSession.clear();
        results.close();
    }

    public int getReindexStatusPercent() {
        return reindexStatusPercent;
    }

    @SuppressWarnings("unchecked")
    public List<Ticket> fullTextSearch(String query, String[] fields, int startPage, int pageSize, User user) {
        FullTextQuery hibQuery = prepareFullTextQuery(query, fields, user);

        hibQuery.setFirstResult(startPage * pageSize);
        hibQuery.setMaxResults(pageSize);
        return hibQuery.list();
    }

    public int fullTextSearchCount(String query, String[] fields, User user) {
        FullTextQuery hibQuery = prepareFullTextQuery(query, fields, user);
        return hibQuery.getResultSize();
    }

    private FullTextQuery prepareFullTextQuery(String query, String[] fields, User user) {
        FullTextSession fullTextSession = Search.getFullTextSession(getSession());

        MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_31, fields, new StandardAnalyzer(Version.LUCENE_31));

        if (StringUtils.startsWith(query, "%%:")) {
            query = StringUtils.removeStart(query, "%%:");
        } else {
            // disable the standard lucene query syntax by escaping the query
            query = MultiFieldQueryParser.escape(query);
        }

        Query luceneQuery = null;
        try {
            luceneQuery = parser.parse(query);
        } catch (ParseException ex) {
            log.error("error parsing lucene query", ex);
            throw new FullTextSearchException(ex.getMessage(), ex);
        }

        FullTextQuery ftq = fullTextSession.createFullTextQuery(luceneQuery, Ticket.class);

        if(!user.getId().equals(User.ADMIN_ID)) {
            ftq.enableFullTextFilter("userVisibleTickets").setParameter("user", user);
        }
        return ftq;
    }
}
