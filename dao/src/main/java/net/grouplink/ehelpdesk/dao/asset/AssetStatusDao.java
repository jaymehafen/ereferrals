package net.grouplink.ehelpdesk.dao.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetStatus;

import java.util.List;

/**
 * User: jaymehafen
 * Date: Jul 31, 2008
 * Time: 11:58:28 AM
 */
public interface AssetStatusDao {
    List<AssetStatus> getAllAssetStatuses();
    AssetStatus getById(Integer assetStatusId);
    AssetStatus getByName(String name);
    void saveAssetStatus(AssetStatus assetStatus);
    void deleteAssetStatus(AssetStatus assetStatus);

    void flush();
}
