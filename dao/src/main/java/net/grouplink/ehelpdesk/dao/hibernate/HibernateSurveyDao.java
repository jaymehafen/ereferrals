package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.SurveyDao;
import net.grouplink.ehelpdesk.domain.Survey;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 8:27:59 AM
 */
public class HibernateSurveyDao extends HibernateDaoSupport implements SurveyDao {

    @SuppressWarnings("unchecked")
    public Survey getSurveyByName(String name) {
        List<Survey> surveys = (List<Survey>) getHibernateTemplate().findByNamedParam("select s from Survey as s where s.name=:NAME", "NAME", name);
        if (surveys.size() == 1) {
            return surveys.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public Survey getSurveyById(Integer id) {
        List<Survey> surveys = (List<Survey>) getHibernateTemplate().findByNamedParam("select s from Survey as s where s.id=:ID", "ID", id);
        if (surveys.size() == 1) {
            return surveys.get(0);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<Survey> getActiveSurveys() {
        return (List<Survey>) getHibernateTemplate().find("select s from Survey as s where s.active = true");
    }

    public void saveSurvey(Survey survey) {
        getHibernateTemplate().saveOrUpdate(survey);
    }

    public void deleteSurvey(Survey survey) {
        getHibernateTemplate().delete(survey);
    }

    @SuppressWarnings("unchecked")
    public boolean hasDuplicateSurveyName(Survey survey) {
        Integer id = Integer.valueOf("-1");
        if (survey.getId() != null) {
            id = survey.getId();
        }
        List<Survey> list = (List<Survey>) getHibernateTemplate().findByNamedParam("select s from Survey as s where s.id != :surveyId and s.name = :surveyName and s.active = true", new String[]{"surveyId", "surveyName"}, new Object[]{id, survey.getName()});
        return list.size() > 0;
    }

    public void flush() {
        getHibernateTemplate().flush();
    }

    @SuppressWarnings("unchecked")
    public List<Survey> getAllSurveys() {
        return (List<Survey>) getHibernateTemplate().find("from Survey");
    }
}
