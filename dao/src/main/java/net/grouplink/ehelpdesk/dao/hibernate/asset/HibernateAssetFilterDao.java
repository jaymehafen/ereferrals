package net.grouplink.ehelpdesk.dao.hibernate.asset;

import net.grouplink.ehelpdesk.dao.asset.AssetFilterDao;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.domain.asset.AssetReportGroupByOptions;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 5, 2008
 * Time: 6:58:58 AM
 */
public class HibernateAssetFilterDao extends HibernateDaoSupport implements AssetFilterDao {

    private final Log log = LogFactory.getLog(HibernateAssetFilterDao.class);
    private SimpleDateFormat filterDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @SuppressWarnings("unchecked")
    public List<AssetFilter> getAll() {
        return (List<AssetFilter>) getHibernateTemplate().loadAll(AssetFilter.class);
    }

    @SuppressWarnings("unchecked")
    public List<AssetFilter> getAllPublic() {
        return (List<AssetFilter>) getHibernateTemplate().find("from AssetFilter as af where af.privateFilter = false");
    }

    @SuppressWarnings("unchecked")
    public List<AssetFilter> getByUser(User user) {
        return (List<AssetFilter>) getHibernateTemplate().findByNamedParam("from AssetFilter as af where af.user = :user", "user", user);
    }

    @SuppressWarnings("unchecked")
    public AssetFilter getById(Integer id) {
        List<AssetFilter> list = (List<AssetFilter>) getHibernateTemplate().findByNamedParam("from AssetFilter as af where af.id = :id", "id", id);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }

    public void save(AssetFilter assetFilter) {
        getHibernateTemplate().saveOrUpdate(assetFilter);
    }

    public void delete(AssetFilter assetFilter) {
        getHibernateTemplate().delete(assetFilter);
    }

    public Integer getAssetCount(AssetFilter f, User user) {
        Criteria c = getAssetFilterCriteria(f, user);
        c.setProjection(Projections.countDistinct("id"));
        List results = c.list();
        Long countLong = (Long) results.get(0);
        return safeLongToInt(countLong);
    }

    private static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(l + " cannot be cast to int without changing its value.");
        }
        return (int) l;
    }

    private Criteria getAssetFilterCriteria(AssetFilter f, User user) {
        Criteria c = getSessionFactory().getCurrentSession().createCriteria(Asset.class);

        // applyUserCriteriaRestrictions(user, c);

        // "in" criteria (Integer[]s)
        addInCriteriaIfArrayNotEmpty(c, f.getTypeIds(), "assetType.id");//typeIds
        addInCriteriaIfArrayNotEmpty(c, f.getOwnerIds(), "owner.id");//ownerIds
        addInCriteriaIfArrayNotEmpty(c, f.getVendorIds(), "vendor.id");//vendorIds
        addInCriteriaIfArrayNotEmpty(c, f.getLocationIds(), "location.id");//locationIds
        addInCriteriaIfArrayNotEmpty(c, f.getGroupIds(), "group.id");//groupIds
        addInCriteriaIfArrayNotEmpty(c, f.getCategoryIds(), "category.id");//categoryIds
        addInCriteriaIfArrayNotEmpty(c, f.getCategoryOptionIds(), "categoryOption.id");//categoryOptionIds
        addInCriteriaIfArrayNotEmpty(c, f.getStatusIds(), "status.id");//statusIds

        // ilike for the String[]s
        addILikeCriteriaIfArrayNotEmpty(c, f.getAssetNumbers(), "assetNumber"); //assetNumbers
        addILikeCriteriaIfArrayNotEmpty(c, f.getNames(), "name"); //names
        addILikeCriteriaIfArrayNotEmpty(c, f.getNotes(), "notes"); //notes
        addILikeCriteriaIfArrayNotEmpty(c, f.getAssetLocations(), "assetLocation"); //assetLocation
        addILikeCriteriaIfArrayNotEmpty(c, f.getManufacturers(), "manufacturer");//manufacturers
        addILikeCriteriaIfArrayNotEmpty(c, f.getModelNumbers(), "modelNumber");//modelNumbers
        addILikeCriteriaIfArrayNotEmpty(c, f.getSerialNumbers(), "serialNumber");//serialNumbers
        addILikeCriteriaIfArrayNotEmpty(c, f.getDescriptions(), "description");//descriptions

        // asset.dates
        if (f.getAcquisitionDateOperator() != null) {
            addDateCriteria(c, "acquisitionDate", f.getAcquisitionDateOperator(), f.getAcquisitionDate(), f.getAcquisitionDate2());
        }

        // accounting info props
        c.createAlias("accountingInfo", "acctInfo");
        addILikeCriteriaIfArrayNotEmpty(c, f.getPoNumbers(), "acctInfo.poNumber");
        addILikeCriteriaIfArrayNotEmpty(c, f.getAccountNumbers(), "acctInfo.accountNumber");
        addILikeCriteriaIfArrayNotEmpty(c, f.getAccumulatedNumbers(), "acctInfo.accumulatedNumber");
        addILikeCriteriaIfArrayNotEmpty(c, f.getExpenseNumbers(), "acctInfo.expenseNumber");
        addILikeCriteriaIfArrayNotEmpty(c, f.getAcquisitionValues(), "acctInfo.acquisitionValue");
        addILikeCriteriaIfArrayNotEmpty(c, f.getLeaseNumbers(), "acctInfo.leaseNumber");
        addILikeCriteriaIfArrayNotEmpty(c, f.getLeaseDurations(), "acctInfo.leaseDuration");
        addILikeCriteriaIfArrayNotEmpty(c, f.getLeaseFrequencys(), "acctInfo.leaseFrequency");
        addILikeCriteriaIfArrayNotEmpty(c, f.getLeaseFrequencyPrices(), "acctInfo.leaseFrequencyPrice");
        addILikeCriteriaIfArrayNotEmpty(c, f.getDisposalMethods(), "acctInfo.disposalMethod");
        addILikeCriteriaIfArrayNotEmpty(c, f.getInsuranceCategories(), "acctInfo.insuranceCategory");
        addILikeCriteriaIfArrayNotEmpty(c, f.getReplacementValuecategories(), "acctInfo.replacementValueCategory");
        addILikeCriteriaIfArrayNotEmpty(c, f.getReplacementValues(), "acctInfo.replacementValue");
        addILikeCriteriaIfArrayNotEmpty(c, f.getMaintenanceCosts(), "acctInfo.maintenanceCost");
        addILikeCriteriaIfArrayNotEmpty(c, f.getDepreciationMethods(), "acctInfo.depreciationMethod");
        // accounting info dates
        if (f.getDisposalDateOperator() != null) {
            addDateCriteria(c, "acctInfo.disposalDate", f.getDisposalDateOperator(), f.getDisposalDate(), f.getDisposalDate2());
        }
        if (f.getLeaseExpirationDateOperator() != null) {
            addDateCriteria(c, "acctInfo.leaseExpirationDate", f.getLeaseExpirationDateOperator(), f.getLeaseExpirationDate(), f.getLeaseExpirationDate2());
        }
        if (f.getWarrantyDateOperator() != null) {
            addDateCriteria(c, "acctInfo.warrantyDate", f.getWarrantyDateOperator(), f.getWarrantyDate(), f.getWarrantyDate2());
        }

        // custom fields
        Map<String, String[]> customFields = f.getCustomFields();
        if (MapUtils.isNotEmpty(customFields)) {
            c.createAlias("customFieldValues", "customFieldValues", Criteria.LEFT_JOIN);
            c.createAlias("customFieldValues.customField", "cfvCustomField", Criteria.LEFT_JOIN);
            for (Map.Entry<String, String[]> entry : customFields.entrySet()) {
                Integer customFieldId = new Integer(entry.getKey());
                String[] searchVals = entry.getValue();
                Disjunction outerDisj = Restrictions.disjunction();
                for (String customFieldValue : searchVals) {
                    if (StringUtils.isNotBlank(customFieldValue)) {
                        Conjunction innerConj = Restrictions.conjunction();
                        innerConj.add(Restrictions.eq("cfvCustomField.id", customFieldId));
                        innerConj.add(Restrictions.ilike("customFieldValues.fieldValue", customFieldValue, MatchMode.ANYWHERE));
                        outerDisj.add(innerConj);
                    }
                }

                c.add(outerDisj);
            }
        }

        Integer[] softLics = f.getSoftLicIds();
        if(!ArrayUtils.isEmpty(softLics)) {
            c.createAlias("softwareLicenses", "softwareLicenses", Criteria.LEFT_JOIN);
            Disjunction dis = Restrictions.disjunction();
            for(Integer licId : softLics) {
                dis.add(Restrictions.eq("softwareLicenses.id", licId));
            }
            c.add(dis);
        }

        if (f.getReport() != null && f.getReport() && f.getGroupBy() != null && f.getGroupBy() != 0) {
            Integer groupBy = f.getGroupBy();
            if (groupBy.equals(AssetReportGroupByOptions.CATEGORY)) {
                c.addOrder(Order.asc("category"));
            } else if (groupBy.equals(AssetReportGroupByOptions.CATEGORY_OPTION)) {
                c.addOrder(Order.asc("categoryOption"));
            } else if (groupBy.equals(AssetReportGroupByOptions.GROUP)) {
                c.addOrder(Order.asc("group"));
            } else if (groupBy.equals(AssetReportGroupByOptions.LOCATION)) {
                c.addOrder(Order.asc("location"));
            } else if (groupBy.equals(AssetReportGroupByOptions.OWNER)) {
                c.addOrder(Order.asc("owner"));
            } else if (groupBy.equals(AssetReportGroupByOptions.STATUS)) {
                c.addOrder(Order.asc("status"));
            } else if (groupBy.equals(AssetReportGroupByOptions.TYPE)) {
                c.addOrder(Order.asc("assetType"));
            } else {
                log.error("unknown groupby column value: " + groupBy);
            }
        }

        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return c;
    }

    @SuppressWarnings("unchecked")
    public List<Asset> getAssets(AssetFilter f, User user) {
        Criteria c = getAssetFilterCriteria(f, user);
        return (List<Asset>) c.list();
    }

    @SuppressWarnings("unchecked")
    public List<Asset> getAssets(AssetFilter f, User user, Integer firstResult, Integer maxResults, String sort) {
        Criteria c = getAssetFilterCriteria(f, user);
        if(firstResult > -1) {
            c.setFirstResult(firstResult);
        }

        if(maxResults > -1) {
            c.setMaxResults(maxResults);
        }

        addOrder(c, f, sort);

        return (List<Asset>) c.list();
    }

    private void addOrder(Criteria c, AssetFilter f, String sort) {
        if(StringUtils.isNotBlank(sort)) {
            addOrderProperty(c, sort);
        } else if (!ArrayUtils.isEmpty(f.getColumnOrder()) && StringUtils.isNotBlank(f.getColumnOrder()[0])) {
            addOrderProperty(c, f.getColumnOrder()[0]);
        }
    }

    private void addOrderProperty(Criteria c, String sort) {
        if(StringUtils.contains(sort, AssetFilter.COLUMN_ASSETNUMBER)) {
            addOrderAscDesc(c, sort, "assetNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_NAME)) {
            addOrderAscDesc(c, sort, "name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_TYPE)) {
            c.createAlias("assetType", "assetType", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "assetType.name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_OWNER)) {
            c.createAlias("owner", "owner", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "owner.lastName");
            addOrderAscDesc(c, sort, "owner.firstName");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_VENDOR)) {
            c.createAlias("vendor", "vendor", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "vendor.name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_ASSETLOCATION)) {
            addOrderAscDesc(c, sort, "assetLocation");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_ACQUISITIONDATE)) {
            addOrderAscDesc(c, sort, "acquisitionDate");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_LOCATION)) {
            c.createAlias("location", "location", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "location.name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_GROUP)) {
            c.createAlias("group", "group", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "group.name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_CATEGORY)) {
            c.createAlias("category", "category", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "category.name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_CATEGORYOPTION)) {
            c.createAlias("categoryOption", "categoryOption", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "categoryOption.name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_MANUFACTURER)) {
            addOrderAscDesc(c, sort, "manufacturer");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_MODELNUMBER)) {
            addOrderAscDesc(c, sort, "modelNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_SERIALNUMBER)) {
            addOrderAscDesc(c, sort, "serialNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_DESCRIPTION)) {
            addOrderAscDesc(c, sort, "description");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_STATUS)) {
            c.createAlias("status", "status", CriteriaSpecification.LEFT_JOIN);
            addOrderAscDesc(c, sort, "status.name");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_SOFTLICS)) {
            // Don't do anything. Can only search by one software license at a time anyway.
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_PONUMBER)) {
            // Accounting info alias already created
            addOrderAscDesc(c, sort, "acctInfo.poNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_ACCOUNTNUMBER)) {
            addOrderAscDesc(c, sort, "acctInfo.accountNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_ACCUMULATEDNUMBER)) {
            addOrderAscDesc(c, sort, "acctInfo.accumulatedNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_EXPENSENUMBER)) {
            addOrderAscDesc(c, sort, "acctInfo.expenseNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_ACQUISITIONVALUE)) {
            addOrderAscDesc(c, sort, "acctInfo.acquisitionValue");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_LEASENUMBER)) {
            addOrderAscDesc(c, sort, "acctInfo.leaseNumber");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_LEASEDURATION)) {
            addOrderAscDesc(c, sort, "acctInfo.leaseDuration");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_LEASEFREQUENCY)) {
            addOrderAscDesc(c, sort, "acctInfo.leaseFrequency");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_LEASEFREQUENCYPRICE)) {
            addOrderAscDesc(c, sort, "acctInfo.leaseFrequencyPrice");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_DISPOSALMETHOD)) {
            addOrderAscDesc(c, sort, "acctInfo.disposalMethod");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_DISPOSALDATE)) {
            addOrderAscDesc(c, sort, "acctInfo.disposalDate");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_INSURANCECATEGORY)) {
            addOrderAscDesc(c, sort, "acctInfo.insuranceCategory");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_REPLACEMENTVALUECATEGORY)) {
            addOrderAscDesc(c, sort, "acctInfo.replacementValueCategory");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_REPLACEMENTVALUE)) {
            addOrderAscDesc(c, sort, "acctInfo.replacementValue");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_MAINTENANCECOST)) {
            addOrderAscDesc(c, sort, "acctInfo.maintenanceCost");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_DEPRECIATIONMETHOD)) {
            addOrderAscDesc(c, sort, "acctInfo.depreciationMethod");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_LEASEEXPIRATIONDATE)) {
            addOrderAscDesc(c, sort, "acctInfo.leaseExpirationDate");
        } else if(StringUtils.contains(sort, AssetFilter.COLUMN_AI_WARRANTYDATE)) {
            addOrderAscDesc(c, sort, "acctInfo.warrantyDate");
        }
    }

    private void addOrderAscDesc(Criteria c, String sort, String sortProp) {
        if (sort.startsWith("-"))
            c.addOrder(Order.desc(sortProp));
        else
            c.addOrder(Order.asc(sortProp));
    }

    private void addDateCriteria(Criteria c, String propertyName, Integer dateOperator, String date1, String date2) {
        if (dateOperator != null) {
            if (dateOperator.equals(TicketFilterOperators.DATE_ANY)) {
                // do nothing
            } else if (dateOperator.equals(TicketFilterOperators.DATE_TODAY)) {
                Date todayStart = DateUtils.truncate(new Date(), Calendar.DATE);
                c.add(Restrictions.ge(propertyName, todayStart));
                c.add(Restrictions.le(propertyName, maxDate(todayStart)));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_WEEK)) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                Date weekStart = calendar.getTime();
                weekStart = DateUtils.truncate(weekStart, Calendar.DATE);
                c.add(Restrictions.ge(propertyName, weekStart));
                calendar.add(Calendar.DAY_OF_MONTH, 7);
                Date weekEnd = calendar.getTime();
                weekEnd = DateUtils.truncate(weekEnd, Calendar.DATE);
                c.add(Restrictions.le(propertyName, weekEnd));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_MONTH)) {
                Date monthStart = DateUtils.truncate(new Date(), Calendar.MONTH);
                c.add(Restrictions.ge(propertyName, monthStart));
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONDAY, 1);
                Date monthEnd = calendar.getTime();
                monthEnd = DateUtils.truncate(monthEnd, Calendar.DATE);
                c.add(Restrictions.le(propertyName, monthEnd));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_ON)) {
                Date selectedDate = parseDate(date1);
                Date start = DateUtils.truncate(selectedDate, Calendar.DATE);
                Date end = maxDate(selectedDate);
                c.add(Restrictions.between(propertyName, start, end));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_BEFORE)) {
                Date selectedDate = parseDate(date1);
                selectedDate = DateUtils.truncate(selectedDate, Calendar.DATE);
                c.add(Restrictions.lt(propertyName, selectedDate));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_AFTER)) {
                Date selectedDate = parseDate(date1);
                selectedDate = maxDate(selectedDate);
                c.add(Restrictions.gt(propertyName, selectedDate));
            } else if (dateOperator.equals(TicketFilterOperators.DATE_RANGE)) {
                Date d1 = parseDate(date1);
                d1 = DateUtils.truncate(d1, Calendar.DATE);
                Date d2 = parseDate(date2);
                d2 = maxDate(d2);
                c.add(Restrictions.between(propertyName, d1, d2));
            } else {
                log.error("invalid date operator value: " + dateOperator);
            }
        }
    }

    private Date parseDate(String date1) {
        Date selectedDate;
        try {
            selectedDate = filterDateFormat.parse(date1);
        } catch (ParseException e) {
            log.error("error parsing date: " + date1, e);
            throw new RuntimeException(e);
        }

        return selectedDate;
    }

    /**
     * Sets the hour, minute, second, and millisecond values of the given date
     * to the maximum (1 ms before next day) and returns the new date.
     *
     * @param selectedDate the base date
     * @return the base date with the maximum values set
     */
    private Date maxDate(Date selectedDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(selectedDate);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    private void addInCriteriaIfArrayNotEmpty(Criteria c, Object[] assetFilterArray, String propertyName) {
        if (!ArrayUtils.isEmpty(assetFilterArray))
            c.add(Restrictions.in(propertyName, assetFilterArray));
    }

    private void addILikeCriteriaIfArrayNotEmpty(Criteria c, String[] afArray, String propertyName) {
        if (!ArrayUtils.isEmpty(afArray)) {
            Disjunction disj = Restrictions.disjunction();
            for (String n : afArray) {
                disj.add(Restrictions.ilike(propertyName, n, MatchMode.ANYWHERE));
            }
            c.add(disj);
        }
    }
}
