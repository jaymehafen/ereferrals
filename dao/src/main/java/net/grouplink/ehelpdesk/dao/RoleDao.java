package net.grouplink.ehelpdesk.dao;

import java.util.List;

import net.grouplink.ehelpdesk.domain.Role;

public interface RoleDao {
	List<Role> getRoles();
    List<Role> getMngAndTechRoles();
    Role getRoleById(Integer id);
	Role getRoleByName(String roleName);
    List<Role> getNonAdminRoles();

    void saveOnly(Role groupMember);

    List<Role> getGroupRoles();
}
