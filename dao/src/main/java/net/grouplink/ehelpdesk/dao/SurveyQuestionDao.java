package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.SurveyQuestion;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 8:24:55 AM
 */
public interface SurveyQuestionDao {

    public List<SurveyQuestion> getDefaultSurveyQuestions();

    public List<SurveyQuestion> getSurveyQuestionsBySurveyId(Integer surveyId);

    public SurveyQuestion getSurveyQuestionById(Integer id);

    public void saveSurveyQuestion(SurveyQuestion surveyQuestion);

    public void deleteSurveyQuestion(SurveyQuestion surveyQuestion);

    public boolean hasDuplicateSurveyQuestionText(SurveyQuestion surveyQuestion);

    public void flush();

    List<String> getByUniqueQuestionText();
}
