package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.SurveyQuestionResponse;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Mar 18, 2008
 * Time: 3:37:39 PM
 */
public interface SurveyQuestionResponseDao {

    public SurveyQuestionResponse getSurveyQuestionResponseById(Integer id);

    public List<SurveyQuestionResponse> getSurveyQuestionResponsesByQuestionId(Integer id);

    public void saveSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse);

    public void deleteSurveyQuestionResponse(SurveyQuestionResponse surveyQuestionResponse);

    public boolean hasDuplicateSurveyQuestionResponseText(SurveyQuestionResponse surveyQuestionResponse);

    public void flush();
}
