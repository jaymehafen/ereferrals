package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.SurveyFrequency;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 27, 2008
 * Time: 8:24:39 AM
 */
public interface SurveyFrequencyDao {

    public List<SurveyFrequency> getSurveyFrequencies();

    public SurveyFrequency getDefaultSurveyFrequency();

    public SurveyFrequency getSurveyFrequencyById(Integer id);

    public void save(SurveyFrequency surveyFrequency);

    public void flush();
}
