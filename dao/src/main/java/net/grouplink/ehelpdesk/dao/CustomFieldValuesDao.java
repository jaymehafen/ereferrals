package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;

/**
 * Created by IntelliJ IDEA.
 * User: mabbasi
 * Date: May 5, 2007
 * Time: 4:12:59 AM
 */
public interface CustomFieldValuesDao {
    public CustomFieldValues getValuesByTicketCustomField(Integer ticketId, CustomField customField);
    public void saveCustomFieldValues(CustomFieldValues customFieldValues);
    public boolean customFieldHasValues(CustomField customField);
}
