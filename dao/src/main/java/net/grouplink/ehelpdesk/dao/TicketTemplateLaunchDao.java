package net.grouplink.ehelpdesk.dao;

import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;

import java.util.Date;
import java.util.List;

public interface TicketTemplateLaunchDao {
    TicketTemplateLaunch getById(Integer id);

    void save(TicketTemplateLaunch ticketTemplateLaunch);

    void delete(TicketTemplateLaunch ticketTemplateLaunch);

    int getLaunchCountByTemplateMaster(TicketTemplateMaster ttm);

    List<TicketTemplateLaunch> getPendingOlderThan(Date activeDate);
}
