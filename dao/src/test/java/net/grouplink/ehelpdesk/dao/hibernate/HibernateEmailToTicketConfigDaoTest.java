package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.LocationDao;
import net.grouplink.ehelpdesk.dao.TicketPriorityDao;
import net.grouplink.ehelpdesk.domain.EmailToTicketConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.TicketPriority;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Jan 4, 2008
 * Time: 11:18:30 AM
 */
public class HibernateEmailToTicketConfigDaoTest extends AbstractDaoTestCase {

    private HibernateEmailToTicketConfigDao hibernateEmailToTicketConfigDao;
    private HibernateGroupDao hibernateGroupDao;
    private LocationDao locationDao;
    private TicketPriorityDao ticketPriorityDao;

    /**
     * test the save method directly. indirectly test the hibernate mapping (.hbm.xml)
     */
    public void testSave(){
        // Create an EmailToTicketConfig
        EmailToTicketConfig et1 = new EmailToTicketConfig();
        et1.setAllowNonUsers(true);
        et1.setEnabled(true);
        et1.setImapFolder("myImapFolder");
        et1.setPassword("myPissword");
        et1.setPort(110);
        et1.setServerName("theserver");
        et1.setServerType(EmailToTicketConfig.SERVER_TYPE_IMAP);
        et1.setUserName("auser");
        et1.setUseSsl(false);

        // Create a new Group to test with this dao
        Group group = new Group();
        group.setActive(true);
        group.setName("ETTestGroup");
        hibernateGroupDao.saveGroup(group);
        et1.setGroup(group);

        Location location = new Location();
        location.setActive(true);
        location.setComments("test location for e2t config");
        location.setName("E2TTestLocation");
        locationDao.saveLocation(location);
        et1.setLocation(location);

        TicketPriority tp = new TicketPriority();
        tp.setName("e2ttestpriority");
        ticketPriorityDao.savePriority(tp);
        et1.setTicketPriority(tp);

        // save the object and check that the id got there
        hibernateEmailToTicketConfigDao.save(et1);
        assertNotNull(et1.getId());

        // get the object back out and check it's not null
        EmailToTicketConfig et2 = hibernateEmailToTicketConfigDao.getById(et1.getId());
        assertNotNull(et2);

        // check that props are same as above
        assertTrue(et2.isAllowNonUsers());
        assertTrue(et2.isEnabled());
        assertEquals(et2.getImapFolder(), "myImapFolder");
        assertEquals(et2.getPassword(), "myPissword");
        assertEquals(et2.getPort(), new Integer(110));
        assertEquals(et2.getServerName(), "theserver");
        assertEquals(et2.getServerType(), EmailToTicketConfig.SERVER_TYPE_IMAP);
        assertEquals(et2.getUserName(), "auser");
        assertFalse(et2.isUseSsl());
        assertEquals(et2.getGroup(), group);
        assertEquals(et2.getLocation(), location);
        assertEquals(et2.getTicketPriority(), tp);
    }


    public void testGetAll(){
        EmailToTicketConfig et1 = new EmailToTicketConfig();
        et1.setEnabled(true);
        EmailToTicketConfig et2 = new EmailToTicketConfig();
        et2.setEnabled(true);
        EmailToTicketConfig et3 = new EmailToTicketConfig();
        et3.setEnabled(false);
        EmailToTicketConfig et4 = new EmailToTicketConfig();
        et4.setEnabled(true);

        List<EmailToTicketConfig> origEts = new ArrayList<EmailToTicketConfig>();
        origEts.add(et1);
        origEts.add(et2);
        origEts.add(et3);
        origEts.add(et4);

        List<EmailToTicketConfig> origEnabled = new ArrayList<EmailToTicketConfig>();
        origEnabled.add(et1);
        origEnabled.add(et2);
        origEnabled.add(et4);

        // save the ets
        hibernateEmailToTicketConfigDao.save(et1);
        hibernateEmailToTicketConfigDao.save(et2);
        hibernateEmailToTicketConfigDao.save(et3);
        hibernateEmailToTicketConfigDao.save(et4);

        List ets = hibernateEmailToTicketConfigDao.getAll();
        assertTrue(ets.containsAll(origEts));

        List etenabled = hibernateEmailToTicketConfigDao.getAllEnabled();
        assertTrue(etenabled.containsAll(origEnabled));
    }

    public void testDelete(){
        // Create an EmailToTicketConfig
        EmailToTicketConfig et1 = new EmailToTicketConfig();
        et1.setAllowNonUsers(true);
        et1.setEnabled(true);
        et1.setImapFolder("myImapFolder");
        et1.setPassword("myPissword");
        et1.setPort(110);
        et1.setServerName("theserver");
        et1.setServerType(EmailToTicketConfig.SERVER_TYPE_IMAP);
        et1.setUserName("auser");
        et1.setUseSsl(false);

        // Create a new Group to test with this dao
        Group group = new Group();
        group.setActive(true);
        group.setName("ETTestGroup");
        hibernateGroupDao.saveGroup(group);
        et1.setGroup(group);

        // save the object and check that the id got there
        hibernateEmailToTicketConfigDao.save(et1);

        Integer newId = et1.getId();
        assertNotNull(newId);

        hibernateEmailToTicketConfigDao.delete(et1);
        EmailToTicketConfig et2 = hibernateEmailToTicketConfigDao.getById(newId);
        assertNull(et2);
    }

    public void setHibernateEmailToTicketConfigDao(HibernateEmailToTicketConfigDao hibernateEmailToTicketConfigDao) {
        this.hibernateEmailToTicketConfigDao = hibernateEmailToTicketConfigDao;
    }

    public void setHibernateGroupDao(HibernateGroupDao hibernateGroupDao) {
        this.hibernateGroupDao = hibernateGroupDao;
    }

    public void setLocationDao(LocationDao locationDao) {
        this.locationDao = locationDao;
    }

    public void setTicketPriorityDao(TicketPriorityDao ticketPriorityDao) {
        this.ticketPriorityDao = ticketPriorityDao;
    }
}
