package net.grouplink.ehelpdesk.dao.hibernate;

import net.grouplink.ehelpdesk.dao.TicketDao;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Feb 21, 2008
 * Time: 8:03:12 PM
 */
public class HibernateTicketFilterDaoTest extends AbstractDaoTestCase {

    protected final Log logger = LogFactory.getLog(getClass());

    private HibernateTicketFilterDao hibernateTicketFilterDao;
    private HibernateUserDao userDao;
    private TicketDao ticketDao;
    private boolean dummyTicketsCreated;

    public HibernateTicketFilterDaoTest() {
//        setDefaultRollback(false);
    }

    public void testSave() {

        // only use users in the database
        List users = userDao.getUsers();

        for (int i = 0; i < users.size(); i++) {
            User user = (User) users.get(i);

            // Create a new TicketFilter
            TicketFilter tf1 = new TicketFilter();
            tf1.setUser(user);
            tf1.setPrivateFilter(Boolean.TRUE);
            tf1.setName("My High Priority Tickets - " + user.getLoginId());

            hibernateTicketFilterDao.save(tf1);

            TicketFilter tf2 = new TicketFilter();
            tf2.setUser(user);
            tf2.setPrivateFilter(Boolean.FALSE);
            tf2.setName("Some important tickets - " + user.getLoginId());

            hibernateTicketFilterDao.save(tf2);
        }
    }

    public void testGetTickets() {
        TicketFilter tf = new TicketFilter();
        List/*<Ticket>*/ tickets;

        User user = userDao.getUserById(Integer.valueOf("2"));
        tf.setUser(user);

        tf.setCustomFieldName(new String[]{"Size", "Priority", "Category"});
        tf.setCustomFieldValue(new String[]{"", "", ""});


        Map<Group, Boolean> viewTicketsPermissions = new HashMap<Group, Boolean>();

        // testing various search scenarios
        tf.setAssignedToIds(new Integer[]{Integer.valueOf("3")});
        tf.setAssignedToIdsOps(new Integer[]{TicketFilterOperators.EQUALS});
        tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertNotNull(tickets);
        assertTrue(tickets.size() >= 2);
        tf.setAssignedToIds(null);

        tf.setAssignedToIds(new Integer[]{Integer.valueOf("3"), Integer.valueOf("7")});
        tf.setAssignedToIdsOps(new Integer[]{TicketFilterOperators.EQUALS, TicketFilterOperators.EQUALS});
        tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertNotNull(tickets);
        assertTrue(tickets.size() >= 5);
        tf.setAssignedToIds(null);

        tf.setCategoryIds(new Integer[]{Integer.valueOf("5")});
        tf.setCategoryIdsOps(new Integer[]{TicketFilterOperators.EQUALS});
        tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertNotNull(tickets);
        assertTrue(tickets.size() >= 14);
        tf.setCategoryIds(null);

        tf.setTicketNumbers(new Integer[]{new Integer(1), new Integer(3)});
        tf.setTicketNumbersOps(new Integer[]{TicketFilterOperators.EQUALS, TicketFilterOperators.EQUALS});
        tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertTrue(tickets.size() == 2);
        tf.setTicketNumbers(null);

        tf.setHistorySubject(new String[]{"Manager Update"});
        tf.setHistorySubjectOps(new Integer[]{TicketFilterOperators.CONTAINS});
        tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertTrue(tickets.size() == 27);
        tf.setHistorySubject(null);

        tf.setHistoryNote(new String[]{"Jim Steffen"});
        tf.setHistoryNoteOps(new Integer[]{TicketFilterOperators.CONTAINS});
        tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertTrue(tickets.size() >= 31);
        tf.setHistoryNote(null);
    }

    public void testGetTicketsCustomFields() {
        TicketFilter tf = new TicketFilter();
        User user = userDao.getUserById(Integer.valueOf("2"));
        tf.setUser(user);

        tf.setCustomFieldName(new String[]{"Size"});
        tf.setCustomFieldValue(new String[]{"Other"});

        Map<Group, Boolean> viewTicketsPermissions = new HashMap<Group, Boolean>();
        List/*<Ticket>*/ tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertNotNull(tickets);
        assertTrue(tickets.size() >= 31);
    }

    public void testGetTicketsCreatedDate() {
        // create dummy data
        createDummyTickets();

        TicketFilter tf = new TicketFilter();
        User user = userDao.getUserById(Integer.valueOf("0"));
        tf.setUser(user);

        Map<Group, Boolean> viewTicketsPermissions = new HashMap<Group, Boolean>();
        // today
        tf.setCreatedDateOperator(TicketFilterOperators.DATE_TODAY);
        List/*<Ticket>*/ tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertNotNull(tickets);
        assertTrue(tickets.size() == 1);

        // week
        tf.setCreatedDateOperator(TicketFilterOperators.DATE_WEEK);
        tickets = hibernateTicketFilterDao.getTickets(tf, user, viewTicketsPermissions);
        assertNotNull(tickets);
        Calendar cal = Calendar.getInstance();
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                assertTrue(tickets.size() == 1);
                break;
            case Calendar.MONDAY:
                assertTrue(tickets.size() == 2);
                break;
            case Calendar.TUESDAY:
                assertTrue(tickets.size() == 3);
                break;
            case Calendar.WEDNESDAY:
                assertTrue(tickets.size() == 4);
                break;
            case Calendar.THURSDAY:
                assertTrue(tickets.size() == 5);
                break;
            case Calendar.FRIDAY:
                assertTrue(tickets.size() == 6);
                break;
            case Calendar.SATURDAY:
                assertTrue(tickets.size() == 7);
                break;
        }
    }

    private void createDummyTickets() {
        if (dummyTicketsCreated) return;

        createDummyTicket(new Date());
        createDummyTicket(DateUtils.addDays(new Date(), -1));
        createDummyTicket(DateUtils.addDays(new Date(), -2));
        createDummyTicket(DateUtils.addDays(new Date(), -3));
        createDummyTicket(DateUtils.addDays(new Date(), -4));
        createDummyTicket(DateUtils.addDays(new Date(), -5));
        createDummyTicket(DateUtils.addDays(new Date(), -6));
        createDummyTicket(DateUtils.addDays(new Date(), -7));
        createDummyTicket(DateUtils.addDays(new Date(), -8));
        createDummyTicket(DateUtils.addDays(new Date(), -9));
        createDummyTicket(DateUtils.addDays(new Date(), -10));
        createDummyTicket(DateUtils.addDays(new Date(), -11));
        createDummyTicket(DateUtils.addDays(new Date(), -12));
        createDummyTicket(DateUtils.addDays(new Date(), -13));
        dummyTicketsCreated = true;
    }

    private void createDummyTicket(Date createdDate) {
        Ticket t = new Ticket();
        t.setCreatedDate(createdDate);
        ticketDao.saveServiceTicket(t);
        logger.debug("created dummy ticket " + t.getId());
    }

    public void setUserDao(HibernateUserDao userDao) {
        this.userDao = userDao;
    }

    public void setHibernateTicketFilterDao(HibernateTicketFilterDao hibernateTicketFilterDao) {
        this.hibernateTicketFilterDao = hibernateTicketFilterDao;
    }

    public void setTicketDao(TicketDao ticketDao) {
        this.ticketDao = ticketDao;
    }
}
