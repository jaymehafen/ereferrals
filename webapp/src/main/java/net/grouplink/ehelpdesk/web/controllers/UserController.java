package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Date: Jul 23, 2007
 * Time: 1:57:32 PM
 */
public class UserController extends MultiActionController {

    private UserService userService;

    public ModelAndView userDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        User loggedInUser = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        if (!loggedInUser.getId().equals(User.ADMIN_ID)) {
            throw new RuntimeException("Delete User Access Denied");
        }

        String userId = ServletRequestUtils.getStringParameter(request, "userId");
        if (StringUtils.isNotBlank(userId)) {
            User user = userService.getUserById(Integer.valueOf(userId));

            if (user != null) {                                                   
                boolean deleted = userService.deleteUser(user);
                if (deleted) {
                    return new ModelAndView("message", "message",
                            getMessageSourceAccessor().getMessage("user.delete.confirm"));
                } else {
                    return new ModelAndView("message", "message",
                            getMessageSourceAccessor().getMessage("user.delete.unabletoDelete"));
                }
            }
        }
        
        return new ModelAndView("message", "message", "User doesn't exist or has been deleted already.");
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
