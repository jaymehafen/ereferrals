package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class UserFormatter implements Formatter<User> {
    @Autowired
    private UserService userService;

    public User parse(String s, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(s)) {
            return userService.getUserById(Integer.valueOf(s));
        } else {
            return null;
        }
    }

    public String print(User user, Locale locale) {
        return user == null ? "" : user.getId().toString();
    }
}
