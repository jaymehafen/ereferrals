package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.service.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 3/19/12
 * Time: 2:38 PM
 */
public class AssignmentConverter implements Converter<String, Assignment> {
    @Autowired private AssignmentService assignmentService;
    
    public Assignment convert(String id){
        try{
            int assId = Integer.parseInt(id);
            return assignmentService.getById(assId);
        } catch (Exception e){
            return null;
        }
    }
}
