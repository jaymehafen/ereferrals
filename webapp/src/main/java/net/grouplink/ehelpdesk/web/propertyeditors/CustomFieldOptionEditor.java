package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.CustomFieldOption;
import net.grouplink.ehelpdesk.service.CustomFieldOptionsService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author mrollins
 * @version 1.0
 */
public class CustomFieldOptionEditor extends PropertyEditorSupport {

    private CustomFieldOptionsService customFieldOptionsService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public CustomFieldOptionEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     *
     * @param customFieldOptionsService the service
     */
    public CustomFieldOptionEditor(CustomFieldOptionsService customFieldOptionsService) {
        this.customFieldOptionsService = customFieldOptionsService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        CustomFieldOption customFieldOption = (CustomFieldOption) getValue();
        return customFieldOption != null ? customFieldOption.getId().toString() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)) {
            setValue(customFieldOptionsService.getCustomFieldOptions(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setCustomFieldOptionsService(CustomFieldOptionsService customFieldOptionsService) {
        this.customFieldOptionsService = customFieldOptionsService;
    }
}
