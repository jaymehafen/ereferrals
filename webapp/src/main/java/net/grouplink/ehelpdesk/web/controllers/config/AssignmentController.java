package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.domain.filter.AssignmentFilter;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.AssignmentService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 3/13/12
 * Time: 2:51 PM
 */

@Controller
@RequestMapping(value = "/assignments")
public class AssignmentController extends ApplicationObjectSupport {

    public static final String AC_ASSIGNMENT_FILTER = "AC_ASSIGNMENT_FILTER";

    @Autowired
    private LocationService locationService;
    @Autowired
    private AssignmentService assignmentService;
    @Autowired
    private UserRoleGroupService userRoleGroupService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryOptionService categoryOptionService;

    @RequestMapping(method = RequestMethod.GET)
    public String indexAssignments(Model model, @ModelAttribute AssignmentFilter assignmentFilter, BindingResult bindingResult,
                                   HttpServletRequest request, @RequestParam(value="filterApplied", required=false) boolean filterApplied) {

        request.getSession().setAttribute(AC_ASSIGNMENT_FILTER, assignmentFilter);
        model.addAttribute("assignmentFilter", assignmentFilter);
        model.addAttribute("locations", locationService.getLocations());
        model.addAttribute("filterApplied", filterApplied);
        return "assignments/index";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") Assignment assignment, HttpServletResponse response) {

        boolean deleted = assignmentService.delete(assignment);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/assignmentStore")
    public void getAssignmentStore(@RequestParam(value = "sort", required = false) String sort,
                                   @RequestParam(value = "start", required = false) Integer start,
                                   @RequestParam(value = "count", required = false) Integer count,
                                   @RequestParam(value = "label", required = false) String label,
                                   HttpServletRequest request, HttpServletResponse response)
            throws IOException, JSONException {
        String queryParam = StringUtils.replace(label, "*", "");

        AssignmentFilter assignmentFilter = (AssignmentFilter) request.getSession().getAttribute(AC_ASSIGNMENT_FILTER);

        if (assignmentFilter == null) {
            assignmentFilter = new AssignmentFilter();
        }

        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
        int numRows;

        // Get all URGs for the filter's assignment id. If prefixed with 'urg_' then that is the only one. If
        // prefixed 'usr_' then get all URGs for that user
        List<UserRoleGroup> urgs = new ArrayList<UserRoleGroup>();
        if (StringUtils.isNotBlank(assignmentFilter.getAssignment())) {
            if (assignmentFilter.getAssignment().startsWith("urg_")) {
                Integer urgId = Integer.parseInt(assignmentFilter.getAssignment().replace("urg_", ""));
                urgs.add(userRoleGroupService.getUserRoleGroupById(urgId));
            } else { // starts with usr_ followed by the user id
                Integer userId = Integer.parseInt(assignmentFilter.getAssignment().replace("usr_", ""));
                urgs = userRoleGroupService.getUserRoleGroupByUserId(userId);
            }
        }
        assignmentFilter.setUserRoleGroupList(urgs);

        // Set up sorting
        Map<String, Boolean> sortFieldMap = new HashMap<String, Boolean>();
        if (StringUtils.isNotBlank(sort)) {
            boolean direction = !StringUtils.startsWith(sort, "-");
            sortFieldMap.put(sort.replace("-", ""), direction);
        } else {
            sortFieldMap.put("userRoleGroup", true);
        }

        List<Assignment> assignments = assignmentService.getAssignments(assignmentFilter,
                queryParam, start, count, sortFieldMap);
        numRows = assignmentService.getAssignmentCount(assignmentFilter, queryParam);

        for (Assignment assignment : assignments) {
            JSONObject assItem = new JSONObject();
            assItem.put("assId", assignment.getId());

            // do the ticket pool or tech name thing
            String urgLabel;
            if (assignment.getUserRoleGroup().getUserRole() != null) {
                urgLabel = assignment.getUserRoleGroup().getUserRole().getUser().getFirstName() + " " +
                        assignment.getUserRoleGroup().getUserRole().getUser().getLastName() + " - " +
                        assignment.getUserRoleGroup().getGroup().getName();
            } else {
                urgLabel = getMessageSourceAccessor().getMessage("ticketList.ticketPool") + " - " +
                        assignment.getUserRoleGroup().getGroup().getName();
            }
            assItem.put("userRoleGroup", urgLabel);
            assItem.put("location", assignment.getLocation() == null ? "" : assignment.getLocation().getName());
            if (assignment.getCategoryOption() != null) {
                assItem.put("categoryOption", assignment.getCategoryOption() == null ? "" : assignment.getCategoryOption().getName());
                assItem.put("category", "(" + assignment.getCategoryOption().getCategory().getName() + ")");
                assItem.put("group", "(" + assignment.getCategoryOption().getCategory().getGroup().getName() + ")");
            } else if (assignment.getCategory() != null) {
                assItem.put("categoryOption", "");
                assItem.put("category", assignment.getCategory().getName());
                assItem.put("group", "(" + assignment.getCategory().getGroup().getName() + ")");
            } else if (assignment.getGroup() != null) {
                assItem.put("categoryOption", "");
                assItem.put("category", "");
                assItem.put("group", assignment.getGroup().getName());
            }

            jsonObjects.add(assItem);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "assId");
        store.put("numRows", numRows);
        store.put("items", jsonObjects);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/assignmentStore/group/{groupId}")
    public void getAssignmentForGroup(@PathVariable("groupId") Group group, HttpServletResponse response) throws IOException, JSONException {

        List<UserRoleGroup> userRoleGroups = userRoleGroupService.getManagerTechnicianUserRoleGroupByGroupId(group);

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (userRoleGroups.size() > 1) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        String label;
        String ticketPoolLabel = getMessageSourceAccessor().getMessage("ticketList.ticketPool");
        for (UserRoleGroup userRoleGroup : userRoleGroups) {
            label = ticketPoolLabel;
            if (userRoleGroup.getUserRole() != null) {
                label = userRoleGroup.getUserRole().getUser().getLastName() + ", " + userRoleGroup.getUserRole().getUser().getFirstName();
            }

            JSONObject item = new JSONObject();
            item.put("id", userRoleGroup.getId());
            item.put("label", label);
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newForm(Model model) {
        model.addAttribute("locations", locationService.getLocations());
        model.addAttribute("groups", groupService.getGroups());
        model.addAttribute("categories", categoryService.getCategories());
        return "assignments/form";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/new")
    public String createAssignment(@RequestParam(value = "technician") UserRoleGroup urg, @RequestParam(value = "location", required = false) Integer[] locationIds,
                                   @RequestParam(value = "category", required = false) Integer[] categoryIds, @RequestParam(value = "categoryOption", required = false) Integer[] categoryOptionIds,
                                   @RequestParam("assignmentType") String assignmentType, HttpServletRequest request) throws ServletRequestBindingException {

        if (urg == null) {
            StringBuilder errorMsg = new StringBuilder(getMessageSourceAccessor().getMessage("assignment.urg.req"));
            request.setAttribute("flash.error", errorMsg);
            return "redirect:/config/assignments/new";
        }

        int createdCount;

        if (assignmentType.equals("group")) {
            createdCount = createGroupAssignment(urg, locationIds);
        } else if (assignmentType.equals("category")) {
            if (ArrayUtils.isEmpty(categoryIds)) {
                request.setAttribute("flash.error", getMessageSourceAccessor().getMessage("assignment.new.noCategorySelected"));
                return "redirect:/config/assignments/new";
            }

            createdCount = createCatAssignment(urg, locationIds, categoryIds);
        } else if (assignmentType.equals("categoryOption")) {
            if (ArrayUtils.isEmpty(categoryOptionIds)) {
                request.setAttribute("flash.error", getMessageSourceAccessor().getMessage("assignment.new.noCategoryOptionSelected"));
                return "redirect:/config/assignments/new";
            }

            createdCount = createCatOptAssignment(urg, locationIds, categoryOptionIds);
        } else {
            request.setAttribute("flash.error", "unknown assignment type: " + assignmentType);
            return "redirect:/config/assignments/new";
        }

        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("assignment.save.success", new Object[]{createdCount}));
        return "redirect:/config/assignments/new";
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/massDelete")
    public void massDelete(@RequestParam("assignmentIds[]") Integer[] assIds, HttpServletResponse response) {
        for (Integer assId : assIds) {
            Assignment a = assignmentService.getById(assId);
            assignmentService.delete(a);
        }

        response.setStatus(HttpServletResponse.SC_OK);
    }

    private int createCatOptAssignment(UserRoleGroup urg, Integer[] locationIds, Integer[] categoryOptionIds) {
        int createdCount = 0;
        Assignment assignment;
        for (Integer catOptId : categoryOptionIds) {
            CategoryOption catOpt = categoryOptionService.getCategoryOptionById(catOptId);
            if (ArrayUtils.isEmpty(locationIds)) {
                assignment = buildAssignment(urg, null, null, catOpt, null);
                assignmentService.save(assignment);
                createdCount++;
            } else {
                for (Integer locationId : locationIds) {
                    Location loc = locationService.getLocationById(locationId);
                    assignment = buildAssignment(urg, null, null, catOpt, loc);
                    assignmentService.save(assignment);
                    createdCount++;
                }
            }
        }
        return createdCount;
    }

    private int createCatAssignment(UserRoleGroup urg, Integer[] locationIds, Integer[] categoryIds) {
        int createdCount = 0;
        Assignment assignment;
        for (Integer categoryId : categoryIds) {
            Category category = categoryService.getCategoryById(categoryId);
            if (ArrayUtils.isEmpty(locationIds)) {
                assignment = buildAssignment(urg, null, category, null, null);
                assignmentService.save(assignment);
                createdCount++;
            } else {
                for (Integer locationId : locationIds) {
                    Location loc = locationService.getLocationById(locationId);
                    assignment = buildAssignment(urg, null, category, null, loc);
                    assignmentService.save(assignment);
                    createdCount++;
                }
            }

        }
        return createdCount;
    }

    private int createGroupAssignment(UserRoleGroup urg, Integer[] locationIds) {
        int createdCount = 0;
        Assignment assignment;

        if (ArrayUtils.isEmpty(locationIds)) {
            // no loc
            assignment = buildAssignment(urg, urg.getGroup(), null, null, null);
            assignmentService.save(assignment);
            createdCount++;
        } else {
            // has loc
            for (Integer locationId : locationIds) {
                Location loc = locationService.getLocationById(locationId);
                assignment = buildAssignment(urg, urg.getGroup(), null, null, loc);
                assignmentService.save(assignment);
                createdCount++;
            }
        }

        return createdCount;
    }

    private Assignment buildAssignment(UserRoleGroup urg, Group group, Category category, CategoryOption categoryOption, Location location) {
        Assignment a = new Assignment();
        a.setUserRoleGroup(urg);
        a.setGroup(group);
        a.setCategory(category);
        a.setCategoryOption(categoryOption);
        a.setLocation(location);
        return a;
    }
}
