package net.grouplink.ehelpdesk.web.controllers.config.acl;

import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;

import java.util.ArrayList;
import java.util.List;

public class PermissionActorsForm {
    private List<User> users = new ArrayList<User>();
    private List<Role> roles = new ArrayList<Role>();

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
