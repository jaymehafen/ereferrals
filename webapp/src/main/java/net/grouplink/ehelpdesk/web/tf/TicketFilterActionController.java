package net.grouplink.ehelpdesk.web.tf;

import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketAudit;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;
import net.grouplink.ehelpdesk.service.AssetTrackerZen10Service;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.SurveyQuestionService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import net.grouplink.ehelpdesk.web.views.TicketFilterListExcelView;
import net.grouplink.ehelpdesk.web.views.TicketFilterListPdfView;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class TicketFilterActionController extends MultiActionController {
    private Log log = LogFactory.getLog(getClass());

    private TicketPriorityService ticketPriorityService;
    private StatusService statusService;
    private UserService userService;
    private UserRoleGroupService userRoleGroupService;
    private LocationService locationService;
    private GroupService groupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private SurveyQuestionService surveyQuestionService;
    private ZenAssetService zenAssetService;
    private TicketFilterService ticketFilterService;
    private TicketFilterListPdfView ticketFilterListPdfView;
    private TicketFilterListExcelView ticketFilterListExcelView;
    private TicketService ticketService;
    private AssetService assetService;
    private AssetTrackerZen10Service assetTrackerZen10Service;
    private PermissionService permissionService;

    public ModelAndView deleteTicketFilter(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer tfId = ServletRequestUtils.getIntParameter(request, "tfId");
        TicketFilter ticketFilter = ticketFilterService.getById(tfId);

        // check permission
        if (!hasPermission(ticketFilter)) {
            throw new AccessDeniedException("Access denied");
        }

        // check if ticketfilter is in use
        if (ticketFilter != null) {
            if (ticketFilterService.getUsageCount(ticketFilter) > 0) {
                // can't delete
                request.setAttribute("cannotDelete", "true");
            } else {
                ticketFilterService.delete(ticketFilter);
                ticketFilterService.flush();
                request.setAttribute("deleted", "true");
            }
        }
        return showTicketFilterList(request, response);
    }

    private boolean hasPermission(TicketFilter ticketFilter) {
        boolean showSaveButton = true;
        if (ticketFilter.getId() != null &&
                !ticketFilter.getUser().equals(userService.getLoggedInUser()) &&
                !ticketFilter.getPrivateFilter()) {
            // check permission
            showSaveButton = permissionService.hasGlobalPermission("global.publicTicketFilterManagement");
        }
        return showSaveButton;
    }

    public ModelAndView showTicketFilterResults(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer tfId = ServletRequestUtils.getIntParameter(request, "tfId");

        TicketFilter ticketFilter;
        if (tfId == null) {
            ticketFilter =
                    (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);
        } else {
            ticketFilter = ticketFilterService.getById(tfId);
        }
        
        User user = userService.getLoggedInUser();

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("tfId", tfId);
        model.put("ticketFilter", ticketFilter);
        model.put("priorities", ticketPriorityService.getPriorities());
        model.put("status", statusService.getStatus());
        model.put("ticketCount", ticketFilterService.getTicketCount(ticketFilter, user));

        return new ModelAndView("ticketFilterResults", model);
    }

    public ModelAndView addItem(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Get the command object from the session
        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        // get the type and value to be added.
        String type = ServletRequestUtils.getStringParameter(request, "type");
        String value = ServletRequestUtils.getStringParameter(request, "value");
        Integer opValue = ServletRequestUtils.getIntParameter(request, "opValue", 9);

        String htmlTable = "";
        String ctxtPath = request.getContextPath();

        if (type.equals("ticketNumbers")) {
            Integer tickNum = null;
            Integer[] items = ticketFilter.getTicketNumbers();
            Integer[] opItems = ticketFilter.getTicketNumbersOps();

            try {
                tickNum = new Integer(value);
            } catch (NumberFormatException nfe) {
                logger.debug("Exception while formating ticket number", nfe);
            }

            if (tickNum == null && items == null) {
                return null;
            }
            if (tickNum != null) {
                if (!ArrayUtils.contains(ticketFilter.getTicketNumbers(), tickNum)) {
                    items = (Integer[]) ArrayUtils.add(ticketFilter.getTicketNumbers(), new Integer(value));
                    ticketFilter.setTicketNumbers(items);

                    opItems = (Integer[]) ArrayUtils.add(ticketFilter.getTicketNumbersOps(), opValue);
                    ticketFilter.setTicketNumbersOps(opItems);
                }
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("priorityIds")) {
            Integer[] items = ticketFilter.getPriorityIds();
            Integer[] opItems = ticketFilter.getPriorityIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getPriorityIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getPriorityIds(), new Integer(value));
                ticketFilter.setPriorityIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getPriorityIdsOps(), opValue);
                ticketFilter.setPriorityIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("statusIds")) {
            Integer[] items = ticketFilter.getStatusIds();
            Integer[] opItems = ticketFilter.getStatusIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getStatusIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getStatusIds(), new Integer(value));
                ticketFilter.setStatusIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getStatusIdsOps(), opValue);
                ticketFilter.setStatusIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("contactIds")) {
            Integer[] items = ticketFilter.getContactIds();
            Integer[] opItems = ticketFilter.getContactIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getContactIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getContactIds(), new Integer(value));
                ticketFilter.setContactIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getContactIdsOps(), opValue);
                ticketFilter.setContactIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("submittedByIds")) {
            Integer[] items = ticketFilter.getSubmittedByIds();
            Integer[] opItems = ticketFilter.getSubmittedByIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getSubmittedByIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getSubmittedByIds(), new Integer(value));
                ticketFilter.setSubmittedByIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getSubmittedByIdsOps(), opValue);
                ticketFilter.setSubmittedByIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("assignedToIds")) {
            Integer[] urg_items = ticketFilter.getAssignedToIds();
            Integer[] urg_opItems = ticketFilter.getAssignedToIdsOps();

            Integer[] usr_items = ticketFilter.getAssignedToUserIds();
            Integer[] opItems = ticketFilter.getAssignedToUserIdsOps();

            if(value.startsWith("urg_")){


                value = value.substring(value.indexOf("urg_")+4);

                if (!ArrayUtils.contains(ticketFilter.getAssignedToIds(), new Integer(value))) {
                    urg_items = (Integer[]) ArrayUtils.add(ticketFilter.getAssignedToIds(), new Integer(value));
                    ticketFilter.setAssignedToIds(urg_items);

                    urg_opItems = (Integer[]) ArrayUtils.add(ticketFilter.getAssignedToIdsOps(), opValue);
                    ticketFilter.setAssignedToIdsOps(urg_opItems);
                }
            } else {
                value = value.substring(value.indexOf("usr_")+4);

                if (!ArrayUtils.contains(ticketFilter.getAssignedToUserIds(), new Integer(value))) {
                    usr_items = (Integer[]) ArrayUtils.add(ticketFilter.getAssignedToUserIds(), new Integer(value));
                    ticketFilter.setAssignedToUserIds(usr_items);

                    opItems = (Integer[]) ArrayUtils.add(ticketFilter.getAssignedToUserIdsOps(), opValue);
                    ticketFilter.setAssignedToUserIdsOps(opItems);
                }
            }

            htmlTable = createAssignedToHtmlTable(urg_items, urg_opItems, usr_items, opItems, ctxtPath);

        } else if (type.equals("locationIds")) {
            Integer[] items = ticketFilter.getLocationIds();
            Integer[] opItems = ticketFilter.getLocationIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getLocationIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getLocationIds(), new Integer(value));
                ticketFilter.setLocationIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getLocationIdsOps(), opValue);
                ticketFilter.setLocationIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("groupIds")) {
            Integer[] items = ticketFilter.getGroupIds();
            Integer[] opItems = ticketFilter.getGroupIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getGroupIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getGroupIds(), new Integer(value));
                ticketFilter.setGroupIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getGroupIdsOps(), opValue);
                ticketFilter.setGroupIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("categoryIds")) {
            Integer[] items = ticketFilter.getCategoryIds();
            Integer[] opItems = ticketFilter.getCategoryIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getCategoryIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryIds(), new Integer(value));
                ticketFilter.setCategoryIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryIdsOps(), opValue);
                ticketFilter.setCategoryIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("categoryOptionIds")) {
            Integer[] items = ticketFilter.getCategoryOptionIds();
            Integer[] opItems = ticketFilter.getCategoryOptionIdsOps();
            if (!ArrayUtils.contains(ticketFilter.getCategoryOptionIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryOptionIds(), new Integer(value));
                ticketFilter.setCategoryOptionIds(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryOptionIdsOps(), opValue);
                ticketFilter.setCategoryOptionIdsOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("subject")) {
            String[] items = ticketFilter.getSubject();
            Integer[] opItems = ticketFilter.getSubjectOps();
            if (!ArrayUtils.contains(ticketFilter.getSubject(), value)) {
                items = (String[]) ArrayUtils.add(ticketFilter.getSubject(), value);
                ticketFilter.setSubject(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getSubjectOps(), opValue);
                ticketFilter.setSubjectOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("note")) {
            String[] items = ticketFilter.getNote();
            Integer[] opItems = ticketFilter.getNoteOps();
            if (!ArrayUtils.contains(ticketFilter.getNote(), value)) {
                items = (String[]) ArrayUtils.add(ticketFilter.getNote(), value);
                ticketFilter.setNote(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getNoteOps(), opValue);
                ticketFilter.setNoteOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("createdDate")) {

        } else if (type.equals("modifiedDate")) {

        } else if (type.equals("estimatedDate")) {

        } else if (type.equals("workTime")) {

        } else if (type.equals("historySubject")) {
            String[] items = ticketFilter.getHistorySubject();
            Integer[] opItems = ticketFilter.getHistorySubjectOps();
            if (!ArrayUtils.contains(ticketFilter.getHistorySubject(), value)) {
                items = (String[]) ArrayUtils.add(ticketFilter.getHistorySubject(), value);
                ticketFilter.setHistorySubject(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getHistorySubjectOps(), opValue);
                ticketFilter.setHistorySubjectOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("historyNote")) {
            String[] items = ticketFilter.getHistoryNote();
            Integer[] opItems = ticketFilter.getHistoryNoteOps();
            if (!ArrayUtils.contains(ticketFilter.getHistoryNote(), value)) {
                items = (String[]) ArrayUtils.add(ticketFilter.getHistoryNote(), value);
                ticketFilter.setHistoryNote(items);

                opItems = (Integer[]) ArrayUtils.add(ticketFilter.getHistoryNoteOps(), opValue);
                ticketFilter.setHistoryNoteOps(opItems);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("zenAssetIds")) {
            Integer[] items = ticketFilter.getZenAssetIds();
            if (!ArrayUtils.contains(ticketFilter.getZenAssetIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getZenAssetIds(), new Integer(value));
                ticketFilter.setZenAssetIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), null, ctxtPath);
        } else if (type.equals("internalAssetIds")){
            Integer[] items = ticketFilter.getInternalAssetIds();
            if (!ArrayUtils.contains(ticketFilter.getInternalAssetIds(), new Integer(value))) {
                items = (Integer[]) ArrayUtils.add(ticketFilter.getInternalAssetIds(), new Integer(value));
                ticketFilter.setInternalAssetIds(items);
            }
            htmlTable = createHtmlTable(type, Arrays.asList(items), null, ctxtPath);
        }

        response.getWriter().print(htmlTable);
        return null;
    }

    public ModelAndView deleteItem(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // Get the command object from the session
        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        // get the type and value to be deleted.
        String type = ServletRequestUtils.getStringParameter(request, "type");
        int row = ServletRequestUtils.getIntParameter(request, "row", -1);

        String htmlTable = "";
        String ctxtPath = request.getContextPath();

        // remove items from the arrays
        if (type.equals("ticketNumbers")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getTicketNumbers(), row);
            ticketFilter.setTicketNumbers(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getTicketNumbersOps(), row);
            ticketFilter.setTicketNumbersOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("priorityIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getPriorityIds(), row);
            ticketFilter.setPriorityIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getPriorityIdsOps(), row);
            ticketFilter.setPriorityIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("statusIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getStatusIds(), row);
            ticketFilter.setStatusIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getStatusIdsOps(), row);
            ticketFilter.setStatusIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("contactIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getContactIds(), row);
            ticketFilter.setContactIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getContactIdsOps(), row);
            ticketFilter.setContactIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("submittedByIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getSubmittedByIds(), row);
            ticketFilter.setSubmittedByIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getSubmittedByIdsOps(), row);
            ticketFilter.setSubmittedByIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.startsWith("assignedTo")) {

            Integer[] urg_items = ticketFilter.getAssignedToIds();
            Integer[] urg_opitems = ticketFilter.getAssignedToIdsOps();

            Integer[] usr_items = ticketFilter.getAssignedToUserIds();
            Integer[] usr_opitems = ticketFilter.getAssignedToUserIdsOps();

            if(type.equals("assignedToIds")) {
                urg_items = (Integer[]) ArrayUtils.remove(urg_items, row);
                urg_opitems = (Integer[]) ArrayUtils.remove(urg_opitems, row);
                ticketFilter.setAssignedToIds(urg_items);
                ticketFilter.setAssignedToIdsOps(urg_opitems);
            } else {
                usr_items = (Integer[]) ArrayUtils.remove(usr_items, row);
                usr_opitems = (Integer[]) ArrayUtils.remove(usr_opitems, row);
                ticketFilter.setAssignedToUserIds(usr_items);
                ticketFilter.setAssignedToUserIdsOps(usr_opitems);
            }

            htmlTable = createAssignedToHtmlTable(urg_items, urg_opitems, usr_items, usr_opitems, ctxtPath);
        } else if (type.equals("locationIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getLocationIds(), row);
            ticketFilter.setLocationIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getLocationIdsOps(), row);
            ticketFilter.setLocationIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("groupIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getGroupIds(), row);
            ticketFilter.setGroupIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getGroupIdsOps(), row);
            ticketFilter.setGroupIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("categoryIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getCategoryIds(), row);
            ticketFilter.setCategoryIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getCategoryIdsOps(), row);
            ticketFilter.setCategoryIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("categoryOptionIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getCategoryOptionIds(), row);
            ticketFilter.setCategoryOptionIds(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getCategoryOptionIdsOps(), row);
            ticketFilter.setCategoryOptionIdsOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("subject")) {
            String[] items = (String[]) ArrayUtils.remove(ticketFilter.getSubject(), row);
            ticketFilter.setSubject(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getSubjectOps(), row);
            ticketFilter.setSubjectOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("note")) {
            String[] items = (String[]) ArrayUtils.remove(ticketFilter.getNote(), row);
            ticketFilter.setNote(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getNoteOps(), row);
            ticketFilter.setNoteOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("cflds")) {
            String[] cfnames = (String[]) ArrayUtils.remove(ticketFilter.getCustomFieldName(), row);
            String[] cfvals = (String[]) ArrayUtils.remove(ticketFilter.getCustomFieldValue(), row);
            ticketFilter.setCustomFieldName(cfnames);
            ticketFilter.setCustomFieldValue(cfvals);
            htmlTable = createHtmlTable(type, Arrays.asList(cfnames), Arrays.asList(cfvals), null, ctxtPath);
        } else if (type.equals("historySubject")) {
            String[] items = (String[]) ArrayUtils.remove(ticketFilter.getHistorySubject(), row);
            ticketFilter.setHistorySubject(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getHistorySubjectOps(), row);
            ticketFilter.setHistorySubjectOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("historyNote")) {
            String[] items = (String[]) ArrayUtils.remove(ticketFilter.getHistoryNote(), row);
            ticketFilter.setHistoryNote(items);

            Integer[] opItems = (Integer[]) ArrayUtils.remove(ticketFilter.getHistoryNoteOps(), row);
            ticketFilter.setHistoryNoteOps(opItems);

            htmlTable = createHtmlTable(type, Arrays.asList(items), Arrays.asList(opItems), ctxtPath);
        } else if (type.equals("surveys")) {
            String[] surveyIds = (String[]) ArrayUtils.remove(ticketFilter.getSurveyQuestionId(), row);
            String[] surveyVals = (String[]) ArrayUtils.remove(ticketFilter.getSurveyQuestionValue(), row);
            ticketFilter.setSurveyQuestionId(surveyIds);
            ticketFilter.setSurveyQuestionValue(surveyVals);
            htmlTable = createHtmlTable(type, Arrays.asList(surveyIds), Arrays.asList(surveyVals), null, ctxtPath);
        } else if (type.equals("zenAssetIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getZenAssetIds(), row);
            ticketFilter.setZenAssetIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), null, ctxtPath);
        } else if (type.equals("internalAssetIds")) {
            Integer[] items = (Integer[]) ArrayUtils.remove(ticketFilter.getInternalAssetIds(), row);
            ticketFilter.setInternalAssetIds(items);
            htmlTable = createHtmlTable(type, Arrays.asList(items), null, ctxtPath);
        }

        response.getWriter().print(htmlTable);
        return null;
    }

    public ModelAndView showTicketFilterList(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();

        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());

        // get list of user filters
        List<TicketFilter> userFilters = ticketFilterService.getByUser(user);

        // get list of public filters
        List<TicketFilter> publicFilters = ticketFilterService.getPublicMinusUser(user);

        // check if the tf's are being used
        Map<TicketFilter, Integer> filterUsage = new HashMap<TicketFilter, Integer>();
        for (TicketFilter userFilter : userFilters) {
            int usageCount = ticketFilterService.getUsageCount(userFilter);
            filterUsage.put(userFilter, usageCount);
        }

        for (TicketFilter publicFilter : publicFilters) {
            int usageCount = ticketFilterService.getUsageCount(publicFilter);
            filterUsage.put(publicFilter,  usageCount);
        }

        model.put("userFilters", userFilters);
        model.put("publicFilters", publicFilters);
        model.put("filterUsage", filterUsage);

        return new ModelAndView("ticketFilterList", model);
    }


    public ModelAndView ticketFilterStore(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        HttpSession session = request.getSession(false);

        TicketFilter ticketFilter;
        Locale locale = RequestContextUtils.getLocale(request);

        // get start and count params from request
        int start = ServletRequestUtils.getIntParameter(request, "start", 0);
        int count = ServletRequestUtils.getIntParameter(request, "count", -1);
        int ticketFilterId = ServletRequestUtils.getIntParameter(request, "ticketFilterId", -1);
        String sort = ServletRequestUtils.getStringParameter(request, "sort");
        boolean showClosed = ServletRequestUtils.getBooleanParameter(request, "showClosed", false);
        int findUserContactId = ServletRequestUtils.getIntParameter(request, "findUserContactId", -2);

        if (ticketFilterId > -1) {
            ticketFilter = ticketFilterService.getById(ticketFilterId);
        } else if (findUserContactId > -2) {
            // create temporary ticket filter for finduser screen
            ticketFilter = ticketFilterService.getFindUserTicketFilter(findUserContactId);
        } else {
            // get the ticket filter from the session
            ticketFilter = (TicketFilter) session.getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);
        }

        if (showClosed) {
            // removed status <> closed criteria if it exists
            Status closedStatus = statusService.getClosedStatus();
            int closedStatusIndex = ArrayUtils.indexOf(ticketFilter.getStatusIds(), closedStatus.getId());
            if (closedStatusIndex > -1 &&
                    ticketFilter.getStatusIdsOps()[closedStatusIndex].equals(TicketFilterOperators.NOT_EQUALS)) {
                ticketFilter.setStatusIds((Integer[]) ArrayUtils.remove(ticketFilter.getStatusIds(), closedStatusIndex));
                ticketFilter.setStatusIdsOps((Integer[]) ArrayUtils.remove(ticketFilter.getStatusIdsOps(), closedStatusIndex));
            }
        }

        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());
        Integer ticketCount = ticketFilterService.getTicketCount(ticketFilter, user);
        List<Ticket> tickets = ticketFilterService.getTickets(ticketFilter, user, sort, start, count);

        String baseUrl = getBaseUrl(request);
        List<JSONObject> items = new ArrayList<JSONObject>();
        for (Ticket ticket : tickets) {
            try {
                items.add(getTicketJsonObject(ticket, locale, ticketFilter, user, baseUrl));
            } catch (JSONException e) {
                log.error("can't get json object for ticket " + ticket.getId(), e);
            }
        }

        JSONObject store = new JSONObject();
        try {
            store.put("identifier", TicketFilter.COLUMN_TICKETNUMBER);
            store.put("numRows", ticketCount);
            store.put("items", items);
        } catch (JSONException e) {
            log.error("error setting store values", e);
        }

        response.setContentType("application/json");
        try {
            response.getWriter().print(store.toString());
        } catch (IOException e) {
            log.error("error writing response", e);
        }

        return null;
    }

    private String getBaseUrl(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder()
                .append(request.getScheme())
                .append("://")
                .append(request.getServerName());
        if (request.getServerPort() != 80)
            sb.append(":").append(request.getServerPort());

        sb.append(request.getContextPath());
        return sb.toString();
    }

    public ModelAndView showTicketFilterAsPDF(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer ticketFilterId = ServletRequestUtils.getIntParameter(request, "tfId", -1);

        TicketFilter ticketFilter;
        if (ticketFilterId > -1) {
            ticketFilter = ticketFilterService.getById(ticketFilterId);
        } else {
            ticketFilter = (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);
        }

        String selectedTids = ServletRequestUtils.getStringParameter(request, "selectedTids");
        if (StringUtils.isNotBlank(selectedTids)) {
            String[] tids = selectedTids.split(",");
            for (String tid : tids) {
                ticketFilter.setTicketNumbers((Integer[]) ArrayUtils.add(ticketFilter.getTicketNumbers(), Integer.valueOf(tid)));
                ticketFilter.setTicketNumbersOps((Integer[]) ArrayUtils.add(ticketFilter.getTicketNumbersOps(), TicketFilterOperators.EQUALS));
            }
        }

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        List<Ticket> ticketList = ticketFilterService.getTickets(ticketFilter, user);

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("ticketFilter", ticketFilter);
        model.put("ticketList", ticketList);

        return new ModelAndView(ticketFilterListPdfView, model);
    }

    public ModelAndView showTicketFilterAsExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer ticketFilterId = ServletRequestUtils.getIntParameter(request, "tfId", -1);

        TicketFilter ticketFilter;
        if (ticketFilterId > -1) {
            ticketFilter = ticketFilterService.getById(ticketFilterId);
        } else {
            ticketFilter = (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);
        }

        String selectedTids = ServletRequestUtils.getStringParameter(request, "selectedTids");
        if (StringUtils.isNotBlank(selectedTids)) {
            String[] tids = selectedTids.split(",");
            for (String tid : tids) {
                ticketFilter.setTicketNumbers((Integer[]) ArrayUtils.add(ticketFilter.getTicketNumbers(), Integer.valueOf(tid)));
                ticketFilter.setTicketNumbersOps((Integer[]) ArrayUtils.add(ticketFilter.getTicketNumbersOps(), TicketFilterOperators.EQUALS));
            }
        }

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        List<Ticket> ticketList = ticketFilterService.getTickets(ticketFilter, user);

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("ticketFilter", ticketFilter);
        model.put("ticketList", ticketList);

        return new ModelAndView(ticketFilterListExcelView, model);
    }

    public ModelAndView fetchCategories(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();
        List<Group> groups = groupService.getGroups();
        model.put("groups", groups);

        return new ModelAndView("categoryFetcher", model);
    }

    public ModelAndView fetchCategoryOptions(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();
        List<Group> groups = groupService.getGroups();
        model.put("groups", groups);

        return new ModelAndView("catOptFetcher", model);
    }

    public ModelAndView fetchZenAssets(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;
        assert response != null;

        Map<String, Object> model = new LinkedHashMap<String, Object>();

        String search = ServletRequestUtils.getStringParameter(request, "search");
        if (StringUtils.isNotBlank(search)) {
            ZenAssetSearch zas = new ZenAssetSearch();
            ServletRequestDataBinder binder = new ServletRequestDataBinder(zas);
            binder.bind(request);
            List<ZenAsset> zenAssets = zenAssetService.search(zas);
            model.put("zenAssets", zenAssets);
            model.put("search", search);
            model.put("zenBaseUrl", assetTrackerZen10Service.getZen10BaseUrl());
        }

        return new ModelAndView("zenAssetFetcher", model);
    }


    // Private Utility methods

    private String createHtmlTable(String propertyName, List items, List opItems, String ctxtPath) {
        String deleteMsg = getMessageSourceAccessor().getMessage("global.delete", "Delete");

        String displayName, operatorName;
        StringBuilder sb = new StringBuilder("<table class=\"filterValues\">");
        for (int i = 0; i < items.size(); i++) {
            displayName = resolveDisplayName(propertyName, items.get(i));
            operatorName = opItems == null ? "" : resolveDisplayName("operator", opItems.get(i));
            sb.append("<tr>")
                    .append("<td>").append(operatorName).append("</td>")
                    .append("<td>").append(displayName).append("</td>")
                    .append("<td class=\"delete\">")
                    .append("  <input type='hidden' name='").append(propertyName).append("' value='").append(items.get(i)).append("'/>")
                    .append("  <a class='deleteItem' href='javascript:void(0)' data-type='").append(propertyName).append("' data-div-id='").append(propertyName).append("' data-row-id='").append(i).append("' title='").append(deleteMsg).append("'>")
                    .append("    <img src='").append(ctxtPath).append("/images/theme/icons/fugue/cross-circle.png' alt=''/>")
                    .append("  </a>")
                    .append("</td>")
                    .append("</tr>");
        }
        sb.append("</table>");
        return sb.toString();
    }

    private String createHtmlTable(String propertyName, List names, List values, List opItems, String ctxtPath) {
        String deleteMsg = getMessageSourceAccessor().getMessage("global.delete", "Delete");

        String displayName, operatorName;
        StringBuilder sb = new StringBuilder("<table class=\"filterValues\">");
        for (int i = 0; i < names.size(); i++) {
            displayName = resolveDisplayName(propertyName, names.get(i));
            operatorName = opItems == null ? "" : resolveDisplayName("operator", opItems.get(i));
            sb.append("<tr>")
                    .append("<td>").append(operatorName).append("</td>")
                    .append("<td>").append(displayName).append("</td>")
                    .append("<td>").append(values.get(i)).append("</td>")
                    .append("<td class=\"delete\">")
                    .append("  <input type='hidden' name='").append(propertyName).append("' value='").append(names.get(i)).append("'/>")
                    .append("  <a class='deleteItem' href='javascript://' data-type='").append(propertyName).append("' data-div-id='").append(propertyName).append("' data-row-id='").append(i).append("' title='").append(deleteMsg).append("'>")
                    .append("    <img src='").append(ctxtPath).append("/images/theme/icons/fugue/cross-circle.png' alt=''/>")
                    .append("  </a>")
                    .append("</td>")
                    .append("</tr>");
        }
        sb.append("</table>");
        return sb.toString();
    }

    private String createAssignedToHtmlTable(Integer[] urg_items, Integer[] urg_opitems, Integer[] usr_items, Integer[] usr_opitems, String contextPath) {
        String deleteMsg = getMessageSourceAccessor().getMessage("global.delete", "Delete");
        String displayName, operatorName;

        StringBuilder sb = new StringBuilder("<table class=\"filterValues\">");
        if(urg_items != null) {
            for (int i = 0; i < urg_items.length; i++) {
                displayName = resolveDisplayName("assignedToIds", urg_items[i]);
                operatorName = urg_opitems == null ? "" : resolveDisplayName("operator", urg_opitems[i]);
                sb.append("<tr>")
                    .append("<td>").append(operatorName).append("</td>")
                    .append("<td>").append(displayName).append("</td>")
                    .append("<td class=\"delete\">")
                    .append("  <input type='hidden' name='assignedToIds' value='").append(urg_items[i]).append("'/>")
                    .append("  <a class='deleteItem' href='javascript:void(0)' data-type='assignedToIds' data-div-id='assignedToIds' data-row-id='").append(i).append("' title='").append(deleteMsg).append("'>")
                    .append("    <img src='").append(contextPath).append("/images/theme/icons/fugue/cross-circle.png' alt=''/>")
                    .append("  </a>")
                    .append("</td>")
                    .append("</tr>");
            }
        }
        if(usr_items != null) {
            for (int i = 0; i < usr_items.length; i++) {
                displayName = resolveDisplayName("assignedToUserIds", usr_items[i]);
                operatorName = usr_opitems == null ? "" : resolveDisplayName("operator", usr_opitems[i]);
                sb.append("<tr>")
                    .append("<td>").append(operatorName).append("</td>")
                    .append("<td>").append(displayName).append("</td>")
                    .append("<td class=\"delete\">")
                    .append("  <input type='hidden' name='assignedToUserIds' value='").append(usr_items[i]).append("'/>")
                    .append("  <a class='deleteItem' href='javascript:void(0)' data-type='assignedToUserIds' data-div-id='assignedToIds' data-row-id='").append(i).append("' title='").append(deleteMsg).append("'>")
                    .append("    <img src='").append(contextPath).append("/images/theme/icons/fugue/cross-circle.png' alt=''/>")
                    .append("  </a>")
                    .append("</td>")
                    .append("</tr>");
            }
        }
        sb.append("</table>");
        return sb.toString();
    }

    private String resolveDisplayName(String type, Object nameOrId) {
        if (type.equals("ticketNumbers")) {
            return nameOrId.toString();
        } else if (type.equals("priorityIds")) {
            return ticketPriorityService.getPriorityById((Integer) nameOrId).getName();
        } else if (type.equals("statusIds")) {
            return statusService.getStatusById((Integer) nameOrId).getName();
        } else if (type.equals("contactIds") || type.equals("submittedByIds")) {
            User user = userService.getUserById((Integer) nameOrId);
            return user.getLastName() + ", " + user.getFirstName();
        } else if (type.equals("assignedToIds")) {
            UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById((Integer) nameOrId);
            if (urg.getUserRole() != null) {
                User user = urg.getUserRole().getUser();
                return user.getLastName() + ", " + user.getFirstName() + " - " + urg.getGroup().getName();
            } else {
                return getMessageSourceAccessor().getMessage("ticketList.ticketPool", "Ticket Pool") + " - " +
                        urg.getGroup().getName();
            }
        } else if(type.equals("assignedToUserIds")){
            User user = userService.getUserById((Integer)nameOrId);
            return user.getLastName() + ", " + user.getFirstName() + " - " + user.getLoginId();
        } else if (type.equals("locationIds")) {
            return locationService.getLocationById((Integer) nameOrId).getName();
        } else if (type.equals("groupIds")) {
            return groupService.getGroupById((Integer) nameOrId).getName();
        } else if (type.equals("categoryIds")) {
            return categoryService.getCategoryById((Integer) nameOrId).getName();
        } else if (type.equals("categoryOptionIds")) {
            return categoryOptionService.getCategoryOptionById((Integer) nameOrId).getName();
        } else if (type.equals("createdDate")) {

        } else if (type.equals("modifiedDate")) {

        } else if (type.equals("estimatedDate")) {

        } else if (type.equals("workTime")) {

        } else if (type.equals("surveys")) {
            return surveyQuestionService.getSurveyQuestionById(new Integer((String) nameOrId)).getSurveyQuestionText();
        } else if (type.equals("zenAssetIds")) {
            return zenAssetService.getById((Integer) nameOrId).getAssetName();
        } else if(type.equals("internalAssetIds")){
            return assetService.getAssetById((Integer)nameOrId).getName();
        } else if (type.equals("operator")) {
            String opName = getMessageSourceAccessor().getMessage("ticketFilter.op.equals");
            switch ((Integer) nameOrId) {
                case 9: // Equals
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.equals");
                    break;
                case 10: // Not Equals
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.notEquals");
                    break;
                case 11: // Greater Than
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.greaterThan");
                    break;
                case 12: // Less Than
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.lessThan");
                    break;
                case 13: // Contains
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.contains");
                    break;
                case 14: // Not Contains
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.notContains");
                    break;
                case 15: // Starts with
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.startsWith");
                    break;
                case 16: // Ends with
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.endsWith");
                    break;
            }
            return opName;
        }

        return (String) nameOrId;
    }

    private JSONObject getTicketJsonObject(Ticket ticket, Locale locale, TicketFilter ticketFilter, User user,
                                           String baseUrl) throws JSONException {
        JSONObject item = new JSONObject();

        // format object instances
        DateFormat dtFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);

        item.put(TicketFilter.COLUMN_TICKETNUMBER, ticket.getId());

        String noPermissionString = getMessageSourceAccessor().getMessage("ticketList.noPermission");

        // contact
        if (hasFieldPermisssion(ticket, "ticket.view.contact", "ticket.change.contact")) {
            String contactLabel = getUserLabel(ticket.getContact());
            item.put(TicketFilter.COLUMN_CONTACT, contactLabel);
        } else {
            item.put(TicketFilter.COLUMN_CONTACT, noPermissionString);
        }

        // submittedBy
        if (hasFieldPermisssion(ticket, "ticket.view.submittedBy")) {
            item.put(TicketFilter.COLUMN_SUBMITTED, getUserLabel(ticket.getSubmittedBy()));
        } else {
            item.put(TicketFilter.COLUMN_SUBMITTED, noPermissionString);
        }

        // assignedTo
        if (hasFieldPermisssion(ticket, "ticket.view.assignedTo", "ticket.change.assignedTo")) {
            String assignedToLabel;
            if (ticket.getAssignedTo() == null) {
                assignedToLabel = "";
            } else if (ticket.getAssignedTo().getUserRole() == null) {
                assignedToLabel = "Ticket Pool";
            } else if (ticket.getAssignedTo().getUserRole().getUser() == null) {
                assignedToLabel = "User Role " + ticket.getAssignedTo().getUserRole().getId() + " missing User";
            } else {
                assignedToLabel = getUserLabel(ticket.getAssignedTo().getUserRole().getUser());
            }

            item.put(TicketFilter.COLUMN_ASSIGNED, assignedToLabel);
        } else {
            item.put(TicketFilter.COLUMN_ASSIGNED, noPermissionString);
        }

        // priority
        if (hasFieldPermisssion(ticket, "ticket.view.priority", "ticket.change.priority")) {
            item.put(TicketFilter.COLUMN_PRIORITY, (ticket.getPriority() == null) ? "" : ticket.getPriority().getName());
        } else {
            item.put(TicketFilter.COLUMN_PRIORITY, noPermissionString);
        }

        // priority icon
        if (hasFieldPermisssion(ticket, "ticket.view.priority", "ticket.change.priority")) {
            item.put(TicketFilter.COLUMN_PRIORITY + ".image", getPriorityIconImagePath(ticket.getPriority(), baseUrl));
        } else {
            item.put(TicketFilter.COLUMN_PRIORITY + ".image", "");
        }

        // status
        if (hasFieldPermisssion(ticket, "ticket.view.status", "ticket.change.status")) {
            item.put(TicketFilter.COLUMN_STATUS, (ticket.getStatus() == null ? "" : ticket.getStatus().getName()));
        } else {
            item.put(TicketFilter.COLUMN_STATUS, noPermissionString);
        }

        // location
        if (hasFieldPermisssion(ticket, "ticket.view.location", "ticket.change.location")) {
            item.put(TicketFilter.COLUMN_LOCATION, (ticket.getLocation() == null) ? "" : ticket.getLocation().getName());
        } else {
            item.put(TicketFilter.COLUMN_LOCATION, noPermissionString);
        }

        // group
        if (hasFieldPermisssion(ticket, "ticket.view.group", "ticket.change.group")) {
            item.put(TicketFilter.COLUMN_GROUP, (ticket.getGroup() == null) ? "" : ticket.getGroup().getName());
        } else {
            item.put(TicketFilter.COLUMN_GROUP, noPermissionString);
        }

        // category
        if (hasFieldPermisssion(ticket, "ticket.view.category", "ticket.change.category")) {
            item.put(TicketFilter.COLUMN_CATEGORY, (ticket.getCategory() == null) ? "" : ticket.getCategory().getName());
        } else {
            item.put(TicketFilter.COLUMN_CATEGORY, noPermissionString);
        }

        // categoryOption
        if (hasFieldPermisssion(ticket, "ticket.view.categoryOption", "ticket.change.categoryOption")) {
            item.put(TicketFilter.COLUMN_CATOPT, (ticket.getCategoryOption() == null) ? "" : ticket.getCategoryOption().getName());
        } else {
            item.put(TicketFilter.COLUMN_CATOPT, noPermissionString);
        }

        // subject
        if (hasFieldPermisssion(ticket, "ticket.view.subject", "ticket.change.subject")) {
            item.put(TicketFilter.COLUMN_SUBJECT, StringUtils.defaultString(ticket.getSubject()));
        } else {
            item.put(TicketFilter.COLUMN_SUBJECT, noPermissionString);
        }

        // note
        if (hasFieldPermisssion(ticket, "ticket.view.note", "ticket.change.note")) {
            item.put(TicketFilter.COLUMN_NOTE, StringUtils.defaultString(ticket.getNote()));
        } else {
            item.put(TicketFilter.COLUMN_NOTE, noPermissionString);
        }

        // createdDate
        if (hasFieldPermisssion(ticket, "ticket.view.createdDate")) {
            item.put(TicketFilter.COLUMN_CREATED, (ticket.getCreatedDate() == null) ? "" : dtFormat.format(ticket.getCreatedDate()));
        } else {
            item.put(TicketFilter.COLUMN_CREATED, noPermissionString);
        }

        // modifiedDate
        if (hasFieldPermisssion(ticket, "ticket.view.modifiedDate")) {
            item.put(TicketFilter.COLUMN_MODIFIED, (ticket.getModifiedDate() == null) ? "" : dtFormat.format(ticket.getModifiedDate()));
        } else {
            item.put(TicketFilter.COLUMN_MODIFIED, noPermissionString);
        }

        // estimatedDate
        if (hasFieldPermisssion(ticket, "ticket.view.estimatedDate", "ticket.change.estimatedDate")) {
            item.put(TicketFilter.COLUMN_ESTIMATED, (ticket.getEstimatedDate() == null) ? "" : dateFormat.format(ticket.getEstimatedDate()));
        } else {
            item.put(TicketFilter.COLUMN_ESTIMATED, noPermissionString);
        }

        // worktime
        if (hasFieldPermisssion(ticket, "ticket.view.workTime", "ticket.change.workTime")) {
            item.put(TicketFilter.COLUMN_WORKTIME, (ticket.getWorkTime() == null) ? "" : ticket.getDisplayWorkTime());
        } else {
            item.put(TicketFilter.COLUMN_WORKTIME, noPermissionString);
        }

        // cc
        // bc
        // parent
        // oldTicketId
        // emailToTicketConfig
        // anonymousEmailAddress

        // customeFieldValues
        String[] customFieldColumns = getCustomFieldColumns(ticketFilter);
        for (String cfCol : customFieldColumns) {

            CustomFieldValues cfv = ticketService.getCustomFieldValue(ticket, StringUtils.removeStart(cfCol, TicketFilter.COLUMN_CUSTOMFIELDPREFIX));
            if (cfv == null) {
                item.put(cfCol, "");
            } else {
                item.put(cfCol, cfv.getFieldValue() == null ? "" : cfv.getFieldValue());
            }
        }

        // surveyData
        String[] surveyColumns = getSurveyColumns(ticketFilter);
        for (String surveyColumn : surveyColumns) {
            if (hasFieldPermisssion(ticket, "ticket.view.surveyData")) {
                SurveyData sd = ticketService.getSurveyData(ticket, StringUtils.removeStart(surveyColumn, TicketFilter.COLUMN_SURVEYPREFIX));
                if (sd == null) {
                    item.put(surveyColumn, "");
                } else {
                    item.put(surveyColumn, sd.getValue() == null ? "" : sd.getValue());
                }
            } else {
                item.put(surveyColumn, noPermissionString);
            }
        }

//        // zenAsset
//        if (hasFieldPermisssion(ticket, "ticket.view.zenAsset", "ticket.change.zenAsset")) {
//            if (ticket.getZenAsset() == null) {
//                item.put(TicketFilter.COLUMN_ZENWORKS10ASSET, "");
//            } else {
//                item.put(TicketFilter.COLUMN_ZENWORKS10ASSET, ticket.getZenAsset().getAssetName() == null ? "" : ticket.getZenAsset().getAssetName());
//            }
//        } else {
//            item.put(TicketFilter.COLUMN_ZENWORKS10ASSET, noPermissionString);
//        }

        // internal Asset
        if (hasFieldPermisssion(ticket, "ticket.view.asset", "ticket.change.asset")) {
            if (ticket.getAsset() == null) {
                item.put(TicketFilter.COLUMN_INTERNALASSET, "");
            } else {
                StringBuilder sb = new StringBuilder();
                if (ticket.getAsset().getAssetNumber() != null) sb.append(ticket.getAsset().getAssetNumber());
                if (ticket.getAsset().getName() != null) sb.append(" ").append(ticket.getAsset().getName());

                item.put(TicketFilter.COLUMN_INTERNALASSET, sb.toString());
            }
        } else {
            item.put(TicketFilter.COLUMN_INTERNALASSET, noPermissionString);
        }

        // most recent ticket history
        if (hasFieldPermisssion(ticket, "ticket.view.ticketHistory", "ticket.change.ticketHistory")) {
            TicketHistory ticketHist = ticketService.getMostRecentTicketHistory(ticket);
            if (ticketHist == null) {
                item.put(TicketFilter.COLUMN_HISTORYSUBJECT, "");
                item.put(TicketFilter.COLUMN_HISTORYNOTE, "");
            } else {
                item.put(TicketFilter.COLUMN_HISTORYSUBJECT, (StringUtils.isBlank(ticketHist.getSubject())) ? "" : ticketHist.getSubject());
                item.put(TicketFilter.COLUMN_HISTORYNOTE, (StringUtils.isBlank(ticketHist.getNote())) ? "" : ticketHist.getNote());
            }
        } else {
            item.put(TicketFilter.COLUMN_HISTORYSUBJECT, noPermissionString);
            item.put(TicketFilter.COLUMN_HISTORYNOTE, noPermissionString);
        }

        // notification flags
        boolean unread = ticket.getCreatedDate().equals(ticket.getModifiedDate());
        item.put("ticketFilter.unread", unread);
        item.put("ticketFilter.hasAttachment", hasAttachment(ticket));
        item.put("ticketFilter.hasParentTicket", ticket.getParent() != null);
        item.put("ticketFilter.hasSubTickets", CollectionUtils.isNotEmpty(ticket.getSubtickets()));

        // action required
        item.put("ticketFilter.actionRequired", (!unread && isActionRequired(ticket, user)));

        return item;
    }

    private boolean hasFieldPermisssion(Ticket ticket, String... permissionKeys) {
        Group group = ticket.getGroup();
        for (String permissionKey : permissionKeys) {
            if (permissionService.hasGroupPermission(permissionKey, group)) {
                return true;
            }
        }

        return false;
    }

    private String getPriorityIconImagePath(TicketPriority priority, String baseUrl) {
        if (priority == null) {
            return "";
        }

        String icon = priority.getIcon();
        if (StringUtils.isBlank(icon)) {
            return "";
        } else if (icon.equals("text")) {
            return "";
        } else {
            return baseUrl + "/images/" + icon + "ball_12.png";
        }
    }

    private boolean isActionRequired(Ticket ticket, User user) {
        // check if the ticket assignedto or contact is the current user and
        // has been modified by or commented on by user other than currently logged in user
        boolean assignedToCurrentUser = ticket.getAssignedTo() != null &&
                ticket.getAssignedTo().getUserRole() != null &&
                ticket.getAssignedTo().getUserRole().getUser().equals(user);
        boolean contactIsCurrentUser = ticket.getContact() != null && ticket.getContact().equals(user);

        if (!assignedToCurrentUser && !contactIsCurrentUser) {
            return false;
        }

        User lastModifiedUser = null;
        Date lastModifiedDate = null;
        List<TicketAudit> ticketAuditList = ticket.getTicketAudit();
        if (CollectionUtils.isNotEmpty(ticketAuditList)) {
            Collections.sort(ticketAuditList, new Comparator<TicketAudit>() {
                public int compare(TicketAudit o1, TicketAudit o2) {
                    Date d1 = o1.getDateStamp();
                    Date d2 = o2.getDateStamp();
                    return d1.compareTo(d2);
                }
            });

            TicketAudit last = ticketAuditList.get(ticketAuditList.size() - 1);
            lastModifiedUser = last.getUser();
            lastModifiedDate = last.getDateStamp();
        }

        List<TicketHistory> ticketHistoryList = new ArrayList<TicketHistory>(ticket.getTicketHistory());
        if (CollectionUtils.isNotEmpty(ticketHistoryList)) {
            Collections.sort(ticketHistoryList, new Comparator<TicketHistory>() {
                public int compare(TicketHistory o1, TicketHistory o2) {
                    Date d1 = o1.getCreatedDate();
                    Date d2 = o2.getCreatedDate();
                    return d1.compareTo(d2);
                }
            });

            TicketHistory last = ticketHistoryList.get(ticketHistoryList.size() - 1);
            if (lastModifiedDate == null || last.getCreatedDate().after(lastModifiedDate)) {
                lastModifiedDate = last.getCreatedDate();
                lastModifiedUser = last.getUser();
            }
        }

        if (lastModifiedDate == null) {
            lastModifiedUser = ticket.getSubmittedBy();
        }

        return !lastModifiedUser.equals(user);
    }

    private String[] getSurveyColumns(TicketFilter ticketFilter) {
        List<String> cols = new ArrayList<String>();
        for (String col : ticketFilter.getColumnOrder()) {
            if (StringUtils.startsWith(col, TicketFilter.COLUMN_SURVEYPREFIX)) {
                cols.add(col);
            }
        }
        return cols.toArray(new String[cols.size()]);
    }

    private String[] getCustomFieldColumns(TicketFilter ticketFilter) {
        List<String> cfcols = new ArrayList<String>();
        for (String col : ticketFilter.getColumnOrder()) {
            if (StringUtils.startsWith(col, TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                cfcols.add(col);
            }
        }
        return cfcols.toArray(new String[cfcols.size()]);
    }

    private boolean hasAttachment(Ticket ticket) {
        int size = ticket.getAttachments().size();
        if (size > 0) return true;

        for (TicketHistory th : ticket.getTicketHistory()) {
            if (th.getAttachments().size() > 0) return true;
        }

        return false;
    }

    private String getUserLabel(User user) {
        String label;
        if (user == null) {
            label = "";
        } else if (user.getId().equals(User.ADMIN_ID)) {
            label = "System Admin";
        } else if (user.getId().equals(User.ANONYMOUS_ID)) {
            label = "Anonymous";
        } else {
            label = (user.getFirstName() == null) ? "" : user.getFirstName();
            label += " ";
            label += (user.getLastName() == null) ? "" : user.getLastName();
        }

        return label;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setTicketFilterListPdfView(TicketFilterListPdfView ticketFilterListPdfView) {
        this.ticketFilterListPdfView = ticketFilterListPdfView;
    }

    public void setTicketFilterListExcelView(TicketFilterListExcelView ticketFilterListExcelView) {
        this.ticketFilterListExcelView = ticketFilterListExcelView;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public AssetTrackerZen10Service getAssetTrackerZen10Service() {
        return assetTrackerZen10Service;
    }

    public void setAssetTrackerZen10Service(AssetTrackerZen10Service assetTrackerZen10Service) {
        this.assetTrackerZen10Service = assetTrackerZen10Service;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
