package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 9/21/11
 * Time: 10:01 AM
 */
public class LocationConverter implements Converter<String, Location> {
    @Autowired private LocationService locationService;

    public Location convert(String id) {
        try {
            int locId = Integer.parseInt(id);
            return locationService.getLocationById(locId);
        } catch (Exception e) {
            return null;
        }
    }
}
