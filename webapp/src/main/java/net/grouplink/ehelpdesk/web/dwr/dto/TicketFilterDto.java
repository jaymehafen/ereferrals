package net.grouplink.ehelpdesk.web.dwr.dto;

import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.util.Map;

public class TicketFilterDto {

    private TicketFilter ticketFilter;
    private Map<String, String> columnNames;

    public TicketFilter getTicketFilter() {
        return ticketFilter;
    }

    public void setTicketFilter(TicketFilter ticketFilter) {
        this.ticketFilter = ticketFilter;
    }

    public Map<String, String> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(Map<String, String> columnNames) {
        this.columnNames = columnNames;
    }
}
