package net.grouplink.ehelpdesk.web.controllers.config.acl;

import net.grouplink.ehelpdesk.domain.acl.Permission;
import net.grouplink.ehelpdesk.domain.acl.PermissionSchemeEntry;

public class PermissionSchemeEntryHolder {
    private Permission permission;
    private PermissionSchemeEntry entry;

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public PermissionSchemeEntry getEntry() {
        return entry;
    }

    public void setEntry(PermissionSchemeEntry entry) {
        this.entry = entry;
    }
}
