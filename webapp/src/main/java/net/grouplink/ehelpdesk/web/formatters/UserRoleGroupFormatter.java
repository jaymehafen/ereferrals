package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class UserRoleGroupFormatter implements Formatter<UserRoleGroup> {
    @Autowired
    private UserRoleGroupService userRoleGroupService;

    public UserRoleGroup parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return userRoleGroupService.getUserRoleGroupById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(UserRoleGroup object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
