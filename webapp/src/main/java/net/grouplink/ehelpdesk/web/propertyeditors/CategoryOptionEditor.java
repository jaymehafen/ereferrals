package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * @author mrollins
 * @version 1.0
 */
public class CategoryOptionEditor extends PropertyEditorSupport {

    private CategoryOptionService categoryOptionService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public CategoryOptionEditor() {
    }

    /**
     * Constructor for use by derived PropertyEditor classes.
     * @param categoryOptionService the service
     */
    public CategoryOptionEditor(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        CategoryOption categoryOption = (CategoryOption)getValue();
        return (categoryOption !=  null)?categoryOption.getId().toString():"";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(categoryOptionService.getCategoryOptionById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }
}
