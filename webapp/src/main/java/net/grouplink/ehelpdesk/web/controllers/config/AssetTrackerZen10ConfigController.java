package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.AssetTrackerZen10Config;
import net.grouplink.ehelpdesk.service.AssetTrackerZen10Service;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.web.validators.AssetTrackerZen10ConfigValidator;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * author: zpearce
 * Date: 9/26/11
 * Time: 12:29 PM
 */
@Controller
@RequestMapping(value = "/asset")
public class AssetTrackerZen10ConfigController extends ApplicationObjectSupport {
    @Autowired private LicenseService licenseService;
    @Autowired private AssetTrackerZen10Service assetTrackerZen10Service;
    @Autowired private AssetTrackerZen10ConfigValidator assetTrackerZen10ConfigValidator;

    @RequestMapping(method = RequestMethod.GET, value = "/assetTrackerZen10Config.glml")
    public String showConfig(Model model, HttpSession session) {
        if(!licenseService.isZen10Integration()) {
            return "zen10Ad";
        }
        String path = session.getServletContext().getRealPath("");
        AssetTrackerZen10Config assetTrackerZen10Config = assetTrackerZen10Service.getAssetTrackerZen10Config(path);
        model.addAttribute("zen10Config", assetTrackerZen10Config);
        return "assetTrackerZen10Config";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/assetTrackerZen10Config.glml")
    public String updateConfig(@ModelAttribute("zen10Config") @Valid AssetTrackerZen10Config zen10Config, BindingResult bindingResult, HttpServletRequest request) {
        if(bindingResult.hasErrors()) {
            return "assetTrackerZen10Config";
        }

        if(zen10Config.getEnableZen10()) {
            String error = doTestConfig(zen10Config);
            if(StringUtils.isNotBlank(error)) {
                bindingResult.reject("error", error);
                return "assetTrackerZen10Config";
            }
        }

        String path = request.getSession().getServletContext().getRealPath("");
        assetTrackerZen10Service.saveAssetTrackerZen10Config(zen10Config, path);

        String msg = getMessageSourceAccessor().getMessage("assetTracker.zen10.saveSuccess");
        request.setAttribute("flash.info", msg);
        return "redirect:/config/asset/assetTrackerZen10Config.glml";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/testZen10Config.glml")
    public ResponseEntity<String> testConfig(@Valid AssetTrackerZen10Config zen10Config) throws Exception {
        JSONObject resp = new JSONObject();
        String error = doTestConfig(zen10Config);
        if(StringUtils.isNotEmpty(error)) {
            resp.put("type", "error");
            resp.put("msg", error);
        } else {
            resp.put("type", "success");
            resp.put("msg", getMessageSourceAccessor().getMessage("assetTracker.connectionSuccess"));
        }
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<String>(resp.toString(), httpHeaders, HttpStatus.OK);
    }

    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        binder.setValidator(assetTrackerZen10ConfigValidator);
    }

    private String doTestConfig(AssetTrackerZen10Config zen10Config) {
        return zen10Config.getZenDatabaseConfig().canConnect();
    }
}
