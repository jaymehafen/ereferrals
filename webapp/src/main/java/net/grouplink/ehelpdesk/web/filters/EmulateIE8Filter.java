package net.grouplink.ehelpdesk.web.filters;

import org.apache.commons.lang.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter that adds a response header to make IE9 emulate IE8.
 */
public class EmulateIE8Filter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String userAgent = req.getHeader("User-Agent");
        if (StringUtils.contains(userAgent, "MSIE 9.0")) {
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.addHeader("X-UA-Compatible", "IE=EmulateIE8");
        }

        chain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {
    }
}
