package net.grouplink.ehelpdesk.web.domain;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 11/22/11
 * Time: 5:20 PM
 */
public class NotificationAssistantForm {
    private List<User> users = new ArrayList<User>();
    private UserRoleGroup backupTech;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public UserRoleGroup getBackupTech() {
        return backupTech;
    }

    public void setBackupTech(UserRoleGroup backupTech) {
        this.backupTech = backupTech;
    }
}
