package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CategoryOptionValidator implements Validator {
    @Autowired
    private CategoryOptionService categoryOptionService;

    public boolean supports(Class clazz) {
        return clazz.equals(CategoryOption.class);
    }

    public void validate(Object target, Errors errors) {
        CategoryOption categoryOption = (CategoryOption) target;

        ValidationUtils.rejectIfEmpty(errors, "category", "ticketSettings.categoryOption.category.required", "You must add a Category before adding a Category Option");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "ticketSettings.categoryOption.required.name", "Category option name required");

        if (!errors.hasErrors()) { // required fields are good, now check for duplicate name
            CategoryOption co = categoryOptionService.getCategoryOptionByNameAndCategoryId(categoryOption.getName(), categoryOption.getCategory());
            if (co != null) {
                if (categoryOption.getId() != null) {
                    if (!categoryOption.getId().equals(co.getId())) {
                        errors.rejectValue("name", "ticketSettings.categoryOption.validate.notuniqueid", "Category option name already exists");
                    }
                } else {
                    errors.rejectValue("name", "ticketSettings.categoryOption.validate.notuniqueid", "Category option name already exists");
                }
            }
        }
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }
}
