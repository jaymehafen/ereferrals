package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.FullTextSearchException;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.TicketFullTextSearchService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: zpearce
 * Date: 5/26/11
 */
public class TicketFullTextSearchController extends MultiActionController {
    private TicketFullTextSearchService ticketFullTextSearchService;
    private UserService userService;

    public ModelAndView ticketFullTextSearch(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            UnsupportedEncodingException {
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();

        User user = (User) request.getSession(false).getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());

        String query = ServletRequestUtils.getRequiredStringParameter(request, "query");
        int page = ServletRequestUtils.getIntParameter(request, "page", 1);
        if (page < 1) page = 1;

        int pageSize = 10;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("ticketFullTextSearchPageSize".equals(cookie.getName())) {
                    pageSize = new Integer(cookie.getValue());
                }
            }
        }

        int ticketCount = 0;
        int pageCount = 0;
        List<Ticket> tickets = null;
        try {
            ticketCount = 0;
            if (StringUtils.isNotBlank(query)) {
                ticketCount = ticketFullTextSearchService.searchCount(query, user);
            }

            pageCount = (int)Math.ceil(ticketCount / (double)pageSize);

            if (page > pageCount) {
                page = pageCount;
            }

            tickets = new ArrayList<Ticket>();
            if (StringUtils.isNotBlank(query) && ticketCount != 0) {
                //hibernate paging is 0 based, but we want the url to be 1 based
                tickets = ticketFullTextSearchService.search(query, page - 1, pageSize, user);
            }

            model.put("tickets", tickets);
            model.put("ticketCount", ticketCount);
            model.put("pageSize", pageSize);
            model.put("pageCount", pageCount);
            model.put("currentPage", page);
        } catch (FullTextSearchException e) {
            model.put("error", "Error parsing query: " +e.getMessage());
        }

        model.put("query", query);

        return new ModelAndView("ticketFullTextSearch", model);
    }

    public void setTicketFullTextSearchService(TicketFullTextSearchService ticketFullTextSearchService) {
        this.ticketFullTextSearchService = ticketFullTextSearchService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
