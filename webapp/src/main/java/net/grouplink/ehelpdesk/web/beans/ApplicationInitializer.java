package net.grouplink.ehelpdesk.web.beans;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.DatabaseHandlerService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.service.UpdateCheckerService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.acl.PermissionInitializerService;
import net.grouplink.ehelpdesk.service.asset.AccountingInfoMigrationService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import java.util.Date;

/**
 * Date: Apr 27, 2007
 * Time: 2:12:46 PM
 */
@Component
public class ApplicationInitializer {

    // as of Spring 3.0, "servletContext" is now available as a default bean in
    // every WebApplicationContext
    @Autowired
    private ServletContext servletContext;
    @Autowired
    private PropertiesService propertiesService;
    @Autowired
    private MailService mailService;
    @Autowired
    private LdapService ldapService;
    @Autowired
    private LicenseService licenseService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private UserRoleGroupService userRoleGroupService;
    @Autowired
    private StatusService statusService;
    @Autowired
    private TicketFilterService ticketFilterService;
    @Autowired
    private PermissionInitializerService permissionInitializerService;
    @Autowired
    private DatabaseHandlerService databaseHandlerService;
    @Autowired
    private AssetService assetService;
    @Autowired
    private AccountingInfoMigrationService accountingInfoMigrationService;
    @Autowired
    private CustomFieldsService customFieldService;
    @Autowired
    private UpdateCheckerService updateCheckerService;
    @Autowired
    private RoleService roleService;

    @PostConstruct
    public void init() {
        databaseHandlerService.databaseInit();
        permissionInitializerService.init();
        mailInit();
        ldapInit();
        collaborationInit();
        applicationCustomizationInit();
        licenseInit();
        statusService.init();
        roleService.init();
        ticketFilterInit();
        accountingInfoMigrationInit();
        customFieldService.migrateFieldTypes();
        updateCheckerService.checkForUpdate();
    }

    private void accountingInfoMigrationInit() {
        if (!propertiesService.getBoolValueByName(PropertiesConstants.REMOVED_ASSET_ACCOUNTINGINFO_CONSTRAINT, false)) {
            accountingInfoMigrationService.removeAssetAccountingInfoForeignKey();
            propertiesService.saveProperties(PropertiesConstants.REMOVED_ASSET_ACCOUNTINGINFO_CONSTRAINT, "true");
        }

        accountingInfoMigrationService.convertAllAccountingInfo();
    }

    private void ticketFilterInit() {
        ticketFilterService.init();
    }

    public void applicationCustomizationInit() {
        servletContext.setAttribute(ApplicationConstants.APP_CUSTOMIZATION_PORTAL_URL, propertiesService.getPropertiesValueByName(PropertiesConstants.APP_CUSTOMIZATION_PORTAL_URL));
        servletContext.setAttribute(ApplicationConstants.APP_CUSTOMIZATION_SYSTEM_MESSAGE, propertiesService.getPropertiesValueByName(PropertiesConstants.APP_CUSTOMIZATION_SYSTEM_MESSAGE));
        servletContext.setAttribute(ApplicationConstants.ALLOW_USER_REGISTRATION, propertiesService.getBoolValueByName(PropertiesConstants.ALLOW_USER_REGISTRATION, true));
    }

    public void collaborationInit() {
        servletContext.setAttribute(ApplicationConstants.GROUP_WISE_INTEGRATION_6X, propertiesService.getBoolValueByName(PropertiesConstants.GROUP_WISE_INTEGRATION_6X, false));
        servletContext.setAttribute(ApplicationConstants.GROUP_WISE_INTEGRATION, propertiesService.getBoolValueByName(PropertiesConstants.GROUP_WISE_INTEGRATION, false));
        servletContext.setAttribute(ApplicationConstants.OUTLOOK_INTEGRATION, propertiesService.getBoolValueByName(PropertiesConstants.OUTLOOK_INTEGRATION, false));
        if (StringUtils.isNotBlank(propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_NAME))
                && StringUtils.isNotBlank(propertiesService.getPropertiesValueByName(PropertiesConstants.GROUPWISE_TRUSTEDAPP_KEY))) {
            servletContext.setAttribute(ApplicationConstants.GROUP_WISE_TRUSTED_APP, Boolean.TRUE);
        }
    }

    public void licenseInit() {
        licenseService.init(servletContext.getRealPath(LicenseService.LICENSE_FILE));
        if (licenseService.areYouThereLicense()) {
            servletContext.setAttribute(ApplicationConstants.LICENSE_EXISTS, Boolean.TRUE);

            Integer teksInLic = licenseService.getTechnicianCount();
            Integer teksInDB = userRoleGroupService.getTechnicianCount();
            servletContext.setAttribute(ApplicationConstants.TEKS_EXCEEDED, teksInDB > teksInLic);

            // Check workflow particpant license compliance
            Integer wfpInLic = licenseService.getWFParticpantCount();
            Integer wfpInDB = userRoleGroupService.getWFParticipantCount();
            servletContext.setAttribute(ApplicationConstants.WFPARTICIPANTS_EXCEEDED, wfpInDB > wfpInLic);

            Date now = new Date();
            Date hBomb = licenseService.getGlobalBombDate();
            if (hBomb != null) {
                servletContext.setAttribute(ApplicationConstants.GLOBAL_BOMB, now.getTime() > hBomb.getTime());
            }

            servletContext.setAttribute(ApplicationConstants.ENTERPRISE, licenseService.isEnterprise());
            Integer groupsInLic = licenseService.getGroupCount();
            Integer groupsInDB = groupService.getGroupCount();
            servletContext.setAttribute(ApplicationConstants.GROUPS_EXCEEDED, groupsInDB > groupsInLic);

            // Free version toggles
            servletContext.setAttribute(ApplicationConstants.DASHBOARDS_ALLOWED, licenseService.isDashboard());
            servletContext.setAttribute(ApplicationConstants.TICKETTEMPLATES, licenseService.isTicketTemplates());
            servletContext.setAttribute(ApplicationConstants.SCHEDULEDREPORTS, licenseService.isScheduledReports());
            servletContext.setAttribute(ApplicationConstants.ASSETTRACKER, licenseService.isAssetTracker());
            servletContext.setAttribute(ApplicationConstants.ZEN10INTEGRATION, licenseService.isZen10Integration());
        } else {
            servletContext.setAttribute(ApplicationConstants.LICENSE_EXISTS, Boolean.FALSE);
        }
    }

    public void mailInit() {
        //Setting all variables to ServletContext for GroupWise Integration and Mail settings
        if (mailService.init()) {
            servletContext.setAttribute(ApplicationConstants.MAIL_CONFIGURED, Boolean.TRUE);
            mailService.setMailConfigured(true);
        } else {
            servletContext.setAttribute(ApplicationConstants.MAIL_CONFIGURED, Boolean.FALSE);
            mailService.setMailConfigured(false);
        }
        servletContext.setAttribute(ApplicationConstants.MAIL_NOTIFY_TECHNIAN, propertiesService.getBoolValueByName(PropertiesConstants.MAIL_NOTIFY_TECHNIAN, false));
        servletContext.setAttribute(ApplicationConstants.MAIL_NOTIFY_USER, propertiesService.getBoolValueByName(PropertiesConstants.MAIL_NOTIFY_USER, false));
        servletContext.setAttribute(ApplicationConstants.MAIL_SEND_LINK_ON_NOTIFICATION, propertiesService.getBoolValueByName(PropertiesConstants.MAIL_SEND_LINK_ON_NOTIFICATION, false));
        servletContext.setAttribute(ApplicationConstants.MAIL_EMAIL_DISPLAY_NAME, propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_EMAIL_DISPLAY_NAME));
        servletContext.setAttribute(ApplicationConstants.MAIL_EMAIL_FROM_ADDRESS, propertiesService.getPropertiesValueByName(PropertiesConstants.MAIL_EMAIL_FROM_ADDRESS));
    }

    public void ldapInit() {
        //Setting all variables to ServletContext for LDAP settings
        Boolean ldapIntegrationEnabled = propertiesService.getBoolValueByName(PropertiesConstants.LDAP_INTEGRATION, false);
        if (ldapIntegrationEnabled && ldapService.init()) {
            servletContext.setAttribute(ApplicationConstants.LDAP_INTEGRATION, Boolean.TRUE);
        } else {
            servletContext.setAttribute(ApplicationConstants.LDAP_INTEGRATION, Boolean.FALSE);
        }
    }


}
