package net.grouplink.ehelpdesk.web.controllers.asset;

import com.Ostermiller.util.CSVParse;
import com.Ostermiller.util.CSVParser;
import com.Ostermiller.util.ExcelCSVParser;
import com.Ostermiller.util.LabeledCSVParser;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import net.grouplink.ehelpdesk.web.controllers.AbstractEHelpDeskFormController;
import net.grouplink.ehelpdesk.web.domain.VendorImport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class VendorImportFormController extends AbstractEHelpDeskFormController {
    protected final Log logger = LogFactory.getLog(getClass());

    private String[] vendorFields = {
            "vendor.name",
            "vendor.street1",
            "vendor.street2",
            "vendor.city",
            "vendor.state",
            "vendor.postalCode",
            "vendor.primaryPhone",
            "vendor.fax",
            "vendor.email",
            "vendor.notes"
    };

    private String[][] parsedValues;
    private String[] labels;
    private LinkedHashMap vendorFieldsMap;

    private VendorService vendorService;

    public VendorImportFormController() {
        super();
        setCommandClass(VendorImport.class);
        setFormView("vendorImport");
        setSuccessView("vendorImport.glml");
    }

    private LinkedHashMap initVendorFields() {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        for (String name : vendorFields) {
            map.put(name, getMessageSourceAccessor().getMessage(name));
        }
        return map;
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        // if forceRefresh is called remove the command object from the session
        if (ServletRequestUtils.getBooleanParameter(request, "forceRefresh", false)) {
            request.getSession().removeAttribute("assImport");
        }

        VendorImport vendorImport = (VendorImport) request.getSession().getAttribute("vendorImport");
        if (vendorImport == null) vendorImport = new VendorImport();

        return vendorImport;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        VendorImport vendorImport = (VendorImport) command;

        // Determine which submit button was pushed
        // if loadButton put the command on the session for the next submit
        if (ServletRequestUtils.getStringParameter(request, "loadButton") != null) {
            request.getSession().setAttribute("vendorImport", vendorImport);
            parseCSV(vendorImport);
        }

        if (ServletRequestUtils.getStringParameter(request, "importButton") != null) {
            doTheImport(vendorImport.getImportMap());
        }

        if (labels[labels.length - 1].trim().equals("")) {
            String[] _labels = new String[labels.length - 1];
            System.arraycopy(labels, 0, _labels, 0, labels.length - 1);
            labels = _labels;
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("vendorImport", vendorImport);
        model.put("csvLabels", labels);
        model.put("fieldOptions", getFieldOptions());
        return showForm(request, response, errors, model);
    }

    private String getFieldOptions(){
        if(vendorFieldsMap == null) vendorFieldsMap = initVendorFields();

        StringBuilder sb = new StringBuilder("<option value=\"\"></option>");
        for(String name: vendorFields){
            sb.append("<option value=\"").append(name).append("\">")
                    .append(vendorFieldsMap.get(name))
                    .append("</option>");
        }

        return sb.toString();
    }

    private void parseCSV(VendorImport vendorImport) throws Exception {
        CSVParse csvParse;

        if ("xcelcsv".equals(vendorImport.getCsvType())) {
            csvParse = new ExcelCSVParser(new ByteArrayInputStream(vendorImport.getFileData()));
            if (vendorImport.isFirstLineHeader()) {
                LabeledCSVParser lcsvp = new LabeledCSVParser(csvParse);
                parsedValues = lcsvp.getAllValues();
                labels = lcsvp.getLabels();
            } else {
                parsedValues = csvParse.getAllValues();
                labels = getGenericLabels(parsedValues[0].length);
            }
        } else /*if("csv".equals(vendorImport.getCsvType()))*/ {
            csvParse = new CSVParser(new ByteArrayInputStream(vendorImport.getFileData()));
            if (vendorImport.isFirstLineHeader()) {
                LabeledCSVParser lcsvp = new LabeledCSVParser(csvParse);
                parsedValues = lcsvp.getAllValues();
                labels = lcsvp.getLabels();
            } else {
                parsedValues = csvParse.getAllValues();
                labels = getGenericLabels(parsedValues[0].length);
            }
        }
    }

    private String[] getGenericLabels(int cols) {
        String[] retVal = new String[cols];
        for (int i = 0; i < cols; i++) {
            retVal[i] = "Column " + (i + 1);
        }
        return retVal;
    }

    private void doTheImport(Map importMap) {

        for (int i = 0; i < parsedValues.length; i++) {
            // every i is a row in the csv file therefore it represents a new asset(s) obj
            Vendor vendor = lookupVendor(i, importMap);

            // catch any exceptions in processing this row
            int j = 0;

            for (; j < parsedValues[i].length; j++) {
                setTheProperty((String) importMap.get("col" + j), parsedValues[i][j], vendor);
            }
            vendorService.saveVendor(vendor);
        }
    }

    private Vendor lookupVendor(int index, Map importMap) {
        Vendor vendor = null;
        if(importMap.containsValue("vendor.vendorName")){
            for (int i = 0; i < parsedValues[index].length; i++) {
                if(importMap.get("col"+i).equals("vendor.vendorName")){
                    vendor = vendorService.getByName(parsedValues[index][i]);
                    break;
                }
            }
        }
        if(vendor == null) vendor = new Vendor();
        return vendor;
    }

    private void setTheProperty(String property, String value, Vendor vendor) {
        if("vendor.name".equals(property)){
            vendor.setName(value);
        } else if("vendor.street1".equals(property)){
            vendor.setStreet1(value);
        } else if("vendor.street2".equals(property)){
            vendor.setStreet2(value);
        } else if("vendor.city".equals(property)){
            vendor.setCity(value);
        } else if("vendor.state".equals(property)){
            vendor.setState(value);
        } else if("vendor.postalCode".equals(property)){
            vendor.setPostalCode(value);
        } else if("vendor.primaryPhone".equals(property)){
            vendor.setPrimaryPhone(value);
        } else if("vendor.fax".equals(property)){
            vendor.setFax(value);
        } else if("vendor.email".equals(property)){
            vendor.setEmail(value);
        } else if("vendor.notes".equals(property)){
            vendor.setNotes(value);
        }
    }

    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    public void afterPropertiesSet() throws Exception {

    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }
}
