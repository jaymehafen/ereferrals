package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Mar 12, 2008
 * Time: 7:59:01 PM
 */
public class TicketFilterValidator extends BaseValidator {
    /**
     * Can this {@link org.springframework.validation.Validator} {@link #validate(Object, org.springframework.validation.Errors) validate}
     * instances of the supplied <code>clazz</code>?
     * <p>This method is <i>'typically'</i> implemented like so:
     * <pre class="code">return Foo.class.isAssignableFrom(clazz);</pre>
     * (Where <code>Foo</code> is the class (or superclass) of the actual
     * object instance that is to be {@link #validate(Object, org.springframework.validation.Errors) validated}).)
     * <p>However, implementations can (of course) choose to implement
     * this method in any manner they please.
     *
     * @param clazz the {@link Class} that this {@link org.springframework.validation.Validator} is
     *              being asked if it can {@link #validate(Object, org.springframework.validation.Errors) validate}
     * @return <code>true</code> if this {@link org.springframework.validation.Validator} can indeed
     *         {@link #validate(Object, org.springframework.validation.Errors) validate} instances of the
     *         supplied <code>clazz</code>
     */
    public boolean supports(Class clazz) {
        return clazz.equals(TicketFilter.class);
    }

    /**
     * Validate the supplied <code>target</code> object, which must be
     * of a {@link Class} for which the {@link #supports(Class)} method
     * typically has (or would) return <code>true</code>.
     * <p>The supplied {@link org.springframework.validation.Errors errors} instance can be used to report
     * any resulting validation errors.
     *
     * @param target the object that is to be validated (can be <code>null</code>)
     * @param errors contextual state about the validation process (never <code>null</code>)
     * @see org.springframework.validation.ValidationUtils
     */
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");      

        TicketFilter ticketFilter = (TicketFilter) target;
        String[] colOrder = ticketFilter.getColumnOrder();
        if(colOrder == null || colOrder.length == 0){
            errors.rejectValue("columnOrder", "ticketFilter.colOrderError", "At least one column should be displayed.");
        }

        int selectedOption;
        if(ticketFilter.getCreatedDateOperator() != null) {
            selectedOption = ticketFilter.getCreatedDateOperator();
            validateDateFieldBasedOnSelection("createdDate", "createdDate2", selectedOption, errors);
        }
        if(ticketFilter.getModifiedDateOperator() != null) {
            selectedOption = ticketFilter.getModifiedDateOperator();
            validateDateFieldBasedOnSelection("modifiedDate", "modifiedDate2", selectedOption, errors);
        }
        if(ticketFilter.getEstimatedDateOperator() != null) {
            selectedOption = ticketFilter.getEstimatedDateOperator();
            validateDateFieldBasedOnSelection("estimatedDate", "estimatedDate2", selectedOption, errors);
        }
        if(ticketFilter.getWorkTimeOperator() != null) {
            String strSelectedOption = ticketFilter.getWorkTimeOperator().trim();
            if(strSelectedOption.equalsIgnoreCase("LESS")) {
                validateWorkTimeBasedOnSelectedOption(1, errors);
            }
            else if(strSelectedOption.equalsIgnoreCase("MORE")) {
                validateWorkTimeBasedOnSelectedOption(2, errors);
            }
        }

    }

    //JDK 1.4 dosn't support case TicketFilterOperators.ON.intValue(), so don't complain on hard coded case numbers.
    private void validateDateFieldBasedOnSelection(String dateFieldName1, String dateFieldName2, int selectedOption, Errors errors) {
        switch(selectedOption) {
            case 4:
            case 5:
            case 6:
                 ValidationUtils.rejectIfEmptyOrWhitespace(errors, dateFieldName1, "global.fieldRequired", "This field cannot be empty");
                 break;
            case 7:
                 ValidationUtils.rejectIfEmptyOrWhitespace(errors, dateFieldName1, "global.fieldRequired", "This field cannot be empty");
                 ValidationUtils.rejectIfEmptyOrWhitespace(errors, dateFieldName2, "global.fieldRequired", "This field cannot be empty");
                 break;
            default:
                 break;
        }
    }

    //JDK 1.4 dosn't support autoboxing, so don't complain on hard coded case numbers.
    private void validateWorkTimeBasedOnSelectedOption(int selectedOption, Errors errors) {
        switch(selectedOption) {
            case 1:
            case 2:
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "workTimeHours", "global.fieldRequired", "This field cannot be empty");
                ValidationUtils.rejectIfEmptyOrWhitespace(errors, "workTimeMinutes", "global.fieldRequired", "This field cannot be empty");
                break;
            default:
                break;
        } 
    }
}
