package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.service.GroupService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class GroupValidator implements Validator {
    @Autowired
	private GroupService groupService;

    public boolean supports(Class clazz) {
		return clazz.equals(Group.class);
	}

    public void validate(Object target, Errors errors) {
		Group group = (Group) target;
        if(StringUtils.isBlank(group.getName())){
            errors.rejectValue("name", "ticketSettings.group.validate.noname", "Group name can not be empty");
        }
        if (groupService.hasDuplicateGroupName(group)) {
            errors.rejectValue("name", "ticketSettings.group.validate.notuniqueid", "Group name already exists");
        }
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }
}
