package net.grouplink.ehelpdesk.web.forms;

import net.grouplink.ehelpdesk.domain.Status;
import org.springframework.util.AutoPopulatingList;

import javax.validation.Valid;

/**
 * author: zpearce
 * Date: 11/28/11
 * Time: 4:05 PM
 */
public class StatusForm {
    @Valid
    private AutoPopulatingList<Status> statuses = new AutoPopulatingList<Status>(Status.class);

    public void setStatuses(AutoPopulatingList<Status> statuses) {
        this.statuses = statuses;
    }

    public AutoPopulatingList<Status> getStatuses() {
        return statuses;
    }

    public Status getStatusById(int id) {
        Status returnVal = null;
        for(Status status : getStatuses()) {
            if(status.getId() == id) {
                returnVal = status;
                break;
            }
        }
        return returnVal;
    }
}
