package net.grouplink.ehelpdesk.web.interceptors;

import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mrollins
 * @version 1.0
 */
public class SessionUserInjector extends HandlerInterceptorAdapter {

    private Integer sessionUserId;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        request.getSession().invalidate();
	// Don't change the userid on the session if there is already one there
        Integer userId = (Integer)request.getSession().getAttribute(SessionConstants.ATTR_USER_ID);
        if(userId == null) {
            request.getSession(true).setAttribute(SessionConstants.ATTR_USER_ID, sessionUserId);
        }

        return true;
    }

    public void setUserId(Integer userId) {
        this.sessionUserId = userId;
    }
}
