package net.grouplink.ehelpdesk.web.pda;

import net.grouplink.ehelpdesk.common.utils.UserSearch;
import net.grouplink.ehelpdesk.domain.Assignment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.CustomFieldValuesService;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.AssignmentService;
import net.grouplink.ehelpdesk.service.TicketHistoryService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.controllers.AbstractEHelpDeskFormController;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryOptionEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.GroupEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.LocationEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.StatusEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.TicketPriorityEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserRoleGroupEditor;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 */
public class PdaFormController extends AbstractEHelpDeskFormController {

    private TicketService ticketService;
    private UserService userService;
    private TicketPriorityService ticketPriorityService;
    private LocationService locationService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private AssignmentService assignmentService;
    private StatusService statusService;
    private CustomFieldsService customFieldsService;
    private CustomFieldValuesService customFieldValuesService;
    private UserRoleGroupService userRoleGroupService;
    private TicketHistoryService ticketHistoryService;
    private LdapService ldapService;
    private LdapFieldService ldapFieldService;
    private GroupService groupService;
    private MailService mailService;


    public PdaFormController() {
        super();
        setCommandClass(Ticket.class);
        setCommandName("ticket");
        setFormView("ticketEditPDA");
        setSuccessView("ticketEdit.glml");
    }

    /**
     * Retrieve a backing object for the current form from the given request.
     * <p>The properties of the form object will correspond to the form field values
     * in your form view. This object will be exposed in the model under the specified
     * command name, to be accessed under that name in the view: for example, with
     * a "spring:bind" tag. The default command name is "command".
     * <p>Note that you need to activate session form mode to reuse the form-backing
     * object across the entire form workflow. Else, a new instance of the command
     * class will be created for each submission attempt, just using this backing
     * object as template for the initial form.
     * <p>The default implementation calls {@link #createCommand()},
     * creating a new empty instance of the specified command class.
     * Subclasses can override this to provide a preinitialized backing object.
     *
     * @param request current HTTP request
     * @return the backing object
     * @throws Exception in case of invalid state or arguments
     * @see #setCommandName
     * @see #setCommandClass
     * @see #createCommand
     */
    protected Object formBackingObject(HttpServletRequest request) throws Exception {

        String tid = ServletRequestUtils.getStringParameter(request, "tid");
        String contactId = ServletRequestUtils.getStringParameter(request, "contactId");
        String parentId = ServletRequestUtils.getStringParameter(request, "parentId");
        Ticket ticket = null;
        if (StringUtils.isNotBlank(tid)) {
            ticket = ticketService.getTicketById(new Integer(tid));
        }

        if (ticket == null) {
            User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
            user = userService.getUserById(user.getId());
            ticket = createNewTicket(user);
            if (StringUtils.isNotBlank(contactId)) {
                User contact = userService.getUserById(new Integer(contactId));
                ticket.setContact(contact);
            }
            if (StringUtils.isNotBlank(parentId)) {
                Ticket parent = ticketService.getTicketById(new Integer(parentId));
                ticket.setContact(parent.getContact());
                ticket.setLocation(parent.getLocation());
                ticket.setGroup(parent.getGroup());
                ticket.setCategory(parent.getCategory());
                ticket.setCategoryOption(parent.getCategoryOption());
                ticket.setAssignedTo(parent.getAssignedTo());
                ticket.setParent(parent);
            }
        }

        //Check for User notification configuration
        ticket.setNotifyUser((Boolean) request.getSession().getServletContext().getAttribute(ApplicationConstants.MAIL_NOTIFY_USER));
        ticket.setNotifyTech((Boolean) request.getSession().getServletContext().getAttribute(ApplicationConstants.MAIL_NOTIFY_TECHNIAN));

        return ticket;
    }

    private Ticket createNewTicket(User submittedByUser) {
        User theRealUser = userService.getUserById(submittedByUser.getId());
        Ticket ticket = new Ticket();
        ticket.setContact(theRealUser);
        ticket.setSubmittedBy(theRealUser);
        ticket.setModifiedDate(new Date());

        //Setting up ticket priority
        List priorityList = ticketPriorityService.getPriorities();
        if(!priorityList.isEmpty()){
            ticket.setPriority((TicketPriority) priorityList.get(0));
        }

        return ticket;
    }

    /**
     * Create a reference data map for the given request and command,
     * consisting of bean name/bean instance pairs as expected by ModelAndView.
     * <p>The default implementation delegates to {@link #referenceData(javax.servlet.http.HttpServletRequest)}.
     * Subclasses can override this to set reference data used in the view.
     *
     * @param request current HTTP request
     * @param command form object with request parameters bound onto it
     * @param errors  validation errors holder
     * @return a Map with reference data entries, or <code>null</code> if none
     * @throws Exception in case of invalid state or arguments
     * @see org.springframework.web.servlet.ModelAndView
     */
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

        Map model = new HashMap();
        Ticket ticket = (Ticket) command;

        List tiHisList = new ArrayList();
        if (ticket.getTicketHistory() != null) {
            tiHisList = new ArrayList(ticket.getTicketHistory());
        }
        Collections.sort(tiHisList);

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        List locations;

        List initAssignedTo = new ArrayList();
        List initAssignedToForUser = new ArrayList();
        List initGroups = new ArrayList();
        List initCats = new ArrayList();
        List initCatOpts = new ArrayList();

        // if this is a submission for a dropdown change, set the particular property and show the form
        String ddSubmit = ServletRequestUtils.getStringParameter(request, "ddSubmit");
        int dd = 5;
        if (StringUtils.isNotBlank(ddSubmit)) {
            if ("location".equals(ddSubmit)) dd = 1;
            else if ("group".equals(ddSubmit)) dd = 2;
            else if ("category".equals(ddSubmit)) dd = 3;
            else if ("categoryoption".equals(ddSubmit)) dd = 4;
        }

        locations = locationService.getLocations();

        if (ticket.getLocation() != null) {
            initGroups = assignmentService.getGroupListByLocationId(ticket.getLocation().getId());
        }

        if (ticket.getGroup() != null && dd > 1) {
            initCats = assignmentService.getCategoryListByLocationIdAndGroupId(ticket.getLocation().getId(), ticket.getGroup().getId());
        }

        if (ticket.getCategory() != null && dd > 2) {
            initCatOpts = new ArrayList(ticket.getCategory().getCategoryOptions());
            for (int i = 0; i < initCatOpts.size(); i++) {
                if (!((CategoryOption) initCatOpts.get(i)).isActive()) {
                    initCatOpts.remove(i);
                }
            }
        }

        if (ticket.getCategoryOption() != null && dd > 3) {
            List list = assignmentService.getByCatOptAndLocation(ticket.getCategoryOption(), ticket.getLocation());
            for (int i = 0; i < list.size(); i++) {
                initAssignedTo.add(((Assignment) list.get(i)).getUserRoleGroup());
                initAssignedToForUser.add(((Assignment) list.get(i)).getUserRoleGroup());
            }
        }

        if (ticket.getId() != null && ticket.getGroup() != null && dd >= 4) {
            initAssignedTo = userRoleGroupService.getUserRoleGroupByGroupId(ticket.getGroup().getId());
        }

        String search = ServletRequestUtils.getStringParameter(request, "searchForUser");
        List ldapUserList = new ArrayList();
        List ldapFieldList = new ArrayList();
        List userList = new ArrayList();
        Boolean showSearchPage = Boolean.FALSE;
        if (StringUtils.isNotBlank(search)) {
            UserSearch userSearch = new UserSearch();
            userSearch.setLastName(ServletRequestUtils.getStringParameter(request, "usearch_lastName"));
            userSearch.setFirstName(ServletRequestUtils.getStringParameter(request, "usearch_firstName"));
            userSearch.setEmail(ServletRequestUtils.getStringParameter(request, "usearch_email"));

            ServletContext application = request.getSession(true).getServletContext();
            Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
            if (ldapIntegration != null && ldapIntegration.booleanValue()) {
                ldapUserList.addAll(ldapService.searchLdapUser(userSearch, 2));
                ldapFieldList = ldapFieldService.getLdapFields();
            } else {
                userList = userService.searchUser(userSearch);
            }
            showSearchPage = Boolean.TRUE;
        }

        model.put("ddVal", new Integer(dd));
        model.put("showSearchPage", showSearchPage.booleanValue() ? "true" : "false");
        model.put("tinfo_details_state", ServletRequestUtils.getStringParameter(request, "tinfo_details_state", "block"));
        model.put("tdescription_state", ServletRequestUtils.getStringParameter(request, "tdescription_state", "block"));
        model.put("hist_details_state", ServletRequestUtils.getStringParameter(request, "hist_details_state", "block"));
        model.put("ldapUserList", ldapUserList);
        model.put("ldapFieldList", ldapFieldList);
        model.put("userList", userList);
        model.put("ticketHistories", tiHisList);
        model.put("ticketPriorities", ticketPriorityService.getPriorities());
        model.put("ticketStatus", statusService.getStatus());
        model.put("locations", locations);
        model.put("initGroups", initGroups);
        model.put("initCats", initCats);
        model.put("initCatOpts", initCatOpts);
        Collections.sort(initAssignedTo);
        model.put("initAssignedTo", initAssignedTo);
        Collections.sort(initAssignedToForUser);
        model.put("initAssignedToForUser", initAssignedToForUser);
        model.put("userRoleGroupList", userRoleGroupService.getUserRoleGroupByUserId(user.getId()));
        model.put("customFields", customFieldsService.getCustomFieldsInHtml(ticket));
        List cfields = new ArrayList();
        if (ticket.getCategoryOption() != null)
            cfields = customFieldsService.getCustomFieldsByCategoryOptionId(ticket.getCategoryOption().getId());
        model.put("cFields", cfields);

        return model;
    }


    /**
     * This implementation calls
     * {@link #showForm(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse,org.springframework.validation.BindException)}
     * in case of errors, and delegates to the full
     * {@link #onSubmit(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse,Object,org.springframework.validation.BindException)}'s
     * variant else.
     * <p>This can only be overridden to check for an action that should be executed
     * without respect to binding errors, like a cancel action. To just handle successful
     * submissions without binding errors, override one of the <code>onSubmit</code>
     * methods or {@link #doSubmitAction}.
     *
     * @see #showForm(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse,org.springframework.validation.BindException)
     * @see #onSubmit(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse,Object,org.springframework.validation.BindException)
     * @see #onSubmit(Object,org.springframework.validation.BindException)
     * @see #onSubmit(Object)
     * @see #doSubmitAction(Object)
     */
    protected ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

        return super.processFormSubmission(request, response, command, errors);
        // Check that required custom filleds our field out.
//        Enumeration enumberation = request.getParameterNames();
//        while (enumberation.hasMoreElements()) {
//            String parm = (String) enumberation.nextElement();
//            if (parm.startsWith("cf_")) {
//                String[] parmpcs = parm.split("_");
//                CustomField cf = customFieldsService.getCustomFieldById(new Integer(parmpcs[1]));
//                String value = request.getParameter(parm);
//                if (cf.isRequired() && StringUtils.isBlank(value)) {
//                    errors.rejectValue(parm, null, getMessageSourceAccessor().getMessage("customField.validate.required")+" : "+cf.getName());
//                }
//            }
//        }

//        if (errors.hasErrors()) {
//            return showForm(request, response, errors);
//        } else {
//            return onSubmit(request, response, command, errors);
//        }
    }

    /**
     * Submit callback with all parameters. Called in case of submit without errors
     * reported by the registered validator, or on every submit if no validator.
     * <p>The default implementation delegates to {@link #onSubmit(Object,org.springframework.validation.BindException)}.
     * For simply performing a submit action and rendering the specified success
     * view, consider implementing {@link #doSubmitAction} rather than an
     * <code>onSubmit</code> variant.
     * <p>Subclasses can override this to provide custom submission handling like storing
     * the object to the database. Implementations can also perform custom validation and
     * call showForm to return to the form. Do <i>not</i> implement multiple onSubmit
     * methods: In that case, just this method will be called by the controller.
     * <p>Call <code>errors.getModel()</code> to populate the ModelAndView model
     * with the command and the Errors instance, under the specified command name,
     * as expected by the "spring:bind" tag.
     *
     * @param request  current servlet request
     * @param response current servlet response
     * @param command  form object with request parameters bound onto it
     * @param errors   Errors instance without errors (subclass can add errors if it wants to)
     * @return the prepared model and view, or <code>null</code>
     * @throws Exception in case of errors
     * @see #onSubmit(Object,org.springframework.validation.BindException)
     * @see #doSubmitAction
     * @see #showForm
     * @see org.springframework.validation.Errors
     * @see org.springframework.validation.BindException#getModel
     */
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        Ticket ticket = (Ticket) command;

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());
        // if this is a submission for a user search don't save, just return the form
        if (StringUtils.isNotBlank(ServletRequestUtils.getStringParameter(request, "searchForUser"))) {
            return showForm(request, response, errors);
        }

        // if this submission is for selecting the contact set the contact and show the form
        String userDn = ServletRequestUtils.getStringParameter(request, "selectedUser");
        if (StringUtils.isNotBlank(userDn)) {
            ServletContext application = request.getSession(true).getServletContext();
            Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
            User contact;
            if (ldapIntegration != null && ldapIntegration.booleanValue()) {
                LdapUser ldapUser = ldapService.getLdapUser(userDn);
                contact = userService.getUserByUsername(ldapUser.getCn());
                contact = userService.synchronizedUserWithLdap(contact, null, ldapUser);
            } else {
                // userDn should just be a user id
                contact = userService.getUserById(new Integer(userDn));
            }
            ticket.setContact(contact);
            return showForm(request, response, errors);
        }

        // if this is a submission for a dropdown change, show the form
        String ddSubmit = ServletRequestUtils.getStringParameter(request, "ddSubmit");
        if (StringUtils.isNotBlank(ddSubmit)) {
            return showForm(request, response, errors);
        }

        // if this is a submission for adding a new ticket history item, save the history item and show the form
        String addingComment = ServletRequestUtils.getStringParameter(request, "addingNewComment");
        if (StringUtils.isNotBlank(addingComment)) {
            String th_subj = ServletRequestUtils.getStringParameter(request, "thist_subject");
            String th_note = ServletRequestUtils.getStringParameter(request, "thist_note");

            TicketHistory ticketHistory = new TicketHistory();
            ticketHistory.setUser(user);
            ticketHistory.setTicket(ticket);
            ticketHistory.setCommentType(0);
            ticketHistory.setCreatedDate(new Date());
            ticketHistory.setActive(true);
            ticketHistory.setSubject(th_subj);
            ticketHistory.setNote(th_note);
            ticketHistory.setNotifyUser((Boolean) session.getServletContext().getAttribute(ApplicationConstants.MAIL_NOTIFY_USER));
            ticketHistory.setNotifyTech((Boolean) session.getServletContext().getAttribute(ApplicationConstants.MAIL_NOTIFY_TECHNIAN));

            //Saving ticket history
            ticketHistoryService.saveTicketHistory(ticketHistory, user);

            //Last thing to do is send email notification so MAKE SURE EVERYTHING GETS DONE BEFORE THAT.
            ticketHistoryService.sendEmailNotification(ticketHistory);

            return showForm(request, response, errors);
        }


        if (ticket.getId() == null) {
            if (ticket.getAssignedTo().getUserRole() == null) {
                ticket.setStatus(statusService.getStatusById(Integer.valueOf("1")));
            } else {
                ticket.setStatus(statusService.getStatusById(Integer.valueOf("2")));
            }
        }

        //create mail message
        String htmlMessage = ticketService.createHTMLComments(ticket, ticket.getOldTicket());
        String textMessage = ticketService.createTextComments(ticket, ticket.getOldTicket());

        ///Creating history note for ticket update
        if(ticket.getId() != null){
            ticketHistoryService.saveComments(user, TicketHistory.TICKET_UPDATE, ticket);
        }
        //end of ticket history

        //SAVING TICKET
        ticketService.saveTicket(ticket, user);

        // ticket should have an id, save custom fields NOW
        Enumeration enumberation = request.getParameterNames();
        while (enumberation.hasMoreElements()) {
            String parm = (String) enumberation.nextElement();
            if (parm.startsWith("cf_")) {
                String[] parmpcs = parm.split("_");
                CustomField cf = customFieldsService.getCustomFieldById(new Integer(parmpcs[1]));

                String value = request.getParameter(parm);

                CustomFieldValues cfv = customFieldValuesService.getValuesByTicketCustomField(ticket.getId(), cf);
                if(cfv == null){
                    cfv = new CustomFieldValues();
                }

                if (cf.getFieldType().equals(CustomField.VALUETYPE_CHECKBOX)) {
                    if (value.equals("on")) value="true";
                }

                cfv.setCustomField(cf);
                cfv.setTicket(ticket);
                cfv.setFieldValue(value);
                customFieldValuesService.saveCustomFieldValues(cfv);
            }
        }

        //Last thing to do is send email notification to MAKE SURE EVERYTHING GETS DONE BEFORE THAT.
        ticketService.sendEmailNotification(ticket, user, htmlMessage, textMessage);

        return new ModelAndView(new RedirectView(getSuccessView() + "?tid=" + ticket.getId()));
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p>This method allows the bean instance to perform initialization only
     * possible when all bean properties have been set and to throw an
     * exception in the event of misconfiguration.
     *
     * @throws Exception in the event of misconfiguration (such
     *                   as failure to set an essential property) or if initialization fails.
     */
    public void afterPropertiesSet() throws Exception {
        if (ticketService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketService");
        if (userService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userService");
        if (ticketPriorityService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketPriorityService");
        if (locationService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: locationService");
        if (categoryService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryService");
        if (categoryOptionService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryOptionService");
        if (assignmentService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: assignmentService");
        if (statusService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: statusService");
        if (customFieldsService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: customFieldsService");
        if (customFieldValuesService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: customFieldValuesService");
        if (userRoleGroupService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userRoleGroupService");
        if (ticketHistoryService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketHistoryService");
        if (ldapService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ldapService");
        if (ldapFieldService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ldapFieldService");
        if (groupService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: groupService");
        if (mailService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: mailService");
    }

    /**
     * Initialize the given binder instance, for example with custom editors.
     * Called by {@link #createBinder}.
     * <p>This method allows you to register custom editors for certain fields of your
     * command class. For instance, you will be able to transform Date objects into a
     * String pattern and back, in order to allow your JavaBeans to have Date properties
     * and still be able to set and display them in an HTML interface.
     * <p>The default implementation is empty.
     *
     * @param request current HTTP request
     * @param binder  the new binder instance
     * @throws Exception in case of invalid state or arguments
     * @see #createBinder
     * @see org.springframework.validation.DataBinder#registerCustomEditor
     * @see org.springframework.beans.propertyeditors.CustomDateEditor
     */
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);

        binder.registerCustomEditor(User.class, new UserEditor(userService));
        binder.registerCustomEditor(TicketPriority.class, new TicketPriorityEditor(ticketPriorityService));
        binder.registerCustomEditor(Location.class, new LocationEditor(locationService));
        binder.registerCustomEditor(Group.class, new GroupEditor(groupService));
        binder.registerCustomEditor(Category.class, new CategoryEditor(categoryService));
        binder.registerCustomEditor(CategoryOption.class, new CategoryOptionEditor(categoryOptionService));
        binder.registerCustomEditor(Status.class, new StatusEditor(statusService));
        binder.registerCustomEditor(UserRoleGroup.class, new UserRoleGroupEditor(userRoleGroupService));
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setAssignmentService(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setCustomFieldsService(CustomFieldsService customFieldsService) {
        this.customFieldsService = customFieldsService;
    }

    public void setCustomFieldValuesService(CustomFieldValuesService customFieldValuesService) {
        this.customFieldValuesService = customFieldValuesService;
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setLdapFieldService(LdapFieldService ldapFieldService) {
        this.ldapFieldService = ldapFieldService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
}
