package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @author mrollins
 * @version 1.0
 */
public class UserRegistrationFormController extends AbstractEHelpDeskFormController {

    private UserService userService;
    private RoleService roleService;
    private AuthenticationManager authenticationManager;
    private PropertiesService propertiesService;

    public UserRegistrationFormController() {
        super();
        setCommandClass(User.class);
        setCommandName("user");
        setFormView("register");
        setSuccessView("register");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        if (!propertiesService.getBoolValueByName(PropertiesConstants.ALLOW_USER_REGISTRATION, true))
            throw new AccessDeniedException("Access Denied");
        
        return new User();
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        User user = (User) command;

        if(StringUtils.isNotBlank(user.getChangePassword())){
            user.setPassword(GLTest.getMd5Password(user.getChangePassword()));
        }

        if(user.getId() == null){
            user.setCreationDate(new Date());
            user.setCreator(user.getFirstName() + " " + user.getLastName());
            user.setActive(true);
            
            UserRole userRole = new UserRole();
            userRole.setUser(user);
            Role role = roleService.getRoleById(Role.ROLE_USER_ID);
            userRole.setRole(role);
            user.getUserRoles().add(userRole);
        }

        // force loginid and email to be stored in lower case
        user.setLoginId(StringUtils.lowerCase(user.getLoginId()));
        user.setEmail(StringUtils.lowerCase(user.getEmail()));
        
        userService.saveUser(user);

        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user.getLoginId(), user.getChangePassword());
            token.setDetails(new WebAuthenticationDetails(request));
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            request.getSession().setAttribute(SessionConstants.ATTR_USER, user);
        } catch (Exception e) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }

        return new ModelAndView(new RedirectView("/home.glml", true));
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void afterPropertiesSet() throws Exception {
        if (userService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userService");
        if (roleService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: roleService");
    }

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }
}
