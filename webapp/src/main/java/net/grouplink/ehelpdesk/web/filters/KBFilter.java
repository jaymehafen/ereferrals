package net.grouplink.ehelpdesk.web.filters;

import java.io.Serializable;

/**
 * @author mrollins
 * @version 1.0
 */
public class KBFilter implements Serializable {

    private String searchText = "";
    private String searchType = "0";

    public boolean equals(Object obj) {
        return (obj instanceof KBFilter && equals((KBFilter) obj));
    }

    public boolean equals(KBFilter cf) {
        if (cf == this) return true;

        // Check each field for equality
        boolean result =(searchText == null ? cf.searchText == null : searchText.equals(cf.searchText));
        if (result) result = (searchType == null ? cf.searchType == null : searchType.equals(cf.searchType));

        return result;
    }

    public int hashCode() {
        int hash = 17;
        hash = 37 * hash + (searchText == null ? 0 : searchText.hashCode());
        hash = 37 * hash + (searchType == null ? 0 : searchType.hashCode());
        return hash;
    }

    
    public String getSearchText() {
        return searchText;
    }

    
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    
    public String getSearchType() {
        return searchType;
    }

    
    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

}
