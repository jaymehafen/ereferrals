package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: jaymehafen
 * Date: Aug 31, 2008
 * Time: 10:42:52 PM
 */
public class VendorFormController extends SimpleFormController {
    private VendorService vendorService;

    public VendorFormController() {
        setCommandClass(Vendor.class);
        setCommandName("vendor");
        setFormView("vendorEdit");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Vendor vendor = null;
        String vendorId = ServletRequestUtils.getStringParameter(request, "vendorId");
        if (StringUtils.isNotBlank(vendorId)) {
            vendor = vendorService.getById(new Integer(vendorId));
        }

        if (vendor == null) {
            vendor = new Vendor();
        }

        return vendor;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        Vendor vendor = (Vendor) command;
        vendorService.saveVendor(vendor);

        return new ModelAndView(new RedirectView("vendors.glml"));
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }
}
