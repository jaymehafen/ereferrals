package net.grouplink.ehelpdesk.web.pagedlistproviders;

import net.grouplink.ehelpdesk.common.beans.support.GLPagedListSourceProvider;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.service.TicketFilterService;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Mar 13, 2008
 * Time: 1:57:09 AM
 */
public class TicketFilterProvider implements GLPagedListSourceProvider, Serializable {

    private TicketFilterService ticketFilterService;
    private User user;

    public TicketFilterProvider(User user, TicketFilterService ticketFilterService) {
        this.user = user;
        this.ticketFilterService = ticketFilterService;
    }

    /**
     * Load the List for the given Locale and filter settings.
     * The filter object can be of any custom class, preferably a bean
     * for easy data binding from a request. An instance will simply
     * get passed through to this callback method.
     *
     * @param locale Locale that the List should be loaded for,
     *               or <code>null</code> if not locale-specific
     * @param filter object representing filter settings,
     *               or <code>null</code> if no filter options are used
     * @return the loaded List
     * @see org.springframework.beans.support.RefreshablePagedListHolder#setLocale
     * @see org.springframework.beans.support.RefreshablePagedListHolder#setFilter
     */
    public List loadList(Locale locale, Object filter) {
        TicketFilter ticketFilter = (TicketFilter) filter;
        return ticketFilterService.getTickets(ticketFilter, user);
    }

}
