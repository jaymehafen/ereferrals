package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;
import net.grouplink.ehelpdesk.domain.dashboard.Widget;
import net.grouplink.ehelpdesk.service.DashboardService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.WidgetService;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class DashboardActionController extends MultiActionController {

    private DashboardService dashboardService;
    private WidgetService widgetService;
    private LicenseService licenseService;

    /**
     * Loads all the Dashboard objects to the "dashboardList" view
     *
     * @param request the http request
     * @param response the http response
     * @return the dashboardlist view with all dashboards
     * @throws Exception everytime one occurs
     */
    public ModelAndView listDashboards(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        // Shortcircuit to the explanation page if license doesn't allow the Dashboard feature
        if(!licenseService.isDashboard()){
            return new ModelAndView("dashboards/dashboardAd");
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("dashboards", dashboardService.getAll());
        return new ModelAndView("dashboards/dashboardList", model);
    }

    /**
     * Create a duplicate of the dashboard with the <code>id</code> of the
     * incoming paramter.
     *
     * @param request the http request
     * @param response the http response
     * @return the dashboardlist view with all dashboards
     * @throws Exception everytime one occurs
     */
    public ModelAndView copyDashboard(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer dbId = ServletRequestUtils.getIntParameter(request, "dashId");
        Dashboard dash = dashboardService.getById(dbId);

        Dashboard newDash = dash.clone();
        newDash.setName(getMessageSourceAccessor().getMessage("global.copyOf", new Object[]{dash.getName()}));
        dashboardService.save(newDash);
        dashboardService.flush(); 

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("dashboards", dashboardService.getAll());
        model.put("showHeader", false);

        return new ModelAndView("dashboards/dashboardList", model);
    }

    /**
     * Delete the dashboard with the <code>id</code> of the incoming paramter.
     *
     * @param request the http request
     * @param response the http response
     * @return the dashboardlist view with all dashboards
     * @throws Exception everytime one occurs
     */
    public ModelAndView deleteDashboard(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;
        Integer dbId = ServletRequestUtils.getIntParameter(request, "dashId");
        Dashboard dash = dashboardService.getById(dbId);
        dashboardService.delete(dash);
        dashboardService.flush();

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("dashboards", dashboardService.getAll());

        return new ModelAndView("dashboards/dashboardList", model);
    }

    /**
     * Delete the widget with the <code>id</code> of the incoming paramter.
     *
     * @param request the http request
     * @param response the http response
     * @return the dashboardlist view with all dashboards
     * @throws Exception everytime one occurs
     */
    public ModelAndView deleteWidget(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer widgetId = ServletRequestUtils.getIntParameter(request, "widgetId");
        Integer dashId = ServletRequestUtils.getIntParameter(request, "dashId");

        Widget widget = widgetService.getById(widgetId);
        widgetService.delete(widget);
        dashboardService.flush();

        return new ModelAndView(new RedirectView("dashboardEdit.glml?dashId="+dashId));
    }

    public void setDashboardService(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    public void setWidgetService(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }
}
