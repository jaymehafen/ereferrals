package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketTemplateLaunch;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.TemplateMasterService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.TicketTemplateLaunchService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TicketTemplateLaunchController extends MultiActionController {

    private TemplateMasterService templateMasterService;
    private TicketTemplateLaunchService ticketTemplateLaunchService;
    private TicketService ticketService;
    private PermissionService permissionService;

    public ModelAndView launchTickets(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer tmId = ServletRequestUtils.getIntParameter(request, "tmId");
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        TicketTemplateMaster master = templateMasterService.getById(tmId);

        // check if user has permission to launch this tt
        boolean hasLaunchPermission = ticketTemplateLaunchService.userHasLaunchPermission(user, master);
        if (!hasLaunchPermission) {
            throw new AccessDeniedException("Access is denied");
        }

        templateMasterService.flush();

        templateMasterService.clear();
        
        TicketTemplateLaunch launch = templateMasterService.launchTicketTemplateMaster(master, user);

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("ttLaunchId", launch.getId());

        return new ModelAndView(new RedirectView("/ticketTemplates/displayTTLaunch.glml", true), model);
    }

    public ModelAndView activateTTLaunch(HttpServletRequest request, HttpServletResponse response) throws ServletException, JSONException {
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();
        
        String ttLaunchId = ServletRequestUtils.getStringParameter(request, "ttLaunchId");
        if (StringUtils.isBlank(ttLaunchId)) return null;

        TicketTemplateLaunch launch = ticketTemplateLaunchService.getById(Integer.valueOf(ttLaunchId));
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        List<Ticket> errorTickets = ticketTemplateLaunchService.readyForActivation(launch);
        if (CollectionUtils.isEmpty(errorTickets)) {
            ticketTemplateLaunchService.activate(launch, user);
            model.put("launchSuccessful", true);
        } else {
            model.put("launchError", true);
        }

        model.put("ttLaunchId", launch.getId());

        return new ModelAndView(new RedirectView("/ticketTemplates/displayTTLaunch.glml", true), model);
    }

    public ModelAndView displayLauncher(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        String ttLaunchId = ServletRequestUtils.getStringParameter(request, "ttLaunchId");
        String launchError = ServletRequestUtils.getStringParameter(request, "launchError");
        String launchSuccessful = ServletRequestUtils.getStringParameter(request, "launchSuccessful");
        if(StringUtils.isBlank(ttLaunchId)) throw new RuntimeException("invalid ttLaunchId");

        TicketTemplateLaunch launch = ticketTemplateLaunchService.getById(Integer.valueOf(ttLaunchId));

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("master", launch.getTicketTemplateMaster());

        List<Ticket> tickets = ticketService.getTopLevelTicketsByTicketTemplateLaunch(launch);
        Collections.sort(tickets);

        model.put("ticketDataJSON", getTicketsInJSONForm(tickets));
        Integer firstId = -1;
        if(tickets.size() > 0) firstId = tickets.get(0).getId();
        model.put("firstId", firstId);
        
        model.put("launch", launch);
        if (StringUtils.isNotBlank(launchError)) {
            model.put("launchError", launchError);
        }

        if (StringUtils.isNotBlank(launchSuccessful)) {
            model.put("launchSuccessful", launchSuccessful);
        }

        return new ModelAndView("templateLauncher", model);
    }

    public ModelAndView abortLaunch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;
        
        String ttLaunchId = ServletRequestUtils.getStringParameter(request, "ttLaunchId");
        if (StringUtils.isBlank(ttLaunchId)) throw new RuntimeException("invalid ttLaunchId");
        TicketTemplateLaunch launch = ticketTemplateLaunchService.getById(Integer.valueOf(ttLaunchId));

        ticketTemplateLaunchService.abort(launch);

        return new ModelAndView(new RedirectView("/config/templateMasterList.glml", true));
    }

    private String getTicketsInJSONForm(List<Ticket> tickets) throws JSONException {

        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
        for (Ticket ticket: tickets) {
            jsonObjects.add(getTicketItems(ticket));
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", jsonObjects);

        return store.toString();
    }

    private JSONObject getTicketItems(Ticket ticket) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("id", ticket.getId());
        item.put("name", ticket.getTicketTemplate().getTemplateName());

        if (ticket.getSubtickets().size() > 0) {
            List<Ticket> subtickets = new ArrayList<Ticket>(ticket.getSubtickets());
            Collections.sort(subtickets);
            List<JSONObject> kids = new ArrayList<JSONObject>();
            for (Ticket subticket: subtickets) {
                kids.add(getTicketItems(subticket));
            }

            item.put("children", kids);
        }

        return item;
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setTicketTemplateLaunchService(TicketTemplateLaunchService ticketTemplateLaunchService) {
        this.ticketTemplateLaunchService = ticketTemplateLaunchService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
