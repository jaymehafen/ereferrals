package net.grouplink.ehelpdesk.web.filters;

import net.grouplink.ehelpdesk.common.utils.Constants;
import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.ldap.LdapConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserRoleService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class GLAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter {

    private Log log = LogFactory.getLog(getClass());

    private UserService userService;
    private LdapService ldapService;
    private UserRoleService userRoleService;
    private LicenseService licenseService;
    private UserRoleGroupService userRoleGroupService;
    private GroupService groupService;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        log.debug("called GLAuthenticationProcessingFilter.attemptAuthentication");
        HttpSession session = request.getSession();
        ServletContext application = session.getServletContext();

        // Check for license violations
        checkLicense(application);

        String username = obtainUsername(request);
        String password = obtainPassword(request);

        username = StringUtils.lowerCase(username);
        log.debug("username = " + username);

        // check for empty username or password
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            log.debug("username or password is empty");
            throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }

        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);

        // Place the last username attempted into HttpSession for views
        session.setAttribute(SPRING_SECURITY_LAST_USERNAME_KEY, username);

        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);

        log.debug("looking up user by username");
        User user = userService.getUserByUsername(username);
        if (user == null) {
            log.debug("user was not found");
        } else {
            log.debug("user.getId() = " + user.getId());
        }

        //Authentication through ldap if ldap authentication is on
        Boolean ldapIntegrationEnabled = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
        log.debug("ldapIntegrationEnabled = " + ldapIntegrationEnabled);
        if (ldapIntegrationEnabled && !User.ADMIN_LOGIN.equals(username)) {
            // ldap authentication
            LdapUser ldapUser = ldapAuthentication(username, password, request);
            if (ldapUser != null) {
                // user match was found in ldap
                log.debug("ldapUser.getDn() = " + ldapUser.getDn());
                log.debug("ldapUser.getCn() = " + ldapUser.getCn());
                log.debug("looking up user by username (cn) and user role");
                user = userRoleService.getUserByUserNameAndRoleId(ldapUser.getCn(), Role.ROLE_USER_ID);
                if (user == null) {
                    log.debug("user not found");
                } else {
                    log.debug("user.getId() = " + user.getId());
                }

                //if user not found see try to retrieve user by email address on loginid
                if (user == null) {
                    log.debug("looking up user by username (email) and user role");
                    log.debug("ldapUser.getMail() = " + ldapUser.getMail());
                    user = userRoleService.getUserByUserNameAndRoleId(StringUtils.lowerCase(ldapUser.getMail()), Role.ROLE_USER_ID);
                    if (user == null) {
                        log.debug("user not found");
                    } else {
                        log.debug("user.getId() = " + user.getId());
                    }
                }

                //if user not found see try to retrieve user by email address on email (This is last change to get a user, if not new user will be created)
                if (user == null) {
                    log.debug("looking up user by email (email) and user role");
                    user = userRoleService.getUserByEmailAndRoleId(StringUtils.lowerCase(ldapUser.getMail()), Role.ROLE_USER_ID);
                    if (user == null) {
                        log.debug("user not found");
                    } else {
                        log.debug("user.getId() = " + user.getId());
                    }
                }

                log.debug("creating authReqest, username = " + ldapUser.getCn());
                authRequest = new UsernamePasswordAuthenticationToken(ldapUser.getCn(), password);

                // Allow subclasses to set the "details" property
                setDetails(request, authRequest);

                // Place the last username attempted into HttpSession for views
                session.setAttribute(SPRING_SECURITY_LAST_USERNAME_KEY, ldapUser.getCn());

                //If email is missing from LDAP then do not allow to authenticate or create account
                if (StringUtils.isBlank(ldapUser.getMail())) {
                    log.debug("ldapUser is missing mail attribute");
                    throw new BadCredentialsException(messages.getMessage("ldap.error.loginEmailError", "Missing username or email from LDAP"));
                } else {
                    log.debug("sychronizing ehd user with ldap user");
                    log.debug("ehd user: " + ((user == null) ? "null" : user.getId()));
                    //Synchronizing ldap information with helpdesk user information
                    user = userService.synchronizedUserWithLdap(user, password, ldapUser);
                    userService.flush();
                }
            } else {
                // ldapUser was null, check if ehd fallback is allowed
                log.debug("user not found in ldap, checking is ehd fallback is enabled");
                LdapConfig lc = ldapService.getLdapConfig();
                if (!lc.getEhdFallback()) {
                    // ehd fallback is false and user wasn't found in ldap, don't attempt to authenticate locally
                    log.debug("ehd fallback not enabled, throwing badcredentialsexception");
                    throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials")); 
                } else {
                    log.debug("ehd fallback enabled");
                }
            }
        }

        // Get user and place it in the session
        session.setAttribute(SessionConstants.ATTR_USER, user);
        session.setAttribute(SessionConstants.ATTR_USER_PWD, GLTest.encryptPassword(password));

        if (user != null) initAssetTracker(request, user);

        return this.getAuthenticationManager().authenticate(authRequest);
    }

    private void initAssetTracker(HttpServletRequest request, User user) {
        boolean showAssetTrackerLink = false;
        if (user.getId().equals(User.ADMIN_ID)) {
            List<Group> groups = groupService.getGroups();
            for (Group group : groups) {
                if (StringUtils.equals(group.getAssetTrackerType(), Group.ASSETTRACKERTYPE_EHD)) {
                    showAssetTrackerLink = true;
                    break;
                }
            }
        } else {
            List<UserRoleGroup> urgs = userRoleGroupService.getUserRoleGroupByUserId(user.getId());
            for (UserRoleGroup urg : urgs) {
                if (StringUtils.equals(urg.getGroup().getAssetTrackerType(), Group.ASSETTRACKERTYPE_EHD)) {
                    showAssetTrackerLink = true;
                    break;
                }
            }
        }

        request.getSession().setAttribute(ApplicationConstants.SHOW_EHD_ASSETTRACKER, showAssetTrackerLink);
    }

    private void checkLicense(ServletContext application) {
        licenseService.init(application.getRealPath(LicenseService.LICENSE_FILE));
        if (licenseService.areYouThereLicense()) {
            application.setAttribute(ApplicationConstants.LICENSE_EXISTS, Boolean.TRUE);

            Integer teksInLic = licenseService.getTechnicianCount();
            Integer teksInDB = userRoleGroupService.getTechnicianCount();

            application.setAttribute(ApplicationConstants.TEKS_EXCEEDED, teksInDB > teksInLic);

            Integer wfpInLic = licenseService.getWFParticpantCount();
            Integer wfpInDB = userRoleGroupService.getWFParticipantCount();
            application.setAttribute(ApplicationConstants.WFPARTICIPANTS_EXCEEDED, wfpInDB > wfpInLic);

            Date now = new Date();
            Date hBomb = licenseService.getGlobalBombDate();
            if (hBomb != null) {
                application.setAttribute(ApplicationConstants.GLOBAL_BOMB, now.getTime() > hBomb.getTime());
            } else {
                application.setAttribute(ApplicationConstants.GLOBAL_BOMB, Boolean.FALSE);
            }

            Date releaseDate = licenseService.getReleaseDate();
            Date upgradeDate = licenseService.getUPDate();
            if (upgradeDate != null) {
                application.setAttribute(ApplicationConstants.UPGRADE_DATE_BOMB, releaseDate.getTime() > upgradeDate.getTime());
            } else {
                application.setAttribute(ApplicationConstants.UPGRADE_DATE_BOMB, Boolean.FALSE);
            }

            application.setAttribute(ApplicationConstants.ENTERPRISE, licenseService.isEnterprise());
            if (licenseService.isEnterprise()) {
                application.setAttribute(ApplicationConstants.MAX_GROUPS_ALLOWED, licenseService.getGroupCount());
            } else {
                application.setAttribute(ApplicationConstants.MAX_GROUPS_ALLOWED, Constants.STANDARD_MAX_GROUPS);
            }

            application.setAttribute(ApplicationConstants.DASHBOARDS_ALLOWED, licenseService.isDashboard());

        } else {
            application.setAttribute(ApplicationConstants.LICENSE_EXISTS, Boolean.FALSE);
        }
    }

    private LdapUser ldapAuthentication(String username, String password, HttpServletRequest request) throws AuthenticationException {
        LdapUser lu;
        List<LdapUser> userList = new ArrayList<LdapUser>(ldapService.authenticateUser(username, password));

        if (CollectionUtils.isEmpty(userList)) {
            lu = null;
        } else if (userList.size() > 1) {
            request.getSession().setAttribute("ldapUserList", userList);
            throw new BadCredentialsException(messages.getMessage("login.error.tooManyUsers", "More than one user found"));
        } else {
            lu = userList.get(0);
        }

        return lu;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setUserRoleService(UserRoleService userRoleService) {
        this.userRoleService = userRoleService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }
}
