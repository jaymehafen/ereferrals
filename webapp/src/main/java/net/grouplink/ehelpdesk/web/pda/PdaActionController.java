package net.grouplink.ehelpdesk.web.pda;

import net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder;
import net.grouplink.ehelpdesk.domain.TicketsFilter;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.pagedlistproviders.TicketsProvider;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 */
public class PdaActionController extends MultiActionController implements InitializingBean {

    private TicketService ticketService;
    private StatusService statusService;
    private UserRoleGroupService userRoleGroupService;
    private TicketPriorityService ticketPriorityService;
    private RoleService roleService;

    private LdapService ldapService;
    private LdapFieldService ldapFieldService;

    private UserService userService;
    private GroupService groupService;


    /**
     * Displays the pda version of ticketList.jsp page with tickets assigned to the <code>User</code> on the session and
     * filtered by the <code>UserRoleGroup</code> associated with the 'urgId' request attribute
     *
     * @param request  the HttpServletRequest
     * @param response the HttpServletResponse
     * @return <code>ModelAndView</code> with appropriate ticket list, metadata and the refresh url '/tickets/tlist_tech.glml'
     * @throws javax.servlet.ServletException if there is no 'urgId' request attribute or parameter
     */
    public ModelAndView handleTicketsByAssignedTo(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());
        int urgId = ServletRequestUtils.getIntParameter(request, "urgId", -1);

        List urgList = userRoleGroupService.getUserRoleGroupByUserId(user.getId());
        // Assume this is a user if urgList is empty
        if(urgList.size() == 0){
            response.getOutputStream().println("Sorry. The PDA version of eHelpDesk is for technicians only.");
            return null;
        }

        Collections.sort(urgList);
        // if no urgId is provided, use the first one in the sorted list
        if (urgId == -1) {
            urgId = ((UserRoleGroup) urgList.get(0)).getId().intValue();
        }

        UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(new Integer(urgId));

        GLRefreshablePagedListHolder listHolder =
                (GLRefreshablePagedListHolder) session.getAttribute(SessionConstants.ATTR_TICKET_LIST + "_pda_" + urgId);

        Locale locale = RequestContextUtils.getLocale(request);

        if (listHolder == null) {
            listHolder = new GLRefreshablePagedListHolder();
            listHolder.setPageSize(5);
            listHolder.setSourceProvider(new TicketsProvider(user, urg, locale, "assignedTo", ticketService));
            TicketsFilter tf = new TicketsFilter();
            tf.setGrouping("assignedToMe");
            listHolder.setFilter(tf);
            session.setAttribute(SessionConstants.ATTR_TICKET_LIST + "_pda_" + urgId, listHolder);
        }

        ServletRequestDataBinder bdr = new ServletRequestDataBinder(listHolder, "tickets");
        bdr.bind(request);

        listHolder.setLocale(locale);
        listHolder.setSource(listHolder.getSourceProvider().loadList(locale, listHolder.getFilter()));
        int page = listHolder.getPage();
        listHolder.resort();
        listHolder.setPage(page);

        Map gMap = new HashMap();
        gMap.put("userRoleGroups", urgList);
        gMap.put("listHolder", bdr.getBindingResult().getModel());
        gMap.put("uniqueStatus", statusService.getStatus());
        gMap.put("refreshUrl", "/tickets/tlist_tech.glml");
        gMap.put("urgId", new Integer(urgId));
        gMap.put("showAllGroupTickets", Boolean.valueOf(urg.getGroup().isShowAllGroupTickets()));
        gMap.put("showGroupFilter", Boolean.TRUE);
        gMap.put("priorities", ticketPriorityService.getPriorities());

        return new ModelAndView("ticketListPDA", gMap);
    }

    public void afterPropertiesSet() throws Exception {
        String propName = null;
        if (ticketService == null) propName = "ticketService";
        if (statusService == null) propName += " statusService";
        if (userRoleGroupService == null) propName += " userRoleGroupService";
        if (ticketPriorityService == null) propName += " ticketPriorityService";
        if (ldapService == null) propName += " ldapService";
        if (ldapFieldService == null) propName += " ldapFieldService";
        if (groupService == null) propName += " groupService";
        if (roleService == null) propName += " roleService";

        if (propName != null)
            throw new BeanCreationException(getClass().getName() + " Property not set: " + propName);
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setLdapFieldService(LdapFieldService ldapFieldService) {
        this.ldapFieldService = ldapFieldService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }
}
