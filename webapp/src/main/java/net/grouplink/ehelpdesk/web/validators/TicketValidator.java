package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.service.PropertiesService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class TicketValidator extends BaseValidator {
    private PropertiesService propertiesService;

    public boolean supports(Class clazz) {
		return clazz.equals(Ticket.class);
	}

	public void validate(Object target, Errors errors) {
		Ticket ticket = (Ticket) target;

        List attchList = ticket.getAttchmnt();
        for (Object anAttchList : attchList) {
            MultipartFile mpf = (MultipartFile) anAttchList;

            String maxVal = propertiesService.getPropertiesValueByName(PropertiesConstants.MAX_UPLOAD_SIZE);
            Integer maxUpload;
            if (StringUtils.isBlank(maxVal)) {
                maxUpload = 1;
                propertiesService.saveProperties(PropertiesConstants.MAX_UPLOAD_SIZE, String.valueOf(maxUpload));
            } else {
                maxUpload = Integer.valueOf(maxVal);
            }


            if (mpf.getSize() > (maxUpload * 1024 * 1024)) {
                errors.rejectValue("attchmnt", "ticket.attch.sizeLimit", "Attachment file size is too large");
            }
        }

        if(StringUtils.isNotBlank(ticket.getCc())){
            if(!isValidEmailString(ticket.getCc())){
                errors.rejectValue("cc", "mail.validation.address", "Invalid email address listed.");
            }
		}

        if(StringUtils.isNotBlank(ticket.getBc())){
			if(!isValidEmailString(ticket.getBc())){
                errors.rejectValue("bc", "mail.validation.address", "Invalid email address listed.");
            }
		}

        if (ticket.getStatus() != null && ticket.getStatus().getId().equals(Status.CLOSED)) {
            List<Ticket> subts = ticket.getSubTicketsList();
            for (Ticket subticket : subts) {
                Integer subTicketStatusId = subticket.getStatus().getId();
                if (!subTicketStatusId.equals(Status.CLOSED) && !subTicketStatusId.equals(Status.DELETED)) {
                    errors.rejectValue("status", "ticket.closeParentError", "All sub-tickets must be closed before closing parent");
                    break;
                }
            }
        }
	}

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }
}