package net.grouplink.ehelpdesk.web.controllers.test;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/test/errorPageTest")
public class ErrorPageTestController {
    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "test/errors/index";
    }

    @RequestMapping(value = "/400")
    public void error400(HttpServletResponse response) throws IOException {
        response.sendError(400);
    }

    @RequestMapping(value = "/401")
    public void error401(HttpServletResponse response) throws IOException {
        response.sendError(401);
    }

    @RequestMapping(value = "/403")
    public void error403(HttpServletResponse response) throws IOException {
        response.sendError(403);
    }

    @RequestMapping(value = "/404")
    public void error404(HttpServletResponse response) throws IOException {
        response.sendError(404);
    }

    @RequestMapping(value = "/500")
    public void error500(HttpServletResponse response) throws IOException {
        response.sendError(500);
    }

    @RequestMapping(value = "/503")
    public void error503(HttpServletResponse response) throws IOException {
        response.sendError(503);
    }

    @RequestMapping(value = "/servletException")
    public void servletException() throws ServletException {
        throw new ServletException("test servlet exception");
    }

    @RequestMapping(value = "/runtimeException")
    public void runtimeException() {
        throw new RuntimeException("test runtime exception");
    }

    @RequestMapping(value = "/accessDeniedException")
    public void accessDeniedException() {
        throw new AccessDeniedException("test access denied exception");
    }
}
