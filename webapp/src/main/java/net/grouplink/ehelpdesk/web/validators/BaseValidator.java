package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import org.apache.commons.validator.EmailValidator;
import org.springframework.validation.Validator;

public abstract class BaseValidator implements Validator {

    public static final String PHONE_REGEXP = "(\\({0,1})(\\d{3})(\\){0,1})(\\s|-)*(\\d{3})(\\s|-)*(\\d{4})";

//    public static final String EMAIL_REGEXP = "^[a-zA-Z][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
//    public static final String EMAIL_REGEXP = "^[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[_A-Za-z0-9-]+)";

    public boolean isValidPhoneNumber(String phoneNumber) {
        return phoneNumber.matches(PHONE_REGEXP);
    }

    public boolean isParsableAsNumber(String number){
        try {
            Double.parseDouble(number);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isParsableAsInteger(String number) {
    	try {
    		Integer.parseInt(number);
    		return true;
    	}
        catch (NumberFormatException e) {
    		return false;
    	}
    }

    public boolean isValidEmailString(String emailString) {
        EmailValidator validator = EmailValidator.getInstance();

        String[] eAddresses = emailString.split(RegexUtils.EMAIL_LIST_REGEX);
        for (String eAddress : eAddresses) {
            if (!validator.isValid(eAddress)) return false;
        }
        
        return true;
    }
}
