package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.service.EmailToTicketConfigService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.mail.EmailToTicketService;
import net.grouplink.ehelpdesk.service.mail.MailMonitorFolder;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;

import javax.mail.Folder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * author: zpearce
 * Date: 9/19/11
 * Time: 12:14 PM
 */
@Controller
@RequestMapping("/emailToTicket")
public class EmailToTicketConfigController extends ApplicationObjectSupport {
    @Autowired private EmailToTicketConfigService emailToTicketConfigService;
    @Autowired private EmailToTicketService emailToTicketService;
    @Autowired private LocationService locationService;
    @Autowired private GroupService groupService;
    @Autowired private TicketPriorityService ticketPriorityService;
    @Autowired private Validator validator;
    @Autowired private ConversionService conversionService;

    @ModelAttribute("locations")
    public List<Location> locations() {
        return locationService.getLocations();
    }

    @ModelAttribute("groups")
    public List<Group> groups(@RequestParam(required = false) Integer e2tId) {
        EmailToTicketConfig emailToTicketConfig;
        if(e2tId != null) {
            emailToTicketConfig = emailToTicketConfigService.getById(e2tId);
        } else {
            emailToTicketConfig = new EmailToTicketConfig();
        }
        return filterUsedGroups(groupService.getGroups(), emailToTicketConfig.getGroup());
    }

    @ModelAttribute("priorities")
    public List<TicketPriority> priorities() {
        return ticketPriorityService.getPriorities();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/edit.glml")
    public String getNewForm(Model model) {
        EmailToTicketConfig emailToTicketConfig = new EmailToTicketConfig();
        emailToTicketConfig.setServerType(EmailToTicketConfig.SERVER_TYPE_IMAP);
        emailToTicketConfig.setImapFolder("Inbox");
        emailToTicketConfig.setPort(EmailToTicketConfig.DEF_PORT_IMAP);
        emailToTicketConfig.setSendDefaultEmail(true);
        emailToTicketConfig.setNonEhdUserHandlingMethod(EmailToTicketConfig.NONEHDUSER_REJECT);
        model.addAttribute(emailToTicketConfig);
        return "emailToTicketEdit";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/edit.glml")
    public String createEmailToTicketConfig(@Valid EmailToTicketConfig emailToTicketConfig, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "emailToTicketEdit";
        }
        emailToTicketConfig.setEnabled(true);
        emailToTicketConfigService.save(emailToTicketConfig);
        return "emailToTicketEdit";
    }

    @RequestMapping(method = RequestMethod.GET, params = "e2tId", value = "/edit.glml")
    public String getEditForm(Model model, @RequestParam Integer e2tId) {
        EmailToTicketConfig emailToTicketConfig = emailToTicketConfigService.getById(e2tId);
        model.addAttribute(emailToTicketConfig);
        return "emailToTicketEdit";
    }

    @RequestMapping(method = RequestMethod.PUT, params = "e2tId", value = "/edit.glml")
    public String updateEmailToTicketConfig(Model model, HttpServletRequest request, @RequestParam Integer e2tId) throws Exception {
        //Binding this manually since I'm too lazy at the moment to fix this controller to use restful routes and @ModelAttribute conversion magic, etc.
        EmailToTicketConfig emailToTicketConfig = emailToTicketConfigService.getById(e2tId);
        ServletRequestDataBinder binder = new ServletRequestDataBinder(emailToTicketConfig);
        binder.setConversionService(conversionService);
        binder.bind(request);

        BindingResult bindingResult = binder.getBindingResult();
        validator.validate(emailToTicketConfig, bindingResult);
        model.addAttribute("emailToTicketConfig", emailToTicketConfig);
        if(bindingResult.hasErrors()) {
            return "emailToTicketEdit";
        }
        emailToTicketConfigService.save(emailToTicketConfig);
        return "emailToTicketEdit";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/edit.glml", params = "test")
    public String testConfig(EmailToTicketConfig emailToTicketConfig, BindingResult bindingResult, Model model) {
        //fake group and location so validation passes
        validator.validate(emailToTicketConfig, bindingResult);
        if(bindingResult.hasErrors()) {
            return "emailToTicketEdit";
        }

        MailMonitorFolder mmf = null;
        Folder folder;
        String msg = "";
        String msgType = "testSuccess";
        try {
            mmf = emailToTicketService.connectToFolder(emailToTicketConfig);
            folder = mmf.getFolder();
            msg = getMessageSourceAccessor().getMessage("mailconfig.testSuccess", new Object[]{folder.getName(), folder.getMessageCount()});
        } catch (Exception e){
            msg = getMessageSourceAccessor().getMessage("mailconfig.testError", new Object[]{e.getMessage()});
            msgType = "testError";
        } finally {
            if (mmf != null) mmf.close(false);
        }
        model.addAttribute(msgType, msg);
        return "emailToTicketEdit";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/toggleEnabled.glml")
    public void toggleEnabled(@RequestParam Integer e2tid, @RequestParam String action, HttpServletResponse response) {
        EmailToTicketConfig emailToTicketConfig = emailToTicketConfigService.getById(e2tid);
        emailToTicketConfig.setEnabled(action.equals("start"));
        emailToTicketConfigService.save(emailToTicketConfig);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete.glml")
    public void deleteConfig(@RequestParam Integer e2tid, HttpServletResponse response, HttpSession session) {
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        emailToTicketConfigService.delete(emailToTicketConfigService.getById(e2tid), user);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private List filterUsedGroups(List groups, Group group) {
        // get the list of groups from emailconfigservice
        List e2ts = emailToTicketConfigService.getAll();
        List<Group> logs = new ArrayList<Group>();
        for (Object e2t : e2ts) {
            EmailToTicketConfig emailToTicketConfig = (EmailToTicketConfig) e2t;
            if (!emailToTicketConfig.getGroup().equals(group)) {
                logs.add(emailToTicketConfig.getGroup());
            }
        }
        groups.removeAll(logs);
        return groups;
    }
}
