package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.Address;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 4:39 PM
 */
@Component
public class AddressValidator implements Validator {
    
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Address.class);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "line1", "address.street.required", "Street is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "line1", "address.city.required", "Street is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "line1", "address.state.required", "Street is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "line1", "address.zip.required", "Street is required");
    }
}
