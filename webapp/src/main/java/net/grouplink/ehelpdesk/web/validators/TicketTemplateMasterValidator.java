package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

public class TicketTemplateMasterValidator extends BaseValidator {
    public boolean supports(Class clazz) {
        return clazz.equals(TicketTemplateMaster.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "global.fieldRequired", "This field cannot be empty");
    }
}
