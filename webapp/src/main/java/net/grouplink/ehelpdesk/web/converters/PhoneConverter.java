package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.Phone;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.PhoneService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 11/8/11
 * Time: 1:11 PM
 */
public class PhoneConverter implements Converter<String, Phone> {
    @Autowired
    private PhoneService phoneService;

    public Phone convert(String id){
        if(StringUtils.isBlank(id)) return null;

        try{
            int phoneId = Integer.parseInt(id);
            return phoneService.getById(phoneId);
        } catch (Exception e){
            return null;
        }
    }
}
