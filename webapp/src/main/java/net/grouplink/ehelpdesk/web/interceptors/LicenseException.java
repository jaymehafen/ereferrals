package net.grouplink.ehelpdesk.web.interceptors;

public class LicenseException extends RuntimeException {
    public LicenseException(String s) {
        super(s);
    }
}
