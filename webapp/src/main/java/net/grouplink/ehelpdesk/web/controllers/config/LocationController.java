package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.web.validators.LocationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/locations")
public class LocationController extends ApplicationObjectSupport {

    @Autowired private LocationService locationService;
    @Autowired private LocationValidator locationValidator;

    @RequestMapping(method = RequestMethod.GET)
    public String indexLocations(Model model){
        List<Location> locations = locationService.getAllLocations();
        model.addAttribute(locations);
        return "locations/index";
    }


    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newForm(Model model){
        model.addAttribute(new Location());
        return "locations/form";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute Location location, BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            locationService.saveLocation(location);
        }
        return "locations/form";
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}/edit")
    public String edit(Model model, @PathVariable("id") Location location){
        model.addAttribute(location);
        return "locations/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{location}")
    public String update(@Valid @ModelAttribute("location") Location location, BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            locationService.saveLocation(location);
        }
        return "locations/form";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void delete(@PathVariable("id") Location location, HttpServletResponse response){
        boolean deleted = locationService.deleteLocation(location);
        if(deleted){
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(locationValidator);
    }
}
