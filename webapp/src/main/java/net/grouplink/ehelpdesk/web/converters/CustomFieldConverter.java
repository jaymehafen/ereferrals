package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;


/**
 * author: zpearce
 * Date: 10/21/11
 * Time: 1:58 PM
 */
public class CustomFieldConverter implements Converter<String, CustomField> {
    @Autowired
    private CustomFieldsService customFieldsService;

    public CustomField convert(String id) {
        try {
            int custFieldId = Integer.parseInt(id);
            return customFieldsService.getCustomFieldById(custFieldId);
        } catch (Exception e) {
            return null;
        }
    }
}
