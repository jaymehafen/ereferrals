package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.Phone;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 1/9/12
 * Time: 4:39 PM
 */
@Component
public class PhoneValidator implements Validator {
    
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Phone.class);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneType", "phone.type.required", "Phone Type required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number", "phone.number.required", "Phone Number required");
    }
}
