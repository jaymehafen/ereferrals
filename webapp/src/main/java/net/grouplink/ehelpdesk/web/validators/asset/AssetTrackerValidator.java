package net.grouplink.ehelpdesk.web.validators.asset;

/**
 * Created by IntelliJ IDEA.
 * User: nrollins
 * Date: Oct 26, 2007
 * Time: 3:43:41 PM
 */


import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;


public class AssetTrackerValidator extends BaseValidator {
    private AssetService assetService;

    public boolean supports(Class clazz) {
        return clazz.equals(Asset.class);
    }

    public void validate(Object target, Errors errors) {
        Asset asset = (Asset) target;
        ValidationUtils.rejectIfEmpty(errors, "assetNumber", "assetTracker.required.number", "Asset Number Required");
        ValidationUtils.rejectIfEmpty(errors, "name", "assetTracker.required.name", "Asset Name Required");
        ValidationUtils.rejectIfEmpty(errors, "status", "assetTracker.required.status", "Asset Status Required");
        ValidationUtils.rejectIfEmpty(errors, "assetType", "assetTracker.required.assetType", "Asset Type Required");

        if (StringUtils.isNotBlank(asset.getAssetNumber())) {
            Asset a = assetService.getByAssetNumber(asset.getAssetNumber());
            if (a != null && (asset.getId() == null || !asset.getId().equals(a.getId()))) {
                errors.rejectValue("assetNumber", "assetTracker.assetNumber.notUnique", "The Asset Number already exists");
            }

        }
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
