package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.common.utils.AssetTrackerZen10Config;
import net.grouplink.ehelpdesk.domain.DatabaseConfig;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.List;

/**
 * author: zpearce
 * Date: 9/26/11
 * Time: 12:44 PM
 */
@Component
public class AssetTrackerZen10ConfigValidator extends LocalValidatorFactoryBean implements Validator {
    @Autowired private GroupService groupService;

    public boolean supports(Class<?> clazz) {
        return AssetTrackerZen10Config.class.equals(clazz);
    }

    public void validate(Object target, Errors errors) {
        super.validate(target, errors);

        AssetTrackerZen10Config zen10Config = (AssetTrackerZen10Config) target;
        if (!zen10Config.getEnableZen10()) {
            List<Group> zenGroups = groupService.getGroupsByAssetTrackerType(Group.ASSETTRACKERTYPE_ZEN10);
            if (zenGroups.size() > 0) {
                errors.reject("error", "ZENworks Integration cannot be disabled because there are " + zenGroups.size() + " Groups still configured to use ZENworks Integration");
            }
        }

        if(zen10Config.getEnableZen10()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zenBaseUrl", "NotEmpty.assetTrackerZen10Config.zenBaseUrl");
            try {
                errors.pushNestedPath("zenDatabaseConfig");
                if(!zen10Config.getZenDatabaseConfig().getDbType().equals(DatabaseConfig.SybaseODBC)) {
                    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "serverName", "NotEmpty.assetTrackerZen10Config.serverName");
                    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "databaseName", "NotEmpty.assetTrackerZen10Config.databaseName");
                    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty.assetTrackerZen10Config.username");
                    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.assetTrackerZen10Config.password");
                } else {
                    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dsn", "NotEmpty.assetTrackerZen10Config.dsn");
                }
            } finally {
                errors.popNestedPath();
            }
        }
    }
}
