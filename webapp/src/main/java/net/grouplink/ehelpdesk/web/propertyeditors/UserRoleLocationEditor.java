package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;

import java.beans.PropertyEditorSupport;
import java.util.NoSuchElementException;


public class UserRoleLocationEditor extends PropertyEditorSupport {

    private UserRoleGroup userRoleGroup;
    private UserRoleGroupService urlService;

    public void setAsText(String urgId) throws NoSuchElementException {
        userRoleGroup = urlService.getUserRoleGroupById(new Integer(urgId));
    }

    public Object getValue() {
        return userRoleGroup;
    }

    public void setUserRoleLocationService(UserRoleGroupService userRoleGroupService) {
        this.urlService = userRoleGroupService;
    }

}