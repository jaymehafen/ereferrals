package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

public class DashboardValidator extends BaseValidator{
    public boolean supports(Class clazz) {
        return clazz.equals(Dashboard.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");
    }
}