package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * User: jaymehafen
 * Date: Aug 26, 2008
 * Time: 11:24:11 AM
 */
public class AssetTypeEditor extends PropertyEditorSupport {
    private AssetTypeService assetTypeService;

    public AssetTypeEditor(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }

    public String getAsText() {
        AssetType assetType = (AssetType) getValue();
        return (assetType == null) ? "" : assetType.getId().toString();
    }

    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)) {
            setValue(assetTypeService.getById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public AssetTypeService getAssetTypeService() {
        return assetTypeService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }
}
