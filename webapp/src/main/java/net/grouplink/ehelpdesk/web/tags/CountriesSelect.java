package net.grouplink.ehelpdesk.web.tags;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;


/**
 * Builds an html <code>select</code> box with all the countries provided
 * by the <code>java.util.Locale</code> object with the 2 letter country code
 * as the key of the select option and the display name as the value.
 *
 * @author mrollins
 * @version 1.0
 * @see java.util.Locale
 */
public class CountriesSelect implements Tag, Serializable {

    private PageContext pageContext;
    private Tag parentTag;

    private String name;
    private String onChange;
    private String selectedValue = "US";
    private boolean html = true;
    private String jsSelectVar;
    private String jsOptionVar;

    private String[] countries;
    private Locale locale;

    public int doStartTag() throws JspException {

        locale = RequestContextUtils.getLocale((HttpServletRequest) pageContext.getRequest());
        countries = Locale.getISOCountries();

        String output = html ? getHtmlString() : getDHtmlString();

        try {
            pageContext.getOut().write(output);
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    private String getHtmlString() {
        StringBuilder output = new StringBuilder();
        output.append("<select dojoType=\"dijit.form.FilteringSelect\" style=\"width:15em;\" name=\"").append(name).append("\" ");

        if (StringUtils.isNotBlank(onChange))
            output.append("onchange=\"").append(onChange).append("\" ");

        output.append("><option></option>");

        for (int j = 0; j < countries.length; j++) {
            String countryCode = countries[j];
            Locale countryLoc = new Locale("en", countryCode);
            String cntryname = countryLoc.getDisplayCountry(locale);
            if (!cntryname.equals(countryCode)) {
                output.append("<option value=\"").append(countryCode).append("\"");
                if (StringUtils.equals(selectedValue, countryCode)) {
                    output.append(" selected ");
                }
                output.append(">");
                output.append(cntryname);
                output.append("</option>");
            }
        }
        output.append("</select>");

        return output.toString();
    }

    private String getDHtmlString() {
        StringBuilder output = new StringBuilder();

        output.append(jsSelectVar).append(" = document.createElement(\"SELECT\");\n");
        output.append(jsSelectVar).append(".name = '").append(name).append("';\n");

        if (StringUtils.isNotBlank(onChange)) {
            output.append(jsSelectVar).append(".onchange = '").append(onChange).append("';\n");
        }

        for (int j = 0; j < countries.length; j++) {
            String countryCode = countries[j];
            Locale countryLoc = new Locale("en", countryCode);
            String cntryname = countryLoc.getDisplayCountry(locale);
            if (!cntryname.equals(countryCode)) {

                //output.append(jsOptionVar).append(" = document.createElement(\"OPTION\");\n");
            	output.append(jsOptionVar).append(" = new Option('")
            		.append(cntryname.replaceAll("'","\\\\'"))
            		.append("','").append(countryCode).append("',false,");
                if (StringUtils.equals(selectedValue, countryCode)) {
                    //output.append(jsOptionVar).append(".selected = 'true';\n");
                	output.append("true");
                } else {
                	output.append("false");
                }
            	output.append(");\n");
                //output.append(jsOptionVar).append(".text = '");
                //output.append(cntryname.replaceAll("'", "\\\\'"));
                //output.append("';\n");
                //output.append(jsOptionVar).append(".value = '").append(countryCode).append("';\n");
                //if (StringUtils.equals(selectedValue, countryCode)) {
                //    output.append(jsOptionVar).append(".selected = 'true';\n");
                //}
                //output.append(jsSelectVar).append(".appendChild(").append(jsOptionVar).append(");\n");
            	output.append(jsSelectVar).append(".options[" + j + "] = ").append(jsOptionVar).append(";\n");
            }
        }
        return output.toString();
    }

    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        pageContext = null;
        parentTag = null;
        name = null;
    }

    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    public void setParent(Tag tag) {
        this.parentTag = tag;
    }

    public Tag getParent() {
        return parentTag;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOnChange(String onChange) {
        this.onChange = onChange;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public void setHtml(boolean html) {
        this.html = html;
    }

    public void setJsSelectVar(String jsSelectVar) {
        this.jsSelectVar = jsSelectVar;
    }

    public void setJsOptionVar(String jsOptionVar) {
        this.jsOptionVar = jsOptionVar;
    }
}
