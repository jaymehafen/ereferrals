package net.grouplink.ehelpdesk.web.dwr;

import net.grouplink.ehelpdesk.common.collaboration.FreeBusyResult;
import net.grouplink.ehelpdesk.common.collaboration.NameAndEmail;
import net.grouplink.ehelpdesk.common.collaboration.Session;
import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.service.AttachmentService;
import net.grouplink.ehelpdesk.service.TicketHistoryService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.service.collaboration.CollaborationService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class DwrTicketService {

    private UserService userService;
    private TicketService ticketService;
    private TicketHistoryService ticketHistoryService;
    private CollaborationService collaborationService;
    private AttachmentService attachmentService;
    private PermissionService permissionService;

    public void deleteAttachment(Integer ticketId, Integer attachmentId, HttpServletRequest request) {
        Ticket ticket = ticketService.getTicketById(ticketId);
        Set<Attachment> attachments = ticket.getAttachments();
        for (Iterator<Attachment> iterator = attachments.iterator(); iterator.hasNext();) {
            Attachment attachment = iterator.next();
            if (attachment.getId().equals(attachmentId)) {
                iterator.remove();
                attachmentService.delete(attachment);
                break;
            }
        }

        User seshUser = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        ticketService.saveTicket(ticket, seshUser);
    }

    public void deleteTicketHistoryAttachment(Integer ticketHistId, Integer attachmentId, HttpServletRequest request) {
        TicketHistory ticketHistory = ticketHistoryService.getTicketHistoryById(ticketHistId);
        Set<Attachment> attachments = ticketHistory.getAttachments();
        for (Iterator<Attachment> iterator = attachments.iterator(); iterator.hasNext();) {
            Attachment attachment = iterator.next();
            if(attachment.getId().equals(attachmentId)) {
                iterator.remove();
                attachmentService.delete(attachment);
                break;
            }
        }

        User seshUser = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);
        ticketHistoryService.saveTicketHistory(ticketHistory, seshUser);
    }

    public void applyContactToAllSubtickets(Integer contactId, Integer ticketId, HttpServletRequest request) {
        User user = userService.getUserById(contactId);
        Ticket ticket = ticketService.getTicketById(ticketId);

        // get the session user for audit trail
        User seshUser = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        setContact(ticket, user);
        ticketService.saveTicket(ticket, seshUser);
    }

    private void setContact(Ticket ticket, User user) {
        // recursively sets user on all tickets
        ticket.setContact(user);

        Set<Ticket> subtickets = ticket.getSubtickets();
        for (Ticket subticket : subtickets) {
            setContact(subticket, user);
        }
    }

    public FreeBusyResult busySearch(String emails, Date startDate, Date startTime, Integer durationAmount,
                                     String durationUnit, HttpServletRequest request) {

        String[] emailList = emails.split("\\s*;\\s*|\\s*,\\s*|\\s+");
        NameAndEmail[] nameAndEmails = new NameAndEmail[emailList.length];
        for (int email = 0; email < emailList.length; email++) {
            nameAndEmails[email] = new NameAndEmail(null, emailList[email]);
        }

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);

        Calendar time = Calendar.getInstance();
        time.setTime(startTime);

        start.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
        start.set(Calendar.MINUTE, time.get(Calendar.MINUTE));

        Calendar end = Calendar.getInstance();
        end.setTime(start.getTime());

        if(durationUnit.equals("collaboration.duration.minutes")){
            end.add(Calendar.MINUTE, durationAmount);
        } else if(durationUnit.equals("collaboration.duration.hours")){
            end.add(Calendar.HOUR_OF_DAY, durationAmount);
        } else if(durationUnit.equals("collaboration.duration.days")){
            end.add(Calendar.DATE, durationAmount);
        }

        User user = (User)request.getSession().getAttribute(SessionConstants.ATTR_USER);
        String encPwd = (String)request.getSession().getAttribute(SessionConstants.ATTR_USER_PWD);
//        Session session = (Session) request.getSession().getAttribute(SessionConstants.COLLABORATION_SESSION);
        Session session = collaborationService.getCollaborationSession(user, GLTest.decryptPassword(encPwd));

        return collaborationService.freeBusySearch(nameAndEmails, start, end, session);
    }

    public Phone getPrimaryPhoneForUser(Integer userId) {
        User user = userService.getUserById(userId);
        return user.getPrimaryPhone();
    }

    public Address getPrimaryAddressForUser(Integer userId) {
        Address address = null;
        User user = userService.getUserById(userId);
        List<Address> addresses = user.getAddressList();
        if (addresses.size() > 0) {
            address = addresses.get(0);
        }

        return address;
    }

    public void deleteTicketIdList(Integer[] ticketIds, HttpServletRequest request){
        User user = userService.getLoggedInUser();
        for (Integer ticketId : ticketIds) {
            Ticket ticket = ticketService.getTicketById(ticketId);

            // check permissions
            if (permissionService.hasGroupPermission("group.deleteTicket", ticket.getGroup())) {
                ticketService.deleteTicket(ticket, user);
            }
        }
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setCollaborationService(CollaborationService collaborationService) {
        this.collaborationService = collaborationService;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
