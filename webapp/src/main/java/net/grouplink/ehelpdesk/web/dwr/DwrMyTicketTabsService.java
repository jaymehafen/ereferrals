package net.grouplink.ehelpdesk.web.dwr;

import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.MyTicketTabService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;

import javax.servlet.http.HttpServletRequest;

public class DwrMyTicketTabsService {
    private TicketFilterService ticketFilterService;
    private MyTicketTabService myTicketTabService;

    public void saveNewTab(String name, Integer ticketFilterId, Integer order, HttpServletRequest request) {
        MyTicketTab mtt = new MyTicketTab();
        User user = (User) request.getSession(false).getAttribute(SessionConstants.ATTR_USER);
        mtt.setUser(user);
        mtt.setActive(true);
        mtt.setName(name);
        mtt.setTicketFilter(ticketFilterService.getById(ticketFilterId));
        mtt.setOrder(order);
        myTicketTabService.save(mtt);
    }

    public void deleteTab(Integer myTicketTabId, HttpServletRequest request) {
        MyTicketTab mtt = myTicketTabService.getById(myTicketTabId);
        User user = (User) request.getSession(false).getAttribute(SessionConstants.ATTR_USER);
        if (mtt.getUser().equals(user)) {
            myTicketTabService.delete(mtt); 
        }
    }

    public Integer getTicketFilterIdFromTabId(Integer myTicketTabId){
        MyTicketTab mtt = myTicketTabService.getById(myTicketTabId);
        return mtt.getTicketFilter().getId();
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setMyTicketTabService(MyTicketTabService myTicketTabService) {
        this.myTicketTabService = myTicketTabService;
    }
}
