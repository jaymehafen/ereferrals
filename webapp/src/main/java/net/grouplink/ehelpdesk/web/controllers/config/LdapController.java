package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.TreeNode;
import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.naming.Context;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Date: Jul 23, 2007
 * Time: 1:57:32 PM
 */
public class LdapController extends MultiActionController {

    private LdapService ldapService;
    private LdapFieldService ldapFieldService;

    public ModelAndView ldapSubFolders(HttpServletRequest request,
                                       HttpServletResponse response) throws Exception {

        String jsonString;
        String data = ServletRequestUtils.getStringParameter(request, "data");

        JSONObject jsonParser = new JSONObject(data);
        JSONObject node = (JSONObject) jsonParser.get("node");
        jsonString = node.getString("widgetId");
        if (StringUtils.isNotBlank(jsonString)) {
            if (jsonString.equals("mainNode")) {
                jsonString = "";
            }

            try {
                List<TreeNode> list = ldapService.getFirstLevelTree(jsonString);
                JSONArray jsonArray = new JSONArray();
                for (TreeNode tr : list) {
                    JSONObject json = new JSONObject();
                    json.put("title", tr.getName());
                    json.put("isFolder", tr.isFolder());
                    json.put("widgetId", tr.getName());

                    jsonArray.put(json);
                }

                jsonString = jsonArray.toString();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.println(jsonString);
        out.flush();
        return null;
    }

    public ModelAndView addLdapField(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;
        Map<String, Object> map = new HashMap<String, Object>();

        String ldapFieldName = ServletRequestUtils.getStringParameter(request, "ldapField");
        if (StringUtils.isNotBlank(ldapFieldName)) {
            LdapField ldapField = new LdapField();
            ldapField.setName(ldapFieldName);
            ldapFieldService.saveLdapField(ldapField);
        }

        List ldapFieldList = ldapFieldService.getLdapFields();
        map.put("ldapFieldList", ldapFieldList);

        return new ModelAndView("ldapFieldTable", map);
    }

    public ModelAndView removeLdapField(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Map<String, Object> map = new HashMap<String, Object>();

        Integer ldapFieldId = ServletRequestUtils.getIntParameter(request, "ldapFieldId");
        if (ldapFieldId != null) {
            LdapField ldapField = ldapFieldService.getLdapFieldById(ldapFieldId);
            if (ldapField != null) {
                ldapFieldService.deleteLdapField(ldapField);
            }
        }

        List ldapFieldList = ldapFieldService.getLdapFields();
        map.put("ldapFieldList", ldapFieldList);

        return new ModelAndView("ldapFieldTable", map);
    }

    public ModelAndView modifyLdapField(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Map<String, Object> map = new HashMap<String, Object>();

        Integer ldapFieldId = ServletRequestUtils.getIntParameter(request, "ldapFieldId");
        String fieldLabel = ServletRequestUtils.getStringParameter(request, "fieldLabel");
        Boolean showOnTicket = ServletRequestUtils.getBooleanParameter(request, "showOnTicket");
        if (ldapFieldId != null) {
            LdapField ldapField = ldapFieldService.getLdapFieldById(ldapFieldId);
            if (ldapField != null) {
                if (fieldLabel != null) {
                    ldapField.setLabel(fieldLabel);
                }
                if (showOnTicket != null) {
                    ldapField.setShowOnTicket(showOnTicket);
                }
            }
            ldapFieldService.saveLdapField(ldapField);
        }

        List ldapFieldList = ldapFieldService.getLdapFields();
        map.put("ldapFieldList", ldapFieldList);

        return new ModelAndView("ldapFieldTable", map);
    }

    public ModelAndView fetchDns(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String ipAddress = ServletRequestUtils.getStringParameter(request, "ipAddress");

        JSONObject retVal = new JSONObject();
        JSONArray items = new JSONArray();

        if (StringUtils.isNotBlank(ipAddress)) {
            Hashtable<String, Object> env = new Hashtable<String, Object>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ipAddress);
            env.put(Context.REFERRAL, "follow");
            try {
                LdapContext context = new InitialLdapContext(env, null);

                Attributes attr = context.getAttributes("", new String[]{"namingcontexts"});
                Attribute att = attr.get("namingcontexts");

                if (att != null) {
                    for (int i = 0; i < att.size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("dn", att.get(i));
                        items.put(obj);
                    }
                }

            } catch (Exception e) {
                logger.error("error getting InitialLdapContext", e);
            }
        }

        retVal.put("label", "dn");
        retVal.put("identifier", "dn");
        retVal.put("items", items);

        response.getOutputStream().println(retVal.toString());

        return null;
    }

    public ModelAndView ldapFieldSetup(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();

        String dn = ServletRequestUtils.getStringParameter(request, "dn");
        if (StringUtils.isNotBlank(dn)) {
            LdapUser ldapUser = ldapService.getLdapUser(dn);
            model.put("ldapUser", ldapUser);
        }

        List ldapFieldList = ldapFieldService.getLdapFields();
        model.put("ldapFieldList", ldapFieldList);


        return new ModelAndView("ldapFieldSetup", model);
    }

    public void afterPropertiesSet() throws Exception {
        if (ldapService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ldapService");
        if (ldapFieldService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ldapFieldService");
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setLdapFieldService(LdapFieldService ldapFieldService) {
        this.ldapFieldService = ldapFieldService;
    }

}
