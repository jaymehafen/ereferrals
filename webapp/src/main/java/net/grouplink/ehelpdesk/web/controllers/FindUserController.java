package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/findUser")
public class FindUserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String findUser() {
        return "findUser/findUser";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/userDisplayName/{userId}")
    public void userDisplayName(@PathVariable Integer userId, HttpServletResponse response) throws IOException {
        User user = userService.getUserById(userId);
        response.setContentType("text/plain");
        response.getWriter().print(user.getFirstName() + " " + user.getLastName());
    }
}
