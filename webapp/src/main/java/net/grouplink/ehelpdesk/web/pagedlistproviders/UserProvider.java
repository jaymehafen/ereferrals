package net.grouplink.ehelpdesk.web.pagedlistproviders;

import net.grouplink.ehelpdesk.common.beans.support.GLPagedListSourceProvider;
import net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder;
import net.grouplink.ehelpdesk.domain.UserFilter;
import net.grouplink.ehelpdesk.service.UserService;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * @author mrollins
 * @version 1.0
 */
public class UserProvider implements GLPagedListSourceProvider, Serializable {

    private UserService userService;


    public UserProvider(UserService userService) {
        this.userService = userService;
    }

    public List loadList(Locale loc, Object listHolder) {
        List list;
        GLRefreshablePagedListHolder listH = (GLRefreshablePagedListHolder) listHolder;
        UserFilter uf = (UserFilter) listH.getFilter();
        list = userService.getFilteredUsers(listH.getSort().getProperty(), listH.getSort().isAscending(), uf.getGroup(), uf.getFirstName(), uf.getLastName(), uf.getRole(), uf.getActive(), uf.getLoginId(), uf.getEmail());
        return list;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
