package net.grouplink.ehelpdesk.web.dwr;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterCriteriaHelper;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.MyTicketTabService;
import net.grouplink.ehelpdesk.service.ReportService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.service.TicketHistoryService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.dwr.dto.MassUpdateDto;
import net.grouplink.ehelpdesk.web.dwr.dto.TicketFilterDto;
import net.grouplink.ehelpdesk.web.dwr.dto.TicketFilterLineItem;
import net.grouplink.ehelpdesk.web.dwr.dto.TicketFilterUsageDto;
import net.grouplink.ehelpdesk.web.tf.TicketFilterFormController;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.support.ApplicationObjectSupport;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DwrTicketFilterService extends ApplicationObjectSupport {
    private TicketFilterService ticketFilterService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private TicketService ticketService;
    private UserService userService;
    private UserRoleGroupService userRoleGroupService;
    private TicketPriorityService priorityService;
    private StatusService statusService;
    private TicketHistoryService ticketHistoryService;
    private MyTicketTabService myTicketTabService;
    private ReportService reportService;
    private PermissionService permissionService;

    public TicketFilterDto getTicketFilterById(int id) {
        TicketFilterDto dto = new TicketFilterDto();
        TicketFilter filter = ticketFilterService.getById(id);
        dto.setTicketFilter(filter);
        dto.setColumnNames(getColumnNameMap(filter));
        return dto;
    }

    public TicketFilterDto getTicketFilterFromSession(HttpServletRequest request) {
        TicketFilterDto dto = new TicketFilterDto();
        TicketFilter tf =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);
        dto.setTicketFilter(tf);
        dto.setColumnNames(getColumnNameMap(tf));
        return dto;
    }

    public TicketFilterDto getFindUserTicketFilter(int userId) {
        TicketFilterDto dto = new TicketFilterDto();
        TicketFilter tf = ticketFilterService.getFindUserTicketFilter(userId);
        dto.setTicketFilter(tf);
        dto.setColumnNames(getColumnNameMap(tf));
        return dto;
    }

    public Map<String, String> getColumnNameMap(TicketFilter filter) {
        Map<String, String> map = new HashMap<String, String>();
        for (String column : filter.getColumnOrder()) {
            String formattedColumn;
            if (column.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                formattedColumn = StringUtils.removeStart(column, TicketFilter.COLUMN_CUSTOMFIELDPREFIX);
            } else if (column.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                formattedColumn = StringUtils.removeStart(column, TicketFilter.COLUMN_SURVEYPREFIX);
            } else {
                formattedColumn = getMessageSourceAccessor().getMessage(column);
            }
            
            formattedColumn = StringEscapeUtils.escapeXml(formattedColumn);
            map.put(column, formattedColumn);
        }

        return map;
    }


    /**
     * This method requires the the operators and values arrays are bot equal in size (1 operator corresponding to the
     * same indexed value in the value array)
     *
     * @param type      the type of field
     * @param operators an array of integer operators
     * @param values    the values that correspond to the operator
     * @param request   the http request for querying the session
     * @return a list of TicketFilterLineItems
     */
    public List<TicketFilterLineItem> addLineItems(String type, Integer[] operators, String[] values,
                                                   HttpServletRequest request) {

        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        Integer[] items = null;
        Integer[] opItems = null;

        for (int i = 0; i < operators.length; i++) {
            Integer operator = operators[i];
            String value = values[i];

            if (type.equals("categoryIds")) {
                items = ticketFilter.getCategoryIds();
                opItems = ticketFilter.getCategoryIdsOps();
                if (!ArrayUtils.contains(ticketFilter.getCategoryIds(), new Integer(value))) {
                    items = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryIds(), new Integer(value));
                    ticketFilter.setCategoryIds(items);

                    opItems = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryIdsOps(), operator);
                    ticketFilter.setCategoryIdsOps(opItems);
                }
            } else if (type.equals("categoryOptionIds")) {
                items = ticketFilter.getCategoryOptionIds();
                opItems = ticketFilter.getCategoryOptionIdsOps();
                if (!ArrayUtils.contains(ticketFilter.getCategoryOptionIds(), new Integer(value))) {
                    items = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryOptionIds(), new Integer(value));
                    ticketFilter.setCategoryOptionIds(items);

                    opItems = (Integer[]) ArrayUtils.add(ticketFilter.getCategoryOptionIdsOps(), operator);
                    ticketFilter.setCategoryOptionIdsOps(opItems);
                }
            }
        }

        // build a list of ticketfilterlineitems from the 2 arrays that were just populated
        List<TicketFilterLineItem> lineItemList = new ArrayList<TicketFilterLineItem>();
        if (items != null) {
            for (int i = 0, itemsLength = items.length; i < itemsLength; i++) {
                Integer item = items[i];
                TicketFilterLineItem ticketFilterLineItem = new TicketFilterLineItem();
                ticketFilterLineItem.setDisplayName(resolveDisplayName(type, item));
                ticketFilterLineItem.setIndex(i);
                ticketFilterLineItem.setOperatorName(resolveDisplayName("operator", opItems[i]));
                ticketFilterLineItem.setPropertyName(type);
                ticketFilterLineItem.setDeleteMethodName("ticketFilterEditDelItem");

                lineItemList.add(i, ticketFilterLineItem);
            }
        }
        return lineItemList;
    }

    public List<TicketFilterLineItem> addCFLineItem(String cfName, String divId, Integer operatorId, String fieldValue,
                                                    HttpServletRequest request) {
        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        String unescapedCfName = StringEscapeUtils.unescapeXml(cfName);
        
        Map<String, List<TicketFilterCriteriaHelper>> customFieldsMap = ticketFilter.getCustomFields();
        List<TicketFilterCriteriaHelper> thisCFList = customFieldsMap.get(unescapedCfName);
        if (thisCFList == null) thisCFList = new ArrayList<TicketFilterCriteriaHelper>();

        TicketFilterCriteriaHelper tfCFHelper = new TicketFilterCriteriaHelper();
        tfCFHelper.setOperatorId(operatorId);
        tfCFHelper.setValue(fieldValue);
        thisCFList.add(tfCFHelper);
        customFieldsMap.put(unescapedCfName, thisCFList);
        ticketFilter.setCustomFields(customFieldsMap);

        List<TicketFilterLineItem> lineItemList = new ArrayList<TicketFilterLineItem>();
        for (int i = 0; i < thisCFList.size(); i++) {
            TicketFilterCriteriaHelper ticketFilterCFHelper = thisCFList.get(i);
            TicketFilterLineItem ticketFilterLineItem = new TicketFilterLineItem();
            ticketFilterLineItem.setDisplayName(ticketFilterCFHelper.getValue());
            ticketFilterLineItem.setIndex(i);
            ticketFilterLineItem.setOperatorName(resolveDisplayName("operator", ticketFilterCFHelper.getOperatorId()));
            ticketFilterLineItem.setPropertyName(divId);
            ticketFilterLineItem.setCfName(cfName);
            ticketFilterLineItem.setDeleteMethodName("ticketFilterEditDelCFItem");

            lineItemList.add(i, ticketFilterLineItem);
        }
        
        return lineItemList;
    }

    public List<TicketFilterLineItem> deleteCFLineItem(String divId, Integer row, String cfName,
                                                       HttpServletRequest request) {
        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        String unescapedCfName = StringEscapeUtils.unescapeXml(cfName);

        Map<String, List<TicketFilterCriteriaHelper>> customFieldsMap = ticketFilter.getCustomFields();
        List<TicketFilterCriteriaHelper> thisCFList = customFieldsMap.get(unescapedCfName);
        thisCFList.remove(row.intValue());
        customFieldsMap.put(unescapedCfName, thisCFList);
        ticketFilter.setCustomFields(customFieldsMap);

        List<TicketFilterLineItem> lineItemList = new ArrayList<TicketFilterLineItem>();
        for (int i = 0; i < thisCFList.size(); i++) {
            TicketFilterCriteriaHelper ticketFilterCFHelper = thisCFList.get(i);
            TicketFilterLineItem ticketFilterLineItem = new TicketFilterLineItem();
            ticketFilterLineItem.setDisplayName(ticketFilterCFHelper.getValue());
            ticketFilterLineItem.setIndex(i);
            ticketFilterLineItem.setOperatorName(resolveDisplayName("operator", ticketFilterCFHelper.getOperatorId()));
            ticketFilterLineItem.setPropertyName(divId);
            ticketFilterLineItem.setCfName(cfName);
            ticketFilterLineItem.setDeleteMethodName("ticketFilterEditDelCFItem");

            lineItemList.add(i, ticketFilterLineItem);
        }
        return lineItemList;
    }

    public List<TicketFilterLineItem> addSurveyLineItem(String question, String divId, Integer operatorId, String fieldValue,
                                                        HttpServletRequest request) {
        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        String unescapedQuestion = StringEscapeUtils.unescapeXml(question);

        Map<String, List<TicketFilterCriteriaHelper>> surveysMap = ticketFilter.getSurveys();
        List<TicketFilterCriteriaHelper> criteriaHelperList = surveysMap.get(unescapedQuestion);
        if (criteriaHelperList == null) criteriaHelperList = new ArrayList<TicketFilterCriteriaHelper>();

        TicketFilterCriteriaHelper tfcHelper = new TicketFilterCriteriaHelper();
        tfcHelper.setOperatorId(operatorId);
        tfcHelper.setValue(fieldValue);
        criteriaHelperList.add(tfcHelper);
        surveysMap.put(unescapedQuestion, criteriaHelperList);
        ticketFilter.setSurveys(surveysMap);

        List<TicketFilterLineItem> lineItemList = new ArrayList<TicketFilterLineItem>();
        for (int i = 0; i < criteriaHelperList.size(); i++) {
            TicketFilterCriteriaHelper criteriaHelper = criteriaHelperList.get(i);
            TicketFilterLineItem ticketFilterLineItem = new TicketFilterLineItem();
            ticketFilterLineItem.setDisplayName(criteriaHelper.getValue());
            ticketFilterLineItem.setIndex(i);
            ticketFilterLineItem.setOperatorName(resolveDisplayName("operator", criteriaHelper.getOperatorId()));
            ticketFilterLineItem.setPropertyName(divId);
            ticketFilterLineItem.setCfName(question);
            ticketFilterLineItem.setDeleteMethodName("ticketFilterEditDelSurveyItem");

            lineItemList.add(i, ticketFilterLineItem);
        }
        return lineItemList;
    }

    public List<TicketFilterLineItem> deleteSurveyLineItem(String divId, Integer row, String question,
                                                           HttpServletRequest request) {
        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        String unescapedQuestion = StringEscapeUtils.unescapeXml(question);

        Map<String, List<TicketFilterCriteriaHelper>> surveysMap = ticketFilter.getSurveys();
        List<TicketFilterCriteriaHelper> criteriaHelperList = surveysMap.get(unescapedQuestion);
        criteriaHelperList.remove(row.intValue());
        surveysMap.put(unescapedQuestion, criteriaHelperList);
        ticketFilter.setCustomFields(surveysMap);

        List<TicketFilterLineItem> lineItemList = new ArrayList<TicketFilterLineItem>();
        for (int i = 0; i < criteriaHelperList.size(); i++) {
            TicketFilterCriteriaHelper criteriaHelper = criteriaHelperList.get(i);
            TicketFilterLineItem ticketFilterLineItem = new TicketFilterLineItem();
            ticketFilterLineItem.setDisplayName(criteriaHelper.getValue());
            ticketFilterLineItem.setIndex(i);
            ticketFilterLineItem.setOperatorName(resolveDisplayName("operator", criteriaHelper.getOperatorId()));
            ticketFilterLineItem.setPropertyName(divId);
            ticketFilterLineItem.setCfName(question);
            ticketFilterLineItem.setDeleteMethodName("ticketFilterEditDelSurveyItem");

            lineItemList.add(i, ticketFilterLineItem);
        }
        return lineItemList;
    }

    public List<TicketFilterLineItem> clearProperty(String propName, String cfName, HttpServletRequest request) {
        TicketFilter ticketFilter =
                (TicketFilter) request.getSession().getAttribute(TicketFilterFormController.TFC_FORM_TICKETFILTER);

        if(propName.startsWith("ticketFilter")) {
            String prop = propName.replace("ticketFilter.", "");

            if (prop.equals("ticketNumber")) {
                ticketFilter.setTicketNumbers(null);
                ticketFilter.setTicketNumbersOps(null);
            } else if (prop.equals("priority")) {
                ticketFilter.setPriorityIds(null);
                ticketFilter.setPriorityIdsOps(null);
            } else if (prop.equals("status")) {
                ticketFilter.setStatusIds(null);
                ticketFilter.setStatusIdsOps(null);
            } else if (prop.equals("contact")) {
                ticketFilter.setContactIds(null);
                ticketFilter.setContactIdsOps(null);
            } else if (prop.equals("submitted")) {
                ticketFilter.setSubmittedByIds(null);
                ticketFilter.setSubmittedByIdsOps(null);
            } else if (prop.equals("assigned")) {
                ticketFilter.setAssignedToIds(null);
                ticketFilter.setAssignedToIdsOps(null);
            } else if (prop.equals("location")) {
                ticketFilter.setLocationIds(null);
                ticketFilter.setLocationIdsOps(null);
            } else if (prop.equals("group")) {
                ticketFilter.setGroupIds(null);
                ticketFilter.setGroupIdsOps(null);
            } else if (prop.equals("category")) {
                ticketFilter.setCategoryIds(null);
                ticketFilter.setCategoryIdsOps(null);
            } else if (prop.equals("catopt")) {
                ticketFilter.setCategoryOptionIds(null);
                ticketFilter.setCategoryOptionIdsOps(null);
            } else if (prop.equals("subject")) {
                ticketFilter.setSubject(null);
                ticketFilter.setSubjectOps(null);
            } else if (prop.equals("note")) {
                ticketFilter.setNote(null);
                ticketFilter.setNoteOps(null);
            } else if (prop.equals("historySubject")) {
                ticketFilter.setHistorySubject(null);
                ticketFilter.setHistorySubjectOps(null);
            } else if (prop.equals("historyNote")) {
                ticketFilter.setHistoryNote(null);
                ticketFilter.setHistoryNoteOps(null);
            } else if (prop.equals("surveys")) {
                ticketFilter.setSurveyQuestionId(null);
                ticketFilter.setSurveyQuestionValue(null);
            } else if (prop.equals("zenAssetIds")) {
                ticketFilter.setZenAssetIds(null);
            } else if (prop.equals("created")) {
                ticketFilter.setCreatedDate(null);
                ticketFilter.setCreatedDate2(null);
                ticketFilter.setCreatedDateLastX(null);
                ticketFilter.setCreatedDateOperator(null);
            } else if (prop.equals("modified")) {
                ticketFilter.setModifiedDate(null);
                ticketFilter.setModifiedDate2(null);
                ticketFilter.setModifiedDateLastX(null);
                ticketFilter.setModifiedDateOperator(null);
            } else if (prop.equals("estimated")) {
                ticketFilter.setEstimatedDate(null);
                ticketFilter.setEstimatedDate2(null);
                ticketFilter.setEstimatedDateLastX(null);
                ticketFilter.setEstimatedDateOperator(null);
            } else if (prop.equals("worktime")) {
                ticketFilter.setWorkTimeHours(null);
                ticketFilter.setWorkTimeMinutes(null);
                ticketFilter.setWorkTimeOperator(null);
            } else if (prop.equals("zenworks10Asset")) {
                ticketFilter.setZenAssetIds(null);
                ticketFilter.setZenAssetIdOps(null);
            }
        } else if (propName.startsWith("customField.") && cfName != null) {
            Map<String, List<TicketFilterCriteriaHelper>> customFieldsMap = ticketFilter.getCustomFields();
            customFieldsMap.remove(cfName);
            ticketFilter.setCustomFields(customFieldsMap);
        }

        // property is cleared. return an empty list
        return new ArrayList<TicketFilterLineItem>();
    }

    private String resolveDisplayName(String prop, Object nameOrId) {
        if (prop.equals("categoryIds")) {
            return categoryService.getCategoryById((Integer) nameOrId).getName();
        } else if (prop.equals("categoryOptionIds")) {
            return categoryOptionService.getCategoryOptionById((Integer) nameOrId).getName();
        } else if (prop.equals("operator")) {
            String opName = getMessageSourceAccessor().getMessage("ticketFilter.op.equals");
            switch ((Integer) nameOrId) {
                case 9: // Equals
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.equals");
                    break;
                case 10: // Not Equals
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.notEquals");
                    break;
                case 11: // Greater Than
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.greaterThan");
                    break;
                case 12: // Less Than
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.lessThan");
                    break;
                case 13: // Contains
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.contains");
                    break;
                case 14: // Not Contains
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.notContains");
                    break;
                case 15: // Starts with
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.startsWith");
                    break;
                case 16: // Ends with
                    opName = getMessageSourceAccessor().getMessage("ticketFilter.op.endsWith");
                    break;
            }
            return opName;
        }
        return (String) nameOrId;
    }

    public String massUpdate(MassUpdateDto mudto, Integer[] tids, HttpServletRequest request) {
        User user = userService.getLoggedInUser();
        List<Integer> successfulTids = new ArrayList<Integer>();
        List<Integer> unsuccessfulTids = new ArrayList<Integer>();
        // loop through ticket ids, check if user is authorized to mod ticket
        for (Integer tid : tids) {
            Ticket ticket = ticketService.getTicketById(tid);
            if (permissionService.hasGroupPermission("group.massEdit", ticket.getGroup())) {
                if (ArrayUtils.contains(mudto.getCbx_workTime(), "on")) {
                    if (StringUtils.isNotBlank(mudto.getWorkTime())) {
                        String[] wtParts = mudto.getWorkTime().split(":");
                        int hoursInSecs = Integer.parseInt(wtParts[0]) * 60 * 60;
                        int minsInSecs = Integer.parseInt(wtParts[1]) * 60;
                        int secsInSecs = Integer.parseInt(wtParts[2]);
                        ticket.setWorkTime(hoursInSecs + minsInSecs + secsInSecs);
                    } else {
                        ticket.setWorkTime(0);
                    }
                }
                if (ArrayUtils.contains(mudto.getCbx_contact(), "on")) {
                    if (mudto.getContact() != null) ticket.setContact(userService.getUserById(mudto.getContact()));
                }
                if (ArrayUtils.contains(mudto.getCbx_assignedTo(), "on")) {
                    if (mudto.getAssignedTo() != null)
                        ticket.setAssignedTo(userRoleGroupService.getUserRoleGroupById(mudto.getAssignedTo()));
                }
                if (ArrayUtils.contains(mudto.getCbx_priority(), "on")) {
                    if (mudto.getPriority() != null)
                        ticket.setPriority(priorityService.getPriorityById(mudto.getPriority()));
                }
                if (ArrayUtils.contains(mudto.getCbx_status(), "on")) {
                    if (mudto.getStatus() != null) ticket.setStatus(statusService.getStatusById(mudto.getStatus()));
                }
                if (ArrayUtils.contains(mudto.getCbx_estimatedDate(), "on")) {
                    ticket.setEstimatedDate(mudto.getEstimatedDate());
                }
                if (ArrayUtils.contains(mudto.getCbx_addComment(), "on")) {
                    TicketHistory ticketHistory = new TicketHistory();
                    ticketHistory.setActive(true);
                    ticketHistory.setCommentType(0);
                    ticketHistory.setCreatedDate(new Date());
                    ticketHistory.setNote(mudto.getAddCommentNote());
                    ticketHistory.setSubject(mudto.getAddCommentSubject());
                    ticketHistory.setTextFormat(true);
                    ticketHistory.setTicket(ticket);
                    ticketHistory.setUser(user);
                    ticketHistoryService.saveTicketHistory(ticketHistory, user);
                }
                if (ArrayUtils.contains(mudto.getCbx_groupAsChildren(), "on")) {
                    Integer parentId = mudto.getGroupAsChildren();
                    Ticket parent = ticketService.getTicketById(parentId);
                    ticket.setParent(parent);
                }
                try {
                    ticketService.saveTicket(ticket, user);
                    successfulTids.add(tid);
                } catch (Exception e) {
                    unsuccessfulTids.add(tid);
                }
            } else {
                unsuccessfulTids.add(tid);
            }
        }
        StringBuilder sb = new StringBuilder(getMessageSourceAccessor().getMessage("ticketList.massUpdate.success"));
        for (Integer tid : successfulTids) {
            sb.append(" ").append(tid);
        }

        if (unsuccessfulTids.size() > 0) {
            sb.append("\n");
            sb.append(getMessageSourceAccessor().getMessage("ticketList.massUpdate.unsuccess"));
            for (Integer tid : unsuccessfulTids) {
                sb.append(" ").append(tid);
            }
        }

        return sb.toString();
    }

    public Integer getUsageCount(Integer ticketFilterId) {
        TicketFilter tf = ticketFilterService.getById(ticketFilterId);
        return ticketFilterService.getUsageCount(tf);
    }

    public TicketFilterUsageDto getTicketFilterUsage(Integer ticketFilterId) {
        TicketFilter tf = ticketFilterService.getById(ticketFilterId);
        List<MyTicketTab> tabs = myTicketTabService.getByTicketFilter(tf);
        List<Report> reports = reportService.getByTicketFilter(tf);

        TicketFilterUsageDto dto = new TicketFilterUsageDto();
        dto.setTabs(tabs);
        dto.setReports(reports);

        return dto;
    }

    public List<MyTicketTab> getMyTicketTabsForFilter(Integer ticketFilterId) {
        TicketFilter tf = ticketFilterService.getById(ticketFilterId);
        return myTicketTabService.getByTicketFilter(tf);
    }

    public List<Category> getCategoriesByGroupId(Integer groupId) {
        List<Category> categories = categoryService.getCategoryByGroupId(groupId);
        Collections.sort(categories);
        return categories;
    }

    public List<CategoryOption> getCatOptsByCatId(Integer catId) {
        List<CategoryOption> catOpts = categoryOptionService.getCategoryOptionByCategoryId(catId);
        Collections.sort(catOpts);
        return catOpts;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setPriorityService(TicketPriorityService priorityService) {
        this.priorityService = priorityService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setMyTicketTabService(MyTicketTabService myTicketTabService) {
        this.myTicketTabService = myTicketTabService;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
