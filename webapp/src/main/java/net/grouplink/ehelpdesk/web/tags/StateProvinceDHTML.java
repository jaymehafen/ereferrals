package net.grouplink.ehelpdesk.web.tags;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 */
public class StateProvinceDHTML implements Tag, Serializable {

    private PageContext pageContext;
    private Tag parentTag;

    private String name;
    private String onChange;
    private String selectedValue;

    private Map valueMap;

    public int doStartTag() throws JspException {

        StringBuilder output = new StringBuilder();
        output.append("<select name=\"").append(name).append("\" ");

        if (StringUtils.isNotBlank(onChange))
            output.append("onchange=\"").append(onChange).append("\" ");

        output.append("><option></otpion>");

        Locale locale = RequestContextUtils.getLocale((HttpServletRequest) pageContext.getRequest());
        String countries[] = Locale.getISOCountries();
        for (int j = 0; j < countries.length; j++) {
            String countryCode = countries[j];
            Locale countryLoc = new Locale("en", countryCode);
            String name = countryLoc.getDisplayCountry(locale);
            if (!name.equals(countryCode)) {
                output.append("<option value=\"").append(countryCode).append("\"");
                if(StringUtils.equals(selectedValue, countryCode)){
                       output.append(" selected ");
                }
                output.append(">");
                output.append(name);
                output.append("</option>");
            }
        }
        output.append("</select>");


        try {
            pageContext.getOut().write(output.toString());
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        pageContext = null;
        parentTag = null;
        name = null;
        onChange = null;
        selectedValue = null;
    }

    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    public void setParent(Tag tag) {
        this.parentTag = tag;
    }

    public Tag getParent() {
        return parentTag;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setOnChange(String onChange) {
        this.onChange = onChange;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }


    private void buildValueMap(){
        valueMap = new LinkedHashMap();
        valueMap.put("AL", "Alabama");
        valueMap.put("AK", "Alaska");
        valueMap.put("AZ", "Arizona");
        valueMap.put("AR", "Arkansas");
        valueMap.put("CA", "California");
        valueMap.put("CO", "Colorado");
        valueMap.put("CT", "Connecticut");
        valueMap.put("DE", "Delaware");
        valueMap.put("DC", "District of Columbia");
        valueMap.put("FL", "Florida");
        valueMap.put("GA", "Georgia");
        valueMap.put("HI", "Hawaii");
        valueMap.put("ID", "Idaho");
        valueMap.put("IL", "Illinois");
        valueMap.put("IN", "Indiana");
        valueMap.put("IA", "Iowa");
        valueMap.put("KS", "Kansas");
        valueMap.put("KY", "Kentucky");
        valueMap.put("LA", "Louisiana");
        valueMap.put("ME", "Maine");
        valueMap.put("MD", "Maryland");
        valueMap.put("MA", "Massachusetts");
        valueMap.put("MI", "Michigan");
        valueMap.put("MN", "Minnesota");
        valueMap.put("MS", "Mississippi");
        valueMap.put("MO", "Missouri");
        valueMap.put("MT", "Montana");
        valueMap.put("NE", "Nebraska");
        valueMap.put("NV", "Nevada");
        valueMap.put("NH", "New Hampshire");
        valueMap.put("NJ", "New Jersey");
        valueMap.put("NM", "New Mexico");
        valueMap.put("NY", "New York");
        valueMap.put("NC", "North Carolina");
        valueMap.put("ND", "North Dakota");
        valueMap.put("OH", "Ohio");
        valueMap.put("OK", "Oklahoma");
        valueMap.put("OR", "Oregon");
        valueMap.put("PA", "Pennsylvania");
        valueMap.put("RI", "Rhode Island");
        valueMap.put("SC", "South Carolina");
        valueMap.put("SD", "South Dakota");
        valueMap.put("TN", "Tennessee");
        valueMap.put("TX", "Texas");
        valueMap.put("UT", "Utah");
        valueMap.put("VT", "Vermont");
        valueMap.put("VA", "Virginia");
        valueMap.put("WA", "Washington");
        valueMap.put("WV", "West Virginia");
        valueMap.put("WI", "Wisconsin");
        valueMap.put("WY", "Wyoming");

        valueMap.put("AB", "Alberta");
        valueMap.put("BC", "British Columbia");
        valueMap.put("MB", "Manitoba");
        valueMap.put("NB", "New Brunswick");
        valueMap.put("NF", "Newfoundland");
        valueMap.put("NT", "Northwest Territories");
        valueMap.put("NU", "Nunavut");
        valueMap.put("NS", "Nova Scotia	");
        valueMap.put("ON", "Ontario");
        valueMap.put("PE", "Prince Edward Island");
        valueMap.put("QC", "Quebec");
        valueMap.put("SK", "Saskatchewan");
        valueMap.put("YT", "Yukon");

    }

}
