package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 9/21/11
 * Time: 9:56 AM
 */
public class GroupConverter implements Converter<String, Group> {
    @Autowired private GroupService groupService;

    public Group convert(String id) {
        try {
            int groupId = Integer.parseInt(id);
            return groupService.getGroupById(groupId);
        } catch (Exception e) {
            return null;
        }
    }
}
