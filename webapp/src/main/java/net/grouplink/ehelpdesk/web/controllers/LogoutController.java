package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author mrollins
 * @version 1.0
 */
public class LogoutController extends MultiActionController {

     public ModelAndView logout(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        if(user == null){
            return new ModelAndView(new RedirectView("/index.jsp", true));
        }
        else{
            return new ModelAndView(new RedirectView("/log.out", true));
        }
    }

    public ModelAndView login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        return new ModelAndView("login");
    }
}
