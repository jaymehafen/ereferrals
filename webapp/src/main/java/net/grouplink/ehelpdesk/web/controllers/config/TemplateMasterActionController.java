package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.RecurrenceScheduleService;
import net.grouplink.ehelpdesk.service.TemplateMasterService;
import net.grouplink.ehelpdesk.service.TicketTemplateService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemplateMasterActionController extends MultiActionController {

    private TemplateMasterService templateMasterService;
    private RecurrenceScheduleService recurrenceScheduleService;
    private GroupService groupService;
    private TicketTemplateService ticketTemplateService;
    private LicenseService licenseService;
    private PermissionService permissionService;

    public ModelAndView listTemplateMasters(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();

        // Shortcircuit to the explanation page if license doesn't allow the Dashboard feature
        if(!licenseService.isTicketTemplates()){
            return new ModelAndView("ticketTemplates/ticketTemplateAd");
        }

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        // get a list of groups that:
        //  - the user has privilege group.ticketTemplateManagement
        //  - the user has privileghe group.launchNonPublicTicketTemplates
        //  - has public ticket templates
        List<TicketTemplateMaster> allMasters = templateMasterService.getAllPublic();
        List<Group> groups = new ArrayList<Group>();
        for (Group group : groupService.getGroups()) {
            if (permissionService.hasGroupPermission(user, "group.ticketTemplateManagement", group)) {
                groups.add(group);
                continue;
            }

            if (permissionService.hasGroupPermission(user, "group.launchNonPublicTicketTemplates", group)) {
                groups.add(group);
                continue;
            }

            if (groupHasPublicTemplates(group, allMasters)) {
                groups.add(group);
                continue;
            }
        }

        model.put("groups", groups);
        return new ModelAndView("ticketTemplates/templateMasterList", model);
    }

    private boolean groupHasPublicTemplates(Group group, List<TicketTemplateMaster> allMasters) {
        for (TicketTemplateMaster ticketTemplateMaster : allMasters) {
            if (ticketTemplateMaster.getGroup().equals(group)) {
                return true;
            }
        }

        return false;
    }

    public ModelAndView copyTemplateMaster(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer tmid = ServletRequestUtils.getIntParameter(request, "tmId");
        TicketTemplateMaster master = templateMasterService.getById(tmid);

        // check permission
        if (!permissionService.hasGroupPermission("group.ticketTemplateManagement", master.getGroup())) {
            throw new AccessDeniedException("Access is denied");
        }

        // clone master
        TicketTemplateMaster newTTM = templateMasterService.cloneTicketTemplateMaster(master);

        newTTM.setName(getMessageSourceAccessor().getMessage("global.copyOf", new Object[]{master.getName()}));
        templateMasterService.save(newTTM);
        templateMasterService.flush();

        return listTemplateMasters(request, response);
    }

    public ModelAndView deleteTemplateMaster(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer tmid = ServletRequestUtils.getIntParameter(request, "tmId");
        TicketTemplateMaster master = templateMasterService.getById(tmid);

        // check permission
        if (!permissionService.hasGroupPermission("group.ticketTemplateManagement", master.getGroup())) {
            throw new AccessDeniedException("Access is denied");
        }

        templateMasterService.delete(master);
        templateMasterService.flush();

        return new ModelAndView(new RedirectView("/config/templateMasterList.glml", true));
    }

    public ModelAndView toggleScheduler(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        String action = ServletRequestUtils.getStringParameter(request, "action");
        Integer recSchedId = ServletRequestUtils.getIntParameter(request, "recSchedId");
        RecurrenceSchedule rs = recurrenceScheduleService.getById(recSchedId);
        
        // check permission
        if (!permissionService.hasGroupPermission("group.ticketTemplateManagement", rs.getTicketTemplateMaster().getGroup())) {
            throw new AccessDeniedException("Access is denied");
        }

        if (action.equals("start")) {
            rs.setRunning(true);
        } else {
            rs.setRunning(false);
        }

        recurrenceScheduleService.save(rs);

        return null;
    }

    public ModelAndView addTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer masterId = ServletRequestUtils.getIntParameter(request, "tmId");
        Integer parentId = ServletRequestUtils.getIntParameter(request, "tpId", -1);
        String templateName = ServletRequestUtils.getStringParameter(request, "name", "New Name");

        TicketTemplateMaster master = templateMasterService.getById(masterId);

        // Build the new ticket template object
        TicketTemplate template = new TicketTemplate();
        template.setTemplateName(templateName);
        template.setParent(parentId == -1 ? null : ticketTemplateService.getById(parentId));

        master.addTemplate(template);
        templateMasterService.save(master);
        templateMasterService.flush();

        response.setContentType("application/json");
        return null;
    }

    public ModelAndView deleteTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;
        response.setContentType("application/json");
        
        Integer ttId = ServletRequestUtils.getIntParameter(request, "ttId", -1);
        if (ttId > -1) {
            TicketTemplate ticketTemplate = ticketTemplateService.getById(ttId);
            ticketTemplateService.delete(ticketTemplate);
        }

        return null;
    }

    public ModelAndView renameTemplate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;

        Integer tpId = ServletRequestUtils.getIntParameter(request, "tpId", -1);
        String templateName = ServletRequestUtils.getStringParameter(request, "name", "New Name");
        if (tpId > -1) {
            TicketTemplate ticketTemplate = ticketTemplateService.getById(tpId);
            ticketTemplate.setTemplateName(templateName);
            ticketTemplateService.save(ticketTemplate);
            ticketTemplateService.flush();
        }
        response.setContentType("application/json");
        return null;
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setRecurrenceScheduleService(RecurrenceScheduleService recurrenceScheduleService) {
        this.recurrenceScheduleService = recurrenceScheduleService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
