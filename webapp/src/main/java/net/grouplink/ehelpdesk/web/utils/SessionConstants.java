package net.grouplink.ehelpdesk.web.utils;

/**
 * User: mrollins
 * Date: Jan 4, 2007
 * Time: 3:47:45 PM
 */
public class SessionConstants {

    public static final String ATTR_USER = "User";
    public static final String ATTR_USER_PWD = "UserToken";
    public static final String ATTR_USER_ID = "User_Id";
    public static final String ATTR_TICKET_LIST = "Ticket_List";
    public static final String ATTR_ASSET_LIST = "Asset_List";
    public static final String ATTR_USER_TICKET_LIST = "User_Ticket_List";
    public static final String ATTR_ADV_SEARCH_LIST = "advancedSearchList";
    public static final String ATTR_KB_LIST = "KB_List";
    public static final String ATTR_KB_MANAGEMENT_LIST = "KB_Management_List";
    public static final String ATTR_TICKET_SEARCH_RESULT_LIST = "Ticket Search Result List";
    public static final String TICKETSETTINGS_VIEW = "ticketSettings.view";
    public static final String TICKETSETTINGS_WRITE = "ticketSettings.write";
    public static final String COLLABORATION_SESSION = "collaborationSession";
    public static final String ATTR_USER_LIST = "User_Management_List";
    public static final String ATTR_ASSIGMENT_LIST = "Assigment_List";
    public static final String HAS_BACKUPS = "hasBackUp";
    public static final String SHOW_SYSTEM_MSG = "showSystemMessage";
    public static final String SHOW_SETTINGS_BUTTON = "showSettingsButton";
    public static final String CONFIG_MENU_OPTIONS = "configMenuOptions";

    // prevent instantiation
    private SessionConstants(){}
}
