package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * User: jaymehafen
 * Date: Sep 11, 2008
 * Time: 2:11:38 PM
 */
public class AssetStatusEditor extends PropertyEditorSupport {
    private AssetStatusService assetStatusService;

    public AssetStatusEditor(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }

    public String getAsText() {
        AssetStatus assetStatus = (AssetStatus) getValue();
        return (assetStatus == null) ? "" : assetStatus.getId().toString();
    }

    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)) {
            setValue(assetStatusService.getById(new Integer(text)));
        } else {
            setValue(null);
        }
    }
}
