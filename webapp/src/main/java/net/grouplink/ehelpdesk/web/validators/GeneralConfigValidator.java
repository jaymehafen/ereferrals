package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.GeneralConfig;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * User: jaymehafen
 * Date: Feb 13, 2008
 * Time: 5:07:05 PM
 */
public class GeneralConfigValidator extends BaseValidator {
    public boolean supports(Class clazz) {
        return clazz.equals(GeneralConfig.class);
    }

    public void validate(Object target, Errors errors) {
        GeneralConfig gc = (GeneralConfig)target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "baseUrl", "generalConfig.baseUrl.required", "Base URL is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "maxUploadSize", "generalConfig.maxUpload.required", "Max Upload Size is required");
        if(gc.getSessionTimeoutLength() == null){
            errors.rejectValue("sessionTimeoutLength", "generalConfig.timeout.required", "Session Timeout Length is required");
        } else if(gc.getSessionTimeoutLength() < 2 || gc.getSessionTimeoutLength() > 480){
            errors.rejectValue("sessionTimeoutLength", "sessiontimeout.minimum", "Session timeout length must be at least 2 minutes and less than 480 (8 hours).");
        }
    }
}
