package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.Survey;
import net.grouplink.ehelpdesk.service.SurveyService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class SurveyValidator implements Validator {

	private SurveyService surveyService;

    public boolean supports(Class clazz) {
		return clazz.equals(Survey.class);
	}

    public void validate(Object target, Errors errors) {
		Survey survey = (Survey) target;
        if (surveyService.hasDuplicateSurveyName(survey)) {
			errors.rejectValue("name", "surveys.validate.name.notunique", "Survey name already exists");
		}
        if(StringUtils.isBlank(survey.getName())){
            errors.rejectValue("name", "surveys.validate.name.empty", "Survey name cannot be empty");
        }
    }

    public void setSurveyService(SurveyService surveyService) {
        this.surveyService = surveyService;
    }
}
