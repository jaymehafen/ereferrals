package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;

@Controller
public class EditTicketController extends TicketController {
    @RequestMapping(method = RequestMethod.GET, value = "/{ticket}/edit")
    // TODO make sure we really don't need @PathVariable
    public String edit(Model model, @ModelAttribute("ticket") Ticket ticket, HttpServletRequest request, @RequestParam(value = "groupId", required = false) Group group) throws Exception {

        if(group != null) {
            ticket.setGroup(group);
            ticket.setCategory(null);
            ticket.setCategoryOption(null);
            ticket.setAssignedTo(null);
        }

        if (!permissionService.canUserViewTicket(userService.getLoggedInUser(), ticket)) {
            model.addAttribute("ticketAccess", Boolean.TRUE);
            model.addAttribute("message", "ticket.access.denied");
            model.addAttribute("redMessage", "global.accessDenied");
            return "noTicketAccess";
        }

        model.addAttribute(ticket);
        ticketService.initializeAuditList(ticket);
        return "tickets/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{ticket}")
    public String update(Model model, @ModelAttribute("ticket") Ticket ticket, BindingResult bindingResult, HttpServletRequest request) throws Exception {
        if(!processTicket(ticket, request, bindingResult)) {
            return "tickets/form";
        }

        if (StringUtils.isNotBlank(ServletRequestUtils.getStringParameter(request, "dt"))) {
            ticket.setDisplayWorkTime(ServletRequestUtils.getStringParameter(request, "dt"));
        } else if (StringUtils.isNotBlank(ServletRequestUtils.getStringParameter(request, "wt"))) {
            ticket.setDisplayWorkTime(ServletRequestUtils.getStringParameter(request, "wt"));
        }

        request.setAttribute("flash.successfulSave", true);
        request.setAttribute("flash.showTicketHistory", true);
        request.setAttribute("flash.ticketChanges", URLEncoder.encode(getTextChanges(ticket), "UTF-8"));
        model.asMap().clear();
        return "redirect:/tickets/" + ticket.getId() + "/edit";
    }
}
