package net.grouplink.ehelpdesk.web.forms;

import net.grouplink.ehelpdesk.domain.TicketPriority;
import org.springframework.util.AutoPopulatingList;

import javax.validation.Valid;

/**
 * author: zpearce
 * Date: 11/29/11
 * Time: 2:49 PM
 */
public class PriorityForm {
    @Valid
    private AutoPopulatingList<TicketPriority> priorities = new AutoPopulatingList<TicketPriority>(TicketPriority.class);

    public AutoPopulatingList<TicketPriority> getPriorities() {
        return priorities;
    }

    public void setPriorities(AutoPopulatingList<TicketPriority> priorities) {
        this.priorities = priorities;
    }

    public TicketPriority getPriorityById(int id) {
        TicketPriority returnVal = null;
        for(TicketPriority tp : getPriorities()) {
            if(tp.getId() == id) {
                returnVal = tp;
                break;
            }
        }
        return returnVal;
    }
}
