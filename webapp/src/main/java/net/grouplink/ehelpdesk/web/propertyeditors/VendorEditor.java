package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * User: jaymehafen
 * Date: Aug 26, 2008
 * Time: 3:17:07 PM
 */
public class VendorEditor extends PropertyEditorSupport {
    private VendorService vendorService;

    public VendorEditor(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    public String getAsText() {
        Vendor v = (Vendor) getValue();
        return (v == null) ? "" : v.getId().toString();
    }

    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)) {
            setValue(vendorService.getById(new Integer(text)));
        } else {
            setValue(null);
        }
    }
}
