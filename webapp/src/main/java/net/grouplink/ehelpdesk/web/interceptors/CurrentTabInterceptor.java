package net.grouplink.ehelpdesk.web.interceptors;

import net.grouplink.ehelpdesk.web.beans.TabMapping;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * author: zpearce
 * Date: 8/12/11
 * Time: 10:58 AM
 */
public class CurrentTabInterceptor extends HandlerInterceptorAdapter {
    private TabMapping tabMapping;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if(modelAndView != null) {
            boolean isRedirectView = modelAndView.getView() instanceof RedirectView;
            boolean isViewObject = modelAndView.getView() != null;
            boolean viewNameStartsWithRedirect = (modelAndView.getViewName() == null ? true : modelAndView.getViewName().startsWith(UrlBasedViewResolver.REDIRECT_URL_PREFIX));

            if(modelAndView.hasView() && (!isViewObject && !viewNameStartsWithRedirect) || (isViewObject && !isRedirectView)) {
                String currentTab = tabMapping.getCurrentTab(StringUtils.removeStart(request.getRequestURI(), request.getContextPath()));
                Map model = modelAndView.getModel();
                if(model != null) {
                    modelAndView.getModel().put("currentTab", currentTab);
                }
            }
        }
    }

    public void setTabMapping(TabMapping tabMapping) {
        this.tabMapping = tabMapping;
    }
}
