package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.acl.Permission;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;


public class PermissionConverter implements Converter<String, Permission> {
    @Autowired
    private PermissionService permissionService;

    public Permission convert(String id) {
        if (StringUtils.isBlank(id)) return null;

        return permissionService.getPermissionById(Integer.valueOf(id));
    }
}
