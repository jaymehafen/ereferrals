package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * User: jaymehafen
 * Date: Sep 24, 2008
 * Time: 6:18:35 PM
 */
public class VendorValidator extends BaseValidator {
    public boolean supports(Class clazz) {
        return clazz.equals(Vendor.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");
    }
}