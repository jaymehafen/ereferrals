package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.TicketTemplateMaster;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.*;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MyTicketsController extends MultiActionController {

    private UserService userService;
    private GroupService groupService;
    private MyTicketTabService myTicketTabService;
    private UserRoleGroupService userRoleGroupService;
    private TicketFilterService ticketFilterService;
    private TicketTemplateLaunchService ticketTemplateLaunchService;

    public ModelAndView handleMyTickets(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;
        Map<String, Object> model = new HashMap<String, Object>();
        HttpSession session = request.getSession(true);
        if (session.getAttribute("theme") == null)
            throw new Exception("&#80ut the i&#110terce&#112tor back where you &#102ound it.");
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());
        Boolean userOnly = userService.isEndUser(user);

        List<UserRoleGroup> urgList = userRoleGroupService.getUserRoleGroupByUserId(user.getId());
        Boolean hasBackUp = Boolean.FALSE;
        Collections.sort(urgList);
        for (UserRoleGroup urg : urgList) {
            if (urg.getBackUpTech() != null) {
                hasBackUp = Boolean.TRUE;
            }
        }
        session.setAttribute(SessionConstants.HAS_BACKUPS, hasBackUp);

        Map<Group, List<TicketTemplateMaster>> groupTicketTemplateMap = new HashMap<Group, List<TicketTemplateMaster>>();
        List<Group> groupList = groupService.getGroups();
        for (Group group : groupList) {
            List<TicketTemplateMaster> ticketTemplateMasterList = new ArrayList<TicketTemplateMaster>();
            groupTicketTemplateMap.put(group, ticketTemplateMasterList);

            for (TicketTemplateMaster ticketTemplateMaster : group.getTemplateMasters()) {
                // check if user has launch permission for each template
                if (ticketTemplateLaunchService.userHasLaunchPermission(user, ticketTemplateMaster)) {
                    ticketTemplateMasterList.add(ticketTemplateMaster);
                }
            }
        }

        List<MyTicketTab> myTicketTabs = myTicketTabService.getByUser(user);
        if (CollectionUtils.isEmpty(myTicketTabs)) {
            myTicketTabs = myTicketTabService.initializeTabs(user);
        }

        Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();
        for (MyTicketTab myTicketTab : myTicketTabs ) {
            countMap.put(myTicketTab.getTicketFilter().getId(), ticketFilterService.getTicketCount(myTicketTab.getTicketFilter(), user));
        }

        // check for tabs with missing ticket filter 
        for (Iterator<MyTicketTab> myTicketTabIterator = myTicketTabs.iterator(); myTicketTabIterator.hasNext();) {
            MyTicketTab myTicketTab = myTicketTabIterator.next();
            if (myTicketTab.getTicketFilter() == null) {
                myTicketTabIterator.remove();
            }
        }

        model.put("countMap", countMap);
        model.put("userOnly", userOnly);
        model.put("user", user);
        model.put("myTicketTabs", myTicketTabs);
        model.put("groupTicketTemplateMap", groupTicketTemplateMap);

        return new ModelAndView("myTickets2", model);
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setMyTicketTabService(MyTicketTabService myTicketTabService) {
        this.myTicketTabService = myTicketTabService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setTicketTemplateLaunchService(TicketTemplateLaunchService ticketTemplateLaunchService) {
        this.ticketTemplateLaunchService = ticketTemplateLaunchService;
    }
}
