package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: mrollins
 * Date: 4/18/12
 * Time: 4:04 PM
 */
@Controller
@RequestMapping(value = "/session")
public class SessionUtilityController {

    @RequestMapping(method = RequestMethod.GET, value = "keepAlive")
    public void keepAlive(HttpServletResponse response){
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "closeSystemMessage")
    public void closeSystemMsg(HttpSession session, HttpServletResponse response){
        session.setAttribute(SessionConstants.SHOW_SYSTEM_MSG, Boolean.FALSE);
        response.setStatus(HttpServletResponse.SC_OK);
    }

}
