package net.grouplink.ehelpdesk.web.ip;

import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Role;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.MyTicketTabService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.AssignmentService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryOptionEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.GroupEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.LocationEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.StatusEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.TicketPriorityEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserRoleGroupEditor;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: Nov 16, 2009
 * Time: 4:41:46 PM
 */
public class IpTicketFormController extends SimpleFormController {

    private TicketService ticketService;
    private UserService userService;
    private TicketPriorityService ticketPriorityService;
    private StatusService statusService;
    private MailService mailService;
    private LdapService ldapService;
    private LdapFieldService ldapFieldService;
    private LocationService locationService;
    private GroupService groupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private UserRoleGroupService userRoleGroupService;
    private AssignmentService assignmentService;
    private CustomFieldsService customFieldsService;
    private MyTicketTabService myTicketTabService;
    private PermissionService permissionService;

    public IpTicketFormController() {
        super();
        setCommandClass(Ticket.class);
        setCommandName("ticket");
        setFormView("ticketEdit");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer tid = ServletRequestUtils.getIntParameter(request, "tid");
        Integer groupId = ServletRequestUtils.getIntParameter(request, "groupId", -1);
        Group group = null;
        if(groupId >-1) group = groupService.getGroupById(groupId);

        User seshUser = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        Ticket ticket;
        if (tid == null) {
            User fullSeshUser = userService.getUserById(seshUser.getId());
            ticket = createNewTicket(fullSeshUser, fullSeshUser);
        } else {
            ticket = ticketService.getTicketById(tid);
        }

        if(group!= null){
            ticket.setGroup(group);
        }

        return ticket;
    }

    private Ticket createNewTicket(User user, User contact) {
        Ticket ticket = new Ticket();
        ticket.setContact(contact == null ? user : contact);
        ticket.setCreatedDate(new Date());
        ticket.setSubmittedBy(user);
        ticket.setModifiedDate(new Date());
        ticket.setPriority(ticketPriorityService.getInitialPriority());
        ticket.setStatus(statusService.getInitialStatus());

        // Figure out those check check notify tech check techs
        // Get notification defaults from mailconfig
        MailConfig mc = mailService.getMailConfig();
        Boolean checkNotifyUser = mc.getNotifyUser();
        Boolean checkNotifyTech = mc.getNotifyTechnician();

        // check user against ticket contact
        if (user.getId().equals(ticket.getContact().getId())) {
            checkNotifyUser = Boolean.FALSE;
        }

        // check user against ticket assignment
        UserRoleGroup urg = ticket.getAssignedTo();
        if (urg != null) {
            UserRole ur = urg.getUserRole();
            if (ur != null) {
                User atu = ur.getUser();
                if (atu != null) {
                    Integer atid = atu.getId();
                    if (user.getId().equals(atid)) {
                        checkNotifyTech = Boolean.FALSE;
                    }
                }
            }
        }

        ticket.setNotifyTech(checkNotifyTech);
        ticket.setNotifyUser(checkNotifyUser);

        if (user.getLocation() != null) {
            ticket.setLocation(user.getLocation());
        }

        return ticket;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
        super.onBindAndValidate(request, command, errors);

        Ticket ticket = (Ticket) command;

        boolean errorReported = false;

        // Manually process the custom fields
        List<String> usedParam = new ArrayList<String>();
        Map<String, String[]> reqParams = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : reqParams.entrySet()) {
            String paramName = entry.getKey();
            String[] paramVals = entry.getValue();
            if ((paramName.startsWith("cf_") || paramName.startsWith("_cf_")) && !usedParam.contains(paramName.split("_")[1])) {
                String[] parmpcs = paramName.split("_");
                if (!paramName.startsWith("_cf_")) {
                    usedParam.add(parmpcs[1]);
                }

                CustomField cf = customFieldsService.getCustomFieldById(new Integer(parmpcs[1]));

                CustomFieldValues cfv = null;
                for (CustomFieldValues cfvFound : ticket.getCustomeFieldValues()) {
                    if (cfvFound.getCustomField().getId().equals(cf.getId())) {
                        cfv = cfvFound;
                        break;
                    }
                }

                if (cfv == null) {
                    cfv = new CustomFieldValues();
                }

                if (cf.getFieldType().equals(CustomField.VALUETYPE_CHECKBOX)) {
                    if (paramVals[0].equals("on")) paramVals[0] = "true";
                }

                cfv.setCustomField(cf);
                cfv.setTicket(ticket);
                cfv.setFieldValue(paramVals[0]);
                ticket.getCustomeFieldValues().add(cfv);

                // Check required fields
                if (cf.isRequired() && StringUtils.isBlank(paramVals[0]) && !errorReported) {
                    errors.reject("customField.validate.requiredField", new String[]{cf.getName()}, "{0} is required");
                }
            }
        }
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        Ticket ticket = (Ticket) command;

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        if (userService.isEndUser(user) && ticket.getId() == null) {
            if (ticket.getAssignedTo() != null && ticket.getAssignedTo().getUserRole() == null) {
                ticket.setStatus(statusService.getTicketPoolStatus());
            }
        }

        ticketService.saveTicket(ticket, user);

        if (ticket.getTicketTemplateStatus() == null ||
                !ticket.getTicketTemplateStatus().equals(-1)) {
            //Get the text and html strings that list the changes that were made to this ticket
            String htmlMailMessage = ticketService.createHTMLComments(ticket, ticket.getOldTicket());
            String textMailMessage = ticketService.createTextComments(ticket, ticket.getOldTicket());
            if (ticket.getId() == null) {
                ticketService.sendEmailNotification(ticket, user, htmlMailMessage, textMailMessage);
            }
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("successfulSave", true);

        return showForm(request, response, errors, model);
    }

    @Override
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

        Ticket ticket = (Ticket) command;

        String mttabIdStr = ServletRequestUtils.getStringParameter(request, "mttabId");
        Integer mttabId = StringUtils.isBlank(mttabIdStr) ? null : Integer.valueOf(mttabIdStr);
        String tabName = "";
        if(mttabId != null){                    
            tabName = myTicketTabService.getById(mttabId).getName();
        }

        List<User> contacts = userService.getActiveUsers();
        if (ticket.getContact() != null && ticket.getContact().getId().equals(User.ANONYMOUS_ID)) {
            contacts.add(userService.getUserById(User.ANONYMOUS_ID));
        }

        Map<String, Object> refData = new HashMap<String, Object>();

        //Searching for LDAP information
        ServletContext application = request.getSession().getServletContext();
        Boolean ldapIntegration = (Boolean) application.getAttribute(ApplicationConstants.LDAP_INTEGRATION);
        LdapUser ldapUser = new LdapUser();
        if (ldapIntegration != null &&
                ldapIntegration &&
                ticket.getContact() != null) {
            if (StringUtils.isNotBlank(ticket.getContact().getLdapDN())) {
                ldapUser = ldapService.getLdapUser(ticket.getContact().getLdapDN());
            }

            List<LdapField> ldapFieldList = ldapFieldService.getLdapFields();
            refData.put("ldapFieldList", ldapFieldList);
            refData.put("ldapUser", ldapUser);
        }

        List<Group> groups = groupService.getGroups();
        User user = userService.getLoggedInUser();
        for(Iterator<Group> iterator = groups.iterator(); iterator.hasNext();){
            Group group = iterator.next();
            if(!permissionService.hasGroupPermission(user, permissionService.getPermissionByKey("group.createTicket"), group)){
                iterator.remove();
            }
        }

        List<Category> categories = new ArrayList<Category>();
        if (ticket.getGroup() != null) {
//            categories = assignmentService.getCategoryListByLocationIdAndGroupId(ticket.getLocation().getId(), ticket.getGroup().getId());
            categories = categoryService.getCategoryByGroupId(ticket.getGroup().getId());
        }

        List<CategoryOption> catOpts = new ArrayList<CategoryOption>();
        if (ticket.getCategory() != null) {
            catOpts = new ArrayList<CategoryOption>(ticket.getCategory().getCategoryOptions());
            for (int i = 0; i < catOpts.size(); i++) {
                if (!catOpts.get(i).isActive()) {
                    catOpts.remove(i);
                }
            }
        }

        List<UserRoleGroup> assignments = new ArrayList<UserRoleGroup>();
        if(ticket.getGroup() != null){
            assignments = userRoleGroupService.getUserRoleGroupByGroupId(ticket.getGroup().getId());
        }

        refData.put("contacts", contacts);
        refData.put("mttabId", mttabId);
        refData.put("tabName", tabName);
        refData.put("locations", locationService.getLocations());
        refData.put("groups", groups);
        refData.put("categories", categories);
        refData.put("catOpts", catOpts);
        refData.put("assignments", assignments);
        refData.put("priorities", ticketPriorityService.getPriorities());
        refData.put("ticketStatus", statusService.getStatus());

        return refData;
    }

    @Override
    protected ModelAndView showForm(HttpServletRequest request, HttpServletResponse response, BindException errors, Map controlModel) throws Exception {
        @SuppressWarnings("unchecked")
        Map<String, Object> model = errors.getModel();
        Ticket ticket = (Ticket) model.get(getCommandName());
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER); //User from session
        user = userService.getUserById(user.getId());

        if (!permissionService.canUserViewTicket(user, ticket)) {
            model = new HashMap<String, Object>();
            String mttabIdStr = ServletRequestUtils.getStringParameter(request, "mttabId");
            Integer mttabId = StringUtils.isBlank(mttabIdStr) ? null : Integer.valueOf(mttabIdStr);
            model.put("mttabId", mttabId);
//            model.put("urgId", ServletRequestUtils.getIntParameter(request, "urgid"));
            model.put("tid", ticket.getId());
            model.put("ticketAccess", Boolean.TRUE);
            model.put("message", "ticket.access.denied");
            model.put("redMessage", "global.accessDenied");
            return new ModelAndView("noTicketAccess", model);
        }

        return showForm(request, errors, getFormView(), controlModel);
    }

    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
        binder.registerCustomEditor(User.class, new UserEditor(userService));
        binder.registerCustomEditor(TicketPriority.class, new TicketPriorityEditor(ticketPriorityService));
        binder.registerCustomEditor(Location.class, new LocationEditor(locationService));
        binder.registerCustomEditor(Group.class, new GroupEditor(groupService));
        binder.registerCustomEditor(Category.class, new CategoryEditor(categoryService));
        binder.registerCustomEditor(CategoryOption.class, new CategoryOptionEditor(categoryOptionService));
        binder.registerCustomEditor(Status.class, new StatusEditor(statusService));
        binder.registerCustomEditor(UserRoleGroup.class, new UserRoleGroupEditor(userRoleGroupService));
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setLdapFieldService(LdapFieldService ldapFieldService) {
        this.ldapFieldService = ldapFieldService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setAssignmentService(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setCustomFieldsService(CustomFieldsService customFieldsService) {
        this.customFieldsService = customFieldsService;
    }

    public void setMyTicketTabService(MyTicketTabService myTicketTabService) {
        this.myTicketTabService = myTicketTabService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
