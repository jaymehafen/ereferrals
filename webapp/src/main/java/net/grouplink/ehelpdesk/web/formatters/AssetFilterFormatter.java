package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.service.asset.AssetFilterService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class AssetFilterFormatter implements Formatter<AssetFilter> {
    @Autowired
    private AssetFilterService assetFilterService;

    public AssetFilter parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return assetFilterService.getById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(AssetFilter object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
