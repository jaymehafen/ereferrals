package net.grouplink.ehelpdesk.web.security;

import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;

public class GLWebSecurityExpressionHandler extends DefaultWebSecurityExpressionHandler {

    private PermissionService permissionService;
    private UserService userService;

    @Override
    protected SecurityExpressionRoot createSecurityExpressionRoot(Authentication authentication, FilterInvocation fi) {

        GLWebSecurityExpressionRoot root = new GLWebSecurityExpressionRoot(authentication, fi);
        root.setPermissionService(permissionService);
        root.setUserService(userService);
        root.setPermissionEvaluator(getPermissionEvaluator());

        return root;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
