package net.grouplink.ehelpdesk.web.pagedlistproviders;

import net.grouplink.ehelpdesk.common.beans.support.GLPagedListSourceProvider;
import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketsFilter;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.TicketService;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketsProvider implements GLPagedListSourceProvider, Serializable {

    private User user;
    private UserRoleGroup urg;
    private Locale locale;
    private String byWho;

    private TicketService ticketService;


    public TicketsProvider(User user, UserRoleGroup urg, Locale locale, String byWho, TicketService ticketService) {
        this.user = user;
        this.urg = urg;
        this.locale = locale;
        this.byWho = byWho;
        this.ticketService = ticketService;
    }

    public List loadList(Locale loc, Object filter) {
        List<Ticket> list;
    	if (filter instanceof AdvancedTicketsFilter) {
    		AdvancedTicketsFilter advFilter = (AdvancedTicketsFilter)filter;
    		list = ticketService.getTicketsByAdvancedSearch(advFilter);
    	} else { //if (filter instanceof TicketsFilter) {
	        TicketsFilter tf = (TicketsFilter) filter;
	
	        list = ticketService.getFilteredTickets(byWho, user, urg, locale, tf.getTicketNumber(), tf.getSubject(),
                    tf.getCategoryOption(), tf.getAssignedTo(), tf.getStatus(), tf.getCreationDate(), tf.getLocation(),
                    tf.getPriority(), tf.getGrouping(), tf.getShowClosed(), tf.getContact());
    	}

        initializeList(list);

        return list;
    }

    private void initializeList(List<Ticket> list) {
        for (Ticket ticket : list) {
            ticket.getAttachments();
            ticket.getSubtickets();
            ticket.getKnowledgeBase();

            Set<TicketHistory> ticketHistorySet = ticket.getTicketHistory();
            for (TicketHistory ticketHistory : ticketHistorySet) {
                ticketHistory.getAttachments();
            }
        }
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }
}