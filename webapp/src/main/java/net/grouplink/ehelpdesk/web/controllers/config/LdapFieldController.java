package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.LdapField;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.web.forms.LdapFieldForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * author: zpearce
 * Date: 12/20/11
 * Time: 9:59 AM
 */
@Controller
@RequestMapping("/ldap/ldapFields")
public class LdapFieldController extends ApplicationObjectSupport {
    @Autowired private LdapFieldService ldapFieldService;
    @Autowired private LdapService ldapService;
    @Autowired private PropertiesService propertiesService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        LdapFieldForm ldapFieldForm = new LdapFieldForm();
        AutoPopulatingList<LdapField> ldapFieldList = new AutoPopulatingList<LdapField>(LdapField.class);
        ldapFieldList.addAll(ldapFieldService.getLdapFields());
        ldapFieldForm.setLdapFields(ldapFieldList);
        model.addAttribute("ldapFieldForm", ldapFieldForm);
        LdapUser ldapUser;
        String savedDn = propertiesService.getPropertiesValueByName("ldapFieldSetupLdapUser");
        if(StringUtils.isNotBlank(savedDn)) {
            ldapUser = ldapService.getLdapUser(savedDn);
        } else {
            ldapUser = new LdapUser();
        }
        model.addAttribute("ldapUser", ldapUser);
        return "ldapFields/index";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String setLdapUser(@RequestParam(required = false, value = "dn") String dn) {
        propertiesService.saveProperties("ldapFieldSetupLdapUser", dn);
        return "redirect:/config/ldap/ldapFields";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/createMultiple")
    public String create(@Valid @ModelAttribute("ldapFieldForm") LdapFieldForm ldapFieldForm,
                         BindingResult result,
                         @RequestParam(value = "idsToDelete", required = false) Integer[] idsToDelete,
                         HttpServletRequest request) {
        if(result.hasErrors()) {
            return "ldapFields/index";
        }
        for(int id : idsToDelete) {
            LdapField field = ldapFieldForm.getLdapFieldById(id);
            ldapFieldService.deleteLdapField(field);
            ldapFieldForm.getLdapFields().remove(field);
        }
        for(LdapField field : ldapFieldForm.getLdapFields()) {
            ldapFieldService.saveLdapField(field);
        }
        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("ldapconfig.ldapField.saveSuccess"));
        return "redirect:/config/ldap/ldapFields";
    }

}
