package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 9/21/11
 * Time: 10:03 AM
 */
public class TicketPriorityConverter implements Converter<String, TicketPriority> {
    @Autowired private TicketPriorityService ticketPriorityService;

    public TicketPriority convert(String id) {
        try {
            int tpId = Integer.parseInt(id);
            return ticketPriorityService.getPriorityById(tpId);
        } catch (Exception e) {
            return null;
        }
    }
}
