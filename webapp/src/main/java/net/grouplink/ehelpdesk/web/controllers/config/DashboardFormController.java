package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.dashboard.Dashboard;
import net.grouplink.ehelpdesk.service.DashboardService;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DashboardFormController extends SimpleFormController {

    private DashboardService dashboardService;

    public DashboardFormController() {
        setCommandClass(Dashboard.class);
        setCommandName("dashboard");
        setFormView("dashboards/dashboardEdit");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer dashId = ServletRequestUtils.getIntParameter(request, "dashId");
        return dashId == null ? new Dashboard() : dashboardService.getById(dashId);
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        Dashboard dashboard = (Dashboard) command;
        dashboardService.save(dashboard);
        request.setAttribute("flash.success", "Dashboard saved successfully");
        return new ModelAndView(new RedirectView("/config/dashboardEdit.glml?dashId=" + dashboard.getId(), true));
    }

    public void setDashboardService(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }
}
