package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomFieldValue;
import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.ImprovedAccountingInfo;
import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.AssetCustomFieldService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import net.grouplink.ehelpdesk.service.asset.SoftwareLicenseService;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import net.grouplink.ehelpdesk.web.propertyeditors.AssetStatusEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.AssetTypeEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryOptionEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.GroupEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.LocationEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.SoftwareLicenseEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.VendorEditor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author mrollins
 * @version 1.0
 */
public class AssetTrackerFormController extends SimpleFormController {

    protected final Log logger = LogFactory.getLog(getClass());

    private UserService userService;
    private LocationService locationService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private GroupService groupService;
    private AssetService assetService;
    private AssetTypeService assetTypeService;
    private VendorService vendorService;
    private AssetCustomFieldService assetCustomFieldService;
    private AssetStatusService assetStatusService;
    private SoftwareLicenseService softwareLicenseService;

    public AssetTrackerFormController() {
        super();
        setCommandClass(Asset.class);
        setCommandName("asset");
        setFormView("assetEdit");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Asset asset = null;
        String assetId = ServletRequestUtils.getStringParameter(request, "assetId");
        if (assetId != null && StringUtils.isNotBlank(assetId)) {
            asset = assetService.getAssetById(Integer.valueOf(assetId));
        }

        if (assetId != null && asset != null && asset.getAccountingInfo() == null) {
            asset.setAccountingInfo(new ImprovedAccountingInfo());
            assetService.saveAsset(asset);
            assetService.flush();
        }

        if (asset == null) {
            asset = new Asset();
            asset.setAccountingInfo(new ImprovedAccountingInfo());
        }

        return asset;
    }

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        Asset asset = (Asset) command;
        List locations = locationService.getLocations();
        List groups = groupService.getGroups();

        List categories = new ArrayList();
        if (asset.getGroup() != null) {
            categories = categoryService.getCategoryByGroupId(asset.getGroup().getId());
        }

        List categoryOptions = new ArrayList();
        if (asset.getCategory() != null) {
            categoryOptions = asset.getCategory().getCategoryOptionsList();
        }

        boolean removeAsset = ServletRequestUtils.getBooleanParameter(request, "removeAsset", false);

        List assetTypes = assetTypeService.getAllAssetTypes();
        List users = userService.getUsers();
        List vendors = vendorService.getAllVendors();
        List statuses = assetStatusService.getAllAssetStatuses();

        if (asset.getAssetType() != null) {
            for (AssetFieldGroup assetFieldGroup : asset.getAssetType().getFieldGroups()) {
                for (AssetCustomField assetCustomField : assetFieldGroup.getCustomFields()) {
                    // check if the asset has a custom field value for this custom field
                    AssetCustomFieldValue customFieldValue = getAssetCustomFieldValue(asset, assetCustomField);
                    if (customFieldValue == null) {
                        customFieldValue = new AssetCustomFieldValue();
                        customFieldValue.setAsset(asset);
                        customFieldValue.setCustomField(assetCustomField);
                        asset.getCustomFieldValues().add(customFieldValue);
                    }
                }
            }
        }

        Map<Integer, String> customFieldValues = new HashMap<Integer, String>();
        for (AssetCustomFieldValue assetCustomFieldValue : asset.getCustomFieldValues()) {
            customFieldValues.put(assetCustomFieldValue.getCustomField().getId(),
                    StringUtils.isNotBlank(assetCustomFieldValue.getFieldValueText()) ?
                            assetCustomFieldValue.getFieldValueText() : assetCustomFieldValue.getFieldValue());
        }

        boolean assTypeChanged = !ServletRequestUtils.getBooleanParameter(request, "changeAssetType", false);
        Map<String, String> cfGroupsWithError = new HashMap<String, String>();
        int indexOfUnderScore = "CFGERR_".indexOf('_');
        int keyIndex = 0;
        for (int ii = 0; ii < errors.getAllErrors().size(); ++ii) {
            Object o = errors.getAllErrors().get(ii);
            if (o != null && o instanceof ObjectError) {
                ObjectError objErr = (ObjectError)o;
                if(objErr.getObjectName().startsWith("CFGERR_")) {
                    String objErrVal = objErr.getObjectName().substring(indexOfUnderScore + 1);
                    if (!cfGroupsWithError.containsValue(objErrVal)) {
                        cfGroupsWithError.put("" + keyIndex++, objErrVal);
                    }
                }
            }
        }
        model.put("cfGroupsWithError", cfGroupsWithError);
        model.put("assTypeChanged", assTypeChanged);
        model.put("removeAsset", removeAsset);
        model.put("locations", locations);
        model.put("groups", groups);
        model.put("categories", categories);
        model.put("categoryOptions", categoryOptions);
        model.put("assetTypes", assetTypes);
        model.put("users", users);
        model.put("vendors", vendors);
        model.put("assetStatuses", statuses);
        model.put("customFieldValues", customFieldValues);
        return model;
    }


    /**
     * Determine whether the given request is a form change request.
     * A form change request changes the appearance of the form
     * and should always show the new form, without validation.
     * <p>Gets called by {@link #suppressValidation} and {@link #processFormSubmission}.
     * Consequently, this single method determines to suppress validation
     * <i>and</i> to show the form view in any case.
     * <p>The default implementation delegates to
     * {@link #isFormChangeRequest(javax.servlet.http.HttpServletRequest)}.
     *
     * @param request current HTTP request
     * @param command form object with request parameters bound onto it
     * @return whether the given request is a form change request
     * @see #suppressValidation
     * @see #processFormSubmission
     */
    protected boolean isFormChangeRequest(HttpServletRequest request, Object command) {
        return ServletRequestUtils.getBooleanParameter(request, "changeAssetType", false);
    }

    private AssetCustomFieldValue getAssetCustomFieldValue(Asset asset, AssetCustomField assetCustomField) {
        if(asset.getId() == null) return null;        
        return assetCustomFieldService.getCustomFieldValue(asset, assetCustomField);
    }

    protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
        logger.debug("global errors");
        for (Object o : errors.getGlobalErrors()) {
            ObjectError error = (ObjectError) o;
            logger.debug("error.getDefaultMessage() = " + error.getDefaultMessage());
        }

        logger.debug("field errors");
        for (Object o : errors.getFieldErrors()) {
            FieldError error = (FieldError) o;
            logger.debug("error.getField() = " + error.getField());
            logger.debug("error.getRejectedValue() = " + error.getRejectedValue());
            logger.debug("error.getDefaultMessage() = " + error.getDefaultMessage());
        }

        Asset asset = (Asset) command;


        // VALIDATION OF CUSTOM FIELDS -
        if (!isFormChangeRequest(request, command)) {
            List<AssetCustomField> requiredCustomFields = getRequiredCustomFieldsForAsset(asset);
            for (AssetCustomField requiredCustomField : requiredCustomFields) {

                // Check to see if a value was submitted for this field
                String rcfName = "cf_" + requiredCustomField.getId();
                String rcfValue = request.getParameter(rcfName);
                if (StringUtils.isBlank(rcfValue)) {
                    String message = getMessageSourceAccessor().getMessage("customField.validate.requiredField",
                            new Object[]{requiredCustomField.getName()}, "{0} is required");
                    errors.addError(new ObjectError("CFGERR_" + requiredCustomField.getFieldGroup().getId(), message));
                }
            }
        }

        Enumeration parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String name = (String) parameterNames.nextElement();
            if (name.startsWith("cf_")) {
                AssetCustomField assetCustomField = assetCustomFieldService.getById(new Integer(name.split("_")[1]));
                AssetCustomFieldValue assetCustomFieldValue = getAssetCustomFieldValue(asset, assetCustomField);
                if (assetCustomFieldValue == null) {
                    assetCustomFieldValue = new AssetCustomFieldValue();
                    assetCustomFieldValue.setAsset(asset);
                    assetCustomFieldValue.setCustomField(assetCustomField);
                    asset.getCustomFieldValues().add(assetCustomFieldValue);
                }

                String value = request.getParameter(name);
                String customFieldType = assetCustomFieldValue.getCustomField().getCustomFieldType();

                if (customFieldType.equals(AssetCustomField.VALUETYPE_TEXT)) {
                    assetCustomFieldValue.setFieldValue(value);
                } else if (customFieldType.equals(AssetCustomField.VALUETYPE_TEXTAREA)) {
                    assetCustomFieldValue.setFieldValueText(value);
                } else if (customFieldType.equals(AssetCustomField.VALUETYPE_CHECKBOX)) {
                    boolean checked = false;
                    String[] values = request.getParameterValues(name);
                    for (String v : values) {
                        if (StringUtils.isNotBlank(v)) {
                            checked = true;
                        }
                    }

                    assetCustomFieldValue.setFieldValue(checked ? "true" : "false");

                } else if (customFieldType.equals(AssetCustomField.VALUETYPE_DATE)) {
                    assetCustomFieldValue.setFieldValue(value);
                } else if (customFieldType.equals(AssetCustomField.VALUETYPE_RADIO)) {
                    assetCustomFieldValue.setFieldValue(value);
                } else if (customFieldType.equals(AssetCustomField.VALUETYPE_SELECT)) {
                    assetCustomFieldValue.setFieldValue(value);
                }
            }
        }
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
            throws Exception {

        Asset asset = (Asset) command;

        if (asset.getAccountingInfo() == null) {
            asset.setAccountingInfo(new ImprovedAccountingInfo());
        }
        // TODO: rework this when generics are available ... get rid of asset.softLicks
        if (asset.getSoftLicks() != null) {
            asset.setSoftwareLicenses(new HashSet<SoftwareLicense>(asset.getSoftLicks()));
        }

        assetService.saveAsset(asset);
        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("assetTracker.assetSavedSuccessfully", "Asset saved successfully"));

        return new ModelAndView(new RedirectView("/asset/assetEdit.glml?assetId=" + asset.getId(), true));
    }

    public void afterPropertiesSet() throws Exception {
        if (userService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userService");
        if (groupService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: groupService");
        if (locationService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: locationService");
        if (categoryService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryService");
        if (categoryOptionService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: categoryOptionService");
        if (assetService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: assetService");
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);

        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
        binder.registerCustomEditor(User.class, new UserEditor(userService));
        binder.registerCustomEditor(Group.class, new GroupEditor(groupService));
        binder.registerCustomEditor(Location.class, new LocationEditor(locationService));
        binder.registerCustomEditor(Category.class, new CategoryEditor(categoryService));
        binder.registerCustomEditor(CategoryOption.class, new CategoryOptionEditor(categoryOptionService));
        binder.registerCustomEditor(AssetType.class, new AssetTypeEditor(assetTypeService));
        binder.registerCustomEditor(Vendor.class, new VendorEditor(vendorService));
        binder.registerCustomEditor(AssetStatus.class, new AssetStatusEditor(assetStatusService));
        binder.registerCustomEditor(SoftwareLicense.class, new SoftwareLicenseEditor(softwareLicenseService));
    }

    private List<AssetCustomField> getRequiredCustomFieldsForAsset(Asset asset) {
        ArrayList<AssetCustomField> retList = new ArrayList<AssetCustomField>();
        AssetType assetType = asset.getAssetType();
        if(assetType != null) {
            Set<AssetFieldGroup> fieldGroups = assetType.getFieldGroups();
            Iterator<AssetFieldGroup> fgIter = fieldGroups.iterator();
            int requiredFieldsCount = 0;
            while (fgIter.hasNext()) {
                AssetFieldGroup fieldGroup = fgIter.next();
                Set<AssetCustomField> customFields = fieldGroup.getCustomFields();
                for (AssetCustomField customField : customFields) {
                    if (customField.getRequired()) {
                        retList.add(customField);
                        requiredFieldsCount++;
                    }
                }
            }
            logger.debug("Required fields count: "+ requiredFieldsCount);
        }

        return retList;
    }


    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    public void setAssetCustomFieldService(AssetCustomFieldService assetCustomFieldService) {
        this.assetCustomFieldService = assetCustomFieldService;
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }

    public void setSoftwareLicenseService(SoftwareLicenseService softwareLicenseService) {
        this.softwareLicenseService = softwareLicenseService;
    }
}
