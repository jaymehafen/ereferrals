package net.grouplink.ehelpdesk.web.views;

import net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder;
import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.grouplink.ehelpdesk.domain.AdvancedTicketsFilter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.TicketsFilter;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.SurveyQuestionService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.support.SortDefinition;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 * @deprecated use TicketFilterListExcelView
 */
public class TicketListExcelView extends AbstractExcelView {

    private final Log log = LogFactory.getLog(TicketListExcelView.class);
    private final SimpleDateFormat filterDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private TicketPriorityService ticketPriorityService;
    private UserRoleGroupService userRoleGroupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private UserService userService;
    private GroupService groupService;
    private LocationService locationService;
    private StatusService statusService;
    private SurveyQuestionService surveyQuestionService;
    private ZenAssetService zenAssetService;
    private TicketService ticketService;

    /**
     * Subclasses must implement this method to create an Excel HSSFWorkbook document,
     * given the model.
     *
     * @param model    the model Map
     * @param workbook the Excel workbook to complete
     * @param request  in case we need locale etc. Shouldn't look at attributes.
     * @param response in case we need to set cookies. Shouldn't write to it.
     */
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map data = (Map) model.get("data");

        GLRefreshablePagedListHolder pgHolder = (GLRefreshablePagedListHolder) data.get("tickets");
        String groupName = (String) data.get("groupName");

        // Create a new sheet.
        HSSFSheet summarySheet = workbook.createSheet(getMessageSourceAccessor().getMessage("ticketList.summary"));

        // We simply put an error message on the first cell if no list is available
        // Nevertheless, it should never be null as the controller verify it.
        if (pgHolder == null) {
            getCell(summarySheet, 0, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage("ticketList.nolist")));
            return;
        }

        // Create a font for headers
        HSSFFont f = workbook.createFont();
        // set font 1 to 12 point type
        f.setFontHeightInPoints((short) 10);
        f.setColor(HSSFColor.BLACK.index);
        f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        // The same for properties data
        HSSFFont fp = workbook.createFont();
        fp.setColor((short) 0xc);
        HSSFCellStyle csp = workbook.createCellStyle();
        csp.setFont(fp);
        csp.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // We create a date style
        HSSFCellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setFont(fp);
        dateStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        dateStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

        // Set the colum width of the two first columns
        summarySheet.setColumnWidth((short) 0, (short) (20 * 256));
        summarySheet.setColumnWidth((short) 1, (short) (20 * 512));

        SortDefinition sort = pgHolder.getSort();

        int row = 0;

        row = buildSummaryCells(pgHolder, groupName, summarySheet, csp, dateStyle, sort, row);

        // Applied Filters
        buildAppliedFiltersCells(workbook, pgHolder, summarySheet, csp, row);

        // Create a second sheet for the tickets
        buildTicketListSheet(workbook, pgHolder, f, dateStyle);
    }

    private int buildSummaryCells(GLRefreshablePagedListHolder pgHolder, String groupName, HSSFSheet sheet,
                                  HSSFCellStyle csp, HSSFCellStyle dateStyle, SortDefinition sort, int row) {
        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage("ticketList.dateExported")));
        getCell(sheet, row, 1).setCellValue(pgHolder.getRefreshDate());
        getCell(sheet, row, 1).setCellStyle(dateStyle);
        row++;

        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage("ticketList.nbTickets")));
        getCell(sheet, row, 1).setCellValue(pgHolder.getNrOfElements());
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;

        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage("ticket.group")));
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(groupName));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;

        // Sorting
        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage("ticketList.sorting")));
        // Get the name of the sorted column
        String colNameKey = "";
        if (sort.getProperty().equals("priority")) colNameKey = "ticket.priority";
        else if (sort.getProperty().equals("ticketId")) colNameKey = "ticket.number";
        else if (sort.getProperty().equals("contact")) colNameKey = "ticket.contact";
        else if (sort.getProperty().equals("location")) colNameKey = "ticket.location";
        else if (sort.getProperty().equals("subject")) colNameKey = "ticket.subject";
        else if (sort.getProperty().equals("categoryOption")) colNameKey = "ticket.categoryOption";
        else if (sort.getProperty().equals("assignedTo")) colNameKey = "ticket.assignedTo";
        else if (sort.getProperty().equals("status")) colNameKey = "ticket.status";
        else if (sort.getProperty().equals("createdDate")) colNameKey = "ticket.creationDate";
        String colName = getMessageSourceAccessor().getMessage(colNameKey, "None") + " - " +
                (sort.isAscending() ? getMessageSourceAccessor().getMessage("pagedList.pltAscending") :
                        getMessageSourceAccessor().getMessage("pagedList.pltDescending"));
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(colName));
        getCell(sheet, row, 1).setCellStyle(csp);
        row += 2;
        return row;
    }

    private void buildAppliedFiltersCells(HSSFWorkbook workbook, GLRefreshablePagedListHolder pgHolder, HSSFSheet sheet,
                                          HSSFCellStyle csp, int row) {
        if (pgHolder.getFilter() instanceof TicketFilter) {
            buildAppliedFiltersCellsDynamic(workbook, pgHolder, sheet, csp, row);
        } else {
            buildAppliedFiltersCellsStatic(workbook, pgHolder, sheet, csp, row);
        }
    }

    private void buildAppliedFiltersCellsDynamic(HSSFWorkbook workbook, GLRefreshablePagedListHolder pgHolder,
                                                 HSSFSheet sheet, HSSFCellStyle csp, int row) {
        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage("ticketList.filters")));
        row++;
        HSSFCellStyle fsp = workbook.createCellStyle();
        fsp.setAlignment(HSSFCellStyle.ALIGN_RIGHT);

        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, pgHolder.getLocale());

        TicketFilter tf = (TicketFilter) pgHolder.getFilter();

        // assignedToIds
        if (!ArrayUtils.isEmpty(tf.getAssignedToIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getAssignedToIds().length; i++) {
                Integer integer = tf.getAssignedToIds()[i];
                UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                if (urg.getUserRole() != null) {
                    sb.append(urg.getUserRole().getUser().getFirstName()).append(" ")
                            .append(urg.getUserRole().getUser().getLastName());
                } else {
                    sb.append(urg.getGroup().getName());
                }
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_ASSIGNED, sb.toString());
            row++;
        }

        // categoryIds
        if (!ArrayUtils.isEmpty(tf.getCategoryIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCategoryIds().length; i++) {
                Integer integer = tf.getCategoryIds()[i];
                Category cat = categoryService.getCategoryById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(cat.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CATEGORY, sb.toString());
            row++;
        }

        // categoryOptionIds
        if (!ArrayUtils.isEmpty(tf.getCategoryOptionIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCategoryOptionIds().length; i++) {
                Integer integer = tf.getCategoryOptionIds()[i];
                CategoryOption catOpt = categoryOptionService.getCategoryOptionById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(catOpt.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CATOPT, sb.toString());
            row++;
        }

        // contactIds
        if (!ArrayUtils.isEmpty(tf.getContactIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getContactIds().length; i++) {
                Integer integer = tf.getContactIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CONTACT, sb.toString());
            row++;
        }

//        tf.getCreatedDateOperator();
        if (tf.getCreatedDateOperator() != null && !tf.getCreatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            String s = getDateFilterDescription(df, tf.getCreatedDateOperator(), tf.getCreatedDate(),
                    tf.getCreatedDate2());
            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CREATED, s);
            row++;
        }

//        tf.getCustomFieldName();
        if (!ArrayUtils.isEmpty(tf.getCustomFieldName()) && !ArrayUtils.isEmpty(tf.getCustomFieldValue())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCustomFieldName().length; i++) {
                String cfn = tf.getCustomFieldName()[i];
                String cfv = tf.getCustomFieldValue()[i];
                if (StringUtils.isNotBlank(cfn) && StringUtils.isNotBlank(cfv)) {
                    if (sb.length() > 0) sb.append(", ");
                    sb.append(cfn).append(" = ").append(cfv);
                }
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CUSTOMFIELD, sb.toString());
            row++;
        }

//        tf.getEstimatedDateOperator();
        if (tf.getEstimatedDateOperator() != null && !tf.getEstimatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            String s = getDateFilterDescription(df, tf.getEstimatedDateOperator(), tf.getEstimatedDate(),
                    tf.getEstimatedDate2());
            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_ESTIMATED, s);
            row++;
        }

//        tf.getGroupIds();
        if (!ArrayUtils.isEmpty(tf.getGroupIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getGroupIds().length; i++) {
                Integer integer = tf.getGroupIds()[i];
                Group g = groupService.getGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(g.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_GROUP, sb.toString());
            row++;
        }

//        tf.getLocationIds();
        if (!ArrayUtils.isEmpty(tf.getLocationIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getLocationIds().length; i++) {
                Integer integer = tf.getLocationIds()[i];
                Location loc = locationService.getLocationById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(loc.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_LOCATION, sb.toString());
            row++;
        }

//        tf.getModifiedDateOperator();
        if (tf.getModifiedDateOperator() != null && !tf.getModifiedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            String s = getDateFilterDescription(df, tf.getModifiedDateOperator(), tf.getModifiedDate(),
                    tf.getModifiedDate2());
            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_MODIFIED, s);
            row++;
        }

//        tf.getNote();
        if (!ArrayUtils.isEmpty(tf.getNote())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getNote().length; i++) {
                String s = tf.getNote()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(s);
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_NOTE, sb.toString());
            row++;
        }

//        tf.getPriorityIds();
        if (!ArrayUtils.isEmpty(tf.getPriorityIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getPriorityIds().length; i++) {
                Integer integer = tf.getPriorityIds()[i];
                TicketPriority p = ticketPriorityService.getPriorityById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(p.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_PRIORITY, sb.toString());
            row++;
        }

//        tf.getStatusIds();
        if (!ArrayUtils.isEmpty(tf.getStatusIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getStatusIds().length; i++) {
                Integer integer = tf.getStatusIds()[i];
                Status status = statusService.getStatusById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(status.getName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_STATUS, sb.toString());
            row++;
        }

//        tf.getSubject();
        if (!ArrayUtils.isEmpty(tf.getSubject())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getSubject().length; i++) {
                String s = tf.getSubject()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(s);
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_SUBJECT, sb.toString());
            row++;
        }

//        tf.getSubmittedByIds();
        if (!ArrayUtils.isEmpty(tf.getSubmittedByIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getSubmittedByIds().length; i++) {
                Integer integer = tf.getSubmittedByIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_SUBMITTED, sb.toString());
            row++;
        }

//        tf.getTicketNumbers();
        if (!ArrayUtils.isEmpty(tf.getTicketNumbers())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getTicketNumbers().length; i++) {
                Integer integer = tf.getTicketNumbers()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(integer);
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_TICKETNUMBER, sb.toString());
            row++;
        }

//        tf.getWorkTimeOperator();
        if (StringUtils.isNotBlank(tf.getWorkTimeOperator())) {
            StringBuilder sb = new StringBuilder();
            if (tf.getWorkTimeOperator().equals("more")) {
                sb.append(getMessageSourceAccessor().getMessage("ticketSearch.worktime.more")).append(" ");
            } else if (tf.getWorkTimeOperator().equals("less")) {
                sb.append(getMessageSourceAccessor().getMessage("ticketSearch.worktime.less")).append(" ");
            } else {
                sb.append("unknown work time operator! ");
            }

            if (StringUtils.isNotBlank(tf.getWorkTimeHours())) {
                sb.append(tf.getWorkTimeHours()).append(" ")
                        .append(getMessageSourceAccessor().getMessage("scheduler.time.hour")).append(" ");
            }

            if (StringUtils.isNotBlank(tf.getWorkTimeMinutes())) {
                sb.append(tf.getWorkTimeMinutes()).append(" ")
                        .append(getMessageSourceAccessor().getMessage("scheduler.time.mins"));
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_WORKTIME, sb.toString());
            row++;
        }

//        tf.surveyQuestionId
        String[] qids = tf.getSurveyQuestionId();
        String[] vals = tf.getSurveyQuestionValue();
        if ((qids != null) && (vals != null)) {
            for (int i=0; i<qids.length; i++) {
                SurveyQuestion sq = surveyQuestionService.getSurveyQuestionById(new Integer(qids[i]));
                String qText = sq.getSurveyQuestionText();
                buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_SURVEYS, qText);
                if (i<qids.length-1) row++;
            }
        }

        // zenAssetIds
        if (!ArrayUtils.isEmpty(tf.getZenAssetIds())) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getZenAssetIds().length; i++) {
                Integer integer = tf.getZenAssetIds()[i];
                ZenAsset za = zenAssetService.getById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(za.getAssetName());
            }

            buildAppliedFilterCell(sheet, row, fsp, csp, TicketFilter.COLUMN_CATEGORY, sb.toString());
            row++;
        }


    }

    private void buildAppliedFilterCell(HSSFSheet sheet, int row, HSSFCellStyle codeStyle,
                                        HSSFCellStyle descriptionStyle, String code, String description) {
        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage(code)));
        getCell(sheet, row, 0).setCellStyle(codeStyle);
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(description));
        getCell(sheet, row, 1).setCellStyle(descriptionStyle);
    }

    private String getDateFilterDescription(DateFormat df, Integer dateOperator, String date1, String date2) {
        StringBuilder sb = new StringBuilder();
        if (dateOperator.equals(TicketFilterOperators.DATE_TODAY)) {
            sb.append(getMessageSourceAccessor().getMessage("date.today"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_WEEK)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thisweek"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_MONTH)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thismonth"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_ON)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.on")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_BEFORE)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.before")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_AFTER)) {
            Date d1 = parseDate(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.after")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_RANGE)) {
            Date d1 = parseDate(date1);
            Date d2 = parseDate(date2);
            sb.append(getMessageSourceAccessor().getMessage("date.range")).append(" ").append(df.format(d1))
                    .append(" - ").append(df.format(d2));
        }

        return sb.toString();
    }

    private Date parseDate(String date1) {
        Date selectedDate;
        try {
            selectedDate = filterDateFormat.parse(date1);
        } catch (ParseException e) {
            log.error("error parsing date: " + date1, e);
            throw new RuntimeException(e);
        }

        return selectedDate;
    }


    private void buildAppliedFiltersCellsStatic(HSSFWorkbook workbook, GLRefreshablePagedListHolder pgHolder, HSSFSheet sheet, HSSFCellStyle csp, int row) {
        getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(getMessageSourceAccessor().getMessage("ticketList.filters")));
        row++;
        String[] colHeads = getColumnHeadings(pgHolder);
        HSSFCellStyle fsp = workbook.createCellStyle();
        fsp.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
        for (int i = 0; i < colHeads.length; i++) {
            getCell(sheet, row + i, 0).setCellValue(new HSSFRichTextString(colHeads[i]));
            getCell(sheet, row + i, 0).setCellStyle(fsp);
        }

        TicketsFilter filter = new TicketsFilter();
        if (pgHolder.getFilter() instanceof TicketsFilter) {
            filter = (TicketsFilter) pgHolder.getFilter();
        }
        if (pgHolder.getFilter() instanceof AdvancedTicketsFilter) {
            AdvancedTicketsFilter advFilter = (AdvancedTicketsFilter) pgHolder.getFilter();
            filter.setTicketNumber(StringUtils.isNotBlank(advFilter.getTicketNumber()) ? advFilter.getTicketNumber() : "");
            filter.setLocation(advFilter.getLocation() != null ? advFilter.getLocation().getName() : "");
            filter.setSubject(StringUtils.isNotBlank(advFilter.getSubject()) ? advFilter.getSubject() : "");
            filter.setCategoryOption(advFilter.getCategoryOption() != null ? advFilter.getCategoryOption().getName() : "");
            if (advFilter.getAssignedTo() != null) {
                if (Boolean.valueOf(advFilter.getAssignedToTicketPool()) == Boolean.TRUE) {
                    filter.setAssignedTo(advFilter.getAssignedToName());
                } else {
                    filter.setAssignedTo(advFilter.getAssignedToName());
                }
            }
            filter.setStatus(advFilter.getTicketStatus() != null ? advFilter.getTicketStatus().getName() : "");
            filter.setCreationDate(StringUtils.isNotBlank(advFilter.getCreationDate()) ? advFilter.getCreationDate() : "");
        }

        // get actual priority from id
        TicketPriority priority;
        String priorityName = "";
        if (StringUtils.isNotBlank(filter.getPriority())) {
            priority = ticketPriorityService.getPriorityById(Integer.valueOf(filter.getPriority()));
            priorityName = priority.getName();
        }
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(priorityName));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getTicketNumber()) ? " " : filter.getTicketNumber()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getContact()) ? " " : filter.getContact()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getLocation()) ? " " : filter.getLocation()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getSubject()) ? " " : filter.getSubject()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getCategoryOption()) ? " " : filter.getCategoryOption()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getAssignedTo()) ? " " : filter.getAssignedTo()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getStatus()) ? " " : filter.getStatus()));
        getCell(sheet, row, 1).setCellStyle(csp);
        row++;
        getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(StringUtils.isBlank(filter.getCreationDate()) ? " " : filter.getCreationDate()));
        getCell(sheet, row, 1).setCellStyle(csp);
        

    }

    private String[] getColumnHeadings(GLRefreshablePagedListHolder pgHolder) {
        String[] colHeads;
        List<String> colHeadsList = new ArrayList<String>();
        if (pgHolder.getFilter() instanceof TicketFilter) {
            TicketFilter tf = (TicketFilter) pgHolder.getFilter();
            for (int i = 0; i < tf.getColumnOrder().length; i++) {
                String s = tf.getColumnOrder()[i];
                if (s.startsWith("ticketFilter.")) {
                    colHeadsList.add(getMessageSourceAccessor().getMessage(s));
                } else if (s.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                    // custom field
                } else if (s.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                    
                }
            }

            if (!ArrayUtils.isEmpty(tf.getSurveyQuestionId())) {
                String[] sqids = tf.getSurveyQuestionId();
                for (String aSqid : sqids) {
                    SurveyQuestion sq = surveyQuestionService.getSurveyQuestionById(new Integer(aSqid));
                    String sqt = sq.getSurveyQuestionText();
                    colHeadsList.add(sqt);
                }
            }

            colHeads = colHeadsList.toArray(new String[colHeadsList.size()]);
        } else {
            colHeads = new String[]{getMessageSourceAccessor().getMessage("ticket.priority"),
                    getMessageSourceAccessor().getMessage("ticket.number"),
                    getMessageSourceAccessor().getMessage("ticket.contact"), 
                    getMessageSourceAccessor().getMessage("ticket.location"),
                    getMessageSourceAccessor().getMessage("ticket.subject"),
                    getMessageSourceAccessor().getMessage("ticket.categoryOption"),
                    getMessageSourceAccessor().getMessage("ticket.assignedTo"),
                    getMessageSourceAccessor().getMessage("ticket.status"),
                    getMessageSourceAccessor().getMessage("ticket.creationDate"),
                    getMessageSourceAccessor().getMessage("ticket.note")};
        }

        return colHeads;
    }

    private void buildTicketListSheet(HSSFWorkbook workbook, GLRefreshablePagedListHolder pgHolder, HSSFFont f,
                                      HSSFCellStyle dateStyle) {
        if (pgHolder.getFilter() instanceof TicketFilter) {
            buildTicketListSheetDynamic(workbook, pgHolder, f, dateStyle);
        } else {
            buildTicketListCellsStatic(workbook, pgHolder, f, dateStyle);
        }
    }

    private void buildTicketListSheetDynamic(HSSFWorkbook workbook, GLRefreshablePagedListHolder pgHolder, HSSFFont f,
                                             HSSFCellStyle dateStyle) {
        HSSFSheet sheet;
        int row;
        sheet = workbook.createSheet(getMessageSourceAccessor().getMessage("ticketList.title"));

        row = 0;
        // Create a style for headers
        HSSFCellStyle cs = workbook.createCellStyle();
        cs.setFont(f);
        cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // Column Headers
        String[] colHeads = getColumnHeadings(pgHolder);
        HSSFCell cell;
        for (int i = 0; i < colHeads.length; i++) {
            cell = getCell(sheet, row, i);
            cell.setCellStyle(cs);
            cell.setCellValue(new HSSFRichTextString(colHeads[i]));
        }

        row++;

        TicketFilter tf = (TicketFilter) pgHolder.getFilter();
        String[] columns = tf.getColumnOrder();
        // Iterate on the tickets list
        @SuppressWarnings("unchecked")
        List<Ticket> tickets = pgHolder.getSource();
        for (Ticket ticket : tickets) {
            for (int i = 0; i < columns.length; i++) {
                String column = columns[i];
                if (column.equals(TicketFilter.COLUMN_PRIORITY)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getPriority().getName()));
                } else if (column.equals(TicketFilter.COLUMN_TICKETNUMBER)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getTicketId().toString()));
                } else if (column.equals(TicketFilter.COLUMN_LOCATION)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getLocation().getName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBJECT)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getSubject()));
                } else if (column.equals(TicketFilter.COLUMN_NOTE)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString((ticket.getNote() == null) ? "" : ticket.getNote()));
                } else if (column.equals(TicketFilter.COLUMN_CATOPT)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getCategoryOption() == null ? "" :
                            ticket.getCategoryOption().getName()));
                } else if (column.equals(TicketFilter.COLUMN_ASSIGNED)) {
                    UserRoleGroup assignedToUrg = ticket.getAssignedTo();
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(assignedToUrg.getUserRole() == null ?
                            assignedToUrg.getGroup().getName() : assignedToUrg.getUserRole().getUser().getFirstName() +
                            " " + ticket.getAssignedTo().getUserRole().getUser().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_STATUS)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getStatus().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CREATED)) {
                    getCell(sheet, row, i).setCellValue(ticket.getCreatedDate());
                    getCell(sheet, row, i).setCellStyle(dateStyle);
                } else if (column.equals(TicketFilter.COLUMN_MODIFIED)) {
                    getCell(sheet, row, i).setCellValue(ticket.getModifiedDate());
                    getCell(sheet, row, i).setCellStyle(dateStyle);
                } else if (column.equals(TicketFilter.COLUMN_ESTIMATED)) {
                    if (ticket.getEstimatedDate() != null)
                        getCell(sheet, row, i).setCellValue(ticket.getEstimatedDate());
                    //else
                    //    getCell(sheet, row, i).setCellValue(new HSSFRichTextString());
                    getCell(sheet, row, i).setCellStyle(dateStyle);
                } else if (column.equals(TicketFilter.COLUMN_CONTACT)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getContact().getFirstName() + " " +
                            ticket.getContact().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBMITTED)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getSubmittedBy().getFirstName() + " " +
                            ticket.getSubmittedBy().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_GROUP)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getGroup().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CATEGORY)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getCategory() == null ? "" : ticket.getCategory().getName()));
                } else if (column.equals(TicketFilter.COLUMN_WORKTIME)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getWorkTime() == null ?
                            "" : ticket.getWorkTime().toString()));
                } else if (column.equals(TicketFilter.COLUMN_SURVEYS)) {
                    String[] vals = tf.getSurveyQuestionValue();
                    for (int x = 0; x < vals.length; x++) {
                        getCell(sheet, row, i + x).setCellValue(new HSSFRichTextString(vals[x]));
                    }
                } else if (column.equals(TicketFilter.COLUMN_ZENWORKS10ASSET)) {
                    getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticket.getZenAsset() == null ? "" : ticket.getZenAsset().getAssetName()));
                } else if (column.equals(TicketFilter.COLUMN_HISTORYSUBJECT)) {
                    TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                    if (ticketHistory == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticketHistory.getSubject() == null ? "" : ticketHistory.getSubject()));
                    }
                } else if (column.equals(TicketFilter.COLUMN_HISTORYNOTE)) {
                    TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                    if (ticketHistory == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(ticketHistory.getNote() == null ? "" : ticketHistory.getNote()));
                    }
                } else if (column.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                    CustomFieldValues cfv = ticketService.getCustomFieldValue(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_CUSTOMFIELDPREFIX));
                    if (cfv == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(cfv.getFieldValue() == null ? "" : cfv.getFieldValue()));
                    }
                } else if (column.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                    SurveyData sd = ticketService.getSurveyData(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_SURVEYPREFIX));
                    if (sd == null) {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(""));
                    } else {
                        getCell(sheet, row, i).setCellValue(new HSSFRichTextString(sd.getValue() == null ? "" : sd.getValue()));
                    }
                }
            }

            row++;
        }
    }

    private void buildTicketListCellsStatic(HSSFWorkbook workbook, GLRefreshablePagedListHolder pgHolder, HSSFFont f,
                                            HSSFCellStyle dateStyle) {
        HSSFSheet sheet;
        int row;
        sheet = workbook.createSheet(getMessageSourceAccessor().getMessage("ticketList.title"));
        sheet.setColumnWidth((short) 2, (short) (30 * 150));
        sheet.setColumnWidth((short) 3, (short) (30 * 256));
        sheet.setColumnWidth((short) 4, (short) (30 * 150));
        sheet.setColumnWidth((short) 5, (short) (30 * 150));
        sheet.setColumnWidth((short) 6, (short) (30 * 150));
        sheet.setColumnWidth((short) 7, (short) (30 * 150));
        sheet.setColumnWidth((short) 8, (short) (30 * 250));
        sheet.setColumnWidth((short) 9, (short) (30 * 250));

        row = 0;

        // Create a style for headers
        HSSFCellStyle cs = workbook.createCellStyle();
        cs.setFont(f);
        cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        // Column Headers
        String[] colHeads = getColumnHeadings(pgHolder);
        HSSFCell cell;
        for (int i = 0; i < colHeads.length; i++) {
            cell = getCell(sheet, row, i);
            cell.setCellStyle(cs);
            cell.setCellValue(new HSSFRichTextString(colHeads[i]));
        }
        row++;

        // Iterate on the tickets list
        @SuppressWarnings("unchecked")
        List<Ticket> tickets = pgHolder.getSource();
        for (Ticket ticket : tickets) {
            getCell(sheet, row, 0).setCellValue(new HSSFRichTextString(ticket.getPriority().getName()));
            getCell(sheet, row, 1).setCellValue(new HSSFRichTextString(ticket.getTicketId().toString()));
            String contactName = ticket.getContact().getFirstName() + " " + ticket.getContact().getLastName();
            getCell(sheet, row, 2).setCellValue(new HSSFRichTextString(contactName));
            getCell(sheet, row, 3).setCellValue(new HSSFRichTextString(ticket.getLocation().getName()));
            getCell(sheet, row, 4).setCellValue(new HSSFRichTextString(ticket.getSubject()));
            getCell(sheet, row, 5).setCellValue(new HSSFRichTextString(ticket.getCategoryOption() == null ? "" :
                    ticket.getCategoryOption().getName()));
            String assTo = ticket.getAssignedTo().getUserRole() == null ?
                    ticket.getAssignedTo().getGroup().getName() :
                    ticket.getAssignedTo().getUserRole().getUser().getFirstName() + " " +
                            ticket.getAssignedTo().getUserRole().getUser().getLastName();
            getCell(sheet, row, 6).setCellValue(new HSSFRichTextString(assTo));
            getCell(sheet, row, 7).setCellValue(new HSSFRichTextString(ticket.getStatus().getName()));
            getCell(sheet, row, 8).setCellValue(ticket.getCreatedDate());
            getCell(sheet, row, 8).setCellStyle(dateStyle);

            String note = ticket.getNote();
            if ((note != null) && (note.length() > 0))
                note = RegexUtils.stripHtmlTags(note);
            else
                note = "";

            getCell(sheet, row, 9).setCellValue(new HSSFRichTextString(note));
            row++;
        }
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setSurveyQuestionService(SurveyQuestionService surveyQuestionService) {
        this.surveyQuestionService = surveyQuestionService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public TicketService getTicketService() {
        return ticketService;
    }
}
