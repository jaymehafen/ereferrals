package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

public class ContactEditor extends PropertyEditorSupport {

    private UserService userService;

    /**
    * Constructor for use by derived PropertyEditor classes.
    */
    public ContactEditor() { }

    /**
     * Constructor for use by derived PropertyEditor classes.
     *
     * @param userService service for getting users
     */
    public ContactEditor(UserService userService) {
        this.userService = userService;
    }

    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        User user = (User) getValue();
        return (user != null) ? user.getFirstName()+" "+user.getLastName() : "";
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(userService.getUserById(new Integer(text)));
        } else {
            setValue(null);
        }
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
