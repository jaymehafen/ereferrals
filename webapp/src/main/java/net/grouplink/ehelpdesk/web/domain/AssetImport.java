package net.grouplink.ehelpdesk.web.domain;

import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.map.LazyMap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Aug 19, 2008
 * Time: 3:10:39 PM
 */
public class AssetImport implements Serializable {
    private byte[] fileData;
    private String csvType;
    private boolean firstLineHeader;
    private Map importMap = LazyMap.decorate(new HashMap(), FactoryUtils.instantiateFactory(String.class));
    private boolean addLocation;
    private boolean addGroup;
    private boolean addCategory;
    private boolean addCategoryOption;

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public String getCsvType() {
        return csvType;
    }

    public void setCsvType(String csvType) {
        this.csvType = csvType;
    }

    public boolean isFirstLineHeader() {
        return firstLineHeader;
    }

    public void setFirstLineHeader(boolean firstLineHeader) {
        this.firstLineHeader = firstLineHeader;
    }

    public Map getImportMap() {
        return importMap;
    }

    public void setImportMap(Map importMap) {
        this.importMap = importMap;
    }

    public boolean isAddLocation() {
        return addLocation;
    }

    public void setAddLocation(boolean addLocation) {
        this.addLocation = addLocation;
    }

    public boolean isAddGroup() {
        return addGroup;
    }

    public void setAddGroup(boolean addGroup) {
        this.addGroup = addGroup;
    }

    public boolean isAddCategory() {
        return addCategory;
    }

    public void setAddCategory(boolean addCategory) {
        this.addCategory = addCategory;
    }

    public boolean isAddCategoryOption() {
        return addCategoryOption;
    }

    public void setAddCategoryOption(boolean addCategoryOption) {
        this.addCategoryOption = addCategoryOption;
    }
}
