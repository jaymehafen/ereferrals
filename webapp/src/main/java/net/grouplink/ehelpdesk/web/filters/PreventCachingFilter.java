package net.grouplink.ehelpdesk.web.filters;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.Arrays;

public class PreventCachingFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, new AddExpiresHeaderResponse((HttpServletResponse)response));
    }

    public void destroy() {

    }
}

class AddExpiresHeaderResponse extends HttpServletResponseWrapper {

    public static final String[] CACHEABLE_CONTENT_TYPES = new String[] {
        "text/css", "text/javascript", "image/png", "image/jpeg",
        "image/gif", "image/jpg" };

    static {
        Arrays.sort(CACHEABLE_CONTENT_TYPES);
    }

    public AddExpiresHeaderResponse(HttpServletResponse response) {
        super(response);
    }

    @Override
    public void setContentType(String contentType) {
        super.setContentType(contentType);

        String mainContentType = contentType;
        if (mainContentType != null && mainContentType.contains(";")) {
            mainContentType = mainContentType.split(";")[0];
        }

        if (mainContentType != null && Arrays.binarySearch(CACHEABLE_CONTENT_TYPES, mainContentType) <= -1) {
            super.setHeader("Expires", "-1");
            super.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            super.setHeader("Pragma", "no-cache");
        }
    }
}
