package net.grouplink.ehelpdesk.web.tf;

import net.grouplink.ehelpdesk.domain.RecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.ReportRecurrenceSchedule;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.mail.EmailMessageTemplate;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.ReportRecurrenceScheduleService;
import net.grouplink.ehelpdesk.service.ReportService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.web.propertyeditors.TicketFilterEditor;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ReportFormController extends SimpleFormController {
    private ReportService reportService;
    private TicketFilterService ticketFilterService;
    private ReportRecurrenceScheduleService reportRecurrenceScheduleService;
    private LicenseService licenseService;


    public ReportFormController() {
        super();
        setCommandClass(Report.class);
        setCommandName("report");
        setFormView("reportEdit");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        Integer reportId = ServletRequestUtils.getIntParameter(request, "reportId");
        Report report;
        if (reportId != null) {
            report = reportService.getById(reportId); 
        } else {
            report = new Report();
            report.setUser(user);
            report.setShowRecurrenceSchedule(false);
        }

        if (report.getSchedule() == null) {
            ReportRecurrenceSchedule reportRecurrenceSchedule = new ReportRecurrenceSchedule();
            reportRecurrenceSchedule.setReport(report);
            reportRecurrenceSchedule.setOccurrenceCount(0);

            RecurrenceSchedule recurrenceSchedule = new RecurrenceSchedule();
            recurrenceSchedule.setReport(report);
            
            recurrenceSchedule.setPattern(RecurrenceSchedule.PATTERN_DAILY);
            recurrenceSchedule.setDailyInterval(1);
            recurrenceSchedule.setWeeklyInterval(1);
            recurrenceSchedule.setMonthlySchedule(RecurrenceSchedule.MONTHLY_DAY_OF_MONTH);
            recurrenceSchedule.setMonthlyDayOfMonth(1);
            recurrenceSchedule.setMonthlyDayOfMonthOccurrence(1);
            recurrenceSchedule.setYearlySchedule(RecurrenceSchedule.YEARLY_DAY_OF_YEAR);
            recurrenceSchedule.setYearlyDayOfYearDay(1);
            recurrenceSchedule.setYearlyDayOfYearMonth(1);
            recurrenceSchedule.setRangeStartDate(new Date());
            recurrenceSchedule.setRangeEnd(RecurrenceSchedule.RANGE_END_NEVER);
            recurrenceSchedule.setRunning(false);

            reportRecurrenceSchedule.setRecurrenceSchedule(recurrenceSchedule);
            report.setSchedule(reportRecurrenceSchedule);

            EmailMessageTemplate emailMessageTemplate = new EmailMessageTemplate();
            reportRecurrenceSchedule.setEmailMessageTemplate(emailMessageTemplate);
        }

        return report;
    }

    @Override
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        Map<String, Object> refData = new HashMap<String, Object>();
        refData.put("myFilters", ticketFilterService.getByUser(user));
        refData.put("pubFilters", ticketFilterService.getPublicMinusUser(user));
        refData.put("scheduledReports", licenseService.isScheduledReports());

        return refData;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        Report report = (Report) command;
        boolean scheduleEnabled = report.getShowRecurrenceSchedule();
        if(!scheduleEnabled){
            if(report.getSchedule() != null){
                if(report.getSchedule().getRecurrenceSchedule() != null){
                    report.getSchedule().getRecurrenceSchedule().setRunning(false);
                }
            }
        }
//        report.getSchedule().getRecurrenceSchedule().setRunning(scheduleEnabled);
//        if (!scheduleEnabled) {
//            // delete report recurrence schedule (cascading to recurrenceschedule and emailtemplate)
//            reportRecurrenceScheduleService.delete(report.getSchedule());
//            report.setSchedule(null);
//        }
        
        reportService.save(report);
        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("report.successSave"));
        return new ModelAndView(new RedirectView("/tf/reportList.glml", true));
//        return showForm(request, response, errors);
    }

    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
        binder.registerCustomEditor(TicketFilter.class, new TicketFilterEditor(ticketFilterService));
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }

    public void setReportRecurrenceScheduleService(ReportRecurrenceScheduleService reportRecurrenceScheduleService) {
        this.reportRecurrenceScheduleService = reportRecurrenceScheduleService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }
}
