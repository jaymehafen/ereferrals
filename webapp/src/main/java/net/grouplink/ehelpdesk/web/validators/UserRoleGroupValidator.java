package net.grouplink.ehelpdesk.web.validators;


import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserRoleGroupValidator implements Validator {

    public boolean supports(Class clazz) {
		return clazz.equals(UserRoleGroup.class);
	}

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "userRole.user", "group.role.userrequired", "User is required");
        ValidationUtils.rejectIfEmpty(errors, "userRole.role", "group.role.rolerequired", "Role is required");
    }

}
