package net.grouplink.ehelpdesk.web.tags;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class StringEscape implements Tag, Serializable {

    private PageContext pageContext;
    private Tag parentTag;
    private String value;
    private String type;

    public int doStartTag() throws JspException {
        String escapedValue = value;
        if (type.equals("html")) {
            escapedValue = StringEscapeUtils.escapeHtml(value);
        } else if (type.equals("javascript")) {
            escapedValue = StringEscapeUtils.escapeJavaScript(value);
        } else if (type.equals("xml")) {
            escapedValue = StringEscapeUtils.escapeXml(value);
        } else if (type.equals("urlEncode")) {
            try {
                escapedValue = URLEncoder.encode(value, "UTF-8");
                // replacing plus sign with a space
                escapedValue = StringUtils.replace(escapedValue, "+", " ");
            } catch (UnsupportedEncodingException ignored) {
            }
        }

        try {
            pageContext.getOut().write(escapedValue);
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        pageContext = null;
        parentTag = null;
        value = null;
    }

    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    public void setParent(Tag tag) {
        this.parentTag = tag;
    }

    public Tag getParent() {
        return parentTag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
