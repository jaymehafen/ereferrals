package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * User: jaymehafen
 * Date: Sep 24, 2008
 * Time: 6:18:35 PM
 */
public class AssetCustomFieldValidator extends BaseValidator  {

    public boolean supports(Class clazz) {
        return clazz.equals(AssetCustomField.class);
    }

    public void validate(Object target, Errors errors) {
        AssetCustomField acf = (AssetCustomField) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "global.fieldRequired", "This field cannot be empty");
    }

}