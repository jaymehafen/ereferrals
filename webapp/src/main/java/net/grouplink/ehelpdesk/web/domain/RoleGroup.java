package net.grouplink.ehelpdesk.web.domain;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Role;

public class RoleGroup {
	private Role role;
	private Group group;
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
}
