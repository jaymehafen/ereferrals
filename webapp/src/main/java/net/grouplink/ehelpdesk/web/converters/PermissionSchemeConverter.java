package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.acl.PermissionScheme;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;


public class PermissionSchemeConverter implements Converter<String, PermissionScheme> {
    @Autowired
    private PermissionService permissionService;

    public PermissionScheme convert(String id) {
        if (StringUtils.isBlank(id)) return null;

        return permissionService.getPermissionSchemeById(Integer.valueOf(id));
    }
}
