package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class AssetFormatter implements Formatter<Asset> {
    @Autowired
    private AssetService assetService;

    public Asset parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return assetService.getAssetByAssetNumber(text);
        } else {
            return null;
        }
    }

    public String print(Asset object, Locale locale) {
        return object == null ? "" : object.getAssetNumber();
    }
}
