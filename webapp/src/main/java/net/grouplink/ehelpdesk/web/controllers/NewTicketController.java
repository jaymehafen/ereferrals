package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class NewTicketController extends TicketController {
    @ModelAttribute("ticket")
    public Ticket ticket(@RequestParam(value = "contactId", required = false) User contact,
                         @RequestParam(value = "groupId", required = false) Group group,
                         @RequestParam(value = "parentId", required = false) Integer parentId) {

        User seshUser = userService.getLoggedInUser();
        return createNewTicket(seshUser, contact, group, parentId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newForm() {
        return "tickets/form";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(Model model, @ModelAttribute("ticket") Ticket ticket, BindingResult bindingResult,
                         HttpServletRequest request) throws Exception {

        setTicketDefaults(ticket);
        if(processTicket(ticket, request, bindingResult)) {

            request.setAttribute("flash.successfulSave", true);
            model.asMap().clear();
            return "redirect:/tickets/" + ticket.getId() + "/edit";
        }
        return "tickets/form";
    }

    private void setTicketDefaults(Ticket ticket) {
        User user = userService.getLoggedInUser();

        if(!permissionService.hasGroupPermission("ticket.change.contact", ticket.getGroup()) && ticket.getContact() == null) {
            ticket.setContact(user);
        }

        if(!permissionService.hasGroupPermission("ticket.change.location", ticket.getGroup()) && ticket.getLocation() == null) {
            ticket.setLocation(ticket.getContact().getLocation());
        }

        if(!permissionService.hasGroupPermission("ticket.change.subject", ticket.getGroup()) && StringUtils.isBlank(ticket.getSubject())) {
            ticket.setSubject(getMessageSourceAccessor().getMessage("ticket.noSubjectProvided"));
        }

        if(ticket.getStatus() == null){
            ticket.setStatus(statusService.getInitialStatus());
        }

        if(ticket.getPriority() == null){
            ticket.setPriority(priorityService.getInitialPriority());
        }

        if(!permissionService.hasGroupPermission("ticket.change.assignedTo", ticket.getGroup()) && ticket.getAssignedTo() == null) {
            List<UserRoleGroup> assignments = assignmentService.getMoreBetterAssignments(ticket.getLocation(), ticket.getGroup(), ticket.getCategory(), ticket.getCategoryOption());
            if(assignments.size() == 1) {
                ticket.setAssignedTo(assignments.get(0));
            } else {
                ticket.setAssignedTo(userRoleGroupService.getTicketPoolByGroupId(ticket.getGroup().getId()));
            }
        }
    }
}
