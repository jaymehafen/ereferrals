package net.grouplink.ehelpdesk.web.controllers.asset;

import com.Ostermiller.util.CSVParse;
import com.Ostermiller.util.CSVParser;
import com.Ostermiller.util.ExcelCSVParser;
import com.Ostermiller.util.LabeledCSVParser;
import net.grouplink.ehelpdesk.common.utils.DateTimeUtils;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomField;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomFieldValue;
import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.ImprovedAccountingInfo;
import net.grouplink.ehelpdesk.domain.asset.Vendor;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.AssetCustomFieldService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import net.grouplink.ehelpdesk.service.asset.AssetTypeService;
import net.grouplink.ehelpdesk.service.asset.VendorService;
import net.grouplink.ehelpdesk.web.controllers.AbstractEHelpDeskFormController;
import net.grouplink.ehelpdesk.web.domain.AssetImport;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Aug 19, 2008
 * Time: 3:12:22 PM
 */
public class AssetImportFormController extends AbstractEHelpDeskFormController {

    protected final Log logger = LogFactory.getLog(getClass());

    private String[] assetFields = {
            "asset.assetnumber",
            "asset.name",
            "asset.type",
            "asset.owner",
            "asset.vendor",
            "asset.assetlocation",
            "asset.acquisitiondate",
            "asset.location",
            "asset.group",
            "asset.category",
            "asset.categoryoption",
            "asset.manufacturer",
            "asset.modelnumber",
            "asset.serialnumber",
            "asset.description",
            "asset.status"
    };

    private String[] acctgFields = {
            "accountinginfo.ponumber",
            "accountinginfo.accountnumber",
            "accountinginfo.accumulatednumber",
            "accountinginfo.expensenumber",
            "accountinginfo.acquisitionvalue",
            "accountinginfo.leasenumber",
            "accountinginfo.leaseduration",
            "accountinginfo.leasefrequency",
            "accountinginfo.leasefrequencyprice",
            "accountinginfo.disposalmethod",
            "accountinginfo.disposaldate",
            "accountinginfo.insurancecategory",
            "accountinginfo.replacementvaluecategory",
            "accountinginfo.replacementvalue",
            "accountinginfo.maintenancecost",
            "accountinginfo.depreciationmethod",
            "accountinginfo.leaseexpirationdate",
            "accountinginfo.warrantydate"
    };

    private String[] remoteFields = {
            "asset.remote.hostname",
            "asset.remote.vncport"
    };

    private String[][] parsedValues;
    private String[] labels;
    private LinkedHashMap assetFieldsMap;
    private LinkedHashMap acctgFieldsMap;
    private LinkedHashMap remoteFieldsMap;

    private AssetService assetService;
    private AssetCustomFieldService assetCustomFieldService;
    private AssetTypeService assetTypeService;
    private UserService userService;
    private VendorService vendorService;
    private LocationService locationService;
    private GroupService groupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private AssetStatusService assetStatusService;

    /**
     * Create a new AssetImportFormController setting command to <code>AssetImport</code>
     * and the assetImport.jsp as the view
     */
    public AssetImportFormController() {
        super();
        setCommandClass(AssetImport.class);
        setCommandName("assetImport");
        setFormView("assetImport");
        setSuccessView("import.glml");
    }

    private LinkedHashMap initAssetFields() {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        for (String name : assetFields) {
            map.put(name, getMessageSourceAccessor().getMessage(name));
        }
        return map;
    }

    private LinkedHashMap initAcctgFields() {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        for (String name : acctgFields) {
            map.put(name, getMessageSourceAccessor().getMessage(name));
        }
        return map;
    }

    private LinkedHashMap initRemoteFields() {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        for (String name : remoteFields) {
            map.put(name, getMessageSourceAccessor().getMessage(name));
        }
        return map;
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {

        // if forceRefresh is called remove the command object from the session
        if (ServletRequestUtils.getBooleanParameter(request, "forceRefresh", false)) {
            request.getSession().removeAttribute("assImport");
        }

        AssetImport assetImport = (AssetImport) request.getSession().getAttribute("assImport");
        if (assetImport == null) assetImport = new AssetImport();

        return assetImport;
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
                                    BindException errors) throws Exception {
        AssetImport assetImport = (AssetImport) command;

        // Determine which submit button was pushed
        // if loadButton put the command on the session for the next submit
        if (ServletRequestUtils.getStringParameter(request, "loadButton") != null) {
            request.getSession().setAttribute("assImport", assetImport);
            parseCSV(assetImport);
        }

        if (ServletRequestUtils.getStringParameter(request, "importButton") != null) {
            doTheImport(assetImport.getImportMap(), assetImport);
        }

        if (labels[labels.length-1].trim().equals("")) {
            String[] _labels = new String[labels.length-1];
            System.arraycopy(labels, 0, _labels, 0, labels.length - 1);
            labels = _labels;
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("assetImport", assetImport);
        model.put("csvLabels", labels);
        model.put("fieldOptions", getFieldOptions());
        return showForm(request, response, errors, model);
    }


    private void doTheImport(Map importMap, AssetImport assetImport) {

        for (int i = 0; i < parsedValues.length; i++) {
            // every i is a row in the csv file therefore it represents a new asset(s) obj
            Asset asset = lookupAsset(i, importMap);

            // catch any exceptions in processing this row
            int j = 0;
            try {
                for (; j < parsedValues[i].length; j++) {
                    setTheProperty((String) importMap.get("col" + j), parsedValues[i][j], asset, asset.getAccountingInfo(), assetImport);
                }
                assetService.saveAsset(asset);
            } catch (ParseException pe) {
                // inform logger of the offending row
                logger.error("Error parsing date at row " + i + ", column " + j + ". Format should be " +
                        ((SimpleDateFormat) DateTimeUtils.STANDARD_DATE).toPattern());
            }
        }
    }

    private Asset lookupAsset(int index, Map importMap){
        Asset asset = null;
        if(importMap.containsValue("asset.assetnumber")){
            for(int i=0; i<parsedValues[index].length; i++){
                if(importMap.get("col"+i).equals("asset.assetnumber")){
                    asset = assetService.getAssetByAssetNumber(parsedValues[index][i]);
                    break;
                }
            }
        }

        if(asset == null){
            asset = new Asset();
            asset.setAccountingInfo(new ImprovedAccountingInfo());
        }
        return asset;
    }

    private void setTheProperty(String property, String value, Asset asset, ImprovedAccountingInfo accountingInfo,
                                AssetImport assetImport) throws ParseException {
        //System.out.println("property: " + property);
        if ("asset.assetnumber".equals(property)) {
            asset.setAssetNumber(value);
        } else if ("asset.name".equals(property)) {
            asset.setName(value);
        } else if ("asset.type".equals(property)) {
            AssetType assetType = assetTypeService.getByName(value);
            if (assetType == null) {
                assetType = new AssetType();
                assetType.setName(value);
                assetTypeService.saveAssetType(assetType);
                assetTypeService.flush();
                logger.warn("New Asset Type '" + value + "' created");
            }
            asset.setAssetType(assetType);
        } else if ("asset.owner".equals(property)) {
            User owner = userService.getUserByUsername(value);
//            if (owner == null) {
//                owner = new User();
//                owner.setLoginId(value);
//                owner.setOnVacation(false);
//                owner.setActive(true);
//                userService.saveUser(owner);
//                userService.flush();
//                logger.warn("New User '" + value + "' created");
//            }
            asset.setOwner(owner);
        } else if ("asset.vendor".equals(property)) {
            Vendor vendor = vendorService.getByName(value);
            if (vendor == null) {
                vendor = new Vendor();
                vendor.setName(value);
                vendorService.saveVendor(vendor);
                vendorService.flush();
                logger.warn("New Vendor '" + value + "' created");
            }
            asset.setVendor(vendor);
        } else if ("asset.assetlocation".equals(property)) {
            asset.setAssetLocation(value);
        } else if ("asset.acquisitiondate".equals(property)) {
            if(!"".equals(value)) {
                asset.setAcquisitionDate(DateTimeUtils.getDateFromStandardDateFormat(value));
            }
        } else if ("asset.location".equals(property)) {
            Location location = locationService.getLocationByName(value);
            if (location == null && assetImport.isAddLocation()) {
                location = new Location();
                location.setName(value);
                location.setActive(true);
                locationService.saveLocation(location);
                locationService.flush();
                logger.warn("New Location '" + value + "' created");
            }
            asset.setLocation(location);
        } else if ("asset.group".equals(property)) {
            Group group = groupService.getGroupByName(value);
            if (group == null && assetImport.isAddGroup()) {
                group = new Group();
                group.setName(value);
                group.setActive(true);
                groupService.saveGroup(group);
                groupService.flush();
                logger.warn("New Group '" + value + "' created");
            }
            asset.setGroup(group);
        } else if ("asset.category".equals(property)) {
            Category category = categoryService.getCategoryByName(value);
//            if (category == null && assetImport.isAddCategory()) {
//                category = new Category();
//                category.setName(value);
//                category.setActive(true);
//                categoryService.saveCategory(category);
//                categoryService.flush();
//                logger.warn("New Category '" + value + "' created");
//            }
            if(category == null){
                logger.warn("Category '"+value+"' not found. Cannot create new Category." );
            } else {
                asset.setCategory(category);
            }
        } else if ("asset.categoryoption".equals(property)) {
            CategoryOption categoryOption = categoryOptionService.getCategoryOptionByName(value);
//            if (categoryOption == null && assetImport.isAddCategoryOption()) {
//                categoryOption = new CategoryOption();
//                categoryOption.setName(value);
//                categoryOption.setActive(true);
//                categoryOptionService.saveCategoryOption(categoryOption);
//                categoryOptionService.flush();
//                logger.warn("New Category Option '" + value + "' created");
//            }
            if(categoryOption == null){
                logger.warn("CategoryOption '"+value+"' not found. Cannot create new CategoryOption.");
            } else {
                asset.setCategoryOption(categoryOption);
            }
        } else if ("asset.manufacturer".equals(property)) {
            asset.setManufacturer(value);
        } else if ("asset.modelnumber".equals(property)) {
            asset.setModelNumber(value);
        } else if ("asset.serialnumber".equals(property)) {
            asset.setSerialNumber(value);
        } else if ("asset.description".equals(property)) {
            asset.setDescription(value);
        } else if ("asset.status".equals(property)) {
            AssetStatus assetStatus = assetStatusService.getByName(value);
            if (assetStatus == null) {
                assetStatus = new AssetStatus();
                assetStatus.setName(value);
                assetStatusService.saveAssetStatus(assetStatus);
                assetStatusService.flush();
                logger.warn("New Asset Status '" + value + "' created");
            }
            asset.setStatus(assetStatus);
        } else if ("accountinginfo.ponumber".equals(property)) {
            accountingInfo.setPoNumber(value);
        } else if ("accountinginfo.accountnumber".equals(property)) {
            accountingInfo.setAccountNumber(value);
        } else if ("accountinginfo.accumulatednumber".equals(property)) {
            accountingInfo.setAccumulatedNumber(value);
        } else if ("accountinginfo.expensenumber".equals(property)) {
            accountingInfo.setExpenseNumber(value);
        } else if ("accountinginfo.acquisitionvalue".equals(property)) {
            accountingInfo.setAcquisitionValue(value);
        } else if ("accountinginfo.leasenumber".equals(property)) {
            accountingInfo.setLeaseNumber(value);
        } else if ("accountinginfo.leaseduration".equals(property)) {
            accountingInfo.setLeaseDuration(value);
        } else if ("accountinginfo.leasefrequency".equals(property)) {
            accountingInfo.setLeaseFrequency(value);
        } else if ("accountinginfo.leasefrequencyprice".equals(property)) {
            accountingInfo.setLeaseFrequencyPrice(value);
        } else if ("accountinginfo.disposalmethod".equals(property)) {
            accountingInfo.setDisposalMethod(value);
        } else if ("accountinginfo.disposaldate".equals(property)) {
            accountingInfo.setDisposalDate(DateTimeUtils.getDateFromStandardDateFormat(value));
        } else if ("accountinginfo.insurancecategory".equals(property)) {
            accountingInfo.setInsuranceCategory(value);
        } else if ("accountinginfo.replacementvaluecategory".equals(property)) {
            accountingInfo.setReplacementValueCategory(value);
        } else if ("accountinginfo.replacementvalue".equals(property)) {
            accountingInfo.setReplacementValue(value);
        } else if ("accountinginfo.maintenancecost".equals(property)) {
            accountingInfo.setMaintenanceCost(value);
        } else if ("accountinginfo.depreciationmethod".equals(property)) {
            accountingInfo.setDepreciationMethod(value);
        } else if ("accountinginfo.leaseexpirationdate".equals(property)) {
            accountingInfo.setLeaseExpirationDate(DateTimeUtils.getDateFromStandardDateFormat(value));
        } else if ("accountinginfo.warrantydate".equals(property)) {
            if(!"".equals(value)) {
                accountingInfo.setWarrantyDate(DateTimeUtils.getDateFromStandardDateFormat(value));
            }
        } else if("asset.remote.hostname".equals(property)){
            asset.setHostname(value);
        } else if("asset.remote.vncport".equals(property)){
            if (StringUtils.isBlank(value)) {
                asset.setVncPort(null);
            } else if (StringUtils.isNumeric(value.trim())) {
                asset.setVncPort(Integer.valueOf(value.trim()));
            }
        } else if (property.split("\\.")[0].equals("assetcustomfield")) {
            Integer cfId = new Integer(property.split("\\.")[1]);
            AssetCustomField custFld = assetCustomFieldService.getById(cfId);

            String cfType = custFld.getCustomFieldType().trim();

            if (cfType.equals("date")) {

                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                try {
                    Date cfDate = sdf.parse(value.trim());
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                    value = fmt.format(cfDate);
                } catch (Exception e) {
                    // inform logger of the offending row
                    logger.error("Error parsing date " + value + ". Format should be " + (sdf.toPattern())); 
                }
            }

            if (cfType.equals("checkbox")) {
                if ((value.trim().equalsIgnoreCase("1")) ||
                   (value.trim().equalsIgnoreCase("true")) ||
                   (value.trim().equalsIgnoreCase("t"))) {
                    value = "true";
                }
                else if ((value.trim().equalsIgnoreCase("0")) ||
                        (value.trim().equalsIgnoreCase("false")) ||
                        (value.trim().equalsIgnoreCase("f"))) {
                    value = "false";
                } else {
                    logger.error("Incorrect value ("+value+") for checkbox custom field.  Must be one of 'true', 'false', '1', '0', 't', 'f'.");
                }
                    
                
            }

            AssetCustomFieldValue acfv = new AssetCustomFieldValue();
            acfv.setAsset(asset);
            acfv.setCustomField(custFld);

            // check that the asset hasn't already been imported so duplicate acfv is not made
            for (AssetCustomFieldValue next : asset.getCustomFieldValues()) {
                if(next.getAsset().equals(asset) && next.getCustomField().equals(custFld)){
                    acfv = next;
                }
            }
            acfv.setFieldValue(value);
            acfv.setFieldValueText(value);
//            assetCustomFieldService.saveAssetCustomField(custFld);

            asset.getCustomFieldValues().add(acfv);
//            assetService.saveAsset(asset);
        }
    }

    private void parseCSV(AssetImport assetImport) throws Exception {
        CSVParse csvParse;

        if ("xcelcsv".equals(assetImport.getCsvType())) {
            csvParse = new ExcelCSVParser(new ByteArrayInputStream(assetImport.getFileData()));
            if (assetImport.isFirstLineHeader()) {
                LabeledCSVParser lcsvp = new LabeledCSVParser(csvParse);
                parsedValues = lcsvp.getAllValues();
                labels = lcsvp.getLabels();
            } else {
                parsedValues = csvParse.getAllValues();
                labels = getGenericLabels(parsedValues[0].length);
            }
        } else /*if("csv".equals(assetImport.getCsvType()))*/ {
            csvParse = new CSVParser(new ByteArrayInputStream(assetImport.getFileData()));
            if (assetImport.isFirstLineHeader()) {
                LabeledCSVParser lcsvp = new LabeledCSVParser(csvParse);
                parsedValues = lcsvp.getAllValues();
                labels = lcsvp.getLabels();
            } else {
                parsedValues = csvParse.getAllValues();
                labels = getGenericLabels(parsedValues[0].length);
            }
        }
    }

    private String[] getGenericLabels(int cols) {
        String[] retVal = new String[cols];
        for (int i = 0; i < cols; i++) {
            retVal[i] = "Column " + (i + 1);
        }
        return retVal;
    }

    private String getFieldOptions() {
        if (assetFieldsMap == null) assetFieldsMap = initAssetFields();
        if (acctgFieldsMap == null) acctgFieldsMap = initAcctgFields();
        if (remoteFieldsMap == null) remoteFieldsMap = initRemoteFields();

        StringBuilder sb = new StringBuilder("<option value=\"\"></option>");
        sb.append("<optgroup label=\"").append(getMessageSourceAccessor().getMessage("asset.fieldNames")).append("\">");
        for (String name : assetFields) {
            sb.append("<option value=\"").append(name).append("\">").append(assetFieldsMap.get(name)).append("</option>");
        }
        sb.append("</optgroup>");

        sb.append("<optgroup label=\"").append(getMessageSourceAccessor().getMessage("asset.acctgFieldNames")).append("\">");
        for (String name : acctgFields) {
            sb.append("<option value=\"").append(name).append("\">").append(acctgFieldsMap.get(name)).append("</option>");
        }
        sb.append("</optgroup>");

        sb.append("<optgroup label=\"").append(getMessageSourceAccessor().getMessage("asset.remoteFieldNames")).append("\">");
        for (String name : remoteFields) {
            sb.append("<option value=\"").append(name).append("\">").append(remoteFieldsMap.get(name)).append("</option>");
        }
        sb.append("</optgroup>");

        List customFields = assetCustomFieldService.getAll();
        if (customFields.size() > 0) {
            sb.append("<optgroup label=\"").append(getMessageSourceAccessor().getMessage("asset.custfieldNames")).append("\">");
            for (Object customField : customFields) {
                AssetCustomField assetCustomField = (AssetCustomField) customField;
                sb.append("<option value=\"assetcustomfield.").
                        append(assetCustomField.getId()).append("\">").append(assetCustomField.getName()).append("</option>");
            }
            sb.append("</optgroup>");
        }
        return sb.toString();
    }



    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p>This method allows the bean instance to perform initialization only
     * possible when all bean properties have been set and to throw an
     * exception in the event of misconfiguration.
     *
     * @throws Exception in the event of misconfiguration (such
     *                   as failure to set an essential property) or if initialization fails.
     */
    public void afterPropertiesSet() throws Exception {

    }

    public void setAssetCustomFieldService(AssetCustomFieldService assetCustomFieldService) {
        this.assetCustomFieldService = assetCustomFieldService;
    }

    public void setAssetTypeService(AssetTypeService assetTypeService) {
        this.assetTypeService = assetTypeService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setVendorService(VendorService vendorService) {
        this.vendorService = vendorService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }
}
