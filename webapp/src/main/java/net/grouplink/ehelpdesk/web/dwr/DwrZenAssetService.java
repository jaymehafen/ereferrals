package net.grouplink.ehelpdesk.web.dwr;

import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.zen.Workstation;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import net.grouplink.ehelpdesk.service.zen.WorkstationService;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * User: jaymehafen
 * Date: Apr 6, 2009
 * Time: 4:26:44 PM
 */
public class DwrZenAssetService implements ApplicationContextAware {

    private Log log = LogFactory.getLog(getClass());

    private ApplicationContext applicationContext;
    
    private ZenAssetService zenAssetService;

    private WorkstationService workstationService;

    public Integer save(ZenAsset zenAsset) {
        zenAssetService.save(zenAsset);
        return zenAsset.getId(); 
    }

    public Integer getZenAssetIdForWorkstationId(String workstationId) {
        ZenAsset za = null;
        try {
            za = zenAssetService.getByWorkstationId(Hex.decodeHex(workstationId.toCharArray()));
        } catch (DecoderException e) {
            log.error("error decoding hex string " + workstationId + ": " + e.getMessage(), e);
        }
        if (za == null) {
            Workstation w = getWorkstationService().getByIdString(workstationId);
            if (w == null) {
                throw new RuntimeException("workstation not found for " + workstationId);
            }
            
            za = new ZenAsset();
            za.setAssetName(w.getMachineName());
            za.setAssetType(w.getSystemProduct());
            za.setDeviceId(w.getDevice().getZuid());
            za.setOperatingSystem(w.getOsProduct());
            za.setWorkstationId(w.getId());
            zenAssetService.save(za);
        }

        return za.getId();
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    private WorkstationService getWorkstationService() {
        if (workstationService == null) {
            workstationService = (WorkstationService) applicationContext.getBean("workstationService");
        }

        return workstationService;
    }

    public void setWorkstationService(WorkstationService workstationService) {
        this.workstationService = workstationService;
    }
}
