package net.grouplink.ehelpdesk.web.domain;

public class ConfigMenuOptions {
    private boolean customFields;
    private boolean groups;
    private boolean scheduler;
    private boolean kbManage;
    private boolean assignments;

    public boolean getCustomFields() {
        return customFields;
    }

    public void setCustomFields(boolean customFields) {
        this.customFields = customFields;
    }

    public boolean getGroups() {
        return groups;
    }

    public void setGroups(boolean groups) {
        this.groups = groups;
    }

    public boolean getScheduler() {
        return scheduler;
    }

    public void setScheduler(boolean scheduler) {
        this.scheduler = scheduler;
    }

    public boolean getKbManage() {
        return kbManage;
    }

    public void setKbManage(boolean kbManage) {
        this.kbManage = kbManage;
    }

    public boolean getAssignments() {
        return assignments;
    }

    public void setAssignments(boolean assignments) {
        this.assignments = assignments;
    }
}
