package net.grouplink.ehelpdesk.web.tags;

import net.grouplink.ehelpdesk.common.utils.RegexUtils;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.io.Serializable;

public class FormatHistoryNote implements Tag, Serializable {

    private PageContext pageContext;
    private Tag parentTag;
    private TicketHistory ticketHistory;

    public int doStartTag() throws JspException {
        String formattedNote;
        if (ticketHistory.isTextFormat()) {
            // replace newlines with <br/>
            formattedNote = StringEscapeUtils.escapeHtml(ticketHistory.getNote());
            formattedNote = RegexUtils.replaceNewLinesWithBRTags(formattedNote);
        } else {
            // strip out html tags but preserve <br>'s
            formattedNote = RegexUtils.replaceBRTagsWithNewLine(ticketHistory.getNote());
            formattedNote = RegexUtils.stripHtmlTags(formattedNote);
            formattedNote = StringUtils.replace(formattedNote, "&nbsp;", " ");

            formattedNote = StringEscapeUtils.escapeHtml(formattedNote);
            formattedNote = RegexUtils.replaceNewLinesWithBRTags(formattedNote);
        }

        try {
            pageContext.getOut().write(formattedNote);
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        pageContext = null;
        parentTag = null;
        ticketHistory = null;
    }

    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    public void setParent(Tag tag) {
        this.parentTag = tag;
    }

    public Tag getParent() {
        return parentTag;
    }

    public TicketHistory getTicketHistory() {
        return ticketHistory;
    }

    public void setTicketHistory(TicketHistory ticketHistory) {
        this.ticketHistory = ticketHistory;
    }
}