package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import net.grouplink.ehelpdesk.service.ScheduleStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 12/13/11
 * Time: 10:28 AM
 */
public class ScheduleStatusConverter implements Converter<String, ScheduleStatus> {
    @Autowired private ScheduleStatusService scheduleStatusService;

    public ScheduleStatus convert(String id) {
        try {
            int sid = Integer.parseInt(id);
            return scheduleStatusService.getScheduleStatusById(sid);
        } catch (Exception e) {
            return null;
        }
    }
}
