package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.domain.acl.PermissionScheme;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.annotations.GlobalPermission;
import net.grouplink.ehelpdesk.web.domain.NotificationAssistantForm;
import net.grouplink.ehelpdesk.web.validators.CategoryOptionValidator;
import net.grouplink.ehelpdesk.web.validators.CategoryValidator;
import net.grouplink.ehelpdesk.web.validators.GroupValidator;
import net.grouplink.ehelpdesk.web.validators.UserRoleGroupValidator;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = "/groups")
public class GroupController extends ApplicationObjectSupport {

    @Autowired
    private GroupService groupService;
    @Autowired
    private GroupValidator groupValidator;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryValidator categoryValidator;
    @Autowired
    private CategoryOptionValidator categoryOptionValidator;
    @Autowired
    private CategoryOptionService categoryOptionService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleGroupService userRoleGroupService;
    @Autowired
    private UserRoleGroupValidator userRoleGroupValidator;
    @Autowired
    private LicenseService licenseService;


    @RequestMapping(method = RequestMethod.GET)
    public String indexGroups(Model model, HttpServletRequest request) {

        List<Group> groups = groupService.getAllGroups();
        // Get the user
        for (Iterator<Group> iterator = groups.iterator(); iterator.hasNext(); ) {
            Group group = iterator.next();
            if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
                iterator.remove();
            }
        }
        model.addAttribute(groups);

        boolean nogroups = ServletRequestUtils.getBooleanParameter(request, "nogroups", false);
        if (nogroups) {
            model.addAttribute("nogroups", true);
        }

        return "groups/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    @GlobalPermission("global.systemConfig")
    public String newForm(Model model) {
        model.addAttribute(new Group());
        return "groups/form";
    }

    @RequestMapping(method = RequestMethod.POST)
    @GlobalPermission("global.systemConfig")
    public String create(@ModelAttribute Group group, BindingResult bindingResult) {
        groupValidator.validate(group, bindingResult);
        if (!bindingResult.hasErrors()) {
            groupService.saveGroup(group);
            return "redirect:/config/groups/" + group.getId() + "/edit";
        }
        return "groups/form";
    }

    @ModelAttribute("permissionSchemeList")
    public List<PermissionScheme> permissionSchemes() {
        return permissionService.getPermissionSchemes();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/edit")
    public String edit(Model model, @PathVariable("groupId") Group group) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        model.addAttribute(group);
        model.addAttribute("urgList", userRoleGroupService.getUserRoleGroupSelectionByGroupId(group.getId()));
        model.addAttribute("urgTicketPool", userRoleGroupService.getTicketPoolByGroupId(group.getId()));
        return "groups/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{group}")
    public String udpate(Model model, @ModelAttribute("group") Group group, BindingResult bindingResult) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        groupValidator.validate(group, bindingResult);
        if (!bindingResult.hasErrors()) {
            groupService.saveGroup(group);
        }
        model.addAttribute("urgList", userRoleGroupService.getUserRoleGroupSelectionByGroupId(group.getId()));
        model.addAttribute("urgTicketPool", userRoleGroupService.getTicketPoolByGroupId(group.getId()));
        return "groups/form";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{groupId}")
    @GlobalPermission("global.systemConfig")
    public void delete(@PathVariable("groupId") Group group, HttpServletResponse response) {
        boolean deleted = groupService.deleteGroupIfUnused(group);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/categoryjson")
    public void getCategoriesByGroup(@PathVariable("groupId") Group group, HttpServletResponse response)
            throws IOException, JSONException {

        List<Category> categories = categoryService.getAllCategoriesByGroup(group);

        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
        for (Category category : categories) {
            JSONObject catItem = new JSONObject();
            catItem.put("id", category.getId());
            catItem.put("name", category.getName());
            catItem.put("type", "category");

            Set<CategoryOption> catOpts = category.getCategoryOptions();
            List<JSONObject> chillens = new ArrayList<JSONObject>();
            for (CategoryOption catOpt : catOpts) {
                JSONObject chile = new JSONObject();
                chile.put("_reference", "copt" + catOpt.getId());
                chillens.add(chile);

                JSONObject catoptItem = new JSONObject();
                catoptItem.put("id", "copt" + catOpt.getId());
                catoptItem.put("name", catOpt.getName());
                catoptItem.put("type", "categoryOption");
                catoptItem.put("catId", catOpt.getCategory().getId());
                jsonObjects.add(catoptItem);
            }
            catItem.put("children", chillens);
            jsonObjects.add(catItem);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", jsonObjects);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/categories/new")
    public String newCategory(Model model, @PathVariable("groupId") Group group) {
        model.addAttribute("category", new Category());
        model.addAttribute("group", group);
        return "groups/categoryForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{groupId}/categories")
    public String createCategory(Model model, @ModelAttribute Category category, BindingResult bindingResult,
                                 @PathVariable("groupId") Group group) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        
        // Set the group so validation can occur properly
        category.setGroup(group);

        // Manually validate category
        categoryValidator.validate(category, bindingResult);
        if (!bindingResult.hasErrors()) {
            categoryService.saveCategory(category);
        }
        model.addAttribute("group", group);
        return "groups/categoryForm";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/categories/{catId}/edit")
    public String editCategory(Model model, @PathVariable("groupId") Group group, @PathVariable("catId") Category category) {
        model.addAttribute("group", group);
        model.addAttribute("category", category);
        return "groups/categoryForm";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{group}/categories/{category}")
    public String updateCategory(Model model, @ModelAttribute("category") Category category, BindingResult bindingResult,
                                 @PathVariable("group") Group group) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        
        // Manually validate category
        categoryValidator.validate(category, bindingResult);
        if (!bindingResult.hasErrors()) {
            categoryService.saveCategory(category);
        }
        model.addAttribute("group", group);
        return "groups/categoryForm";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/categoryOptions/new")
    public String newCategoryOption(Model model, @PathVariable("groupId") Group group,
                                    @RequestParam(value = "catId", required = false) Category category,
                                    @RequestParam(value = "saveSuccess", required = false, defaultValue = "false") Boolean saveSuccess) {
        CategoryOption categoryOption = new CategoryOption();
        categoryOption.setCategory(category);
        model.addAttribute("categoryOption", categoryOption);
        model.addAttribute("group", group);
        model.addAttribute("categories", group.getCategoryList());
        model.addAttribute("saveSuccess", saveSuccess);

        return "groups/categoryOptionForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{groupId}/categoryOptions")
    public String createCategoryOption(Model model, @ModelAttribute CategoryOption cateogoryOption,
                                       BindingResult bindingResult, @PathVariable("groupId") Group group) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        
        categoryOptionValidator.validate(cateogoryOption, bindingResult);
        if (!bindingResult.hasErrors()) {
            categoryOptionService.saveCategoryOption(cateogoryOption);
        }
        model.addAttribute("group", group);
        return "groups/categoryOptionForm";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/categoryOptions/{catOptId}/edit")
    public String editCategoryOption(Model model, @PathVariable("groupId") Group group,
                                     @PathVariable("catOptId") CategoryOption categoryOption) {
        model.addAttribute("group", group);
        model.addAttribute("category", categoryOption.getCategory());
        model.addAttribute("categoryOption", categoryOption);
        return "groups/categoryOptionForm";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{groupId}/categoryOptions/{categoryOption}")
    public String updateCategoryOption(Model model, @ModelAttribute("categoryOption") CategoryOption categoryOption,
                                       BindingResult bindingResult, @PathVariable("groupId") Group group) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        
        categoryOptionValidator.validate(categoryOption, bindingResult);
        if (!bindingResult.hasErrors()) {
            categoryOptionService.saveCategoryOption(categoryOption);
        }
        model.addAttribute("group", group);
        return "groups/categoryOptionForm";
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/{groupId}/category/{catId}")
    public void deleteCategory(@PathVariable("catId") Category category, @PathVariable("groupId") Group group, 
                               HttpServletResponse response) {        
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        
        boolean deleted = categoryService.deleteCategoryIfUnused(category);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{groupId}/categoryOptions/{catOptId}")
    public void deleteCategoryOption(@PathVariable("catOptId") CategoryOption categoryOption, 
                                     @PathVariable("groupId") Group group, HttpServletResponse response) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        
        boolean deleted = categoryOptionService.deleteCategoryOptionIfUnused(categoryOption);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/groupRoles/new")
    public String newGroupRole(Model model, @PathVariable("groupId") Group group,
                               @RequestParam(value = "saveSuccess", required = false, defaultValue = "false") Boolean saveSuccess) {

        model.addAttribute("userRoleGroup", new UserRoleGroup());
        model.addAttribute("group", group);
        model.addAttribute("saveSuccess", saveSuccess);

        Integer allowedTechs = licenseService.getTechnicianCount();
        Integer techCount = userRoleGroupService.getTechnicianCount();

        Integer allowedWFPs = licenseService.getWFParticpantCount();
        Integer wfpCount = userRoleGroupService.getWFParticipantCount();

        if(techCount >= allowedTechs) model.addAttribute("tekCountExceeded", true);
        if(wfpCount >= allowedWFPs && wfpCount > 0) model.addAttribute("wfpCountExceeded", true);

        return "groups/groupRoleForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{groupId}/groupRoles")
    public String createGroupRole(Model model, @ModelAttribute UserRoleGroup userRoleGroup,
                                  BindingResult bindingResult, @PathVariable("groupId") Group group) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial?");
        }
        
        userRoleGroup.setGroup(group);
        userRoleGroupValidator.validate(userRoleGroup, bindingResult);
        if (!bindingResult.hasErrors()) {
            userRoleGroupService.saveUserRoleGroup(userRoleGroup);
        }
        model.addAttribute("group", group);
        return "groups/groupRoleForm";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{groupId}/groupRoles/{urgId}")
    public void deleteUserRoleGroup(@PathVariable("urgId") UserRoleGroup userRoleGroup,
                                    @PathVariable("groupId") Group group, HttpServletResponse response) {
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial");
        }

        boolean deleted = userRoleGroupService.delete(userRoleGroup);
        if (deleted) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/groupRoles/userNoRolesJson")
    public void getNoRoleUsersByGroup(@PathVariable("groupId") Group group, HttpServletResponse response)
            throws IOException, JSONException {

        List<User> userList = userService.getUsers();
        List<UserRoleGroup> urgList = userRoleGroupService.getUserRoleGroupSelectionByGroupId(group.getId());
        for (UserRoleGroup urg : urgList) {
            userList.remove((urg).getUserRole().getUser());
        }
        Collections.sort(userList);

        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
        for (User user : userList) {
            JSONObject userItem = new JSONObject();
            userItem.put("id", user.getId());
            userItem.put("name", user.getLastName() + ", " + user.getFirstName());
            jsonObjects.add(userItem);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", jsonObjects);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/groupRoles/roles")
    public void getGroupRolesByGroup(@PathVariable("groupId") Group group, HttpServletResponse response)
            throws IOException, JSONException {

        Integer allowedTechs = licenseService.getTechnicianCount();
        Integer techCount = userRoleGroupService.getTechnicianCount();

        Integer allowedWFPs = licenseService.getWFParticpantCount();
        Integer wfpCount = userRoleGroupService.getWFParticipantCount();

        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();

        JSONObject userItem = new JSONObject();

        if (techCount < allowedTechs) {
            userItem.put("id", Role.ROLE_MANAGER_ID);
            userItem.put("name", getMessageSourceAccessor().getMessage("ROLE_MANAGER"));
            jsonObjects.add(userItem);

            userItem = new JSONObject();
            userItem.put("id", Role.ROLE_TECHNICIAN_ID);
            userItem.put("name", getMessageSourceAccessor().getMessage("ROLE_TECHNICIAN"));
            jsonObjects.add(userItem);
        }

        if(wfpCount < allowedWFPs && allowedWFPs > 0){
            userItem = new JSONObject();
            userItem.put("id", Role.ROLE_WFPARTICIPANT_ID);
            userItem.put("name", getMessageSourceAccessor().getMessage("ROLE_WFPARTICIPANT"));
            jsonObjects.add(userItem);
        }

        userItem = new JSONObject();
        userItem.put("id", Role.ROLE_MEMBER_ID);
        userItem.put("name", getMessageSourceAccessor().getMessage("ROLE_MEMBER"));
        jsonObjects.add(userItem);

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", jsonObjects);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{groupId}/groupRoles/{urgId}/notificationAsst/edit")
    public String editNotificationAssistant(Model model, @PathVariable("groupId") Group group,
                                            @PathVariable("urgId") UserRoleGroup userRoleGroup) {


        NotificationAssistantForm notAssForm = new NotificationAssistantForm();
        notAssForm.setUsers(new ArrayList<User>(userRoleGroup.getNotificationUsers()));
        model.addAttribute("notificationAssistantForm", notAssForm);
        model.addAttribute("urg", userRoleGroup);
        model.addAttribute("group", group);
        
        String assignmentForLabel = getMessageSourceAccessor().getMessage("ticketList.ticketPool");
        if(userRoleGroup.getUserRole() != null){
            assignmentForLabel = userRoleGroup.getUserRole().getUser().getFirstName() + " " +
                    userRoleGroup.getUserRole().getUser().getLastName();
        }
        model.addAttribute("assignmentLabel", assignmentForLabel);

        List<User> techniciansByGroupId = userRoleGroupService.getTechnicianByGroupId(group.getId());

        if (userRoleGroup.getUserRole() != null) {
            techniciansByGroupId.remove(userRoleGroup.getUserRole().getUser());
        }

        model.addAttribute("urgUserList", techniciansByGroupId);
        return "groups/notificationAssistantForm";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{groupId}/groupRoles/{urgId}/notificationAsst")
    public String updateURGNotificationUsers(Model model, @ModelAttribute NotificationAssistantForm notAssForm,
                                             BindingResult bindingResult, @PathVariable("groupId") Group group,
                                             @PathVariable("urgId") UserRoleGroup userRoleGroup){
        if(!permissionService.hasGroupPermission("group.manageGroupSettings", group)){
            throw new AccessDeniedException("R U Denial");
        }

        model.addAttribute("notificationAssistantForm", notAssForm);
        model.addAttribute("urg", userRoleGroup);
        model.addAttribute("group", group);

        if(!bindingResult.hasErrors()){
            if(notAssForm.getUsers()!= null){
                userRoleGroup.setNotificationUsers(new HashSet<User>(notAssForm.getUsers()));
            } else {
                userRoleGroup.setNotificationUsers(new HashSet<User>());
            }
            userRoleGroupService.saveUserRoleGroup(userRoleGroup);
        }
        return "groups/notificationAssistantForm";
    }
}
