package net.grouplink.ehelpdesk.web.tags;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

public class NotAuthorized extends TagSupport {

    private Tag parent;
    private String permission;
    private Group group;

    public int doStartTag() throws JspException {
        User user = (User) pageContext.getSession().getAttribute(SessionConstants.ATTR_USER);
        PermissionService permissionService = getPermissionService();
        boolean hasPermission = permissionService.hasGroupPermission(user, permission, group);
        return !hasPermission ? Tag.EVAL_BODY_INCLUDE : Tag.SKIP_BODY;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    private PermissionService getPermissionService() {
        return (PermissionService) WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext()).getBean("permissionService");
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
