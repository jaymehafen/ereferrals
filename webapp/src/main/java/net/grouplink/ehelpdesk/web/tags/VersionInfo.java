package net.grouplink.ehelpdesk.web.tags;


import org.apache.commons.lang.StringUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: jaymehafen
 * Date: Nov 15, 2007
 * Time: 3:06:32 PM
 */
public class VersionInfo implements Tag {

    private PageContext pageContext;
    private Tag parentTag;
    private boolean buildNumber;

    public void setPageContext(PageContext pc) {
        this.pageContext = pc;
    }

    public void setParent(Tag t) {
        this.parentTag = t;
    }

    public Tag getParent() {
        return parentTag;
    }

    public int doStartTag() throws JspException {
        Properties applicationProperties = new Properties();
        InputStream is = null;
        try {
            is = getClass().getClassLoader().getResourceAsStream("application.properties");
            if (is != null)
                applicationProperties.load(is);

            if (buildNumber) {
                String buildNumber = applicationProperties.getProperty("application.buildnumber", "error");
                buildNumber = StringUtils.abbreviate(buildNumber, 13);
                pageContext.getOut().write(buildNumber);
            } else {
                String version = applicationProperties.getProperty("application.version", "error");
                pageContext.getOut().write(version);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        return Tag.SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return Tag.EVAL_PAGE;
    }

    public void release() {

    }


    public void setBuildNumber(boolean buildNumber) {
        this.buildNumber = buildNumber;
    }
}
