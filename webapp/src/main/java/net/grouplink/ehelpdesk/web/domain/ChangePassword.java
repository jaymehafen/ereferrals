package net.grouplink.ehelpdesk.web.domain;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 12/15/11
 * Time: 4:49 PM
 */
public class ChangePassword {
    private String password1;
    private String password2;

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }
}
