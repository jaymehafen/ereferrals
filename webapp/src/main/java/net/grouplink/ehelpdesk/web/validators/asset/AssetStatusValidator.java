package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetStatus;
import net.grouplink.ehelpdesk.service.asset.AssetStatusService;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * User: jaymehafen
 * Date: Sep 24, 2008
 * Time: 6:18:35 PM
 */
public class AssetStatusValidator implements Validator {
    private AssetStatusService assetStatusService;

    public boolean supports(Class clazz) {
        return clazz.equals(AssetStatus.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");
        AssetStatus as = (AssetStatus) target;
        if (StringUtils.isNotBlank(as.getName())) {
            AssetStatus a = assetStatusService.getByName(as.getName());
            if (a != null && (as.getId() == null || !as.getId().equals(a.getId()))) {
                errors.rejectValue("name", "asset.status.notUnique", "The Status Name already exists");
            }
        }
    }

    public void setAssetStatusService(AssetStatusService assetStatusService) {
        this.assetStatusService = assetStatusService;
    }
}