package net.grouplink.ehelpdesk.web.tags;

import org.apache.commons.lang.StringEscapeUtils;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: pericope
 * Date: Feb 1, 2008
 * Time: 2:46:32 PM
 */
public class CutLongString implements Tag, Serializable {

    private PageContext pageContext;
    private Tag parentTag;

    private String value;
    private String length;

    public int doStartTag() throws JspException {
//        StringBuffer sbValue = new StringBuffer(value);
//        String tValue = sbValue.toString();
        StringBuilder output = new StringBuilder();
        value = StringEscapeUtils.escapeHtml(value);
        int thresholdLength = Integer.parseInt(length);

        String[] theWords = value.split(" ");
        for (String theWord : theWords) {
            if (theWord.length() > thresholdLength) {
                // the word is too long so put a space every length chars.
                char[] theChars = theWord.toCharArray();
                for (int j = 0; j < theChars.length; j++) {
                    output.append(theChars[j]);
                    if (j != 0 && j % thresholdLength == 0) output.append(" ");
                }
            } else {
                // not too long, so leave it
                output.append(theWord).append(" ");
            }
        }

//        if (tValue.length() <= thresholdLength) {
//            output.append(tValue);
//        } else {
//            output.append("<br>");
//            while (tValue.length() > thresholdLength) {
//                output.append(tValue.substring(0,thresholdLength)+"<br>");
//                tValue = tValue.substring(thresholdLength,tValue.length());
//            }
//            if (tValue.length() > 0)
//                output.append(tValue);
//        }

        try {
            pageContext.getOut().write(output.toString());
        } catch (IOException ioe) {
            throw new JspException(ioe);
        }

        return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    public void release() {
        pageContext = null;
        parentTag = null;
    }

    public Tag getParent() {
        return parentTag;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void setPageContext(PageContext pageContext) {
        this.pageContext = pageContext;
    }

    public void setParent(Tag parentTag) {
        this.parentTag = parentTag;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

