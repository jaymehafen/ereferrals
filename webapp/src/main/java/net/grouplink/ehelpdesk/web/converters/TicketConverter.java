package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 1/5/12
 * Time: 5:39 PM
 */
public class TicketConverter implements Converter<String, Ticket> {
    @Autowired private TicketService ticketService;
    
    public Ticket convert(String id) {
        try {
            int tid = Integer.parseInt(id);
            return ticketService.getTicketById(tid);
        } catch (Exception e) {
            return null;
        }
    }
}
