package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.common.collaboration.Appointment;
import net.grouplink.ehelpdesk.common.collaboration.NameAndEmail;
import net.grouplink.ehelpdesk.common.collaboration.Session;
import net.grouplink.ehelpdesk.common.collaboration.Task;
import net.grouplink.ehelpdesk.common.utils.Constants;
import net.grouplink.ehelpdesk.common.utils.GLTest;
import net.grouplink.ehelpdesk.common.utils.MailConfig;
import net.grouplink.ehelpdesk.domain.AppointmentType;
import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRole;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.AppointmentTypeService;
import net.grouplink.ehelpdesk.service.MailService;
import net.grouplink.ehelpdesk.service.TicketHistoryService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.collaboration.CollaborationService;
import net.grouplink.ehelpdesk.web.propertyeditors.AppointmentTypeEditor;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author mrollins
 * @version 1.0
 */
public class TicketHistoryFormController2 extends AbstractEHelpDeskFormController {

    private UserService userService;
    private TicketService ticketService;
    private TicketHistoryService ticketHistoryService;
    private AppointmentTypeService appointmentTypeService;
    private CollaborationService collaborationService;
    private MailService mailService;

    public TicketHistoryFormController2() {
        super();
        setCommandClass(TicketHistory.class);
        setCommandName("ticketHistory");
        setFormView("ticketHistoryEdit2");
        setSuccessView("ticketHistoryEdit2.glml");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        // Get request parameters
        Integer thid = ServletRequestUtils.getIntParameter(request, "thid");

        TicketHistory ticketHistory = null;
        if (thid != null) {
            ticketHistory = ticketHistoryService.getTicketHistoryById(thid);
        }

        if (ticketHistory == null) {
            ticketHistory = initializeTicketHistory(request);
        }

        return ticketHistory;
    }

    private TicketHistory initializeTicketHistory(HttpServletRequest request) throws ServletRequestBindingException {
        Integer ticketId = ServletRequestUtils.getIntParameter(request, "tid");
        Integer thtype = ServletRequestUtils.getIntParameter(request, "thtype", Constants.THISTTYPE_COMMENT);
        String ticketChanges = ServletRequestUtils.getStringParameter(request, "tchanges", "");

        Ticket ticket = ticketService.getTicketById(ticketId);
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        TicketHistory th = new TicketHistory();
        th.setUser(user);
        th.setTicket(ticket);
        th.setCommentType(thtype);
        th.setNote(ticketChanges);
        th.setCreatedDate(new Date());
        th.setActive(true);
        th.setTextFormat(true);

        // Get notification defaults from mailconfig
        MailConfig mc = mailService.getMailConfig();
        Boolean checkNotifyUser = mc.getNotifyUser();
        Boolean checkNotifyTech = mc.getNotifyTechnician();

        // check user against ticket contact
        if (user.getId().equals(ticket.getContact().getId())) {
            checkNotifyUser = Boolean.FALSE;
        }

        // check user against ticket assignment
        UserRoleGroup urg = ticket.getAssignedTo();
        if (urg != null) {
            UserRole ur = urg.getUserRole();
            if (ur != null) {
                User atu = ur.getUser();
                if (atu != null) {
                    Integer atid = atu.getId();
                    if (user.getId().equals(atid)) {
                        checkNotifyTech = Boolean.FALSE;
                    }
                }
            }
        }

        th.setNotifyTech(checkNotifyTech);
        th.setNotifyUser(checkNotifyUser);

        // set ticket history type specific attributes
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, RequestContextUtils.getLocale(request));
        String now = dateFormat.format(new Date());
        switch (thtype) {
            case Constants.THISTTYPE_PHONEMADE:
                th.setSubject(now + " - " +
                        getMessageSourceAccessor().getMessage("phone.callPlaced", "Placed a phone call"));
                break;
            case Constants.THISTTYPE_PHONERECD:
                th.setSubject(now + " - " +
                        getMessageSourceAccessor().getMessage("phone.callReceived", "Received a phone call"));
                break;
            case Constants.THISTTYPE_TASK:
                th.setStartDate(new Date());
                th.setDurationAmount(1);
                th.setDurationUnit("collaboration.duration.days");
                break;
            case Constants.THISTTYPE_APPOINTMENT:
                th.setStartDate(new Date());
                th.setDurationAmount(1);
                th.setDurationUnit("collaboration.duration.hours");
                break;
            default:
                break;
        }

        return th;
    }


    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        TicketHistory ticketHistory = (TicketHistory) command;

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("apptTypes", appointmentTypeService.getApptTypes());
        model.put("justCancel", StringUtils.isBlank(ticketHistory.getNote()));

        return model;
    }


    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
                                    BindException errors) throws Exception {

        TicketHistory ticketHistory = (TicketHistory) command;
        if (ticketHistory.getCommentType() == Constants.THISTTYPE_APPOINTMENT ||
                ticketHistory.getCommentType() == Constants.THISTTYPE_TASK) {
            // Determine the start and end dates from some unbound params from the form
            String apptStartDate = ServletRequestUtils.getStringParameter(request, "apptStartDate");
            String apptStartTime = ServletRequestUtils.getStringParameter(request, "apptStartTime");

            if(StringUtils.isBlank(apptStartTime)){
                apptStartTime = "T00:00:00";
            }

            // Only process start and end date stuff if the right stuff was filled in on the form
            if (StringUtils.isNotBlank(apptStartDate) && ticketHistory.getDurationAmount() != null) {

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date start = df.parse(apptStartDate + " " + apptStartTime.replace("T", ""));
                ticketHistory.setStartDate(start);

                Calendar endcal = Calendar.getInstance();
                endcal.setTime(start);
                int durUnit = Calendar.MINUTE;
                if (ticketHistory.getDurationUnit().equals("collaboration.duration.hours")) {
                    durUnit = Calendar.HOUR;
                } else if (ticketHistory.getDurationUnit().equals("collaboration.duration.days")) {
                    durUnit = Calendar.DAY_OF_MONTH;
                }
                endcal.add(durUnit, ticketHistory.getDurationAmount());
                ticketHistory.setDueDate(endcal.getTime());
            }
        }

        //Adding attachments
        @SuppressWarnings("unchecked")
        List<Object> attchList = ticketHistory.getAttchmnt();
        Set<Attachment> attchments = new HashSet<Attachment>();
        if (ticketHistory.getAttachments() != null) {
            attchments.addAll(ticketHistory.getAttachments());
        }

        for (Object o : attchList) {
            MultipartFile mpf = (MultipartFile) o;
            if (mpf == null) continue;
            if (mpf.getSize() != 0) {
                Attachment attch = new Attachment();
                attch.setContentType(mpf.getContentType());
                attch.setFileData(mpf.getBytes());
                attch.setFileName(mpf.getOriginalFilename());
                attchments.add(attch);
            }
        }

        ticketHistory.setAttachments(attchments);
        //End of attachments

        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        // Check to see if ticketHistory has cc recipient.  If so, add annotation.
        String cc = ticketHistory.getCc();
        if ((cc != null) && (!cc.trim().equals(""))) {
            String note = ticketHistory.getNoteText();
            note += "\r\n\r\n\t[cc: " + cc + "]";
            ticketHistory.setNote(note);
        }

        //Saving ticket history
        ticketHistoryService.saveTicketHistory(ticketHistory, user);

        //Sending email notification
        ticketHistoryService.sendEmailNotification(ticketHistory);

        if (ticketHistory.getCommentType() == Constants.THISTTYPE_APPOINTMENT ||
                ticketHistory.getCommentType() == Constants.THISTTYPE_TASK) {
            String encPwd = (String)request.getSession().getAttribute(SessionConstants.ATTR_USER_PWD);
            Session collSession = collaborationService.getCollaborationSession(user, GLTest.decryptPassword(encPwd));
//            Session collSession = (Session) request.getSession().getAttribute(SessionConstants.COLLABORATION_SESSION);
            sendCalendarItem(user, ticketHistory, collSession);
        }

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("successfulSave", true);
        model.put("timerStatus", ServletRequestUtils.getStringParameter(request, "ts"));
        return showForm(request, response, errors, model);
    }

    /**
     * Sends an appointment or task to the users' collaboration calendar.
     *
     * @param user          the user
     * @param ticketHistory - Object containing appointment and task information
     * @param session       the session
     */
    private void sendCalendarItem(User user, TicketHistory ticketHistory, Session session) {
        if (session != null) {
            // send appointments and tasks to the collaborating calendar system
            switch (ticketHistory.getCommentType()) {
                case Constants.THISTTYPE_APPOINTMENT: {
                    Appointment appt = new Appointment();

                    // Message is alwasy html
                    appt.setMessage(ticketHistory.getNote());
                    appt.setContentType("text/html");
                    appt.setSubject(ticketHistory.getSubject());
                    appt.setPlace(ticketHistory.getPlace());

                    // attachments
                    Set<Attachment> attachments = ticketHistory.getAttachments();
                    if (attachments != null) {
                        for (Attachment attachment : attachments) {
                            appt.addAttachment(attachment);
                        }
                    }

                    // recipients
                    String recipients = ticketHistory.getRecipients();
                    if (StringUtils.isNotBlank(recipients)) {
                        String[] recipientList = recipients.split(";");
                        for (String aRecipientList : recipientList) {
                            appt.addRecipient(null, aRecipientList);
                        }
                        appt.addRecipient(null, user.getEmail());
                    }

                    // from
                    appt.setFrom(new NameAndEmail(null, user.getEmail()));

                    // Start and end dates
                    if (ticketHistory.getStartDate() != null) {
                        Calendar startDate = Calendar.getInstance();
                        startDate.setTime(ticketHistory.getStartDate());
                        appt.setStartDate(startDate);
                    }

                    if (ticketHistory.getDueDate() != null) {
                        Calendar endDate = Calendar.getInstance();
                        endDate.setTime(ticketHistory.getDueDate());
                        appt.setEndDate(endDate);
                    }

                    collaborationService.scheduleCalendarItem(appt, session);
                    break;
                }
                case Constants.THISTTYPE_TASK: {
                    Task task = new Task();
                    // Message is alwasy html
                    task.setMessage(ticketHistory.getNote());
                    task.setContentType("text/html");
                    task.setSubject(ticketHistory.getSubject());

                    // attachments
                    Set<Attachment> attachments = ticketHistory.getAttachments();
                    if (attachments != null) {
                        for (Attachment attachment : attachments) {
                            task.addAttachment(attachment);
                        }
                    }

                    // recipients
                    String recipients = ticketHistory.getRecipients();
                    if (StringUtils.isNotBlank(recipients)) {
                        String[] recipientList = recipients.split(";");
                        for (String aRecipientList : recipientList) {
                            task.addRecipient(null, aRecipientList);
                        }
                        // assigned tasks do not need to go in my mailbox
                        //task.addRecipient(null, user.getEmail());
                    }

                    // from
                    task.setFrom(new NameAndEmail(null, user.getEmail()));

                    // Start and end dates
                    if (ticketHistory.getStartDate() != null) {
                        Calendar startDate = Calendar.getInstance();
                        startDate.setTime(ticketHistory.getStartDate());
                        task.setStartDate(startDate);
                    }

                    if (ticketHistory.getDueDate() != null) {
                        Calendar dueDate = Calendar.getInstance();
                        dueDate.setTime(ticketHistory.getDueDate());
                        task.setDueDate(dueDate);
                    }

                    // assigned as of right now
                    task.setAssignedDate(Calendar.getInstance());

                    task.setPriority(ticketHistory.getTaskPriority());

                    collaborationService.scheduleCalendarItem(task, session);
                    break;
                }
                default:
                    break;
            }
        }
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p>This method allows the bean instance to perform initialization only
     * possible when all bean properties have been set and to throw an
     * exception in the event of misconfiguration.
     *
     * @throws Exception in the event of misconfiguration (such
     *                   as failure to set an essential property) or if initialization fails.
     */
    public void afterPropertiesSet() throws Exception {
        if (ticketService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketService");
        if (userService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: userService");
        if (ticketHistoryService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: ticketHistoryService");
        if (appointmentTypeService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: appointmentTypeService");
        if (collaborationService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: collaborationService");
        if (mailService == null)
            throw new BeanCreationException(getClass().getName() + " Property not set: mailService");
    }

    /**
     * Initialize the given binder instance, for example with custom editors.
     * Called by {@link #createBinder}.
     * <p>This method allows you to register custom editors for certain fields of your
     * command class. For instance, you will be able to transform Date objects into a
     * String pattern and back, in order to allow your JavaBeans to have Date properties
     * and still be able to set and display them in an HTML interface.
     * <p>The default implementation is empty.
     *
     * @param request current HTTP request
     * @param binder  the new binder instance
     * @throws Exception in case of invalid state or arguments
     * @see #createBinder
     * @see org.springframework.validation.DataBinder#registerCustomEditor
     * @see org.springframework.beans.propertyeditors.CustomDateEditor
     */
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(AppointmentType.class, new AppointmentTypeEditor(appointmentTypeService));
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setTicketHistoryService(TicketHistoryService ticketHistoryService) {
        this.ticketHistoryService = ticketHistoryService;
    }

    public void setAppointmentTypeService(AppointmentTypeService appointmentTypeService) {
        this.appointmentTypeService = appointmentTypeService;
    }

    public void setCollaborationService(CollaborationService collaborationService) {
        this.collaborationService = collaborationService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
}