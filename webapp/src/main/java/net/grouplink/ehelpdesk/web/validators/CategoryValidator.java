package net.grouplink.ehelpdesk.web.validators;


import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.service.CategoryService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CategoryValidator implements Validator {
    @Autowired
	private CategoryService categoryService;

    public boolean supports(Class clazz) {
		return clazz.equals(Category.class);
	}

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "ticketSettings.category.required.name", "Category name required");
        Category category = (Category) target;
        Category cat = categoryService.getCategoryByNameAndGroupId(category.getName(), category.getGroup().getId());
        if (cat != null) {
            if (category.getId() != null) {
                if (!category.getId().equals(cat.getId())){
                    errors.rejectValue("name", "ticketSettings.category.validate.notuniqueid", "Category name already exists");
                }
            } else {
                errors.rejectValue("name", "ticketSettings.category.validate.notuniqueid", "Category name already exists");
            }
        }

    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
}
