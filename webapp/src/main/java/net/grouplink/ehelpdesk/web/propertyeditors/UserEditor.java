package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;


public class UserEditor extends PropertyEditorSupport {

    private UserService userService;

    /**
     * Constructor for use by derived PropertyEditor classes.
     */
    public UserEditor() {
    }

    public UserEditor(UserService userService) {
        this.userService = userService;
    }

    /**
     * Sets the property value by parsing a given String.  May raise
     * java.lang.IllegalArgumentException if either the String is
     * badly formatted or if this kind of property can't be expressed
     * as text.
     *
     * @param text The string to be parsed.
     */
    public void setAsText(String text) throws IllegalArgumentException {
        if (StringUtils.isNotBlank(text)){
            setValue(userService.getUserById(Integer.valueOf(text)));
        } else {
            setValue(null);
        }
    }


    /**
     * Gets the property value as a string suitable for presentation
     * to a human to edit.
     *
     * @return The property value as a string suitable for presentation
     *         to a human to edit.
     *         <p>   Returns "null" is the value can't be expressed as a string.
     *         <p>   If a non-null value is returned, then the PropertyEditor should
     *         be prepared to parse that string back in setAsText().
     */
    public String getAsText() {
        User user = (User)getValue();
        return (user !=  null)?user.getId().toString():"";
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
