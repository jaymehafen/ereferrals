package net.grouplink.ehelpdesk.web.propertyeditors;

import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.service.ReportService;
import org.apache.commons.lang.StringUtils;

import java.beans.PropertyEditorSupport;

/**
 * Date: Mar 19, 2010
 * Time: 3:22:22 PM
 */
public class ReportEditor extends PropertyEditorSupport {
    private ReportService reportService;

    public ReportEditor(ReportService reportService){
        this.reportService = reportService;
    }

    @Override
    public String getAsText() {
        Report report = (Report) getValue();
        return (report == null)?"":report.getId().toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if(StringUtils.isNotBlank(text)){
            setValue(reportService.getById(Integer.valueOf(text)));
        }
    }
}
