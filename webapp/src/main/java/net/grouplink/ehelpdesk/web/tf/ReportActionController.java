package net.grouplink.ehelpdesk.web.tf;

import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.constants.JasperReportsConstants;
import net.grouplink.ehelpdesk.service.ReportService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Date: Mar 17, 2010
 * Time: 4:13:27 PM
 */
public class ReportActionController extends MultiActionController {

    private Log log = LogFactory.getLog(ReportActionController.class);

    private UserService userService;
    private ReportService reportService;

    public ModelAndView reportList(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;

        Map<String, Object> model = new HashMap<String, Object>();

        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());

        model.put("userReports", reportService.getByUser(user));
        model.put("publicReports", reportService.getPublicMinusUser(user));

        return new ModelAndView("reportList", model);
    }

    public ModelAndView reportDelete(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Integer reportId = ServletRequestUtils.getIntParameter(request, "reportId");
        Report report = reportService.getById(reportId);

        if (report != null) {
            // Check if the report is in use
            if (reportService.getUsageCount(report) > 0) {
                // can't delete
                request.setAttribute("flash.info", getMessageSourceAccessor().getMessage("report.cannotDelete"));
            } else {
                reportService.delete(report);
                reportService.flush();
                request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("report.successDelete"));
            }
        }

        return new ModelAndView(new RedirectView("/tf/reportList.glml", true));
    }

    public ModelAndView reportView(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert response != null;
        HttpSession session = request.getSession(true);

        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());
        Integer reportId = ServletRequestUtils.getIntParameter(request, "reportId");
        Report report = reportService.getById(reportId);
        String value = ServletRequestUtils.getStringParameter(request, "value", "");
        Integer level = JasperReportsConstants.FIRST_DRILL_DOWN_LEVEL;
        Integer dd = 0;

        if(StringUtils.isBlank(value)) {
            session.setAttribute("level", level);
            session.setAttribute("dd", dd);
        } else {
            level = (Integer)session.getAttribute("level");
            level++;
            session.setAttribute("level", level);
            if(level == JasperReportsConstants.SECOND_DRILL_DOWN_LEVEL) {
                if (!report.getShowReportByDate()) {
                    dd = Integer.valueOf(value);
                    session.setAttribute("dd", dd);
                }
            } else {
                dd = (Integer)session.getAttribute("dd");
            }
        }
        int byDate = ServletRequestUtils.getIntParameter(request, "byDate", (report.getByDate() != null) ? report.getByDate() : -1);

        JasperPrint jasperPrint;
        if (report.getShowReportByDate()) {
            jasperPrint = reportService.generateReportByDate(report, user, value, byDate);
        } else {
            jasperPrint = reportService.generateReport(report, user, value, level, dd);
        }

        if (report.getFormat().equals(Report.FORMAT_HTML)) {
            response.setContentType("text/html");
            request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
            JRHtmlExporter htmlExporter = new JRHtmlExporter();
            htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            htmlExporter.setParameter(JRExporterParameter.OUTPUT_WRITER, response.getWriter());
            htmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath() + "/images/report?image=");
            htmlExporter.exportReport();
        } else if (report.getFormat().equals(Report.FORMAT_PDF)) {
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline; filename=tickets.pdf");
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
            pdfExporter.exportReport();
        } else if (report.getFormat().equals(Report.FORMAT_XLS)) {
            response.setContentType("application/xls");
            response.setHeader("Content-Disposition", "inline; filename=tickets.xls");
            JExcelApiExporter xlsExporter = new JExcelApiExporter();
            xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
            xlsExporter.exportReport();
        } else if (report.getFormat().equals(Report.FORMAT_CSV)) {
            response.setContentType("text/csv");
            response.setHeader("Content-Disposition", "inline; filename=tickets.csv");
            JRCsvExporter csvExporter = new JRCsvExporter();
            csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            csvExporter.setParameter(JRExporterParameter.OUTPUT_WRITER, response.getWriter());
            csvExporter.exportReport();
        } else {
            log.error("unknown report format: " + report.getFormat());
            throw new RuntimeException("unknown report format: " + report.getFormat());
        }

        return null;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }
}
