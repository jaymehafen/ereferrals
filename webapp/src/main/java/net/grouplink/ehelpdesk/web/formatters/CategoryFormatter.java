package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.service.CategoryService;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class CategoryFormatter implements Formatter<Category> {
    @Autowired
    private CategoryService categoryService;

    public Category parse(String id, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(id)) {
            return categoryService.getCategoryById(Integer.parseInt(id));
        } else {
            return null;
        }
    }

    public String print(Category category, Locale locale) {
        return category == null ? "" : category.getId().toString();
    }
}
