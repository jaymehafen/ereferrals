package net.grouplink.ehelpdesk.web.interceptors;

import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.OpenSessionInViewInterceptor;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;

public class Zen10ConditionalOpenSessionInViewInterceptor extends OpenSessionInViewInterceptor {
    private BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) {
        super.setBeanFactory(beanFactory);
        this.beanFactory = beanFactory;
    }

    @Override
    public void afterPropertiesSet() {
        // look up bean zen10SessionFactory
        SessionFactory sf = null;

        try {
            sf = (SessionFactory) beanFactory.getBean("zen10SessionFactory");
        } catch (BeansException e) {
            logger.debug("zen10SessionFactory is not defined");
        }
        
        if (sf != null) {
            setSessionFactory(sf);
        }
    }

    @Override
    public void preHandle(WebRequest request) throws DataAccessException {
        if (getSessionFactory() != null) {
            super.preHandle(request);
        } else {
            logger.debug("sessionFactory is null");
        }
    }

    @Override
    public void postHandle(WebRequest request, ModelMap model) throws DataAccessException {
        if (getSessionFactory() != null) {
            super.postHandle(request, model);
        } else {
            logger.debug("sessionFactory is null");
        }
    }

    @Override
    public void afterCompletion(WebRequest request, Exception ex) throws DataAccessException {
        if (getSessionFactory() != null) {
            super.afterCompletion(request, ex);
        } else {
            logger.debug("sessionFactory is null");
        }
    }
}
