package net.grouplink.ehelpdesk.web.security;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.common.utils.UserUtils;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.service.UserDetailsImpl;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.domain.ConfigMenuOptions;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class AuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private Log log = LogFactory.getLog(getClass());

    private UserService userService;
    private GroupService groupService;
    private PermissionService permissionService;
    private PropertiesService propertiesService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = getLoggedInUser();

        log.info("Authentication success for user " + user.getLoginId());

        ConfigMenuOptions menuOptions = getConfigMenuOptions(user);
        session.setAttribute(SessionConstants.CONFIG_MENU_OPTIONS, menuOptions);

        boolean showSettingsButton = getDisplaySettingsButton(user, menuOptions);
        session.setAttribute(SessionConstants.SHOW_SETTINGS_BUTTON, showSettingsButton);

        // Get and set the timeout on the session
        // Check to see if there is an entry in the database (they overrode the XML)
        String to = propertiesService.getPropertiesValueByName(PropertiesConstants.SESSION_TIMEOUT_LENGTH);
        if (StringUtils.isNotBlank(to)) {
            try {
                Integer timeout = new Integer(to);
                session.setMaxInactiveInterval(timeout * 60);
            } catch (NumberFormatException nfe) {
                // Do nothing
            }
        }

        super.onAuthenticationSuccess(request, response, authentication);
    }

    private User getLoggedInUser() {
        User user = null;
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext != null) {
            Authentication authentication = securityContext.getAuthentication();
            if (authentication != null) {
                if (authentication.getPrincipal() instanceof UserDetails) {
                    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
                    user = userDetails.getUser();
                    user = userService.getUserById(user.getId());
                }
            }
        }
        return user;
    }

    private boolean getDisplaySettingsButton(User user, ConfigMenuOptions configMenuOptions) {
        // if any configmenuoptions are true, return true
        if (configMenuOptions.getAssignments() || configMenuOptions.getCustomFields() || configMenuOptions.getGroups()
                || configMenuOptions.getKbManage() || configMenuOptions.getScheduler()) {
            return true;
        }

        // check if user has any of the remaining config menu permissions
        String[] configPermissions = new String[]{"global.userManagement", "global.surveyManagement",
                "global.systemConfig"};
        for (String permission : configPermissions) {
            if (permissionService.hasGlobalPermission(user, permission)) {
                return true;
            }
        }

        return false;
    }

    private ConfigMenuOptions getConfigMenuOptions(User user) {
        ConfigMenuOptions menuOptions = new ConfigMenuOptions();

        List<Group> groupList = groupService.getGroups();
        // custom fields
        for (Group group : groupList) {
            if (permissionService.hasGroupPermission(user, "group.customFieldManagement", group)) {
                menuOptions.setCustomFields(true);
                break;
            }
        }

        // groups
        if (permissionService.hasGlobalPermission(user, "global.systemConfig")) {
            menuOptions.setGroups(true);
        } else {
            for (Group group : groupList) {
                if (permissionService.hasGroupPermission(user, "group.manageGroupSettings", group)) {
                    menuOptions.setGroups(true);
                    break;
                }
            }
        }

        // scheduler
        for (Group group : groupList) {
            if (permissionService.hasGroupPermission(user, "group.scheduledTaskManagement", group)) {
                menuOptions.setScheduler(true);
                break;
            }
        }

        // kb manage
        for (Group group : groupList) {
            if (permissionService.hasGroupPermission(user, "group.kbManage", group)) {
                menuOptions.setKbManage(true);
                break;
            }
        }

        // assignments
        for (Group group : groupList) {
            if (permissionService.hasGroupPermission(user, "group.assignmentManagement", group)) {
                menuOptions.setAssignments(true);
                break;
            }
        }

        return menuOptions;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }
}
