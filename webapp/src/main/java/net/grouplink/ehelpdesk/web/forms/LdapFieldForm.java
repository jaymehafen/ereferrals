package net.grouplink.ehelpdesk.web.forms;

import net.grouplink.ehelpdesk.domain.LdapField;
import org.springframework.util.AutoPopulatingList;

/**
 * author: zpearce
 * Date: 12/20/11
 * Time: 10:35 AM
 */
public class LdapFieldForm {
    private AutoPopulatingList<LdapField> ldapFields = new AutoPopulatingList<LdapField>(LdapField.class);

    public AutoPopulatingList<LdapField> getLdapFields() {
        return ldapFields;
    }

    public void setLdapFields(AutoPopulatingList<LdapField> ldapFields) {
        this.ldapFields = ldapFields;
    }

    public LdapField getLdapFieldById(int id) {
        LdapField returnVal = null;
        for(LdapField field : getLdapFields()) {
            if(field.getId() == id) {
                returnVal = field;
                break;
            }
        }
        return returnVal;
    }
}