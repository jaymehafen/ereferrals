package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 11/19/11
 * Time: 8:42 AM
 */
public class UserRoleGroupConverter implements Converter<String, UserRoleGroup>{
    @Autowired
    private UserRoleGroupService userRoleGroupService;

    public UserRoleGroup convert(String id){
        try{
            int urgId = Integer.parseInt(id);
            return userRoleGroupService.getUserRoleGroupById(urgId);
        } catch (Exception e){
            return null;
        }
    }
}
