package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.forms.StatusForm;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * author: zpearce
 * Date: 11/23/11
 * Time: 11:34 AM
 */
@Controller
@RequestMapping(value = "statuses")
public class StatusController extends ApplicationObjectSupport {
    @Autowired private StatusService statusService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        StatusForm statusForm = new StatusForm();
        AutoPopulatingList<Status> statuses = new AutoPopulatingList<Status>(Status.class);
        statuses.addAll(statusService.getAllStatuses());
        statusForm.setStatuses(statuses);
        model.addAttribute("statusForm", statusForm);
        return "statuses/index";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute("statusForm") StatusForm statusForm,
                         BindingResult bindingResult,
                         @RequestParam(value = "idsToDelete", required = false) Integer[] idsToDelete,
                         HttpServletRequest request) {
        if(bindingResult.hasErrors()) {
            return "statuses/index";
        }
        for(int id : idsToDelete) {
            Status status = statusForm.getStatusById(id);
            statusService.deleteStatus(status);
            statusForm.getStatuses().remove(status);
        }
        for(Status status : statusForm.getStatuses()) {
            statusService.saveStatus(status);
        }
        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("statusSetup.saved"));
        return "redirect:/config/statuses";
    }
}
