package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.service.MyTicketTabService;
import net.grouplink.ehelpdesk.service.TicketFilterService;
import net.grouplink.ehelpdesk.web.domain.MyTicketsConfig;
import net.grouplink.ehelpdesk.web.propertyeditors.TicketFilterEditor;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTicketsConfigController extends SimpleFormController {
    private MyTicketTabService myTicketTabService;
    private TicketFilterService ticketFilterService;

    public MyTicketsConfigController() {
        setFormView("mytickets-config");
        setCommandClass(MyTicketsConfig.class);
        setCommandName("myTicketsConfig");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        MyTicketsConfig mtc = new MyTicketsConfig();
        User user = (User) request.getSession(false).getAttribute(SessionConstants.ATTR_USER);
        List<MyTicketTab> myTicketTabs = myTicketTabService.getByUser(user);
        if (CollectionUtils.isEmpty(myTicketTabs)) {
            myTicketTabs = myTicketTabService.initializeTabs(user);
        }
        mtc.setMyTicketTabs(myTicketTabs);
        return mtc;
    }
    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        MyTicketsConfig mtc = (MyTicketsConfig) command;
        Integer maxOrder = 0;
        for (MyTicketTab tab : mtc.getMyTicketTabs()) {
            if (tab.getOrder() != null && tab.getOrder() > maxOrder)
                maxOrder = tab.getOrder();
        }

        User user = (User) request.getSession(false).getAttribute(SessionConstants.ATTR_USER);
        List<TicketFilter> myFilters = ticketFilterService.getByUser(user);
        List<TicketFilter> pubFilters = ticketFilterService.getPublicMinusUser(user);

        model.put("maxOrder", maxOrder);
        model.put("myFilters", myFilters);
        model.put("pubFilters", pubFilters);

        return model;
    }

    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);

        binder.registerCustomEditor(TicketFilter.class, new TicketFilterEditor(ticketFilterService));
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        MyTicketsConfig mtc = (MyTicketsConfig) command;
        User user = (User) request.getSession(false).getAttribute(SessionConstants.ATTR_USER);
        for (MyTicketTab myTicketTab : mtc.getMyTicketTabs()) {
            myTicketTab.setUser(user);
        }

        myTicketTabService.saveAll(mtc.getMyTicketTabs());

        String submitType = ServletRequestUtils.getStringParameter(request, "submitType");
        if (StringUtils.equals(submitType, "apply")) {
            return new ModelAndView(new RedirectView("/mytickets-config.glml", true));
        }

        return new ModelAndView(new RedirectView("/mytickets.glml", true));
    }

    public void setMyTicketTabService(MyTicketTabService myTicketTabService) {
        this.myTicketTabService = myTicketTabService;
    }

    public void setTicketFilterService(TicketFilterService ticketFilterService) {
        this.ticketFilterService = ticketFilterService;
    }
}
