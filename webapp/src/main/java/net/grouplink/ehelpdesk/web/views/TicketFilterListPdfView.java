package net.grouplink.ehelpdesk.web.views;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomFieldValues;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.TicketHistory;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterCriteriaHelper;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilterOperators;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Date: Mar 30, 2010
 * Time: 5:27:18 PM
 */
public class TicketFilterListPdfView extends AbstractPdfView {

    private static final Font TITLE_FONT_ITALIC = new Font(Font.HELVETICA, 18, Font.ITALIC, new Color(153, 153, 153));
    private static final Font TITLE_FONT_BOLD = new Font(Font.HELVETICA, 18, Font.BOLD, new Color(204, 51, 0));
    private static final Font HEADING_FONT = new Font(Font.HELVETICA, 12, Font.ITALIC, Color.black);
    private static final Font HEADING_DATA_FONT = new Font(Font.HELVETICA, 12, Font.ITALIC, Color.blue);
    private static final Font DATA_HEAD_FONT = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.black);

    private static final int MARGIN = 32;

    private final Log log = LogFactory.getLog(TicketFilterListPdfView.class);
    private final SimpleDateFormat filterDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private UserRoleGroupService userRoleGroupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private UserService userService;
    private GroupService groupService;
    private LocationService locationService;
    private TicketPriorityService ticketPriorityService;
    private StatusService statusService;
    private ZenAssetService zenAssetService;
    private TicketService ticketService;
    private AssetService assetService;


    @Override
    protected void buildPdfMetadata(Map model, Document document, HttpServletRequest request) {
        document.addTitle(getMessageSourceAccessor().getMessage("ticketList.title", "Ticket List"));
        document.addCreator(getMessageSourceAccessor().getMessage("global.systemName", "everything HelpDesk\u2122"));
        document.addCreationDate();
    }

    @SuppressWarnings("unchecked")
    protected void buildPdfDocument(Map model, Document document, PdfWriter writer, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        TicketFilter ticketFilter = (TicketFilter) model.get("ticketFilter");
        List<Ticket> ticketList = (List<Ticket>) model.get("ticketList");
        Locale locale = RequestContextUtils.getLocale(request);

        // Create and add the event handler.
        // This is to ensure that only entire cells are printed at end of pages
        MyPageEvents events = new MyPageEvents(getMessageSourceAccessor());
        writer.setPageEvent(events);
        events.onOpenDocument(writer, document);

        Paragraph title = new Paragraph();
        title.add(new Chunk("everything ", TITLE_FONT_ITALIC));
        title.add(new Chunk("HelpDesk\u2122", TITLE_FONT_BOLD));
        document.add(title);
        document.add(new Paragraph(" "));

        DateFormat dateTimeFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);

        // Ticket List Summary table
        buildSummaryTable(document, ticketFilter, ticketList.size(), dateTimeFormat);
        // TicketFilter table
        buildFilterTable(document, ticketFilter, dateTimeFormat);
        // Ticket List table
        buildTicketsTable(document, ticketFilter, ticketList, dateTimeFormat, dateFormat);
    }

    private void buildSummaryTable(Document document, TicketFilter ticketFilter, Integer ticketCount, DateFormat df)
            throws DocumentException {

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(50);
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setPadding(4);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.summary"), HEADING_FONT));
        cell.setColspan(2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setGrayFill(0.7f);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.dateExported"), HEADING_FONT));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(df.format(new Date()), HEADING_DATA_FONT));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.nbTickets"), HEADING_FONT));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(String.valueOf(ticketCount), HEADING_DATA_FONT));
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketFilter.title"), HEADING_FONT));
        table.addCell(cell);
        cell = new PdfPCell(new Phrase(ticketFilter.getName(), HEADING_DATA_FONT));
        table.addCell(cell);

        document.add(table);

        document.add(new Paragraph(" "));
    }

    private void buildFilterTable(Document document, TicketFilter tf, DateFormat df) throws Exception {
        PdfPTable table = new PdfPTable(2);
        PdfPCell cell;

        table.setWidthPercentage(100);
        table.setWidths(new float[]{30f, 70f});
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setPadding(4);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);


        cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage("ticketList.filters"), HEADING_FONT));
        cell.setColspan(2);
        cell.setGrayFill(0.75f);
        table.addCell(cell);
        table.setHeaderRows(1);

        // Assigned To
        if (!ArrayUtils.isEmpty(tf.getAssignedToIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_ASSIGNED)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getAssignedToIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getAssignedToIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getAssignedToIds()[i];
                UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                if (urg.getUserRole() == null) {
                    sb.append(opMessage).append(" - ").append(urg.getGroup().getName());
                } else {
                    sb.append(opMessage).append(" - ").append(urg.getUserRole().getUser().getFirstName()).append(" ")
                            .append(urg.getUserRole().getUser().getLastName());
                }
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Category
        if (!ArrayUtils.isEmpty(tf.getCategoryIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CATEGORY)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCategoryIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getCategoryIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getCategoryIds()[i];
                Category cat = categoryService.getCategoryById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(cat.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Category Option
        if (!ArrayUtils.isEmpty(tf.getCategoryOptionIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CATOPT)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getCategoryOptionIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getCategoryOptionIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getCategoryOptionIds()[i];
                CategoryOption catOpt = categoryOptionService.getCategoryOptionById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(catOpt.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Contact
        if (!ArrayUtils.isEmpty(tf.getContactIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CONTACT)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getContactIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getContactIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getContactIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Created Date
        if (tf.getCreatedDateOperator() != null && !tf.getCreatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_CREATED)));
            table.addCell(cell);
            String s = getDateFilterDescription(df, tf.getCreatedDateOperator(), tf.getCreatedDate(), tf.getCreatedDate2(), tf.getCreatedDateLastX());
            cell = new PdfPCell(new Phrase(s));
            table.addCell(cell);
        }

        // Estimated Date
        if (tf.getEstimatedDateOperator() != null && !tf.getEstimatedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_ESTIMATED)));
            table.addCell(cell);
            String s = getDateFilterDescription(df, tf.getEstimatedDateOperator(), tf.getEstimatedDate(),
                    tf.getEstimatedDate2(), tf.getEstimatedDateLastX());
            cell = new PdfPCell(new Phrase(s));
            table.addCell(cell);
        }

        // Group
        if (!ArrayUtils.isEmpty(tf.getGroupIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_GROUP)));
            table.addCell(cell);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getGroupIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getGroupIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getGroupIds()[i];
                Group g = groupService.getGroupById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(g.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Location
        if (!ArrayUtils.isEmpty(tf.getLocationIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_LOCATION)));
            table.addCell(cell);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getLocationIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getLocationIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getLocationIds()[i];
                Location loc = locationService.getLocationById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(loc.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Modified Date
        if (tf.getModifiedDateOperator() != null && !tf.getModifiedDateOperator().equals(TicketFilterOperators.DATE_ANY)) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_MODIFIED)));
            table.addCell(cell);
            String s = getDateFilterDescription(df, tf.getModifiedDateOperator(), tf.getModifiedDate(),
                    tf.getModifiedDate2(), tf.getModifiedDateLastX());
            cell = new PdfPCell(new Phrase(s));
            table.addCell(cell);
        }

        // Note
        if (!ArrayUtils.isEmpty(tf.getNote())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_NOTE)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getNote().length; i++) {
                // Get associated operator
                Integer operator = tf.getNoteOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                String s = tf.getNote()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(s);
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        //  Priority
        if (!ArrayUtils.isEmpty(tf.getPriorityIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_PRIORITY)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getPriorityIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getPriorityIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getPriorityIds()[i];
                TicketPriority p = ticketPriorityService.getPriorityById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(p.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Status
        if (!ArrayUtils.isEmpty(tf.getStatusIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_STATUS)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getStatusIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getStatusIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getStatusIds()[i];
                Status status = statusService.getStatusById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(status.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Subject
        if (!ArrayUtils.isEmpty(tf.getSubject())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_SUBJECT)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getSubject().length; i++) {
                // Get associated operator
                Integer operator = tf.getSubjectOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                String s = tf.getSubject()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(s);
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Submitted By
        if (!ArrayUtils.isEmpty(tf.getSubmittedByIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_SUBMITTED)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getSubmittedByIds().length; i++) {
                // Get associated operator
                Integer operator = tf.getSubmittedByIdsOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getSubmittedByIds()[i];
                User u = userService.getUserById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(u.getFirstName()).append(" ").append(u.getLastName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Ticket Number
        if (!ArrayUtils.isEmpty(tf.getTicketNumbers())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_TICKETNUMBER)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getTicketNumbers().length; i++) {
                // Get associated operator
                Integer operator = tf.getTicketNumbersOps()[i];
                String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                Integer integer = tf.getTicketNumbers()[i];
                if (sb.length() > 0) sb.append(", ");
                sb.append(opMessage).append(" - ").append(integer);
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Work Time
        if (StringUtils.isNotBlank(tf.getWorkTimeOperator())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_WORKTIME)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            if (tf.getWorkTimeOperator().equals("more")) {
                sb.append(getMessageSourceAccessor().getMessage("ticketSearch.worktime.more")).append(" ");
            } else if (tf.getWorkTimeOperator().equals("less")) {
                sb.append(getMessageSourceAccessor().getMessage("ticketSearch.worktime.less")).append(" ");
            } else {
                sb.append("unknown work time operator! ");
            }

            if (StringUtils.isNotBlank(tf.getWorkTimeHours())) {
                sb.append(tf.getWorkTimeHours()).append(" ")
                        .append(getMessageSourceAccessor().getMessage("scheduler.time.hour")).append(" ");
            }

            if (StringUtils.isNotBlank(tf.getWorkTimeMinutes())) {
                sb.append(tf.getWorkTimeMinutes()).append(" ")
                        .append(getMessageSourceAccessor().getMessage("scheduler.time.mins"));
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // zenAssetIds
        if (!ArrayUtils.isEmpty(tf.getZenAssetIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_ZENWORKS10ASSET)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getZenAssetIds().length; i++) {
                Integer integer = tf.getZenAssetIds()[i];
                ZenAsset za = zenAssetService.getById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(za.getAssetName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // assetIds
        if (!ArrayUtils.isEmpty(tf.getInternalAssetIds())) {
            cell = new PdfPCell(new Phrase(getMessageSourceAccessor().getMessage(TicketFilter.COLUMN_INTERNALASSET)));
            table.addCell(cell);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tf.getInternalAssetIds().length; i++) {
                Integer integer = tf.getInternalAssetIds()[i];
                Asset asset = assetService.getAssetById(integer);
                if (sb.length() > 0) sb.append(", ");
                sb.append(asset.getName());
            }

            cell = new PdfPCell(new Phrase(sb.toString()));
            table.addCell(cell);
        }

        // Custom Fields
        if (MapUtils.isNotEmpty(tf.getCustomFields())) {
            for (Map.Entry<String, List<TicketFilterCriteriaHelper>> cfMapEntry : tf.getCustomFields().entrySet()) {
                cell = new PdfPCell(new Phrase(cfMapEntry.getKey()));
                table.addCell(cell);

                StringBuilder sb = new StringBuilder();
                for (TicketFilterCriteriaHelper ticketFilterCFHelper : cfMapEntry.getValue()) {
                    // Get associated operator
                    Integer operator = ticketFilterCFHelper.getOperatorId();
                    String opMsgCode = TicketFilterOperators.getMessageCode(operator);
                    String opMessage = getMessageSourceAccessor().getMessage(opMsgCode, "Equals");

                    if (sb.length() > 0) sb.append(", ");
                    sb.append(opMessage).append(" - ").append(ticketFilterCFHelper.getValue());
                }
                cell = new PdfPCell(new Phrase(sb.toString()));
                table.addCell(cell);
            }
        }

        document.add(table);
        document.add(new Paragraph(" "));
    }

    private void buildTicketsTable(Document document, TicketFilter tf, List<Ticket> ticketList,
                                   DateFormat dateTimeFormat, DateFormat dateFormat)
            throws DocumentException {

        String[] colHeads = getColumnHeadings(tf);
        int indexOfColNote = indexOfColumn(tf.getColumnOrder(), TicketFilter.COLUMN_NOTE);
        int indexOfColHistoryNote = indexOfColumn(tf.getColumnOrder(), TicketFilter.COLUMN_HISTORYNOTE);
        int tableLength = indexOfColNote >= 0 ? colHeads.length - 1 : colHeads.length;
        tableLength = indexOfColHistoryNote >= 0 ? tableLength - 1 : tableLength;

        PdfPTable table = new PdfPTable(tableLength);
        table.setWidthPercentage(100);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setBorderColor(Color.black);
        table.getDefaultCell().setGrayFill(0.75f);
        table.getDefaultCell().setPadding(3);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        // header row
        for (int i = 0; i < colHeads.length; i++) {
            String colHead = colHeads[i];
            if (i != indexOfColNote && i != indexOfColHistoryNote) {
                table.addCell(new Phrase(colHead, DATA_HEAD_FONT));
            }
        }
       
        table.setHeaderRows(1);
        table.getDefaultCell().setBorderWidth(1);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

        String[] columns = tf.getColumnOrder();

        // Iterate on the tickets list
        PdfPCell cell;
        PdfPCell noteCell;
        boolean even = false;

        for (Ticket ticket : ticketList) {
            table.getDefaultCell().setColspan(1);
            table.getDefaultCell().setPaddingLeft(3);
            table.getDefaultCell().setGrayFill(even ? 0.90f : 1.00f);

            boolean showNote = false;
            boolean showHistoryNote = false;

            for (String column : columns) {
                cell = null;
                if (column.equals(TicketFilter.COLUMN_PRIORITY)) {
                    cell = new PdfPCell(new Phrase(ticket.getPriority().getName()));
                } else if (column.equals(TicketFilter.COLUMN_TICKETNUMBER)) {
                    cell = new PdfPCell(new Phrase(ticket.getTicketId().toString()));
                } else if (column.equals(TicketFilter.COLUMN_LOCATION)) {
                    cell = new PdfPCell(new Phrase(ticket.getLocation().getName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBJECT)) {
                    cell = new PdfPCell(new Phrase(ticket.getSubject()));
                } else if (column.equals(TicketFilter.COLUMN_NOTE)) {
                    showNote = true;
                } else if (column.equals(TicketFilter.COLUMN_CATOPT)) {
                    cell = new PdfPCell(new Phrase(ticket.getCategoryOption() == null ? "" :
                            ticket.getCategoryOption().getName()));
                } else if (column.equals(TicketFilter.COLUMN_ASSIGNED)) {
                    UserRoleGroup assignedToUrg = ticket.getAssignedTo();
                    cell = new PdfPCell(new Phrase(assignedToUrg.getUserRole() == null ?
                            assignedToUrg.getGroup().getName() : assignedToUrg.getUserRole().getUser().getFirstName() +
                            " " + ticket.getAssignedTo().getUserRole().getUser().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_STATUS)) {
                    cell = new PdfPCell(new Phrase(ticket.getStatus().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CREATED)) {
                    cell = new PdfPCell(new Phrase(dateTimeFormat.format(ticket.getCreatedDate())));
                } else if (column.equals(TicketFilter.COLUMN_MODIFIED)) {
                    cell = new PdfPCell(new Phrase(dateTimeFormat.format(ticket.getModifiedDate())));
                } else if (column.equals(TicketFilter.COLUMN_ESTIMATED)) {
                    cell = new PdfPCell(new Phrase(ticket.getEstimatedDate() == null ?
                            "" : dateFormat.format(ticket.getEstimatedDate())));
                } else if (column.equals(TicketFilter.COLUMN_CONTACT)) {
                    cell = new PdfPCell(new Phrase(ticket.getContact().getFirstName() + " " +
                            ticket.getContact().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_SUBMITTED)) {
                    cell = new PdfPCell(new Phrase(ticket.getSubmittedBy().getFirstName() + " " +
                            ticket.getSubmittedBy().getLastName()));
                } else if (column.equals(TicketFilter.COLUMN_GROUP)) {
                    cell = new PdfPCell(new Phrase(ticket.getGroup().getName()));
                } else if (column.equals(TicketFilter.COLUMN_CATEGORY)) {
                    cell = new PdfPCell(new Phrase(ticket.getCategory() == null ? "" : ticket.getCategory().getName()));
                } else if (column.equals(TicketFilter.COLUMN_WORKTIME)) {
                    cell = new PdfPCell(new Phrase(ticket.getWorkTime() == null ? "" : ticket.getDisplayWorkTime()));
                } else if (column.equals(TicketFilter.COLUMN_ZENWORKS10ASSET)) {
                    cell = new PdfPCell(new Phrase(ticket.getZenAsset() == null ? "" : ticket.getZenAsset().getAssetName()));
                } else if (column.equals(TicketFilter.COLUMN_INTERNALASSET)) {
                    StringBuilder sb = new StringBuilder();
                    if (ticket.getAsset() != null) {
                        if (ticket.getAsset().getAssetNumber() != null) sb.append(ticket.getAsset().getAssetNumber());
                        if (ticket.getAsset().getName() != null) sb.append(" ").append(ticket.getAsset().getName());
                    } else {
                        sb.append("");
                    }

                    cell = new PdfPCell(new Phrase(sb.toString()));
                } else if (column.equals(TicketFilter.COLUMN_HISTORYSUBJECT)) {
                    TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                    if (ticketHistory == null) {
                        cell = new PdfPCell(new Phrase(""));
                    } else {
                        cell = new PdfPCell(new Phrase(ticketHistory.getSubject() == null ? "" : ticketHistory.getSubject()));
                    }
                } else if (column.equals(TicketFilter.COLUMN_HISTORYNOTE)) {
                    showHistoryNote = true;
                } else if (column.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                    // column is a custom field
                    // check the ticket for a custom field value name that matches the column name
                    CustomFieldValues cfVal = ticketService.getCustomFieldValue(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_CUSTOMFIELDPREFIX));
                    if (cfVal == null) {
                        cell = new PdfPCell(new Phrase(""));
                    } else {
                        cell = new PdfPCell(new Phrase(cfVal.getFieldValue() == null ? "" : cfVal.getFieldValue()));
                    }
                } else if (column.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                    SurveyData surveyData = ticketService.getSurveyData(ticket, StringUtils.removeStart(column, TicketFilter.COLUMN_SURVEYPREFIX));
                    if (surveyData == null) {
                        cell = new PdfPCell(new Phrase(""));
                    } else {
                        cell = new PdfPCell(new Phrase(surveyData.getValue() == null ? "" : surveyData.getValue()));
                    }
                } else {
                    log.error("unknown column value: " + column);
                }

                if (cell != null) {
                    cell.setGrayFill(even ? 0.9f : 1.0f);
                    table.addCell(cell);
                }
            }

            if (showNote) {
                String noteLabel = getMessageSourceAccessor().getMessage("ticket.note") + ": ";
                String note = noteLabel + (StringUtils.isBlank(ticket.getNote()) ? "" : ticket.getNote());
                noteCell = new PdfPCell(new Phrase(note));
                noteCell.setColspan(columns.length - 1);
                noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                noteCell.setGrayFill(even ? 0.9f : 1.0f);
                table.addCell(noteCell);
            }

            if (showHistoryNote) {
                String noteLabel = getMessageSourceAccessor().getMessage("ticketFilter.historyNote") + ": ";
                TicketHistory ticketHistory = ticketService.getMostRecentTicketHistory(ticket);
                String note;
                if (ticketHistory != null) {
                    note = noteLabel + (StringUtils.isBlank(ticketHistory.getNoteText()) ? "" : ticketHistory.getNoteText());
                } else {
                    note = noteLabel; 
                }
                
                noteCell = new PdfPCell(new Phrase(note));
                noteCell.setColspan(columns.length - 1);
                noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                noteCell.setGrayFill(even ? 0.9f : 1.0f);
                table.addCell(noteCell);
            }

            even = !even;
        }
        
        document.add(table);
    }

    private String[] getColumnHeadings(TicketFilter ticketFilter) {
        String[] columns = ticketFilter.getColumnOrder();
        String[] colHeads = new String[columns.length];
        for (int i = 0; i < columns.length; i++) {
            String column = columns[i];
            if (column.startsWith(TicketFilter.COLUMN_CUSTOMFIELDPREFIX)) {
                colHeads[i] = StringUtils.removeStart(column, TicketFilter.COLUMN_CUSTOMFIELDPREFIX);
            } else if (column.startsWith(TicketFilter.COLUMN_SURVEYPREFIX)) {
                colHeads[i] = StringUtils.removeStart(column,  TicketFilter.COLUMN_SURVEYPREFIX);
            } else {
                colHeads[i] = getMessageSourceAccessor().getMessage(column);
            }
        }
        return colHeads;
    }

    private int indexOfColumn(String[] colHeads, String colName) {
        int indexOfCol = -1;
        for (int i = 0; i < colHeads.length; i++) {
            if (colHeads[i].equals(colName)) {
                indexOfCol = i;
                break;
            }
        }
        return indexOfCol;
    }

    private String getDateFilterDescription(DateFormat df, Integer dateOperator, String date1, String date2,
                                            Integer lastX) throws Exception {
        StringBuilder sb = new StringBuilder();
        if (dateOperator.equals(TicketFilterOperators.DATE_TODAY)) {
            sb.append(getMessageSourceAccessor().getMessage("date.today"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_WEEK)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thisweek"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_MONTH)) {
            sb.append(getMessageSourceAccessor().getMessage("date.thismonth"));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_ON)) {
            Date d1 = filterDateFormat.parse(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.on")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_BEFORE)) {
            Date d1 = filterDateFormat.parse(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.before")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_AFTER)) {
            Date d1 = filterDateFormat.parse(date1);
            sb.append(getMessageSourceAccessor().getMessage("date.after")).append(" ").append(df.format(d1));
        } else if (dateOperator.equals(TicketFilterOperators.DATE_RANGE)) {
            Date d1 = filterDateFormat.parse(date1);
            Date d2 = filterDateFormat.parse(date2);
            sb.append(getMessageSourceAccessor().getMessage("date.range")).append(" ").append(df.format(d1))
                    .append(" - ").append(df.format(d2));
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_HOURS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.hours")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_DAYS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.days")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_WEEKS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.weeks")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_MONTHS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.months")).append(" ").append(lastX);
        } else if(dateOperator.equals(TicketFilterOperators.DATE_LAST_X_YEARS)){
            sb.append(getMessageSourceAccessor().getMessage("date.lastx.years")).append(" ").append(lastX);
        }

        return sb.toString();
    }


    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    protected Document newDocument() {
        return new Document(PageSize.A4.rotate());
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    private static class MyPageEvents extends PdfPageEventHelper {

        private MessageSourceAccessor messageSourceAccessor;

        // This is the PdfContentByte object of the writer
        private PdfContentByte cb;

        // We will put the final number of pages in a template
        private PdfTemplate template;

        // This is the BaseFont we are going to use for the header / footer
        private BaseFont bf = null;

        public MyPageEvents(MessageSourceAccessor messageSourceAccessor) {
            this.messageSourceAccessor = messageSourceAccessor;
        }

        // we override the onOpenDocument method
        public void onOpenDocument(PdfWriter writer, Document document) {
            try {
                bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb = writer.getDirectContent();
                template = cb.createTemplate(50, 50);
            } catch (DocumentException de) {
                de.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        // we override the onEndPage method
        public void onEndPage(PdfWriter writer, Document document) {
            int pageN = writer.getPageNumber();
            String text = messageSourceAccessor.getMessage("page", "page") + " " + pageN + " " +
                    messageSourceAccessor.getMessage("of", "of") + " ";

//            String text = messageSourceAccessor.getMessage("pagedList.pageNumOfNum", new Object[]{""+pageN });

            float len = bf.getWidthPoint(text, 8);
            cb.beginText();
            cb.setFontAndSize(bf, 8);

            cb.setTextMatrix(MARGIN, 16);
            cb.showText(text);
            cb.endText();

            cb.addTemplate(template, MARGIN + len, 16);
            cb.beginText();
            cb.setFontAndSize(bf, 8);

            cb.endText();
        }

        // we override the onCloseDocument method
        public void onCloseDocument(PdfWriter writer, Document document) {
            template.beginText();
            template.setFontAndSize(bf, 8);
            template.showText(String.valueOf(writer.getPageNumber() - 1));
            template.endText();
        }
    }
}
