package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.Location;
import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.domain.TicketTemplate;
import net.grouplink.ehelpdesk.domain.TicketTemplateCustomFieldValue;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.ZenAsset;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketTemplateService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.ZenAssetService;
import net.grouplink.ehelpdesk.web.propertyeditors.AssetEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.CategoryOptionEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.GroupEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.LocationEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.StatusEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.TicketPriorityEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.UserRoleGroupEditor;
import net.grouplink.ehelpdesk.web.propertyeditors.ZenAssetEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TicketTemplateFormController extends SimpleFormController{
    private TicketTemplateService ticketTemplateService;
    private TicketPriorityService ticketPriorityService;
    private StatusService statusService;
    private LocationService locationService;
    private UserService userService;
    private GroupService groupService;
    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private UserRoleGroupService userRoleGroupService;
    private AssetService assetService;
    private ZenAssetService zenAssetService;
    private CustomFieldsService customFieldsService;

    public TicketTemplateFormController() {
        super();
        setCommandClass(TicketTemplate.class);
        setCommandName("template");
        setFormView("ticketTemplates/ticketTemplateEdit");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        Integer ttId = ServletRequestUtils.getIntParameter(request, "ttId");
        return ticketTemplateService.getById(ttId);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        TicketTemplate ticketTemplate = (TicketTemplate) command;

        // Manually process the custom fields
        Map<String, String[]> reqParams = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : reqParams.entrySet()) {
            String paramName = entry.getKey();
            String[] paramVals = entry.getValue();

            if(paramName.startsWith("cf_") || paramName.startsWith("_cf_")) {
                Integer cfId;
                if (paramName.startsWith("_cf_")) {
                    // this is a param for an unselected checkbox.
                    // check if the request contains a param for the selected checkbox
                    if (reqParams.containsKey(paramName.substring(1))) {
                        // checkbox is selected, skip this param
                        continue;
                    }
                    cfId = Integer.parseInt(paramName.split("_")[2]);
                } else {
                    cfId = Integer.parseInt(paramName.split("_")[1]);
                }

                CustomField cf = customFieldsService.getCustomFieldById(cfId);
                TicketTemplateCustomFieldValue cfv = null;
                for (TicketTemplateCustomFieldValue cfvFound : ticketTemplate.getCustomeFieldValues()) {
                    if (cfvFound.getCustomField().getId().equals(cf.getId())) {
                        cfv = cfvFound;
                        break;
                    }
                }

                if (cfv == null) {
                    cfv = new TicketTemplateCustomFieldValue();
                }

                String cfValue;
                if (cf.getFieldType().equals(CustomField.VALUETYPE_CHECKBOX)) {
                    cfValue = String.valueOf(ServletRequestUtils.getBooleanParameter(request, paramName, false));
                } else {
                    cfValue = paramVals[0];
                }

                cfv.setCustomField(cf);
                cfv.setTicketTemplate(ticketTemplate);
                cfv.setFieldValue(cfValue);
                ticketTemplate.getCustomeFieldValues().add(cfv);
            }
        }

        // Convert multipart files to Attachment objects
        List<Object> attchmnts = ticketTemplate.getAttchmnt();
        for (Object o : attchmnts) {
            MultipartFile attchmnt = (MultipartFile) o;
            if (attchmnt == null) continue;

            if (attchmnt.getSize() != 0) {
                Attachment attachment = new Attachment();
                attachment.setContentType(attchmnt.getContentType());
                attachment.setFileData(attchmnt.getBytes());
                attachment.setFileName(attchmnt.getOriginalFilename());
                ticketTemplate.getAttachments().add(attachment);
            }
        }

        ticketTemplateService.save(ticketTemplate);

        // This form submission is designed to be called asynchronously because it is
        // from a jsp fragment in a contentpane that should not be refreshed after a success
        // so return a success message and return null (so the page doesn't refresh)

        // message has to be wrapped in an html document
        // see dojo docs: http://livedocs.dojotoolkit.org/dojo/io/iframe
        StringBuilder message = new StringBuilder()
                .append("<html><body><textarea>")
                .append(getMessageSourceAccessor().getMessage("global.saveSuccess"))
                .append("</textarea></body></html>");

        response.getWriter().print(message.toString());
        return null;
    }

    @Override
    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map<String, Object> refData = new HashMap<String, Object>();

        refData.put("locations", locationService.getLocations());
        refData.put("priorities", ticketPriorityService.getPriorities());
        refData.put("ticketStatus", statusService.getStatus());

        return refData;
    }

    @Override
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);

        binder.registerCustomEditor(User.class, new UserEditor(userService));
        binder.registerCustomEditor(TicketPriority.class, new TicketPriorityEditor(ticketPriorityService));
        binder.registerCustomEditor(Location.class, new LocationEditor(locationService));
        binder.registerCustomEditor(Group.class, new GroupEditor(groupService));
        binder.registerCustomEditor(Category.class, new CategoryEditor(categoryService));
        binder.registerCustomEditor(CategoryOption.class, new CategoryOptionEditor(categoryOptionService));
        binder.registerCustomEditor(Status.class, new StatusEditor(statusService));
        binder.registerCustomEditor(UserRoleGroup.class, new UserRoleGroupEditor(userRoleGroupService));
        binder.registerCustomEditor(Asset.class, new AssetEditor(assetService));
        binder.registerCustomEditor(ZenAsset.class, new ZenAssetEditor(zenAssetService));
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setTicketPriorityService(TicketPriorityService ticketPriorityService) {
        this.ticketPriorityService = ticketPriorityService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setZenAssetService(ZenAssetService zenAssetService) {
        this.zenAssetService = zenAssetService;
    }

    public void setCustomFieldsService(CustomFieldsService customFieldsService) {
        this.customFieldsService = customFieldsService;
    }
}
