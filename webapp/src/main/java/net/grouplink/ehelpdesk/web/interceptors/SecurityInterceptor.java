package net.grouplink.ehelpdesk.web.interceptors;

import net.grouplink.ehelpdesk.common.utils.Constants;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LicenseService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.web.controllers.LogoutController;
import net.grouplink.ehelpdesk.web.utils.AdminLicenseIssue;
import net.grouplink.ehelpdesk.web.utils.ApplicationConstants;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 * @author mrollins
 * @version 1.0
 */
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    private GroupService groupService;
    private LicenseService licenseService;
    private UserRoleGroupService userRoleGroupService;

    /**
     * Check that a user is logged in.
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // allow static resources
        if (handler instanceof ResourceHttpRequestHandler) return true;

        // let the logout controller through otherwise admin can't logout after updating license file
        if (handler instanceof LogoutController) return true;

        String contextPath = request.getContextPath();

        // let the licensing page through since this interceptor redirects there and we don't
        // want an infinite redirect, do we?
        if (request.getRequestURI().startsWith(contextPath + "/config/licensing/")) return true;

        // allow error pages
        if (request.getRequestURI().startsWith(contextPath + "/errors/")) return true;

        request.getSession().setAttribute("theme", "DEFAULT");

        // Get handle to the servlet context
        ServletContext servletContext = request.getSession().getServletContext();
        boolean licenseExists = getBoolFromContext(ApplicationConstants.LICENSE_EXISTS, servletContext, false);
        boolean teksExceeded = getBoolFromContext(ApplicationConstants.TEKS_EXCEEDED, servletContext, true);
        boolean wfPartsExceeded = getBoolFromContext(ApplicationConstants.WFPARTICIPANTS_EXCEEDED, servletContext, true);
        boolean globalBomb = getBoolFromContext(ApplicationConstants.GLOBAL_BOMB, servletContext, true);
        boolean upgradeDateBomb = getBoolFromContext(ApplicationConstants.UPGRADE_DATE_BOMB, servletContext, false);
        int groupCount = groupService.getGroups().size();

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        ArrayList<AdminLicenseIssue> issues = new ArrayList<AdminLicenseIssue>();
        String licensingUrl = contextPath + "/config/licensing/licensing.glml";
        if (!licenseExists) {
            if (user != null && User.ADMIN_LOGIN.equalsIgnoreCase(user.getLoginId())) {
                response.sendRedirect(licensingUrl);
                return false;
            } else {
                throw new LicenseException("This product is not licensed. Contact your system administrator");
            }
        } else if (globalBomb) {
            if (user != null && User.ADMIN_LOGIN.equalsIgnoreCase(user.getLoginId())) {
                response.sendRedirect(licensingUrl);
                return false;
            } else {
                throw new LicenseException("Your timed trial is over. Please request a new license");
            }
        } else if (upgradeDateBomb) {
            if (user != null && User.ADMIN_LOGIN.equalsIgnoreCase(user.getLoginId())) {
                response.sendRedirect(licensingUrl);
                return false;
            } else {
                throw new LicenseException("Your upgrade protection is expired and will not work with this version of eHelpDesk");
            }
        } else {
            if (teksExceeded) {
                if (user != null && User.ADMIN_LOGIN.equalsIgnoreCase(user.getLoginId())) {
                    issues.add(new AdminLicenseIssue(Constants.TECHS_EXCEEDED));
                } else {
                    throw new LicenseException("Number of technicians exceeds number allowed by license.");
                }
            }

            if(wfPartsExceeded){
                if(user != null && User.ADMIN_LOGIN.equalsIgnoreCase(user.getLoginId())){
                    issues.add(new AdminLicenseIssue((Constants.WFPARTS_EXCEEDED)));
                } else {
                    throw new LicenseException("Number of workflow participants exceeds number allowed by license.");
                }
            }

            if (groupCount > licenseService.getGroupCount()) {
                if (user != null && User.ADMIN_LOGIN.equalsIgnoreCase(user.getLoginId())) {
                    issues.add(new AdminLicenseIssue(Constants.STANDARD_GROUPS_EXCEEDED));
                } else {
                    throw new LicenseException("Number of groups exceeds number allowed in license");
                }
            }
        }

        session.setAttribute("licenseIssue", issues);
        return true;
    }

    private boolean getBoolFromContext(String attr, ServletContext servletContext, boolean def) {
        Object obj = servletContext.getAttribute(attr);
        if (obj == null) return def;
        return (Boolean) obj;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setLicenseService(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService){
        this.userRoleGroupService = userRoleGroupService;
    }
}
