package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.ScheduleStatus;
import net.grouplink.ehelpdesk.service.ScheduleStatusService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class ScheduleStatusFormatter implements Formatter<ScheduleStatus> {
    @Autowired
    private ScheduleStatusService scheduleStatusService;

    public ScheduleStatus parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return scheduleStatusService.getScheduleStatusById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(ScheduleStatus object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
