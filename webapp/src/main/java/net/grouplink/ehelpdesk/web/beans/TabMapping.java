package net.grouplink.ehelpdesk.web.beans;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import java.util.*;

/**
 * author: zpearce
 * Date: 8/11/11
 * Time: 11:54 AM
 */
public class TabMapping extends WebApplicationObjectSupport {
    private PathMatcher pathMatcher = new AntPathMatcher();

    private final Map<String, String> tabMap = new HashMap<String, String>();

    public String getCurrentTab(String url) {
        // exact match?
        String tabName = tabMap.get(url);
        if(tabName != null) {
            return tabName;
        }
        List<String> matchingPatterns = new ArrayList<String>();
        for(String pattern : tabMap.keySet()) {
            if(pathMatcher.match(pattern, url)) {
                matchingPatterns.add(pattern);
            }
        }
        String bestPatternMatch = null;
        Comparator<String> patternComparator = pathMatcher.getPatternComparator(url);
        if(!matchingPatterns.isEmpty()) {
            Collections.sort(matchingPatterns, patternComparator);
            bestPatternMatch = matchingPatterns.get(0);
        }
        return tabMap.get(bestPatternMatch);
    }

    public void setTabMap(Map<String, String> tabMap) {
        this.tabMap.putAll(tabMap);
    }
}
