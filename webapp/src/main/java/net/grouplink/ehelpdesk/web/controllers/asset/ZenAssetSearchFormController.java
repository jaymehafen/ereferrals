package net.grouplink.ehelpdesk.web.controllers.asset;

import net.grouplink.ehelpdesk.common.utils.PropertiesConstants;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.zen.UserPrimaryDeviceInformation;
import net.grouplink.ehelpdesk.domain.zen.Workstation;
import net.grouplink.ehelpdesk.domain.zen.ZenAssetSearch;
import net.grouplink.ehelpdesk.service.AssetTrackerZen10Service;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.PropertiesService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.zen.DeviceService;
import net.grouplink.ehelpdesk.service.zen.ReferenceService;
import net.grouplink.ehelpdesk.service.zen.UserPrimaryDeviceInformationService;
import net.grouplink.ehelpdesk.service.zen.WorkstationService;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: jaymehafen
 * Date: Mar 31, 2009
 * Time: 6:31:08 PM
 */
public class ZenAssetSearchFormController extends SimpleFormController {

    // injected properties
    private AssetTrackerZen10Service assetTrackerZen10Service;
    private PropertiesService propertiesService;
    private LdapService ldapService;

    // looked up dynamically
    private WorkstationService workstationService;
    private DeviceService deviceService;
    private UserService userService;
    private ReferenceService referenceService;
    private UserPrimaryDeviceInformationService userPrimaryDeviceInformationService;

    public ZenAssetSearchFormController() {
        setFormView("searchZen10Asset");
        setCommandClass(ZenAssetSearch.class);
        setCommandName("zenAssetSearch");
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        return new ZenAssetSearch();
    }

    @Override
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        // determine which button was pressed
        String searchButton = ServletRequestUtils.getStringParameter(request, "searchButton");
        // if search button wasn't pressed, then it's either the initial form load or a reset
        if (StringUtils.isBlank(searchButton)) {
            boolean ldapEnabled = propertiesService.getBoolValueByName(PropertiesConstants.LDAP_INTEGRATION, false);
            logger.debug("ldap enabled: " + ldapEnabled);
            if (ldapEnabled) {
                String contactUserId = ServletRequestUtils.getStringParameter(request, "contactUserId");
                logger.debug("contactUserId: " + contactUserId);
                if (StringUtils.isNotBlank(contactUserId)) {
                    User contactUser = userService.getUserById(Integer.valueOf(contactUserId));
                    logger.debug("contactUser.ldapDN: " + contactUser.getLdapDN());
                    if (StringUtils.isNotBlank(contactUser.getLdapDN())) {
                        byte[] ldapGuid = ldapService.getGuidForUserDn(contactUser.getLdapDN());
                        logger.debug("ldap guid == null:" + (ldapGuid == null));
                        if (ldapGuid != null) {
                            String guidString = String.valueOf(Hex.encodeHex(ldapGuid));
                            logger.debug("ldap guid string: " + guidString);

                            // get list of workstations that contact is the primary user of
                            List<Workstation> primaryUserWorkstations = getWorkstationService().getByPrimaryUser(guidString);
                            model.put("primaryUserWorkstations", primaryUserWorkstations);

                            // get list of workstations that contact has logged into
                            byte[] zuid = getReferenceService().getZuidForObjectUid(guidString);
                            if (zuid != null) {
                                List<UserPrimaryDeviceInformation> userPrimaryDeviceInformations = getUserPrimaryDeviceInformationService().getByZuid(zuid);
                                Collections.sort(userPrimaryDeviceInformations, new Comparator<UserPrimaryDeviceInformation>() {
                                    public int compare(UserPrimaryDeviceInformation o1, UserPrimaryDeviceInformation o2) {
                                        return o2.getLoginCount().compareTo(o1.getLoginCount());
                                    }
                                });

                                List primaryDevices = getWorkstationService().getByUserPrimaryDeviceInformation(userPrimaryDeviceInformations);
                                model.put("recentWorkstations", primaryDevices);
                            }
                        }
                    }
                }
            }

            model.put("workstations", getWorkstationService().getAll());
        }

        model.put("zenBaseUrl", assetTrackerZen10Service.getZen10BaseUrl());
        return model;
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        ZenAssetSearch zas = (ZenAssetSearch) command;
        // determine which button was pressed
        String searchButton = ServletRequestUtils.getStringParameter(request, "searchButton");
        Map<String, Object> model = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(searchButton)) {
            List<Workstation> searchResults = workstationService.search(zas);
            model.put("searchResults", searchResults);
        }

        return showForm(request, response, errors, model);
//        return new ModelAndView(getFormView(), model);
    }

    private WorkstationService getWorkstationService() {
        if (workstationService == null) {
            WorkstationService ws = (WorkstationService) getApplicationContext().getBean("workstationService");
            setWorkstationService(ws);
        }

        return workstationService;
    }

    public void setWorkstationService(WorkstationService workstationService) {
        this.workstationService = workstationService;
    }

    public DeviceService getDeviceService() {
        if (deviceService == null) {
            DeviceService ds = (DeviceService) getApplicationContext().getBean("deviceService");
            setDeviceService(ds);
        }

        return deviceService;
    }

    public void setDeviceService(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    public ReferenceService getReferenceService() {
        if (referenceService == null) {
            referenceService = (ReferenceService) getApplicationContext().getBean("referenceService");
        }

        return referenceService;
    }

    public UserPrimaryDeviceInformationService getUserPrimaryDeviceInformationService() {
        if (userPrimaryDeviceInformationService == null) {
            userPrimaryDeviceInformationService = (UserPrimaryDeviceInformationService) getApplicationContext().getBean("userPrimaryDeviceInformationService");
        }

        return userPrimaryDeviceInformationService;
    }

    public void setAssetTrackerZen10Service(AssetTrackerZen10Service assetTrackerZen10Service) {
        this.assetTrackerZen10Service = assetTrackerZen10Service;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setPropertiesService(PropertiesService propertiesService) {
        this.propertiesService = propertiesService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
