package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * User: jaymehafen
 * Date: Sep 24, 2008
 * Time: 6:18:35 PM
 */
public class SoftwareLicenseValidator extends BaseValidator {
    public boolean supports(Class clazz) {
        return clazz.equals(SoftwareLicense.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "productName", "global.fieldRequired", "This field cannot be empty");
    }
}