package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.domain.CustomField;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.CustomFieldValuesService;
import net.grouplink.ehelpdesk.service.CustomFieldsService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LocationService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 3/30/12
 * Time: 2:24 PM
 */

@Controller
@RequestMapping(value = "/customFields")
public class CustomFieldController extends ApplicationObjectSupport {
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private CustomFieldsService customFieldsService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private CustomFieldValuesService customFieldValuesService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        // Get the customfields for the groups
        User user = userService.getLoggedInUser();
        List<Group> groups = groupService.getGroups();

        // If user does not have global permissions for custom fields, then limit the list of custom fields to the group
        // level custom field permissions
        boolean hasGlobalCFManagement = permissionService.hasGlobalPermission(user, "global.customFieldManagement");
        if (!hasGlobalCFManagement) {
            for (Iterator<Group> iterator = groups.iterator(); iterator.hasNext(); ) {
                Group group = iterator.next();
                if (!permissionService.hasGroupPermission(user, "group.customFieldManagement", group)) {
                    iterator.remove();
                }
            }
        }

        List<CustomField> customFields = new ArrayList<CustomField>();

        if (hasGlobalCFManagement) {
            // load all global custom fields
            customFields.addAll(customFieldsService.getGlobalCustomFields());

            // load customfields that only have the location set.
            customFields.addAll(customFieldsService.getByLocation());
        }

        // load any that are specific to the groups this user is allowed to access (filtered above)
        for (Group group : groups) {
            customFields.addAll(customFieldsService.getCustomFieldsByGroup(group));
        }

        Collections.sort(customFields);
        model.addAttribute("customFields", customFields);

        return "customFields/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String newCustomField(Model model) {
        model.addAttribute("customField", new CustomField());
        model.addAllAttributes(referenceData());
        return "customFields/form";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createCustomField(Model model, @Valid @ModelAttribute CustomField customField, BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            customFieldsService.saveCustomField(customField);
        }
        model.addAllAttributes(referenceData());
        return "customFields/form";
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}/edit")
    public String edit(Model model, @PathVariable("id") CustomField customField){
        model.addAttribute("customField", customField);
        model.addAllAttributes(referenceData());
        return "customFields/form";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{customField}")
    public String update(Model model, @Valid @ModelAttribute("customField") CustomField customField, BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            customFieldsService.saveCustomField(customField);
        }
        model.addAllAttributes(referenceData());
        return "customFields/form";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public String delete(@PathVariable("id") CustomField customField, HttpServletRequest request){

        if(customFieldValuesService.customFieldHasValues(customField)){
            customField.setActive(false);
            customFieldsService.saveCustomField(customField);
            request.setAttribute("flash.info", getMessageSourceAccessor().getMessage("customField.inuse"));
        } else {
            customFieldsService.deleteCustomField(customField);
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("customField.deleteResult0"));
        }

        return "customFields/index";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteMultiple")
    public String deleteMany(@RequestParam(value = "cfListCbx") Integer[] cfIds, HttpServletRequest request, HttpSession session){
        User user = (User)session.getAttribute(SessionConstants.ATTR_USER);

        List<CustomField> cantDel = new ArrayList<CustomField>();
        for (int cfid : cfIds) {
            CustomField customField = customFieldsService.getCustomFieldById(cfid);

            // check permissions
            // if user has global custom fields permissions allow, otherwise check group permission
            if (!hasCustomFieldManagementPermission(user, customField)) {
                throw new AccessDeniedException("Access is denied");
            }

            if (customFieldValuesService.customFieldHasValues(customField)) {
                cantDel.add(customField);
                customField.setActive(false);
                customFieldsService.saveCustomField(customField);
            } else {
                customFieldsService.deleteCustomField(customField);
            }
        }

        StringBuilder cfNames = new StringBuilder();
        for (int i = 0; i < cantDel.size(); i++) {
            CustomField customField = cantDel.get(i);
            cfNames.append(customField.getName());
            if (i + 1 < cantDel.size()) cfNames.append(",");
        }

        StringBuilder retVal = new StringBuilder(getMessageSourceAccessor().getMessage("customField.deleteResult1",
                new Object[]{cfIds.length - cantDel.size(), cfIds.length}));

        if (cantDel.size() > 0) {
            retVal.append(getMessageSourceAccessor().getMessage("customField.deleteResult2",
                    new Object[]{cfNames.toString()}));
        }

        if(cantDel.size() > 0) {
            request.setAttribute("flash.info", retVal);

        } else {
            request.setAttribute("flash.success", retVal);
        }
        return "customFields/index";
    }

    private boolean hasCustomFieldManagementPermission(User user, CustomField customField) {
        if (permissionService.hasGlobalPermission("global.customFieldManagement")) {
            return true;
        }

        if (isGlobalCustomField(customField)) {
            // custom field is global, but user doesn't have global custom field perm
            return false;
        }

        // custom field is not global, get the group
        Group group = null;
        if (customField.getCategoryOption() != null) {
            group = customField.getCategoryOption().getCategory().getGroup();
        } else if (customField.getCategory() != null) {
            group = customField.getCategory().getGroup();
        } else if (customField.getGroup() != null) {
            group = customField.getGroup();
        }

        // check group permission
        return permissionService.hasGroupPermission("group.customFieldManagement", group);

    }

    private boolean isGlobalCustomField(CustomField customField) {
        return customField.getCategoryOption() == null &&
                customField.getCategory() == null &&
                customField.getGroup() == null;
    }

    private Map<String, Object> referenceData(){
        Map<String, Object> model = new HashMap<String, Object>();

        model.put("locations", locationService.getLocations());

        Map<String, String> customFieldTypes = new LinkedHashMap<String, String>();
        customFieldTypes.put(CustomField.VALUETYPE_TEXT, getMessageSourceAccessor().getMessage("customField.textField"));
        customFieldTypes.put(CustomField.VALUETYPE_TEXTAREA, getMessageSourceAccessor().getMessage("customField.textArea"));
        customFieldTypes.put(CustomField.VALUETYPE_RADIO, getMessageSourceAccessor().getMessage("customField.radio"));
        customFieldTypes.put(CustomField.VALUETYPE_CHECKBOX, getMessageSourceAccessor().getMessage("customField.checkbox"));
        customFieldTypes.put(CustomField.VALUETYPE_SELECT, getMessageSourceAccessor().getMessage("customField.dropDown"));
        customFieldTypes.put(CustomField.VALUETYPE_DATE, getMessageSourceAccessor().getMessage("customField.date"));
        customFieldTypes.put(CustomField.VALUETYPE_NUMBER, getMessageSourceAccessor().getMessage("customField.number"));
        customFieldTypes.put(CustomField.VALUETYPE_DATETIME, getMessageSourceAccessor().getMessage("customField.datetime"));
        model.put("customFieldTypes", customFieldTypes);

        return model;
    }
}
