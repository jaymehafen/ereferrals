package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.acl.Permission;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class PermissionFormatter implements Formatter<Permission> {
    @Autowired
    private PermissionService permissionService;

    public Permission parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return permissionService.getPermissionById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Permission object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
