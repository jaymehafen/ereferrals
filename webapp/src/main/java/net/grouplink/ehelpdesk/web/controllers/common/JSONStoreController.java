package net.grouplink.ehelpdesk.web.controllers.common;

import net.grouplink.ehelpdesk.domain.*;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.SoftwareLicense;
import net.grouplink.ehelpdesk.service.*;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.service.asset.SoftwareLicenseService;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.util.*;


/**
 * Spring MultiActionController methods for return JSON styled data stores. Each ModelAndView get*Store method has
 * zero or more private utility methods for constructing minor JSON items for the store's major JSON object.
 * <p/>
 * User: mark
 * Date: Sep 23, 2008
 * Time: 1:36:15 PM
 */
public class JSONStoreController extends MultiActionController {

    private CategoryService categoryService;
    private CategoryOptionService categoryOptionService;
    private SoftwareLicenseService softwareLicenseService;
    private UserService userService;
    private UserRoleGroupService userRoleGroupService;
    private TicketTemplateService ticketTemplateService;
    private TemplateMasterService templateMasterService;
    private LocationService locationService;
    private TicketPriorityService priorityService;
    private GroupService groupService;
    private AssignmentService assignmentService;
    private StatusService statusService;
    private CustomFieldsService customFieldsService;
    private TicketService ticketService;
    private KnowledgeBaseService knowledgeBaseService;
    private PermissionService permissionService;
    private AssetService assetService;

    public ModelAndView getSingleJSONUser(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Locale locale = RequestContextUtils.getLocale(request);

        Integer contactId = ServletRequestUtils.getIntParameter(request, "contactId");
        User user = userService.getUserById(contactId);

        JSONObject jsonUser = getJSONUserItem(user, locale);

        response.setContentType("application/json");
        response.getWriter().print(jsonUser.toString());
        return null;
    }


    public ModelAndView getTicketTemplateTreeStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Integer ticketMasterId = ServletRequestUtils.getIntParameter(request, "tmId");
        TicketTemplateMaster master = templateMasterService.getById(ticketMasterId);
        List<TicketTemplate> templates = ticketTemplateService.getTopLevelByTicketTemplateMaster(master);

        Collections.sort(templates);

        List<JSONObject> jsonObjects = new ArrayList<JSONObject>();
        for (TicketTemplate template : templates) {
            jsonObjects.add(getTicketTemplateItems(template));
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", jsonObjects);

        response.setContentType("application/json");

        response.getWriter().print(store.toString());

        return null;
    }

    private JSONObject getTicketTemplateItems(TicketTemplate template) throws JSONException {

        JSONObject item = new JSONObject();
        item.put("id", template.getId());
        item.put("name", template.getTemplateName());

        if (template.getSubtickets().size() > 0) {
            List<TicketTemplate> subTickets = new ArrayList<TicketTemplate>(template.getSubtickets());
            Collections.sort(subTickets);
            List<JSONObject> kids = new ArrayList<JSONObject>();
            for (TicketTemplate ticketTemplate : subTickets) {
                kids.add(getTicketTemplateItems(ticketTemplate));
            }

            item.put("children", kids);
        }

        return item;
    }

    /**
     * Writes a JSON string to the response writer representing all the {@link User}s in the system.
     * <p/>
     * A request parameter distinguishes whether this returns just {@link User}s or respresentations
     * of {@link UserRoleGroup} objects. The parameter is "tech" and the value can be either true, false, 1, 0.
     *
     * @param request  the http request
     * @param response the http response
     * @return null. The JSON string of users is written to the response writer.
     * @throws Exception right after one occurs
     */
    @SuppressWarnings("unchecked")
    public ModelAndView getUserStore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Locale locale = RequestContextUtils.getLocale(request);

        String queryParam = ServletRequestUtils.getStringParameter(request, "label", "");
        queryParam = StringUtils.replace(queryParam, "*", "");
        Integer start = ServletRequestUtils.getIntParameter(request, "start", -1);
        Integer count = ServletRequestUtils.getIntParameter(request, "count", -1);
        boolean techs = ServletRequestUtils.getBooleanParameter(request, "tech", false);
        boolean incInactive = ServletRequestUtils.getBooleanParameter(request, "incInactive", false);
        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);
        boolean allUrgs = ServletRequestUtils.getBooleanParameter(request, "allUrgs", false);

        String sort = ServletRequestUtils.getStringParameter(request, "sort");
        Map<String, Boolean> sortFieldMap = new HashMap<String, Boolean>();
        if(sort != null) {
            boolean direction = true;
            if(StringUtils.startsWith(sort, "-")) {
                direction = false;
            }
            sortFieldMap.put(sort.replace("-", ""), direction);
        } else {
            sortFieldMap.put("lastName", true);
            sortFieldMap.put("firstName", true);
        }

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("userId", "");
            emptyItem.put("label", getMessageSourceAccessor().getMessage("global.notSelected", "(None)"));
            emptyItem.put("login", "");
            emptyItem.put("email", "");
            items.add(emptyItem);
        }

        int numRows;
        if (!techs) {
            List<User> users;
            if (start > -1) {
                users = userService.getUsers(queryParam, start, count, sortFieldMap, incInactive);
                numRows = userService.getUserCount(queryParam, incInactive);
            } else {
                users = userService.getUsers();
                numRows = users.size();
            }

            for (User user : users) {
                items.add(getJSONUserItem(user, locale));
            }
        } else {
            List<UserRoleGroup> urgs;
            if (start > -1) {
                urgs = userRoleGroupService.getAssignableUserRoleGroups(queryParam, start, count);
                numRows = userRoleGroupService.getAssignableUserRoleGroupCount(queryParam);
            } else {
                urgs = userRoleGroupService.getAllAssignableUserRoleGroups();
                numRows = urgs.size();
            }

            for (UserRoleGroup urg : urgs) {
                if (allUrgs) {
                    items.add(getJSONUrgItem(urg));
                } else {
                    // modifying this for loop so that it gets only the ticket pool urgs and ignores the urgs representing
                    // technicians as part of the enhancement to have only a single user in the assignedTo dropdown on the
                    // ticket search pages
                    if (urg.getUserRole() == null) {
                        items.add(getJSONTechnicianItem(urg, null));
                    }
                }

            }

            if (!allUrgs) {
                // Ticket pool urgs are added, now add users that are technicians
                List<User> techsAsUsers = userRoleGroupService.getTechnicianAsUsers(queryParam);
                for (User user : techsAsUsers) {
                    items.add(getJSONTechnicianItem(null, user));
                }
            }
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "userId");
        store.put("numRows", numRows);
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    public ModelAndView getTicketSearchAssignments(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String queryParam = ServletRequestUtils.getStringParameter(request, "label");
        queryParam = StringUtils.replace(queryParam, "*", "");
        Integer start = ServletRequestUtils.getIntParameter(request, "start", 0);
        Integer count = ServletRequestUtils.getIntParameter(request, "count", 0);
        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);

        List<Object> masterList = new ArrayList<Object>();

        masterList.addAll(userRoleGroupService.getAllTicketPool(queryParam, getMessageSourceAccessor().getMessage("ticketList.ticketPool")));
        masterList.addAll(userRoleGroupService.getTechnicianAsUsers(queryParam));

        int numRows = masterList.size();

        int toIndex = start+count;
        if(toIndex > numRows || toIndex<0) toIndex = numRows;

        List sublist = masterList.subList(start, toIndex);

        List<JSONObject> items = new ArrayList<JSONObject>();

        if (incEmpty) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("userId", "");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        for (Object object : sublist) {
            if(object instanceof UserRoleGroup){
                items.add(getJSONTechnicianItem((UserRoleGroup)object, null));
            } else if(object instanceof User){
                items.add(getJSONTechnicianItem(null, (User)object));
            }
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "userId");
        store.put("numRows", numRows);
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    private JSONObject getJSONTechnicianItem(UserRoleGroup urg, User user) throws JSONException {
        JSONObject item = new JSONObject();

        if (urg != null) {
            item.put("userId", "urg_" + urg.getId());

            String label;
            String login = "";

            String pool = getMessageSourceAccessor().getMessage("ticketList.ticketPool");
            label = pool + " - " + urg.getGroup().getName();

            item.put("label", label);
            item.put("login", login);
        }
        if (user != null) {
            item.put("userId", "usr_" + user.getId());
            String label = new StringBuilder()
                    .append(user.getLastName()).append(", ").append(user.getFirstName())
                    .append(" (").append(user.getLoginId()).append(")").toString();
            item.put("label", label);
            item.put("login", user.getLoginId());
        }

        return item;
    }

    private JSONObject getJSONUrgItem(UserRoleGroup urg) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("userId", urg.getId());

        String label;
        String login = "";

        // Determine the label to use. If userRole is not null use user's name with group name
        // otherwise use Ticket Pool text plus group name
        if (urg.getUserRole() != null)  {
            User user = urg.getUserRole().getUser();
            label = user.getLastName() + ", " + user.getFirstName() + " - " + urg.getGroup().getName();
            login = user.getLoginId();
        } else {
            String pool = getMessageSourceAccessor().getMessage("ticketList.ticketPool");
            label = pool + " - " + urg.getGroup().getName();
        }

        item.put("label", label);
        item.put("login", login);
        return item;
    }

    private JSONObject getJSONUserItem(User user, Locale locale) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("userId", user.getId());
        item.put("lastName", user.getLastName());
        item.put("firstName", user.getFirstName());
        item.put("email", user.getEmail());
        item.put("active", getMessageSourceAccessor().getMessage(user.getActiveString(), locale));
        StringBuilder rolesString = new StringBuilder();
        Object[] roles = user.getRoles().toArray();
        for (int x = 0; x < roles.length; x++) {
            rolesString.append(getMessageSourceAccessor().getMessage(roles[x].toString(), locale));
            if(x < roles.length - 1) {
                rolesString.append(", ");
            }
        }
        item.put("roles", rolesString);
        StringBuilder groupsString = new StringBuilder();
        for (int y = 0; y < user.getGroups().size(); y++) {
            groupsString.append(user.getGroups().get(y).getName());
            if(y < user.getGroups().size() - 1) {
                groupsString.append(", ");
            }
        }
        item.put("groups", groupsString);
        String label = new StringBuilder()
                .append(user.getLastName()).append(", ").append(user.getFirstName())
                .append(" (").append(user.getLoginId()).append(")").toString();
        item.put("label", label);
        item.put("loginId", user.getLoginId());
        item.put("email", user.getEmail());
        List<Phone> phoneList = user.getPhoneList();
        if (phoneList.size() == 0) {
            item.put("phone", "");
        } else {
            item.put("phone", phoneList.get(0).getNumber());
        }

        if(user.getLocation() != null){
            item.put("location", user.getLocation().getName());
            item.put("location_id", user.getLocation().getId());
        } else {
            item.put("location", "");
            item.put("location_id", "");
        }
        return item;
    }

    /**
     * Writes a JSON string to the response writer representing all the {@link SoftwareLicense}s in the system
     *
     * @param request  the http request
     * @param response the http response
     * @return null. The JSON string of software licenses is written to the response writer.
     * @throws Exception right after one occurs
     */
    public ModelAndView getSoftwareLicenseStore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;

        List<SoftwareLicense> lics = softwareLicenseService.getAllSoftwareLicenses();
        List<JSONObject> items = new ArrayList<JSONObject>();
        JSONObject item;
        for (SoftwareLicense lic : lics) {
            item = new JSONObject();
            item.put("id", lic.getId());
            item.put("name", lic.getProductName());
            item.put("version", lic.getProductVersion());
            item.put("displayName", lic.getProductName() + " - " + lic.getProductVersion());
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "displayName");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    /**
     * Writes a JSON string to the response writer representing all the {@link Category} objects in the system
     *
     * @param request  the http request
     * @param response the http response
     * @return null. The JSON string of categories is written to the response writer.
     * @throws Exception right after one occurs
     */
    public ModelAndView getCategoryStore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;

        List<JSONObject> items = new ArrayList<JSONObject>();

        JSONObject item = new JSONObject();
        // add a blank item
        item.put("id", "");
        item.put("name", "");
        item.put("groupId", "");
        item.put("active", "");
        items.add(item);

        List<Category> categories = categoryService.getCategories();
        for (Category category : categories) {
            item = new JSONObject();
            item.put("id", category.getId());
            item.put("name", category.getName());
            item.put("groupId", category.getGroup().getId());
            item.put("active", category.isActive());
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    /**
     * Writes a JSON string to the response writer representing all the {@link CategoryOption} objects in the system
     *
     * @param request  the http request
     * @param response the http response
     * @return null. The JSON string of category options is written to the response writer.
     * @throws Exception right after one occurs
     */
    public ModelAndView getCategoryOptionStore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        assert request != null;

        List<JSONObject> items = new ArrayList<JSONObject>();

        JSONObject item = new JSONObject();
        // add a blank item
        item.put("id", "");
        item.put("name", "");
        item.put("categoryId", "");
        item.put("active", "");
        items.add(item);

        List<CategoryOption> catOpts = categoryOptionService.getCategoryOptions();
        for (CategoryOption option : catOpts) {
            item = new JSONObject();
            item.put("id", option.getId());
            item.put("name", option.getName());
            item.put("categoryId", option.getCategory().getId());
            item.put("active", option.isActive());
            items.add(item);
        }


        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("label", "name");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    /**
     * This method writes a JSON represention of the states, territories of the U.S. and
     * the provinces of Canada to the <code>response</code>. Each line item contains the display
     * value, it's abbreviation and a type to signify state, territory or province.
     * <p/>
     * By default all 3 sets are returned. A <code>request</code> parameter can be used to return
     * any combination of the 3. The parameter name is "items" and the value should be a dash("-")
     * separated string of "st", "tr", "pr".
     * st == include U.S. states
     * tr == include U.S. Territories
     * pr == include Canadian Provinces
     * <p/>
     * For example, to get all 3 the value should be "st-tr-pr", to exclude Canada the value is "st-tr"
     * <p/>
     * The list is ordered alphabetically with U.S. states then territories Canadian provinces.
     *
     * @param request  the http request
     * @param response the http response
     * @return null right after writing the JSON string to the response.
     * @throws Exception when one occurs
     */
    public ModelAndView getStatesAndProvincesStore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String itemsToGet = ServletRequestUtils.getStringParameter(request, "items", "st-tr-pr");
        List itemsSplit = Arrays.asList(itemsToGet.split("-"));

        boolean includeUSStates = itemsSplit.contains("st");
        boolean includeTerritories = itemsSplit.contains("tr");
        boolean includeProvinces = itemsSplit.contains("pr");

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (includeUSStates) {
            items.add(getJSONStateItem("Alabama", "AL", "state"));
            items.add(getJSONStateItem("Alaska", "AK", "state"));
            items.add(getJSONStateItem("Arizona", "AZ", "state"));
            items.add(getJSONStateItem("Arkansas", "AR", "state"));
            items.add(getJSONStateItem("California", "CA", "state"));
            items.add(getJSONStateItem("Colorado", "CO", "state"));
            items.add(getJSONStateItem("Connecticut", "CT", "state"));
            items.add(getJSONStateItem("Delaware", "DE", "state"));
            items.add(getJSONStateItem("Florida", "FL", "state"));
            items.add(getJSONStateItem("Georgia", "GA", "state"));
            items.add(getJSONStateItem("Hawaii", "HI", "state"));
            items.add(getJSONStateItem("Idaho", "ID", "state"));
            items.add(getJSONStateItem("Illinois", "IL", "state"));
            items.add(getJSONStateItem("Indiana", "IN", "state"));
            items.add(getJSONStateItem("Iowa", "IA", "state"));
            items.add(getJSONStateItem("Kansas", "KS", "state"));
            items.add(getJSONStateItem("Kentucky", "KY", "state"));
            items.add(getJSONStateItem("Louisiana", "LA", "state"));
            items.add(getJSONStateItem("Maine", "ME", "state"));
            items.add(getJSONStateItem("Maryland", "MD", "state"));
            items.add(getJSONStateItem("Massachusetts", "MA", "state"));
            items.add(getJSONStateItem("Michigan", "MI", "state"));
            items.add(getJSONStateItem("Minnesota", "MN", "state"));
            items.add(getJSONStateItem("Mississippi", "MS", "state"));
            items.add(getJSONStateItem("Missouri", "MO", "state"));
            items.add(getJSONStateItem("Montana", "MT", "state"));
            items.add(getJSONStateItem("Nebraska", "NB", "state"));
            items.add(getJSONStateItem("Nevada", "NV", "state"));
            items.add(getJSONStateItem("New Hampshire", "NH", "state"));
            items.add(getJSONStateItem("New Jersey", "NJ", "state"));
            items.add(getJSONStateItem("New Mexico", "NM", "state"));
            items.add(getJSONStateItem("New York", "NY", "state"));
            items.add(getJSONStateItem("North Carolina", "NC", "state"));
            items.add(getJSONStateItem("North Dakota", "ND", "state"));
            items.add(getJSONStateItem("Ohio", "OH", "state"));
            items.add(getJSONStateItem("Oklahoma", "OK", "state"));
            items.add(getJSONStateItem("Oregon", "OR", "state"));
            items.add(getJSONStateItem("Pennsylvania", "PA", "state"));
            items.add(getJSONStateItem("Rhode Island", "RI", "state"));
            items.add(getJSONStateItem("South Carolina", "SC", "state"));
            items.add(getJSONStateItem("South Dakota", "SD", "state"));
            items.add(getJSONStateItem("Tennessee", "TN", "state"));
            items.add(getJSONStateItem("Texas", "TX", "state"));
            items.add(getJSONStateItem("Utah", "UT", "state"));
            items.add(getJSONStateItem("Vermont", "VT", "state"));
            items.add(getJSONStateItem("Virginia", "VA", "state"));
            items.add(getJSONStateItem("Washington", "WA", "state"));
            items.add(getJSONStateItem("West Virginia", "WV", "state"));
            items.add(getJSONStateItem("Wisconsin", "WI", "state"));
            items.add(getJSONStateItem("Wyoming", "WY", "state"));
        }
        if (includeTerritories) {
            items.add(getJSONStateItem("American Samoa", "AS", "territory"));
            items.add(getJSONStateItem("District of Columbia", "DC", "territory"));
            items.add(getJSONStateItem("Fed. States of Micronesia", "FM", "territory"));
            items.add(getJSONStateItem("Guam", "GU", "territory"));
            items.add(getJSONStateItem("Marshall Islands", "MH", "territory"));
            items.add(getJSONStateItem("Northern Marianas", "MP", "territory"));
            items.add(getJSONStateItem("Puerto Rico", "PR", "territory"));
            items.add(getJSONStateItem("(U.S.) Virgin Islands", "VI", "territory"));
            items.add(getJSONStateItem("Armed Forces, the Americas", "AA", "territory"));
            items.add(getJSONStateItem("Armed Forces, Europe", "AE", "territory"));
            items.add(getJSONStateItem("Armed Forces, Pacific", "AP", "territory"));
        }
        if (includeProvinces) {
            items.add(getJSONStateItem("Alberta", "AB", "province"));
            items.add(getJSONStateItem("British Columbia", "BC", "province"));
            items.add(getJSONStateItem("Manitoba", "MB", "province"));
            items.add(getJSONStateItem("New Brunswick", "NBR", "province"));
            items.add(getJSONStateItem("Newfoundland", "NF", "province"));
            items.add(getJSONStateItem("Northwest Territories and Nunavut", "NT", "province"));
            items.add(getJSONStateItem("Nova Scotia", "NS", "province"));
            items.add(getJSONStateItem("Ontario", "ON", "province"));
            items.add(getJSONStateItem("Prince Edward Island", "PE", "province"));
            items.add(getJSONStateItem("Quebec", "QC", "province"));
            items.add(getJSONStateItem("Saskatchewan", "SK", "province"));
            items.add(getJSONStateItem("Yukon Territory", "YT", "province"));
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "abbreviation");
        store.put("label", "name");
        store.put("items", items);

        response.getOutputStream().print(store.toString());
        return null;
    }

    private JSONObject getJSONStateItem(String name, String abbr, String type) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("name", name);
        item.put("abbreviation", abbr);
        item.put("type", type);
        return item;
    }

    public ModelAndView getLocationStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String queryParam = ServletRequestUtils.getStringParameter(request, "label");
        queryParam = StringUtils.replace(queryParam, "*", "");
        Integer start = ServletRequestUtils.getIntParameter(request, "start", -1);
        Integer count = ServletRequestUtils.getIntParameter(request, "count", -1);
        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        int numRows;
        List<Location> locations;
        if (start > -1) {
            locations = locationService.getLocations(queryParam, start, count);
            numRows = locationService.getLocationCount(queryParam);
        } else {
            locations = locationService.getLocations();
            numRows = locations.size();
        }

        for (Location location : locations) {
            JSONObject item = new JSONObject();
            item.put("id", location.getId());
            item.put("label", location.getName());
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("numRows", numRows);
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    public ModelAndView getPriorityStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        List<TicketPriority> priorities = priorityService.getPriorities();
        for (TicketPriority priority : priorities) {
            JSONObject item = new JSONObject();
            item.put("id", priority.getId());
            item.put("label", priority.getName());
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    public ModelAndView getGroupStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);
        String filterPerm = ServletRequestUtils.getStringParameter(request, "filterByPerm", "");

        List<Group> groups = groupService.getGroups();

        User user = userService.getLoggedInUser();

        if(!"".equals(filterPerm)) {
            for (Iterator<Group> iterator = groups.iterator(); iterator.hasNext(); ) {
                Group group = iterator.next();
                if (!permissionService.hasGroupPermission(user, permissionService.getPermissionByKey(filterPerm), group)) {
                    iterator.remove();
                }
            }
        }

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty && groups.size() > 1) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            emptyItem.put("assTrackType", "");
            items.add(emptyItem);
        }

        for (Group group : groups) {
            JSONObject item = new JSONObject();
            item.put("id", group.getId());
            item.put("label", group.getName());
            item.put("assTrackType", group.getAssetTrackerType());
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    public ModelAndView getStatusStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        List<Status> statusi = statusService.getStatus();
        for (Status status : statusi) {
            JSONObject item = new JSONObject();
            item.put("id", status.getId());
            item.put("label", status.getName());
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    public ModelAndView getCategoriesStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);
        String prevIdStr = ServletRequestUtils.getStringParameter(request, "prevId"); // previd is group id
        Integer prevId = StringUtils.isNotBlank(prevIdStr) ? Integer.parseInt(prevIdStr) : null;
        Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);

        List<Category> categories = new ArrayList<Category>();
        if(id > 0) {
            categories.add(categoryService.getCategoryById(id));
        } else {
            if (prevIdStr == null) {
                categories = categoryService.getCategories();
            } else {
                if (prevId != null) {
                    categories = categoryService.getCategoryByGroupId(prevId);
                }
            }
        }

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty && categories.size() > 1) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            emptyItem.put("catOpts", "");
            items.add(emptyItem);
        }

        for (Category category : categories) {
            JSONObject item = new JSONObject();
            item.put("id", category.getId());
            item.put("label", category.getName());
            item.put("catOpts", getCatOptsAsJSON(category.getCategoryOptions()));
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    private List<JSONObject> getCatOptsAsJSON(Set<CategoryOption> categoryOptions) throws Exception {
        List<JSONObject> items = new ArrayList<JSONObject>();
        for (CategoryOption categoryOption : categoryOptions) {
            JSONObject item = new JSONObject();
            item.put("catOptId", categoryOption.getId());
            item.put("catOptLabel", categoryOption.getName());
            items.add(item);
        }
        return items;
    }

    public ModelAndView getCatOptionsStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);
        String prevIdStr = ServletRequestUtils.getStringParameter(request, "prevId"); // prevId is category id
        Integer prevId = StringUtils.isNotBlank(prevIdStr) ? Integer.parseInt(prevIdStr) : null;
        Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);

        List<CategoryOption> catOptions = new ArrayList<CategoryOption>();
        if(id > 0) {
            catOptions.add(categoryOptionService.getCategoryOptionById(id));
        } else {
            if (prevIdStr == null) {
                catOptions = categoryOptionService.getCategoryOptions();
            } else {
                if (prevId != null) {
                    catOptions = categoryOptionService.getCategoryOptionByCategoryId(prevId);
                }
            }
        }

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty && catOptions.size() > 1) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        for (CategoryOption catOption : catOptions) {
            JSONObject item = new JSONObject();
            item.put("id", catOption.getId());
            item.put("label", catOption.getName());
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    public ModelAndView getAssignmentStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);

        // prevId is category option id
        String catOptIdStr = ServletRequestUtils.getStringParameter(request, "prevId");
        String groupIdStr = ServletRequestUtils.getStringParameter(request, "groupId");
        String locationIdStr = ServletRequestUtils.getStringParameter(request, "locationId");

        Integer catOptId = StringUtils.isNotBlank(catOptIdStr) ? Integer.valueOf(catOptIdStr) : null;
        Integer groupId = StringUtils.isNotBlank(groupIdStr) ? Integer.valueOf(groupIdStr) : null;
        Integer locationId = StringUtils.isNotBlank(locationIdStr) ? Integer.valueOf(locationIdStr) : null;

        // Retrieve the UserRoleGroup items for this store. There are 3 options:
        // 1. if the catOptId is not passed then this is not a store for a cascading dropdown so don't limit the URGs based
        //    on a previous value (eg. Category Option or Group) just return all URGs in the system (active)
        // 2. catOptId is provided and the User only has the User role, then give the URGs for just the catOptId category option
        // 3. catOptId is provided and the User is a technician, manager or admin, the give all URGs for the group
        List<UserRoleGroup> allUrgs = new ArrayList<UserRoleGroup>();

        List<UserRoleGroup> urgs = new ArrayList<UserRoleGroup>();
        if (groupId != null) {
            if (catOptIdStr == null) {
                urgs = userRoleGroupService.getUserRoleGroupByGroupId(groupId);
            } else {
                if (catOptId != null) {
                    User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

                    boolean userOnly = true;

                    if(user.getId().equals(User.ADMIN_ID)){
                        userOnly = false;
                    } else {
                        // check if user is a tech, manager, or admin in group
                        UserRoleGroup urg = userRoleGroupService.getUserRoleGroupByUserIdAndGroupId(user.getId(), groupId);
                        if (urg != null) {
                            Role role = urg.getUserRole().getRole();
                            if(role.getId().equals(Role.ROLE_MANAGER_ID) || role.getId().equals(Role.ROLE_TECHNICIAN_ID)){
                                userOnly = false;
                            }
                        }
                    }

                    Location location = locationService.getLocationById(locationId);
                    CategoryOption categoryOption = categoryOptionService.getCategoryOptionById(catOptId);
                    Category category = categoryOption.getCategory();
                    Group group = category.getGroup();

                    urgs = assignmentService.getAssignments(location, group, category, categoryOption);

                    if (!userOnly ){
                        allUrgs = userRoleGroupService.getAssignableUserRoleGroupByGroupId(groupId);
                        for (Iterator<UserRoleGroup> i = allUrgs.iterator(); i.hasNext();) {
                            UserRoleGroup userRoleGroup = i.next();
                            if (urgs.contains(userRoleGroup)) {
                                i.remove();
                            }
                        }
                        Collections.sort(allUrgs);
                    }
                    Collections.sort(urgs);
                }
            }
        }

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty && urgs.size() > 1) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "-1");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        String label;
        String ticketPoolLabel = getMessageSourceAccessor().getMessage("ticketList.ticketPool");
        if (allUrgs.size() > 0) {
            JSONObject emptyItem;
            emptyItem = new JSONObject();
            emptyItem.put("id", "-2");
            String assignmentsLabel = getMessageSourceAccessor().getMessage("ticketSettings.assignments");
            emptyItem.put("label", "- " + assignmentsLabel + " -");
            items.add(emptyItem);

            for (UserRoleGroup urg : urgs) {
                label = ticketPoolLabel;
                if (urg.getUserRole() != null) {
                    label = urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName();
                }

                JSONObject item = new JSONObject();
                item.put("id", urg.getId());
                item.put("label", label);
                items.add(item);
            }

            emptyItem = new JSONObject();
            emptyItem.put("id", "-3");
            String allGroupMembersLabel = getMessageSourceAccessor().getMessage("ticketSettings.allGroupMembers");
            emptyItem.put("label", "- " + allGroupMembersLabel + " -");
            items.add(emptyItem);

            for (UserRoleGroup urg : allUrgs) {
                label = ticketPoolLabel;
                if (urg.getUserRole() != null) {
                    label = urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName();
                }

                JSONObject item = new JSONObject();
                item.put("id", urg.getId());
                item.put("label", label);
                items.add(item);
            }
        } else {
            for (UserRoleGroup urg : urgs) {
                label = ticketPoolLabel;
                if (urg.getUserRole() != null) {
                    label = urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName();
                }

                JSONObject item = new JSONObject();
                item.put("id", urg.getId());
                item.put("label", label);
                items.add(item);
            }
        }

        // check if ticket.assignedto is not in the urg lists, and add it if necessary
        Ticket ticket = null;
        String ticketIdStr = ServletRequestUtils.getStringParameter(request, "ticketId");
        if (StringUtils.isNotBlank(ticketIdStr)) {
            ticket = ticketService.getTicketById(Integer.valueOf(ticketIdStr));
        }

        if (ticket != null && ticket.getAssignedTo() != null &&
                !urgs.contains(ticket.getAssignedTo()) && !allUrgs.contains(ticket.getAssignedTo())) {
            label = ticketPoolLabel;
            if (ticket.getAssignedTo().getUserRole() != null) {
                label = ticket.getAssignedTo().getUserRole().getUser().getLastName() + ", " +
                        ticket.getAssignedTo().getUserRole().getUser().getFirstName();
            }

            JSONObject item = new JSONObject();
            item.put("id", ticket.getAssignedTo().getId());
            item.put("label", label);
            items.add(item);
        }

        // If items is empty at this point, then assume this is a case where no assignemnts were set, add a ticket pool
        // URG for the group id.
        if(items.size() == 0 && groupId != null){
            UserRoleGroup tp = userRoleGroupService.getTicketPoolByGroupId(groupId);
            JSONObject item = new JSONObject();
            item.put("id", tp.getId());
            item.put("label", ticketPoolLabel);
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    // this is more better than getAssignmentStore because it will return urgs based on whatever you can attach an assignment
    // to these days, namely: location, group, category, category option
    public ModelAndView getMoreBetterAssignmentStore(HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean incEmpty = ServletRequestUtils.getBooleanParameter(request, "incEmpty", false);

        String catIdStr = ServletRequestUtils.getStringParameter(request, "catId");
        String catOptIdStr = ServletRequestUtils.getStringParameter(request, "catOptId");
        String groupIdStr = ServletRequestUtils.getRequiredStringParameter(request, "groupId");
        String locationIdStr = ServletRequestUtils.getStringParameter(request, "locationId");

        Integer catId = StringUtils.isNotBlank(catIdStr) ? Integer.valueOf(catIdStr) : null;
        Integer catOptId = StringUtils.isNotBlank(catOptIdStr) ? Integer.valueOf(catOptIdStr) : null;
        Integer groupId = StringUtils.isNotBlank(groupIdStr) ? Integer.valueOf(groupIdStr) : null;
        Integer locationId = StringUtils.isNotBlank(locationIdStr) ? Integer.valueOf(locationIdStr) : null;

        // Retrieve the UserRoleGroup items for this store. There are 2 options:
        // 2. catOptId is provided and the User only has the User role, then give the URGs for just the catOptId category option
        // 3. catOptId is provided and the User is a technician, manager or admin, the give all URGs for the group
        List<UserRoleGroup> allUrgs = new ArrayList<UserRoleGroup>();

        List<UserRoleGroup> urgs = new ArrayList<UserRoleGroup>();
        if (groupId != null) {
            User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

            boolean userOnly = true;

            if(user.getId().equals(User.ADMIN_ID)){
                userOnly = false;
            } else {
                // check if user is a tech, manager, or admin in group
                UserRoleGroup urg = userRoleGroupService.getUserRoleGroupByUserIdAndGroupId(user.getId(), groupId);
                if (urg != null) {
                    Role role = urg.getUserRole().getRole();
                    if(role.getId().equals(Role.ROLE_MANAGER_ID) || role.getId().equals(Role.ROLE_TECHNICIAN_ID)){
                        userOnly = false;
                    }
                }
            }

            Location location = locationService.getLocationById(locationId);
            CategoryOption categoryOption = categoryOptionService.getCategoryOptionById(catOptId);
            Category category = categoryService.getCategoryById(catId);
            Group group = groupService.getGroupById(groupId);

            urgs = assignmentService.getMoreBetterAssignments(location, group, category, categoryOption);

            if (!userOnly ){
                allUrgs = userRoleGroupService.getAssignableUserRoleGroupByGroupId(groupId);
                for (Iterator<UserRoleGroup> i = allUrgs.iterator(); i.hasNext();) {
                    UserRoleGroup userRoleGroup = i.next();
                    if (urgs.contains(userRoleGroup)) {
                        i.remove();
                    }
                }
                Collections.sort(allUrgs);
            }
            Collections.sort(urgs);
        }

        List<JSONObject> items = new ArrayList<JSONObject>();
        if (incEmpty) {
            JSONObject emptyItem = new JSONObject();
            emptyItem.put("id", "");
            emptyItem.put("label", "");
            items.add(emptyItem);
        }

        String label;
        String ticketPoolLabel = getMessageSourceAccessor().getMessage("ticketList.ticketPool");
        if (allUrgs.size() > 0) {
            JSONObject emptyItem;
            emptyItem = new JSONObject();
            emptyItem.put("id", "-2");
            String assignmentsLabel = getMessageSourceAccessor().getMessage("ticketSettings.assignments");
            emptyItem.put("label", "- " + assignmentsLabel + " -");
            items.add(emptyItem);

            for (UserRoleGroup urg : urgs) {
                label = ticketPoolLabel;
                if (urg.getUserRole() != null) {
                    label = urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName();
                }

                JSONObject item = new JSONObject();
                item.put("id", urg.getId());
                item.put("label", label);
                items.add(item);
            }

            emptyItem = new JSONObject();
            emptyItem.put("id", "-3");
            String allGroupMembersLabel = getMessageSourceAccessor().getMessage("ticketSettings.allGroupMembers");
            emptyItem.put("label", "- " + allGroupMembersLabel + " -");
            items.add(emptyItem);

            for (UserRoleGroup urg : allUrgs) {
                label = ticketPoolLabel;
                if (urg.getUserRole() != null) {
                    label = urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName();
                }

                JSONObject item = new JSONObject();
                item.put("id", urg.getId());
                item.put("label", label);
                items.add(item);
            }
        } else {
            for (UserRoleGroup urg : urgs) {
                label = ticketPoolLabel;
                if (urg.getUserRole() != null) {
                    label = urg.getUserRole().getUser().getLastName() + ", " + urg.getUserRole().getUser().getFirstName();
                }

                JSONObject item = new JSONObject();
                item.put("id", urg.getId());
                item.put("label", label);
                items.add(item);
            }
        }

        // check if ticket.assignedto is not in the urg lists, and add it if necessary
        Ticket ticket = null;
        String ticketIdStr = ServletRequestUtils.getStringParameter(request, "ticketId");
        if (StringUtils.isNotBlank(ticketIdStr)) {
            ticket = ticketService.getTicketById(Integer.valueOf(ticketIdStr));
        }

        if (ticket != null && ticket.getAssignedTo() != null &&
                !urgs.contains(ticket.getAssignedTo()) && !allUrgs.contains(ticket.getAssignedTo())) {
            label = ticketPoolLabel;
            if (ticket.getAssignedTo().getUserRole() != null) {
                label = ticket.getAssignedTo().getUserRole().getUser().getLastName() + ", " +
                        ticket.getAssignedTo().getUserRole().getUser().getFirstName();
            }

            JSONObject item = new JSONObject();
            item.put("id", ticket.getAssignedTo().getId());
            item.put("label", label);
            items.add(item);
        }

        // If items is empty at this point, then assume this is a case where no assignemnts were set, add a ticket pool
        // URG for the group id.
        if(items.size() == 0 && groupId != null){
            UserRoleGroup tp = userRoleGroupService.getTicketPoolByGroupId(groupId);
            JSONObject item = new JSONObject();
            item.put("id", tp.getId());
            item.put("label", ticketPoolLabel);
            items.add(item);
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    public ModelAndView getTemplateCustomFields(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Integer templateId = null;
        Integer groupId = null;
        Integer locId = null;
        Integer catId = null;
        Integer catOptId = null;

        if (StringUtils.isNotBlank(request.getParameter("ttId")))
            templateId = ServletRequestUtils.getIntParameter(request, "ttId");

        if (StringUtils.isNotBlank(request.getParameter("groupId")))
            groupId = ServletRequestUtils.getIntParameter(request, "groupId");

        if (StringUtils.isNotBlank(request.getParameter("locId")))
            locId = ServletRequestUtils.getIntParameter(request, "locId");

        if (StringUtils.isNotBlank(request.getParameter("catId")))
            catId = ServletRequestUtils.getIntParameter(request, "catId");

        if (StringUtils.isNotBlank(request.getParameter("catOptId")))
            catOptId = ServletRequestUtils.getIntParameter(request, "catOptId");


        TicketTemplate template = null;
        if (templateId != null) template = ticketTemplateService.getById(templateId);

        Location location = null;
        if (locId != null)
            location = locationService.getLocationById(locId);

        CategoryOption categoryOption = null;
        if (catOptId != null)
            categoryOption = categoryOptionService.getCategoryOptionById(catOptId);

        Category category = null;
        if (catId != null)
            category = categoryService.getCategoryById(catId);

        Group group = null;
        if (groupId != null)
            group = groupService.getGroupById(groupId);

        JSONArray cfs = new JSONArray();

        List<CustomField> customFields = customFieldsService.getCustomFields(location, group, category, categoryOption);
        Collections.sort(customFields);

        for (CustomField cf : customFields) {

            JSONObject item = new JSONObject();
            item.put("id", cf.getId());
            item.put("name", StringEscapeUtils.escapeHtml(cf.getName()));
            item.put("type", cf.getFieldType());

            JSONArray options = new JSONArray();
            List<CustomFieldOption> opts = cf.getOptions();
            for (CustomFieldOption opt : opts) {
                JSONObject jopt = new JSONObject();
                jopt.put("id", opt.getId());
                jopt.put("value", opt.getValue() == null ? "" : StringEscapeUtils.escapeHtml(opt.getValue()));
                jopt.put("displayValue", opt.getDisplayValue() == null ? "" : StringEscapeUtils.escapeHtml(opt.getDisplayValue()));
                options.put(jopt);
            }

            item.put("options", options);
            item.put("cfValue", getTemplateCustomFieldValue(template, cf));
            cfs.put(item);
        }

        response.setContentType("application/json");
        response.getWriter().print(cfs.toString());
        return null;
    }

    private JSONObject getTemplateCustomFieldValue(TicketTemplate template, CustomField cf) throws JSONException {

        // get the custom field values for this template, if any
        TicketTemplateCustomFieldValue cfv = null;
        if (template != null) {
            for (TicketTemplateCustomFieldValue cfvFound : template.getCustomeFieldValues()) {
                if (cfvFound.getCustomField().getId().equals(cf.getId())) {
                    cfv = cfvFound;
                    break;
                }
            }
        }

        JSONObject json = new JSONObject();
        if (cfv != null) {
            json.put("id", cfv.getId());
            json.put("value", cfv.getFieldValue() == null ? "" : StringEscapeUtils.escapeHtml(cfv.getFieldValue()));
            json.put("ttId", cfv.getTicketTemplate().getId());
            json.put("cfId", cfv.getCustomField().getId());
        } else {
            json.put("id", "");
            json.put("value", "");
            json.put("ttId", "");
            json.put("cfId", "");
        }
        return json;
    }

    public ModelAndView getTicketCustomFields(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Integer groupId = ServletRequestUtils.getIntParameter(request, "groupId", -1);
        Integer catId = ServletRequestUtils.getIntParameter(request, "catId", -1);
        Integer catOptId = ServletRequestUtils.getIntParameter(request, "catOptId", -1);
        String ticketId = ServletRequestUtils.getStringParameter(request, "tid");
        Integer locationId = ServletRequestUtils.getIntParameter(request, "locId", -1);
        Ticket ticket = null;
        if (ticketId != null) ticket = ticketService.getTicketById(Integer.valueOf(ticketId));

        JSONArray cfs = new JSONArray();

        Location location = null;
        if (locationId > -1) {
            location = locationService.getLocationById(locationId);
        }

        CategoryOption categoryOption = null;
        if (catOptId > -1) {
            categoryOption = categoryOptionService.getCategoryOptionById(catOptId);
        }

        Category category = null;
        if (catId > -1) {
            category = categoryService.getCategoryById(catId);
        }

        Group group = null;
        if (groupId > -1) {
            group = groupService.getGroupById(groupId);
        }

//        List<CustomField> customFields = customFieldsService.getCustomFieldsByCategoryOptionId(catOptId);
        List<CustomField> customFields = customFieldsService.getCustomFields(location, group, category, categoryOption);
        Collections.sort(customFields);

        // TODO: check permissions when acl's on custom fields are implemented
        // remove custom fields that user doesn't have view or edit permission
        /*
        for (Iterator<CustomField> iterator = customFields.iterator(); iterator.hasNext(); ) {
            CustomField customField = iterator.next();
            if(!permissionService.hasGroupPermission("ticket.view." + customField.getName(), customField.getGroup())) {
                iterator.remove();
            }
        }
        */

        for (CustomField cf : customFields) {

            JSONObject item = new JSONObject();
            item.put("id", cf.getId());
            item.put("name", StringEscapeUtils.escapeHtml(cf.getName()));
            item.put("type", cf.getFieldType());
            item.put("required", cf.isRequired());

            JSONArray options = new JSONArray();
            List<CustomFieldOption> opts = cf.getOptions();
            for (CustomFieldOption opt : opts) {
                JSONObject jopt = new JSONObject();
                jopt.put("id", opt.getId());
                jopt.put("value", opt.getValue() == null ? "" : StringEscapeUtils.escapeHtml(opt.getValue()));
                jopt.put("displayValue", opt.getDisplayValue() == null ? "" : StringEscapeUtils.escapeHtml(opt.getDisplayValue()));
                options.put(jopt);
            }

            item.put("options", options);
            cfs.put(item);

            item.put("cfValue", getTicketCustomFieldValue(ticket, cf));
        }

        response.setContentType("application/json");
        response.getWriter().print(cfs.toString());
        return null;
    }

    private JSONObject getTicketCustomFieldValue(Ticket ticket, CustomField cf) throws JSONException {

        // get the custom field values for this template, if any
        CustomFieldValues cfv = null;
        if (ticket != null) {
            for (CustomFieldValues cfvFound : ticket.getCustomeFieldValues()) {
                if (cfvFound.getCustomField().getId().equals(cf.getId())) {
                    cfv = cfvFound;
                    break;
                }
            }
        }

        JSONObject json = new JSONObject();
        if (cfv != null) {
            json.put("id", cfv.getId());
            json.put("value", cfv.getFieldValue() == null ? "" : StringEscapeUtils.escapeHtml(cfv.getFieldValue()));
            json.put("tid", cfv.getTicket().getId());
            json.put("cfId", cfv.getCustomField().getId());
        } else {
            json.put("id", "");
            json.put("value", "");
            json.put("tid", "");
            json.put("cfId", "");
        }
        return json;
    }

    public ModelAndView getKbArticles(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Locale locale = RequestContextUtils.getLocale(request);
        String searchText = ServletRequestUtils.getStringParameter(request, "kbQuery", "");
        searchText = StringUtils.replace(searchText, "*", "");
        Integer start = ServletRequestUtils.getIntParameter(request, "start", -1);
        Integer count = ServletRequestUtils.getIntParameter(request, "count", -1);

        String sort = ServletRequestUtils.getStringParameter(request, "sort");
        Map<String, Boolean> sortFieldMap = new HashMap<String, Boolean>();
        if(sort != null) {
            boolean direction = true;
            if(StringUtils.startsWith(sort, "-")) {
                direction = false;
            }
            sortFieldMap.put(sort.replace("-", ""), direction);
        } else {
            sortFieldMap.put("id", true);
        }

        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);

        List<KnowledgeBase> kbArticles = knowledgeBaseService.searchKnowledgeBaseByUser(searchText, start, count, sortFieldMap, user);
        List<JSONObject> items = new ArrayList<JSONObject>();
        for(KnowledgeBase kb : kbArticles) {
            items.add(getJSONKbItem(kb, locale));
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("numRows", knowledgeBaseService.getKbArticleCountByUser(searchText, user));
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    private JSONObject getJSONKbItem(KnowledgeBase kb, Locale locale) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("id", kb.getId());
        item.put("group", kb.getGroup().getName());
        item.put("subject", kb.getSubject());
        item.put("isPrivate", kb.getIsPrivate());
        item.put("timesViewed", kb.getTimesViewed());
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
        item.put("date", dateFormat.format(kb.getDate()));
        return item;
    }

    public ModelAndView getAssets(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Locale locale = RequestContextUtils.getLocale(request);
        String searchText = ServletRequestUtils.getStringParameter(request, "label", "");
        searchText = StringUtils.replace(searchText, "*", "");
        Integer start = ServletRequestUtils.getIntParameter(request, "start", -1);
        Integer count = ServletRequestUtils.getIntParameter(request, "count", -1);

        String sort = ServletRequestUtils.getStringParameter(request, "sort");
        Map<String, Boolean> sortFieldMap = new HashMap<String, Boolean>();
        if(sort != null) {
            boolean direction = true;
            if(StringUtils.startsWith(sort, "-")) {
                direction = false;
            }
            sortFieldMap.put(sort.replace("-", ""), direction);
        } else {
            sortFieldMap.put("id", true);
        }

        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);

        List<Asset> assets = assetService.searchAsset(searchText, start, count, sortFieldMap);
        List<JSONObject> items = new ArrayList<JSONObject>();
        for(Asset asset : assets) {
            items.add(getJSONAssetItem(asset, locale));
        }

        JSONObject store = new JSONObject();
        store.put("identifier", "id");
        store.put("numRows", assetService.getAssetCount(searchText));
        store.put("items", items);

        response.setContentType("application/json");
        response.getWriter().print(store.toString());
        return null;
    }

    private JSONObject getJSONAssetItem(Asset asset, Locale locale) throws JSONException {
        JSONObject item = new JSONObject();
        item.put("id", asset.getId());
        item.put("assetNumber", asset.getAssetNumber());
        item.put("name", asset.getName());
        item.put("assetLocation", asset.getAssetLocation());

        String location, group, category, categoryOption, owner;
        location = group = category = categoryOption = owner = "";

        if(asset.getLocation() != null) {
            location = asset.getLocation().getName();
        }
        if(asset.getGroup() != null) {
            group = asset.getGroup().getName();
        }
        if(asset.getCategory() != null) {
            category = asset.getCategory().getName();
        }
        if(asset.getCategoryOption() != null) {
            categoryOption = asset.getCategoryOption().getName();
        }
        if(asset.getOwner() != null) {
            User user = asset.getOwner();
            owner = user.getFirstName() + " " + user.getLastName() + " (" + user.getLoginId() + ")";
        }

        item.put("location", location);
        item.put("group", group);
        item.put("category", category);
        item.put("categoryOption", categoryOption);

        item.put("owner", owner);
        return item;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryOptionService(CategoryOptionService categoryOptionService) {
        this.categoryOptionService = categoryOptionService;
    }

    public void setSoftwareLicenseService(SoftwareLicenseService softwareLicenseService) {
        this.softwareLicenseService = softwareLicenseService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setTicketTemplateService(TicketTemplateService ticketTemplateService) {
        this.ticketTemplateService = ticketTemplateService;
    }

    public void setTemplateMasterService(TemplateMasterService templateMasterService) {
        this.templateMasterService = templateMasterService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setPriorityService(TicketPriorityService priorityService) {
        this.priorityService = priorityService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setAssignmentService(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    public void setCustomFieldsService(CustomFieldsService customFieldsService) {
        this.customFieldsService = customFieldsService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setKnowledgeBaseService(KnowledgeBaseService knowledgeBaseService) {
        this.knowledgeBaseService = knowledgeBaseService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }
}
