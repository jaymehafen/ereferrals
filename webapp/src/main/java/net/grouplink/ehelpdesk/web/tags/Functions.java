package net.grouplink.ehelpdesk.web.tags;

import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.PageContext;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 */
public class Functions {

    protected final Log logger = LogFactory.getLog(getClass());

    public Functions() {
    }

    public static boolean isPDA(String userAgent) {
        Functions f = new Functions();
        f.logger.debug("USER_AGENT: " + userAgent);

        return userAgent.indexOf("Windows CE") > -1 ||
                userAgent.indexOf("Mobile") > -1 ||
                userAgent.indexOf("Treo") > -1 ||
                userAgent.indexOf("Smartphone") > -1 ||
                userAgent.indexOf("BlackBerry") > -1 ||
                userAgent.indexOf("AvantGo") > -1 ||
                userAgent.indexOf("Blazer") > -1 ||
                userAgent.indexOf("IEMobile") > -1 ||
                userAgent.indexOf("Mazingo") > -1 ||
                userAgent.indexOf("240x320") > -1 ||
                userAgent.indexOf("iPod") > -1 ||
                userAgent.indexOf("iPhone") > -1 ||
                userAgent.indexOf("Android") > -1;
    }

    public static boolean isIPodPhone(String userAgent) {
        return userAgent.indexOf("iPod") > -1 ||
                userAgent.indexOf("iPhone") > -1 ||
                userAgent.indexOf("iPad") > -1 ||
                userAgent.indexOf("Android") > -1;
    }

    public static boolean collectionContains(Collection collection, Object o) {
        return collection.contains(o);
    }

    public static boolean arrayContains(Object[] array, Object o) {
        return Arrays.asList(array).contains(o);
    }

    public static boolean mapContainsKey(Map m, Object o) {
        return m.containsKey(o);
    }

    public static boolean mapContainsValue(Map m, Object o) {
        return m.containsValue(o);
    }

    public static String truncateString(String s, int length) {
        if(s.length() > length) {
            return s.substring(0, length) + "...";
        }
        return s;
    }
    
    public static Boolean hasGroupPermission(String permission, Group group, PageContext pageContext) {
        PermissionService permissionService = (PermissionService) WebApplicationContextUtils.getWebApplicationContext(pageContext.getServletContext()).getBean("permissionService");
        return permissionService.hasGroupPermission(permission, group);
    }
}
    