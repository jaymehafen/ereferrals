package net.grouplink.ehelpdesk.web.filters;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.support.OpenSessionInViewFilter;

public class GLOpenSessionInViewFilter extends OpenSessionInViewFilter {
    @Override
    protected Session getSession(SessionFactory sessionFactory) throws DataAccessResourceFailureException {
        Session session = super.getSession(sessionFactory);
        //enabling filter to not show deleted status
        session.enableFilter("deletedTickets").setParameter("deletedId", Integer.valueOf("-1"));
        session.enableFilter("ttPendingTickets").setParameter("ttPendingId", Integer.valueOf("-1"));
        //enabling filter to not show inactive assetes
        session.enableFilter("deletedObject").setParameter("isActive", Boolean.TRUE);

        return session;
    }
}
