package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.common.beans.support.GLRefreshablePagedListHolder;
import net.grouplink.ehelpdesk.domain.LdapUser;
import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.SurveyQuestion;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.LdapFieldService;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.service.RoleService;
import net.grouplink.ehelpdesk.service.StatusService;
import net.grouplink.ehelpdesk.service.SurveyDataService;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.surveys.SurveyItem;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import net.grouplink.ehelpdesk.web.views.TicketListExcelView;
import net.grouplink.ehelpdesk.web.views.TicketListPdfView;
import net.grouplink.ehelpdesk.web.views.TicketPdfView;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author mrollins
 * @version 1.0
 */
public class TicketListController extends MultiActionController {

    private TicketService ticketService;
    private UserRoleGroupService userRoleGroupService;
    private SurveyDataService surveyDataService;
    private LdapService ldapService;
    private UserService userService;
    private TicketPdfView ticketPdfView;
    private TicketListPdfView ticketListPdfView;
    private TicketListExcelView ticketListExcelView;
    private PermissionService permissionService;

    private void processSortToggling(HttpServletRequest request, HttpSession session, GLRefreshablePagedListHolder listHolder) {
        Boolean refreshParent = Boolean.valueOf(String.valueOf(session.getAttribute("refreshParent")));
        if (request.getParameter("pageSize") != null || refreshParent) {
            ((MutableSortDefinition) listHolder.getSort()).setToggleAscendingOnProperty(false);
            session.setAttribute("refreshParent", "false");
        } else {
            if (!StringUtils.isBlank(listHolder.getSort().getProperty())) {
                ((MutableSortDefinition) listHolder.getSort()).setToggleAscendingOnProperty(true);
            }
        }
    }

    public ModelAndView createUserAccountFromLdap(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String dn = ServletRequestUtils.getStringParameter(request, "dn");
        LdapUser ldapUser = ldapService.getLdapUser(dn);

        User user = userService.getUserByUsername(ldapUser.getCn());
        if(user==null || !user.getId().equals(0)){
            user = userService.synchronizedUserWithLdap(user, null, ldapUser);
        }

        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();
        if (user != null) {
            out.print(user.getId() + ";" + user.getJsEscapeFirstName() + " " + user.getJsEscapeLastName());
        } else {
            out.print("loginEmailError");
        }
        return null;
    }

    public ModelAndView saveWorkTime(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        assert response != null;

        User sessionUser = (User) request.getSession(true).getAttribute(SessionConstants.ATTR_USER);
        sessionUser = userService.getUserById(sessionUser.getId());

        String ticketId = ServletRequestUtils.getStringParameter(request, "ticketId");
        String workTime = ServletRequestUtils.getStringParameter(request, "workTime");
        Ticket ticket = ticketService.getTicketById(new Integer(ticketId));
        ticket.setDisplayWorkTime(workTime);
        ticketService.saveTicket(ticket, sessionUser);

        response.setContentType("application/json");
        return null;
    }

    /**
     * Custom handler for tickets Excel document.
     *
     * @param request  current HTTP request
     * @param response current HTTP response
     * @return a ModelAndView to render the response
     * @throws javax.servlet.ServletException when something don't work
     */
    public ModelAndView handleExcel(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        int userId = ServletRequestUtils.getIntParameter(request, "userId", -9999);

        Integer urgId;
        try {
            urgId = Integer.valueOf(request.getParameter("urgId"));
        } catch (NumberFormatException nfe) {
            throw new ServletException("User's role group was not supplied");
        }
        String groupName = getMessageSourceAccessor().getMessage("ticketList.ownedByMe");
        UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(urgId);
        if (urg != null) groupName = urg.getGroup().getName();

        String tlSeshName;
        switch (urgId) {
            case -4:
                tlSeshName = SessionConstants.ATTR_USER_TICKET_LIST + "_" + userId;
                break;
            case -3:
                tlSeshName = SessionConstants.ATTR_USER_TICKET_LIST;
                groupName = getMessageSourceAccessor().getMessage("ticketList.ownedByUser");
                break;
            case -2:
                tlSeshName = SessionConstants.ATTR_ADV_SEARCH_LIST;
                groupName = getMessageSourceAccessor().getMessage("ticketList.fromTicketSearch");
                break;
            case 0:
                tlSeshName = SessionConstants.ATTR_TICKET_LIST;
                break;
            default:
                tlSeshName = SessionConstants.ATTR_TICKET_LIST + "_" + urgId;
                break;
        }

        GLRefreshablePagedListHolder listHolder =
                (GLRefreshablePagedListHolder) request.getSession(true).getAttribute(tlSeshName);

        Locale locale = RequestContextUtils.getLocale(request);

        if (listHolder == null) {
            throw new ServletException("No ticket list found in session");
        }

        listHolder.setLocale(locale);
        listHolder.setSource(listHolder.getSourceProvider().loadList(locale, listHolder.getFilter()));

        int page = listHolder.getPage();
        listHolder.resort();
        listHolder.setPage(page);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("groupName", groupName);
        data.put("tickets", listHolder);

        return new ModelAndView(ticketListExcelView, "data", data);
    }

    /**
     * Custom handler for countries PDF document.
     *
     * @param request  current HTTP request
     * @param response current HTTP response
     * @return a ModelAndView to render the response
     * @throws javax.servlet.ServletException when something don't work
     */
    public ModelAndView handlePdf(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        assert response != null;

        int userId = ServletRequestUtils.getIntParameter(request, "userId", -9999);

        Integer urgId;
        try {
            urgId = Integer.valueOf(request.getParameter("urgId"));
        } catch (NumberFormatException nfe) {
            throw new ServletException("User's role group was not supplied");
        }
        String groupName = getMessageSourceAccessor().getMessage("ticketList.ownedByMe");
        UserRoleGroup urg = userRoleGroupService.getUserRoleGroupById(urgId);
        if (urg != null) groupName = urg.getGroup().getName();

        String tlSeshName;
        switch (urgId) {
            case -4:
                tlSeshName = SessionConstants.ATTR_USER_TICKET_LIST + "_" + userId;
                break;
            case -3:
                tlSeshName = SessionConstants.ATTR_USER_TICKET_LIST;
                groupName = getMessageSourceAccessor().getMessage("ticketList.ownedByUser");
                break;
            case -2:
                tlSeshName = SessionConstants.ATTR_ADV_SEARCH_LIST;
                groupName = getMessageSourceAccessor().getMessage("ticketList.fromTicketSearch");
                break;
            case 0:
                tlSeshName = SessionConstants.ATTR_TICKET_LIST;
                break;
            default:
                tlSeshName = SessionConstants.ATTR_TICKET_LIST + "_" + urgId;
                break;
        }

        GLRefreshablePagedListHolder listHolder =
                (GLRefreshablePagedListHolder) request.getSession().getAttribute(tlSeshName);

        Locale locale = RequestContextUtils.getLocale(request);

        if (listHolder == null) {
            throw new ServletException("No ticket list found in session");
        }

        listHolder.setLocale(locale);
        listHolder.setSource(listHolder.getSourceProvider().loadList(locale, listHolder.getFilter()));

        int page = listHolder.getPage();
        listHolder.resort();
        listHolder.setPage(page);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("groupName", groupName);
        data.put("tickets", listHolder);

        return new ModelAndView(ticketListPdfView, "data", data);
    }


    /**
     * Custom handler for countries PDF document.
     *
     * @param request  current HTTP request
     * @param response current HTTP response
     * @return a ModelAndView to render the response
     * @throws javax.servlet.ServletException when something don't work
     */
    public ModelAndView handleSinglePdf(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        assert response != null;
        Map<String, Object> data = new HashMap<String, Object>();
        Integer ticketId;
        Ticket ticket = null;

        try {
            ticketId = Integer.valueOf(request.getParameter("ticketId"));
        } catch (NumberFormatException nfe) {
            throw new ServletException("Ticket ID was not provided");
        }

        if (ticketId != null)
            ticket = ticketService.getTicketById(ticketId);

        // check if user has view ticket permission
        if (!permissionService.canUserViewTicket(userService.getLoggedInUser(), ticket)) {
            throw new AccessDeniedException("Access denied");
        }

        Locale locale = RequestContextUtils.getLocale(request);

        data.put("ticket", ticket);
        data.put("locale", locale);

        List<SurveyItem> surveyItems = new ArrayList<SurveyItem>();
        Boolean surveyCompleted = Boolean.FALSE;
        List<SurveyData> surveyDataList = surveyDataService.getByTicketId(ticketId);
        if ((surveyDataList != null) && (surveyDataList.size() > 0)) {
            surveyCompleted = Boolean.TRUE;
            for (SurveyData surveyData : surveyDataList) {
                String value = surveyData.getValue();
                SurveyQuestion surveyQuestion = surveyData.getSurveyQuestion();
                String question = surveyQuestion.getSurveyQuestionText();
                SurveyItem surveyItem = new SurveyItem(question, value);
                surveyItems.add(surveyItem);
            }
        }
        data.put("surveyCompleted", surveyCompleted);
        data.put("surveyItems", surveyItems);

        return new ModelAndView(ticketPdfView, "data", data);
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setSurveyDataService(SurveyDataService surveyDataService) {
        this.surveyDataService = surveyDataService;
    }

    public void setTicketListPdfView(TicketListPdfView ticketListPdfView) {
        this.ticketListPdfView = ticketListPdfView;
    }

    public void setTicketListExcelView(TicketListExcelView ticketListExcelView) {
        this.ticketListExcelView = ticketListExcelView;
    }

    public void setTicketPdfView(TicketPdfView ticketPdfView) {
        this.ticketPdfView = ticketPdfView;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
