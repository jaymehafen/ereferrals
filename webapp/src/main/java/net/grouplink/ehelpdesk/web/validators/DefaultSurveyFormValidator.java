package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.domain.SurveyData;
import net.grouplink.ehelpdesk.domain.SurveyDataCollection;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class DefaultSurveyFormValidator implements Validator {
    public boolean supports(Class<?> clazz) {
        return clazz.equals(SurveyDataCollection.class);
    }

    public void validate(Object target, Errors errors) {
        SurveyDataCollection surveyDataCollection = (SurveyDataCollection) target;
        // default survey has 3 questions
        for (int i=0; i<3; i++) {
            SurveyData surveyData = surveyDataCollection.getSurveyData(i);
            if (StringUtils.isBlank(surveyData.getValue())) {
                errors.reject("surveys.validate.pleaseAnswerAllSurveyQuestions", "Please answer all survey questions");
                return;
            }
        }
    }
}
