package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.CustomFieldType;
import net.grouplink.ehelpdesk.service.CustomFieldTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 10/12/11
 * Time: 4:45 PM
 */
public class CustomFieldTypeConverter implements Converter<String, CustomFieldType> {
    @Autowired CustomFieldTypesService customFieldTypesService;

    public CustomFieldType convert(String id) {
        try {
            int customFieldTypeId = Integer.parseInt(id);
            return customFieldTypesService.getCustomFieldType(customFieldTypeId);
        } catch (Exception e) {
            return null;
        }
    }
}
