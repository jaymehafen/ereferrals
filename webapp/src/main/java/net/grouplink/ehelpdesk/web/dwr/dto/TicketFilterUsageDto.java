package net.grouplink.ehelpdesk.web.dwr.dto;

import net.grouplink.ehelpdesk.domain.MyTicketTab;
import net.grouplink.ehelpdesk.domain.Report;
import net.grouplink.ehelpdesk.domain.ticket.TicketFilter;

import java.io.Serializable;
import java.util.List;

public class TicketFilterUsageDto implements Serializable {
    private List<MyTicketTab> tabs;
    private List<Report> reports;

    public List<MyTicketTab> getTabs() {
        return tabs;
    }

    public void setTabs(List<MyTicketTab> tabs) {
        this.tabs = tabs;
    }

    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }
}
