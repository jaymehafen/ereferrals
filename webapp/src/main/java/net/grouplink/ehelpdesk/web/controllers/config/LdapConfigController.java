package net.grouplink.ehelpdesk.web.controllers.config;

import net.grouplink.ehelpdesk.common.utils.ldap.LdapConfig;
import net.grouplink.ehelpdesk.service.LdapService;
import net.grouplink.ehelpdesk.web.beans.ApplicationInitializer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class LdapConfigController extends SimpleFormController {

    private LdapService ldapService;
    private ApplicationInitializer applicationInit;
    protected final Log logger = LogFactory.getLog(getClass());

    public LdapConfigController() {
        super();
        setCommandClass(LdapConfig.class);
        setCommandName("ldap");
        setFormView("ldapConfig");
        setSuccessView("ldapConfig.glml");
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        return ldapService.getLdapConfig();
    }

    public ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        LdapConfig ldap = (LdapConfig) command;

        if (!ldap.getLdapIntegration()) {
            ldapService.saveLdapConfig(new LdapConfig());
            applicationInit.ldapInit();
            request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("ldapconfig.save.success"));
            return new ModelAndView(new RedirectView(getSuccessView()));
        }

        if (StringUtils.isNotBlank(ldap.getChangePassword())) {
            ldap.setPassword(ldap.getChangePassword());
        }

        List<String> dnsToDelete = Arrays.asList(ServletRequestUtils.getStringParameters(request, "inclDnsToDelete"));
        if(!dnsToDelete.isEmpty()) {
            Iterator<String> iterator = ldap.getBaseSearchList().iterator();
            while(iterator.hasNext()) {
                if(dnsToDelete.contains(iterator.next())) {
                    iterator.remove();
                }
            }
        }

        return super.processFormSubmission(request, response, command, errors);
    }

    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        LdapConfig ldap = (LdapConfig) command;
        ldapService.saveLdapConfig(ldap);
        applicationInit.ldapInit();
        request.setAttribute("flash.success", getMessageSourceAccessor().getMessage("ldapconfig.save.success"));
        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    public void afterPropertiesSet() throws Exception {
        if (ldapService == null)
            throw new BeanCreationException("Must set ldapService on "
                    + getClass().getName());
    }

    public void setApplicationInitializer(ApplicationInitializer applicationInit) {
        this.applicationInit = applicationInit;
    }

    public void setLdapService(LdapService ldapService) {
        this.ldapService = ldapService;
    }

}