package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.web.domain.AssetImport;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.springframework.validation.Errors;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Aug 22, 2008
 * Time: 6:37:21 PM
 */
public class AssetImportValidator extends BaseValidator {

    /**
     * Can this {@link org.springframework.validation.Validator} {@link #validate(Object, org.springframework.validation.Errors) validate}
     * instances of the supplied <code>clazz</code>?
     * <p>This method is <i>typically</i> implemented like so:
     * <pre class="code">return Foo.class.isAssignableFrom(clazz);</pre>
     * (Where <code>Foo</code> is the class (or superclass) of the actual
     * object instance that is to be {@link #validate(Object, org.springframework.validation.Errors) validated}.)
     *
     * @param clazz the {@link Class} that this {@link org.springframework.validation.Validator} is
     *              being asked if it can {@link #validate(Object, org.springframework.validation.Errors) validate}
     * @return <code>true</code> if this {@link org.springframework.validation.Validator} can indeed
     *         {@link #validate(Object, org.springframework.validation.Errors) validate} instances of the
     *         supplied <code>clazz</code>
     */
    public boolean supports(Class clazz) {
        return clazz.equals(AssetImport.class);
    }

    /**
     * Validate the supplied <code>target</code> object, which must be
     * of a {@link Class} for which the {@link #supports(Class)} method
     * typically has (or would) return <code>true</code>.
     * <p>The supplied {@link org.springframework.validation.Errors errors} instance can be used to report
     * any resulting validation errors.
     *
     * @param target the object that is to be validated (can be <code>null</code>)
     * @param errors contextual state about the validation process (never <code>null</code>)
     * @see org.springframework.validation.ValidationUtils
     */
    public void validate(Object target, Errors errors) {
        AssetImport assetImport = (AssetImport) target;
        checkForDups(assetImport.getImportMap(), errors);
    }

    private void checkForDups(Map importMap, Errors errors){
        Map countMap = new HashMap(); // for counting the number of importMap values
        for (Iterator iterator = importMap.keySet().iterator(); iterator.hasNext();) {
            String key = (String)iterator.next();
            String mapValue = (String)importMap.get(key);

            // Check if this value is already in the countMap
            if(countMap.containsKey(mapValue)){
                // get the count for this value and increment
                int cnt = ((Integer)countMap.get(mapValue)).intValue();
                countMap.put(mapValue, new Integer(cnt+1));
            } else {
                // add to countmap with count 1
                countMap.put(mapValue, new Integer(1));
            }
        }

        for (Iterator iterator = countMap.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry entry = (Map.Entry) iterator.next();
            String name = (String)entry.getKey();
            Integer count = (Integer)entry.getValue();

            if(count.intValue() >1){ // add to error message if greater than 1
                errors.reject(name, new String[]{"("+count+")"}, name+" {0}");
            }
        }
    }
}
