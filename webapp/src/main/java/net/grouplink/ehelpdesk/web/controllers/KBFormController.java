package net.grouplink.ehelpdesk.web.controllers;

import net.grouplink.ehelpdesk.domain.Attachment;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.KnowledgeBase;
import net.grouplink.ehelpdesk.domain.Ticket;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.UserRoleGroup;
import net.grouplink.ehelpdesk.service.AttachmentService;
import net.grouplink.ehelpdesk.service.GroupService;
import net.grouplink.ehelpdesk.service.KnowledgeBaseService;
import net.grouplink.ehelpdesk.service.TicketService;
import net.grouplink.ehelpdesk.service.UserRoleGroupService;
import net.grouplink.ehelpdesk.web.propertyeditors.GroupEditor;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author mrollins
 * @version 1.0
 */
public class KBFormController extends SimpleFormController {

    private KnowledgeBaseService knowledgeBaseService;
    private UserRoleGroupService userRoleGroupService;
    private GroupService groupService;
    private TicketService ticketService;
    private AttachmentService attachmentService;

    public KBFormController() {
        super();
        setCommandClass(KnowledgeBase.class);
        setCommandName("kb");
        setFormView("kbEdit");
        setSuccessView("kbView.glml");
    }

    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
        super.initBinder(request, binder);
        binder.registerCustomEditor(Group.class, new GroupEditor(groupService));
    }

    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        String kbId = ServletRequestUtils.getStringParameter(request, "kbId");
        if (StringUtils.isNotBlank(kbId)) {
            return this.knowledgeBaseService.getKnowledgeBaseById(new Integer(kbId));
        } else {
            return new KnowledgeBase();
        }
    }

    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        User user = (User) request.getSession().getAttribute(SessionConstants.ATTR_USER);

        List<Group> groupList = new ArrayList<Group>();
        if (user.getLoginId().equals(User.ADMIN_LOGIN)) {
            groupList = groupService.getGroups();
        } else {
            List<UserRoleGroup> urgList = userRoleGroupService.getUserRoleGroupByUserId(user.getId());
            for (UserRoleGroup anUrgList : urgList) {
                groupList.add(anUrgList.getGroup());
            }
        }

        //Converting ticket to KB
        String ticketId = ServletRequestUtils.getStringParameter(request, "ticketId");
        if (StringUtils.isNotBlank(ticketId)) {
            Ticket ticket = ticketService.getTicketById(Integer.valueOf(ticketId));
//            if (ticket.getKnowledgeBase().isEmpty()) {
            model.put("ticket", ticket);
//            }
            
            request.getSession(true).setAttribute("showHeader", Boolean.TRUE);
        }

        model.put("groupList", groupList);
        return model;
    }

    public ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        KnowledgeBase kb = (KnowledgeBase) command;
	    if (request.getParameter("cancel") != null) {
            if(kb.getId() == null){
                return new ModelAndView(new RedirectView("/kbManagement/kbManagement.glml", true));
            }
            else {
                return new ModelAndView(new RedirectView(getSuccessView() + "?kbId=" + kb.getId()));
            }
        }
        
        return super.processFormSubmission(request, response, command, errors);
    }
    
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
        KnowledgeBase kb = (KnowledgeBase) command;
        kb.setDate(new Date());

        @SuppressWarnings("unchecked")
        List<Object> attchList = kb.getAttchmnt();
        Set<Attachment> attchments = new HashSet<Attachment>();
        if(kb.getAttachments() != null){
            attchments.addAll(kb.getAttachments());
        }

        for (Object o : attchList) {
            MultipartFile mpf = (MultipartFile)o;
            if (mpf.getSize() != 0) {
                Attachment attch = new Attachment();
                attch.setContentType(mpf.getContentType());
                attch.setFileData(mpf.getBytes());
                attch.setFileName(mpf.getOriginalFilename());
                attchments.add(attch);
            }
        }

        kb.getAttachments().clear();
        kb.getAttachments().addAll(attchments);

        //Attaching Ticket to KB
        String ticketId = ServletRequestUtils.getStringParameter(request, "ticketId");
        if(StringUtils.isNotBlank(ticketId)){
            Ticket ticket = ticketService.getTicketById(Integer.valueOf(ticketId));
            ticket.addKnowledgeBase(kb);
            kb.addTicket(ticket);
            for (Attachment attachment : ticket.getAttachments()) {
                // reinitialize attachment to avoid lazy property exception
                attachment = attachmentService.getById(attachment.getId());
                Attachment kbAttch = new Attachment();
                kbAttch.setContentType(attachment.getContentType());
                kbAttch.setFileData(attachment.getFileData());
                kbAttch.setFileName(attachment.getFileName());
                kb.getAttachments().add(kbAttch);
            }
        }

        this.knowledgeBaseService.saveKnowledgeBase(kb);
        return new ModelAndView(new RedirectView(getSuccessView() + "?kbId=" + kb.getId()));
    }

    public void setKnowledgeBaseService(KnowledgeBaseService knowledgeBaseService) {
        this.knowledgeBaseService = knowledgeBaseService;
    }

    public void setUserRoleGroupService(UserRoleGroupService userRoleGroupService) {
        this.userRoleGroupService = userRoleGroupService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public void setAttachmentService(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }
}
