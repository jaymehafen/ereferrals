package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.Address;
import net.grouplink.ehelpdesk.service.AddressService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class AddressFormatter implements Formatter<Address> {
    @Autowired
    private AddressService addressService;

    public Address parse(String text, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(text)) {
            return addressService.getById(Integer.valueOf(text));
        } else {
            return null;
        }
    }

    public String print(Address object, Locale locale) {
        return object == null ? "" : object.getId().toString();
    }
}
