package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetFieldGroup;
import net.grouplink.ehelpdesk.service.asset.AssetFieldGroupService;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * User: jaymehafen
 * Date: Sep 24, 2008
 * Time: 6:18:35 PM
 */
public class AssetFieldGroupValidator extends BaseValidator {
    private AssetFieldGroupService assetFieldGroupService;

    public boolean supports(Class clazz) {
        return clazz.equals(AssetFieldGroup.class);
    }

    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");
        AssetFieldGroup acf = (AssetFieldGroup) target;
        if (StringUtils.isNotBlank(acf.getName())) {
            AssetFieldGroup a = assetFieldGroupService.getByName(acf.getName());
            if (a != null && (acf.getId() == null || !acf.getId().equals(a.getId()))) {
                errors.rejectValue("name", "asset.fieldGroup.notUnique", "The Field Group Name already exists");
            }
        }
    }

    public void setAssetFieldGroupService(AssetFieldGroupService assetFieldGroupService) {
        this.assetFieldGroupService = assetFieldGroupService;
    }
}