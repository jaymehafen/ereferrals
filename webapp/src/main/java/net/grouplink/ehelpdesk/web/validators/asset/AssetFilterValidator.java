package net.grouplink.ehelpdesk.web.validators.asset;

import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.web.validators.BaseValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * Created by IntelliJ IDEA.
 * User: mark
 * Date: Sep 18, 2008
 * Time: 4:57:31 PM
 */
public class AssetFilterValidator extends BaseValidator{

    /**
     * Can this {@link org.springframework.validation.Validator} {@link #validate(Object, org.springframework.validation.Errors) validate}
     * instances of the supplied <code>clazz</code>?
     * <p>This method is <i>typically</i> implemented like so:
     * <pre class="code">return Foo.class.isAssignableFrom(clazz);</pre>
     * (Where <code>Foo</code> is the class (or superclass) of the actual
     * object instance that is to be {@link #validate(Object, org.springframework.validation.Errors) validated}.)
     *
     * @param clazz the {@link Class} that this {@link org.springframework.validation.Validator} is
     *              being asked if it can {@link #validate(Object, org.springframework.validation.Errors) validate}
     * @return <code>true</code> if this {@link org.springframework.validation.Validator} can indeed
     *         {@link #validate(Object, org.springframework.validation.Errors) validate} instances of the
     *         supplied <code>clazz</code>
     */
    public boolean supports(Class clazz) {
        return clazz.equals(AssetFilter.class);
    }

    /**
     * Validate the supplied <code>target</code> object, which must be
     * of a {@link Class} for which the {@link #supports(Class)} method
     * typically has (or would) return <code>true</code>.
     * <p>The supplied {@link org.springframework.validation.Errors errors} instance can be used to report
     * any resulting validation errors.
     *
     * @param target the object that is to be validated (can be <code>null</code>)
     * @param errors contextual state about the validation process (never <code>null</code>)
     * @see org.springframework.validation.ValidationUtils
     */
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "name", "global.fieldRequired", "This field cannot be empty");

        AssetFilter assetFilter = (AssetFilter) target;
        String[] colOrder = assetFilter.getColumnOrder();
        if (colOrder == null || colOrder.length == 0) {
            errors.rejectValue("columnOrder", "ticketFilter.colOrderError", "At least one column should be displayed.");
        }
    }
}
