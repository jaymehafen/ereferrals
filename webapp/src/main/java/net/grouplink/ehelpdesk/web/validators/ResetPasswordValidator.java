package net.grouplink.ehelpdesk.web.validators;

import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.web.domain.ResetPassword;
import org.springframework.validation.Errors;

public class ResetPasswordValidator extends BaseValidator {

	private UserService userService;
	
	public boolean supports(Class clazz) {
		return ResetPassword.class.equals(clazz);
	}
	
	public void validate(Object target, Errors errors) {
		ResetPassword resetPassword = (ResetPassword)target;
		String email = resetPassword.getEmail();
        if (userService.getUserByEmail(email) == null) {
            errors.rejectValue("email", "resetPassword.validate.notfound", "Email address not found");
        }
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
