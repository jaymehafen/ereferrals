package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Category;
import net.grouplink.ehelpdesk.domain.CategoryOption;
import net.grouplink.ehelpdesk.service.CategoryOptionService;
import net.grouplink.ehelpdesk.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by IntelliJ IDEA.
 * User: mrollins
 * Date: 11/8/11
 * Time: 1:11 PM
 */
public class CategoryOptionConverter implements Converter<String, CategoryOption> {
    @Autowired
    private CategoryOptionService categoryOptionService;

    public CategoryOption convert(String id){
        try{
            int categoryOptionId = Integer.parseInt(id);
            return categoryOptionService.getCategoryOptionById(categoryOptionId);
        } catch (Exception e){
            return null;
        }
    }
}
