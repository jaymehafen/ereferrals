package net.grouplink.ehelpdesk.web.domain;

import net.grouplink.ehelpdesk.domain.Status;

import java.util.List;

public class AppCustomization {
    
    private String portalUrl;
	private String marqueeText;
//    private Boolean enableAssetTracker;
    private Status initialStatus;
    private Status ticketPoolStatus;
    private Status closedStatus;
    private List customStatus;
    private List customPriorities;
    private Integer sessionTimeoutLength;

    public String getMarqueeText() {
		return marqueeText;
	}
	public void setMarqueeText(String marqueeText) {
		this.marqueeText = marqueeText;
	}
	public String getPortalUrl() {
		return portalUrl;
	}
	public void setPortalUrl(String portalUrl) {
		this.portalUrl = portalUrl;
	}

    public List getCustomStatus() {
        return customStatus;
    }

    public void setCustomStatus(List customStatus) {
        this.customStatus = customStatus;
    }

    public Status getInitialStatus() {
        return initialStatus;
    }

    public void setInitialStatus(Status initialStatus) {
        this.initialStatus = initialStatus;
    }

    public Status getTicketPoolStatus() {
        return ticketPoolStatus;
    }

    public void setTicketPoolStatus(Status ticketPoolStatus) {
        this.ticketPoolStatus = ticketPoolStatus;
    }

    public Status getClosedStatus() {
        return closedStatus;
    }

    public void setClosedStatus(Status closedStatus) {
        this.closedStatus = closedStatus;
    }

    public List getCustomPriorities() {
        return customPriorities;
    }

    public void setCustomPriorities(List customPriorities) {
        this.customPriorities = customPriorities;
    }

    public Integer getSessionTimeoutLength() {
        return sessionTimeoutLength;
    }

    public void setSessionTimeoutLength(Integer length) {
        this.sessionTimeoutLength = length;
    }
}
