package net.grouplink.ehelpdesk.web.converters;

import net.grouplink.ehelpdesk.domain.Status;
import net.grouplink.ehelpdesk.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

/**
 * author: zpearce
 * Date: 11/23/11
 * Time: 4:51 PM
 */
public class StatusConverter implements Converter<String, Status> {
    @Autowired private StatusService statusService;

    public Status convert(String id) {
        try {
            int sid = Integer.parseInt(id);
            return statusService.getStatusById(sid);
        } catch (Exception e) {
            return null;
        }
    }
}
