package net.grouplink.ehelpdesk.web.controllers;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ChartBuilderException;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.GroupLayout;
import ar.com.fdvs.dj.domain.entities.DJGroup;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import ar.com.fdvs.dj.util.PropertiesMap;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.AssetCustomFieldValue;
import net.grouplink.ehelpdesk.domain.asset.AssetFilter;
import net.grouplink.ehelpdesk.domain.asset.AssetReportGroupByOptions;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.asset.AssetFilterService;
import net.grouplink.ehelpdesk.service.asset.AssetService;
import net.grouplink.ehelpdesk.web.controllers.asset.AssetFilterFormController;
import net.grouplink.ehelpdesk.web.utils.SessionConstants;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.jasperreports.GLJasperReportsMultiFormatView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.Color;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Date: May 22, 2008
 * Time: 10:37:04 AM
 */
public class JasperReportController extends MultiActionController {

    private Log log = LogFactory.getLog(JasperReportController.class);

    private UserService userService;
    private AssetService assetService;
    private AssetFilterService assetFilterService;
    private GLJasperReportsMultiFormatView assetsByOwnerReport;
    private GLJasperReportsMultiFormatView assetsByGroupReport;
    private GLJasperReportsMultiFormatView assetsByTypeReport;
    private GLJasperReportsMultiFormatView assetLeaseExpirationReport;
    private GLJasperReportsMultiFormatView assetWarrantyExpirationReport;
    private GLJasperReportsMultiFormatView assetPurchaseReport;

    public ModelAndView dynamicAssetReport(HttpServletRequest request, HttpServletResponse response) throws ColumnBuilderException, ChartBuilderException, JRException, ClassNotFoundException, IOException, ServletRequestBindingException {
        assert response != null;
        HttpSession session = request.getSession(true);

        User user = (User) session.getAttribute(SessionConstants.ATTR_USER);
        user = userService.getUserById(user.getId());

        AssetFilter af;
        // check if filterId is passed as query param
        Integer filterId = ServletRequestUtils.getIntParameter(request, "filterId");
        if (filterId != null) {
            af = assetFilterService.getById(filterId);
        } else {
            af = (AssetFilter) session.getAttribute(AssetFilterFormController.AFC_FORM_ASSETFILTER);
        }

        List assets = assetFilterService.getAssets(af, user);

        JasperPrint jasperPrint = buildDynamicReport(af, assets);

        if (af.getFormat().equals("html")) {
            response.setContentType("text/html");
            request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
            JRHtmlExporter htmlExporter = new JRHtmlExporter();
            htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            htmlExporter.setParameter(JRExporterParameter.OUTPUT_WRITER, response.getWriter());
            htmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath() + "/images/report?image=");

            htmlExporter.exportReport();
        } else if (af.getFormat().equals("pdf")) {
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline; filename=assets.pdf");
            JRPdfExporter pdfExporter = new JRPdfExporter();
            pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
            pdfExporter.exportReport();
        } else if (af.getFormat().equals("xls")) {
            response.setContentType("application/xls");
            response.setHeader("Content-Disposition", "inline; filename=assets.xls");
            JExcelApiExporter xlsExporter = new JExcelApiExporter();
            xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            xlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
            xlsExporter.exportReport();
        } else if (af.getFormat().equals("csv")) {
            response.setContentType("text/csv");
            response.setHeader("Content-Disposition", "inline; filename=assets.csv");
            JRCsvExporter csvExporter = new JRCsvExporter();
            csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            csvExporter.setParameter(JRExporterParameter.OUTPUT_WRITER, response.getWriter());
            csvExporter.exportReport();
        }

        return null;
    }

    private JasperPrint buildDynamicReport(AssetFilter assetFilter, List assets) throws ColumnBuilderException, ClassNotFoundException, ChartBuilderException, JRException {
        FastReportBuilder frb = new FastReportBuilder();

        frb.setTitle(assetFilter.getName());
        frb.setPrintBackgroundOnOddRows(true);
//        Style oddRowBgStyle = new Style();
//        oddRowBgStyle.setBackgroundColor(Color.LIGHT_GRAY);
//        frb.setOddRowBackgroundStyle(oddRowBgStyle);
        frb.setSubtitle("Generated by everything Helpdesk - " + new Date());
        frb.setUseFullPageWidth(true);
        frb.setWhenNoDataBlankPage();

        Map<String, AbstractColumn> columns = new HashMap<String, AbstractColumn>();
        String[] columnOrder = assetFilter.getColumnOrder();
        columnOrder = reorderColumnsByGroupBy(columnOrder, assetFilter.getGroupBy());

        Font columnFont = new Font(8, "Arial", false, false, false);
//        columnFont.setFontSize(6);
        Style columnStyle = Style.createBlankStyle("columnStyle");
        columnStyle.setFont(columnFont);

        for (final String col : columnOrder) {
            AbstractColumn djColumn;
            if (col.equals("asset.assetNumber")) {
                djColumn = buildAbstractColumn("Asset #", "assetNumber", String.class.getName(), 30, columnStyle);

            } else if (col.equals("asset.name")) {
                djColumn = buildAbstractColumn("Name", "name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.type")) {
                djColumn = buildAbstractColumn("Type", "assetType.name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.owner")) {
                frb.addField("owner", User.class.getName());
                djColumn = ColumnBuilder.getInstance()
                        .setTitle("Owner")
                        .setCustomExpression(new CustomExpression() {

                            public Object evaluate(Map fields, Map variables, Map parameters) {
                                User u = (User) fields.get("owner");
                                if (u == null) {
                                    return "None";
                                } else {
                                    return u.getLastName() + ", " + u.getFirstName();
                                }
                            }

                            public String getClassName() {
                                return String.class.getName();
                            }

                            public Object evaluate(Object object) {
                                PropertiesMap pm = (PropertiesMap) object;
                                User u = (User) pm.get("owner");
                                if (u == null) {
                                    return "None";
                                } else {
                                    return u.getLastName() + ", " + u.getFirstName();
                                }
                            }
                        })
                        .setWidth(30)
                        .setStyle(columnStyle)
                        .build();
            } else if (col.equals("asset.vendor")) {
                djColumn = buildAbstractColumn("Vendor", "vendor.name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.notes")) {
                djColumn = buildAbstractColumn("Notes", "notes", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.assetlocation")) {
                djColumn = buildAbstractColumn("Asset Location", "assetLocation", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.acquisitionDate")) {
                djColumn = buildAbstractColumn("Acquisition Date", "acquisitionDate", Date.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.location")) {
                djColumn = buildAbstractColumn("Location", "location.name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.group")) {
                djColumn = buildAbstractColumn("Group", "group.name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.category")) {
                djColumn = buildAbstractColumn("Category", "category.name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.categoryoption")) {
                djColumn = buildAbstractColumn("Cat. Option", "categoryOption.name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.manufacturer")) {
                djColumn = buildAbstractColumn("Manufacturer", "manufacturer", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.modelNumber")) {
                djColumn = buildAbstractColumn("Model #", "modelNumber", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.serialnumber")) {
                djColumn = buildAbstractColumn("Serial #", "serialNumber", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.description")) {
                djColumn = buildAbstractColumn("Description", "description", String.class.getName(), 30, columnStyle);
            } else if (col.equals("asset.status")) {
                djColumn = buildAbstractColumn("Status", "status.name", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.ponumber")) {
                djColumn = buildAbstractColumn("PO #", "accountingInfo.poNumber", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.accountnumber")) {
                djColumn = buildAbstractColumn("Account #", "accountingInfo.accountNumber", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.accumulatednumber")) {
                djColumn = buildAbstractColumn("Accum. #", "accountingInfo.accumulatedNumber", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.expensenumber")) {
                djColumn = buildAbstractColumn("Expense #", "accountingInfo.expenseNumber", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.acquisitionvalue")) {
                djColumn = buildAbstractColumn("Acquis. Value", "accountingInfo.acquisitionValue", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.leasenumber")) {
                djColumn = buildAbstractColumn("Lease #", "accountingInfo.leaseNumber", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.leaseduration")) {
                djColumn = buildAbstractColumn("Lease Duration", "accountingInfo.leaseDuration", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.leasefrequency")) {
                djColumn = buildAbstractColumn("Lease Frequency", "accountingInfo.leaseFrequency", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.leasefrequencyprice")) {
                djColumn = buildAbstractColumn("Lease Freq. Price", "accountingInfo.leaseFrequencyPrice", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.disposalmethod")) {
                djColumn = buildAbstractColumn("Disposal Method", "accountingInfo.disposalMethod", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.disposaldate")) {
                djColumn = buildAbstractColumn("Disposal Date", "accountingInfo.disposalDate", Date.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.insurancecategory")) {
                djColumn = buildAbstractColumn("Insurance Cat.", "accountingInfo.insuranceCategory", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.replacementvaluecategory")) {
                djColumn = buildAbstractColumn("Replacement Value Cat.", "accountingInfo.replacementValueCategory", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.replacementvalue")) {
                djColumn = buildAbstractColumn("Replacement Value", "accountingInfo.replacementValue", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.maintenancecost")) {
                djColumn = buildAbstractColumn("Maint. Cost", "accountingInfo.maintenanceCost", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.depreciationmethod")) {
                djColumn = buildAbstractColumn("Depr. Method", "accountingInfo.depreciationMethod", String.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.leaseexpirationdate")) {
                djColumn = buildAbstractColumn("Lease Exp. Date", "accountingInfo.leaseExpirationDate", Date.class.getName(), 30, columnStyle);
            } else if (col.equals("accountinginfo.warrantydate")) {
                djColumn = buildAbstractColumn("Warranty Date", "accountingInfo.warrantyDate", Date.class.getName(), 30, columnStyle);
            } else {
                frb.addField("customFieldValues", Set.class.getName());
                djColumn = ColumnBuilder.getInstance()
                        .setTitle(col)
                        .setCustomExpression(new CustomExpression() {


                            public Object evaluate(Map fields, Map variables, Map parameters) {
                                Set<AssetCustomFieldValue> customFieldValues = (Set<AssetCustomFieldValue>) fields.get("customFieldValues");
                                String value = null;
                                for (AssetCustomFieldValue assetCustomFieldValue : customFieldValues) {
                                    if (assetCustomFieldValue.getCustomField().getName().equals(col)) {
                                        value = assetCustomFieldValue.getFieldValue();
                                        break;
                                    }
                                }

                                return (value != null) ? value : "";
                            }

                            public String getClassName() {
                                return String.class.getName();
                            }

                            public Object evaluate(Object object) {
                                PropertiesMap pm = (PropertiesMap) object;
                                @SuppressWarnings("unchecked")
                                Set<AssetCustomFieldValue> customFieldValues = (Set<AssetCustomFieldValue>) pm.get("customFieldValues");
                                String value = null;
                                for (AssetCustomFieldValue assetCustomFieldValue : customFieldValues) {
                                    if (assetCustomFieldValue.getCustomField().getName().equals(col)) {
                                        value = assetCustomFieldValue.getFieldValue();
                                        break;
                                    }
                                }

                                return (value != null) ? value : "";
                            }
                        })
                        .setWidth(30)
                        .setStyle(columnStyle)
                        .build();
            }

            if (djColumn != null) {
                columns.put(col, djColumn);
                frb.addColumn(djColumn);
            }
        }

        if (assetFilter.getGroupBy() != null && assetFilter.getGroupBy() != 0) {
            GroupBuilder gb1 = new GroupBuilder();
            AbstractColumn groupByDJColumn = columns.get(getColumnKeyForGroupBy(assetFilter.getGroupBy()));
            Style groupStyle = new Style();
            groupStyle.setFont(new Font(16, Font._FONT_VERDANA, true));
            groupStyle.setTextColor(Color.BLACK);
//            groupStyle.setBackgroundColor(Color.MAGENTA);
            groupByDJColumn.setStyle(groupStyle);
//            ColumnsGroupVariable cgv = new ColumnsGroupVariable(groupByDJColumn, ColumnsGroupVariableOperation.COUNT);
            GroupLayout g1Layout = new GroupLayout(true, false, false, true, false);

            DJGroup g1 = gb1.setCriteriaColumn((PropertyColumn) groupByDJColumn)
//                        .addHeaderVariable()
//                    .addFooterVariable(cgv)
                    .setGroupLayout(g1Layout)
//                    .setDefaultHeaderVariableStyle(groupStyle)
                    .build();

            frb.addGroup(g1);
        }

//        DJChartBuilder cb = new DJChartBuilder();
//        DJChart chart = cb.addType(DJChart.BAR_CHART)
//                .addColumnsGroup(g1)
//                .addOperation(DJChart.CALCULATION_COUNT)
//                .addColumn(columnType)
//                .build();
//        frb.addChart(chart);

        DynamicReport dr = frb.build();

//        Collections.sort(assets, new Comparator() {
//            public int compare(Object o1, Object o2) {
//                Asset a1 = (Asset) o1;
//                Asset a2 = (Asset) o2;
//                 sort first by owner, then by type
//                if (a1.getOwner() == null && a2.getOwner() == null) {
//                    return 0;
//                } else if (a1.getOwner() == null && a2.getOwner() != null) {
//                    return 1;
//                } else if (a1.getOwner() != null && a2.getOwner() == null) {
//                    return -1;
//                } else {
//                    String u1Name = a1.getOwner().getLastName() + ", " + a1.getOwner().getFirstName();
//                    String u2Name = a2.getOwner().getLastName() + ", " + a2.getOwner().getFirstName();
//                    return u1Name.compareToIgnoreCase(u2Name);
//                }
//            }
//        });

        JRDataSource ds = new JRBeanCollectionDataSource(assets);
        //        JasperExportManager.exportReportToPdfFile(jp, getBaseDir() + "src/main/webapp/WEB-INF/reports/dynamic_assets.pdf");
        return DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);
    }

    private String[] reorderColumnsByGroupBy(String[] columnOrder, Integer groupBy) {
        String colKey = getColumnKeyForGroupBy(groupBy);
        if (colKey != null) {
            columnOrder = positionGroupByColumnFirst(columnOrder, colKey);
        }

        return columnOrder;
    }

    private String getColumnKeyForGroupBy(Integer groupBy) {
        String key = null;
        if (groupBy.equals(AssetReportGroupByOptions.CATEGORY)) {
            key = "asset.category";
        } else if (groupBy.equals(AssetReportGroupByOptions.CATEGORY_OPTION)) {
            key = "asset.categoryoption";
        } else if (groupBy.equals(AssetReportGroupByOptions.GROUP)) {
            key = "asset.group";
        } else if (groupBy.equals(AssetReportGroupByOptions.LOCATION)) {
            key = "asset.location";
        } else if (groupBy.equals(AssetReportGroupByOptions.OWNER)) {
            key = "asset.owner";
        } else if (groupBy.equals(AssetReportGroupByOptions.STATUS)) {
            key = "asset.status";
        } else if (groupBy.equals(AssetReportGroupByOptions.TYPE)) {
            key = "asset.type";
        } else {
            log.error("unknown groupby column value: " + groupBy);
        }

        return key;
    }

    private String[] positionGroupByColumnFirst(String[] columnOrder, String colkey) {
        if (ArrayUtils.contains(columnOrder, colkey)) {
            if (ArrayUtils.indexOf(columnOrder, colkey) != 0) {
                columnOrder = (String[]) ArrayUtils.removeElement(columnOrder, colkey);
                columnOrder = (String[]) ArrayUtils.add(columnOrder, 0, colkey);
            }
        } else {
            columnOrder = (String[]) ArrayUtils.add(columnOrder, 0, colkey);
        }

        return columnOrder;
    }

    private AbstractColumn buildAbstractColumn(String title, String property, String propertyClassName, int width,
                                               Style columnStyle) throws ColumnBuilderException {
        return ColumnBuilder.getInstance()
                .setTitle(title)
                .setColumnProperty(property, propertyClassName)
                .setWidth(width)
                .setStyle(columnStyle)
                .build();
    }

    public ModelAndView assetsByOwner(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;
        List assets = assetService.getAllAssets();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("myData", assets);
        model.put("format", "pdf");
        Map<String, Object> imagesMap = new HashMap<String, Object>();
        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI.toString(), request.getContextPath() + "/images/report?image=");
        model.putAll(imagesMap);
        return new ModelAndView(assetsByOwnerReport, model);
    }

    public ModelAndView assetsByGroup(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;
        List assets = assetService.getAllAssets();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("myData", assets);
        model.put("format", "pdf");
        Map<String, Object> imagesMap = new HashMap<String, Object>();
        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI.toString(), request.getContextPath() + "/images/report?image=");
        model.putAll(imagesMap);
        return new ModelAndView(assetsByGroupReport, model);
    }

    public ModelAndView assetsByType(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;
        List assets = assetService.getAllAssets();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("myData", assets);
        model.put("format", "pdf");
        Map<String, Object> imagesMap = new HashMap<String, Object>();
        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI.toString(), request.getContextPath() + "/images/report?image=");
        model.putAll(imagesMap);
        return new ModelAndView(assetsByTypeReport, model);
    }

    public ModelAndView assetLeaseExpiration(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;
        List assets = assetService.getAssetsWithLeaseExpirationDate();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("myData", assets);
        model.put("format", "pdf");
        Map<String, Object> imagesMap = new HashMap<String, Object>();
        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI.toString(), request.getContextPath() + "/images/report?image=");
        model.putAll(imagesMap);
        return new ModelAndView(assetLeaseExpirationReport, model);
    }

    public ModelAndView assetWarrantyExpiration(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;
        List assets = assetService.getAssetsWithWarrantyExpirationDate();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("myData", assets);
        model.put("format", "pdf");
        Map<String, Object> imagesMap = new HashMap<String, Object>();
        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI.toString(), request.getContextPath() + "/images/report?image=");
        model.putAll(imagesMap);
        return new ModelAndView(assetWarrantyExpirationReport, model);
    }

    public ModelAndView assetPurchase(HttpServletRequest request, HttpServletResponse response) {
        assert response != null;
        List assets = assetService.getAssetsWithAcquisitionDate();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("myData", assets);
        model.put("format", "pdf");
        Map<String, Object> imagesMap = new HashMap<String, Object>();
        imagesMap.put(JRHtmlExporterParameter.IMAGES_URI.toString(), request.getContextPath() + "/images/report?image=");
        model.putAll(imagesMap);
        return new ModelAndView(assetPurchaseReport, model);
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setAssetService(AssetService assetService) {
        this.assetService = assetService;
    }

    public void setAssetsByOwnerReport(GLJasperReportsMultiFormatView assetsByOwnerReport) {
        this.assetsByOwnerReport = assetsByOwnerReport;
    }

    public void setAssetsByGroupReport(GLJasperReportsMultiFormatView assetsByGroupReport) {
        this.assetsByGroupReport = assetsByGroupReport;
    }

    public void setAssetFilterService(AssetFilterService assetFilterService) {
        this.assetFilterService = assetFilterService;
    }

    public void setAssetsByTypeReport(GLJasperReportsMultiFormatView assetsByTypeReport) {
        this.assetsByTypeReport = assetsByTypeReport;
    }

    public void setAssetLeaseExpirationReport(GLJasperReportsMultiFormatView assetLeaseExpirationReport) {
        this.assetLeaseExpirationReport = assetLeaseExpirationReport;
    }

    public void setAssetWarrantyExpirationReport(GLJasperReportsMultiFormatView assetWarrantyExpirationReport) {
        this.assetWarrantyExpirationReport = assetWarrantyExpirationReport;
    }

    public void setAssetPurhaseReport(GLJasperReportsMultiFormatView assetPurhaseReport) {
        this.assetPurchaseReport = assetPurhaseReport;
    }
}
