package net.grouplink.ehelpdesk.web.formatters;

import net.grouplink.ehelpdesk.domain.TicketPriority;
import net.grouplink.ehelpdesk.service.TicketPriorityService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class TicketPriorityFormatter implements Formatter<TicketPriority> {
    @Autowired
    TicketPriorityService ticketPriorityService;

    public TicketPriority parse(String s, Locale locale) throws ParseException {
        if (StringUtils.isNotBlank(s)) {
            return ticketPriorityService.getPriorityById(Integer.parseInt(s));
        } else {
            return null;
        }
    }

    public String print(TicketPriority ticketPriority, Locale locale) {
        return ticketPriority == null ? "" : ticketPriority.getId().toString();
    }
}
