package net.grouplink.ehelpdesk.web.interceptors;

import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.service.UserService;
import net.grouplink.ehelpdesk.service.acl.PermissionService;
import net.grouplink.ehelpdesk.web.annotations.GlobalPermission;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

public class GlobalPermissionInterceptor extends ApplicationObjectSupport implements HandlerInterceptor {

    private UserService userService;
    private PermissionService permissionService;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        GlobalPermission typePermissions = null;
        GlobalPermission methodPermissions = null;
        if (handler instanceof HandlerMethod) {
            Method method = ((HandlerMethod) handler).getMethod();
            typePermissions = AnnotationUtils.findAnnotation(method.getDeclaringClass(), GlobalPermission.class);
            methodPermissions = AnnotationUtils.findAnnotation(method, GlobalPermission.class);
        }

        if (typePermissions != null || methodPermissions != null) {
            SecurityContext securityContext = SecurityContextHolder.getContext();
            Authentication authentication = securityContext.getAuthentication();
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            String username = userDetails.getUsername();
            User user = userService.getUserByUsername(username);

            if (typePermissions != null) {
                for (String key : typePermissions.value()) {
                    if (!permissionService.hasGlobalPermission(user, permissionService.getPermissionByKey(key))) {
                        throw new AccessDeniedException("R U Denial?");
                    }
                }
            }

            if (methodPermissions != null) {
                for (String key : methodPermissions.value()) {
                    if (!permissionService.hasGlobalPermission(user, permissionService.getPermissionByKey(key))) {
                        throw new AccessDeniedException("R U Denial?");
                    }
                }
            }
        }

        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setPermissionService(PermissionService permissionService) {
        this.permissionService = permissionService;
    }
}
