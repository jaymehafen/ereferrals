<%--@elvariable id="groupList" type="java.util.List<net.grouplink.ehelpdesk.domain.Group>"--%>
<%--@elvariable id="listHolder" type="java.util.Map"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title>
        <spring:message code="kb.knowledgeBaseArticle"/>
    </title>

    <script type="text/javascript">
    	function goTo(url){
            document.location.href = url;
        }

        function setPageCookie(fld) {
            var value = fld.value;
            setCookie("kbSearchPageSize", value);
        }
    </script>

</head>

<body>
<form action="" method="post" class="form">
<section class="grid_12">
    <div class="block-content">
        <h1><spring:message code="kb.knowledgeBaseSearch"/></h1>
        <spring:message code="global.group"/>
        <select name="filter.groupId" >
            <option value="-1" ><spring:message code="global.all" /></option>
            <c:forEach items="${groupList}" var="group" >
                <option value="<c:out value="${group.id}" />"
                    <c:if test="${listHolder.kbList.filter.groupId == group.id}" >
                        selected="selected"
                    </c:if>>
                    <c:out value="${group.name}" />
                </option>
            </c:forEach>
        </select>
        &nbsp;
        <spring:message code="global.search"/><input size="40" type="text" name="filter.searchText"
                                                       <c:choose>
                                                        <c:when test="${listHolder.kbList.filter.searchText != '%'}">
                                                       value="<c:out value="${listHolder.kbList.filter.searchText}"/>"
                                                        </c:when>
                                                        <c:otherwise>
                                                            value=""
                                                        </c:otherwise>
                                                       </c:choose>
             />
        &nbsp;
        <spring:message code="global.in"/>
        <select name="filter.searchType" >
            <option value="0" <c:if test="${listHolder.kbList.filter.searchType == 0}" >selected="selected"</c:if>><spring:message code="kb.subject"/></option>
            <option value="1" <c:if test="${listHolder.kbList.filter.searchType == 1}" >selected="selected"</c:if>><spring:message code="kb.body"/></option>
            <option value="2" <c:if test="${listHolder.kbList.filter.searchType == 2}" >selected="selected"</c:if>><spring:message code="kb.subjectandbody"/></option>
        </select>
        &nbsp;
        <input class="button" type="submit" name="forceRefresh" value="<spring:message code="kb.button.searchKb"/>" />
    </div>
    <br>
    <div class="block-content no-title no-padding">
        <c:choose>
            <c:when test="${fn:length(listHolder.kbList.pageList) == 0 && not empty listHolder.kbList.filter.searchText}" >
                <p class="with-padding">
                    <spring:message code="kb.noArticlesFound" />
                </p>
            </c:when>
            <c:otherwise>
                <ul class="extended-list no-margin">
                    <c:forEach items="${listHolder.kbList.pageList}" var="kb" >
                        <li>
                            <a class="no-padding" href="<c:url value="/kb/kbView.glml" ><c:param name="kbId" value="${kb.id}" /></c:url>">
                                <c:out value="${kb.subject}" /><br />
                                <small>
                                    <spring:message code="kb.articleNumber" /><c:out value="${kb.id}" /><br />
                                    <spring:message code="kb.problem" />: <c:out escapeXml="false" value="${fn:substring(kb.problemText, 0, 100)}" />...
                                </small>
                            </a>

                            <ul class="extended-options">
                                <li>
                                    <spring:message code="kb.timesViewed" />: <c:out value="${kb.timesViewed}"/>
                                </li>
                                <li>
                                    <spring:message code="global.group" />: <c:out value="${kb.group.name}"/>
                                </li>
                                <li>
                                    <spring:message code="kb.lastModified" />: <fmt:formatDate value="${kb.date}" type="DATE" dateStyle="MEDIUM"/>
                                </li>
                            </ul>
                        </li>
                    </c:forEach>
                </ul>
            </c:otherwise>
        </c:choose>
        <div class="grey-bg no-margin">
            <div class="float-left with-padding">
                <spring:message code="pagedList.pltRecords"
                    arguments="${listHolder.kbList.firstElementOnPage + 1},${listHolder.kbList.lastElementOnPage + 1},${listHolder.kbList.nrOfElements}"/>
            </div>
            <div class="float-right">
                <spring:message code="pagedList.pltSize" var="ps"/>
                <select name="pageSize" onChange="setPageCookie(this); document.forms[0].submit();">
                    <c:forTokens items="5,10,25,50,75,100" delims="," var="crtps">
                        <option <c:if test="${listHolder.kbList.pageSize == crtps}">selected</c:if> value="<c:out value="${crtps}"/>">
                            <c:out value="${crtps} ${ps}"/>
                        </option>
                    </c:forTokens>
                </select>
            </div>
            <div class="float-right with-padding">
                <c:if test="${listHolder.kbList.pageCount > 1}">
                    <c:set var="page" value="${listHolder.kbList.page + 1}"/>
                    <c:set var="prev" value="${page - 1}"/>
                    <c:set var="next" value="${page + 1}"/>
                    <c:set var="first" value="${1}"/>
                    <c:set var="last" value="${listHolder.kbList.pageCount}"/>
                    <ul class="small-pagination">
                        <c:if test="${prev > 0}">
                            <li class="prev"><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${listHolder.kbList.page - 1}"/></c:url>"><spring:message code="global.prev"/></a></li>
                        </c:if>
                        <c:if test="${page > 1}">
                            <c:if test="${page > 2}">
                                <c:if test="${page - 2 > first}">
                                    <li><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="0"/></c:url>"><c:out value="${first}"/></a></li>
                                    <c:if test="${page - 2 > first + 1}">
                                        <li>&nbsp;.&nbsp;.&nbsp;.&nbsp;</li>
                                    </c:if>
                                </c:if>
                                <li><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${listHolder.kbList.page - 2}"/></c:url>"><c:out value="${page - 2}"/></a></li>
                            </c:if>
                            <li><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${listHolder.kbList.page - 1}"/></c:url>"><c:out value="${page - 1}"/></a></li>
                        </c:if>

                        <li class="current"><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${listHolder.kbList.page}"/></c:url>"><c:out value="${page}"/></a></li>

                        <c:if test="${page < last}">
                            <li><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${listHolder.kbList.page + 1}"/></c:url>"><c:out value="${page + 1}"/></a></li>
                            <c:if test="${page < last - 1}">
                                <li><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${listHolder.kbList.page + 2}"/></c:url>"><c:out value="${page + 2}"/></a></li>
                                <c:if test="${page + 2 < last}">
                                    <c:if test="${page + 2 < last - 1}">
                                        <li>&nbsp;.&nbsp;.&nbsp;.&nbsp;</li>
                                    </c:if>
                                    <li><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${last - 1}"/></c:url>"><c:out value="${last}"/></a></li>
                                </c:if>
                            </c:if>
                        </c:if>

                        <c:if test="${next <= last}">
                            <li class="next"><a href="<c:url value="/kb/kbSearch.glml"><c:param name="page" value="${listHolder.kbList.page + 1}"/></c:url>"><spring:message code="global.prev"/></a></li>
                        </c:if>
                    </ul>
                </c:if>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

<div>
    <div class="pagernav">

    </div>

</div>

<br class="clearFix"/>

<div class="pages">
    <div class="right">

    </div>
</div>

</form>
</body>

