<%--@elvariable id="userReports" type="java.util.List<net.grouplink.ehelpdesk.domain.Report>"--%>
<%--@elvariable id="publicReports" type="java.util.List<net.grouplink.ehelpdesk.domain.Report>"--%>
<%--@elvariable id="deleted" type="java.lang.String"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <link rel="stylesheet" href="<c:url value="/css/ticketFilter.css"/>" type="text/css">

    <title>
        <spring:message code="header.reports"/>
    </title>

    <script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>

    <script type="text/javascript">
        function reportListAction(action, reportId) {
            var url;
            if (action == 'view') {
                url = '<c:url value="/tf/reportView.glml"/>?reportId=' + reportId;
                openWindow(url, 'reportView', 900, 600);
            } else if (action == 'delete') {
                if(confirm("<spring:message code="global.confirmdelete"/>")){
                    url = '<c:url value="/tf/reportDelete.glml"/>?reportId=' + reportId;
                    location.href = url;
                }
            }
        }

        function toggleScheduler(action, recSchedId) {

            dojo.xhrPost({
                url: '<c:url value="/config/reportToggleScheduler.glml"/>',
                handleAs: "text",
                content: {action: action, recSchedId: recSchedId},
                load: function() {
                    location.href = location.href + '';
                },
                error: function(response, ioArgs) {
                    alert("XHR error (reportList:toggleScheduler) HTTP status code: " + ioArgs.xhr.status);
                    return response;
                }
            });
        }
    </script>

</head>

<body>

<section class="grid_6">
    <div class="block-content no-padding">
        <h1><spring:message code="report.myreports"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/tf/reportEdit.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <c:choose>
            <c:when test="${not empty userReports}">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="report.name"/></th>
                        <th scope="col"><spring:message code="report.visibility"/></th>
                        <th scope="col" class="table-actions"><spring:message code="ticketFilter.actions"/></th>
                        <th scope="col" class="table-actions"><spring:message code="ticketTemplate.schedule"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="report" items="${userReports}">
                        <tr>
                            <td><c:out value="${report.name}"/></td>
                            <td>
                                <c:choose>
                                    <c:when test="${report.privateReport}">
                                        <spring:message code="report.private"/>
                                    </c:when>
                                    <c:otherwise>
                                        <spring:message code="report.public"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="table-actions">
                                <a href="javascript://" onclick="reportListAction('view', '${report.id}');"
                                   title="<spring:message code="report.action.view"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/application-table.png"/>" alt="">
                                </a> &nbsp;
                                <a href="<c:url value="/tf/reportEdit.glml"><c:param name="reportId" value="${report.id}"/></c:url>"
                                   title="<spring:message code="report.action.edit"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                                </a> &nbsp;
                                <a href="javascript://" onclick="reportListAction('delete', '${report.id}');"
                                   title="<spring:message code="report.action.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                </a>
                            </td>
                            <td class="table-actions">
                                <c:if test="${report.showRecurrenceSchedule eq false}">
                                    <spring:message code="ticketTemplate.noScheduler"/>
                                </c:if>
                                <c:if test="${report.showRecurrenceSchedule eq true}">
                                    <c:choose>
                                        <c:when test="${report.schedule.recurrenceSchedule.running}">
                                            <img src="<c:url value='/images/theme/icons/fugue/control-stop-square.png'/>" alt=""
                                                 title="<spring:message code="scheduler.stopProcess"/>"
                                                 onclick="toggleScheduler('stop', '${report.schedule.recurrenceSchedule.id}');"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value='/images/theme/icons/fugue/control.png'/>" alt=""
                                                 title="<spring:message code="scheduler.startProcess"/>"
                                                 onclick="toggleScheduler('start', '${report.schedule.recurrenceSchedule.id}');"/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                &nbsp;
                                <c:if test="${report.showRecurrenceSchedule eq true}">
                                    <c:choose>
                                        <c:when test="${report.schedule.recurrenceSchedule.running}">
                                            <img src="<c:url value="/images/circle-ball-dark-antialiased.gif"/>"
                                                 alt=""/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value="/images/circle-ball-dark-noanim.gif"/>" alt=""/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <p class="with-padding"><spring:message code="report.noneFound"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>

<section class="grid_6">
    <div class="block-content no-padding">
    <h1><spring:message code="report.publicreports"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/tf/reportEdit.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <c:choose>
            <c:when test="${not empty publicReports}">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="report.name"/></th>
                        <th scope="col"><spring:message code="report.owner"/></th>
                        <th scope="col" class="table-actions"><spring:message code="report.actions"/></th>
                        <th scope="col" class="table-actions"><spring:message code="ticketTemplate.schedule"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="report" items="${publicReports}">
                        <tr>
                            <td><c:out value="${report.name}"/></td>
                            <td>${report.user.firstName} ${report.user.lastName}</td>
                            <td class="table-actions">
                                <a href="javascript://" onclick="reportListAction('view', '${report.id}');"
                                   title="<spring:message code="report.action.view"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/application-table.png"/>" alt="">
                                </a> &nbsp;
                                <a href="<c:url value="/tf/reportEdit.glml"><c:param name="reportId" value="${report.id}"/></c:url>"
                                   title="<spring:message code="report.action.edit"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                                </a> &nbsp;
                                <a href="javascript://" onclick="reportListAction('delete', '${report.id}');"
                                   title="<spring:message code="report.action.delete"/>">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                </a>
                            </td>
                            <td class="table-actions">
                                <c:if test="${empty report.schedule}">
                                    <spring:message code="ticketTemplate.noScheduler"/>
                                </c:if>
                                <c:if test="${not empty report.schedule}">
                                    <c:choose>
                                        <c:when test="${report.schedule.recurrenceSchedule.running}">
                                            <img src="<c:url value='/images/theme/icons/fugue/control-stop-square.png'/>" alt=""
                                                 title="<spring:message code="scheduler.stopProcess"/>"
                                                 onclick="toggleScheduler('stop', '${report.schedule.recurrenceSchedule.id}');"/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value='/images/theme/icons/fugue/control.png'/>" alt=""
                                                 title="<spring:message code="scheduler.startProcess"/>"
                                                 onclick="toggleScheduler('start', '${report.schedule.recurrenceSchedule.id}');"/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                &nbsp;
                                <c:if test="${not empty report.schedule.recurrenceSchedule}">
                                    <c:choose>
                                        <c:when test="${report.schedule.recurrenceSchedule.running}">
                                            <img src="<c:url value="/images/circle-ball-dark-antialiased.gif"/>"
                                                 alt=""/>
                                        </c:when>
                                        <c:otherwise>
                                            <img src="<c:url value="/images/circle-ball-dark-noanim.gif"/>" alt=""/>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <p class="with-padding"><spring:message code="report.noneFound"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>
</body>
