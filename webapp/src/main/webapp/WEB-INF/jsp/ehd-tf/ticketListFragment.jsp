<%@ include file="/WEB-INF/jsp/include.jsp" %>

<style type="text/css">
    @import "<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>";
    @import "<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>";
    .dojoxGridCell {
        white-space: nowrap;
    }
</style>

<script type="text/javascript" src="<c:url value="/dwr/interface/TicketFilterService.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/interface/TicketService.js"/>"></script>
<script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

<script type="text/javascript">

function formatSelection() {
    return '<input type="checkbox"/>';
}

var maxVisibleLength = 25;
function formatTFColumn(origValue, rowIndex, cell) {
    var formattedHtml = "";
    var row = this.grid.getItem(rowIndex);
    if (cell.field == "ticketFilter.priority" && row.i["ticketFilter.priority.image"]) {
        formattedHtml += '<img src="' + row.i["ticketFilter.priority.image"] + '" title="' + origValue + '" alt="">';
    } else {
        formattedHtml += '<span ';
        //        console.debug("rowIndex: " + rowIndex);

        var unread = false;
        if (row.i['ticketFilter.unread']) {
            unread = true;
        }

        var styles = "";
        if (unread) {
            styles += "font-weight:bold;";
        }

        if (styles != "") {
            formattedHtml += 'style="' + styles + '"';
        }

        formattedHtml += '>';
        formattedHtml += origValue;
        formattedHtml += '</span>';
    }

    return formattedHtml;
}


function formatNotificationColumn(value) {
    var html = '';
    if (value.i['ticketFilter.hasAttachment']) {
        html += '<img src="<c:url value="/images/attach.gif"/>" alt="" title="<spring:message code="ticket.attachments"/>"/>';
    }

    if (value.i['ticketFilter.hasParentTicket']) {
        html += '<img alt="" src="<c:url value="/images/form_up_16.png"/>" title="<spring:message code="ticketList.hasParentTicket" />"/>';
    }

    if (value.i['ticketFilter.hasSubTickets']) {
        html += '<img alt="" src="<c:url value="/images/form_down_16.png"/>" title="<spring:message code="ticketList.hasSubTickets" />"/>';
    }

    if (value.i['ticketFilter.actionRequired']) {
        html += '&nbsp;<img src="<c:url value="/images/flagged.gif"/>" alt="" title="<spring:message code="ticket.needsAttention"/>"/>';
    }

    return html;
}

function gridRowClicked(e) {
    var ticketId = e.grid.store.getValue(e.grid.getItem(e.rowIndex), "ticketFilter.ticketNumber");
    if (ticketId && ticketId > -1) {
        openTicketWindow(ticketId);
    }
}

function showTooltip(e) {
    var grid = e.grid;
    if (e.cell.field != "_item") {
        var item = grid.getItem(e.rowIndex);
        if (item) {
            var value = grid.store.getValue(item, e.cell.field);
            if (value && value.length > maxVisibleLength) {
                var newvalue = value.replace(new RegExp("\\r?\\n", "g"), "<br/>");
                var maxLength = 1000;
                if (newvalue.length > maxLength) {
                    newvalue = newvalue.substring(0, maxLength);
                    var lastspace = newvalue.lastIndexOf(' ');
                    if (lastspace != -1) newvalue = newvalue.substr(0, lastspace);
                    newvalue += " ...";
                }

                dijit.showTooltip(newvalue, e.cellNode, ["above", "below"]);
            }
        }
    }
}

function hideTooltip(e) {
    dijit.hideTooltip(e.cellNode);
}

function openWindow(url, windowName, width, height) {
    var features = "modal=yes,toolbar=no,status=yes,width=" + width + ",height=" + height + ",scrollbars=yes,resizable=yes,";
    var popup = window.open(url, windowName, features);
    if (popup != null) {
        popup.focus();
    }
}

function noSort(columnIndex) {
    return columnIndex != 1;
}

function handleGetTicketFilter(ticketFilterDto, args) {
    var gridContainer = args.gridContainer;
    var ticketFilterId = args.ticketFilterId;
    var findUserContactId = args.findUserContactId;
    var showClosed = args.showClosed;

    var ticketFilter = ticketFilterDto.ticketFilter;
    console.debug('got ticketFilter id: ' + ticketFilterId);

    var storeUrl = getTicketFilterStoreUrl(ticketFilterId, findUserContactId, showClosed);

    console.debug("storeUrl: " + storeUrl);
    // ticket data store
    var ticketFilterStore = new dojox.data.QueryReadStore({
        url: storeUrl
    });

    // grid layout
    var gridLayout = [

        //                { type: "dojox.grid._CheckBoxSelector",
        //                    field: 'ticketFilter.selectedFlag',
        //                    name: '',
        //                    width: '50px'
        //                    formatter: formatSelection }

        {
            field: '_item',
            name: ' ',
            width: '40px',
            resize: false,
            formatter: formatNotificationColumn
        }
    ];

    var columnMap = ticketFilterDto.columnNames;

    var obj;
    var columnNum = ticketFilter.columnOrder.length;
    var widthPercent = 100/columnNum;
    for (var i = 0; i < ticketFilter.columnOrder.length; i++) {
        obj = {
            field: ticketFilter.columnOrder[i],
            name: getGridColumnName(ticketFilter.columnOrder[i], columnMap),
            width: widthPercent + '%',
            formatter: formatTFColumn
        };

        gridLayout[i + 1] = obj;
    }

    // add actions column for edit button
    gridLayout[gridLayout.length] = {
        field: '_item',
        name: '<spring:message code="global.actions" javaScriptEscape="true"/>',
        width: '40px',
        formatter: formatActions
    };

    var grid = new dojox.grid.DataGrid({
        query: { ticketId: '*' },
        clientSort: false,
        rowSelector: '20px',
        structure: gridLayout,
        store: ticketFilterStore,
        editable: false,
        canSort: noSort,
        id: gridContainer + "_grid",
        noDataMessage: "<spring:message code="ticketList.nolist"/>",
        plugins: {
            cookie: true
        }
    });

    dojo.connect(grid, "onRowDblClick", gridRowClicked);
    dojo.connect(grid, "onCellMouseOver", showTooltip);
    dojo.connect(grid, "onCellMouseOut", hideTooltip);

    console.debug("appending to id " + gridContainer);

    dijit.byId(gridContainer).set("content", grid);

    grid.startup();

    // The cookie plugin only saves cookies when the grid is destroyed. We have to do the following because
    // dojox.grid.enhanced.plugins.Cookie uses dijit.findWidgets to try to do the same thing but findWidgets doesn't
    // search recursively for widgets.
    dojo.addOnWindowUnload(function() {
        grid.destroyRecursive();
    });

    var tab = dijit.getEnclosingWidget(grid.domNode.parentNode);
    if(tab) {
        dojo.connect(grid, "_onFetchComplete", function() {
            $("#" + tab.id + "_badge").text(grid.rowCount);
        });
    }
}

function formatActions(value, rowIndex, cell) {
    var ticketId = value.i["ticketFilter.ticketNumber"];

    var text = '<a href="javascript:openTicketWindow(' + ticketId + ');"><img style="width:15px;height:15px;" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"></a>';

//    text += "&nbsp;&nbsp;"

    return text;
}

function getTicketFilterStoreUrl(ticketFilterId, findUserContactId, showClosed) {
    var storeUrl = '<c:url value="/tf/ticketFilterStore.glml" />';
    storeUrl += "?filter=true";
    if (ticketFilterId) {
        storeUrl += '&ticketFilterId=' + ticketFilterId;
    }

    if (findUserContactId) {
        storeUrl += '&findUserContactId=' + findUserContactId;
    }

    if (showClosed) {
        storeUrl += '&showClosed=true';
    }

    return storeUrl;
}

function getGridColumnName(columnName, columnMap) {
    return columnMap[columnName];
}

function createGrid(gridContainer, ticketFilterId, findUserContactId, showClosed) {
    var arg = {
        gridContainer: gridContainer,
        ticketFilterId: ticketFilterId,
        findUserContactId: findUserContactId,
        showClosed: showClosed
    };

    var callMetaData = {
        callback: handleGetTicketFilter,
        arg: arg
    };

    var ticketFilterDto;
    if (ticketFilterId && ticketFilterId > -1) {
        ticketFilterDto = TicketFilterService.getTicketFilterById(ticketFilterId, callMetaData);
    } else if (findUserContactId) {
        ticketFilterDto = TicketFilterService.getFindUserTicketFilter(findUserContactId, callMetaData);
    } else {
        ticketFilterDto = TicketFilterService.getTicketFilterFromSession(callMetaData);
    }
}

function getSelectedTidsFromGrid(gridId) {
    var theGrid = dijit.byId(gridId);
    var items = theGrid.selection.getSelected();
    var tids = new Array();
    for (var index = 0; index < items.length; index++) {
        tids.push(items[index].i['ticketFilter.ticketNumber']);
    }
    return tids;
}

function deleteSelected(gridContainerId, ticketFilterId) {
    var tids = getSelectedTidsFromGrid(gridContainerId + "_grid");

    if (tids.length > 0) {
        console.debug("ticketListFragment:deleteSelected() Ticket IDS: " + tids);
        if (confirm("<spring:message code="ticketList.deleteConfirm"/>\n" + tids)) {
            var callMetaData = {
                callback: refreshGrid,
                arg: {'gridContainerId':gridContainerId, 'ticketFilterId':ticketFilterId},
                errorHandler: outputError
            };
            TicketService.deleteTicketIdList(tids, callMetaData);
        }
    } else {
        alert("<spring:message code="ticketList.noSelection"/>");
    }
}

function outputError(e, ex) {
    console.debug("Error: " + ex);
}

function refreshGrid(dummy, args) {
    var gridContainerId = args.gridContainerId;
    var tfId = args.ticketFilterId;
    var gContainer = document.getElementById(gridContainerId);

    var theGrid = dijit.byId(gridContainerId + "_grid");
    theGrid.destroy();

    // Clear the parent div
    while (gContainer.firstChild) gContainer.removeChild(gContainer.firstChild);

    createGrid(gridContainerId, tfId);
}

function showMUDialog(gridContainerId) {
    var tids = getSelectedTidsFromGrid(gridContainerId + "_grid");
    document.getElementById("gridId").value = gridContainerId;
    if (tids.length > 0) {
        dijit.byId("massUpdateDialog").show();
    } else {
        alert("<spring:message code="ticketList.noSelection"/>");
    }
}

function clearDialog() {
    // clear the fields
    var fieldIds = ["cbx_workTime","workTime", "cbx_contact","contact", "cbx_assignedTo","assignedTo", "cbx_priority",
        "priority", "cbx_status","status", "cbx_estimatedDate", "estimatedDate", "cbx_addComment", "addCommentSubject",
        "addCommentNote", "cbx_groupAsChildren", "groupAsChildren"
    ];

    for (var index in fieldIds) {
        console.debug("resetting id: " + fieldIds[index]);
        dijit.byId(fieldIds[index]).reset();
    }

    dijit.byId("massUpdateDialog").hide();
}

function massUpdateDialogIsValid(){
    var retVal = true;
    var mudValues = dijit.byId("massUpdateDialog").attr('value');
    console.debug(mudValues);

    var errMsg = "";

    if(mudValues.cbx_workTime[0]){
        if(mudValues.workTime != "" && !dijit.byId("workTime").isValid()){
            errMsg += "Worktime is not formatted correctly.\n";
            retVal = false;
        }
    }
    if(mudValues.cbx_contact[0]){
        if(mudValues.contact == ""){
            errMsg += "Contact cannot be blank.\n";
            retVal = false;
        }
    }
    if(mudValues.cbx_assignedTo[0]){
        if(mudValues.assignedTo == ""){
            errMsg += "Assigned To cannot be blank.\n";
            retVal = false;
        }
    }
    if(mudValues.cbx_priority[0]){
        if(mudValues.priority == ""){
            errMsg += "Priority cannot be blank.\n";
            retVal = false;
        }
    }
    if(mudValues.cbx_status[0]){
        if(mudValues.status == ""){
            errMsg += "Status cannot be blank.\n";
            retVal = false;
        }
    }
    if(mudValues.cbx_estimatedDate[0]){
        if(!dijit.byId("estimatedDate").isValid()){
            errMsg += "Estimated Date is not valid\n";
            retVal = false;
        }
    }
    if(mudValues.cbx_groupAsChildren[0]){
        if(mudValues.groupAsChildren != "" && !dijit.byId("groupAsChildren").isValid()){
            errMsg += "Parent ticket number must be an integer.";
            retVal = false;
        }
    }
    if(errMsg != "") alert(errMsg);
    return retVal;
}

function massUpdateSubmit(args){
    var callMetaData = {
        callback: handleMassUpdate,
        errorHandler: outputError
    };
    var tids = getSelectedTidsFromGrid(args.gridId + "_grid");
    console.debug("updating tids: " + tids);
    TicketFilterService.massUpdate(args, tids, callMetaData);
}

function handleMassUpdate(msg){
    clearDialog();
    alert(msg);
    location.href = location.href + '';
}
</script>
