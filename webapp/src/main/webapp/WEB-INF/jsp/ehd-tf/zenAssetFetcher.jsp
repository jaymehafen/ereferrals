<%@ include file="/WEB-INF/jsp/include.jsp" %>

<%--@elvariable id="zenBaseUrl" type="java.lang.String"--%>
<%--@elvariable id="zenAssets" type="java.util.List<net.grouplink.ehelpdesk.domain.asset.ZenAsset>"--%>
<%--@elvariable id="search" type="java.lang.String"--%>
<head>
    <title><spring:message code="assetTracker.zenworks10Assets" text="ZENworks 10 Assets"/></title>

    <script type="text/javascript">
        function getSelectedZenAssetIds() {
            var selectedAssetIds = new Array();
            var selectedAssetIdsIndex = 0;
            var cbxs = document.getElementsByName('selectedZenAssetsCbx');

            for (var i = 0; i < cbxs.length; i++) {
                var cb = dijit.byId(cbxs[i].id);
                if (cb.checked) {
                    selectedAssetIds[selectedAssetIdsIndex++] = cb.value;
                }
            }

            return selectedAssetIds;
        }

        function addAssets() {
            var selectedAssetIds = getSelectedZenAssetIds();
            window.opener.addSelectedZenAssets(selectedAssetIds, window);

        }

    </script>

</head>

<body>
<form id="zenAssetFetcherForm" method="post" action="<c:url value="/tf/fetchZenAssets.glml"><c:param name="dialog" value="true"/></c:url>" class="form">

    <p>
        <label for="assetName"><spring:message code="assetTracker.name"/></label>
        <input type="text" id="assetName" name="assetName" value=""/>
    </p>

    <p>
        <label for="assetType"><spring:message code="asset.type"/></label>
        <input type="text" id="assetType" name="assetType" value=""/>
    </p>

    <p>
        <label for="operatingSystem"><spring:message code="assetTracker.operatingSystem"/></label>
        <input type="text" id="operatingSystem" name="operatingSystem" value=""/>
    </p>

    <p>
        <button type="submit" name="search" value="true"><spring:message code="global.search"/></button>
        <button type="button" id="zenCancel"><spring:message code="global.cancel"/></button>
    </p>


    <c:if test="${not empty search}">
        <fieldset>
            <legend><spring:message code="ticketFilter.searchResults"/></legend>
            <c:choose>
                <c:when test="${fn:length(zenAssets) > 0}">
                    <div style="maxheight:500px;overflow:auto" class="with-margin">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="zenSelectAll"></th>
                                    <th><spring:message code="assetTracker.name"/></th>
                                    <th><spring:message code="asset.type"/></th>
                                    <th><spring:message code="assetTracker.operatingSystem"/></th>
                                </tr>
                            </thead>
                            <tbody>

                            <c:forEach var="zenAsset" items="${zenAssets}">
                                <tr>
                                    <td>
                                        <input type="checkbox" value="${zenAsset.id}" id="zenCbx_${zenAsset.id}">
                                        <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                                            <a href="${zenBaseUrl}/jsp/index.jsp?pageid=workstationDetails&uid=${zenAsset.deviceIdString}&adminid=Devices" target="_blank">
                                                <img src="<c:url value="/images/theme/icons/fugue/application-table.png"/>"
                                                     alt="<spring:message code="global.view" />"/>
                                            </a>
                                        </authz:authorize>
                                    </td>
                                    <td>${zenAsset.assetName}</td>
                                    <td>${zenAsset.assetType}</td>
                                    <td>${zenAsset.operatingSystem}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <p>
                        <button type="button" id="zenAddSelectedButton"><spring:message code="global.applySelected"/></button>
                    </p>
                </c:when>
                <c:otherwise>
                    <p class="message">
                        <spring:message code="assetTracker.noResultsFound"/>
                    </p>
                </c:otherwise>
            </c:choose>
        </fieldset>
    </c:if>

</form>


</body>