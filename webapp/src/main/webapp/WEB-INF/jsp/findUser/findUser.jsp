<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title><spring:message code="user.find" /></title>
    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>" type="text/css">

    <script type="text/javascript" src="<c:url value="/js/jquery.watermark.min.js"/>"></script>

        <%@ include file="/WEB-INF/jsp/ehd-tf/ticketListFragment.jsp" %>

        <script type="text/javascript">

        var lastUserId;

        function init(){

            var userStore = new dojox.data.QueryReadStore({
                url: "<c:url value="/stores/users.json"><c:param name="incInactive" value="true"/></c:url>"
            });

            function formatActions(value, rowIndex, cell) {
                var userId = value.i.userId;

                var showTicketsLink = '<a class="showTicketsLink" href="javascript:showTickets(' + userId + ');"><img style="width:15px;height:15px;" src="<c:url value="/images/theme/icons/fugue/application-table.png"/>" title="<spring:message code="ticketList.showTickets"/>"></a>';

                var createTicketScript = "newTicket('" + userId + "')";
                var createTicketLink = '<a class="createTicketLink" onclick="' + createTicketScript + ';" href="javascript://"><img style="width:15px;height:15px;" src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" title="<spring:message code="ticket.newTicket"/>"></a>';

                var editUserLink = '<a class="editUserLink" href="<c:url value="/config/users/"/>' + userId + '/edit"><img style="width:15px;height:15px;" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" title="<spring:message code="user.edit"/>"></a>';

                return showTicketsLink + "&nbsp;&nbsp" + createTicketLink + "&nbsp;&nbsp" + editUserLink;
            }

            var usersGrid = new dojox.grid.DataGrid({
                store: userStore,
                structure: [
                    {name:"<spring:message code="user.lastName"/>", field: "lastName", width: 'auto'},
                    {name:"<spring:message code="user.firstName"/>", field: "firstName", width: 'auto'},
                    {name:"<spring:message code="user.email"/>", field: "email", width: 'auto'},
                    {name:"<spring:message code="user.userName"/>", field: "loginId", width: 'auto'},
                    {name:"<spring:message code="ticket.location"/>", field: "location", width: 'auto'},
                    {name:"<spring:message code="user.status"/>", field: "active", width: 'auto'},
                    {name:"<spring:message code="user.roles"/>", field: "roles", width: 'auto'},
                    {name:"<spring:message code="user.groups"/>", field: "groups", width: 'auto'},
                    {name:"<spring:message code="global.actions"/>", field: "_item", width: "100px", formatter: formatActions}
                ],
                editable: false,
                autoHeight: 20,
                noDataMessage: "<spring:message code="userManagement.noUsers" javaScriptEscape="true"/>"
            }, "usersGrid");

            //disallow sorting on roles or groups
            usersGrid.canSort = function(col) {
                if(Math.abs(col) == 7 || Math.abs(col) == 8 || Math.abs(col) == 9) {
                    return false;
                }
                return true;
            };

            usersGrid.startup();

            dojo.connect(dojo.byId("searchUser"), "onkeyup", function(e) {
                var searchQuery = this.value;
                delay(function() {
                    usersGrid.setQuery({label: searchQuery});
                }, 500);
            });

            dojo.connect(dojo.byId("showClosedCheckbox"), "onchange", function(e) {
                console.debug("show closed toggled");
                showTickets(lastUserId);
            });
        }

        dojo.addOnLoad(init);

        $(function() {
            $("#searchUser").watermark("<spring:message code="global.search"/>");
        });

        function showTickets(userId) {
            lastUserId = userId;
            var oldGrid = dijit.byId("ticketsGrid_grid");
            if (oldGrid) {
                oldGrid.destroy();
                // Clear the parent div
                var gContainer = document.getElementById("ticketsGrid");
                while (gContainer.firstChild) gContainer.removeChild(gContainer.firstChild);
            }

            dojo.xhrGet({
                url: "<spring:url value="/findUser/userDisplayName"/>/" + userId,
                handleAs: "text",
                preventCache: true,
                handle: function(response) {
                    dojo.byId("showTicketsInfo").style.display = "block";
                    dojo.byId("showTicketsName").textContent = response;
                }
            });

            var showClosed = dojo.byId("showClosedCheckbox").checked;
            document.getElementById("ticketsGrid").style.height = "300px";
            createGrid("ticketsGrid", null, userId, showClosed);
        }
    </script>

</head>

<body>

<section class="grid_12">
    <div class="block-content">
        <h1><spring:message code="user.find" /></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li class="form">
                    <input type="text" id="searchUser" name="searchUser">
                </li>
                <li class="sep"></li>
                <li>
                    <a href="<c:url value="/config/users/new"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                        <spring:message code="userManagement.createnew" />
                    </a>
                </li>
                <c:if test="${applicationScope.ldapIntegration}" >
                    <li>
                        <a href="<c:url value="/config/users/import" />">
                            <img src="<c:url value="/images/theme/icons/fugue/arrow-270.png"/>">
                            <spring:message code="global.importLdapUser" />
                        </a>
                    </li>
                </c:if>
            </ul>
        </div>
        <div class="no-margin">
            <div id="usersGrid"></div>
            <br>
            <hr>
            <br>
            <div id="showTicketsInfo" style="display: none;">
                <spring:message code="ticketList.ticketsFor" />: <strong><span id="showTicketsName"></span></strong>
                <span class="float-right"><spring:message code="ticketList.showClosed" />: <input type="checkbox" id="showClosedCheckbox"/></span>
            </div>
            <br>
            <div id="ticketsGrid" dojotype="dijit.layout.ContentPane"></div>
        </div>

    </div>
</section>
</body>
