<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="userRoleGroups" type="java.util.List"--%>
<%--@elvariable id="myTicketTabs" type="java.util.List<net.grouplink.ehelpdesk.domain.MyTicketTab>"--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title><spring:message code="global.systemName"/></title>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="index,follow" name="robots"/>
    <meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type"/>
    <link href="<c:url value="/images/eReferrals_logo.png"/>" rel="apple-touch-icon"/>
    <meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no"
          name="viewport"/>
    <link href="<c:url value="/css/iwk_style.css"/>" rel="stylesheet" type="text/css"/>
    <script src="<c:url value="/js/iwk_functions.js"/>" type="text/javascript"></script>
</head>
<body>

<div id="topbar">
    <div id="leftnav">
        <a href="<c:url value="/ip/home.glml"/>">
            <img alt="home" src="<c:url value="/images/home.png"/>" />
            <%--<img src="<c:url value="/images/header/records_48.png"/>" alt="" style="height:20px;width:20px;">--%>
        </a>
        <a href="<c:url value="/ip/ticketEdit.glml"/>">
            <%--<spring:message code="ticket.new"/>--%>
            <img src="<c:url value="/images/header/records_add_48.png"/>" style="height:20px;width:20px;" alt=""/>
        </a>
    </div>
    <div id="rightnav">
        <a href="<c:url value="/logout.glml"/>">
            <spring:message code="header.signout"/>
        </a>
    </div>
</div>

<div id="content">
    <span class="graytitle"><spring:message code="header.myTickets"/></span>
    <ul class="pageitem">
        <c:forEach var="mttab" items="${myTicketTabs}" varStatus="row">
            <li class="menu">
                    <a href="<c:url value="/ip/ticketList.glml"><c:param name="mttabId" value="${mttab.id}"/></c:url>">
                        <span class="name"><c:out value="${mttab.name}"/></span>
                        <span class="arrow"></span>
                    </a>
                </li>
        </c:forEach>

        <%--<c:forEach var="urg" items="${userRoleGroups}">--%>
            <%--<c:if test="${urg.active}">--%>
                <%--<li class="menu">--%>
                    <%--<a href="<c:url value="/ip/ticketList.glml"><c:param name="urgid" value="${urg.id}"/></c:url>">--%>
                        <%--<span class="name"><c:out value="${urg.group.name}"/></span>--%>
                        <%--<span class="arrow"></span>--%>
                    <%--</a>--%>
                <%--</li>--%>
            <%--</c:if>--%>
        <%--</c:forEach>--%>
        <%--<li class="menu">--%>
            <%--<a href="<c:url value="/ip/ticketList.glml"><c:param name="obm" value="true"/></c:url>">--%>
                <%--<span class="name"><spring:message code="ticketList.ownedByMe" /></span>--%>
                <%--<span class="arrow"></span>--%>
            <%--</a>--%>
        <%--</li>--%>
    </ul>    
</div>
<div id="footer">
    <a href="http://www.grouplink.net">Powered by GroupLink</a></div>
</body>

</html>