<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="ticket" type="net.grouplink.ehelpdesk.domain.Ticket"--%>
<%--@elvariable id="contacts" type="java.util.List"--%>
<%--@elvariable id="ldapFieldList" type="java.util.List"--%>
<%--@elvariable id="ldapUser" type="net.grouplink.ehelpdesk.domain.LdapUser"--%>
<%--@elvariable id="locations" type="java.util.List"--%>
<%--@elvariable id="groups" type="java.util.List"--%>
<%--@elvariable id="categories" type="java.util.List"--%>
<%--@elvariable id="catOpts" type="java.util.List"--%>
<%--@elvariable id="assignments" type="java.util.List"--%>
<%--@elvariable id="priorities" type="java.util.List"--%>
<%--@elvariable id="ticketStatus" type="java.util.List"--%>
<%--@elvariable id="successfulSave" type="java.lang.Boolean"--%>
<%--@elvariable id="tabName" type="java.lang.String"--%>
<%--@elvariable id="mttabId" type="java.lang.Integer"--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

<title><spring:message code="global.systemName"/></title>

<meta content="yes" name="apple-mobile-web-app-capable"/>
<meta content="index,follow" name="robots"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

<link href="<c:url value="/images/eReferrals_logo.png"/>" rel="apple-touch-icon"/>
<meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no"/>

<link href="<c:url value="/css/iwk_style.css"/>" rel="stylesheet" type="text/css"/>
<script src="<c:url value="/js/iwk_functions.js"/>" type="text/javascript"></script>

<link rel="stylesheet" href="<c:url value="/js/spinningwheel/spinningwheel.css"/>" type="text/css" media="all"/>

<script type="text/javascript" src="<c:url value="/js/spinningwheel/spinningwheel-min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/theme/jquery-1.7.0.min.js"/>"></script>

<script type="text/javascript">

    // create a collection of ldap fields ids to hide when user changes
    var ldapFlds = {};
    <c:forEach items="${ldapFieldList}" var="ldapField">
    <c:if test="${ldapField.showOnTicket}">
    ldapFlds[ldapFlds.length] = 'ldapfield_${ldapField.id}';
    </c:if>
    </c:forEach>



    function hideLdapFields() {
        for (var lf in ldapFlds) {
            document.getElementById(ldapFlds[lf]).style.display = 'none';
        }
    }

    function setContactEmail(address) {
        var emailml;

        if (address.length == 0) {
            emailml = "<a href='javascript://'>" +
                      "<img src='<c:url value="/images/mail.png"/>' alt=''>" +
                      "<span class='name'><spring:message code="global.notSelected"/></span>" +
                      "</a>";
        } else {
            emailml = "<a href='mailto:" + address + "'>" +
                      "<img src='<c:url value="/images/mail.png"/>' alt=''>" +
                      "<span class='name'>" + address + "</span>" +
                      "<span class='arrow'></span></a>";
        }
        document.getElementById('ctctEmail').innerHTML = emailml;
    }

    function setContactPhone(number) {
        var phoneml;

        if (number.length == 0) {
            phoneml = "<a href='javascript://'>" +
                      "<img src='<c:url value="/images/telephone.png"/>' alt=''>" +
                      "<span class='name'><spring:message code="global.notSelected"/></span>" +
                      "</a>";
        } else {
            phoneml = "<a href='tel:" + number + "'>" +
                      "<img src='<c:url value="/images/telephone.png"/>' alt=''>" +
                      "<span class='name'>" + number + "</span>" +
                      "<span class='arrow'></span></a>";
        }
        document.getElementById('ctctPhone').innerHTML = phoneml;
    }

    function resetCustomFields() {
        var cfUl = document.getElementById('cfUl');

        while (cfUl.hasChildNodes()) {
            cfUl.removeChild(cfUl.lastChild);
        }

        buildCustomFields();
    }

    function buildCustomFields() {
        var groupId = $("#group").val();

        // check if group is initialized yet
        if (!groupId) return;

        var locationId = $("#location").val();
        var catId  = $("#category").val();
        var catOptId = $("#categoryOption").val();

        var url = "<c:url value="/stores/ticketCustomFields.json"/>";

        var firstParam = true;
        if (locationId) {
            url += "?locId=" + locationId;
            firstParam = false;
        }

        if (groupId) {
            url += (firstParam) ? "?" : "&";
            url += "groupId=" + groupId;
            firstParam = false;
        }

        if (catId) {
            url += (firstParam) ? "?" : "&";
            url += "catId=" + catId;
            firstParam = false;
        }

        if (catOptId) {
            url += (firstParam) ? "?" : "&";
            url += "catOptId=" + catOptId;
        }

        <c:if test="${not empty ticket.ticketId}">
        url += "&tid=<c:out value="${ticket.ticketId}"/>";
        </c:if>

        $.get(url, function(custFields) {
            for (var ind in custFields) {
                buildCustomField(custFields[ind]);
            }
        });
    }

    var cfVals = {};
    <c:if test="${empty ticket.id}">
    <c:forEach var="cfVal" items="${ticket.customeFieldValues}">
    cfVals["${cfVal.customField.id}"] = "<c:out value="${cfVal.fieldValue}"/>";
    </c:forEach>
    </c:if>

    function buildCustomField(cf) {

        var cfUl = document.getElementById('cfUl'), tid;
    <c:choose>
    <c:when test="${not empty ticket.ticketId}">
        tid = "<c:out value="${ticket.ticketId}"/>";
    </c:when>
    <c:otherwise>
        tid = "new";
    </c:otherwise>
    </c:choose>

        var label = cf.name + ":";
        if (cf.required) label = "*" + label;

        var html;

        var cfId = "cf_" + cf.id + "_" + tid;
        var cfVal = cf.cfValue.value;
        if (cfVal.trim() == "" && cfVals[cf.id]) cfVal = cfVals[cf.id];
        var checked = "";

        var li = document.createElement("li");

        switch (cf.type) {
            case "text":
                li.className = "form";
                html = "<span class=\"narrow\"><span class=\"name\">" + label + "</span>";
                html += "<input type=\"text\" name=\"" + cfId + "\" id='" + cfId + "' value='" + cfVal + "'/></span>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                break;
            case "textarea":
                li.className = "textbox";
                html = "<span class=\"name\">" + label + "</span>";
                html += "<textarea name='" + cfId + "' id='" + cfId + "'>" + cfVal + "</textarea>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                break;
            case "radio":
                li.className = "form";
                html = "<li class=\"form\"> <span class=\"choice\"> <span class=\"name\">" + label + "</span> </span> </li>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                for (var ind in cf.options) {
                    checked = "";
                    var cfOption = cf.options[ind];
                    var cfOptionId = cfId + "_" + cfOption.id;
                    if (cfOption.displayValue == cfVal) checked = "checked='checked'";
                    li = document.createElement("li");
                    li.className = "form";
                    html = "<span class=\"choice\"> <span class=\"radioname\">" + cfOption.displayValue + "</span>";
                    html += "<input type='radio' name='" + cfId + "' id='" + cfOptionId + "' value='" + cfOption.displayValue + "' " + checked + "/>";
                    html += "</span>";
                    li.innerHTML = html;
                    cfUl.appendChild(li);
                    iDecorateInput(cfOptionId);
                }
                break;
            case "checkbox":
                if (cfVal == "true") {
                    checked = "checked='checked'";
                } else {
                    checked = '';
                }
                li.className = "form";
                html = "<span class=\"check\"><span class=\"name\">" + label + "</span>";
                html += "<input name='" + cfId + "' id='" + cfId + "' type='checkbox' " + checked + "/>";
                html += "</span>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                iDecorateInput(cfId);
                break;
            case "select":
                li.className = "form";
                html =  '<label for="' + cfId + '" class="selectlabel">' + label + '</label>';
                html += "<select name='" + cfId + "' id='" + cfId + "' >";
                for (ind in cf.options) {
                    cfOption = cf.options[ind];
                    if (cfOption.displayValue == cfVal) {
                        checked = "selected='selected'";
                    } else {
                        checked = "";
                    }

                    html += "<option value='" + cfOption.displayValue + "' " + checked + ">" + cfOption.displayValue + "</option>";
                }
                html += "</select> <span class=\"arrow\"></span>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                iDecorateSelect(cfId);
                break;
            case "number":
                li.className = "form";
                html = "<span class=\"narrow\"><span class=\"name\">" + label + "</span>";
                html += "<input type=\"text\" name=\"" + cfId + "\" id='" + cfId + "' value='" + cfVal + "'/></span>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                break;
            case "date":
                li.className = "form";
                html = "<span class=\"narrow\"><span class=\"name\">" + label + "</span>";
                html += "<input type=\"text\" name=\"" + cfId + "\" id='" + cfId + "' value='" + cfVal + "'/></span>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                break;
            case "datetime":
                li.className = "form";
                html = "<span class=\"narrow\"><span class=\"name\">" + label + "</span>";
                html += "<input type=\"text\" name=\"" + cfId + "\" id='" + cfId + "' value='" + cfVal + "'/></span>";
                li.innerHTML = html;
                cfUl.appendChild(li);
                break;
            default:
                break;
        }
    }

    function resetCatOpts() {
        var catId = $("#category").val();
        var locationId = $("#location").val();
        var url = '<c:url value="/stores/catOptions.json"/>?prevId=' + catId + '&locationId=' + locationId;

        $.get(url, function(opts) {
            replaceSelectOptions(opts, "categoryOption");
        });
    }

    var translatedDDNames = {};
    translatedDDNames['group'] = '<spring:message code="ticket.group"/>';
    translatedDDNames['category'] = '<spring:message code="ticket.category"/>';
    translatedDDNames['categoryOption'] = '<spring:message code="ticket.categoryOption"/>';
    translatedDDNames['assignedTo'] = '<spring:message code="ticket.assignedTo"/>';

    function replaceSelectOptions(opts, selectId) {
        $("#" + selectId).empty();
        $("#" + selectId).append("<option value=\"\">&nbsp;</option>");
        for (var i in opts.items) {
            var item = opts.items[i];
            $("#" + selectId).append("<option value=\"" + item.id + "\">" + item.label + "</option>");
        }
    }

    function resetAssignments() {
        var locationId = $("#location").val();
        var groupId = $("#group").val();
        var catId  = $("#category").val();
        var catOptId = $("#categoryOption").val();

        var url = "<c:url value="/stores/moreBetterAssignments.json"/>";
        var firstParam = true;
        if (locationId) {
            url += "?locationId=" + locationId;
            firstParam = false;
        }

        if (groupId) {
            url += (firstParam) ? "?" : "&";
            url += "groupId=" + groupId;
            firstParam = false;
        }

        if (catId) {
            url += (firstParam) ? "?" : "&";
            url += "catId=" + catId;
            firstParam = false;
        }

        if (catOptId) {
            url += (firstParam) ? "?" : "&";
            url += "catOptId=" + catOptId;
        }

        $.get(url, function(data) {
            replaceSelectOptions(data, "assignedTo")
        });

    }

    function resetContact() {
        var url = "<c:url value="/stores/singleUser.json"/>";
        url += "?contactId=" + $("#contact").val();

        $.get(url, function(user) {
            setContactEmail(user.email);
            setContactPhone(user.phone);
            hideLdapFields();
        })
    }

    function openEstimatedDate() {
        var now = new Date();
        var days = { };
        var years = { };
        var months = { 0: 'Jan', 1: 'Feb', 2: 'Mar', 3: 'Apr', 4: 'May', 5: 'Jun', 6: 'Jul', 7: 'Aug', 8: 'Sep', 9: 'Oct', 10: 'Nov', 11: 'Dec' };

        for (var i = 1; i < 32; i += 1) {
            days[i] = i;
        }

        for (i = now.getFullYear(); i < now.getFullYear() + 50; i++) {
            years[i] = i;
        }

        SpinningWheel.addSlot(years, 'right', now.getFullYear());
        SpinningWheel.addSlot(months, '', now.getMonth());
        SpinningWheel.addSlot(days, 'right', now.getDate());

        SpinningWheel.setCancelAction(function() {
            document.getElementById('estDate').innerHTML = "";
            document.getElementById('estimatedDate').value = "";
        });

        SpinningWheel.setDoneAction(function() {
            var results = SpinningWheel.getSelectedValues();
            var selDate = new Date();
            selDate.setFullYear(results.keys[0]);
            document.getElementById('estDate').innerHTML = results.values.join('-');
            document.getElementById('estimatedDate').value = selDate.getFullYear() + "-" +
                    (selDate.getMonth() + 1) + "-" + selDate.getDate();
        });

        SpinningWheel.open();
    }

    // init
    $(function() {
        $("#group").on("change", function(e) {
            console.debug("group changed");
            // reload page
            var url = "<c:url value="/ip/ticketEdit.glml"/>?groupId=" + $("#group").val();
            <c:if test="${not empty ticket.id}">
                url += "&tid=${ticket.id}";
            </c:if>
            window.location = url;
        });

        $("#contact").on("change", function() {
            resetContact();
        });

        $("#location").on("change", function(e) {
            console.debug("location changed");
            // reload assignments
            resetAssignments();

            // reload custom fields
            resetCustomFields();

        });

        $("#category").on("change", function(e) {
            console.debug("category changed");
            // reload cat opts
            resetCatOpts();

            // reload assignments
            resetAssignments();

            // reload custom fields
            resetCustomFields();
        });

        $("#categoryOption").on("change", function(e) {
            console.debug("catopt changed");
            // reload assignments
            resetAssignments();

            // reload custom fields
            resetCustomFields();
        });

        // init custom fields
        resetCustomFields();

    });
</script>

</head>
<body>

<div id="topbar">
    <div id="leftnav">
        <a href="<c:url value="/ip/home.glml"/>">
            <img alt="home" src="<c:url value="/images/home.png"/>"/>
        </a>
        <c:if test="${not empty tabName}">
        <a href="<c:url value="/ip/ticketList.glml"><c:param name="mttabId" value="${mttabId}"/></c:url>">
            <c:out value="${tabName}"/>
        </a>
        </c:if>
    </div>
    <div id="rightnav">
        <a href="<c:url value="/logout.glml"/>">
            <spring:message code="header.signout"/>
        </a>
    </div>
</div>

<form action="" method="post" name="ipTicketForm">

<div id="content">

<spring:bind path="ticket.*">
    <c:if test="${not empty status.errorMessages}">
        <ul class="pageitem">
            <li class="textbox">
                <c:forEach items="${status.errorMessages}" var="error">
                    <c:if test="${not empty error}">
                        <span style="color: red">* <c:out value="${error}"/></span><br/>
                    </c:if>
                </c:forEach>
            </li>
        </ul>
    </c:if>
</spring:bind>

<c:if test="${successfulSave}">
    <ul class="pageitem">
        <li class="textbox">
            <span style="color:green">
                <spring:message code="global.saveSuccess"/>
                <img src="<c:url value="/images/ok_16.gif"/>" alt="">
            </span>
        </li>
    </ul>
</c:if>

<ul class="pageitem">
    <li class="menu">
        <span class="name1"><spring:message code="ticket.number"/> :</span>
        <span class="name2">
            <c:choose>
                <c:when test="${not empty ticket.ticketId}">
                    <c:out value="${ticket.ticketId}"/>
                </c:when>
                <c:otherwise>
                    <spring:message code="ticket.newTicket"/>
                </c:otherwise>
            </c:choose>
        </span>
    </li>
    <ehd:authorizeGroup permission="ticket.view.submittedBy" group="${ticket.group}">
    <li class="menu">
        <span class="name1"><spring:message code="ticket.submittedBy"/> :</span>
        <span class="name2">
            <c:out value="${ticket.submittedBy.firstName} ${ticket.submittedBy.lastName}"/>
        </span>
    </li>
    </ehd:authorizeGroup>
    <ehd:authorizeGroup permission="ticket.view.createdDate" group="${ticket.group}">
    <li class="menu">
        <span class="name1"><spring:message code="ticket.creationDate"/> :</span>
        <span class="name2">
            <fmt:formatDate value="${ticket.createdDate}" type="both" dateStyle="MEDIUM" timeStyle="SHORT"/>
        </span>
    </li>
    </ehd:authorizeGroup>
    <ehd:authorizeGroup permission="ticket.view.modifiedDate" group="${ticket.group}">
    <li class="menu">
        <span class="name1"><spring:message code="ticket.modifiedDate"/> :</span>
        <span class="name2">
            <fmt:formatDate value="${ticket.modifiedDate}" type="both" dateStyle="MEDIUM" timeStyle="SHORT"/>
        </span>
    </li>
    </ehd:authorizeGroup>
</ul>

<span class="graytitle">
    <spring:message code="ticket.group"/>
</span>
<ul class="pageitem">
<li class="form" id="group_li">
    <spring:bind path="ticket.group">
        <select name="${status.expression}" id="${status.expression}">
            <option value="">&nbsp;</option>
            <c:forEach var="group" items="${groups}">
                <option value="${group.id}"
                        <c:if test="${not empty status.value && status.value == group.id}">selected="selected"</c:if>>
                    <c:out value="${group.name}"/>
                </option>
            </c:forEach>
        </select>
    </spring:bind>
    <span class="arrow"></span>
</li>
</ul>

<c:choose>
<c:when test="${empty ticket.group}">
    <c:set var="grpdisplay" value="none"/>
</c:when>
<c:otherwise>
    <c:set var="grpdisplay" value="block"/>
</c:otherwise>
</c:choose>
<div id="groupHideDiv" style="display:${grpdisplay}">

<ehd:authorizeGroup permission="ticket.view.contact" group="${ticket.group}">
<span class="graytitle">
    <spring:message code="ticket.contactInfo"/>
</span>

<ul class="pageitem">
        <li class="form">
            <label for="contact" class="selectlabel"><spring:message code="ticket.contact"/>:&nbsp;&nbsp;</label>
            <spring:bind path="ticket.contact">
                <select name="${status.expression}" id="contact" <ehd:notAuthorized permission="ticket.change.contact" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>>
                    <c:forEach items="${contacts}" var="user">
                        <option value="${user.id}"
                                <c:if test="${status.value == user.id}">selected="selected"</c:if>>
                            <c:out value="${user.lastName}"/>, <c:out value="${user.firstName}"/>
                        </option>
                    </c:forEach>
                </select>
                <span class="arrow"></span>
            </spring:bind>
        </li>
    <li class="menu" id="ctctPhone">
        <c:choose>
            <c:when test="${empty ticket.contact.phoneList}">
                <a href="javascript://">
                    <img src="<c:url value="/images/telephone.png"/>" alt=""/>
                    <span class="name"><spring:message code="global.notSelected"/></span>
                </a>
            </c:when>
            <c:otherwise>
                <a href="tel:${ticket.contact.phoneList[0].number}">
                    <img src="<c:url value="/images/telephone.png"/>" alt="">
                    <span class="name">
                        <c:out value="${ticket.contact.phoneList[0].number}"/>
                        ( <c:out value="${ticket.contact.phoneList[0].phoneType}"/> )
                        <c:out value="${ticket.contact.phoneList[0].extension}"/>
                    </span>
                    <span class="arrow"></span>
                </a>
            </c:otherwise>
        </c:choose>
    </li>
    <li class="menu" id="ctctEmail">
        <c:choose>
            <c:when test="${not empty ticket.contact.email}">
                <a href="mailto:<c:out value="${ticket.contact.email}"/>">
                    <img src="<c:url value="/images/mail.png"/>" alt="">
                    <span class="name"> <c:out value="${ticket.contact.email}"/></span>
                    <span class="arrow"></span>
                </a>
            </c:when>
            <c:otherwise>
                <a href="javascript://">
                    <img src="<c:url value="/images/mail.png"/>" alt="">
                    <span class="name"><spring:message code="global.notSelected"/></span>
                </a>
            </c:otherwise>
        </c:choose>
    </li>

    <ehd:authorizeGroup permission="ticket.view.location" group="${ticket.group}">
        <li class="form" id="location_li">
            <label for="location" class="selectlabel"><spring:message code="ticket.location"/>:&nbsp;&nbsp;</label>
            <spring:bind path="ticket.location">
                <select name="${status.expression}" id="location"
                        <ehd:notAuthorized permission="ticket.change.location" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>>
                    <option value="">&nbsp;</option>
                    <c:forEach var="loc" items="${locations}">
                        <option value="${loc.id}"
                                <c:if test="${not empty status.value && status.value == loc.id}">selected="selected"</c:if>>
                            <c:out value="${loc.name}"/>
                        </option>
                    </c:forEach>
                </select>
            </spring:bind>
            <span class="arrow"></span>
        </li>
    </ehd:authorizeGroup>

    <%-- Ldap fields --%>
    <c:forEach items="${ldapFieldList}" var="ldapField">
        <c:if test="${ldapField.showOnTicket}">
            <li class="menu" id="ldapfield_${ldapField.id}">
                <span class="name1">
                    <c:out value="${ldapField.label}"/><c:if test="${empty ldapField.label}"><c:out
                        value="${ldapField.name}"/></c:if> :
                </span>
                <span class="name2">
                    <c:out value="${ldapUser.attributeMap[ldapField.name]}"/>
                </span>
            </li>
        </c:if>
    </c:forEach>
</ul>
</ehd:authorizeGroup>

<span class="graytitle">
    <spring:message code="ticket.ticketInfo"/>
</span>

    <ul class="pageitem">
        <ehd:authorizeGroup permission="ticket.view.priority" group="${ticket.group}">
        <li class="form">
            <label for="priority" class="selectlabel"><spring:message code="ticket.priority"/>:&nbsp;&nbsp;</label>
            <spring:bind path="ticket.priority">
                <select name="${status.expression}" id="priority" <ehd:notAuthorized permission="ticket.change.priority" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>>
                    <option value="">&nbsp;</option>
                    <c:forEach var="prior" items="${priorities}">
                        <option value="${prior.id}"
                                <c:if test="${prior.id eq status.value}">selected="selected"</c:if>>${prior.name}</option>
                    </c:forEach>
                </select>
            </spring:bind>
            <span class="arrow"></span>
        </li>
        </ehd:authorizeGroup>
        <ehd:authorizeGroup permission="ticket.view.status" group="${ticket.group}">
        <li class="form">
            <label for="status" class="selectlabel"><spring:message code="ticket.status"/>:&nbsp;&nbsp;</label>
            <spring:bind path="ticket.status">
                <select name="${status.expression}" id="status" <ehd:notAuthorized permission="ticket.change.status" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>>
                    <option value="">&nbsp;</option>
                    <c:forEach var="stat" items="${ticketStatus}">
                        <option value="${stat.id}"
                                <c:if test="${stat.id eq status.value}">selected="selected"</c:if>>${stat.name}</option>
                    </c:forEach>
                 </select>
            </spring:bind>
            <span class="arrow"></span>
        </li>
        </ehd:authorizeGroup>
        <ehd:authorizeGroup permission="ticket.view.estimatedDate" group="${ticket.group}">
        <li class="menu">
            <a href="javascript:openEstimatedDate();">
                <span class="name1">&nbsp;<spring:message code="ticket.estimatedDate"/>:&nbsp;</span>
                <spring:bind path="ticket.estimatedDate">
                    <span class="name2" id="estDate">
                        <fmt:formatDate  value="${ticket.estimatedDate}" type="date" dateStyle="MEDIUM"/>
                    </span>
                    <input type="hidden" name="${status.expression}" id="${status.expression}" <ehd:notAuthorized permission="ticket.change.estimatedDate" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>
                           value="<c:out value="${status.value}"/>"/>
                </spring:bind>
                <span class="arrow"></span>
            </a>
        </li>
        </ehd:authorizeGroup>
    </ul>

<ul class="pageitem">

    <ehd:authorizeGroup permission="ticket.view.category" group="${ticket.group}">
    <li class="form" id="category_li">
        <label for="category" class="selectlabel"><spring:message code="ticket.category"/>:&nbsp;&nbsp;</label>
        <spring:bind path="ticket.category">
            <select name="${status.expression}" id="category" <ehd:notAuthorized permission="ticket.change.category" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>>
                <option value="">&nbsp;</option>
                <c:forEach var="category" items="${categories}">
                    <option value="${category.id}"
                            <c:if test="${not empty status.value && status.value == category.id}">selected="selected"</c:if>>
                        <c:out value="${category.name}"/>
                    </option>
                </c:forEach>
            </select>
        </spring:bind>
        <span class="arrow"></span>
    </li>
    </ehd:authorizeGroup>
    <ehd:authorizeGroup permission="ticket.view.categoryOption" group="${ticket.group}">
    <li class="form" id="categoryOption_li">
        <label for="categoryOption" class="selectlabel"><spring:message code="ticket.categoryOption"/>:&nbsp;&nbsp;</label>
        <spring:bind path="ticket.categoryOption">
            <select name="${status.expression}" id="categoryOption" <ehd:notAuthorized permission="ticket.change.categoryOption" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized> >
                <option value="">&nbsp;</option>
                <c:forEach var="catOpt" items="${catOpts}">
                    <option value="${catOpt.id}"
                            <c:if test="${not empty status.value && status.value == catOpt.id}">selected="selected"</c:if>>
                        <c:out value="${catOpt.name}"/>
                    </option>
                </c:forEach>
            </select>
        </spring:bind>
        <span class="arrow"></span>
    </li>
    </ehd:authorizeGroup>
    <ehd:authorizeGroup permission="ticket.view.assignedTo" group="${ticket.group}">
    <li class="form" id="assignedTo_li">
        <label for="assignedTo" class="selectlabel"><spring:message code="ticket.assignedTo"/>:&nbsp;&nbsp;</label>
        <spring:bind path="ticket.assignedTo">
            <select name="${status.expression}" id="assignedTo" <ehd:notAuthorized permission="ticket.change.assignedTo" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>>
                <option value="">&nbsp;</option>
                <c:forEach var="urg" items="${assignments}">
                    <option value="${urg.id}"
                            <c:if test="${not empty status.value && status.value == urg.id}">selected="selected"</c:if>>
                        <c:choose>
                            <c:when test="${not empty urg.userRole}">
                                <c:out value="${urg.userRole.user.lastName}"/>, <c:out
                                    value="${urg.userRole.user.firstName}"/>
                            </c:when>
                            <c:otherwise>
                                <spring:message code="ticketList.ticketPool"/>
                            </c:otherwise>
                        </c:choose>
                    </option>
                </c:forEach>
            </select>
        </spring:bind>
        <span class="arrow"></span>
    </li>
    </ehd:authorizeGroup>
</ul>
<ul class="pageitem" id="cfUl"></ul>

<ul class="pageitem">
    <ehd:authorizeGroup permission="ticket.view.subject" group="${ticket.group}">
    <li class="textbox">
        <span class="name"><spring:message code="ticket.subject"/></span>
        <spring:bind path="ticket.subject">
            <textarea id="${status.expression}" name="${status.expression}" placeholder="note" rows="5" <ehd:notAuthorized permission="ticket.change.subject" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>><c:out value="${status.value}"/></textarea>
        </spring:bind>
    </li>
    </ehd:authorizeGroup>
    <ehd:authorizeGroup permission="ticket.view.note" group="${ticket.group}">
    <li class="textbox">
        <span class="name"><spring:message code="ticket.note"/></span>
        <spring:bind path="ticket.note">
            <textarea id="${status.expression}" name="${status.expression}" placeholder="note" rows="5"<ehd:notAuthorized permission="ticket.change.note" group="${ticket.group}"> disabled="disabled"</ehd:notAuthorized>><c:out value="${status.value}"/></textarea>
        </spring:bind>
    </li>
    </ehd:authorizeGroup>
</ul>
</div>
<ul class="pageitem">
    <li class="form"><input type="submit" value="<spring:message code="global.save"/>"/></li>
</ul>

<c:if test="${not empty ticket.id}">
    <ul class="pageitem">
        <li class="menu">
            <a href="<c:url value="/ip/ticketHistoryList.glml"><c:param name="tid" value="${ticket.id}"/><c:param name="mttabId" value="${mttabId}"/></c:url>">
                <img src="<c:url value="/images/documents_14.png"/>" alt=""/>
                <span class="name"> <spring:message code="global.comments"/> </span>
                <span class="arrow"></span>
            </a>
        </li>
    </ul>
</c:if>



</div>
</form>

<div id="footer"><a href="http://www.grouplink.net">Powered by GroupLink</a></div>

</body>

</html>
