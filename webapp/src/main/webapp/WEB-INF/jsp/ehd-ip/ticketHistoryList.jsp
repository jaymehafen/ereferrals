<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="ticket" type="net.grouplink.ehelpdesk.domain.Ticket"--%>
<%--@elvariable id="mttabId" type="java.lang.Integer"--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

    <title><spring:message code="global.systemName"/></title>

    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="index,follow" name="robots"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no"/>

    <link href="<c:url value="/images/eReferrals_logo.png"/>" rel="apple-touch-icon"/>
    <link rel="stylesheet" href="<c:url value="/js/core/core-css/gigpress.css"/>" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<c:url value="/js/default/style-compressed.css"/>" type="text/css" media="screen"/>
    <link href="<c:url value="/css/iwk_style.css"/>" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="<c:url value="/js/jquery/jquery.js?ver=1.3.2"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/core/core.js?ver=1.9"/>"></script>
    <script type="text/javascript" src="<c:url value="/js/iwk_functions.js"/>"></script>

</head>

<body>

<div id="topbar">
    <div id="leftnav">
        <a href="<c:url value="/ip/home.glml"/>">
            <img alt="home" src="<c:url value="/images/home.png"/>"/>
        </a>
        <a href="<c:url value="/ip/ticketEdit.glml"><c:param name="tid" value="${ticket.id}"/><c:param name="mttabId" value="${mttabId}"/></c:url>">
            <spring:message code="ticket.number"/>: ${ticket.id}
        </a>
    </div>
    <div id="rightnav">
        <a href="<c:url value="/logout.glml"/>">
            <spring:message code="header.signout"/>
        </a>
    </div>
</div>

<div class="content">

    <ul class="pageitem">
        <li class="store">
            <a href="<c:url value="/ip/ticketEdit.glml"><c:param name="tid" value="${ticket.id}"/><c:param name="mttabId" value="${mttabId}"/></c:url>">
                <div class="topper">
                    <table class="topper" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="cellleft">
                                <span class="tid">#${ticket.id}</span>
                                <c:choose>
                                    <c:when test="${ticket.priority.icon == 'text'}">
                                        <c:out value="${ticket.priority.name}"/>
                                    </c:when>
                                    <c:when test="${ticket.priority.icon == 'blank'}">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="<c:url value="/images/${ticket.priority.icon}ball_12.png"/>"
                                             title="<c:out value="${ticket.priority.name}"/>" alt="">
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="cellleft">
                                <c:if test="${not empty ticket.ticketTemplate}">
                                    <img alt="" src="<c:url value="/images/form_16.png"/>"
                                         title="<spring:message code="ticketList.hasTicketTemplate" />"/>
                                </c:if>
                                <c:if test="${fn:length(ticket.subtickets) > 0}">
                                    <img alt="" src="<c:url value="/images/form_down_16.png"/>"
                                         title="<spring:message code="ticketList.hasSubTickets" />"/>
                                </c:if>
                                <c:if test="${not empty ticket.parent}">
                                    <img alt="" src="<c:url value="/images/form_up_16.png"/>"
                                         title="<spring:message code="ticketList.hasParentTicket" />"/>
                                </c:if>
                                <c:set var="attchCount" value="${fn:length(ticket.attachments)}"/>
                                <c:forEach items="${ticket.ticketHistory}" var="tHist">
                                    <c:set var="attchCount"
                                           value="${attchCount + fn:length(tHist.attachments)}"/>
                                </c:forEach>
                                <c:if test="${attchCount > 0}">
                                    <img src="<c:url value="/images/clip_14.png"/>"
                                         alt="" title="<spring:message code="ticket.attachments" />"/>
                                </c:if>
                            </td>
                            <td class="cellright" align="right"><c:out value="${ticket.status.name}"/></td>
                        </tr>
                    </table>
                </div>
                <div class="midder">
                    <table class="midder" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="cellleft">
                                <c:out value="${ticket.contact.firstName}"/> <c:out
                                    value="${ticket.contact.lastName}"/>
                            </td>
                            <td class="cellright" align="right">
                                <spring:message code="ticket.creationDate"/>:
                                <fmt:formatDate value="${ticket.createdDate}" type="both"
                                                dateStyle="SHORT"
                                                timeStyle="SHORT"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="cellleft">
                                <c:out value="${ticket.location.name}"/>
                            </td>
                            <td class="cellright" align="right">
                                <spring:message code="ticket.modifiedDate"/>:
                                <fmt:formatDate value="${ticket.modifiedDate}" type="both"
                                                dateStyle="SHORT"
                                                timeStyle="SHORT"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="bottomer">
                    <table>
                        <tr>
                            <td class="cellleft">
                                <spring:message code="ticket.subject"/>:
                                <span style="color:#4c4c4c"> <c:out value="${ticket.subject}"/></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="cellleft">
                                <spring:message code="ticket.note"/>:
                                <span style="color:#4c4c4c"><c:out value="${ticket.note}"/></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </a>
        </li>
    </ul>

    <ul class="pageitem">
        <li class="menu" style="height:30px;">
            <a class="noeffect" onclick="iWebkit.popup('createTHist');">
                <img src="<c:url value="/images/doc_add_16_hot.gif"/>" alt=""/>
                <span class="name"> <spring:message code="ticket.addComment"/></span>
                <span class="arrow"></span>
            </a>
        </li>
    </ul>

    <span class="graytitle">
        <spring:message code="global.comments"/>
    </span>

    <c:if test="${fn:length(ticket.ticketHistoryList) > 0}">
        <c:forEach items="${ticket.ticketHistoryList}" var="thist">

            <fmt:formatDate var="monthLetters" value="${thist.createdDate}" pattern="MMM"/>
            <fmt:formatDate var="monthNumber" value="${thist.createdDate}" pattern="MM"/>
            <fmt:formatDate var="dateNumber" value="${thist.createdDate}" pattern="dd"/>

            <div class="post" id="thist-${thist.id}">
                <script type="text/javascript">
                    $wptouch(document).ready(function() {
                        $wptouch("a#arrow-${thist.id}").click(function() {
                            $wptouch(this).toggleClass("post-arrow-down");
                            $wptouch('#entry-${thist.id}').fadeToggle(500);
                        });
                    });
                </script>

                <a class="post-arrow" id="arrow-${thist.id}" href="javascript: return false;"></a>

                <div class="calendar">
                    <div class="cal-month month-${monthNumber}"> ${monthLetters} </div>
                    <div class="cal-date"> ${dateNumber} </div>
                </div>


                    <%--<a href="<c:url value="/ip/ticketHistoryEdit.glml"><c:param name="thid" value="${thist.id}"/></c:url>">--%>
                <a class="h2" href="javascript: return false;">
                    <c:out value="${thist.subject}"/>
                </a>

                <div class="post-author">
                    <span class="lead"><spring:message code="ticket.submittedBy"/></span>
                    <c:choose>
                        <c:when test="${thist.user.id == 0}">
                            <spring:message code="global.systemUserName"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${thist.user.firstName}"/>
                            <c:out value="${thist.user.lastName}"/>
                        </c:otherwise>
                    </c:choose>
                </div>
                <br/>
                <fmt:formatDate value="${thist.createdDate}" type="time" timeStyle="default"/>

                <div class="clearer"></div>
                <div id="entry-${thist.id}" style="display:none" class="mainentry full-justified">
                    <p><ehd:formatHistoryNote ticketHistory="${thist}"/></p>
                    <a class="read-more"
                       href="<c:url value="/ip/ticketHistoryEdit.glml"><c:param name="thid" value="${thist.id}"/><c:param name="mttabId" value="${mttabId}"/></c:url>">
                        <spring:message code="ticket.hist.edit"/>
                    </a>
                </div>
            </div>
        </c:forEach>
    </c:if>
    <c:if test="${fn:length(ticket.ticketHistoryList) == 0}">
        <br/><br/>
        <div style="font-style: italic;"><spring:message code="ticket.noComments"/></div>
    </c:if>

    <div id="footer"><a href="http://www.grouplink.net">Powered by GroupLink</a></div>

    <div id="createTHist" class="popup">
        <div id="frame" class="confirm_screen">
            <a href="<c:url value="/ip/ticketHistoryEdit.glml"/>?tid=${ticket.id}&thtype=0&mttabId=${mttabId}">
                <span class="gray"><spring:message code="ticket.addComment"/></span>
            </a>
            <a href="<c:url value="/ip/ticketHistoryEdit.glml"/>?tid=${ticket.id}&thtype=3&mttabId=${mttabId}">
                <span class="gray"><spring:message code="phone.callReceived"/></span>
            </a>
            <a href="<c:url value="/ip/ticketHistoryEdit.glml"/>?tid=${ticket.id}&thtype=4&mttabId=${mttabId}">
                <span class="gray"><spring:message code="phone.callPlaced"/></span>
            </a>
            <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                <c:if test="${applicationScope.groupWiseIntegration || applicationScope.groupWiseIntegration6x}">

                    <a href="<c:url value="/ip/ticketHistoryEdit.glml"/>?tid=${ticket.id}&thtype=1&mttabId=${mttabId}">
                        <span class="gray"><spring:message code="collaboration.apptSchedule"/></span>
                    </a>
                    <a href="<c:url value="/ip/ticketHistoryEdit.glml"/>?tid=${ticket.id}&thtype=2&mttabId=${mttabId}">
                        <span class="gray"><spring:message code="collaboration.taskSchedule"/></span>
                    </a>
                </c:if>
            </authz:authorize>

            <a class="noeffect" onclick="iWebkit.closepopup(event);"><span class="black">Cancel</span></a>
        </div>
    </div>

</div>

</body>
</html>