<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="apptTypes" type="java.util.List<net.grouplink.ehelpdesk.domain.AppointmentType>"--%>
<%--@elvariable id="ticketHistory" type="net.grouplink.ehelpdesk.domain.TicketHistory"--%>
<%--@elvariable id="showButton" type="java.lang.String"--%>
<%--@elvariable id="justCancel" type="java.lang.Boolean"--%>
<%--@elvariable id="successfulSave" type="java.lang.Boolean"--%>
<%--@elvariable id="timerStatus" type="java.lang.String"--%>


<html>
<head>
    <title><spring:message code="ticket.addCommentTitle" arguments="${ticketHistory.ticket.id}"/> </title>

    <script type="text/javascript" src="<c:url value="/dwr/interface/TicketService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
        });

        var inputIndex = 1;
        function fileSelected() {
            var createNew = true;
            for (var i = 0; i < inputIndex; i++) {
                var uploadFiles = document.getElementsByName('attchmnt[' + i + ']')[0];
                if (uploadFiles && uploadFiles.value.length == 0) {
                    createNew = false;
                }
            }
            if (createNew) {
                addFileSelect();
            }
        }

        function addFileSelect() {
            var newNode = document.createElement('input');
            var breakNode = document.createElement('br');
            var name = 'attchmnt[' + inputIndex + ']';
            var fileLoader = dojo.byId('fileLoader');

            newNode.setAttribute('type', 'file');
            newNode.setAttribute('name', name);
            newNode.setAttribute('id', name);
            newNode.setAttribute('size', '50');
            newNode.setAttribute('onchange', 'fileSelected()');
            fileLoader.appendChild(newNode);
            fileLoader.appendChild(breakNode);
            inputIndex++;

            //for IE to work
            var node = document.getElementById(name);
            if (node.attachEvent) {
                node.attachEvent('onchange', fileSelected);
                node.size = 50;
            }
        }

        function busySearch(){
            // Hide the div first, this is for when 'Busy Search is clicked a second time ie. hide the results until
            // after validation.
            hideResults();

            var to = dijit.byId("recipients");
            var cc = dijit.byId("cc");
            var bc = dijit.byId("bc");

            var validTo = to.getValue().length==0 || (to.getValue().length>0 && to.isValid());
            var validCC = cc.getValue().length==0 || (cc.getValue().length>0 && cc.isValid());
            var validBC = bc.getValue().length==0 || (bc.getValue().length>0 && bc.isValid());

            var startDate = dijit.byId("apptStartDate").getValue();
            var startTime = dijit.byId("apptStartTime").getValue();
            var durationAmount = dijit.byId("durationAmount").getValue();
            var durationUnit = dijit.byId("durationUnit").getValue();

            // Combine To CC and BC into a single field
            var recips = to.getValue() + ";" +cc.getValue() +";" + bc.getValue();

            // Validate appointment fields before performing the busy search
            var errMsg = "";
            if(recips == ";;"){
                errMsg += "<spring:message code="mail.validation.addressEmpty"/>\n";
            }
            if(!validTo || !validCC || !validBC){
                errMsg += "<spring:message code="mail.validation.addressRecipient"/>\n";
            }
            if(startDate == null) {
                errMsg += "<spring:message code="mail.validation.startDate"/>\n";
            }
            if(startTime == null) {
                errMsg += "<spring:message code="mail.validation.startTime"/>\n";
            }
            if(isNaN(durationAmount)){
                errMsg += "<spring:message code="mail.validation.durationAmount"/>\n";
            }

            if(errMsg.length > 0){
                alert(errMsg);
                return false;
            }

            TicketService.busySearch(recips, startDate, startTime, durationAmount, durationUnit, busyResults);

            return false;
        }

        function busyResults(freeBusyResult){
            var fbInfoList = freeBusyResult.freeBusyInfo;
            var html = "";
            html += '<table width="100%" id="busyResultTable" cellpadding="2" class="busySearch">';
            html += '<thead><tr class="title"><td>&nbsp;</td>';
            html += '<td class="borderedCell"><spring:message code="collaboration.appointment.acceptLevel"/></td>';
            html += '<td class="borderedCell"><spring:message code="collaboration.appointment.startDate"/></td>';
            html += '<td class="borderedCell"><spring:message code="collaboration.appointment.endDate"/></td>';
            html += '<td><spring:message code="ticket.subject"/></td>';
            html += '</tr></thead>';
            html += '<tbody id="busyResultTableBody">';

            for(var fbInfoIndex in fbInfoList){
                var fbInfo = fbInfoList[fbInfoIndex];
                html += "<tr>";
                html += "<td colspan='5' style='font-weight:bold;border-top:1px solid lightgray;'>"+fbInfo.emailAddress+"</td>";
                html += "</tr>";
                for(var fbBlockIndex in fbInfo.freeBusyBlocks){
                    var fbBlock = fbInfo.freeBusyBlocks[fbBlockIndex];
                    html += "<tr>";
                    html += "<td style='font: 11pt Arial, Myriad, Tahoma, Verdana, sans-serif;'>&nbsp;&#149;</td>";
                    html += "<td class='borderedCell'>";
                    if(fbBlock.acceptLevel == "Busy"){
                        html += "<spring:message code="apptType.busy"/>";
                    } else if(fbBlock.acceptLevel == "Free"){
                        html += "<spring:message code="apptType.free"/>";
                    } else if(fbBlock.acceptLevel == "OutOfOffice"){
                        html += "<spring:message code="apptType.outofoffice"/>";
                    } else if(fbBlock.acceptLevel == "Tentative"){
                        html += "<spring:message code="apptType.tentative"/>";
                    } else {
                        html += fbBlock.acceptLevel;
                    }
                    html += "</td>";
                    html += "<td class='borderedCell'>"+fbBlock.startDate.toLocaleDateString() + " " + fbBlock.startDate.toLocaleTimeString()+"</td>";
                    html += "<td class='borderedCell'>"+fbBlock.endDate.toLocaleDateString() + " " + fbBlock.endDate.toLocaleTimeString()+"</td>";
                    html += "<td>"+parseSubject(fbBlock.subject)+"</td>";
                    html += "</tr>";
                }
            }

            html += '</tbody> </table>';

            // get a handle on the tables tbody that the results will be displayed in.
            var tbody = document.getElementById("busySearchDiv");
            tbody.innerHTML = html;
            document.getElementById("busySearchDiv").style.display = "block";
        }

        function hideResults(){
            document.getElementById("busySearchDiv").style.display = "none";
            document.getElementById("hideLink").style.display = "none";
        }

        function parseSubject(s){
            if(s == null) return "";
            else return s;
        }

        function saveComment(){
            document.forms[0].submit();
            return false;
        }

        function cancelComment(){
            window.close();
            return false;
        }

        function init(){
            <c:if test="${successfulSave}">
            window.opener.refreshSelf("${timerStatus}");
            cancelComment();
            </c:if>
        }

        dojo.addOnLoad(init);
    </script>
</head>
<body>
<article class="white-bg">
    <div dojoType="dijit.layout.BorderContainer" gutters="false" id="mainDiv">
        <div dojoType="dijit.layout.ContentPane" region="center">
            <form:form name="ticketHistoryForm" id="ticketHistoryForm" commandName="ticketHistory" enctype="multipart/form-data" cssClass="form">
                <spring:hasBindErrors name="ticketHistory">
                    <p class="error no-margin">
                        <form:errors path="*"/>
                    </p>
                </spring:hasBindErrors>
                <c:if test="${ticketHistory.commentType eq 1 or ticketHistory.commentType eq 2}">
                    <div class="with-margin">
                        <p>
                            <label><spring:message code="collaboration.appointment.from"/></label>
                            <c:out value="${sessionScope['User'].firstName}"/> <c:out value="${sessionScope['User'].lastName}"/>
                        </p>
                        <p>
                            <form:label path="recipients"><spring:message code="collaboration.appointment.to"/></form:label>
                            <form:input path="recipients" value="${sessionScope['User'].email}" dojoType="dijit.form.ValidationTextBox" maxlength="255" trim="true" regExpGen="dojox.validate.regexp.emailAddressList" cssStyle="width:98%;"/>
                        </p>
                        <div class="columns">
                            <div class="colx2-left">
                                <p>
                                    <form:label path="cc"><spring:message code="mail.cc"/></form:label>
                                    <form:input path="cc" dojoType="dijit.form.ValidationTextBox" trim="true" regExpGen="dojox.validate.regexp.emailAddressList"/>
                                </p>
                                <p>
                                    <form:label path="place"><spring:message code="collaboration.appointment.place"/></form:label>
                                    <form:input path="place" dojoType="dijit.form.TextBox" trim="true"/>
                                </p>
                            </div>
                            <div class="colx2-right">
                                <p>
                                    <form:label path="bc"><spring:message code="mail.bcc"/></form:label>
                                    <form:input path="bc" dojoType="dijit.form.ValidationTextBox" trim="true" regExpGen="dojox.validate.regexp.emailAddressList"/>
                                </p>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="colx2-left">
                                <p>
                                    <label for="apptStartDate"><spring:message code="collaboration.appointment.startDate"/></label>
                                    <input type="text" name="apptStartDate" id="apptStartDate"
                                           dojotype="dijit.form.DateTextBox"
                                           value="<fmt:formatDate pattern="yyyy-MM-dd" value="${ticketHistory.startDate}"/>">
                                </p>
                                <p>
                                    <form:label path="durationAmount"><spring:message code="collaboration.appointment.duration"/></form:label>
                                    <form:input path="durationAmount" dojoType="dijit.form.NumberSpinner" constraints="{min:1,max:100,places:0}" cssStyle="width:40px"/>
                                    <form:select path="durationUnit" dojoType="dijit.form.FilteringSelect" cssStyle="width:100px">
                                        <form:option value="collaboration.duration.minutes"><spring:message code="collaboration.duration.minutes"/></form:option>
                                        <form:option value="collaboration.duration.hours"><spring:message code="collaboration.duration.hours"/></form:option>
                                        <form:option value="collaboration.duration.days"><spring:message code="collaboration.duration.days"/></form:option>
                                    </form:select>
                                </p>
                                <p>
                                    <form:label path="apptType"><spring:message code="collaboration.appointment.markAs"/></form:label>
                                    <form:select path="apptType" dojoType="dijit.form.FilteringSelect">
                                        <c:forEach var="at" items="${apptTypes}">
                                            <form:option value="${at.id}"><spring:message code="${at.name}"/></form:option>
                                        </c:forEach>
                                    </form:select>
                                </p>
                            </div>
                            <div class="colx2-right">
                                <p>
                                    <label for="apptStartTime"><spring:message code="collaboration.appointment.startTime"/></label>
                                    <input type="text" name="apptStartTime" id="apptStartTime" dojotype="dijit.form.TimeTextBox"
                                           constraints="{clickableIncrement:'T00:15:00', visibleIncrement:'T00:15:00', visibleRange:'T03:00:00'}"
                                           value="T<fmt:formatDate pattern="HH:mm:ss" value="${ticketHistory.startDate}"/>">
                                </p>
                            </div>
                        </div>
                        <div>
                            <button dojotype="dijit.form.Button" type="button" onclick="busySearch();" type="button"><spring:message code="collaboration.busySearch"/></button>
                            <span id="hideLink" style="display:none"><a href="javascript:hideResults();"><spring:message code="global.hide"/></a> </span>
                            <br>
                            <div id="busySearchDiv" style="display:none;border:1px solid lightgray;overflow:auto;width:98%"></div>
                        </div>
                    </div>
                </c:if>
                <p>
                    <form:label path="subject"><spring:message code="ticket.subject"/></form:label>
                    <form:input path="subject" dojoType="dijit.form.TextBox" maxlength="255" cssStyle="width: 98%"/>
                </p>
                <p>
                    <form:label path="note"><spring:message code="ticket.note"/></form:label>
                    <form:textarea path="note" dojoType="dijit.form.Textarea" cssStyle="width: 98%; height: 140px"/>
                </p>
                <div class="with-margin">
                    <label><spring:message code="global.attachFile"/></label>
                    <div id="fileLoader">
                        <input type="file" name="attchmnt[0]" size="50" onchange="fileSelected();"/><br/>
                    </div>
                </div>
                <ehd:authorizeGroup permission="ticket.view.notifyTech" group="${ticketHistory.ticket.group}">
                <p>
                    <label class="inline"><spring:message code="ticket.notify"/></label>&nbsp;
                    <ehd:authorizeGroup permission="ticket.view.notifyTech" group="${ticketHistory.ticket.group}">
                        <form:checkbox path="notifyTech" dojoType="dijit.form.CheckBox" disabled="${!ehd:hasGroupPermission('ticket.change.notifyTech', ticketHistory.ticket.group, pageContext)}"/>
                        <form:label path="notifyTech" cssClass="inline"><spring:message code="ROLE_TECHNICIAN"/></form:label>
                    </ehd:authorizeGroup>

                    <ehd:authorizeGroup permission="ticket.view.notifyUser" group="${ticketHistory.ticket.group}">
                        <form:checkbox path="notifyUser" dojoType="dijit.form.CheckBox" disabled="${!ehd:hasGroupPermission('ticket.change.notifyUser', ticketHistory.ticket.group, pageContext)}"/>
                        <form:label path="notifyUser" cssClass="inline"><spring:message code="ROLE_USER"/></form:label>
                    </ehd:authorizeGroup>
                </p>
                </ehd:authorizeGroup>
                <c:if test="${ticketHistory.commentType != 1 and ticketHistory.commentType != 2}">
                    <p>
                        <form:label path="cc"><spring:message code="mail.cc"/></form:label>
                        <form:input path="cc" dojoType="dijit.form.ValidationTextBox" maxlength="255" trim="true" regExpGen="dojox.validate.regexp.emailAddressList" cssStyle="width: 98%"/>
                    </p>
                </c:if>
                <button dojoType="dijit.form.Button" type="button" onclick="saveComment();">
                    <spring:message code="global.save"/>
                </button>
                <button dojoType="dijit.form.Button" type="button" onclick="cancelComment();">
                    <c:choose>
                        <c:when test="${justCancel}">
                            <spring:message code="global.cancel"/>
                        </c:when>
                        <c:otherwise>
                            <spring:message code="global.submitNoComments"/>
                        </c:otherwise>
                    </c:choose>
                </button>
            </form:form>
        </div>
    </div>
</article>
</body>
</html>
