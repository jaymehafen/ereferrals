<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <title><spring:message code="surveys.survey" /></title>

    <spring:theme code="css" var="cssfile"/>

    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>
    <link rel="stylesheet" href="<c:url value="/css/surveys.css"/>" type="text/css">
</head>

<body>
<div id="survey">
    <form id="frmDefaultSurvey" action="" method="post">
        <div id="messages" class="no-margin">
            <spring:hasBindErrors name="surveyDataCollection">
                <spring:bind path="surveyDataCollection.*">
                    <c:if test="${status.errors.errorCount > 0}">
                        <ul id="bindErrors" class="message error no-margin">
                            <c:forEach var="error" items="${status.errors.allErrors}">
                                <li><spring:message message="${error}"/></li>
                            </c:forEach>
                        </ul>
                    </c:if>
                </spring:bind>
            </spring:hasBindErrors>
        </div>
        <table class="table">
            <tr>
                <td class="question">
                    <strong><spring:message code="surveys.default.question.1"/></strong>
                </td>
                <td>
                    <input type="radio" name="grpQuestion1" value="<spring:message code="global.no"/>"/><spring:message code="global.no"/>
                    <input type="radio" name="grpQuestion1" value="<spring:message code="global.yes"/>"/><spring:message code="global.yes"/>
                </td>
            </tr>
            <tr>
                <td class="question">
                    <strong><spring:message code="surveys.default.question.2"/></strong>
                </td>
                <td>
                    <input type="radio" name="grpQuestion2" value="<spring:message code="global.no"/>"/><spring:message code="global.no"/>
                    <input type="radio" name="grpQuestion2" value="<spring:message code="global.yes"/>"/><spring:message code="global.yes"/>
                </td>
            </tr>
            <tr>
                <td class="question">
                    <strong><spring:message code="surveys.default.question.3"/></strong>
                </td>
                <td>
                    <input type="radio" name="grpQuestion3" value="1"/>1
                    <input type="radio" name="grpQuestion3" value="2"/>2
                    <input type="radio" name="grpQuestion3" value="3"/>3
                    <input type="radio" name="grpQuestion3" value="4"/>4
                    <input type="radio" name="grpQuestion3" value="5"/>5
                </td>
            </tr>
        </table>

        <br>
        <div id="submit_btn">
            <input type="submit" value="Submit" name="btnSubmit"/>
        </div>
    </form>

</div>
<br>
<br>
</body>
