<ul>
    <li><a href="errorPageTest/400">400</a></li>
    <li><a href="errorPageTest/401">401</a></li>
    <li><a href="errorPageTest/403">403</a></li>
    <li><a href="errorPageTest/404">404</a></li>
    <li><a href="errorPageTest/500">500</a></li>
    <li><a href="errorPageTest/503">503</a></li>
    <li><a href="errorPageTest/servletException">ServletException</a></li>
    <li><a href="errorPageTest/runtimeException">RuntimeException</a></li>
    <li><a href="errorPageTest/accessDeniedException">AccessDeniedException</a></li>
</ul>
