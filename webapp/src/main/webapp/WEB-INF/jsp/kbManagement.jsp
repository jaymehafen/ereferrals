<%--@elvariable id="listHolder" type="java.util.Map"--%>
<%--@elvariable id="groupList" type="java.util.List<net.grouplink.ehelpdesk.domain.Group>"--%>
<%--@elvariable id="refreshUrl" type="java.lang.String"--%>
<%--@elvariable id="groupId" type="java.lang.Integer"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title>
        <spring:message code="kb.knowledgeBaseArticle"/>
    </title>

    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>" type="text/css">

<script type="text/javascript">
        dojo.addOnLoad(function() {
            var kbStore = new dojox.data.QueryReadStore({
                url: "<c:url value="/stores/kbArticles.json"/>"
            });

            function formatIsPrivateCol(value, rowIndex, cell) {
                if(value) {
                    return "<spring:message code="kb.private"/>";
                } else {
                    return "<spring:message code="kb.public"/>";
                }
            }

            var grid = new dojox.grid.DataGrid({
                store: kbStore,
                structure: [
                    {name:"<spring:message code="kb.articleNumber"/>", field: "id", width: "6%"},
                    {name:"<spring:message code="global.group"/>", field: "group", width: "auto"},
                    {name:"<spring:message code="kb.subject"/>", field: "subject", width: "50%"},
                    {name:"<spring:message code="kb.type"/>", field: "isPrivate", width: "6%", formatter: formatIsPrivateCol},
                    {name:"<spring:message code="kb.timesViewed"/>", field: "timesViewed", width: "6%"},
                    {name:"<spring:message code="kb.lastModified"/>", field: "date", width: "10%"},
                    {name:"<spring:message code="global.actions"/>", field: "_item", width: "50px", formatter: formatActions}
                ],
                editable: false,
                autoHeight: 20,
                noDataMessage: "<spring:message code="kbManagement.noArticles" javaScriptEscape="true"/>"
            }, "kbGrid");

            dojo.connect(grid, "onRowDblClick", function(e) {
                var kbId = e.grid.getItem(e.rowIndex).i.id;
                window.location = "<c:url value="/kb/kbView.glml"/>?kbId=" + kbId;
            });

            function formatActions(value, rowIndex, cell) {
                var kbId = value.i.id;

                return '<a title="<spring:message code="global.edit"/>" href="<c:url value="/kb/kbView.glml"/>?kbId=' + kbId + '"> <img style="width:15px;height:15px;padding-right:5px;" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/></a>';
            }

            function setGridQuery() {
                var searchText = dojo.byId("searchKb").value;
                grid.setQuery({kbQuery: searchText});
            }

            dojo.connect(dojo.byId("searchKbBtn"), "onclick", function(e) {
                setGridQuery();
                e.preventDefault();
            });

            dojo.connect(dojo.byId("searchKb"), "onkeyup", function(e) {
                var keynum;
                if(window.event) { //IE
                    keynum = e.keyCode;
                }
                else if(e.which) { //The rest of the world
                    keynum = e.which;
                }
                if(keynum == 13 || keynum == 3) {
                    setGridQuery();
                }
            });

            grid.startup();
        });
    </script>
</head>

<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="kbManagement.title"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons form">
                <li class="form">
                    <input type="text" id="searchKb" name="searchKb">
                </li>
                <li>
                    <a id="searchKbBtn" href="javascript://">
                        <spring:message code="global.search"/>
                    </a>
                </li>

                <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER,ROLE_TECHNICIAN" >
                    <li class="sep"></li>
                    <li>
                        <a href="<c:url value="/kbManagement/kbEdit.glml" />">
                            <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                            <spring:message code="kb.newKb" />
                        </a>
                    </li>
                </authz:authorize>
            </ul>
        </div>
        <div class="no-margin">
            <div id="kbGrid"></div>
        </div>
    </div>
</section>
</body>

