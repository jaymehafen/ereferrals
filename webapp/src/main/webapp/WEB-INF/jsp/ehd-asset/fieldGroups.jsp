<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.fieldGroups"/></title>

    <script type="text/javascript">

        function viewFieldGroup(fieldGroupId) {
            var url = '<c:url value="/asset/editFieldGroup.glml"/>';
            if (fieldGroupId != null) url += '?fieldGroupId=' + fieldGroupId;
            window.location.href = url;
        }

        function deleteFieldGroup(fieldGroupId) {
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/asset/deleteFieldGroup.glml"/>?fieldGroupId=' + fieldGroupId;
            }
        }

        function init() {
        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>
<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="assetTracker.fieldGroups"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/asset/editFieldGroup.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="assetTracker.fieldGroupNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <div class="mini-infos">
            <spring:message code="assetTracker.fieldGroupExplanation"/>
        </div>

        <div class="no-margin">

        <table class="table">
            <thead>
            <tr>
                <th scope="col"><spring:message code="global.name"/></th>
                <th scope="col"><spring:message code="global.description"/></th>
                <th scope="col"><spring:message code="assetTracker.assetTypes"/></th>
                <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${fieldGroups}" var="fieldGroup">
                <tr>
                    <td><c:out value="${fieldGroup.name}"/></td>
                    <td><c:out value="${fieldGroup.description}"/></td>
                    <td>
                        <ul class="keywords">
                            <c:forEach items="${fieldGroup.assetTypes}" var="assetType" varStatus="feStatus">
                                <li><c:out value="${assetType.name}"/></li>
                            </c:forEach>
                        </ul>
                    </td>
                    <td class="table-actions">
                        <a href="javascript:viewFieldGroup(<c:out value="${fieldGroup.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png" />"/></a>
                        <a href="javascript:deleteFieldGroup(<c:out value="${fieldGroup.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png" />"/></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </div>
    </div>
</section>
</body>

