<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.import"/></title>
    <script type="text/javascript">

        var transFields = {};
        transFields["asset.assetnumber"] = "<spring:message code="asset.assetnumber"/>";
        transFields["asset.name"] = "<spring:message code="asset.name"/>";
        transFields["asset.type"] = "<spring:message code="asset.type"/>";
        transFields["asset.owner"] = "<spring:message code="asset.owner"/>";
        transFields["asset.vendor"] = "<spring:message code="asset.vendor"/>";
        transFields["asset.assetlocation"] = "<spring:message code="asset.assetlocation"/>";
        transFields["asset.acquisitiondate"] = "<spring:message code="asset.acquisitiondate"/>";
        transFields["asset.location"] = "<spring:message code="asset.location"/>";
        transFields["asset.group"] = "<spring:message code="asset.group"/>";
        transFields["asset.category"] = "<spring:message code="asset.category"/>";
        transFields["asset.categoryoption"] = "<spring:message code="asset.categoryoption"/>";
        transFields["asset.manufacturer"] = "<spring:message code="asset.manufacturer"/>";
        transFields["asset.modelnumber"] = "<spring:message code="asset.modelnumber"/>";
        transFields["asset.serialnumber"] = "<spring:message code="asset.serialnumber"/>";
        transFields["asset.description"] = "<spring:message code="asset.description"/>";
        transFields["asset.status"] = "<spring:message code="asset.status"/>";
        transFields["accountinginfo.ponumber"] = "<spring:message code="accountinginfo.ponumber"/>";
        transFields["accountinginfo.accountnumber"] = "<spring:message code="accountinginfo.accountnumber"/>";
        transFields["accountinginfo.accumulatednumber"] = "<spring:message code="accountinginfo.accumulatednumber"/>";
        transFields["accountinginfo.expensenumber"] = "<spring:message code="accountinginfo.expensenumber"/>";
        transFields["accountinginfo.acquisitionvalue"] = "<spring:message code="accountinginfo.acquisitionvalue"/>";
        transFields["accountinginfo.leasenumber"] = "<spring:message code="accountinginfo.leasenumber"/>";
        transFields["accountinginfo.leaseduration"] = "<spring:message code="accountinginfo.leaseduration"/>";
        transFields["accountinginfo.leasefrequency"] = "<spring:message code="accountinginfo.leasefrequency"/>";
        transFields["accountinginfo.leasefrequencyprice"] = "<spring:message code="accountinginfo.leasefrequencyprice"/>";
        transFields["accountinginfo.disposalmethod"] = "<spring:message code="accountinginfo.disposalmethod"/>";
        transFields["accountinginfo.disposaldate"] = "<spring:message code="accountinginfo.disposaldate"/>";
        transFields["accountinginfo.insurancecategory"] = "<spring:message code="accountinginfo.insurancecategory"/>";
        transFields["accountinginfo.replacementvaluecategory"] = "<spring:message code="accountinginfo.replacementvaluecategory"/>";
        transFields["accountinginfo.replacementvalue"] = "<spring:message code="accountinginfo.replacementvalue"/>";
        transFields["accountinginfo.maintenancecost"] = "<spring:message code="accountinginfo.maintenancecost"/>";
        transFields["accountinginfo.depreciationmethod"] = "<spring:message code="accountinginfo.depreciationmethod"/>";
        transFields["accountinginfo.leaseexpirationdate"] = "<spring:message code="accountinginfo.leaseexpirationdate"/>";
        transFields["accountinginfo.warrantydate"] = "<spring:message code="accountinginfo.warrantydate"/>";
        transFields["asset.remote.hostname"] = "<spring:message code="asset.remote.hostname"/>";
        transFields["asset.remote.vncport"] = "<spring:message code="asset.remote.vncport"/>";

        function dupCheck() {
            var theDiv = document.getElementById('importErrors');
            if (theDiv.hasChildNodes())
                while (theDiv.childNodes.length > 1)
                    theDiv.removeChild(theDiv.childNodes[theDiv.childNodes.length - 1]);
            theDiv.style.display = 'none';


            var theForm = document.forms['assetImportForm'];
            var errMsg = new Array();

            var countMap = {};
            for (var i = 0; i < theForm.elements.length; i++) {
                if (theForm.elements[i].type == 'select-one') {
                    // check for empty select
                    if (theForm.elements[i].value == '') continue;
                    var countForThis = getMapCount(theForm.elements[i].value, countMap);
                    countMap[theForm.elements[i].value] = countForThis + 1;
                }
            }

            for (var n in countMap) {
                if (countMap[n] > 1)
                    errMsg[errMsg.length] = transFields[n] + ' (' + countMap[n] + ')';
            }

            // display any duplicates found
            if (errMsg.length > 0) {
                var ul = document.createElement("UL");
                theDiv.appendChild(ul);
                for (var s in errMsg) {
                    var li = document.createElement("LI");
                    li.appendChild(document.createTextNode(errMsg[s]));
                    ul.appendChild(li);
                }
                theDiv.style.display = 'block';
                return false;
            }

            return true;
        }

        function getMapCount(name, map) {
            for (var n in map) {
                if (n == name) return map[n];
            }
            return 0;
        }

        function fileSelected() {
            document.getElementById('loadButton').disabled = document.getElementById('fileData').value.trim() == '';
        }

        function init(e) {
        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="assetTracker.import"/></h1>

        <spring:url var="formActionUrl" value="/asset/import.glml"/>
        <form:form modelAttribute="assetImport" id="loadCSVForm" action="${formActionUrl}" method="post" enctype="multipart/form-data" cssClass="form">

            <spring:hasBindErrors name="assetImport">
                <p class="message error no-margin">
                    &nbsp;
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>

            <p>
                <label for="fileData"><spring:message code="assetTracker.import.selectFile"/></label>
                <input type="file" id="fileData" name="fileData" onchange="fileSelected()"/>
            </p>

            <p>
                <span class="label"><spring:message code="assetTracker.import.chooseType"/></span>
                <input dojotype="dijit.form.RadioButton" id="csv" name="csvType" type="radio" value="csv"
                       checked="checked"/>
                <label for="csv" class="inline"><spring:message code="assetTracker.csv"/></label>
                &nbsp;
                <input dojotype="dijit.form.RadioButton" id="xcelcsv" name="csvType" type="radio"
                       value="xcelcsv"/>
                <label for="xcelcsv" class="inline"><spring:message code="assetTracker.excelcsv"/></label>
            </p>

            <p>
                <input type="checkbox" dojotype="dijit.form.CheckBox" name="firstLineHeader" id="firstLineHeader"/>
                <label for="firstLineHeader" class="inline"><spring:message code="assetTracker.firstLine"/>?</label>
            </p>

            <p>
                <input type="checkbox" dojotype="dijit.form.CheckBox" name="addLocation" id="addLocation"/>
                <label for="addLocation" class="inline"><spring:message code="assetTracker.addLocation"/>?</label>
            </p>

            <p>
                <input type="checkbox" dojotype="dijit.form.CheckBox" name="addGroup" id="addGroup"/>
                <label for="addGroup" class="inline"><spring:message code="assetTracker.addGroup"/>?</label>
            </p>

            <p class="grey-bg no-margin">
                <button type="submit" name="loadButton" id="loadButton" disabled><spring:message code="assetTracker.load" /></button>
            </p>

        </form:form>

        <c:if test="${not empty csvLabels}">
            <hr width="100%"/>
            <form action="" id="assetImportForm" method="post">
                <table style="margin-left: 20px">
                    <c:forEach var="label" items="${csvLabels}" varStatus="row">
                        <tr>
                            <td><c:out value="${label}"/></td>
                            <td>
                                <select name="importMap[col${row.index}]">
                                        ${fieldOptions}
                                </select>
                            </td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="padding-top: 10px">
                            <button type="submit" name="importButton" onclick="return dupCheck();">
                                <spring:message code="assetTracker.import" />
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="padding-top: 10px">
                            <div id="importErrors" style="display:none">
                                <spring:message code="asset.import.duperror"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </c:if>

    </div>
</section>
</body>
