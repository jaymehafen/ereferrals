<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.statuses"/></title>
    <script type="text/javascript">

        function viewAssetStatus(assetStatusId) {
            var url = '<c:url value="/asset/viewAssetStatus.glml"/>';
            if (assetStatusId != null)url += '?assetStatusId=' + assetStatusId;
            window.location.href = url;
        }

        var nonDelIds = {
            <c:forEach var="assStat" items="${nonDelStatus}" varStatus="status">
            "${assStat.id}":''<c:if test="${not status.last}">,</c:if>
            </c:forEach>
        };

        function deleteAssetStatus(assetStatusId) {
            // avoid a db error if trying to delete a referenced assettype
            if (assetStatusId in nonDelIds) {
                alert('<spring:message code="asset.status.nodelete"/>');
                return;
            }
            // OK to delete
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/asset/deleteAssetStatus.glml"/>?assetStatusId=' + assetStatusId;
            }
        }

        function init() {
        }

        dojo.addOnLoad(init);

    </script>
</head>

<body>

<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="assetTracker.statuses"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/asset/viewAssetStatus.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="assetTracker.statusNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <div class="no-margin">

        <table class="table">
            <thead>
            <tr>
                <th scope="col"><spring:message code="global.name"/></th>
                <th scope="col"><spring:message code="global.description"/></th>
                <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${statuses}" var="assetStatus">
                <tr>
                    <td><c:out value="${assetStatus.name}"/></td>
                    <td><c:out value="${assetStatus.description}"/></td>
                    <td class="table-actions">
                        <a href="javascript:viewAssetStatus(<c:out value="${assetStatus.id}"/>);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png" />"/></a>
                        <a href="javascript:deleteAssetStatus(<c:out value="${assetStatus.id}" />);">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png" />"/></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        </div>
    </div>
</section>
</body>
