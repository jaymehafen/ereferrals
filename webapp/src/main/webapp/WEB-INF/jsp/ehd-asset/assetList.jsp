<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.listAssets"/></title>

    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>" type="text/css">

    <script type="text/javascript" src="<c:url value="/js/jquery.watermark.min.js"/>"></script>

    <script type="text/javascript">
        function init() {


            var assStore = new dojox.data.QueryReadStore({
                url: "<c:url value="/stores/assets.json"/>"
            });

            var assGrid = new dojox.grid.DataGrid({
                store: assStore,
                structure: [
                    {name:"<spring:message code="asset.assetNumber" javaScriptEscape="true"/>", field: "assetNumber", width: "auto"},
                    {name:"<spring:message code="asset.name" javaScriptEscape="true"/>", field: "name", width: "auto"},
                    {name:"<spring:message code="asset.assetlocation" javaScriptEscape="true"/>", field: "assetLocation", width: "auto"},
                    {name:"<spring:message code="asset.location" javaScriptEscape="true"/>", field: "location", width: "auto"},
                    {name:"<spring:message code="asset.group" javaScriptEscape="true"/>", field: "group", width: "auto"},
                    {name:"<spring:message code="asset.category" javaScriptEscape="true"/>", field: "category", width: "auto"},
                    {name:"<spring:message code="asset.categoryoption" javaScriptEscape="true"/>", field: "categoryOption", width: "auto"},
                    {name:"<spring:message code="asset.owner" javaScriptEscape="true"/>", field: "owner", width: "auto"},
                    {name:"<spring:message code="global.actions" javaScriptEscape="true" />", field: "_item", width: "50px", formatter: formatActions}

                ],
                editable: false,
                autoHeight: 20,
                query: {assetNumber: '*' },
                canSort: canSort,
                noDataMessage: "<spring:message code="assetTracker.noResultsFound" javaScriptEscape="true"/>"
            }, "assGrid");

            function canSort(colIndex) {
                return colIndex != 9;
            }

            function formatActions(value, rowIndex, cell) {
                var assetId = value.i.id;

                var text = '<a class="editLink" href="<c:url value="/asset/assetEdit.glml?assetId="/>' + assetId + '"><img style="width:15px;height:15px;" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"></a>';
                text += "&nbsp;&nbsp;"

                var deleteOnClick = "return confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>');";
                text += '<a class="deleteLink" href="<c:url value="/asset/deleteAsset.glml?assetId="/>' + assetId + '" onclick="' + deleteOnClick + '"><img style="width:15px;height:15px;" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"></a>';

                return text;
            }

            assGrid.startup();

            dojo.connect(assGrid, "onRowDblClick", function(e) {
                var assetId = e.grid.getItem(e.rowIndex).i.id;
                location.replace('<c:url value="/asset/assetEdit.glml"/>' + "?assetId=" + assetId);
            });

            dojo.connect(dojo.byId("assSearch"), "keyup", function() {
                var searchQuery = dojo.attr(this, "value");
                delay(function() {
                    assGrid.setQuery({label: searchQuery});
                }, 500);
            });
        }

        dojo.addOnLoad(init);

        $(function() {
            $("#assSearch").watermark("<spring:message code="global.search"/>");
        });

    </script>
</head>

<body>

<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="assetTracker.listAssets"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <input type="text" id="assSearch">
                </li>
                <li class="sep"></li>
                <li>
                    <a href="<c:url value="/asset/assetEdit.glml"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="assetTracker.addAsset"/>
                    </a>
                </li>
            </ul>
        </div>

        <div class="no-margin">
            <div id="assGrid"></div>
        </div>
    </div>
</section>
</body>
