<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title><spring:message code="assetFilter.results"/></title>

    <style type="text/css">
        @import "<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>";
        @import "<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>";
    </style>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            dojo.parser.parse();

            columns = {};
            <c:forEach var="col" items="${assetFilter.columnOrder}">
                <c:choose>
                    <c:when test="${fn:startsWith(col, 'customField.')}">
                        columns["<ehd:stringEscape type="javascript" value="${col}"/>"] = "<ehd:stringEscape type="javascript" value="${fn:replace(col, 'customField.', '')}"/>";
                    </c:when>
                    <c:otherwise>
                        columns["<ehd:stringEscape type="javascript" value="${col}"/>"] = "<ehd:stringEscape type="javascript" value="${allColumns[col]}"/>";
                    </c:otherwise>
                </c:choose>
            </c:forEach>

            <c:choose>
                <c:when test="${assetFilter.id ne null}">
                    var storeUrl = "<c:url value="/assets/filters/${assetFilter.id}/store"/>";
                </c:when>
                <c:otherwise>
                    var storeUrl = "<c:url value="/assets/filters/current/store"/>";
                </c:otherwise>
            </c:choose>

            var afStore = new dojox.data.QueryReadStore({
                url: storeUrl
            });

            var gridLayout = [];
            _.each(columns, function(value, key) {
                var colObj = {
                    field: key,
                    name: value,
                    width: 'auto'
                };
                gridLayout.push(colObj);
            });

            var grid = new dojox.grid.DataGrid({
                query: { assetId: "*" },
                clientSort: false,
                rowSelector: '20px',
                structure: gridLayout,
                store: afStore,
                autoHeight: 25,
                editable: false,
                id: "assetGrid",
                noDataMessage: "<spring:message code="assetFilter.noList"/>"
            }, "gridContainer");

            grid.startup();

            dojo.connect(grid, "onRowDblClick", function(e) {
                var assetId = e.grid.store.getValue(e.grid.getItem(e.rowIndex), "asset.assetId");
                window.opener.location.replace('<c:url value="/asset/assetEdit.glml"/>' + "?assetId=" + assetId);
            });
        });

    </script>
</head>
<body>
<article class="container_12">
<br>
<section class="grid_12">
    <div class="block-content">
        <h1><c:out value="${assetFilter.name}"/></h1>
        <div class="no-margin">
            <div dojoType="dijit.layout.ContentPane" id="gridContainer"></div>
        </div>
    </div>
</section>
</article>
</body>