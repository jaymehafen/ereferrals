<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="myFilters" type="java.util.List"--%>
<%--@elvariable id="pubFilters" type="java.util.List"--%>
<head>
    <title><spring:message code="assetTracker.searchAsset"/></title>

    <meta name="assetSidebar" content="true">

    <script type="text/javascript">
        function closeExpansion(image, detailDiv) {
            var div = document.getElementById(detailDiv);
            if (image.src.indexOf("plus.gif") == -1) {
                image.src = '<c:url value="/images/plus.gif"/>';
                div.style.width = "0px";
                div.style.height = "0px";
                div.style.border = "0px";
            }
        }

        function toggleExpansion(image, detailDiv) {
            var div = document.getElementById(detailDiv);
            if (image.src.indexOf("plus.gif") > -1) {
                image.src = '<c:url value="/images/minus.gif"/>';
                div.style.width = "98%";
                div.style.height = "515px";
                div.style.border = "";
            } else {
                image.src = '<c:url value="/images/plus.gif"/>';
                div.style.width = "0px";
                div.style.height = "0px";
                div.style.border = "0px";
            }
        }

        function loadFilter(id) {
            var yerl = '<c:url value="/asset/filter/edit.glml"/>';
            if (id > -1) yerl += '?afId=' + id;
            frames['afFrame'].location.href = yerl;
        }

        function reloadFilterList(id) {
            dojo.xhrPost({
                url: '<c:url value="/asset/filter/reloadList.glml"/>',
                handleAs: "text",
                content: {id: id},
                load: function(response) {
                    document.getElementById('nfDiv').innerHTML = response;
                    return response;
                },
                error: function(response, ioArgs) {
                    alert("XHR error HTTP status code: " + ioArgs.xhr.status);
                    return response;
                }
            });
        }

        function applyFilter(yerl) {
            frames['assetListFrame'].location.href = yerl;
        }

        function deleteFilter() {
            dojo.xhrPost({
                url: '<c:url value="/asset/filter/reloadList.glml"/>',
                handleAs: "text",
                load: function(response) {
                    document.getElementById('nfDiv').innerHTML = response;
                    return response;
                },
                error: function(response, ioArgs) {
                    alert("XHR error HTTP status code: " + ioArgs.xhr.status);
                    return response;
                }
            });
        }

        dojo.addOnLoad(function() {
            dojo.parser.parse();
        });

    </script>

</head>
<body>
<section class="grid_10">
    <table width="99%">
        <tr>
            <td>
                <div class="menuBar" style="width:100%">
                    <table width="100%">
                        <tr>
                            <td nowrap="nowrap" style="color:white;font-weight:bold;" width="50">
                                <nobr>
                                    <img id="minusButton" src="<c:url value="/images/minus.gif"/>" alt=""
                                         onclick="toggleExpansion(this, 'afFrame');">
                                    <spring:message code="assetFilter.title"/></nobr>
                            </td>
                            <td style="padding-left:10px">
                                <div id="nfDiv">
                                    <select name="namedFilters" onchange="loadFilter(this.value)">
                                        <option value="-1"><spring:message code="ticketFilter.createNew"/></option>
                                        <c:if test="${fn:length(myFilters)>0}">
                                            <optgroup label="<spring:message code="assetFilter.myFilters"/>">
                                                <c:forEach var="mtf" items="${myFilters}">
                                                    <option value="${mtf.id}">${mtf.name}</option>
                                                </c:forEach>
                                            </optgroup>
                                        </c:if>
                                        <c:if test="${fn:length(pubFilters)>0}">
                                            <optgroup label="<spring:message code="assetFilter.publicFilters"/>">
                                                <c:forEach var="ptf" items="${pubFilters}">
                                                    <option value="${ptf.id}">${ptf.name}</option>
                                                </c:forEach>
                                            </optgroup>
                                        </c:if>
                                    </select>
                                </div>
                            </td>
                            <td nowrap="nowrap" align="right" style="padding-right:10px" width="100%">
                                <nobr>
                                    <a href="javascript://"
                                       onclick="closeExpansion(document.getElementById('minusButton'), 'afFrame');frames['afFrame'].submitForm('apply')">
                                        <img src="<c:url value="/images/ticketSettings/filter_data_down_24.png"/>" alt=""
                                             title="<spring:message code="assetFilter.applyFilter"/>"/>
                                    </a>
                                    <a href="javascript://" onclick="frames['afFrame'].submitForm('save')">
                                        <img src="<c:url value="/images/ticketSettings/filter_data_save_24.png"/>" alt=""
                                             title="<spring:message code="assetFilter.saveFilter"/>"/>
                                    </a>
                                    <a href="javascript://" onclick="frames['afFrame'].submitForm('delete')">
                                        <img src="<c:url value="/images/ticketSettings/filter_data_close_24.png"/>" alt=""
                                             title="<spring:message code="assetFilter.deleteFilter"/>"/>
                                    </a>
                                </nobr>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="afDetails">
                    <iframe align="top" id="afFrame" name="afFrame" src="<c:url value="/asset/filter/edit.glml"/>"
                            style="width: 100%; height: 400px; background-color:transparent;padding-left:3px;"
                            frameborder="0" width="100%" height="400px">
                    </iframe>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="menuBar" style="width:100%">
                    <spring:message code="asset.title"/>
                </div>
                <div id="assetList">
                    <iframe id="assetListFrame" name="assetListFrame" src="" scrolling="auto" frameborder="1"
                            style="width:100%;height:500px;background-color:transparent;margin-left:4px">
                    </iframe>
                </div>
            </td>
        </tr>
    </table>
</section>
</body>
