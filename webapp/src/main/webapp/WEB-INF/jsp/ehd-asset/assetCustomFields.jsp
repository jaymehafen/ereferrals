<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.custField.title"/></title>
    
    <script type="text/javascript">

        function addCustomField() {

            var tabContainer = dijit.byId("mainTabContainer");
            var selectedTabId = tabContainer.selectedChildWidget.id;
            var fgId = selectedTabId.substring(4);

            var url = '<c:url value="/asset/editCustomField.glml"/>';
            url += '?fieldGroupId=' + fgId;

            window.location.href = url;
        }

        function editCustomField(customFieldId) {
            var url = '<c:url value="/asset/editCustomField.glml"/>';
            url += '?customFieldId=' + customFieldId;
            window.location.href = url;
        }

        function deleteCustomField(custFieldId) {
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/asset/deleteCustomField.glml"/>?custFieldId=' + custFieldId;
            }
        }

        function init() {

        <c:if test="${not empty fieldGroupId}">
            var tabContainer = dijit.byId("mainTabContainer");
            var selectedTab = dijit.byId("tab_<c:out value="${fieldGroupId}"/>");
            tabContainer.selectChild(selectedTab);
        </c:if>

        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>

<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="assetTracker.custField.title"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="javascript:addCustomField();">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="assetTracker.custField.new"/>
                    </a>
                </li>
            </ul>
        </div>

        <div id="mainTabContainer" dojoType="dijit.layout.TabContainer" style="height:500px;">
            <c:forEach items="${assetFieldGroups}" var="assetFieldGroup">
                <div id="tab_<c:out value="${assetFieldGroup.id}"/>" dojoType="dijit.layout.ContentPane" title="<c:out value="${assetFieldGroup.name}"/>">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><spring:message code="global.name"/></th>
                            <th scope="col"><spring:message code="assetTracker.custField.type"/></th>
                            <th scope="col"><spring:message code="customField.required"/></th>
                            <th scope="col"><spring:message code="global.order"/></th>
                            <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="customField" items="${assetFieldGroup.customFields}">
                            <tr>
                                <td><c:out value="${customField.name}"/></td>
                                <td><c:out value="${customField.customFieldType}"/></td>
                                <td><c:out value="${customField.required}"/></td>
                                <td><c:out value="${customField.fieldOrder}"/></td>
                                <td class="table-actions">
                                    <a href="javascript:editCustomField(<c:out value="${customField.id}" />);">
                                        <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png" />"/></a>
                                    <a href="javascript:deleteCustomField(<c:out value="${customField.id}" />);">
                                        <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png" />"/></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:forEach>
        </div>
    </div>
</section>
</body>
