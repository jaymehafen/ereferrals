<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="assetSearch" type="net.grouplink.ehelpdesk.common.utils.AssetSearch"--%>
<%--@elvariable id="assetList" type="java.util.List<Asset>"--%>
<%--@elvariable id="user" type="net.grouplink.ehelpdesk.domain.LdapUser"--%>
<head>

    <script type="text/javascript">
        function view(url, name, width, height) {
            openWindow(url, name, width, height);
        }


        function chooseAsset(assetId) {
            if(window.opener) window.opener.choozAsset(assetId);
            else  window.parent.choozAsset(assetId);
        }

        function clearSearchFields() {
            document.getElementById('assetNumber').value = '';
            document.getElementById('assetName').value = '';
            document.getElementById('assetLocation').value = '';
            document.getElementById('location').value = '';
            document.getElementById('group').value = '';
            document.getElementById('category').value = '';
            document.getElementById('categoryOption').value = '';
            document.getElementById('owner').value = '';
        }

        function resetSearchFields() {

            var parentAssetNumber = window.parent.document.getElementById('asset');
            document.getElementById('assetNumber').value = parentAssetNumber.value;

            var parentLocation = window.parent.document.getElementById('location');
            document.getElementById('location').value = parentLocation.options[parentLocation.selectedIndex].text;

            var parentGroup = window.parent.document.getElementById('group');
            document.getElementById('group').value = parentGroup.options[parentGroup.selectedIndex].text;

            var parentCategory = window.parent.document.getElementById('category');
            document.getElementById('category').value = parentCategory.options[parentCategory.selectedIndex].text;

            var parentCatOpt = window.parent.document.getElementById('categoryOption');
            document.getElementById('categoryOption').value = parentCatOpt.options[parentCatOpt.selectedIndex].text;

        }

        dojo.addOnLoad(function() {
            dojo.parser.parse();
        });
    </script>

</head>

<body>
<article class="white-bg">
    <div dojoType="dijit.layout.BorderContainer" gutters="false" id="mainDiv">
        <div dojoType="dijit.layout.ContentPane" region="center">
            <form action="" method="get" class="form with-margin">
                <div class="columns">
                    <div class="colx2-left">
                        <p>
                            <label for="assetNumber"><spring:message code="assetTracker.assetNumber"/></label>
                            <input type="text" name="assetNumber" id="assetNumber" value="<c:out value="${assetSearch.assetNumber}"/>" dojoType="dijit.form.TextBox">
                        </p>
                        <p>
                            <label for="assetlocation"><spring:message code="assetTracker.assetLocation"/></label>
                            <input type="text" name="assetLocation" id="assetLocation" value="<c:out value="${assetSearch.assetLocation}"/>" dojoType="dijit.form.TextBox">
                        </p>
                        <p>
                            <label for="group"><spring:message code="assetTracker.group"/></label>
                            <input type="text" name="group" id="group" value="<c:out value="${assetSearch.group}"/>" dojoType="dijit.form.TextBox">
                        </p>
                        <p>
                            <label for="categoryOption"><spring:message code="ticket.categoryOption"/></label>
                            <input type="text" name="categoryOption" id="categoryOption" value="<c:out value="${assetSearch.categoryOption}"/>" dojoType="dijit.form.TextBox">
                        </p>
                    </div>
                    <div class="colx2-right">
                        <p>
                            <label for="assetName"><spring:message code="assetTracker.name"/></label>
                            <input type="text" name="assetName" id="assetName" value="<c:out value="${assetSearch.assetName}"/>" dojoType="dijit.form.TextBox">
                        </p>
                        <p>
                            <label for="location"><spring:message code="ticket.location"/></label>
                            <input type="text" name="location" id="location" value="<c:out value="${assetSearch.location}"/>" dojoType="dijit.form.TextBox">
                        </p>
                        <p>
                            <label for="category"><spring:message code="assetTracker.category"/></label>
                            <input type="text" name="category" id="category" value="<c:out value="${assetSearch.category}"/>" dojoType="dijit.form.TextBox">
                        </p>
                        <p>
                            <label for="owner"><spring:message code="asset.owner"/></label>
                            <input type="text" name="owner" id="owner" value="<c:out value="${assetSearch.owner}"/>" dojoType="dijit.form.TextBox">
                        </p>
                    </div>
                </div>
                <input type="submit" name="search" value="<spring:message code="global.search" />"/>
                <input type="button" name="clear" value="<spring:message code="global.clear" />" onclick="clearSearchFields();"/>
                <input type="button" name="reset" value="<spring:message code="global.reset" />" onclick="resetSearchFields();"/>
            </form>

            <c:if test="${not empty assetList}">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width:32px">
                                &nbsp;
                            </th>
                            <th>
                                <spring:message code="assetTracker.assetNumber"/>
                            </th>
                            <th>
                                <spring:message code="assetTracker.name"/>
                            </th>
                            <th>
                                <spring:message code="ticket.location"/>
                            </th>
                            <th>
                                <spring:message code="assetTracker.assetLocation"/>
                            </th>
                            <th>
                                <spring:message code="assetTracker.group"/>
                            </th>
                            <th>
                                <spring:message code="ticket.category"/>
                            </th>
                            <th>
                                <spring:message code="ticket.categoryOption"/>
                            </th>
                            <th>
                                <spring:message code="asset.owner"/>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${assetList}" var="asset">
                        <tr>
                            <td>
                                <a href="javascript://" onclick="chooseAsset('<c:out value="${asset.assetNumber}" />');"><img
                                        src="<c:url value="/images/add.gif" />" alt="<spring:message code="asset.chooseAsset"/>"/></a>
                                <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                                    <a href="javascript://"
                                       onclick="view('<c:url value="/asset/assetEdit.glml" ><c:param name="assetId" value="${asset.id}" /></c:url>', 'assset_' + ${asset.id}, 800, 600);">
                                        <img src="<c:url value="/images/view.gif" />" alt="<spring:message code="global.view" />"/>
                                    </a>
                                </authz:authorize>
                            </td>
                            <td>
                                <c:out value="${asset.assetNumber}"/>
                            </td>
                            <td>
                                <c:out value="${asset.name}"/>
                            </td>
                            <td>
                                <c:out value="${asset.location.name}"/>
                            </td>
                            <td>
                                <c:out value="${asset.assetLocation}"/>
                            </td>
                            <td>
                                <c:out value="${asset.group.name}"/>
                            </td>
                            <td>
                                <c:out value="${asset.category.name}"/>
                            </td>
                            <td>
                                <c:out value="${asset.categoryOption.name}"/>
                            </td>
                            <td>
                                <c:out value="${asset.owner.firstName}"/> <c:out value="${asset.owner.lastName}"/>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</article>



</body>
