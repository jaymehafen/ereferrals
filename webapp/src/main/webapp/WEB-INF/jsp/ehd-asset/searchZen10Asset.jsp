<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="workstations" type="java.util.List<net.grouplink.ehelpdesk.domain.zen.Workstation>"--%>
<%--@elvariable id="devices" type="java.util.List<net.grouplink.ehelpdesk.domain.zen.Device>"--%>
<%--@elvariable id="searchResults" type="java.util.List<net.grouplink.ehelpdesk.domain.zen.Workstation>"--%>
<%--@elvariable id="primaryUserWorkstations" type="java.util.List<net.grouplink.ehelpdesk.domain.zen.Workstation>"--%>
<%--@elvariable id="recentWorkstations" type="java.util.List<net.grouplink.ehelpdesk.domain.zen.Workstation>"--%>
<%--@elvariable id="zenBaseUrl" type="java.lang.String"--%>
<head>
    <title><spring:message code="assetTracker.zenworks10Assets" text="ZENworks 10 Assets"/></title>
    <script type="text/javascript" src="<c:url value="/dwr/interface/ZenAssetService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

    <script type="text/javascript">
        function view(url, name, width, height) {
            openWindow(url, name, width, height);
        }

        function viewZenAssetDetails(zuidString) {
            var url = '${zenBaseUrl}/jsp/index.jsp?pageid=workstationDetails&uid=' + zuidString + '&adminid=Devices';
            openWindow(url, 'zenAssetDetails_' + zuidString, 800, 600);
        }

        function chooseAsset(workstationId) {
            // get the ZenAsset.id for the selected workstation
            ZenAssetService.getZenAssetIdForWorkstationId(workstationId, handleGetZenAssetId);
        }

        function handleGetZenAssetId(zenAssetId) {
            if(window.opener) window.opener.chooseZenAsset(zenAssetId);
            else  window.parent.chooseZenAsset(zenAssetId);
        }

        function clearSearchFields() {
            document.getElementById('assetName').value = '';
            document.getElementById('assetType').value = '';
            document.getElementById('operatingSystem').value = '';
        }

        function init() {
        }

        dojo.addOnLoad(init);

    </script>

</head>

<body>
<article class="white-bg">
<div dojoType="dijit.layout.BorderContainer" id="mainDiv" gutters="false">
<div dojoType="dijit.layout.ContentPane" region="center">
<div id="zenSearchFields">
    <form:form commandName="zenAssetSearch" cssClass="form">
        <input type="hidden" name="search" value="true"/>
        <p>
            <form:label path="assetName"><spring:message code="assetTracker.name"/></form:label>
            <form:input path="assetName" dojoType="dijit.form.TextBox"/>
        </p>
        <p>
            <form:label path="assetType"><spring:message code="asset.type"/></form:label>
            <form:input path="assetType" dojoType="dijit.form.TextBox"/>
        </p>
        <p>
            <form:label path="operatingSystem"><spring:message code="assetTracker.operatingSystem"/></form:label>
            <form:input path="operatingSystem" dojoType="dijit.form.TextBox"/>
        </p>

        <input type="submit" name="searchButton" value="<spring:message code="global.search"/>"/>
        <input type="button" name="clearButton" value="<spring:message code="global.clear"/>" onclick="clearSearchFields();"/>
        <input type="submit" name="resetButton" value="<spring:message code="global.reset"/>"/>
    </form:form>
</div>

<br/><br/>

<%--PRIMARY ASSETS--%>
<c:if test="${empty searchResults and !empty primaryUserWorkstations}">
<div id="primaryUserAssets">
    <h3><spring:message code="assetTracker.contactsPrimaryAssets"/></h3>
    <table class="table">
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th><spring:message code="assetTracker.name"/></th>
            <th><spring:message code="asset.type"/></th>
            <th><spring:message code="assetTracker.operatingSystem"/></th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="workstation" items="${primaryUserWorkstations}">
            <tr>
                <td>
                    <a href="javascript://" onclick="chooseAsset('<c:out value="${workstation.idString}" />');">
                        <img src="<c:url value="/images/add.gif" />" alt="<spring:message code="asset.chooseAsset"/>">
                    </a>
                    <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                        <a href="javascript://" onclick="viewZenAssetDetails('${workstation.device.zuidString}');">
                            <img src="<c:url value="/images/view.gif" />" alt="<spring:message code="global.view" />">
                        </a>
                    </authz:authorize>
                </td>
                <td>${workstation.machineName}</td>
                <td>${workstation.systemProduct}</td>
                <td>${workstation.osProduct}</td>
            </tr>
        </c:forEach>

        </tbody>
    </table>
</div>

<br/><br/>

<%--RECENT ASSETS--%>
<div id="recentUserAssets">
    <h3><spring:message code="assetTracker.assetsContactHasLoggedInto"/></h3>
    <table class="table">
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th><spring:message code="assetTracker.name"/></th>
            <th><spring:message code="asset.type"/></th>
            <th><spring:message code="assetTracker.operatingSystem"/></th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="workstation" items="${recentWorkstations}">
            <tr>
                <td>
                    <a href="javascript://" onclick="chooseAsset('<c:out value="${workstation.idString}" />');">
                        <img src="<c:url value="/images/add.gif" />" alt="<spring:message code="asset.chooseAsset"/>">
                    </a>
                    <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                        <a href="javascript://" onclick="viewZenAssetDetails('${workstation.device.zuidString}');">
                            <img src="<c:url value="/images/view.gif" />" alt="<spring:message code="global.view" />">
                        </a>
                    </authz:authorize>
                </td>
                <td>${workstation.machineName}</td>
                <td>${workstation.systemProduct}</td>
                <td>${workstation.osProduct}</td>
            </tr>
        </c:forEach>

        </tbody>
    </table>
</div>
<br/><br/>
</c:if>

<%--SEARCH RESULTS--%>
<c:if test="${not empty searchResults}">
<div id="workstationSearchResults">
    <h3><spring:message code="assetTracker.searchResults"/></h3>
    <table class="table">
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th><spring:message code="assetTracker.name"/></th>
            <th><spring:message code="asset.type"/></th>
            <th><spring:message code="assetTracker.operatingSystem"/></th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="workstation" items="${searchResults}">
            <tr>
                <td>
                    <a href="javascript://" onclick="chooseAsset('<c:out value="${workstation.idString}" />');">
                        <img src="<c:url value="/images/add.gif" />" alt="<spring:message code="asset.chooseAsset"/>"/>
                    </a>
                    <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                        <a href="javascript://" onclick="viewZenAssetDetails('${workstation.device.zuidString}');">
                            <img src="<c:url value="/images/view.gif" />" alt="<spring:message code="global.view" />"/>
                        </a>
                    </authz:authorize>
                </td>
                <td>${workstation.machineName}</td>
                <td>${workstation.systemProduct}</td>
                <td>${workstation.osProduct}</td>
            </tr>
        </c:forEach>

        </tbody>
    </table>
</div>
</c:if>
</div>
</div>
</article>
</body>