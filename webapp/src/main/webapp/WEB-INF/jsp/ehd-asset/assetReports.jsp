<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.reports"/></title>

    <script type="text/javascript">
        function init() {
        }

        dojo.addOnLoad(init);
    </script>

</head>

<body>

<section class="grid_4">
    <div class="block-content">
        <h1><spring:message code="assetTracker.reports"/></h1>

        <br><br>
        <ul class="simple-list with-icon icon-file">
            <li><a href="<c:url value="/asset/report/assetsByOwner.glml"/>"><spring:message code="assetTracker.reportsOwner"/></a></li>
            <li><a href="<c:url value="/asset/report/assetsByGroup.glml"/>"><spring:message code="assetTracker.reportsGroup"/></a></li>
            <li><a href="<c:url value="/asset/report/assetsByType.glml"/>"><spring:message code="assetTracker.reportsType"/></a></li>
            <li><a href="<c:url value="/asset/report/assetLeaseExpiration.glml"/>"><spring:message code="assetTracker.reportsLeaseExp"/></a></li>
            <li><a href="<c:url value="/asset/report/assetWarrantyExpiration.glml"/>"><spring:message code="assetTracker.reportsWarrantyExp"/></a></li>
            <li><a href="<c:url value="/asset/report/assetPurchase.glml"/>"><spring:message code="assetTracker.reportsPurchase"/></a></li>
        </ul>
    </div>
</section>
</body>
