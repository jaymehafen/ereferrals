<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>
        <spring:message code="assetTracker.title"/>
    </title>
    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css"/>
    </c:if>

    <script type="text/javascript" src="<c:url value="/js/accordionMenu/prototype.lite.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/accordionMenu/moo.fx.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/accordionMenu/moo.fx.pack.js" />"></script>

    <script type="text/javascript">//feel free to copy & paste *this script* as much as you want.
    var exists;
    var allStretch;

    function forceRefresh() {
        frames['menuConsole'].location.href = '<c:url value="/asset/listAsset.glml" ><c:param name="forceRefresh" value="true" /></c:url>';
    }

    </script>

</head>
<body>
<table width="100%" height="600px" style="width: 100%; height: 600px; float: left">
<tr>
<td align="center" valign="top" width="300px" style="width: 300px">
<div id="content">
<h3 class="display menuBar">
    <a href="javascript://">
        <spring:message code="assetTracker.assets"/>
    </a>
</h3>

<div class="stretcher">
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/asset/listAsset.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="assetTracker.listAssets"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/asset/filter/filters.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="assetTracker.searchAsset"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/asset/assetEdit.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="assetTracker.addAsset"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/asset/vendors.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="assetTracker.vendors"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/asset/softwareLicenses.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="softwareLicense.title"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/asset/import.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="assetTracker.import"/>
            </a>
        </li>
        <%--<li>
            <a href="<c:url value="/asset/export.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="assetTracker.export"/>
            </a>
        </li>--%>


        <li>
            <a href="<c:url value="/asset/reports.glml" ><c:param name="forceRefresh" value="true" /></c:url>"
               target="menuConsole">
                <spring:message code="assetTracker.reports"/>
            </a>
        </li>

        <%--<li>
            <a href="<c:url value="/report/reportAssetView.glml" ><c:param name="forceRefresh" value="true" /></c:url>"  target="menuConsole">
                Asset Reports
            </a>
        </li>--%>

        <%--<li>
            &nbsp;
        </li>
        <li>
            <a href="<c:url value="/dashboard/db.glml" />" target="menuConsole">
                <spring:message code="assetTracker.listAssetType"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/dashboard/db.glml" />" target="menuConsole">
                <spring:message code="assetTracker.addAssetType"/>
            </a>
        </li>--%>
    </ul>
</div>

<%--<h3 class="display menuBar">
    <a href="javascript://">
        <spring:message code="assetTracker.vendors"/>
    </a>
</h3>
<div class="stretcher">
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/dashboard/db.glml" />" target="menuConsole">
                <spring:message code="assetTracker.listVendor"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/dashboard/db.glml" />" target="menuConsole">
                <spring:message code="assetTracker.addVendor"/>
            </a>
        </li>
    </ul>
</div>--%>

<%--<h3 class="display menuBar">
    <a href="javascript://">
        <spring:message code="assetTracker.owners"/>
    </a>
</h3>
<div class="stretcher">
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/asset/ownerList.glml" />" target="menuConsole">
                <spring:message code="assetTracker.listOwners"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/dashboard/db.glml" />" target="menuConsole">
                <spring:message code="assetTracker.listUsers"/>
            </a>
        </li>
        <li>
            <a href="<c:url value="/dashboard/db.glml" />" target="menuConsole">
                <spring:message code="userManagement.createnew"/>
            </a>
        </li>
    </ul>
</div>--%>

<h3 class="display menuBar">
    <a href="javascript://">
        <spring:message code="assetTracker.settings"/>
    </a>
</h3>

<div class="stretcher">
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/asset/assetTypes.glml" />" target="menuConsole">
                <spring:message code="assetTracker.assetTypes"/>
            </a>
        </li>
    </ul>
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/asset/statuses.glml" />" target="menuConsole">
                <spring:message code="assetTracker.statuses"/>
            </a>
        </li>
    </ul>
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/asset/fieldGroups.glml" />" target="menuConsole">
                <spring:message code="assetTracker.fieldGroups"/>
            </a>
        </li>
    </ul>
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/asset/customFields.glml" />" target="menuConsole">
                <spring:message code="asset.custfieldNames"/>
            </a>
        </li>
    </ul>
</div>

<%--<h3 class="display menuBar">
    <a href="javascript://">
        <spring:message code="assetTracker.groups"/>
    </a>
</h3>
<div class="stretcher">
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/asset/groupList.glml" />" target="menuConsole">
                <spring:message code="assetTracker.listGroups"/>
            </a>
        </li>
    </ul>
</div>--%>

<%--<h3 class="display menuBar">
    <a href="javascript://">
        <spring:message code="report.title"/>
    </a>
</h3>
<div class="stretcher">
    <ul class="menuUl">
        <li>
            <a href="<c:url value="/dashboard/db.glml" />" target="menuConsole">
                <spring:message code="report.title"/>
            </a>
        </li>
    </ul>
</div>--%>
</div>
</td>
<td valign="top">
    <iframe align="top" id="menuConsole" name="menuConsole" src="<c:url value="/asset/listAsset.glml" />"
            style="width: 100%; height: 800px; background-color:transparent; float: left"
            frameborder="0" width="100%" height="800px">
    </iframe>
</td>
</tr>
</table>
<script type="text/javascript">
    Element.cleanWhitespace('content');
    init();
</script>
</body>
</html>