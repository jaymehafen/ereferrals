<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="vendor.editVendor"/></title>

    <script type="text/javascript">

        var transFields = {};
        transFields["vendor.name"] = "<spring:message code="vendor.name"/>";
        transFields["vendor.street1"] = "<spring:message code="vendor.street1"/>";
        transFields["vendor.street2"] = "<spring:message code="vendor.street2"/>";
        transFields["vendor.city"] = "<spring:message code="vendor.city"/>";
        transFields["vendor.state"] = "<spring:message code="vendor.state"/>";
        transFields["vendor.postalCode"] = "<spring:message code="vendor.postalCode"/>";
        transFields["vendor.primaryPhone"] = "<spring:message code="vendor.primaryPhone"/>";
        transFields["vendor.fax"] = "<spring:message code="vendor.fax"/>";
        transFields["vendor.email"] = "<spring:message code="vendor.email"/>";
        transFields["vendor.notes"] = "<spring:message code="vendor.notes"/>";

        function dupCheck() {
            var theDiv = document.getElementById('importErrors');
            if (theDiv.hasChildNodes())
                while (theDiv.childNodes.length > 1)
                    theDiv.removeChild(theDiv.childNodes[theDiv.childNodes.length - 1]);
            theDiv.style.display = 'none';


            var theForm = document.forms['vendorImportForm'];
            var errMsg = new Array();

            var countMap = {};
            for (var i = 0; i < theForm.elements.length; i++) {
                if (theForm.elements[i].type == 'select-one') {
                    // check for empty select
                    if (theForm.elements[i].value == '') continue;
                    var countForThis = getMapCount(theForm.elements[i].value, countMap);
                    countMap[theForm.elements[i].value] = countForThis + 1;
                }
            }

            for (var n in countMap) {
                if (countMap[n] > 1)
                    errMsg[errMsg.length] = transFields[n] + ' (' + countMap[n] + ')';
            }

            // display any duplicates found
            if (errMsg.length > 0) {
                var ul = document.createElement("UL");
                theDiv.appendChild(ul);
                for (var s in errMsg) {
                    var li = document.createElement("LI");
                    li.appendChild(document.createTextNode(errMsg[s]));
                    ul.appendChild(li);
                }
                theDiv.style.display = 'block';
                return false;
            }

            return true;
        }

        function getMapCount(name, map) {
            for (var n in map) {
                if (n == name) return map[n];
            }
            return 0;
        }

        function fileSelected() {
            document.getElementById('loadButton').disabled = document.getElementById('fileData').value.trim() == '';
        }

        function init(e) {
        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="assetTracker.import.vendor"/></h1>

        <form id="loadCSVForm" action="<c:url value="/asset/importVendor.glml"/>" method="post"
              enctype="multipart/form-data" class="form">
            <p>
                <label for="fileData"><spring:message code="assetTracker.import.selectFile"/></label>
                <input type="file" id="fileData" name="fileData" onchange="fileSelected()"/>
            </p>

            <p>
                <span class="label"><spring:message code="assetTracker.import.chooseType"/></span>
                <input dojotype="dijit.form.RadioButton" id="csv" name="csvType" type="radio" value="csv"
                       checked="checked"/>
                <label for="csv" class="inline"><spring:message code="assetTracker.csv"/></label>
                &nbsp;
                <input dojotype="dijit.form.RadioButton" id="xcelcsv" name="csvType" type="radio"
                       value="xcelcsv"/>
                <label for="xcelcsv" class="inline"><spring:message code="assetTracker.excelcsv"/></label>
            </p>

            <p>
                <label for="firstLineHeader"><spring:message code="assetTracker.firstLine"/>?</label>
                <input type="checkbox" dojotype="dijit.form.CheckBox" name="firstLineHeader" id="firstLineHeader"/>
            </p>

            <input type="submit" name="loadButton" id="loadButton" value="<spring:message code="assetTracker.load" />" disabled/>

        </form>

        <c:if test="${not empty csvLabels}">
            <hr width="100%"/>
            <form action="" id="vendorImportForm" method="post">
                <table style="margin-left: 20px">
                    <c:forEach var="label" items="${csvLabels}" varStatus="row">
                        <tr>
                            <td><c:out value="${label}"/></td>
                            <td>
                                <select name="importMap[col${row.index}]">
                                        ${fieldOptions}
                                </select>
                            </td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="padding-top: 10px">
                            <input type="submit" name="importButton" onclick="return dupCheck();"
                                   value="<spring:message code="assetTracker.import" />"/>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="padding-top: 10px">
                            <div id="importErrors" style="display:none">
                                <spring:message code="asset.import.duperror"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </c:if>
    </div>
</section>
</body>

