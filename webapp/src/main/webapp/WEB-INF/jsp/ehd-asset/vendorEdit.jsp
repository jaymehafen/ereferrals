<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="vendor.editVendor"/></title>

    <script type="text/javascript">
        function viewAsset() {
            window.location.href = '<c:url value="/asset/viewVendor.glml"/>';
        }

        function cancelForm() {
            window.location.href = '<c:url value="/asset/vendors.glml"/>';
        }

        function init() {
        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="vendor.editVendor"/></h1>

        <form:form commandName="vendor" method="post" cssClass="form">
            <spring:hasBindErrors name="vendor">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
            <input type="hidden" name="vendorId" value="${vendor.id}"/>

            <div class="columns">
                <div class="colx2-left">

                    <p class="required">
                        <form:label path="name"><spring:message code="global.name"/></form:label>
                        <form:input path="name" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="street1"><spring:message code="vendor.address"/></form:label>
                        <form:input path="street1" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                        <br><br>
                        <form:input path="street2" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="city"><spring:message code="vendor.city"/></form:label>
                        <form:input path="city" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="state"><spring:message code="vendor.state"/></form:label>
                        <form:input path="state" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="postalCode"><spring:message code="vendor.postalCode"/></form:label>
                        <form:input path="postalCode" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                </div>

                <div class="colx2-right">

                    <p>
                        <form:label path="primaryPhone"><spring:message code="vendor.primaryPhone"/></form:label>
                        <form:input path="primaryPhone" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="fax"><spring:message code="vendor.fax"/></form:label>
                        <form:input path="fax" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="email"><spring:message code="vendor.email"/></form:label>
                        <form:input path="email" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="notes"><spring:message code="vendor.notes"/></form:label>
                        <form:input path="notes" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
                    </p>

                </div>
            </div>

            <p class="grey-bg no-margin" id="commandButtons">
                <button type="submit"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelForm();"><spring:message
                        code="global.cancel"/></button>
            </p>

        </form:form>
    </div>
</section>
</body>

