<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="assetFilter" type="net.grouplink.ehelpdesk.domain.assetFilter"--%>
<%--@elvariable id="fieldGroups" type="java.util.List"--%>
<%--@elvariable id="dateOps" type="java.util.List"--%>
<%--@elvariable id="groupByOptions" type="java.util.List"--%>
<%--@elvariable id="saved" type="java.lang.String"--%>
<%--@elvariable id="apply" type="java.lang.String"--%>
<%--@elvariable id="deleted" type="java.lang.String"--%>
<%--@elvariable id="assetStatus" type="java.util.List"--%>
<%--@elvariable id="users" type="java.util.List"--%>
<%--@elvariable id="locations" type="java.util.List"--%>
<%--@elvariable id="groups" type="java.util.List"--%>
<%--@elvariable id="categories" type="java.util.List"--%>
<%--@elvariable id="catOptions" type="java.util.List"--%>
<%--@elvariable id="assetTypes" type="java.util.List"--%>
<%--@elvariable id="vendors" type="java.util.List"--%>
<%--@elvariable id="availableCols" type="java.util.List"--%>
<%--@elvariable id="rc" type="org.springframework.web.servlet.support.RequestContext"--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

<style type="text/css">
    @import "<c:url value="/js/dojo-1.6.2/dojo/resources/dnd.css"/>";

    .dndContainer {
        border: 3px solid #ccc;
        padding: 1em 3em;
        cursor: default;
        radius: 8pt;
        -moz-border-radius: 8pt 8pt;
        background: #fff;
        width: 150px;
        display: block;
        text-align: left
    }

    td {
        font-weight: bolder;
    }

    .afInputs {
        width: 130px;
        height: 18px
    }

    .afSelects {
        width: 130px;
    }

    .afOuterbox {
        border: 1px black outset;
        float: left;
        margin: 5px 0 5px 5px;
        radius: 8pt;
        -moz-border-radius: 8pt 8pt;
        padding: 3px 3px 3px 3px;
        height: 135px;
        background-color: #EEEEEE;
    }

    .afInnerbox {
        border: 1px black inset;
        height: 80px;
        overflow: auto;
        background-color: white;
        min-width: 150px;
        radius: 5pt;
        -moz-border-radius: 5pt 5pt;
        padding: 2px 2px 2px 2px
    }

    #reportBox {
        border: 1px dashed black;
        padding: 5px 10px 5px 10px;
        margin-left: 10px;
        width: 600px;
    <c:if test="${not assetFilter.report}" > display: none </c:if>
    }

</style>

<script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/dojo/dojo.js"/>"
        djConfig="parseOnLoad: true,
        locale: '<tags:dojoLocale locale="${rc.locale}" />' ">
</script>
<script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/grouplink/includes.js"/>"></script>

<script type="text/javascript">
//dojo.require("dojo.parser");

//dojo.require("dojo.data.ItemFileReadStore");

//dojo.require("dojo.dnd.Container");
//dojo.require("dojo.dnd.Manager");
//dojo.require("dojo.dnd.Source");

//dojo.require("dijit.TitlePane");
//dojo.require("dijit.Dialog");

//dojo.require("dijit.form.Button");
//dojo.require("dijit.form.CheckBox");
//dojo.require("dijit.form.TextBox");
//dojo.require("dijit.form.DateTextBox");
//dojo.require("dijit.form.FilteringSelect");

/**
 * Adds a search value item to the servers AssetFilter object via Ajax. On return
 * an html block is sent with all entered search values for this search block.
 *
 * @param divId the id of the <div> element that has an item being added.
 * @param fldId the field id that contains the search value.
 */
function addItem(divId, fldId) {
    var fld = dijit.byId(fldId);
    var fldVal = fld.getValue();

    fld.setValue('');

    // Trim the value
    // fldVal = fldVal.replace(/^\s*(.*?)\s*$/, "$1");  relying on dojo's textbox trim attrib.

    if (fldVal.length == 0) return;

    dojo.xhrPost({
        url: '<c:url value="/asset/filter/addItem.glml"/>',
        handleAs: "text",
        content: { value: fldVal, type: divId},
        load: function (response) {
            document.getElementById(divId).innerHTML = response;
            return response;
        },
        error: function(response, ioArgs) {
            alert("XHR error HTTP status code: " + ioArgs.xhr.status);
            return response;
        }
    });
}

/**
 * Deletes the given item from the server's AssetFilter object via Ajax. On return
 * an html block with the item removed is sent to populate the div block.
 *
 * @param divId The id of the div that has an item being deleted
 * @param row the row number that is going to be deleted
 */
function delItem(divId, row) {
    dojo.xhrPost({
        url: '<c:url value="/asset/filter/deleteItem.glml"/>',
        handlesAs: "text",
        content: { row: row, type: divId },
        load: function (response) {
            document.getElementById(divId).innerHTML = response;
            return response;
        },
        error: function(response, ioArgs) {
            alert("XHR error HTTP status code: " + ioArgs.xhr.status);
            return response;
        }
    });
}

/**
 * Displays the hidden div(s) on date search fields depending on the
 * operator chosen.
 *
 * @param slct the html select box that hold the operator
 * @param prefix identifies which date field to work with.
 */
function showDate(slct, prefix) {
    var value = slct.value;

    // on before and after show 1 date
    switch (value) {
        case '4':
        case '5':
        case '6':
            document.getElementById(prefix + "1").style.display = 'block';
            document.getElementById(prefix + "2").style.display = 'none';
            break;
        case '7':
            document.getElementById(prefix + "1").style.display = 'block';
            document.getElementById(prefix + "2").style.display = 'block';
            break;
        default:
            document.getElementById(prefix + "1").style.display = 'none';
            document.getElementById(prefix + "2").style.display = 'none';
            break;
    }
}

/**
 * Toggles the display value of the report div
 * @param showIt true if it is to display false to hide
 */
function toggleReportDiv(showIt) {
    document.getElementById('reportBox').style.display = showIt ? "block" : "none";
}

var modalDivId;
/**
 * Displays the modal dialog for category and categoryoption fetcher dialog
 * @param yerl the URL to display
 * @param divId the id of the div that is to get the new display options after
 *              save on the dialog window is hit.
 */
function showModal(yerl, divId) {
    modalDivId = divId;
    dijit.byId('fetcher').setHref(yerl);
    dijit.byId('fetcher').show();
}

/**
 * Used by the 'select all' checkbox in the fetcher dialogs.
 * Toggles the check on the associated checkboxes
 * @param cbx the 'select all' checkbox
 * @param cbxName the name of the associated checkboxes
 */
function toggleSelectAll(cbx, cbxName) {
    // find all checkboxes for this row
    var cbxs = document.getElementsByName(cbxName);
    var toCheck = cbx.checked;
    for (var i = 0; i < cbxs.length; i++) {
        var cb = dijit.byId(cbxs[i].id);
        cb.setChecked(toCheck);
    }
}

/**
 * the return function that is called after someone hits apply on
 * either the catgory modal dialog window or categoryoption window.
 * It takes all the items that were checked and lists them in the apprpriate
 * div element via addItem()
 */
function applyChecks() {
    var slctdIds = new Array();
    var els = document.getElementsByTagName("input");
    for (var i = 0; i < els.length; i++) {
        if (els[i].name.indexOf("cbx_") > -1 && els[i].checked)
            slctdIds[slctdIds.length] = els[i].id;
    }

    for (var n in slctdIds) {
        addItem(modalDivId, slctdIds[n]);
    }
}

/**
 * Called by the parent window. Submits this page's form
 * @param action 'apply', 'save' or 'delete'
 */
function submitForm(action) {
    createColOrderFields();
    document.getElementById("formAction").value = action;
    document.afForm.submit();
}

/**
 * looks up the key in the transFiltedNames hash or returns the key if not found
 * @param key the field name or custom field name
 */
function getTransFieldName(key) {
    var transName = transFieldNames[key];
    return transName == null ? key : transName;
}

var transFieldNames = {
    'asset.assetNumber':                        '<spring:message code="asset.assetNumber"/>',
    'asset.name':                               '<spring:message code="asset.name"/>',
    'asset.type':                               '<spring:message code="asset.type"/>',
    'asset.owner':                              '<spring:message code="asset.owner"/>',
    'asset.vendor':                             '<spring:message code="asset.vendor"/>',
    'asset.assetlocation':                      '<spring:message code="asset.assetlocation"/>',
    'asset.acquisitionDate':                    '<spring:message code="asset.acquisitionDate"/>',
    'asset.location':                           '<spring:message code="asset.location"/>',
    'asset.group':                              '<spring:message code="asset.group"/>',
    'asset.category':                           '<spring:message code="asset.category"/>',
    'asset.categoryoption':                     '<spring:message code="asset.categoryoption"/>',
    'asset.manufacturer':                       '<spring:message code="asset.manufacturer"/>',
    'asset.modelNumber':                        '<spring:message code="asset.modelNumber"/>',
    'asset.serialnumber':                       '<spring:message code="asset.serialnumber"/>',
    'asset.description':                        '<spring:message code="asset.description"/>',
    'asset.status':                             '<spring:message code="asset.status"/>',
    'accountinginfo.ponumber':                  '<spring:message code="accountinginfo.ponumber"/>',
    'accountinginfo.accountnumber':             '<spring:message code="accountinginfo.accountnumber"/>',
    'accountinginfo.accumulatednumber':         '<spring:message code="accountinginfo.accumulatednumber"/>',
    'accountinginfo.expensenumber':             '<spring:message code="accountinginfo.expensenumber"/>',
    'accountinginfo.acquisitionvalue':          '<spring:message code="accountinginfo.acquisitionvalue"/>',
    'accountinginfo.leasenumber':               '<spring:message code="accountinginfo.leasenumber"/>',
    'accountinginfo.leaseduration':             '<spring:message code="accountinginfo.leaseduration"/>',
    'accountinginfo.leasefrequency':            '<spring:message code="accountinginfo.leasefrequency"/>',
    'accountinginfo.leasefrequencyprice':       '<spring:message code="accountinginfo.leasefrequencyprice"/>',
    'accountinginfo.disposalmethod':            '<spring:message code="accountinginfo.disposalmethod"/>',
    'accountinginfo.disposaldate':              '<spring:message code="accountinginfo.disposaldate"/>',
    'accountinginfo.insurancecategory':         '<spring:message code="accountinginfo.insurancecategory"/>',
    'accountinginfo.replacementvaluecategory':  '<spring:message code="accountinginfo.replacementvaluecategory"/>',
    'accountinginfo.replacementvalue':          '<spring:message code="accountinginfo.replacementvalue"/>',
    'accountinginfo.maintenancecost':           '<spring:message code="accountinginfo.maintenancecost"/>',
    'accountinginfo.depreciationmethod':        '<spring:message code="accountinginfo.depreciationmethod"/>',
    'accountinginfo.leaseexpirationdate':       '<spring:message code="accountinginfo.leaseexpirationdate"/>',
    'accountinginfo.warrantydate':              '<spring:message code="accountinginfo.warrantydate"/>'
};


/**
 * Creates a hidden field for each item in the dnd target list.
 */
function createColOrderFields() {
    var div = document.getElementById('colOrderFlds');
    // remove existing fields
    while (div.childNodes[0]) {
        div.removeChild(div.childNodes[0]);
    }

    // add a hidden field for each item in the columnOrderBox
    var cols = colOrderList.getAllNodes();
    for (var i = 0; i < cols.length; ++i) {
        var hfld = document.createElement("input");
        hfld.type = "hidden";
        hfld.name = "columnOrder";

        var ih = cols[i].innerHTML.replace(/^\s*(.*?)\s*$/, "$1");

        // reverse lookup on transFieldNames
        for (var key in transFieldNames) {
            if (transFieldNames[key] == ih) {
                hfld.value = key;
                break;
            } else {
                hfld.value = ih;
            }
        }
        div.appendChild(hfld);
    }
}

/**
 * A function for initializing the page
 */
function init() {

    // if this was just saved send a message to update parent's filter dropdown
<c:if test="${saved == 'true'}">
    window.parent.reloadFilterList('${assetFilter.id}');
</c:if>
    // if apply was clicked send a message to parent to apply the filter
<c:if test="${apply == 'true'}">
    window.parent.applyFilter('<c:url value="${applyURL}"/>');
</c:if>
    // if delete was clicked send a message to parent to delete the filter
<c:if test="${deleted == 'true'}">
    window.parent.deleteFilter();
</c:if>

    showDate(document.getElementById('disposalDateOperator'), 'dispDate');
    showDate(document.getElementById('leaseExpirationDateOperator'), 'lexpdate');
    showDate(document.getElementById('warrantyDateOperator'), 'wdate');
    showDate(document.getElementById('acquisitionDateOperator'), 'acqdate');

}

dojo.addOnLoad(init);
</script>

</head>
<body class="tfBackground tundra">
<form:form commandName="assetFilter" id="afForm" name="afForm">

<input type="hidden" name="formAction" id="formAction" value="save"/>

<table style="padding:3px 3px 3px 3px">
    <tr>
        <td><spring:message code="ticketFilter.filterName"/>:</td>
        <td><form:input path="name" size="100" id="tfName"/></td>
        <td><span style="color: red"><form:errors path="name"/></span></td>
        <td>
            <form:checkbox path="privateFilter" cssStyle="vertical-align:middle"/>
            <spring:message code="ticketFilter.private"/>
        </td>
        <td>
            <form:checkbox path="report" id="isReport" onclick="toggleReportDiv(this.checked)"
                           cssStyle="vertical-align:middle;"/>
            <spring:message code="assetFilter.isReport"/>
        </td>
    </tr>
</table>

<table width="99%">
<tr>
<td valign="top" style="padding-left:5px">

<div id="reportBox">
    <table>
        <tr>
            <td><spring:message code="assetFilter.report.format"/></td>
            <td>
                <form:select path="format">
                    <form:option value="pdf">PDF</form:option>
                    <%--<form:option value="html">HTML</form:option>--%>
                    <form:option value="xls">Excel</form:option>
                    <form:option value="csv">CSV</form:option>
                </form:select>
            </td>
            <td style="padding-left:5px">
                <%--<spring:message code="assetFilter.report.graphType"/>--%>
            </td>
            <td>
                <%--<c:set var="pie"><spring:message code="assetFilter.report.pie"/></c:set>
                <c:set var="bar"><spring:message code="assetFilter.report.bar"/></c:set>
                <form:radiobutton path="showPieChart" value="true" label="${pie}" cssStyle="vertical-align:middle;"/>
                <form:radiobutton path="showPieChart" value="false" label="${bar}" cssStyle="vertical-align:middle"/>--%>
            </td>
        </tr>
        <tr>
            <td><spring:message code="assetFilter.report.groupBy"/></td>
            <td>
                <form:select path="groupBy">
                    <form:options items="${groupByOptions}" itemLabel="display" itemValue="value"/>
                </form:select>
            </td>
            <td style="padding-left:5px">
                <%--<spring:message code="assetFilter.report.detail"/>--%>
            </td>
            <td>
                <%--<form:checkbox path="showDetail"/>--%>
            </td>
        </tr>
    </table>
</div>
<br>

<div dojoType="dijit.TitlePane" title="<spring:message code="asset.fieldNames"/>" id="assetGeneral">
<table width="100%">
<tr>
<td>

<!--Asset #-->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.assetnumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="anumFld" id="anumFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('assetNumbers', 'anumFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="assetNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="anum" items="${assetFilter.assetNumbers}" varStatus="row">
                            <tr>
                                <td>${anum}</td>
                                <td align="right">
                                    <form:hidden path="assetNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('assetNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Name -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.name"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="anameFld" id="anameFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('names', 'anameFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="names" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="name" items="${assetFilter.names}" varStatus="row">
                            <tr>
                                <td>${name}</td>
                                <td align="right">
                                    <form:hidden path="names[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('names', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Type -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.type"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <select name="typeFld" id="typeFld" dojoType="dijit.form.FilteringSelect" class="afSelects">
                    <option value=""></option>
                    <c:forEach var="stat" items="${assetTypes}">
                        <option value="${stat.id}">${stat.name}</option>
                    </c:forEach>
                </select>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('typeIds', 'typeFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="typeIds" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="stat" items="${assetFilter.typeIds}" varStatus="row">
                            <tr>
                                <td>
                                    <c:forEach var="st" items="${assetTypes}">
                                        <c:if test="${st.id == stat}">
                                            ${st.name}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="typeIds[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('typeIds', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Owner -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.owner"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <div dojoType="dojo.data.ItemFileReadStore" jsId="userStore"
                     url="<c:url value="/stores/users.json"/>"></div>
                <input dojoType="dijit.form.FilteringSelect"
                       store="userStore"
                       searchAttr="label"
                       name="ctctFld"
                       id="ctctFld"
                       autocomplete="true"
                       class="afSelects"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('ownerIds', 'ctctFld');">
                    <img src="<c:url value="/images/add.gif"/>" alt=""/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="ownerIds" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="ownerId" items="${assetFilter.ownerIds}" varStatus="row">
                            <tr>
                                <td>
                                    <c:forEach var="user" items="${users}">
                                        <c:if test="${user.id == ownerId}">
                                            ${user.lastName}, ${user.firstName}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="ownerIds[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('ownerIds', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Vendor -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.vendor"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <select name="vendorFld" id="vendorFld" dojoType="dijit.form.FilteringSelect" class="afSelects">
                    <option value=""></option>
                    <c:forEach var="vendor" items="${vendors}">
                        <option value="${vendor.id}">${vendor.name}</option>
                    </c:forEach>
                </select>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('vendorIds', 'vendorFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="vendorIds" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="vendor" items="${assetFilter.vendorIds}" varStatus="row">
                            <tr>
                                <td>
                                    <c:forEach var="vend" items="${vendors}">
                                        <c:if test="${vend.id == vendor}">
                                            ${vend.name}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="vendorIds[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('vendorIds', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Asset Location -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.assetlocation"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="asslocFld" id="asslocFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('assetLocations', 'asslocFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="assetLocations" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="aloc" items="${assetFilter.assetLocations}" varStatus="row">
                            <tr>
                                <td>${aloc}</td>
                                <td align="right">
                                    <form:hidden path="assetLocations[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('assetLocations', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Location -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.location"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <select name="locFld" id="locFld" dojoType="dijit.form.FilteringSelect" class="afSelects">
                    <option value=""></option>
                    <c:forEach var="loc" items="${locations}">
                        <option value="${loc.id}">${loc.name}</option>
                    </c:forEach>
                </select>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('locationIds', 'locFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="locationIds" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="locId" items="${assetFilter.locationIds}" varStatus="row">
                            <tr>
                                <td>
                                    <c:forEach var="loc" items="${locations}">
                                        <c:if test="${loc.id == locId}">
                                            ${loc.name}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="locationIds[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('locationIds', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Group -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.group"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <select name="groupFld" id="groupFld" dojoType="dijit.form.FilteringSelect" class="afSelects">
                    <option value=""></option>
                    <c:forEach var="group" items="${groups}">
                        <option value="${group.id}">${group.name}</option>
                    </c:forEach>
                </select>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('groupIds', 'groupFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="groupIds" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="groupId" items="${assetFilter.groupIds}" varStatus="row">
                            <tr>
                                <td>
                                    <c:forEach var="gr" items="${groups}">
                                        <c:if test="${gr.id == groupId}">
                                            ${gr.name}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="groupIds[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('groupIds', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Category -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.category"/></td>
            <td align="right">
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="showModal('<c:url value="/asset/filter/fetchCategories.glml"/>', 'categoryIds');">
                    <img src="<c:url value="/images/find2.gif"/>" alt=""/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="categoryIds" class="afInnerbox" style="height:100px;">
                    <table width="100%">
                        <c:forEach var="catId" items="${assetFilter.categoryIds}" varStatus="row">
                            <tr>
                                <td>
                                    <c:forEach var="cat" items="${categories}">
                                        <c:if test="${cat.id == catId}">
                                            ${cat.name}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="categoryIds[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('categoryIds', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Category Options -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.categoryoption"/></td>
            <td align="right">
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="showModal('<c:url value="/asset/filter/fetchCategoryOptions.glml"/>', 'categoryOptionIds');">
                    <img src="<c:url value="/images/find2.gif"/>" alt=""/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="categoryOptionIds" class="afInnerbox" style="height:100px;">
                    <table width="100%">
                        <c:forEach var="catOptId" items="${assetFilter.categoryOptionIds}" varStatus="row">
                            <tr>
                                <td>
                                    <c:forEach var="catOpt" items="${catOptions}">
                                        <c:if test="${catOpt.id == catOptId}">
                                            ${catOpt.name}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="categoryOptionIds[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('categoryOptionIds', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Manufacturer -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.manufacturer"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="manFld" id="manFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('manufacturers', 'manFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="manufacturers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="man" items="${assetFilter.manufacturers}" varStatus="row">
                            <tr>
                                <td>${man}</td>
                                <td align="right">
                                    <form:hidden path="manufacturers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('manufacturers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Model Number -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.modelnumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="modelFld" id="modelFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('modelNumbers', 'modelFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="modelNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="modelNum" items="${assetFilter.modelNumbers}" varStatus="row">
                            <tr>
                                <td>${modelNum}</td>
                                <td align="right">
                                    <form:hidden path="modelNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('modelNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Serial Number -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.serialNumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="serFld" id="serFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('serialNumbers', 'serFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="serialNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="serialnum" items="${assetFilter.serialNumbers}" varStatus="row">
                            <tr>
                                <td>${serialnum}</td>
                                <td align="right">
                                    <form:hidden path="serialNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('serialNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Description -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.description"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="descFld" id="descFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('descriptions', 'descFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="descriptions" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="desc" items="${assetFilter.descriptions}" varStatus="row">
                            <tr>
                                <td>${desc}</td>
                                <td align="right">
                                    <form:hidden path="descriptions[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('descriptions', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!--Status -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.status"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <select name="statFld" id="statFld" dojoType="dijit.form.FilteringSelect" class="afSelects">
                    <option value=""></option>
                    <c:forEach var="stat" items="${assetStatus}">
                        <option value="${stat.id}">${stat.name}</option>
                    </c:forEach>
                </select>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('statusIds', 'statFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="statusIds" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="stat" items="${assetFilter.statusIds}" varStatus="statRow">
                            <tr>
                                <td>
                                    <c:forEach var="st" items="${assetStatus}">
                                        <c:if test="${st.id == stat}">
                                            ${st.name}
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td align="right">
                                    <form:hidden path="statusIds[${statRow.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('statusIds', '${statRow.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Acquisition Date-->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="asset.acquisitionDate"/></td>
        </tr>
        <tr>
            <td colspan="2" height="110px" valign="top" style="padding:3px 3px 3px 3px">
                <form:select path="acquisitionDateOperator" id="acquisitionDateOperator" onchange="showDate(this, 'acqdate');">
                    <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                </form:select>
                <br><br>

                <div id="acqdate1" style="display:none">
                    <spring:bind path="assetFilter.acquisitionDate">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
                <br>

                <div id="acqdate2" style="display:none">
                    <spring:bind path="assetFilter.acquisitionDate2">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
            </td>
        </tr>
    </table>
</div>

</td>
</tr>
</table>
</div>

<div dojoType="dijit.TitlePane" title="<spring:message code="asset.acctgFieldNames"/>" id="acctgInfo" open="false">
<table width="100%">
<tr>
<td>

<!-- PO # -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.ponumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="poFld" id="poFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('poNumbers', 'poFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="poNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.poNumbers}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="poNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('poNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Account Number -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.accountnumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="acctFld" id="acctFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('accountNumbers', 'acctFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="accountNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.accountNumbers}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="accountNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('accountNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Accumulated Number -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.accumulatednumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="acumFld" id="acumFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('accumulatedNumbers', 'acumFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="accumulatedNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.accumulatedNumbers}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="accumulatedNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('accumulatedNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Expense Number -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.expensenumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="expFld" id="expFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('expenseNumbers', 'expFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="expenseNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.expenseNumbers}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="expenseNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('expenseNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Acquisition Value -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.acquisitionvalue"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="acqValFld" id="acqValFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('acquisitionValues', 'acqValFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="acquisitionValues" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.acquisitionValues}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="acquisitionValues[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('acquisitionValues', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Lease Number -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.leasenumber"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="leaseNumFld" id="leaseNumFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('leaseNumbers', 'leaseNumFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="leaseNumbers" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.leaseNumbers}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="leaseNumbers[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('leaseNumbers', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Lease Duration -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.leaseduration"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="leaseDurFld" id="leaseDurFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('leaseDurations', 'leaseDurFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="leaseDurations" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.leaseDurations}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="leaseDurations[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('leaseDurations', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Lease Frequency -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.leasefrequency"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="leaseFreqFld" id="leaseFreqFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('leaseFrequencys', 'leaseFreqFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="leaseFrequencys" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.leaseFrequencys}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="leaseFrequencys[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('leaseFrequencys', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Lease Frequency Price -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.leasefrequencyprice"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="leaseFreqPriceFld" id="leaseFreqPriceFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('leaseFrequencyPrices', 'leaseFreqPriceFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="leaseFrequencyPrices" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.leaseFrequencyPrices}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="leaseFrequencyPrices[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('leaseFrequencyPrices', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Disposal Method-->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.disposalmethod"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="dispMethFld" id="dispMethFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('disposalMethods', 'dispMethFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="disposalMethods" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.disposalMethods}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="disposalMethods[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('disposalMethods', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Insurance Category -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.insurancecategory"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="insFld" id="insFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('insuranceCategories', 'insFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="insuranceCategories" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.insuranceCategories}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="insuranceCategories[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('insuranceCategories', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Replacement Value Category -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.replacementvaluecategory"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="repValCatFld" id="repValCatFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('replacementValuecategories', 'repValCatFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="replacementValuecategories" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.replacementValuecategories}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="replacementValuecategories[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('replacementValuecategories', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Replacement Value -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.replacementvalue"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="repValFld" id="repValFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('replacementValues', 'repValFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="replacementValues" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.replacementValues}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="replacementValues[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('replacementValues', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Maintenance Cost -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.maintenancecost"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="maintFld" id="maintFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('maintenanceCosts', 'maintFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="maintenanceCosts" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.maintenanceCosts}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="maintenanceCosts[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('maintenanceCosts', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Depreciation Method -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.depreciationmethod"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="text" name="depMethFld" id="depMethFld" class="afInputs"
                       dojoType="dijit.form.TextBox" trim="true"/>
                <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                   onclick="addItem('depreciationMethods', 'depMethFld');">
                    <img alt="" src="<c:url value="/images/add.gif"/>"/>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="depreciationMethods" class="afInnerbox">
                    <table width="100%">
                        <c:forEach var="listItem" items="${assetFilter.depreciationMethods}" varStatus="row">
                            <tr>
                                <td>${listItem}</td>
                                <td align="right">
                                    <form:hidden path="depreciationMethods[${row.index}]"/>
                                    <a href="javascript://"
                                       onclick="delItem('depreciationMethods', '${row.index}')"
                                       title="<spring:message code="global.delete"/>">
                                        <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Disposal Date -->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.disposaldate"/></td>
        </tr>
        <tr>
            <td colspan="2" height="110px" valign="top" style="padding:3px 3px 3px 3px">
                <form:select path="disposalDateOperator" id="disposalDateOperator" onchange="showDate(this, 'dispDate');">
                    <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                </form:select>
                <br><br>

                <div id="dispDate1" style="display:none">
                    <spring:bind path="assetFilter.disposalDate">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
                <br>

                <div id="dispDate2" style="display:none">
                    <spring:bind path="assetFilter.disposalDate2">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Lease Expiration Date-->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.leaseexpirationdate"/></td>
        </tr>
        <tr>
            <td colspan="2" height="110px" valign="top" style="padding:3px 3px 3px 3px">
                <form:select path="leaseExpirationDateOperator" id="leaseExpirationDateOperator" onchange="showDate(this, 'lexpdate');">
                    <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                </form:select>
                <br><br>

                <div id="lexpdate1" style="display:none">
                    <spring:bind path="assetFilter.leaseExpirationDate">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
                <br>

                <div id="lexpdate2" style="display:none">
                    <spring:bind path="assetFilter.leaseExpirationDate2">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
            </td>
        </tr>
    </table>
</div>

<!-- Warranty Date-->
<div class="afOuterbox">
    <table>
        <tr>
            <td><spring:message code="accountinginfo.warrantydate"/></td>
        </tr>
        <tr>
            <td colspan="2" height="110px" valign="top" style="padding:3px 3px 3px 3px">
                <form:select path="warrantyDateOperator" id="warrantyDateOperator" onchange="showDate(this, 'wdate');">
                    <form:options items="${dateOps}" itemLabel="display" itemValue="value"/>
                </form:select>
                <br><br>

                <div id="wdate1" style="display:none">
                    <spring:bind path="assetFilter.warrantyDate">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
                <br>

                <div id="wdate2" style="display:none">
                    <spring:bind path="assetFilter.warrantyDate2">
                        <input type="text" name="${status.expression}" value="${status.value}"
                               style="width:100px"
                               dojoType="dijit.form.DateTextBox" />
                    </spring:bind>
                </div>
            </td>
        </tr>
    </table>
</div>

</td>
</tr>
</table>
</div>

<c:forEach var="fldGroup" items="${fieldGroups}" varStatus="row">
    <div dojoType="dijit.TitlePane" title="${fldGroup.name}" id="div_fldgrp_${row.index}" open="false">
        <c:forEach var="custFld" items="${fldGroup.customFields}">
            <div class="afOuterbox">
                <table>
                    <tr>
                        <td>${custFld.name}</td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="cfFld_${custFld.id}" id="cfFld_${custFld.id}" class="afInputs"
                                   dojoType="dijit.form.TextBox" trim="true"/>
                            <a href="javascript://" title="<spring:message code="ticketFilter.addNew"/>"
                               onclick="addItem('customFieldsDiv_${custFld.id}', 'cfFld_${custFld.id}');">
                                <img alt="" src="<c:url value="/images/add.gif"/>"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="customFieldsDiv_${custFld.id}" class="afInnerbox">
                                <table width="100%">
                                    <c:forEach var="cfval" items="${assetFilter.customFields[custFld.id]}"
                                               varStatus="innerRow">
                                        <tr>
                                            <td>${cfval}</td>
                                            <td align="right">
                                                <form:hidden path="customFields[${custFld.id}][${innerRow.index}]"/>
                                                <a href="javascript://"
                                                   onclick="delItem('customFieldsDiv_${custFld.id}', '${innerRow.index}')"
                                                   title="<spring:message code="global.delete"/>">
                                                    <img src="<c:url value="/images/delete.gif"/>" alt=""/>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </c:forEach>
    </div>
</c:forEach>

<div dojoType="dijit.TitlePane" title="<spring:message code="ticketFilter.colOrder"/>" open="false">
    <spring:message code="assetFilter.col.instruction"/>
    <spring:message code="assetFilter.col.instruction2"/>
    <spring:message code="assetFilter.col.instruction3"/>
    <table width="100%">
        <tr>
            <td valign="top" width="50%">
                <div align="center">
                    <h3><spring:message code="assetFilter.col.available"/></h3>

                    <div dojoType="dojo.dnd.Source" jsId="availableList" class="dndContainer">
                        <c:forEach var="aCol" items="${availableCols}">
                            <div class="dojoDndItem"><c:set var="msg"><spring:message code="${aCol}"
                                                                                      text=""/></c:set><c:choose><c:when
                                    test="${not empty msg}">${msg}</c:when><c:otherwise>${aCol}</c:otherwise></c:choose></div>
                        </c:forEach>
                    </div>
                </div>
            </td>
            <td valign="top">
                <div align="center">
                    <h3><spring:message code="assetFilter.col.colorder"/></h3>

                    <div dojoType="dojo.dnd.Source" jsId="colOrderList" class="dndContainer">
                        <c:forEach var="colName" items="${assetFilter.columnOrder}">
                            <div class="dojoDndItem"><c:set var="msg"><spring:message code="${colName}"
                                                                                      text=""/></c:set><c:choose><c:when
                                    test="${not empty msg}">${msg}</c:when><c:otherwise>${colName}</c:otherwise></c:choose></div>
                        </c:forEach>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>

</td>
</tr>
</table>

<div id="colOrderFlds">
    <c:forEach var="colName" items="${assetFilter.columnOrder}" varStatus="coRow">
        <form:hidden path="columnOrder[${coRow.index}]"/>
    </c:forEach>
</div>

<div dojoType="dijit.Dialog" id="fetcher" title="<spring:message code="ticketFilter.fetcherTitle"/>"
     execute="applyChecks();" style="overflow-y:auto;width:420px;height:400px;"></div>


</form:form>
</body>
</html>
