<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>
    <meta name="assetSidebar" content="true">
    <title><spring:message code="assetTracker.editAssetStatus"/></title>

    <script type="text/javascript">

    function cancelForm() {
        window.location.href = '<c:url value="/asset/statuses.glml"/>';
    }

    function init() {
    }

    dojo.addOnLoad(init);
</script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="assetTracker.editAssetStatus"/></h1>

        <form:form commandName="assetStatus" method="post" cssClass="form">
            <spring:hasBindErrors name="assetStatus">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>

            <p class="required">
                <form:label path="name"><spring:message code="global.name"/></form:label>
                <form:input path="name" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="description"><spring:message code="global.description"/></form:label>
                <form:input path="description" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p class="grey-bg no-margin" id="commandButtons">
                <button type="submit"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelForm();"><spring:message code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>

