<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="assetSidebar" content="true">

    <style type="text/css">
        .cfRed {color: red}
        /*#mainTabContainer {width:100%; height:650px;}*/
    </style>

    <script type="text/javascript">
        // variables for holding data stores
        var categoryStore, categoryOptionStore;

        function submitForm() {
            document.getElementById("changeAssetType").value = "false";
            numberHiddenSoftwareLicenses();
            document.forms[0].submit();
        }

        function cancelSubmit() {
            location.href = "<c:url value="/asset/listAsset.glml"/>";
        }

        function changeAssetType() {
            document.getElementById("changeAssetType").value = "true";
            document.forms[0].submit();
            return true;
        }

        function filterOptions(fld) {

            // determine which select box changed by the field id
            if (fld.id == "group") {

                // if changedto value is empty clear stores and return
                if (fld.value == "" || fld.value == null) {
                    clearStore("category");
                    clearStore("categoryOption");
                    return;
                }

                categoryStore = new dojo.data.ItemFileReadStore({url:'<c:url value="/stores/category.json"/>'});
                categoryStore.fetch({
                    query:{'groupId': fld.value},
                    onComplete: getCats,
                    onError: errorHandler
                });
            } else if (fld.id == "category") {

                // if changedto value is empty clear cat opt store only and return
                if (fld.value == "" || fld.value == null) {
                    clearStore("categoryOption");
                    return;
                }

                categoryOptionStore = new dojo.data.ItemFileReadStore({url:'<c:url value="/stores/categoryOption.json"/>'});
                categoryOptionStore.fetch({
                    query:{'categoryId':fld.value},
                    onComplete: getCatOpts,
                    onError: errorHandler
                });
            }
        }

        var getCats = function(items) {
            clearStore("category");
            dijit.byId("category").store = new dojo.data.ItemFileReadStore({data:{identifier:'id', label:'name', items:items}});
        };

        var getCatOpts = function(items) {
            clearStore("categoryOption");
            dijit.byId("categoryOption").store = new dojo.data.ItemFileReadStore({data:{identifier:'id', label:'name', items:items}});
        };

        var clearStore = function(id) {
            if (id == "category") {
                dijit.byId(id).store = new dojo.data.ItemFileReadStore({data:{identifier:'id', label:'name', items: [{id:'', name:'', groupId:'', active:''}]}});
            } else {
                dijit.byId(id).store = new dojo.data.ItemFileReadStore({data:{identifier:'id', label:'name', items: [{id:'', name:'', categoryId:'', active:''}]}});
            }
            dijit.byId(id).setDisplayedValue('');
        };

        var errorHandler = function(error) {
            console.debug(error);
        };

        function addLicToAsset() {
            var softLickId = dijit.byId("softLick").getValue();
            if (softLickId.length == 0) return;

            var hiddenDiv = document.getElementById("hiddenSoftwarefields");
            var hidden = document.createElement("input");
            hidden.id = "lic_inp_" + softLickId;
            hidden.type = "hidden";
            hidden.value = softLickId;
            hidden.className = "hiddenSoftLicInput";
            hiddenDiv.appendChild(hidden);

            softLicStore.fetchItemByIdentity({
                identity: softLickId,
                onItem: function(item) {
                    addNewRowToTable(item);
                }
            });
        }

        function addNewRowToTable(item) {
            var name = softLicStore.getValue(item, "name");
            var version = softLicStore.getValue(item, "version");
            var id = softLicStore.getValue(item, "id");

            var lickTable = document.getElementById("softLickTableBody");
            var newRow = document.createElement("tr");
            var newCol = document.createElement("td");
            newRow.setAttribute("id", "lic_" + id);

            newCol.appendChild(document.createTextNode(name));
            newRow.appendChild(newCol);

            newCol = document.createElement("td");
            newCol.appendChild(document.createTextNode(version));
            newRow.appendChild(newCol);

            newCol = document.createElement("td");
            var deleteLink = document.createElement("a");
            deleteLink.setAttribute("href", "javascript:removeLicenseFromAsset(" + id + ");");
            var deleteImg = document.createElement("img");
            deleteImg.setAttribute("src", "<c:url value="/images/theme/icons/fugue/cross-circle.png"/>");
            deleteLink.appendChild(deleteImg);
            newCol.appendChild(deleteLink);
            newRow.appendChild(newCol);

            lickTable.appendChild(newRow);
        }

        function removeLicenseFromAsset(id) {
            //remove the license identified by id from license table and hiddenSoftwarefields
            var license = document.getElementById("lic_" + id);
            license.parentNode.removeChild(license);

            var hiddenLicense = document.getElementById("lic_inp_" + id);
            hiddenLicense.parentNode.removeChild(hiddenLicense);
        }

        function numberHiddenSoftwareLicenses() {
            var hiddenDivChildren = dojo.query(".hiddenSoftLicInput");
            for(var i=0; i<hiddenDivChildren.length; i++) {
                hiddenDivChildren[i].name = "softLicks[" + i + "]";
            }
        }

        function init() {

            dojo.connect(dojo.byId("assetType"), "onchange", changeAssetType);

            // load up some dojo data stores for dynamic select box filtering
            categoryStore = new dojo.data.ItemFileReadStore({url:'<c:url value="/stores/category.json"/>'});
            categoryOptionStore = new dojo.data.ItemFileReadStore({url:'<c:url value="/stores/categoryOption.json"/>'});
            dijit.byId("mainTabContainer").resize();
            var infoTitleDiv = document.getElementById('infoDivTitle');
            <c:forEach var="fg" items="${asset.assetType.fieldGroups}">
                var cfTitleDiv${fg.id} = document.getElementById('cfTitleDiv${fg.id}');
            </c:forEach>
            // add cfRed to all Tabs with bind errors
            <spring:bind path="asset.*">
                <c:if test="${assTypeChanged}">
                    <c:choose>
                        <c:when test="${empty asset.status.name}">
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="cfgerr" items="${cfGroupsWithError}">
                                <%--alert("Please fill in required fields in Custom Field Group (ID: " + ${cfgerr.value} + ")");--%>
                                dojo.addClass(cfTitleDiv${cfgerr.value}, "cfRed");
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </c:if>
            </spring:bind>

            dojo.query('.view-ticket').connect("onclick", function() {
                var ticketId = $(this).parents("tr").data("ticketId");
                openTicketWindow(ticketId);
            });
        }

        dojo.addOnLoad(init);

    </script>
</head>

<body>

<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="assetTracker.editAsset" text="Edit Asset"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="<c:url value="/asset/deleteAsset.glml" ><c:param name="assetId" value="${asset.id}"/></c:url>"
                            onclick="return confirm('<spring:message code="global.confirmdelete"/>');">
                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                        <spring:message code="assetTracker.delete" />
                    </a>
                </li>
            </ul>
        </div>

        <form:form method="post" commandName="asset" cssClass="form">
            <spring:hasBindErrors name="asset">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>

            <div class="with-margin" id="mainTabContainer" dojoType="dijit.layout.TabContainer" doLayout="false">
                <div id="information" dojoType="dijit.layout.ContentPane" title="<spring:message code="asset.fieldNames"/>" selected="true">
                    <input type="hidden" id="changeAssetType" name="changeAssetType" value="false"/>
                    <div class="columns">

                        <div class="colx2-left">


                            <p class="required">
                                <form:label path="assetNumber"><spring:message code="asset.assetNumber"/></form:label>
                                <form:input path="assetNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p class="required">
                                <form:label path="name"><spring:message code="asset.name"/></form:label>
                                <form:input path="name" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p class="required">
                                <form:label path="assetType"><spring:message code="asset.type"/></form:label>
                                <form:select path="assetType" cssErrorClass="error" dojoType="dijit.form.FilteringSelect">
                                    <form:option value="">&nbsp;</form:option>
                                    <form:options items="${assetTypes}" itemValue="id" itemLabel="name" cssErrorClass="error"/>
                                </form:select>
                            </p>

                            <p class="required">
                                <form:label path="status"><spring:message code="asset.status"/></form:label>
                                <form:select path="status" cssErrorClass="error" dojoType="dijit.form.FilteringSelect">
                                    <form:option value="">&nbsp;</form:option>
                                    <form:options items="${assetStatuses}" itemValue="id" itemLabel="name" cssErrorClass="error"/>
                                </form:select>
                            </p>

                            <p>
                                <form:label path="description"><spring:message code="asset.description"/></form:label>
                                <form:input path="description" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="assetLocation"><spring:message code="asset.assetlocation"/></form:label>
                                <form:input path="assetLocation" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="owner"><spring:message code="asset.owner"/></form:label>
                                <form:select path="owner" cssErrorClass="error" dojoType="dijit.form.FilteringSelect">
                                    <form:option value="">&nbsp;</form:option>
                                    <form:options items="${users}" itemValue="id" itemLabel="displayName" cssErrorClass="error"/>
                                </form:select>
                            </p>

                            <p>
                                <form:label path="vendor"><spring:message code="asset.vendor"/></form:label>
                                <form:select path="vendor" cssErrorClass="error" dojoType="dijit.form.FilteringSelect">
                                    <form:option value="">&nbsp;</form:option>
                                    <form:options items="${vendors}" itemValue="id" itemLabel="name" cssErrorClass="error"/>
                                </form:select>
                            </p>

                        </div>

                        <div class="colx2-right">

                            <p>
                                <form:label path="location"><spring:message code="asset.location"/></form:label>
                                <form:select path="location" cssErrorClass="error" dojoType="dijit.form.FilteringSelect">
                                    <form:option value="">&nbsp;</form:option>
                                    <form:options items="${locations}" itemValue="id" itemLabel="name" cssErrorClass="error"/>
                                </form:select>
                            </p>

                            <p>
                                <form:label path="group"><spring:message code="asset.group"/></form:label>
                                <spring:bind path="asset.group">
                                    <select name="${status.expression}" id="group" dojoType="dijit.form.FilteringSelect"
                                            onchange="filterOptions(this);" required="false" trim="true">
                                        <option value="">&nbsp;</option>
                                        <c:forEach var="group" items="${groups}">
                                            <option value="<c:out value="${group.id}"/>"
                                                    <c:if test="${group.id == status.value}">
                                                        selected="selected"
                                                    </c:if>
                                                    >${group.name}</option>
                                        </c:forEach>
                                    </select>
                                </spring:bind>
                            </p>

                            <p>
                                <form:label path="category"><spring:message code="asset.category"/></form:label>
                                <spring:bind path="asset.category">
                                    <select name="${status.expression}" id="category" dojoType="dijit.form.FilteringSelect"
                                            onchange="filterOptions(this);" required="false" trim="true">
                                        <option value="">&nbsp;</option>
                                        <c:forEach var="category" items="${categories}">
                                            <option value="<c:out value="${category.id}"/>"
                                                    <c:if test="${category.id == status.value}">
                                                        selected="selected"
                                                    </c:if>
                                                    >${category.name}</option>
                                        </c:forEach>
                                    </select>

                                </spring:bind>
                            </p>

                             <p>
                                <form:label path="categoryOption"><spring:message code="asset.categoryoption"/></form:label>
                                <spring:bind path="asset.categoryOption">
                                    <select name="${status.expression}" id="categoryOption" dojoType="dijit.form.FilteringSelect"
                                            required="false" trim="true">
                                        <option value="">&nbsp;</option>
                                        <c:forEach var="catOpt" items="${categoryOptions}">
                                            <option value="<c:out value="${catOpt.id}"/>"
                                                    <c:if test="${catOpt.id == status.value}">
                                                        selected="selected"
                                                    </c:if>
                                                    >${catOpt.name}</option>
                                        </c:forEach>
                                    </select>
                                </spring:bind>
                            </p>

                            <p>
                                <form:label path="modelNumber"><spring:message code="asset.modelNumber"/></form:label>
                                <form:input path="modelNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="serialNumber"><spring:message code="asset.serialNumber"/></form:label>
                                <form:input path="serialNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                                <p>
                                <form:label path="manufacturer"><spring:message code="asset.manufacturer"/></form:label>
                                <form:input path="manufacturer" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="acquisitionDate"><spring:message code="asset.acquisitionDate"/></form:label>
                                <form:input path="acquisitionDate" dojoType="dijit.form.DateTextBox" cssErrorClass="error"/>
                            </p>

                        </div>

                    </div>

                </div>

                <div id="accountingInfo" dojoType="dijit.layout.ContentPane" title="<spring:message code="asset.acctgFieldNames"/>">

                    <div class="columns">
                        <div class="colx2-left">
                            <p>
                                <form:label path="accountingInfo.poNumber"><spring:message code="accountinginfo.ponumber"/></form:label>
                                <form:input path="accountingInfo.poNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.accountNumber"><spring:message code="accountinginfo.accountnumber"/></form:label>
                                <form:input path="accountingInfo.accountNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.accumulatedNumber"><spring:message code="accountinginfo.accumulatednumber"/></form:label>
                                <form:input path="accountingInfo.accumulatedNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.expenseNumber"><spring:message code="accountinginfo.expensenumber"/></form:label>
                                <form:input path="accountingInfo.expenseNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.acquisitionValue"><spring:message code="accountinginfo.acquisitionvalue"/></form:label>
                                <form:input path="accountingInfo.acquisitionValue" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.leaseNumber"><spring:message code="accountinginfo.leasenumber"/></form:label>
                                <form:input path="accountingInfo.leaseNumber" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.leaseExpirationDate"><spring:message code="accountinginfo.leaseexpirationdate"/></form:label>
                                <form:input path="accountingInfo.leaseExpirationDate" dojoType="dijit.form.DateTextBox" cssErrorClass="error"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.leaseDuration"><spring:message code="accountinginfo.leaseduration"/></form:label>
                                <form:input path="accountingInfo.leaseDuration" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.leaseFrequency"><spring:message code="accountinginfo.leasefrequency"/></form:label>
                                <form:input path="accountingInfo.leaseFrequency" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>
                        </div>

                        <div class="colx2-right">
                            <p>
                                <form:label path="accountingInfo.leaseFrequencyPrice"><spring:message code="accountinginfo.leasefrequencyprice"/></form:label>
                                <form:input path="accountingInfo.leaseFrequencyPrice" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>
                            
                            <p>
                                <form:label path="accountingInfo.warrantyDate"><spring:message code="accountinginfo.warrantydate"/></form:label>
                                <form:input path="accountingInfo.warrantyDate" dojoType="dijit.form.DateTextBox" cssErrorClass="error"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.disposalDate"><spring:message code="accountinginfo.disposaldate"/></form:label>
                                <form:input path="accountingInfo.disposalDate" dojoType="dijit.form.DateTextBox" cssErrorClass="error"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.disposalMethod"><spring:message code="accountinginfo.disposalmethod"/></form:label>
                                <form:input path="accountingInfo.disposalMethod" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.insuranceCategory"><spring:message code="accountinginfo.insurancecategory"/></form:label>
                                <form:input path="accountingInfo.insuranceCategory" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.replacementValueCategory"><spring:message code="accountinginfo.replacementvaluecategory"/></form:label>
                                <form:input path="accountingInfo.replacementValueCategory" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.replacementValue"><spring:message code="accountinginfo.replacementvalue"/></form:label>
                                <form:input path="accountingInfo.replacementValue" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.maintenanceCost"><spring:message code="accountinginfo.maintenancecost"/></form:label>
                                <form:input path="accountingInfo.maintenanceCost" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                            <p>
                                <form:label path="accountingInfo.depreciationMethod"><spring:message code="accountinginfo.depreciationmethod"/></form:label>
                                <form:input path="accountingInfo.depreciationMethod" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                            </p>

                        </div>
                    </div>
                </div>

                <div id="helpdesk" dojoType="dijit.layout.ContentPane" title="<spring:message code="asset.associatedTickets"/>">

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col"><spring:message code="ticket.group"/></th>
                            <th scope="col"><spring:message code="ticket.subject"/></th>
                            <th scope="col"><spring:message code="ticket.status"/></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${asset.orderList}" var="ticket">
                            <tr data-ticket-id="${ticket.id}">
                                <td>${ticket.id}</td>
                                <td><c:out value="${ticket.group.name}"/></td>
                                <td><ehd:trimString value="${ticket.subject}" length="25"/></td>
                                <td><c:out value="${ticket.status.name}"/></td>
                                <td>
                                    <a href="javascript:void(0)" class="view-ticket">
                                        <img src="<c:url value="/images/view.gif"/>" alt="">
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div id="software" dojoType="dijit.layout.ContentPane" title="<spring:message code="softwareLicense.title"/>" style="padding:10px">
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="softLicStore"
                         url="<c:url value="/stores/softwareLics.json"/>"></div>
                    <input dojoType="dijit.form.FilteringSelect" store="softLicStore" searchAttr="name" id="softLick" autocomplete="true" required="false"/>
                    <img src="<c:url value="/images/addButton.gif"/>" alt=""
                         title="<spring:message code="asset.addLicense" text="Add license to this asset"/>" onclick="addLicToAsset();"/>

                    <div id="hiddenSoftwarefields">
                        <c:forEach var="softwareLicense" items="${asset.softwareLicenses}" varStatus="row">
                            <input id="lic_inp_${softwareLicense.id}" type="hidden" value="${softwareLicense.id}" class="hiddenSoftLicInput"/>
                        </c:forEach>
                    </div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"><spring:message code="softwareLicense.productName"/></th>
                            <th scope="col"><spring:message code="softwareLicense.productVersion"/></th>
                            <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
                        </tr>
                        </thead>
                        <tbody id="softLickTableBody">
                        <c:forEach var="softwareLicense" items="${asset.softwareLicenses}">
                            <tr id="lic_${softwareLicense.id}">
                                <td><c:out value="${softwareLicense.productName}"/></td>
                                <td><c:out value="${softwareLicense.productVersion}"/></td>
                                <td>
                                    <a href="javascript:removeLicenseFromAsset(<c:out value="${softwareLicense.id}" />);">
                                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div id="remoteManagement" dojoType="dijit.layout.ContentPane" title="<spring:message code="assetTracker.remoteManagement"/>"
                     style="padding:10px">

                    <p>
                        <form:label path="hostname"><spring:message code="assetTracker.remoteManagement.hostName"/></form:label>
                        <form:input path="hostname" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="255"/>
                    </p>

                    <p>
                        <form:label path="vncPort"><spring:message code="assetTracker.remoteManagement.vncPort"/></form:label>
                        <form:input path="vncPort" dojoType="dijit.form.TextBox" cssErrorClass="error" maxlength="6"/>
                    </p>

                    <c:if test="${!empty asset.hostname && !empty asset.vncPort}">
                        <p>
                            <a href="<c:url value="/jnlp/vncviewerAsset.jnlp"><c:param name="assetId"
                                                        value="${asset.id}"/></c:url>">
                                <img src="<c:url value="/images/RMPolicy.png"/>" alt="<spring:message code="remoteControl.remoteControl"/>">
                                <spring:message code="remoteControl.remoteControl"/>
                            </a>
                        </p>
                    </c:if>
                </div>

                <c:forEach var="fieldGroup" items="${asset.assetType.fieldGroups}">
                    <div id="fieldGroup<c:out value="${fieldGroup.id}"/>" dojoType="dijit.layout.ContentPane"
                         title="<span id='cfTitleDiv${fieldGroup.id}'><c:out value="${fieldGroup.name}"/></span>">

                        <c:forEach var="customField" items="${fieldGroup.prioritizedCustomFields}" varStatus="vs">
                            <c:set var="cfId" value="${customField.id}"/>
                            <c:set var="cfName" value="cf_${customField.id}"/>

                            <p <c:if test="${customField.required}"> class="required"</c:if>>

                                <label for="${cfName}"><c:out value="${customField.name}"/></label>
                                <c:choose>
                                    <c:when test="${customField.customFieldType == 'text'}">
                                        <input type="text" dojotype="dijit.form.TextBox" name="${cfName}" maxlength="255"
                                               value="${customFieldValues[cfId]}"/>
                                    </c:when>
                                    <c:when test="${customField.customFieldType == 'date'}">
                                        <input type="text" dojotype="dijit.form.DateTextBox"
                                               name="${cfName}" value="${customFieldValues[cfId]}"/>
                                    </c:when>
                                    <c:when test="${customField.customFieldType == 'textarea'}">
                                        <%--<textarea dojotype="dijit.form.Textarea" cols="10" rows="5">${customFieldValues[cfId]}</textarea>--%>
                                        <textarea name="${cfName}" dojotype="dijit.form.Textarea"
                                                  style="width:300px; font: 11px Arial, Myriad, Tahoma, Verdana, sans-serif;"
                                                  rows="5" cols="10">${customFieldValues[cfId]}</textarea>
                                    </c:when>
                                    <c:when test="${customField.customFieldType == 'checkbox'}">
                                        <input type="hidden" name="${cfName}" value=""/>
                                        <input type="checkbox" dojotype="dijit.form.CheckBox" name="${cfName}" value="true"
                                                <c:if test="${customFieldValues[cfId] eq 'true'}">
                                                    checked="checked"
                                                </c:if>
                                                />
                                    </c:when>
                                    <c:when test="${customField.customFieldType == 'radio'}">
                                        <c:forEach var="cfOption" items="${customField.customFieldOptions}">
                                            <input type="radio" id="cf${customField.id}cfOption${cfOption.id}"
                                                   dojotype="dijit.form.RadioButton" name="${cfName}" value="${cfOption.value}"
                                                    <c:if test="${customFieldValues[cfId] eq cfOption.value}">
                                                        checked="checked"
                                                    </c:if>
                                                    />
                                            <label for="cf${customField.id}cfOption${cfOption.id}">${cfOption.value}</label>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${customField.customFieldType == 'select'}">
                                        <select name="${cfName}">
                                            <option value="">&nbsp;</option>
                                            <c:forEach var="cfOption" items="${customField.customFieldOptions}">
                                                <option
                                                        <c:if test="${customFieldValues[cfId] eq cfOption.value}">
                                                            selected="selected"
                                                        </c:if>
                                                        >${cfOption.value}</option>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                </c:choose>
                            </p>
                        </c:forEach>
                    </div>
                </c:forEach>
            </div>

            <p class="grey-bg no-margin" id="commandButtons">
                <button type="button" onclick="submitForm();"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelSubmit();"><spring:message code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
