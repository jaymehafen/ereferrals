<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="model" type="java.util.Map"--%>

<div style="overflow:auto;height:300px;width:400px">
    <c:forEach var="entry" items="${model}" varStatus="row">
        <c:set var="groupName" value="${entry.key}"/>
        <c:set var="categories" value="${entry.value}"/>
        <div dojoType="dijit.TitlePane" title="${groupName}" id="div_${row.index}" open="false">
            <input type="checkbox" onclick="toggleSelectAll(this, 'cbx_${row.index}');"
                   dojoType="dijit.form.CheckBox"/>
            <spring:message code="ticketFilter.selectAll"/>
            <br>
            <c:forEach var="category" items="${categories}">
                <div style="float:left;margin-left:3px;padding-left:3px">
                    <nobr>
                        <input type="checkbox" name="cbx_${row.index}" id="cbx_${row.index}_${category.id}"
                               dojoType="dijit.form.CheckBox" value="${category.id}"/>${category.name}
                    </nobr>
                </div>
            </c:forEach>
            <br>
        </div>
    </c:forEach>
</div>
<div>
    <button dojoType="dijit.form.Button" type="submit">
        <spring:message code="ticketFilter.apply"/>
    </button>
</div>