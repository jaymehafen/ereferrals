<%@ include file="/WEB-INF/jsp/include.jsp" %>

<head>

    <meta name="assetSidebar" content="true">
    <title><spring:message code="customField.setupTitle"/></title>

    <script type="text/javascript">

        function showCustomFieldOptionsDiv(selectBox) {
            // Get the custom field type selected
            var custFieldType = selectBox.value;
            document.getElementById("divCustFieldOptions").style.display = (custFieldType == "select" || custFieldType == "radio") ? "block" : "none";
        }

        function addCustomFieldOption() {
            var optTable = document.getElementById("tblFieldOpts");
            var newcfopt_text = document.getElementById("newcfopt_text").value;

            var tblIndex = optTable.rows.length - 1;
            var insIndex = tblIndex - 1;

            var newRow = optTable.insertRow(tblIndex);

            var leftCell = newRow.insertCell(0);
            var el = document.createElement('input');
            el.type = 'text';
            el.name = 'customFieldOptions.value';
            el.value = newcfopt_text;
            el.size = 30;
            leftCell.appendChild(el);

            // Clear the last field and incrment the index
            document.getElementById("newcfopt_text").value = "";
            document.getElementById("newcfopt_id").value = insIndex;

            var rightCell = newRow.insertCell(1);
            el = document.createElement('a');
            el.id = tblIndex;
            el.href = 'javascript://';
            el.onclick = delNewOpt;

            var delImage = document.createElement("img");
            delImage.alt = "";
            delImage.align = "middle";
            delImage.title = "<spring:message code="customField.delete"/>";
            delImage.src = "<c:url value="/images/theme/icons/fugue/cross-circle.png" />";
            el.appendChild(delImage);
            rightCell.appendChild(el);

        }

        function delNewOpt(s) {
            var theRowToDel;
            if (s.parentNode) {
                theRowToDel = s.parentNode.parentNode;
            } else {
                theRowToDel = this.parentNode.parentNode;
            }

            var optTable = document.getElementById("tblFieldOpts");
            optTable.deleteRow(theRowToDel.rowIndex);
        }

        function cancelForm() {
            window.location.href = '<c:url value="/asset/customFields.glml"/>';
        }

        function init() {
            showCustomFieldOptionsDiv(dijit.byId("cfType"));
        }

        dojo.addOnLoad(init);
    </script>
</head>

<body>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="customField.setupTitle"/></h1>

        <form:form commandName="customField" method="post" cssClass="form">
            <spring:hasBindErrors name="customField">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>

            <input type="hidden" name="fieldGroupId" value="<c:out value="${fieldGroupId}"/>"/>
            <input type="hidden" name="customFieldId" value="<c:out value="${customField.id}"/>"/>

            <p class="required">
                <form:label path="name"><spring:message code="global.name"/></form:label>
                <form:input path="name" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="description"><spring:message code="global.description"/></form:label>
                <form:input path="description" cssErrorClass="error" dojoType="dijit.form.TextBox" maxlength="255"/>
            </p>

            <p>
                <form:label path="customFieldType"><spring:message code="customField.fieldType"/></form:label>
                <spring:bind path="customField.customFieldType">


                    <select name="${status.expression}" dojoType="dijit.form.FilteringSelect" id="cfType"
                            onchange="showCustomFieldOptionsDiv(this);">
                        <c:forEach items="${customFieldTypes}" var="entry">
                            <option value="${entry.key}"
                                    <c:if test="${status.value == entry.key}">
                                        selected="true"
                                    </c:if>
                                    >${entry.value}</option>
                        </c:forEach>
                    </select>

                </spring:bind>
            </p>

            <p>
                <form:label path="required"><spring:message code="customField.required"/></form:label>
                <form:checkbox path="required" cssErrorClass="error" dojoType="dijit.form.CheckBox"/>
            </p>

            <p>
                <form:label path="fieldOrder"><spring:message code="global.order"/></form:label>
                <form:input path="fieldOrder" cssErrorClass="error" dojoType="dijit.form.NumberSpinner"/>
            </p>

            <div id="divCustFieldOptions" style="display:none;">
                <div id="divCustFieldOptionsTable">
                    <table id="tblFieldOpts" class="table">
                        <thead>
                        <tr>
                            <th colspan="2"><spring:message code="customField.options"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <spring:bind path="customField.customFieldOptions">
                            <c:forEach var="customFieldOption" items="${customField.customFieldOptions}" varStatus="cfrow">
                                <tr>
                                    <td>
                                        <input type="text" size="30" maxlength="255" id="input_${cfrow.index}" name="customFieldOptions.value"
                                               value="${customFieldOption.value}"/>
                                    </td>
                                    <td align="center">
                                        <a href="javascript://" onclick="delNewOpt(this);">
                                            <img alt="" align="middle" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"
                                                 title="<spring:message code="customField.delete"/>"/>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </spring:bind>
                        <c:set var="newIndex" value="${fn:length(customField.customFieldOptions)}"/>
                        <tr>
                            <td>
                                <input type="hidden" name="newcfopt_id" id="newcfopt_id" value="${newIndex}"/>
                                <input type="text" size="30" name="newcfopt_text" id="newcfopt_text"/>
                            </td>
                            <td align="center">
                                <a href="javascript://" onclick="addCustomFieldOption()">
                                    <img src="<c:url value="/images/addButton.gif"/>" alt=""
                                         title="<spring:message code="customField.addOption"/>"/>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
            <p class="grey-bg no-margin" id="commandButtons">
                <button type="submit"><spring:message code="global.save"/></button>
                <button type="button" onclick="cancelForm();"><spring:message
                        code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
