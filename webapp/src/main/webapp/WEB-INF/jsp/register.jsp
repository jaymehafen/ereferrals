<%@ include file="/WEB-INF/jsp/include.jsp"%>
<jsp:useBean id="user" class="net.grouplink.ehelpdesk.domain.User" />
<head>
    <title><spring:message code="register.title" /></title>
</head>
<body>
<section id="register" class="login-block">
    <div class="block-content">
        <h1><spring:message code="register.title" /></h1>
        <img class="logo" src="<c:url value='/images/logo.png'/>" alt="" />
        <form action="" method="post" class="form">
            <spring:bind path="user.*">
                <c:if test="${not empty status.errorMessages}">
                    <div class="message error no-margin">
                        <c:forEach items="${status.errorMessages}" var="errorMsg">
                            <c:out value="${errorMsg}"/><br />
                        </c:forEach>
                    </div>
                </c:if>
            </spring:bind>
            <p class="inline-small-label">
                <spring:bind path="user.firstName">
                    <label for="${status.expression}"><spring:message code="user.firstName" /></label>
                    <input class="full-width<c:if test="${not empty status.errorMessage}"> error</c:if>" type="text"
                           name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
                </spring:bind>
            </p>
            <p class="inline-small-label">
                <spring:bind path="user.lastName">
                    <label for="${status.expression}"><spring:message code="user.lastName" /></label>
                    <input class="full-width<c:if test="${not empty status.errorMessage}"> error</c:if>" type="text"
                           name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
                </spring:bind>
            </p>
            <p class="inline-small-label">
                <spring:bind path="user.email">
                    <label for="${status.expression}"><spring:message code="user.email" /></label>
                    <input class="full-width<c:if test="${not empty status.errorMessage}"> error</c:if>" type="text"
                           name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
                </spring:bind>
            </p>
            <p class="inline-small-label">
                <spring:bind path="user.loginId">
                    <label for="${status.expression}"><spring:message code="login.id" /></label>
                    <input class="full-width<c:if test="${not empty status.errorMessage}"> error</c:if>" type="text"
                           name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
                </spring:bind>
            </p>
            <p class="inline-small-label">
                <spring:bind path="user.changePassword">
                    <label for="${status.expression}"><spring:message code="login.password" /></label>
                    <input class="full-width<c:if test="${not empty status.errorMessage}"> error</c:if>" type="password"
                           name="${status.expression}" />
                </spring:bind>
            </p>
            <p class="inline-small-label">
                <spring:bind path="user.retypePassword">
                    <label for="${status.expression}"><spring:message code="mailconfig.repassword" /></label>
                    <input class="full-width<c:if test="${not empty status.errorMessage}"> error</c:if>" type="password"
                           name="${status.expression}" />
                </spring:bind>
            </p>
            <p class="float-right">
                <button type="submit"><spring:message code="register.register"/></button>
                <button type="button" onclick="location.href = '<c:url value="/"/>';"><spring:message code="global.cancel"/></button>
            </p>

            <div class="clearfix"></div>
        </form>
    </div>
</section>
</body>