<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>

    <title><spring:message code="global.helpDeskAndIncidentManagement"/> <spring:message code="global.login"/></title>

    <c:choose>
        <c:when test="${ehd:isIPodPhone(header['User-Agent'])}">
            <meta content="yes" name="apple-mobile-web-app-capable"/>
            <meta content="index,follow" name="robots"/>
            <meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type"/>
            <link href="<c:url value="/images/eReferrals_logo.png"/>" rel="apple-touch-icon">
            <meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no"
                  name="viewport"/>
            <link href="<c:url value="/css/iwk_style.css"/>" rel="stylesheet" type="text/css">
            <link href="<c:url value="/css/login.ip.css"/>" rel="stylesheet" type="text/css">
            <script src="<c:url value="/js/iwk_functions.js"/>" type="text/javascript"></script>
        </c:when>
        <c:otherwise>
            <script type="text/javascript">
                function blankLoginSelect() {
                    var lginSlct = document.getElementById('loginSelect');
                    if (lginSlct != null) lginSlct.options[0].selected = true;
                }

                function fillLoginId(sel) {
                    document.getElementById('loginId').value = sel.options[sel.selectedIndex].value;
                }

                function breakOutOfFrame() {
                    if (top.location != location) {
                        top.location.href = document.location.href;
                    }
                }

                dojo.addOnLoad(function() {
                    dojo.byId('loginId').focus();
                    breakOutOfFrame();
                });

            </script>
        </c:otherwise>
    </c:choose>
</head>

<c:choose>
<c:when test="${ehd:isIPodPhone(header['User-Agent'])}">
    <body>
    <form action="j_acegi_security_check" method="post">

        <div id="topbar">
            <div id="title"><spring:message code="global.systemName"/></div>
        </div>

        <div id="content">
            <ul class="pageitem">
                <li class="textbox" style="text-align:center">
                    <img src="<c:url value='/images/eReferrals_169_50.png'/>" alt=""/>
                </li>
                <li class="form">
                    <input type="text" name="j_username" autocorrect="off" autocapitalize="off" placeholder="<spring:message code="login.id"/>"
                            <c:if test="${not empty param.login_error}"> value="<c:out value="${sessionScope.SPRING_SECURITY_LAST_USERNAME}"/>"</c:if>/>
                </li>
                <li class="form">
                    <input type="password" name="j_password" placeholder="<spring:message code="login.password"/>">
                </li>
                <li class="form">
                    <input type="submit" value="<spring:message code="global.login"/>"/>
                </li>
                <c:if test="${not empty param.login_error}">
                <li class="textbox">
                    <span>
                        <c:if test="${param.login_error == 1}">
                            <spring:message code="login.error"/>
                            <BR>
                            <BR><c:out value="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message}"/>
                        </c:if>
                        <c:if test="${param.login_error == 2}">
                            <spring:message code="login.error.session"/>
                            <BR>
                            <BR>
                            <spring:message code="login.error.sameuser"/>
                        </c:if>
                        <c:if test="${param.login_error == 3}">
                            <spring:message code="login.error.session"/>
                            <BR>
                        </c:if>

                    </span>
                </li>
                </c:if>
            </ul>

        </div>
    </form>
    </body>
    
</c:when>
<c:otherwise>
    <body>
    <section class="login-block">
        <div class="block-content no-title">
            <div class="with-margin"><img class="logo" src="<c:url value='/images/logo.png'/>" alt=""/></div>
                <form class="form with-margin" action="j_acegi_security_check" method="post">
                    <c:if test="${not empty param.login_error}">
                        <p class="message error no-margin">
                            <c:if test="${param.login_error == 1}">
                                <spring:message code="login.error"/><br />
                                <c:out value="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION.message}"/>
                            </c:if>
                            <c:if test="${param.login_error == 2}">
                                <spring:message code="login.error.session"/><br />
                                <spring:message code="login.error.sameuser"/>
                            </c:if>
                            <c:if test="${param.login_error == 3}">
                                <spring:message code="login.error.session"/>
                            </c:if>
                        </p>
                    </c:if>
                    <p class="inline-small-label">
                        <label for="loginId"><spring:message code="login.id"/></label>
                        <input class="full-width" id="loginId" type="text" name="j_username"
                               <c:if test="${not empty param.login_error}">
                               value="<c:out value="${sessionScope.SPRING_SECURITY_LAST_USERNAME}"/>"</c:if>>
                        <c:if test="${not empty sessionScope.ldapUserList}">
                            <select id="loginSelect" onchange="fillLoginId(this);">
                                <option value="">----</option>
                                <c:forEach items="${sessionScope.ldapUserList}" var="ldapUser">
                                    <option value="<c:out value="${ldapUser.dn}" />">
                                        <c:out value="${ldapUser.dn}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </c:if>
                    </p>
                    <p class="inline-small-label">
                        <label for="password"><spring:message code="login.password"/></label>
                        <input class="full-width" id="password" type="password" name="j_password" size="25">
                    </p>
                    <p class="inline-small-label">
                        <label for="language"><spring:message code="global.language"/></label>
                        <tags:localeSelect name="language"
                                onChange="location.href=location.href.replace(/\?.*/,'')+'?locale='+this.value;"
                                selectedCountry="${pageContext.response.locale.country}"
                                selectedLanguage="${pageContext.response.locale.language}"/>
                    </p>
                    <button type="submit" class="float-right"><spring:message code="global.login"/></button>
                    <div class="clearfix"></div>
                </form>
                <div class="block-footer">
                    <div class="columns">
                        <div class="colx3-left">
                            <a class="button" href="<c:url value="/kb/kbSearch.glml"/>">
                                <spring:message code="kb.knowledgeBaseSearch"/>
                            </a>
                        </div>
                        <div class="colx3-center">
                            <c:if test="${not applicationScope.ldapIntegration}">
                                <a class="button" href="<c:url value="/resetPassword.aglml"/>">
                                    <spring:message code="login.forgotPassword"/>
                                </a>
                            </c:if>
                        </div>
                        <div class="colx3-right">
                            <c:if test="${applicationScope.allowUserRegistration}">
                                <a class="button" href="<c:url value="/register.aglml"/>">
                                    <spring:message code="login.register"/>
                                </a>
                            </c:if>
                        </div>
                    </div>
                </div>
        </div>
        <p class="align-center fyd">
            <spring:message code="global.copyright"/>
            <spring:message code="global.helpDeskAndIncidentManagement"/> -
            <spring:message code="global.cloudHostedOnPremiseOrFreeTrial"/> - 
            <a target="_blank" href="http://www.grouplink.com">
                www.GroupLink.com
            </a>
        </p>
    </section>
    </body>
</c:otherwise>
</c:choose>

