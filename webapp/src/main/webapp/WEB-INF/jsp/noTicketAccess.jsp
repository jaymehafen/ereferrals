<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<title><spring:message code="global.accessDenied"/></title>

    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>
    <link rel="stylesheet" href="<c:url value="/css/noTicketAccess.css"/>" type="text/css">

</head>

<body>
<c:if test="${ticketAccess}">
    <div class="topheader"></div>
    <div class="message">
        <p class="redmessage">
            <img src="<c:url value="/images/remove_32.gif" />" alt="<spring:message code="global.accessDenied"/>"/>
            <spring:message code="${redMessage}" />
        </p>
        <p class="explanation">
            <spring:message code="${message}" />
        </p>
    </div>
</c:if>
<c:if test="${kbAccess}">
    <div class="message">
        <p class="redmessage">
            <img src="<c:url value="/images/remove_32.gif" />" alt="<spring:message code="global.accessDenied"/>"/>
            <spring:message code="${message}" />
        </p>
    </div>
</c:if>
</body>
</html>
