<section class="grid_2">
    <div dojoType="dijit.TitlePane" title="<spring:message code="assetTracker.assets"/>">
        <ul class="simple-list">
            <li>
                <a href="<c:url value="/asset/listAsset.glml" />">
                    <spring:message code="assetTracker.listAssets"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/assets/filters/new"/>">
                    <spring:message code="assetTracker.searchAsset"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/assets/filters"/>">
                    <spring:message code="assetFilter.myFilters"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/assetEdit.glml" />">
                    <spring:message code="assetTracker.addAsset"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/vendors.glml" />">
                    <spring:message code="assetTracker.vendors"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/softwareLicenses.glml" />">
                    <spring:message code="softwareLicense.title"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/import.glml" ><c:param name="forceRefresh" value="true" /></c:url>">
                    <spring:message code="assetTracker.import"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/reports.glml" />">
                    <spring:message code="assetTracker.reports"/>
                </a>
            </li>

        </ul>
    </div>
    <div dojoType="dijit.TitlePane" title="<spring:message code="assetTracker.settings"/>">
        <ul class="simple-list">
            <li>
                <a href="<c:url value="/asset/assetTypes.glml" />">
                    <spring:message code="assetTracker.assetTypes"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/statuses.glml" />">
                    <spring:message code="assetTracker.statuses"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/fieldGroups.glml" />">
                    <spring:message code="assetTracker.fieldGroups"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/asset/customFields.glml" />">
                    <spring:message code="asset.custfieldNames"/>
                </a>
            </li>
        </ul>
    </div>
</section>
