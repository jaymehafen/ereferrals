<header>
    <div class="container_12">
        <img src="<c:url value="/images/logo.png"/>">
    </div>
</header>
<nav id="main-nav">
    <ehd:tabs htmlClass="container_12" currentTab="${currentTab}">
    <authz:authorize ifAnyGranted="ROLE_MANAGER, ROLE_TECHNICIAN, ROLE_USER">
        <ehd:tab name="mytickets" htmlClass="mytickets">
            <jsp:attribute name="title"><spring:message code="tabs.myTickets"/></jsp:attribute>
            <jsp:attribute name="url"><c:url value="/home.glml"/></jsp:attribute>
        </ehd:tab>
    </authz:authorize>
    <ehd:tab name="newticket" htmlClass="newticket" url="#" >
        <jsp:attribute name="title"><spring:message code="tabs.newTicket"/></jsp:attribute>
    </ehd:tab>
    <ehd:tab name="ticketsearch" htmlClass="ticketsearch" >
        <jsp:attribute name="title"><spring:message code="tabs.search"/></jsp:attribute>
        <jsp:attribute name="url"><c:url value="/tf/ticketFilterEdit.glml"><c:param name="clearSessionAttr" value="true"/></c:url></jsp:attribute>
    </ehd:tab>
    <ehd:tab name="ticketfilter" htmlClass="ticketfilter" >
        <jsp:attribute name="title"><spring:message code="tabs.filters"/></jsp:attribute>
        <jsp:attribute name="url"><c:url value="/tf/ticketFilterList.glml"/></jsp:attribute>
    </ehd:tab>
    <ehd:tab name="reports" htmlClass="reports" >
        <jsp:attribute name="title"><spring:message code="tabs.reports"/></jsp:attribute>
        <jsp:attribute name="url"><c:url value="/tf/reportList.glml"/></jsp:attribute>
    </ehd:tab>
    <c:if test="${sessionScope.showEhdAssetTracker}">
        <ehd:authorizeGlobal permission="global.assetTracker">
            <ehd:tab name="assets" htmlClass="assets" >
                <jsp:attribute name="title"><spring:message code="tabs.assetTracker"/></jsp:attribute>
                <jsp:attribute name="url"><c:url value="/asset/listAsset.glml"/></jsp:attribute>
            </ehd:tab>
        </ehd:authorizeGlobal>
    </c:if>
    <ehd:tab name="kb" htmlClass="kb" >
        <jsp:attribute name="title"><spring:message code="tabs.knowledgebase"/></jsp:attribute>
        <jsp:attribute name="url"><c:url value="/kb/kbSearch.glml"/></jsp:attribute>
    </ehd:tab>
    <ehd:tab name="dashboards" htmlClass="dashboards" >
        <jsp:attribute name="title"><spring:message code="tabs.dashboards"/></jsp:attribute>
        <jsp:attribute name="url"><c:url value="/config/dashboardList.glml"/></jsp:attribute>
    </ehd:tab>
    <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN, ROLE_USER">
        <ehd:tab name="tickettemplates" htmlClass="tickettemplates" >
            <jsp:attribute name="title"><spring:message code="tabs.ticketTemplates"/></jsp:attribute>
            <jsp:attribute name="url"><c:url value="/config/templateMasterList.glml"/></jsp:attribute>
        </ehd:tab>
    </authz:authorize>
    </ehd:tabs>
</nav>
<div id="sub-nav">
    <div class="container_12">
        <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
            <div class="form">
                <input type="text" name="searchQuery" id="searchQuery" />
                <a class="button small" href="javascript:quickTicketSearch();" title="<spring:message code="header.ticketSearch"/>">
                    <b><spring:message code="header.ticketSearch"/></b>
                </a>
            </div>
        </authz:authorize>
    </div>
</div>
<div id="status-bar">
    <div class="container_12">
        <decorator:getProperty property="page.breadCrumb"/>
        <ul id="status-infos">
            <li class="spaced">
                <c:choose>
                <c:when test="${not empty sessionScope.User}">
                    <spring:message code="global.loggedInAs"/>
                    <a class="button" href="<c:url value="/config/profile/${sessionScope.User.id}/edit"/>" title="<spring:message code="user.edit"/>">
                        <c:out value="${sessionScope.User.firstName}"/>
                        <c:out value="${sessionScope.User.lastName}"/>
                    </a>
                    <%--<a class="button" href="javascript://" title="<spring:message code="user.edit"/>"--%>
                        <%--onclick="openWindow('<c:url value="/config/profile/${sessionScope.User.id}/edit"><c:param name="dialog" value="true"/></c:url>', 'userEdit', 800, 650);">--%>
                        <%--<c:out value="${sessionScope.User.firstName}"/>--%>
                        <%--<c:out value="${sessionScope.User.lastName}"/>--%>
                    <%--</a>--%>
                </c:when>
                <c:otherwise>
                    <strong><spring:message code="global.notLoggedIn"/></strong>
                </c:otherwise>
                </c:choose>
            </li>
            <li class="spaced">
                    <a class="button" target="_blank" href="
                <authz:authorize ifAnyGranted="ROLE_ADMINISTRATOR">
                    <c:url value="/help/eHD-Admin-Help.pdf"/>
                </authz:authorize>
                    <authz:authorize ifAnyGranted="ROLE_MANAGER, ROLE_TECHNICIAN">
                    <c:url value="/help/eHD-ManTech-Help.pdf"/>
                </authz:authorize>
                <authz:authorize ifNotGranted="ROLE_ADMINISTRATOR, ROLE_MANAGER, ROLE_TECHNICIAN">
                    <c:url value="/help/eHD-User-Help.pdf"/>
                </authz:authorize>
                    ">
                        <img src="<c:url value="/images/theme/icons/fugue/question-white.png"/>">
                        <strong><spring:message code="global.help"/></strong>
                    </a>
            </li>
            <c:if test="${showSettingsButton}">
                <li class="spaced">
                    <a class="button" href="<c:url value="/config"/>">
                        <spring:message code="header.managementConsole"/>
                    </a>
                </li>
            </c:if>
            <li class="spaced">
            <c:choose>
                <c:when test="${not empty sessionScope.User}">
                    <a class="button red" href="<c:url value="/logout.glml"/>">
                        <spring:message code="header.signout"/>
                    </a>
                </c:when>
                <c:otherwise>
                    <a class="button" href="<c:url value="/logout.glml"/>">
                        <spring:message code="header.login"/>
                    </a>
                </c:otherwise>
            </c:choose>
            </li>
        </ul>
    </div>
</div>
<div id="header-shadow"></div>
