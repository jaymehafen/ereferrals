<%@ page import="net.grouplink.ehelpdesk.domain.ticket.TicketFilter" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<script type="text/javascript">
    function columnCreator(item, hint) {
        var text = item["text"];
        if(text.length > 48) {
            text = text.substring(0, 48) + "...";
        }

        var node = dojo.create("li", {innerHTML: text, "data-field-name": item["name"], "data-field-text": item["text"]});
        var input = dojo.create("input", {name: "columnOrder", style: "display:none", value: item["name"]}, node, "last");
        var span = dojo.create("span", {"class": "close-bt-no-js"}, node);
        dojo.connect(span, "onclick", removeColumn);

        if(hint == "avatar") {
            dojo.style(node, "height", "15px");
            dojo.style(node, "margin", "0");
            node = dojo.create("ol", {"class": "mini-blocks-list", innerHTML: node.outerHTML, style: "width:180px"});
        }

        return {node: node, data: item};
    }

    function removeColumn() {
        var li = this.parentNode;
        var text = dojo.attr(li, "data-field-text");
        var fieldName = dojo.attr(li, "data-field-name");
        var fieldType = fieldName.substring(0, fieldName.indexOf("."));

        var optgroupClass = undefined;
        switch(fieldType) {
            case "ticketFilter":
                optgroupClass = "ticketFields";
                break;

            case "customField":
                optgroupClass = "customFields";
                break;

            case "survey":
                optgroupClass = "surveyQuestions";
                break;

            case "asset":
                optgroupClass = "assetFields";
                break;

            case "accountinginfo":
                optgroupClass = "assetFields";
                break;
        }

        dojo.fadeOut({
            node: li,
            onEnd: function() {
                insertColumnOptionSorted("field-in-filter-select", optgroupClass, {value: fieldName, innerHTML: text}, "innerHTML");
                $("#field-in-filter-select").trigger("liszt:updated");
                dojo.destroy(li);
            }
        }).play();
    }

    dojo.addOnLoad(function() {
        dojo.require("dojo.NodeList-manipulate");
        dojo.require("dojo.NodeList-traverse");

        hideLoader();

        //add a column to the column order dnd list
        dojo.connect(dojo.byId("addColumnToColOrder"), "onclick", function() {
            var selectedOption = dojo.query("#field-in-filter-select option:checked")[0];
            var text = dojo.attr(selectedOption, "innerHTML");
            var value = dojo.attr(selectedOption, "value");
            if(value != "") {
                addColToColumnOrder({name: value, text: text});
            }
        });

        function addColToColumnOrder(colItem) {
            window.columnOrder.insertNodes(false, [colItem]);
            dojo.destroy(dojo.query('#field-in-filter-select option[value="' + colItem.name + '"]')[0]);
            $("#field-in-filter-select").trigger("liszt:updated");
        }

        //set up the field select boxes for both the filter fields and the column order box
        $("#field-select").chosen({
            allow_single_deselect: true
        });
        $("#field-in-filter-select").chosen({
            allow_single_deselect: true
        });
        dojo.connect(dojo.byId("showFilterLi"), "onclick", function() {
            var filterSelectVal = dojo.attr("field-select", "value");

            if(filterSelectVal !== "") {
                //if the field isn't already in the column order box, add it
                var allActiveColumns = [];
                dojo.query("#columnOrder li").forEach(function(li) {
                    allActiveColumns.push(dojo.attr(li, "data-field-name"));
                });

                var selectedCol = dojo.query("#field-select option:checked")[0];
                var colItem = {name: filterSelectVal, text: dojo.trim(dojo.attr(selectedCol, "innerHTML"))};
                if($.inArray(colItem.name, allActiveColumns) == -1) {
                    addColToColumnOrder(colItem);
                }

                //if the column is a custom field, check if we've already created it from the template, otherwise just show it
                if(colItem.name.substring(0, 12) === "customField." && dojo.query('#filter-list li[data-filter-colname="' + colItem.name + '"]').length == 0) {
                    var rowIndex = $.inArray(selectedCol, $("#field-select optgroup.customFields option"));
                    if(filterMode == "assetFilter") {
                        rowIndex = $(selectedCol).attr("data-custom-field-id");
                    }
                    showCustomFieldLi(colItem.text, rowIndex);
                } else {
                    //show the filter li for the selected column
                    var filterDef = dojo.query('li[data-filter-colname="' + colItem.name + '"]').orphan()[0];
                    dojo.style(filterDef, "display", "block");
                    dojo.place(filterDef, "filter-list", "first");
                }

                //remove the filter name from filter-select
                dojo.destroy(dojo.query('#field-select option[value="' + colItem.name + '"]')[0]);
                $("#field-select").trigger("liszt:updated");
            }
        });

        $(".hideFilterLi").live("click", function() {
            var hideLink = $(this);
            var li = dojo.query(this).parents(".filter-definition")[0];
            var columnName = dojo.attr(li, "data-filter-colname");
            var columnText = dojo.attr(li, "data-filter-title");
            var fieldType = columnName.substring(0, columnName.indexOf("."));

            var filterBoxDiv = dojo.query(".filterValuesBox", li)[0];
            var filterBoxDivId = "";
            if(filterBoxDiv != undefined) {
                filterBoxDivId = dojo.attr(filterBoxDiv, "id");
            }

            clearDateTimeFields(columnName);

            var optgroupClass = "";
            var cfName = undefined;
            switch(fieldType) {
                case "ticketFilter":
                    optgroupClass = "ticketFields";
                    break;

                case "customField":
                    optgroupClass = "customFields";
                    cfName = columnText;
                    break;

                case "survey":
                    optgroupClass = "surveyQuestions";
                    break;
                case "asset":
                    optgroupClass = "assetFields";
                    break;
                case "accountinginfo":
                    optgroupClass = "assetFields";
                    break;
            }

            if(filterMode == "ticketFilter") {
                var callMetaData = {
                    callback: function(lineItemList, filterBoxDivId) {
                        $(li).fadeOut(function() {
                            //this will effectively clear out the filter values box when the li is hidden
                            buildLineItemTable(lineItemList, filterBoxDivId);
                            insertColumnOptionSorted("field-select", optgroupClass, {value: columnName, innerHTML: columnText}, "innerHTML");
                            $("#field-select").trigger("liszt:updated");
                        });
                    },
                    arg: filterBoxDivId,
                    errorHandler: outputError
                };
                TicketFilterService.clearProperty(columnName, cfName, callMetaData);
            } else {
                var cfId = filterBoxDivId.replace("customFieldBox_", "");
                dojo.xhrPost({
                    url: '<c:url value="/assets/filters/current/clearProperty"/>',
                    handleAs: "text",
                    content: { columnName: columnName, cfId: cfId },
                    handle: function (response, ioArgs) {
                        if(dojo.isString(response)) {
                            $(li).fadeOut(function() {
                                insertColumnOptionSorted("field-select", optgroupClass, {value: columnName, innerHTML: columnText}, "innerHTML");
                                $("#field-select").trigger("liszt:updated");
                                clearFilterLi(li);
                            });

                        } else {
                            alert("XHR error HTTP status code: " + ioArgs.xhr.status);
                        }
                    }
                });
            }
        });

        function clearFilterLi(li) {
            dojo.query(".filterValues tr", li).forEach(dojo.destroy);
        }

        //Set the form action, i.e. save, save as, reset, or apply
        dojo.query('a[data-form-action]').connect("onclick", function() {
            var action = dojo.attr(this, "data-form-action");
            dojo.attr("formAction", "value", action);

            var form = dojo.byId("ticketFilterForm");
            if(filterMode == "assetFilter") {
                form = dojo.byId("assetFilterForm");
            }

            switch(action) {
                case "saveas":
                    dijit.byId("saveAsDialog").show();
                    return false;
                    break;
                case "apply":
                    var url = "<c:url value="/tf/ticketFilterResults.glml"/>";
                    if(filterMode == "assetFilter") {
                        if (dojo.byId("reportCheckbox").checked) {
                            url = "<c:url value="/asset/report/dynamicAssetReport.glml"/>";
                        } else {
                            url = "<c:url value="/assets/filters/current/results"/>";
                        }

                    }
                    applyFilter(form, url);
                    return false;
                    break;
            }
            form.submit();
        });

        $(".addItem").live("click", function() {
            var opFldId = dojo.attr(dojo.query(this).parents("li.filter-definition")[0], "id").replace("fd.ticketFilter.", "op_");
            var divId = dojo.attr(dojo.query(this).parent("li").siblings("li.filterValuesBoxContainer").children("div.filterValuesBox")[0], "id");
            var fldId = dojo.attr(dojo.query(this).prev("div.filterValueInput")[0], "widgetId");
            var callback = postTicketFilterItem;
            if(filterMode == "assetFilter") {
                callback = postAssetFilterItem;
            }
            filterEditAddItem(opFldId, divId, fldId, callback);
        });

        $("a.deleteItem").live("click", function() {
            var type = dojo.attr(this, "data-type");
            var divId = dojo.attr(dojo.query(this).parents("div.filterValuesBox")[0], "id");
            var rowId = dojo.attr(this, "data-row-id");

            if(type == null) {
                type = divId;
            }
            var url = '<c:url value="/tf/deleteItem.glml"/>';
            if(filterMode == "assetFilter") {
                url = '<c:url value="/assets/filters/current/deleteItem"/>';
            }
            filterEditDelItem(type, divId, rowId, this, url);
        });

        $(".addCFItem").live("click", function() {
            var opFldId = dojo.attr(dojo.query(this).parents("li.filter-definition")[0], "id").replace("fd.ticketFilter.", "op_");
            var divId = dojo.attr(dojo.query(this).parent("li").siblings("li.filterValuesBoxContainer").children("div.filterValuesBox")[0], "id");
            var fldId = opFldId.replace("op_", "");
            var cfName = dojo.attr(dojo.query(this).parents(".filter-definition")[0], "data-filter-title");
            ticketFilterEditAddCFItem(opFldId, divId, fldId, cfName);
        });

        $(".deleteCFItem").live("click", function() {
            var filterDef = dojo.query(this).parents("li.filter-definition")[0];
            var rowId = dojo.attr(this, "data-row-id");
            var cfName = dojo.attr(filterDef, "data-filter-colname").replace("customField.", "");
            var divId = dojo.attr(dojo.query(this).parents("div.filterValuesBox")[0], "id");

            ticketFilterEditDelCFItem(divId, rowId, cfName);
        });

        $(".addSurveyItem").live("click", function() {
            var opFldId = dojo.attr(dojo.query(this).parents("li.filter-definition")[0], "id").replace("fd.ticketFilter.", "op_");
            var divId = dojo.attr(dojo.query(this).parent("li").siblings("li.filterValuesBoxContainer").children("div.filterValuesBox")[0], "id");
            var fldId = opFldId.replace("op_", "");
            var question = dojo.attr(dojo.query(this).parents(".filter-definition")[0], "data-filter-title");
            ticketFilterEditAddSurveyItem(opFldId, divId, fldId, question);
        });

        var fetcher = new dijit.Dialog({
            title: "<spring:message code="ticketFilter.fetcherTitle" javaScriptEscape="true"/>"
        }, "fetcher");

        dojo.connect(dojo.byId("showCategoryChooser"), "onclick", function() {
            catFetcherFetchedGroups = {};
            fetcher.set("href", "<c:url value="/tf/fetchCategories.glml"><c:param name="dialog" value="true"/></c:url>");
            fetcher.set("filterType", "categoryIds");
            fetcher.set("execute", applyChecks);
            fetcher.show();
        });

        dojo.connect(dojo.byId("showCatOptChooser"), "onclick", function() {
            catOptFetcherFetchedCats = {};
            fetcher.set("href", "<c:url value="/tf/fetchCategoryOptions.glml"><c:param name="dialog" value="true"/></c:url>");
            fetcher.set("filterType", "categoryOptionIds");
            fetcher.set("execute", applyChecks);
            fetcher.show();
        });

        var assStore = new dojox.data.QueryReadStore({
            url: "<c:url value="/stores/assets.json"/>"
        });

        var assGrid = new dojox.grid.DataGrid({
            store: assStore,
            structure: [
                {name:"<spring:message code="asset.assetNumber" javaScriptEscape="true"/>", field: "assetNumber", width: "auto"},
                {name:"<spring:message code="asset.name" javaScriptEscape="true"/>", field: "name", width: "auto"},
                {name:"<spring:message code="asset.assetlocation" javaScriptEscape="true"/>", field: "assetLocation", width: "auto"},
                {name:"<spring:message code="asset.location" javaScriptEscape="true"/>", field: "location", width: "auto"},
                {name:"<spring:message code="asset.group" javaScriptEscape="true"/>", field: "group", width: "auto"},
                {name:"<spring:message code="asset.category" javaScriptEscape="true"/>", field: "category", width: "auto"},
                {name:"<spring:message code="asset.categoryoption" javaScriptEscape="true"/>", field: "categoryOption", width: "auto"},
                {name:"<spring:message code="asset.owner" javaScriptEscape="true"/>", field: "owner", width: "auto"}
            ],
            editable: false,
            autoHeight: 20
        }, "assGrid");

        if(dojo.byId("showInternalAssetChooser")) {
            dojo.connect(dojo.byId("showInternalAssetChooser"), "onclick", function() {
                assGrid.startup();
                assGrid.setQuery({assetQuery: "*"});
                var internalAssChooser = dijit.byId("assChooser");
                internalAssChooser.show();
            });
        }

        if(dojo.byId("assSearch")) {
            dojo.connect(dojo.byId("assSearch"), "keyup", function() {
                var searchQuery = dojo.attr(this, "value");
                delay(function() {
                    assGrid.setQuery({label: searchQuery});
                }, 500);
            });
        }

        if(dojo.byId("intAssApplyBtn")) {
            dojo.connect(dojo.byId("intAssApplyBtn"), "onclick", function() {
                var selected = assGrid.selection.getSelected();
                if(selected.length) {
                    _.each(selected, function(selectedItem) {
                        var callback = postTicketFilterItem;
                        if(filterMode == "assetFilter") {
                            callback = postAssetFilterItem;
                        }
                        filterEditAddItem('', 'internalAssetIds', selectedItem.i.id, callback);
                    });
                    dijit.byId("assChooser").hide();
                    assGrid.selection.clear();
                } else {
                    alert("<spring:message code="ticketFilter.selectAtLeastOne" javaScriptEscape="true"/>");
                }
            });
        }

        if(dojo.byId("intAssCancelBtn")) {
            dojo.connect(dojo.byId("intAssCancelBtn"), "onclick", function() {
                dijit.byId("assChooser").hide();
            });
        }

        function zenDlgOnDownloadEnd() {
            var form = dojo.byId("zenAssetFetcherForm");
            dojo.connect(form, "submit", function(e) {
                dojo.stopEvent(e);
                dojo.xhrPost({
                    form: form,
                    content: {search: true},
                    load: function(response) {
                        zenDlg.set("content", response);

                        dojo.connect(dojo.byId("zenSelectAll"), "onclick", function() {
                            if(dojo.attr(this, "checked")) {
                                dojo.query('input[id^="zenCbx"]').attr("checked", true);
                            } else {
                                dojo.query('input[id^="zenCbx"]').attr("checked", false);
                            }
                        });

                        var addSelectedBtn = dojo.byId("zenAddSelectedButton");
                        if(addSelectedBtn != undefined) {
                            dojo.connect(addSelectedBtn, "onclick", function() {
                                var checkedBoxes = dojo.query('input[id^="zenCbx"]:checked');
                                if(checkedBoxes.length) {
                                    checkedBoxes.forEach(function(item) {
                                        var callback = postTicketFilterItem;
                                        if(filterMode == "assetFilter") {
                                            callback = postAssetFilterItem;
                                        }
                                        filterEditAddItem('', 'zenAssetIds', dojo.attr(item, "value"), callback);
                                    });
                                    zenDlg.hide();
                                    zenDlg.set("href", "<c:url value="/tf/fetchZenAssets.glml"><c:param name="dialog" value="true"/></c:url>");
                                } else {
                                    alert("<spring:message code="ticketFilter.selectAtLeastOne" javaScriptEscape="true"/>");
                                }
                            });
                        }

                        zenDlgOnDownloadEnd();
                    }
                });
            });
            dojo.connect(dojo.byId("zenCancel"), "onclick", function() {
                zenDlg.hide();
                zenDlg.set("href", "<c:url value="/tf/fetchZenAssets.glml"><c:param name="dialog" value="true"/></c:url>");
            });
        }

        var zenDlg = new dijit.Dialog({
            title: "<spring:message code="ticketFilter.fetcherTitle" javaScriptEscape="true"/>",
            href: "<c:url value="/tf/fetchZenAssets.glml"><c:param name="dialog" value="true"/></c:url>",
            onDownloadEnd: zenDlgOnDownloadEnd
        });

        if(dojo.byId("showZenAssetChooser")) {
            dojo.connect(dojo.byId("showZenAssetChooser"), "onclick", function() {
                zenDlg.show();
            });
        }

        $('input[id^="sall_"]').live("click", function() {
            var all_cbx = this;
            var cbx_name = dojo.attr(all_cbx, "id").replace("sall_", "cbx_");
            dojo.query('[name="' + cbx_name + '"]').forEach(function(checkbox) {
                dijit.byId(dojo.attr(checkbox, "id")).set("checked", dojo.attr(all_cbx, "checked"));
            });
        });

        if(dojo.byId("createdDateOperator")) {
            showDate(document.getElementById('createdDateOperator'), 'cdate');
        }
        if(dojo.byId("modifiedDateOperator")) {
            showDate(document.getElementById('modifiedDateOperator'), 'mdate');
        }
        if(dojo.byId("estimatedDateOperator")) {
            showDate(document.getElementById('estimatedDateOperator'), 'estdate');
        }
        if(dojo.byId("workTimeOperator")) {
            showWorktime(document.getElementById('workTimeOperator'));
        }
    });

    function clearDateTimeFields(columnName){
        switch(columnName) {
            case "ticketFilter.created":
                dojo.attr(dojo.byId("createdDateOperator"), "value", "0");
                break;
            case "ticketFilter.modified":
                dojo.attr(dojo.byId("modifiedDateOperator"), "value", "0");
                break;
            case "ticketFilter.estimated":
                dojo.attr(dojo.byId("estimatedDateOperator"), "value", "0");
                break;
            case "ticketFilter.worktime":
                dojo.attr(dojo.byId("workTimeOperator"), "selectedIndex", 0);
                dijit.byId("workTimeHours").setValue("");
                break;
        }
    }

    function applyFilter(form, url) {
        dojo.xhrPost({
            form: form,
            load: function() {
                openWindow(url, 'filterResults', 900, 700);
            }
        });
    }

    function insertColumnOptionSorted(selectId, optgroupClass, itemToInsert, attribName) {
        var selectQuery = "#" + selectId + " ." + optgroupClass;
        var arrayOfOption = dojo.query(selectQuery  + " option");
        arrayOfOption.push(dojo.create("option", itemToInsert));
        for(var j = 1; j < arrayOfOption.length; j++) {
            var nextOption = arrayOfOption[j];
            var i = j - 1;

            while(i >= 0 && dojo.trim(dojo.attr(arrayOfOption[i], attribName)) > dojo.trim(dojo.attr(nextOption, attribName))) {
                arrayOfOption[i+1] = arrayOfOption[i];
                i = i - 1;
            }

            arrayOfOption[i+1] = nextOption;
        }
        $(selectQuery).html($('<div>').append($(arrayOfOption)).html());
    }

    function showCustomFieldLi(cfName, rowIndex) {
        var template = _.template($("#filter-def-tmpl").html());
        var html = dojo.trim(template({
            customFieldName: cfName,
            rowId: rowIndex
        }));
        dojo.place(html, "filter-list", "first");
        dojo.parser.parse("filter-list");
    }

    function filterEditAddItem(opFldId, divId, fldId, callback) {
        var fldVal, opFldVal;

        if (divId == 'zenAssetIds' || divId == 'internalAssetIds') {
            fldVal = fldId;
        } else {
            var fld = dijit.byId(fldId);
            var opFld = dijit.byId(opFldId);

            fldVal = dojo.attr(fld, "value");
            if (opFld) opFldVal = dojo.attr(opFld, "value");

            dojo.attr(fld, "value", "");
        }

        // convert the fldVal to a string
        fldVal += '';

        if (fldVal.length == 0) return;

        callback(fldVal, divId, opFldVal);

        if(fld) {
            fld.attr("value", "");
        }
    }

    function postTicketFilterItem(fldVal, divId, opFldVal) {
        dojo.xhrPost({
            url: '<c:url value="/tf/addItem.glml"/>',
            handleAs: "text",
            content: { value: fldVal, type: divId, opValue: opFldVal},
            handle: function (response, ioArgs) {
                if(dojo.isString(response)) {
                    dojo.byId(divId).innerHTML = response;
                } else {
                    alert("XHR error HTTP status code: " + ioArgs.xhr.status);
                }
            }
        });
    }

    function postAssetFilterItem(fldVal, divId) {
        dojo.xhrPost({
            url: '<c:url value="/assets/filters/current/addItem"/>',
            handleAs: "text",
            content: { value: fldVal, type: divId },
            handle: function (response, ioArgs) {
                if(dojo.isString(response)) {
                    dojo.byId(divId).innerHTML = response;
                } else {
                    alert("XHR error HTTP status code: " + ioArgs.xhr.status);
                }
            }
        });
    }

    function filterEditDelItem(type, divId, row, link, url) {
        dojo.xhrPost({
            url: url,
            handlesAs: "text",
            content: { row: row, type: type },
            load: function (response) {
                $(link).parents("tr").remove();
                dojo.byId(divId).innerHTML = response;
            },
            error: function(response, ioArgs) {
                alert("XHR error HTTP status code: " + ioArgs.xhr.status);
            }
        });
    }

    function ticketFilterEditAddCFItem(opFldId, divId, fldId, cfName) {
        var fldVal, opFldVal;

        var fld = dijit.byId(fldId);
        var opFld = dijit.byId(opFldId);

        fldVal = fld.getValue();
        if (opFld) opFldVal = opFld.getValue();

        // reset the value of the search field to blank
        fld.setValue('');
        // don't bother with the ajax call if nothing was entered
        if (fldVal.length == 0) return;

        var callMetaData = {
            callback: buildLineItemTable,
            arg: divId,
            errorHandler: outputError
        };

        TicketFilterService.addCFLineItem(cfName, divId, opFldVal, fldVal, callMetaData);
    }

    function ticketFilterEditDelCFItem(divId, row, cfname) {
        var callMetaData = {
            callback: buildLineItemTable,
            arg: divId,
            errorHandler: outputError
        };

        TicketFilterService.deleteCFLineItem(divId, row, cfname, callMetaData);
    }

    function ticketFilterEditAddSurveyItem(opFldId, divId, fldId, question) {
        var fldVal, opFldVal;

        var fld = dijit.byId(fldId);
        var opFld = dijit.byId(opFldId);

        fldVal = fld.getValue();
        if (opFld) opFldVal = opFld.getValue();

        fld.setValue('');
        if (fldVal.length == 0) return;

        var callMetaData = {
            callback: buildLineItemTable,
            arg: divId,
            errorHandler: outputError
        };

        TicketFilterService.addSurveyLineItem(question, divId, opFldVal, fldVal, callMetaData);
    }

    // Callback function for selection dialogs
    function applyChecks() {
        var slctdIds = new Array();
        var slctdIdsOps = new Array();

        dojo.query('input[id^="cbx_"]').forEach(function(checkbox) {
            if(dojo.attr(checkbox, "checked")) {
                slctdIds.push(dijit.byId(dojo.attr(checkbox, "id")).getValue());
                slctdIdsOps.push(dijit.byId(dojo.attr(checkbox, "id") + "_dd").getValue());
            }
        });

        var filterType = this.filterType;

        if(filterMode == "ticketFilter") {
            var callMetaData = {
                callback: buildLineItemTable,
                arg: filterType,
                errorHandler: outputError
            };

            TicketFilterService.addLineItems(filterType, slctdIdsOps, slctdIds, callMetaData);
        } else {
            postAssetFilterItem(slctdIds, filterType);
        }
    }

    function buildLineItemTable(lineItemList, divId) {
        if (!divId) return;

        var template = _.template($("#filter-tr-tmpl").html());
        var trHtml = "";

        for (var i in lineItemList) {
            var tfLineItem = lineItemList[i];

            var deleteClass = "deleteItem";
            if(tfLineItem.deleteMethodName == "ticketFilterEditDelCFItem") {
                deleteClass = "deleteCFItem";
            }

            trHtml += template({
                operator: tfLineItem.operatorName,
                item: tfLineItem.displayName,
                divId: divId,
                rowId: i,
                deleteClass: deleteClass
            });
        }

        dojo.create("table", {"class": "filterValues", innerHTML: dojo.trim(trHtml)}, divId, "only");
    }

    function outputError(e, ex) {
        console.debug("Error: " + ex);
    }

    <%-- CategoryFetcher --%>
    var catFetcherFetchedGroups;
    var catOptFetcherFetchedCats;

    function catFetcherShowTitlePane(groupId, rowIndex) {
        console.debug("called showTitlePane");
        // check if group categories div has been created
        if (!dojo.byId("catFetcher_group_" + groupId) && !catFetcherFetchedGroups[groupId]) {
            // create the row with the group's categories
            var callMetadata = {
                callback: updateTitlePane,
                arg: {containerId: groupId, rowIndex: rowIndex, containerName: "catFetcherGroup"}
            };
            TicketFilterService.getCategoriesByGroupId(groupId, callMetadata);

            catFetcherFetchedGroups[groupId] = true;
        }
    }

    <%-- Cat Opt Fetcher --%>
    function catOptFetcherShowTitlePane(groupId, catId, rowIndex) {
        console.debug("called showTitlePane");
        // check if group categories div has been created
        if (!dojo.byId("catOptFetcher_cat_" + catId) && !catOptFetcherFetchedCats[catId]) {
            // create the row with the group's categories
            var callMetadata = {
                callback: updateTitlePane,
                arg: {containerId: catId, rowIndex: rowIndex, containerName: "catOptFetcherDiv_"}
            };
            TicketFilterService.getCatOptsByCatId(catId, callMetadata);

            catOptFetcherFetchedCats[catId] = true;
        }
    }

    function updateTitlePane(objects, args) {
        var rowIndex = args.rowIndex;
        var container = dojo.byId(args.containerName + args.containerId);
        var template = _.template($("#fetcher-li-tmpl").html());
        var liHtml = "";

        for (var i=0; i<objects.length; i++) {
            var obj = objects[i];

            liHtml += template({
                rowIndex: rowIndex,
                objId: obj.id,
                objName: obj.name
            });
        }

        var ul = dojo.create("ul", {innerHTML: dojo.trim(liHtml)}, container, "last");
        dojo.parser.parse(ul);
    }

    function showDate(slct, prefix) {
        var value = slct.value;

        // on before and after show 1 date
        switch (value) {
            case '4':
            case '5':
            case '6':
                dojo.style(dojo.byId(prefix + "1"), "display", "block");
                dojo.style(dojo.byId(prefix + "2"), "display", "none");
                dojo.style(dojo.byId(prefix + "LastXDiv"), "display", "none");
                break;
            case '7':
                dojo.style(dojo.byId(prefix + "1"), "display", "block");
                dojo.style(dojo.byId(prefix + "2"), "display", "block");
                dojo.style(dojo.byId(prefix + "LastXDiv"), "display", "none");
                break;
            case '8':
            case '17':
            case '18':
            case '19':
            case '20':
                dojo.style(dojo.byId(prefix + "1"), "display", "none");
                dojo.style(dojo.byId(prefix + "2"), "display", "none");
                dojo.style(dojo.byId(prefix + "LastXDiv"), "display", "block");
                break;
            default:
                dojo.style(dojo.byId(prefix + "1"), "display", "none");
                dojo.style(dojo.byId(prefix + "2"), "display", "none");
                dojo.style(dojo.byId(prefix + "LastXDiv"), "display", "none");
                break;
        }
    }

    function showWorktime(slct) {
        switch (slct.value) {
            case 'more':
            case 'less':
                dojo.style(dojo.byId("workTime1"), "display", "block");
                dojo.style(dojo.byId("workTime2"), "display", "block");
                break;
            default:
                dojo.style(dojo.byId("workTime1"), "display", "none");
                dojo.style(dojo.byId("workTime2"), "display", "none");
                break;
        }
    }
</script>
