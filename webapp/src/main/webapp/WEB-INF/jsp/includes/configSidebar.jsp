<%--@elvariable id="configMenuOptions" type="net.grouplink.ehelpdesk.web.domain.ConfigMenuOptions"--%>
<section class="grid_2">
    <div dojoType="dijit.TitlePane" title="<spring:message code="config.menu.gl"/>" id="globalConfig">
        <ul class="simple-list">
            <ehd:authorizeGlobal permission="global.userManagement">
                <li>
                    <a href="<c:url value="/config/users"/>">
                        <spring:message code="userManagement.accounts"/>
                    </a>
                </li>
            </ehd:authorizeGlobal>
            <c:if test="${configMenuOptions.scheduler}">
                <li>
                    <a href="<c:url value="/config/groups/schedules"/>">
                        <spring:message code="scheduler.title"/>
                    </a>
                </li>
            </c:if>
            <c:if test="${configMenuOptions.kbManage}">
                <li>
                    <a href="<c:url value="/kbManagement/kbManagement.glml"/>">
                        <spring:message code="kb.knowledgeBaseArticle"/>
                    </a>
                </li>
            </c:if>
            <ehd:authorizeGlobal permission="global.surveyManagement">
                <li>
                    <a href="<c:url value='/config/surveys/management/surveyManage.glml'/>">
                        <spring:message code="surveys.surveys"/>
                    </a>
                </li>
            </ehd:authorizeGlobal>
        </ul>
    </div>

    <ehd:authorizeGlobal permission="global.systemConfig">
    <div dojoType="dijit.TitlePane" title="<spring:message code="config.menu.sys"/>" id="systemConfig">
        <ul class="simple-list">
            <li>
                <a href="<c:url value="/config/general/generalConfig.glml"/>">
                    <spring:message code="generalConfig.title"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/config/ldap/ldapConfig.glml"/>">
                    <spring:message code="ldapconfig.title"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/config/mail/mailConfig.glml"/>">
                    <spring:message code="mailconfig.title"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/config/collaboration/collaborationConfig.glml"/>">
                    <spring:message code="collaborationConfig.title"/>
                </a>
            </li>
            <%--<li>--%>
                <%--<a href="<c:url value="/config/asset/assetTrackerZen10Config.glml"/>">--%>
                    <%--<spring:message code="assetTracker.zenworksConfig"/>--%>
                <%--</a>--%>
            <%--</li>--%>
            <li>
                <a href="<c:url value="/config/db/dbConfig.glml" />">
                    <spring:message code="databaseConfig.title"/>
                </a>
            </li>
            <li>
                <a href="<c:url value="/config/licensing/licensing.glml"/>">
                    <spring:message code="licensing.title"/>
                </a>
            </li>
        </ul>
    </div>
    </ehd:authorizeGlobal>
    
    <div dojoType="dijit.TitlePane" title="<spring:message code="config.menu.tc"/>" id="ticketConfig">
        <ul class="simple-list">
            <ehd:authorizeGlobal permission="global.systemConfig">
                <li>
                    <a href="<c:url value="/config/locations"/>">
                        <spring:message code="locationSetup.title"/>
                    </a>
                </li>
            </ehd:authorizeGlobal>
            <c:if test="${configMenuOptions.groups}">
                <li>
                    <a href="<c:url value="/config/groups"/>">
                        <spring:message code="group.title"/>
                    </a>
                </li>
            </c:if>
            <c:if test="${configMenuOptions.assignments}">
                <li>
                    <a href="<c:url value="/config/assignments"/>">
                        <spring:message code="ticketSettings.assignments"/>
                    </a>
                </li>
            </c:if>
            <c:if test="${configMenuOptions.customFields}">
                <li>
                    <a href="<c:url value="/config/customFields"/>">
                        <spring:message code="customField.setupTitle"/>
                    </a>
                </li>
            </c:if>
            <ehd:authorizeGlobal permission="global.systemConfig">
                <li>
                    <a href="<c:url value="/config/statuses"/>">
                        <spring:message code="statusSetup.title"/>
                    </a>
                </li>
                <li>
                    <a href="<c:url value="/config/priorities"/>">
                        <spring:message code="prioritySetup.title"/>
                    </a>
                </li>
            </ehd:authorizeGlobal>
        </ul>
    </div>

    <ehd:authorizeGlobal permission="global.systemConfig">
        <div dojotype="dijit.TitlePane" title="<spring:message code="acl.permissions"/>" id="permissionConfig">
            <ul class="simple-list">

                <li>
                    <a href="<c:url value="/config/acl/globalPermissions"/>">
                        <spring:message code="acl.globalPermissionsTitle"/>
                    </a>
                </li>

                <li>
                    <a href="<c:url value="/config/acl/permissionSchemes"/>">
                        <spring:message code="acl.permissionModels"/>
                    </a>
                </li>
            </ul>
        </div>
    </ehd:authorizeGlobal>
</section>
