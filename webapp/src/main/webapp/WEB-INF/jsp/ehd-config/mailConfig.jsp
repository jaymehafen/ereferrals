<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="e2ts" type="java.util.List"--%>
<%--@elvariable id="mailConfig" type="net.grouplink.ehelpdesk.common.utils.MailConfig"--%>
<head>
    <meta name="configSidebar" content="true">
    <title>
        <spring:message code="mailconfig.title"/>
    </title>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            //adjust port input value based on security radio button selection
            dojo.query('#outgoingSecurity input[name="security"]').connect("onclick", function() {
                var ports = {"none": 25, "tlsIfAvailable": 587, "tls": 587, "ssl": 465};
                dojo.attr(dojo.byId("port"), "value", ports[dojo.attr(this, "value")]);
            });

            function dlgDownloadEnd() {
                dojo.query("#emailToTicketConfig").connect("submit", function(e) {
                    dojo.stopEvent(e);
                    var form = dojo.byId("emailToTicketConfig");
                    dojo.xhrPost({
                        form: form,
                        load: function(response) {
                            //create a tmp div so we can use dojo.query on the response
                            //to check for errors and either hide the form or leave it up
                            var tempDiv = document.createElement("div");
                            tempDiv.innerHTML = response;
                            var errors = dojo.query("[id=e2tErrors]", tempDiv);
                            if(errors.length) {
                                e2tDlg.set("content", response);
                                dlgDownloadEnd();
                            } else {
                                e2tDlg.hide();
                                location.reload(true);
                            }
                        }
                    });
                });
            }

            //setup email to ticket dialog
            var e2tDlg = new dijit.Dialog({
                title: "<spring:message code="mailconfig.e2tTitle"/>",
                href: "<c:url value="/config/emailToTicket/edit.glml"/>",
                style: "width:800px",
                onDownloadEnd: dlgDownloadEnd
            });

            dojo.connect(dojo.byId("createNewE2tBtn"), "onclick", function(e) {
                e2tDlg.set("href", "<c:url value="/config/emailToTicket/edit.glml"/>");
                e2tDlg.show();
                e.preventDefault();
            });

            dojo.query(".editE2t").connect("onclick", function(e) {
                e.preventDefault();
                var href = dojo.attr(this, "href");
                e2tDlg.set("href", href);
                e2tDlg.show();
            });

            function updatePort() {
                var ports = {
                    "1": { //imap
                        "ssl": 993,
                        "none": 143,
                        "tlsIfAvailable": 143,
                        "tls": 143
                    },
                    "2": { //pop
                        "ssl": 995,
                        "none": 110,
                        "tlsIfAvailable": 110,
                        "tls": 110
                    }
                };
                var sec = dojo.attr(dojo.query('#e2tSecurity input[name="security"]:checked')[0], "value");
                var serverType = dojo.attr(dojo.query('input[name="serverType"]:checked')[0], "value");
                dojo.attr(dojo.byId("e2tPort"), "value", ports[serverType][sec]);
            }

            function toggleImapFolder(show) {
                var display = "none";
                if(show) display = "block";
                dojo.style(dojo.byId("imapFolderDiv"), "display", display);
            }

            $('input[name="serverType"]').live("click", function() {
                updatePort();
                toggleImapFolder($('input[name="serverType"]:checked').val() == "1"); //1 is for imap
            });

            $('#e2tSecurity input[name="security"]').live("click", function() {
                updatePort();
            });

            $(".cancelBtn").live("click", function() {
                e2tDlg.hide();
            });

            $("#testButton").live("click", function() {
                dojo.query("#buttons button").attr("disabled", true);
                dojo.xhrGet({
                    url: "<c:url value="/config/emailToTicket/edit.glml"><c:param name="test"/></c:url>",
                    content: dojo.formToObject("emailToTicketConfig"),
                    handleAs: "text",
                    load: function(response) {
                        e2tDlg.set("content", response);
                        dlgDownloadEnd();
                    }
                });
            });

            dojo.query(".startMailbox").connect("click", function(e) {
                dojo.stopEvent(e);
                var e2tid = getMailboxId(this);
                inboxMonitor("start", e2tid);
            });

            dojo.query(".stopMailbox").connect("click", function(e) {
                dojo.stopEvent(e);
                var e2tid = getMailboxId(this);
                inboxMonitor("stop", e2tid);
            });

            function getMailboxId(element) {
                return $(element).parents("tr").attr("id").replace("mailbox_", "");
            }

            function inboxMonitor(action, e2tid) {
                dojo.xhrPost({
                    url: '<c:url value="/config/emailToTicket/toggleEnabled.glml"/>',
                    content: { action: action, e2tid: e2tid },
                    load: function() {
                        location.reload(true);
                    }
                });
            }

            dojo.query(".deleteMailbox").connect("click", function(e) {
                dojo.stopEvent(e);
                var e2tid = getMailboxId(this);
                var node = dojo.byId("mailbox_" + e2tid);
                dojo.xhrPost({
                    url: "<c:url value="/config/emailToTicket/delete.glml"/>",
                    content: { e2tid: e2tid },
                    load: function() {
                        dojo.fadeOut({
                            node: node,
                            onEnd: function() {
                                var table = $(node).parents("table")[0];
                                if(dojo.query('tr[id^="mailbox_"]', table).length == 1) {
                                    dojo.fadeOut({
                                        node: table,
                                        onEnd: function() {
                                            dojo.place('<p><spring:message code="mailconfig.noInboxes"/></p>', table, "replace");
                                        }
                                    }).play();
                                } else {
                                    dojo.destroy(node);
                                }
                            }
                        }).play();
                    }
                });
            });
        });
    </script>
</head>
<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="mailconfig.outgoing"/></h1>
        <form:form modelAttribute="mailConfig" cssClass="form">
            <spring:hasBindErrors name="mailConfig">
                <p id="e2tErrors" class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
            <div class="columns">
                <div class="colx3-left">
                    <p>
                        <form:label path="emailDisplayName"><spring:message code="mailconfig.emailDisplayName"/></form:label>
                        <form:input path="emailDisplayName" cssClass="full-width"/>
                    </p>
                    <p>
                        <form:label path="emailFromAddress"><spring:message code="mailconfig.emailFromAddress"/></form:label>
                        <form:input path="emailFromAddress">
                            <jsp:attribute name="cssClass">
                                full-width <spring:bind path="mailConfig.emailFromAddress"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                            </jsp:attribute>
                        </form:input>
                    </p>
                    <div class="with-margin">
                        <span class="label"><spring:message code="mailconfig.ticketChanges"/></span>
                        <ul class="checkable-list">
                            <li>
                                <form:checkbox path="notifyTechnician"/>
                                <form:label path="notifyTechnician" cssClass="inline"><spring:message code="mailconfig.notifyTechnician"/></form:label>
                            </li>
                            <li>
                                <form:checkbox path="notifyUser"/>
                                <form:label path="notifyUser" cssClass="inline"><spring:message code="mailconfig.notifyUser"/></form:label>
                            </li>
                        </ul>
                    </div>
                    <p>
                        <form:checkbox path="sendLinkOnNotification"/>
                        <form:label path="sendLinkOnNotification" cssClass="inline"><spring:message code="mailconfig.sendLinkOnNotification"/></form:label>
                    </p>
                </div>
                <div class="colx3-center">
                    <p>
                        <form:label path="host"><spring:message code="mailconfig.host"/></form:label>
                        <form:input path="host">
                            <jsp:attribute name="cssClass">
                                full-width <spring:bind path="mailConfig.host"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                            </jsp:attribute>
                        </form:input>
                    </p>
                    <div id="outgoingSecurity" class="with-margin">
                        <form:label path="security"><spring:message code="mailconfig.security"/></form:label>
                        <ul class="checkable-list">
                            <li>
                                <form:radiobutton path="security" value="none"/>
                                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.none"/></form:label>
                            </li>
                            <li>
                                <form:radiobutton path="security" value="tlsIfAvailable"/>
                                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.tlsIfAvailable"/></form:label>
                            </li>
                            <li>
                                <form:radiobutton path="security" value="tls"/>
                                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.tls"/></form:label>
                            </li>
                            <li>
                                <form:radiobutton path="security" value="ssl"/>
                                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.ssl"/></form:label>
                            </li>
                        </ul>
                    </div>
                    <p>
                        <form:label path="port"><spring:message code="mailconfig.port"/></form:label>
                        <form:input path="port">
                            <jsp:attribute name="cssClass">
                                full-width<spring:bind path="mailConfig.port"><c:if test="${not empty status.errorMessage}"> error</c:if></spring:bind>
                            </jsp:attribute>
                        </form:input>
                    </p>
                    <p>
                        <form:label path="userName"><spring:message code="mailconfig.userName"/></form:label>
                        <form:input path="userName" cssClass="full-width"/>
                    </p>
                    <p>
                        <form:label path="password"><spring:message code="mailconfig.password"/></form:label>
                        <form:password path="password" showPassword="true" cssClass="full-width"/>
                    </p>
                </div>
                <div class="colx3-right">
                    <p>
                        <form:label path="encoding"><spring:message code="mailconfig.encoding"/></form:label>
                        <form:input path="encoding" cssClass="full-width"/>
                    </p>
                    <p>
                        <form:label path="email"><spring:message code="mailconfig.testEmail"/></form:label>
                        <form:input path="email" cssClass="full-width">
                            <jsp:attribute name="disabled">
                                <c:if test="${not applicationScope.mailConfigured}">true</c:if>
                            </jsp:attribute>
                        </form:input>
                        <button type="submit" name="sendTestEmail"
                        <c:if test="${not applicationScope.mailConfigured}">
                            disabled="disabled" title="<spring:message code="mailconfig.sendtest.saveconfig"/>"
                        </c:if>>
                            <spring:message code="mailconfig.testEmail" />
                        </button>
                    </p>
                </div>
            </div>
            <p class="grey-bg no-margin">
                <button type="submit" name="submit"><spring:message code="mailconfig.button.submit" /></button>
                <button type="submit" name="reset"><spring:message code="mailconfig.button.reset" /></button>
            </p>
        </form:form>
    </div>

    <br>
    <br>

    <div class="block-content">
        <h1><spring:message code="mailconfig.incoming"/></h1>
        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="createNewE2tBtn" href="javascript://">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="<spring:message code="mailconfig.createNew"/>">
                        <spring:message code="mailconfig.createNew"/>
                    </a>
                </li>
            </ul>
        </div>
        <c:choose>
            <c:when test="${fn:length(e2ts) > 0}">
                <div class="no-margin">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col"><spring:message code="mailconfig.mailbox"/></th>
                                <th scope="col"><spring:message code="mailconfig.host"/></th>
                                <th scope="col"><spring:message code="mailconfig.location"/></th>
                                <th scope="col"><spring:message code="mailconfig.group"/></th>
                                <th scope="col"><spring:message code="mailconfig.priority"/></th>
                                <th scope="col"><spring:message code="mailconfig.actions"/></th>
                                <th scope="col"><spring:message code="mailconfig.status"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="e2t" items="${e2ts}">
                            <tr id="mailbox_${e2t.id}">
                                <td><c:out value="${e2t.replyDisplayName}"/> &lt;<c:out value="${e2t.replyEmailAddress}"/>&gt;</td>
                                <td><c:out value="${e2t.serverName}"/>:<c:out value="${e2t.port}"/></td>
                                <td><c:out value="${e2t.location.name}"/></td>
                                <td><c:out value="${e2t.group.name}"/></td>
                                <td><c:out value="${e2t.ticketPriority.name}"/></td>
                                <td class="table-actions">
                                    <a href="<c:url value="/config/emailToTicket/edit.glml" ><c:param name="e2tId" value="${e2t.id}"/></c:url>" class="editE2t"
                                            title="<spring:message code="global.edit"/>">
                                        <img alt="" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>">
                                    </a>
                                    &nbsp;
                                    <a href="javascript://" class="deleteMailbox"
                                            title="<spring:message code="global.delete"/>">
                                        <img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                                    </a>
                                </td>
                                <td class="table-actions">
                                    <c:choose>
                                        <c:when test="${e2t.enabled}">
                                            <a class="stopMailbox" href="javascript://">
                                                <img src="<c:url value="/images/theme/icons/fugue/control-stop-square.png"/>" alt=""
                                                     title="<spring:message code="mailconfig.disable"/>">
                                            </a>
                                            &nbsp;
                                            <img src="<c:url value="/images/circle-ball-dark-antialiased.gif"/>" alt="">
                                        </c:when>
                                        <c:otherwise>
                                            <a class="startMailbox" href="javascript://">
                                                <img src="<c:url value="/images/theme/icons/fugue/control.png"/>" alt=""
                                                     title="<spring:message code="mailconfig.enable"/>">
                                            </a>
                                            &nbsp;
                                            <img src="<c:url value="/images/circle-ball-dark-noanim.gif"/>" alt="">
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:when>
            <c:otherwise>
                <p><spring:message code="mailconfig.noInboxes"/></p>
            </c:otherwise>
        </c:choose>
    </div>
</section>
</body>
