<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title><spring:message code="prioritySetup.title"/></title>
    <meta name="configSidebar" content="true"/>

    <tags:jsTemplate id="priority-tmpl">
        <tr class="dojoDndItem">
            <td style="width:15px">
                <img class="dojoDndHandle" src="<c:url value="/images/theme/icons/fugue/arrow-move.png"/>">
                <input type="hidden" name="priorities[{{ index }}].order" value="{{ order }}">
            </td>
            <td><input type="text" name="priorities[{{ index }}].name" dojoType="dijit.form.TextBox" maxlength="50" style="width:100%"></td>
            <td style="width:20px">
                <select name="priorities[{{ index }}].icon" dojoType="dijit.form.ComboBox" style="width:70px">
                    <option value="text">text</option>
                    <option value="blue">blue</option>
                    <option value="gray">gray</option>
                    <option value="green">green</option>
                    <option value="orange">orange</option>
                    <option value="purple">purple</option>
                    <option value="red">red</option>
                    <option value="yellow">yellow</option>
                    <option value="blank">blank</option>
                </select>
            </td>
            <td style="width:15px">
                <a class="delPriority" href="javascript:void(0)">
                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                </a>
            </td>
        </tr>
    </tags:jsTemplate>

    <script type="text/javascript">
        function getNextPriorityInfo() {
            var index;
            var order;

            var lastPriority = dojo.query("#priorityTable tr:last-child")[0];
            if (lastPriority) {
                var orderInput = dojo.query('input[name$=".order"]', lastPriority)[0];
                order = parseInt(dojo.attr(orderInput, "value")) + 1;
                var rowName = dojo.attr(orderInput, "name");
                var lastIndexRegexMatch = /\[(\d+)\]/.exec(rowName);
                var lastIndex = lastIndexRegexMatch[1];
                index = parseInt(lastIndex) + 1;
            } else {
                index = 0;
                order = 0;
            }
            return {"index": index, "order": order};
        }

        function listReordered() {
            dojo.query("#priorityTable tr").forEach(function(item, index) {
                var orderInput = dojo.query('input[name$=".order"]', item)[0];
                dojo.attr(orderInput, "value", index);
            });

            dojo.addClass("addPriority", "disabled")
        }

        dojo.addOnLoad(function() {
            dojo.query("#addPriority").connect("onclick", function() {
                if (dojo.hasClass("addPriority", "disabled")) return;
                var template = _.template(dojo.attr(dojo.byId("priority-tmpl"), "innerHTML"));
                var refnode = dojo.query("#priorityTable tbody")[0];
                dojo.place(dojo.trim(template(getNextPriorityInfo())), refnode);
                dojo.parser.parse(refnode);
            });

            $(".delPriority").live("click", function(e) {
                e.stopPropagation();
                var elmToRemove = this.parentNode.parentNode;
                dojo.fadeOut({
                    node: elmToRemove,
                    onEnd: function() {
                        var thisId = dojo.attr(elmToRemove, "id");
                        if(thisId != null && thisId != "priority_") {
                            var priorityId = thisId.replace("priority_", "");
                            var idsToDeleteInput = dojo.query('input[name="idsToDelete"]')[0];
                            var ids = dojo.attr(idsToDeleteInput, "value");
                            dojo.attr(idsToDeleteInput, "value", ids + (ids.length ? "," + priorityId : priorityId));
                            dojo.style(elmToRemove, "display", "none");
                        } else {
                            dojo.destroy(elmToRemove);
                        }
                    }
                }).play();
            });

            dojo.connect(dojo.byId("savePriorities"), "onclick", function() {
                dojo.byId("priorityForm").submit();
            });
        });
    </script>
</head>
<body>
<section class="grid_5">
    <form:form modelAttribute="priorityForm">
        <input type="hidden" name="idsToDelete">
        <div class="block-content">
            <h1><spring:message code="prioritySetup.title"/></h1>

            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a id="addPriority" href="javascript:void(0)">
                            <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>">
                            <spring:message code="prioritySetup.add"/>
                        </a>
                    </li>
                    <li>
                        <a id="savePriorities" href="javascript:void(0)">
                            <img src="<c:url value="/images/save.gif"/>">
                            <spring:message code="global.short.save"/>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="no-margin">
                <spring:hasBindErrors name="priorityForm">
                    <p class="message error no-margin">
                        <form:errors path="*"/>
                    </p>
                </spring:hasBindErrors>
                <table id="priorityTable" class="table no-margin" dojoType="dojo.dnd.Source" withHandles="true" autoSync="true">
                    <script type="dojo/connect" event="onDrop" args="source, nodes, copy">
                        listReordered();
                    </script>
                    <tbody>
                    <c:forEach items="${priorityForm.priorities}" var="priority" varStatus="counter">
                        <tr id="priority_${priority.id}" class="dojoDndItem">
                            <td style="width:15px">
                                <img class="dojoDndHandle" src="<c:url value="/images/theme/icons/fugue/arrow-move.png"/>">
                                <c:if test="${priority.id != null}"><form:hidden path="priorities[${counter.index}].id"/></c:if>
                                <form:hidden path="priorities[${counter.index}].order"/>
                            </td>
                            <td><form:input path="priorities[${counter.index}].name" dojoType="dijit.form.TextBox" maxlength="50" cssStyle="width:100%" cssErrorClass="error"/></td>
                            <td style="width:20px">
                                <form:select path="priorities[${counter.index}].icon" dojoType="dijit.form.ComboBox" cssStyle="width:70px">
                                    <form:option value="text" label="text"/>
                                    <form:option value="blue" label="blue"/>
                                    <form:option value="gray" label="gray"/>
                                    <form:option value="green" label="green"/>
                                    <form:option value="orange" label="orange"/>
                                    <form:option value="purple" label="purple"/>
                                    <form:option value="red" label="red"/>
                                    <form:option value="yellow" label="yellow"/>
                                    <form:option value="blank" label="blank"/>
                                </form:select>
                            </td>
                            <td style="width:15px">
                                <a class="delPriority" href="javascript:void(0)">
                                    <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </form:form>
</section>
</body>
