<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="myReports" type="java.util.List<Report>"--%>
<%--@elvariable id="pubReports" type="java.util.List<Report>"--%>
<%--@elvariable id="widget" type="net.grouplink.ehelpdesk.domain.dashboard.Widget"--%>
<%--@elvariable id="reportGroupBys" type="java.util.Map<Integer, Integer>"--%>

<head>

    <style type="text/css">

        .wijTypeRow td {
            padding-left: 25px;
            white-space: nowrap;
        }

        .wijTypeRow img {
            vertical-align: top;
            padding-left: 25px;
            width: 30px;
        }
    </style>

    <script type="text/javascript">

        // map of ticketfilter ids and whether that ticketfilter has is a simple list(0), group(1) or regroup(2)
        var reportGroupBys = {};
        <c:forEach var="rgbEntry" items="${reportGroupBys}">
        reportGroupBys['${rgbEntry.key}'] = '${rgbEntry.value}';
        </c:forEach>

        function enableWidgetTypes(reportId) {
            switch (reportGroupBys[reportId]) {
                case '0':
                    dijit.byId('bar').setAttribute("disabled", true);
                    dijit.byId('stackedBar').setAttribute("disabled", true);
                    dijit.byId('pie').setAttribute("disabled", true);
                    dijit.byId('multiPie').setAttribute("disabled", true);
                    break;
                case '1':
                    dijit.byId('bar').setAttribute("disabled", false);
                    dijit.byId('stackedBar').setAttribute("disabled", true);
                    dijit.byId('pie').setAttribute("disabled", false);
                    dijit.byId('multiPie').setAttribute("disabled", true);
                    break;
                case '2':
                    dijit.byId('bar').setAttribute("disabled", false);
                    dijit.byId('stackedBar').setAttribute("disabled", false);
                    dijit.byId('pie').setAttribute("disabled", false);
                    dijit.byId('multiPie').setAttribute("disabled", false);
                    break;
                default:
                    dijit.byId('bar').setAttribute("disabled", true);
                    dijit.byId('stackedBar').setAttribute("disabled", true);
                    dijit.byId('pie').setAttribute("disabled", true);
                    dijit.byId('multiPie').setAttribute("disabled", true);
                    break;
            }
        }

        function cancelForm() {
            window.location.href = '<c:url value="/config/dashboardEdit.glml"/>?dashId=${widget.dashboard.id}';
        }

        function viewWidget(widgetId) {
            var url = '<c:url value="/config/widgetEdit.glml"/>';
            if (widgetId != null) url += '?widgetId=' + widgetId + '&dashId=${widget.dashboard.id}';
            window.location.href = url;
        }

        function deleteWidget(widgetId) {
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/config/widgetDelete.glml"/>?widgetId=' + widgetId + '&dashId=${widget.dashboard.id}';
            }
        }

        function showThresholds(wijType) {
            // Get a handle to each threshold row
            var thresh1 = document.getElementById("thresh_1"); // Min Max
            var thresh2 = document.getElementById("thresh_2"); // Critical
            var thresh3 = document.getElementById("thresh_3"); // Grean
            var thresh4 = document.getElementById("thresh_4"); // Yellow
            var thresh5 = document.getElementById("thresh_5"); // Red

            switch (wijType) {
                case 1: // Dial {min, max, gyr}
                    showRow(thresh1);
                    hideRow(thresh2);
                    showRow(thresh3);
                    showRow(thresh4);
                    showRow(thresh5);
                    break;
                case 2: // Horizontal Arc Dial {min, max}
                case 3: // Vertical Arc Dial {min, max}
                case 5: // Bar Chart {min, max}
                case 6: // Stacked Bar Chart {min, max}
                    showRow(thresh1);
                    hideRow(thresh2);
                    hideRow(thresh3);
                    hideRow(thresh4);
                    hideRow(thresh5);
                    break;
                case 4: // Thermometer {min, max, critical, gyr}
                    showRow(thresh1);
                    showRow(thresh2);
                    showRow(thresh3);
                    showRow(thresh4);
                    showRow(thresh5);
                    break;
                case 7: // Pie Chart {}
                case 8: // Multiple Pie Chart {}
                default:
                    hideRow(thresh1);
                    hideRow(thresh2);
                    hideRow(thresh3);
                    hideRow(thresh4);
                    hideRow(thresh5);
                    break;
            }
        }

        function showRow(row) {
            row.style.display = "";
            row.style.visibility = "visible";
        }

        function hideRow(row) {
            row.style.display = "none";
            row.style.visibility = "hidden";
        }

        function init() {
            // Hide all thresholds initially by calling showThresholds with a bad wijType
            showThresholds(0);

        <c:if test="${not empty widget.type}">
            // determine the thresholds to show
            showThresholds(${widget.type});
        </c:if>

            enableWidgetTypes(${widget.ticketFilter.id});

        }

        dojo.addOnLoad(init);
    </script>

</head>

<body>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/config/dashboardList.glml"/>"><spring:message code="dashboard.title"/></a></li>
        <li><a href="<c:url value="/config/dashboardEdit.glml?dashId=${widget.dashboard.id}"/>"><spring:message code="dashboard.edit.title"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="dashboard.widget.title"/></a></li>
    </ul>
</content>

<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="dashboard.widget.title"/></h1>

        <form:form modelAttribute="widget" cssClass="form" action="" method="post">

        <spring:hasBindErrors name="widget">
            <p class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>

        <p>
            <form:label path="name" cssClass="required"><spring:message code="dashboard.widget.name"/></form:label>
            <form:input path="name" cssErrorClass="error" size="60" maxlength="255"/>
        </p>

        <p>
            <form:label path="report" cssClass="required"><spring:message code="dashboard.widget.report"/></form:label>
            <spring:bind path="widget.report">
                <select name="${status.expression}" id="${status.expression}" onchange="enableWidgetTypes(this.value);">
                    <option value="-1"><spring:message code="dashboard.widget.tfPleaseSelect"/></option>
                    <c:if test="${fn:length(myReports)>0}">
                        <optgroup label="<spring:message code="report.myreports"/>">
                            <c:forEach var="myrep" items="${myReports}">
                                <option value="${myrep.id}"
                                        <c:if test="${status.value eq myrep.id}">selected="selected"</c:if>>${myrep.name}</option>
                            </c:forEach>
                        </optgroup>
                    </c:if>
                    <c:if test="${fn:length(pubReports)>0}">
                        <optgroup label="<spring:message code="report.publicreports"/>">
                            <c:forEach var="pubrep" items="${pubReports}">
                                <option value="${pubrep.id}"
                                        <c:if test="${status.value eq pubrep.id}">selected="selected"</c:if>>${pubrep.name}</option>
                            </c:forEach>
                        </optgroup>
                    </c:if>
                </select>
            </spring:bind>
        </p>

        <div class="columns">
            <div class="colx2-left">
                <p>
                    <form:label path="row" cssClass="required"><spring:message code="dashboard.widget.row"/></form:label>
                    <form:input path="row" dojoType="dijit.form.NumberTextBox" required="true" constraints="{min:0,max:100,places:0}" >
                        <jsp:attribute name="promptMessage"><spring:message code="dashboard.widget.rowPrompt"/></jsp:attribute>
                        <jsp:attribute name="invalidMessage"><spring:message code="dashboard.widget.requireNumber100"/></jsp:attribute>
                    </form:input>
                </p>
                <p>
                    <form:label path="colspan" cssClass="required"><spring:message code="dashboard.widget.colspan"/></form:label>
                    <form:input path="colspan" dojoType="dijit.form.NumberSpinner" required="true" constraints="{min:1,max:10,places:0}" >
                        <jsp:attribute name="promptMessage"><spring:message code="dashboard.widget.colspanPrompt"/></jsp:attribute>
                        <jsp:attribute name="invalidMessage"><spring:message code="dashboard.widget.requireNumber10"/></jsp:attribute>
                    </form:input>
                </p>
            </div>
            <div class="colx2-right">
                <p>
                    <form:label path="column" cssClass="required"><spring:message code="dashboard.widget.col"/></form:label>
                    <form:input path="column" dojoType="dijit.form.NumberTextBox" required="true" constraints="{min:0,max:100,places:0}" >
                        <jsp:attribute name="promptMessage"><spring:message code="dashboard.widget.colPrompt"/></jsp:attribute>
                        <jsp:attribute name="invalidMessage"><spring:message code="dashboard.widget.requireNumber100"/></jsp:attribute>
                    </form:input>
                </p>
                <p>
                    <form:label path="rowspan" cssClass="required"><spring:message code="dashboard.widget.rowspan"/></form:label>
                    <form:input path="rowspan" dojoType="dijit.form.NumberSpinner" required="true" constraints="{min:1,max:10,places:0}" >
                        <jsp:attribute name="promptMessage"><spring:message code="dashboard.widget.rowspanPrompt"/></jsp:attribute>
                        <jsp:attribute name="invalidMessage"><spring:message code="dashboard.widget.requireNumber10"/></jsp:attribute>
                    </form:input>
                </p>
            </div>
        </div>

        <table cellpadding="3" cellspacing="3" border="0">

        <%--WIDGET TYPES--%>
        <tr>
            <td colspan="5">
                <table>
                    <spring:bind path="widget.type">
                        <tr>
                            <td colspan="4">
                                <div id="typeFld">
                                    <form:label path="type" class="required"><spring:message code="dashboard.widget.type"/></form:label>
                                </div>
                            </td>
                        </tr>

                        <tr class="wijTypeRow">
                            <td>
                                <input type="radio" id="dial" name="${status.expression}" dojoType="dijit.form.RadioButton"
                                       value="1"
                                       <c:if test="${status.value eq 1}">checked</c:if> onclick="showThresholds(1);"/>
                                <label for="dial" class="inline">
                                    <spring:message code="dashboard.widget.type.dial"/><br/>
                                </label>
                            </td>
                            <td>
                                <input type="radio" id="harcDial" name="${status.expression}" dojoType="dijit.form.RadioButton"
                                       value="2"
                                       <c:if test="${status.value eq 2}">checked</c:if> onclick="showThresholds(2);"/>
                                <label for="harcDial" class="inline">
                                    <spring:message code="dashboard.widget.type.harcDial"/><br/>
                                </label>
                            </td>
                            <td>
                                <input type="radio" id="varcDial" name="${status.expression}" dojoType="dijit.form.RadioButton"
                                       value="3"
                                       <c:if test="${status.value eq 3}">checked</c:if> onclick="showThresholds(3);"/>
                                <label for="varcDial" class="inline">
                                    <spring:message code="dashboard.widget.type.varcDial"/><br/>
                                </label>
                            </td>
                            <td>
                                <input type="radio" id="thermo" name="${status.expression}" dojoType="dijit.form.RadioButton"
                                       value="4"
                                       <c:if test="${status.value eq 4}">checked</c:if> onclick="showThresholds(4);"/>
                                <label for="thermo" class="inline">
                                    <spring:message code="dashboard.widget.type.thermo"/><br/>
                                </label>
                            </td>
                        </tr>
                        <tr class="wijTypeRow">
                            <td id="dialImg">
                                <img src="<c:url value="/images/dashboards/dial.jpg"/>" alt="Dial"/>
                            </td>
                            <td id="horizArcDialImg">
                                <img src="<c:url value="/images/dashboards/horizArcDial.jpg"/>" alt="Hor Arc"/>
                            </td>
                            <td id="vertArcDialImg">
                                <img src="<c:url value="/images/dashboards/vertArcDial.jpg"/>" alt="Vert Arc"/>
                            </td>
                            <td id="thermoImg">
                                <img src="<c:url value="/images/dashboards/thermometer.jpg"/>" alt="Thermometer"/>
                            </td>
                        </tr>
                        <tr class="wijTypeRow">
                            <td>
                                <input type="radio" id="bar" name="${status.expression}" dojoType="dijit.form.RadioButton"
                                       value="5"
                                       <c:if test="${status.value eq 5}">checked</c:if> onclick="showThresholds(5);"/>
                                <label for="bar" class="inline">
                                    <spring:message code="dashboard.widget.type.bar"/><br/>
                                </label>
                            </td>
                            <td>
                                <input type="radio" id="stackedBar" name="${status.expression}"
                                       dojoType="dijit.form.RadioButton"
                                       value="6"
                                       <c:if test="${status.value eq 6}">checked</c:if> onclick="showThresholds(6);"/>
                                <label for="stackedBar" class="inline">
                                    <spring:message code="dashboard.widget.type.stackedBar"/><br/>
                                </label>
                            </td>
                            <td>
                                <input type="radio" id="pie" name="${status.expression}" dojoType="dijit.form.RadioButton"
                                       value="7"
                                       <c:if test="${status.value eq 7}">checked</c:if> onclick="showThresholds(7);"/>
                                <label for="pie" class="inline">
                                    <spring:message code="dashboard.widget.type.pie"/><br/>
                                </label>
                            </td>
                            <td>
                                <input type="radio" id="multiPie" name="${status.expression}" dojoType="dijit.form.RadioButton"
                                       value="8"
                                       <c:if test="${status.value eq 8}">checked</c:if> onclick="showThresholds(8);"/>
                                <label for="multiPie" class="inline">
                                    <spring:message code="dashboard.widget.type.multiPie"/><br/>
                                </label>
                            </td>
                        </tr>
                        <tr class="wijTypeRow">
                            <td id="barImg">
                                <img src="<c:url value="/images/dashboards/barChart.jpg"/>" alt="Bar"/>
                            </td>
                            <td id="stackBarImg">
                                <img src="<c:url value="/images/dashboards/stackedBarChart.jpg"/>" alt="Bar Stack"/>
                            </td>
                            <td id="pieImg">
                                <img src="<c:url value="/images/dashboards/pieChart.jpg"/>" alt="Pie"/>
                            </td>
                            <td id="multiPieImg">
                                <img src="<c:url value="/images/dashboards/multiPieChart.jpg"/>" alt="Multi Pie"/>
                            </td>
                        </tr>
                    </spring:bind>
                </table>
            </td>
        </tr>
        <%--END WIDGET TYPES--%>

        <%--THRESHOLDS--%>
        <tr>
            <td width="20%">
                <span class="label"><spring:message code="dashboard.widget.thresholds"/></span>
            </td>
        </tr>
        <tr class="wijTypeRow">
            <td colspan="5">
                <table>
                    <tr id="thresh_1">
                        <td>
                            <spring:message code="dashboard.widget.minValue"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.minimum">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                        <td>
                            <spring:message code="dashboard.widget.maxValue"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.maximum">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                    </tr>
                    <tr id="thresh_2">
                        <td>
                            <spring:message code="dashboard.widget.critical"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.critical">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                    </tr>
                    <tr id="thresh_3">
                        <td>
                            <spring:message code="dashboard.widget.green"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.greenRangeBegin">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                        <td>
                            <spring:message code="dashboard.widget.to"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.greenRangeEnd">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                    </tr>
                    <tr id="thresh_4">
                        <td>
                            <spring:message code="dashboard.widget.yellow"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.yellowRangeBegin">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                        <td>
                            <spring:message code="dashboard.widget.to"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.yellowRangeEnd">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                    </tr>
                    <tr id="thresh_5">
                        <td>
                            <spring:message code="dashboard.widget.red"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.redRangeBegin">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                        <td>
                            <spring:message code="dashboard.widget.to"/>:
                        </td>
                        <td>
                            <spring:bind path="widget.redRangeEnd">
                                <input type="text" name="${status.expression}" value="${status.value}"
                                       dojotype="dijit.form.NumberTextBox" required="true" style="width:50px"
                                       constraints="{places:0}"/>
                            </spring:bind>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
        <br/>
        <c:choose>
            <c:when test="${fn:length(myReports)>0 or fn:length(pubReports)>0}">
                <p class="grey-bg no-margin" id="commandButtons">
                    <button type="submit"><spring:message code="global.save"/></button>
                    <button type="button" onclick="cancelForm();"><spring:message code="global.cancel"/></button>
                </p>
            </c:when>
            <c:otherwise>
                <p class="grey-bg no-margin" id="commandButtons">
                    <button disabled="disabled" type="submit"><spring:message code="global.save"/></button>
                    <button type="button" onclick="cancelForm();"><spring:message code="global.cancel"/></button>
                    <span style="color: red"> * <spring:message code="dashboard.widget.nofilters"/></span>
                </p>
            </c:otherwise>
        </c:choose>
        </form:form>
    </div>
</section>
<div id="tooltips" style="display:none">
    <div dojotype="dijit.Tooltip" connectid="dialImg">
        <img src="<c:url value="/images/dashboards/dial.jpg"/>" alt=""/>
    </div>
    <div dojotype="dijit.Tooltip" connectid="horizArcDialImg">
        <img src="<c:url value="/images/dashboards/horizArcDial.jpg"/>" alt=""/>
    </div>
    <div dojotype="dijit.Tooltip" connectid="vertArcDialImg">
        <img src="<c:url value="/images/dashboards/vertArcDial.jpg"/>" alt=""/>
    </div>
    <div dojotype="dijit.Tooltip" connectid="thermoImg">
        <img src="<c:url value="/images/dashboards/thermometer.jpg"/>" alt=""/>
    </div>
    <div dojotype="dijit.Tooltip" connectid="barImg">
        <img src="<c:url value="/images/dashboards/barChart.jpg"/>" alt=""/>
    </div>
    <div dojotype="dijit.Tooltip" connectid="stackBarImg">
        <img src="<c:url value="/images/dashboards/stackedBarChart.jpg"/>" alt=""/>
    </div>
    <div dojotype="dijit.Tooltip" connectid="pieImg">
        <img src="<c:url value="/images/dashboards/pieChart.jpg"/>" alt="Pie"/>
    </div>
    <div dojotype="dijit.Tooltip" connectid="multiPieImg">
        <img src="<c:url value="/images/dashboards/multiPieChart.jpg"/>" alt=""/>
    </div>
</div>
</body>
