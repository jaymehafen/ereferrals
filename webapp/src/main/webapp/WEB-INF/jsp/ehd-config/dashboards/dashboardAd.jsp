<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="dashboards" type="java.util.List"--%>
<%--@elvariable id="showHeader" type="java.lang.Boolean"--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>
        <spring:message code="dashboard.title"/>
    </title>
    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>

</head>
<body>
<div id="bodyContent" style="padding: 10px;">
    <strong><spring:message code="dashboard.title"/></strong>
    <br/><br/>

    <p>
        Dashboards are not available in the free edition of <i>eReferrals</i>&reg;. To upgrade to eReferrals&trade;'s Enterprise edition,
        or for more information, please contact a Sales Representative at
        <a href="mailto:sales@grouplink.net">sales@grouplink.net</a> or 801-335-0702.
    </p>

    <p>
        Dashboards give management the ability to gauge real-time performance with dashboards. With eReferrals, you can create
        customized dashboards based on saved trouble ticket filters. This will help identify trends so you can quickly
        respond to critical process issues including work order management, incident management, ticket tracking and
        others. Choose the gauges and dials you want to display your key performance indicators (KPI's) and easily
        arrange and customize the look of the dashboard to your preference.
    </p>

    <p>To view the capability of eReferrals' Dashboards, watch the video below.</p>

    <p>
        <object width="500" height="405">
            <param name="movie"
                   value="http://www.youtube.com/v/zs1XNj0YZAw&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"/>
            <param name="allowFullScreen" value="true"/>
            <param name="allowscriptaccess" value="always"/>
            <embed src="http://www.youtube.com/v/zs1XNj0YZAw&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"
                   type="application/x-shockwave-flash" allowscriptaccess="always"
                   allowfullscreen="true" width="500" height="405"></embed>
        </object>

    </p>

</div>
</body>
</html>
