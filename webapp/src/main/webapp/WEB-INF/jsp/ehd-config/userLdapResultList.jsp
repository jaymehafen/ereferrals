<%@ include file="/WEB-INF/jsp/include.jsp" %>
<jsp:useBean id="field" class="net.grouplink.ehelpdesk.domain.LdapField" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>"
              type="text/css"/>
    </c:if>
    <script type="text/javascript" src="<c:url value="/js/dojo/dojo.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/ticketTable.js" />"></script>
    <script type="text/javascript" src="<c:url value="/js/scripts.js" />"></script>

    <script type="text/javascript">

        function init() {

            // decorate the ticket table
            tableruler();
            tablestripe();
        }

        function submitAll(){
            <c:forEach items="${ldapUserList}" var="user" varStatus="index">
                chooseUser('<c:out value="${user.URLEncodeDN}" />', 'plus_${index.index}');
            </c:forEach>
        }

<c:choose>
    <c:when test="${importLdap}" >
        function chooseUser(userId, buttonId) {
            dojo.byId(buttonId).src = '<c:url value='/images/circle-ball-dark-antialiased.gif'/>';
            dojo.io.bind({
                url: '<c:url value="/tickets/legacy/createUserAccountFromLdap.glml?dn=" />' + userId,
                handle: function(type, data, evt) {
                    if (type == "error") {
                        dojo.byId(buttonId).src = '<c:url value='/images/delete.gif'/>';
                        alert(data.message);
                    }
                    else if(data == "loginEmailError") {
                        dojo.byId(buttonId).src = '<c:url value='/images/delete.gif'/>';
                        dojo.byId('tr_' + buttonId).title = "<spring:message code="ldap.error.loginEmailError" />";

                    }
                    else {
                        dojo.byId(buttonId).src = '<c:url value='/images/ok_16.gif'/>';

                    }
                },
                mimetype: "text/plain"
            });
        }
    </c:when>
    <c:otherwise>
        function chooseUser(userId, buttonId) {
            dojo.io.bind({
                url: '<c:url value="/tickets/legacy/createUserAccountFromLdap.glml?dn=" />' + userId,
                handle: function(type, data, evt) {
                    if (type == "error") {

                    }
                    else if(data == "loginEmailError") {
                        dojo.byId(buttonId).src = '<c:url value='/images/delete.gif'/>';
                        dojo.byId('tr_' + buttonId).title = "<spring:message code="ldap.error.loginEmailError" />";

                    }
                    else {
                        var elements = data.split(';');
                        openWindow('<c:url value="/tickets/legacy/userSearchList.glml?userId=" />' + elements[0], 'ticketSearchResult');
                    }
                },
                mimetype: "text/plain"
            });
        }
    </c:otherwise>
</c:choose>

        dojo.addOnLoad(init);
    </script>
</head>
<body>
<c:if test="${importLdap}" >
    <table><tr><td>
    <a href="javascript://" onclick="submitAll()"><spring:message code="config.importAll" /></a>
    </td></tr></table>
</c:if>
    <table class="stripe ruler paged">
        <tr>
            <td></td>
            <td nowrap="nowrap"><strong><spring:message code="user.lastName" /></strong></td>
            <td nowrap="nowrap"><strong><spring:message code="user.firstName" /></strong></td>
            <td nowrap="nowrap"><strong><spring:message code="user.userName" /></strong></td>
            <td nowrap="nowrap"><strong><spring:message code="user.email" /></strong></td>
            <c:forEach items="${ldapFieldList}" var="field">
                <td nowrap="nowrap"><strong>
                    <c:out value="${field.label}"/><c:if test="${empty field.label}" ><c:out value="${field.name}"/></c:if>
                </strong></td>
            </c:forEach>
        </tr>
        <c:forEach items="${ldapUserList}" var="user" varStatus="index">
            <tr class="toberuled" id="tr_plus_${index.index}" title="<c:out value="${user.dn}" />" onclick="chooseUser('<c:out value="${user.URLEncodeDN}" />', 'plus_${index.index}')" >
                <td nowrap="nowrap">
                    <img id="plus_${index.index}" src="<c:url value="/images/add.gif" />" alt="<c:out value="${user.dn}" />"/>
                </td>
                <td nowrap="nowrap">
                    <c:out value="${user.sn}"/>
                </td>
                <td nowrap="nowrap">
                    <c:out escapeXml="false" value="${user.givenName}"/>
                </td>
                <td nowrap="nowrap">
                    <c:out escapeXml="false" value="${user.cn}"/>
                </td>
                <td nowrap="nowrap">
                    <c:out escapeXml="false" value="${user.mail}"/>
                </td>
                <c:forEach items="${user.attributeMap}" var="map">
                    <td nowrap="nowrap">
                        <c:out escapeXml="false" value="${map.value}"/>
                    </td>
                </c:forEach>
            </tr>
        </c:forEach>
    </table>
</body>
</html>