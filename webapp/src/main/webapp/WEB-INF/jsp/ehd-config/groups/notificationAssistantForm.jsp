<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="urgUserList" type="java.util.List<User>"--%>
<%--@elvariable id="urg" type="net.grouplink.ehelpdesk.domain.UserRoleGroup"--%>
<%--@elvariable id="assignmentLabel" type="java.lang.String"--%>

<body>
<form:form modelAttribute="notificationAssistantForm" cssClass="form" method="put">
    <jsp:attribute name="action"><c:url value="/config/groups/${group.id}/groupRoles/${urg.id}/notificationAsst"><c:param name="dialog" value="true"/></c:url></jsp:attribute>
    <jsp:body>
        <br/>
        <fieldset>
            <p>
                <form:label path="users"><spring:message code="notAssist.notify" arguments="${assignmentLabel}"/></form:label>
                <form:select path="users" items="${urgUserList}" multiple="multiple" cssClass="chosen full-width" itemValue="id" itemLabel="displayName"/>
                <br/><br/><br/><br/><br/><br/>
            </p>
        </fieldset>

        <p id="buttons">
            <button type="submit" id="notAssSubmitBtn" name="save"><spring:message code="global.save"/></button>
            <button type="button" id="notAssCancelBtn" name="cancel"><spring:message code="global.cancel"/></button>
        </p>
    </jsp:body>
</form:form>
</body>
