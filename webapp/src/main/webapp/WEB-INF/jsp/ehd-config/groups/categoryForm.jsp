<%@ include file="/WEB-INF/jsp/include.jsp" %>

<c:choose>
    <c:when test="${category.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/config/groups/${group.id}/categories"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/config/groups/${group.id}/categories/${category.id}">
                <c:param name="dialog" value="true"/>
            </c:url>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="category" cssClass="form" method="${method}" action="${action}">
    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="category">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <br>
    <fieldset>
        <p>
            <form:label path="name"><spring:message code="category.name"/></form:label>
            <form:input path="name" size="50" maxlength="255"/>
        </p>

        <p>
            <form:label path="active" cssClass="float-left" cssStyle="padding-right: 15px;">
                <spring:message code="global.active"/>
            </form:label>
            <form:checkbox path="active" cssClass="mini-switch with-tip">
                <jsp:attribute name="title"><spring:message code="global.active.toggle"/></jsp:attribute>
            </form:checkbox>
        </p>
    </fieldset>
    <p>
        <button type="submit" id="catSaveBtn" name="save"><spring:message code="global.save"/></button>
        <button type="button" id="catCancelBtn" name="cancel"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
