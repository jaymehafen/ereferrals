<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="saveSuccess" type="java.lang.Boolean"--%>

<c:choose>
    <c:when test="${categoryOption.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/config/groups/${group.id}/categoryOptions"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/config/groups/${group.id}/categoryOptions/${categoryOption.id}">
                <c:param name="dialog" value="true"/>
            </c:url>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="categoryOption" method="${method}" action="${action}">
    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="categoryOption">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <c:if test="${saveSuccess}">
        <div class="no-margin">
            <p id="successMessage" class="message success no-margin">
                <spring:message code="categoryOption.creation.success"/>
            </p>
        </div>
    </c:if>
    <br>
    <fieldset
            style="border: 1px solid #D9D9D9; border-radius: 0.25em 0.25em 0.25em 0.25em; margin-bottom: 1.667em; padding: 1em 1.667em 1.667em;">
        <p>
            <form:label path="category"
                        cssStyle="color: #808080; display: block; font-weight: bold; margin-bottom: 0.5em;">
                <spring:message code="ticket.category"/>
            </form:label>
            <form:select id="catFilterSelectBox" path="category"/>
        </p>

        <p class="form">
            <form:label path="name"><spring:message code="category.name"/></form:label>
            <form:input path="name" size="50" maxlength="255"/>
        </p>

        <p class="form">
            <form:label path="active" cssClass="float-left" cssStyle="padding-right: 15px;">
                <spring:message code="global.active"/>
            </form:label>
            <form:checkbox path="active" cssClass="mini-switch with-tip">
                <jsp:attribute name="title"><spring:message code="global.active.toggle"/></jsp:attribute>
            </form:checkbox>
        </p>
    </fieldset>
    <p class="form">
        <button type="submit" id="catSaveBtn" name="save"><spring:message code="global.save"/></button>
        <button type="button" id="catOptCancelBtn" name="cancel"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
