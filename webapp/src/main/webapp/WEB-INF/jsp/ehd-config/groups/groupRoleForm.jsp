<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="saveSuccess" type="java.lang.Boolean"--%>
<%--@elvariable id="roleList" type="java.util.List"--%>

<c:set var="action">
    <c:url value="/config/groups/${group.id}/groupRoles"><c:param name="dialog" value="true"/></c:url>
</c:set>

<c:if test="${tekCountExceeded}">
<p class="message error no-margin">
    <spring:message code="licensing.tech.limit"/>
</p>
</c:if>

<c:if test="${wfpCountExceeded}">
<p class="message error no-margin">
    <spring:message code="licensing.wfp.limit"/>
</p>
</c:if>

<form:form modelAttribute="userRoleGroup" method="post" action="${action}">
    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="userRoleGroup">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <c:if test="${saveSuccess}">
        <div class="no-margin">
            <p id="successMessage" class="message success no-margin">
                <spring:message code="group.role.createSuccess"/>
            </p>
        </div>
    </c:if>
    <br>
    <fieldset
            style="border: 1px solid #D9D9D9; border-radius: 0.25em 0.25em 0.25em 0.25em; margin-bottom: 1.667em; padding: 1em 1.667em 1.667em;">
        <p>
            <form:label path="userRole.user"
                        cssStyle="color: #808080; display: block; font-weight: bold; margin-bottom: 0.5em;">
                <spring:message code="user.user"/>
            </form:label>
            <form:select id="groupRoleUserSelectBox" path="userRole.user"/>
        </p>

        <p class="form">
            <form:label path="userRole.role"><spring:message code="user.role"/></form:label>
            <form:select id="groupRoleRoleSelectBox" path="userRole.role"/>
        </p>
    </fieldset>
    <p class="form">
        <button type="submit" id="groupRoleSaveBtn" name="save"><spring:message code="global.save"/></button>
        <button type="button" id="groupRoleCancelBtn" name="cancel"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
