<%--@elvariable id="groupList" type="java.util.List"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true"/>
    <title>
        <spring:message code="group.title"/>
    </title>
    <script type="text/javascript">

        dojo.addOnLoad(function () {

            //handle button clicks for deleting a location
            dojo.query('a[id^="del_group_"]').connect("onclick", function (evt) {
                if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
                    var groupId = this.id.replace("del_group_", "");
                    var elementToRemove = this.parentNode.parentNode;
                    dojo.xhrDelete({
                        url:"<c:url value="/config/groups/"/>" + groupId,
                        handle:function (response, ioargs) {
                            if (ioargs.xhr.status == 417) {
                                dojo.place('<ul class="message warning grid_12">\
                                     <li><spring:message code="group.inuse" javaScriptEscape="true"/></li>\
                                  </ul>', "flashMessage", "only");
                            } else {
                                dojo.fadeOut({
                                    node:elementToRemove,
                                    onEnd:function () {
                                        dojo.destroy(elementToRemove);
                                    }
                                }).play();
                            }
                        }
                    });
                }
            });


        });
    </script>
</head>
<body>

<section class="grid_10">
    <div class="block-content no-padding">
        <h1><spring:message code="group.title"/></h1>

        <div class="block-controls">
            <ehd:authorizeGlobal permission="global.systemConfig">
                <ul class="controls-buttons">
                    <li>
                        <a id="addNewGroup" href="<c:url value="/config/groups/new"/>">
                            <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                            <spring:message code="unitSetup.addGroup"/>
                        </a>
                    </li>
                </ul>
            </ehd:authorizeGlobal>
        </div>
        <table class="table full-width">
            <thead>
            <tr>
                <th scope="col"><spring:message code="group.name"/></th>
                <th scope="col"><spring:message code="group.comments"/></th>
                <th scope="col"><spring:message code="global.active"/></th>
                <th scope="col"><spring:message code="group.assetTrackerType"/></th>
                <th scope="col"><spring:message code="group.permissionModel"/></th>
                <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${groupList}" var="group" varStatus="index">
                <tr>
                    <td><c:out value="${group.name}"/></td>
                    <td><c:out value="${group.comments}"/></td>
                    <td><c:out value="${group.active}"/></td>
                    <td><c:out value="${group.assetTrackerType}"/></td>
                    <td><c:out value="${group.permissionScheme.name}"/></td>
                    <td class="table-actions">
                        <a href="<c:url value="/config/groups/${group.id}/edit"/>">
                            <img alt="edit" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/>
                        </a>
                        <ehd:authorizeGlobal permission="global.systemConfig">
                            <a id="del_group_${group.id}" href="javascript://">
                                <img alt="remove"
                                     src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                            </a>
                        </ehd:authorizeGlobal>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</section>
</body>