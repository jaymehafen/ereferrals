<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="assignmentFilter" type="net.grouplink.ehelpdesk.domain.filter.AssignmentFilter"--%>
<head>
<meta name="configSidebar" content="true"/>
<title>
    <spring:message code="ticketSettings.assignments"/>
</title>

<link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/Grid.css"/>" type="text/css">
<link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dojox/grid/resources/tundraGrid.css"/>" type="text/css">

<script type="text/javascript">

    var groupStore = new dojo.data.ItemFileReadStore({
        url:"<c:url value="/stores/groups.json"><c:param name="incEmpty" value="true"/></c:url>",
        urlPreventCache:true,
        clearOnClose:true
    });

    var categoryStore = new dojo.data.ItemFileReadStore({
        url:"<c:url value="/stores/categories.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${assignmentFilter.group.id}"/></c:url>",
        urlPreventCache:true,
        hierarchical: false,
        clearOnClose:true
    });

    var catOptStore = new dojo.data.ItemFileReadStore({
        url:"<c:url value="/stores/catOptions.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${assignmentFilter.category.id}"/></c:url>",
        urlPreventCache:true,
        clearOnClose:true
    });

    var techStore = new dojo.data.ItemFileReadStore({
        url:"<c:url value="/stores/ticketSearchAssignments.json"><c:param name="incEmpty" value="true"/><c:param name="count" value="-1"/></c:url>",
        urlPreventCache:true,
        clearOnClose:true
    });

    var assignmentStore = new dojox.data.QueryReadStore({
        url:"<c:url value="/config/assignments/assignmentStore"/>"
    });

    dojo.addOnLoad(function () {
        // Create the FilteringSelects for the Filter Options
        new dijit.form.FilteringSelect({
            store:groupStore,
            autoComplete:true,
            required:false,
            searchAttr:"label",
            style:"width: 15em;",
            id:"group",
            name:"group",
            value:"${assignmentFilter.group.id}",
            onChange:function (group) {
                loadNextSelectForPrev('group', 'category', categoryStore, '<c:url value="/stores/categories.json"/>');
            }
        }, "group");

        new dijit.form.FilteringSelect({
            store:categoryStore,
            autoComplete:true,
            required:false,
            searchAttr:"label",
            style:"width: 15em;",
            id:"category",
            name:"category",
            value:"${assignmentFilter.category.id}",
            onChange:function (cat) {
                loadNextSelectForPrev('category', 'categoryOption', catOptStore, '<c:url value="/stores/catOptions.json"/>');
            }
        }, "category");

        new dijit.form.FilteringSelect({
            store:catOptStore,
            autoComplete:true,
            required:false,
            searchAttr:"label",
            style:"width: 15em;",
            id:"categoryOption",
            name:"categoryOption",
            value:"${assignmentFilter.categoryOption.id}"
        }, "categoryOption");

        new dijit.form.FilteringSelect({
            store:techStore,
            autoComplete:true,
            required:false,
            searchAttr:"label",
            style:"width:15em;",
            id:"assignment",
            name:"assignment",
            value:"<c:out value="${assignmentFilter.assignment}"/>"
        }, "assignment");

        function formatActions(value, rowIndex, cell) {
            var assId = value.i.assId;
            return '<a title="<spring:message code="global.delete"/>" class="deleteLink" id="del_assignment_' + assId + '" href="javascript://"><img style="width:12px;height:12px;" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"></a>';
        }

        var assGrid = new dojox.grid.DataGrid({
            store:assignmentStore,
            structure:[
                {name:"<spring:message code="ROLE_TECHNICIAN"/>", field:"userRoleGroup", width:'auto'},
                {name:"<spring:message code="global.group"/>", field:"group", width:'auto'},
                {name:"<spring:message code="ticket.category"/>", field:"category", width:'auto'},
                {name:"<spring:message code="ticket.categoryOption"/>", field:"categoryOption", width:'auto'},
                {name:"<spring:message code="ticket.location"/>", field:"location", width:'auto'},
                {name:"<spring:message code="global.actions"/>", field:"_item", width:"50px", formatter:formatActions}
            ],
            editable:false,
            autoHeight:20,
            rowSelector: '20px',
            noDataMessage:"<spring:message code="assignment.nogriddata" text="no data"/>",
            id: "assignmentsGrid_grid"
        }, "assignmentsGrid");

        assGrid.canSort = function (col) {
            return !(Math.abs(col) == 6);
        };

        assGrid.startup();

        $(".deleteLink").live("click", function (e) {
            e.preventDefault();
            if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
                var assId = $(this).attr("id").replace("del_assignment_", "");
                var elementToRemove = this.parentNode.parentNode;
                dojo.xhrDelete({
                    url:"<c:url value="/config/assignments/"/>" + assId,
                    handle:function (response, ioargs) {
                        if (ioargs.xhr.status == 417) {
                            dojo.place('<ul class="message warning grid_12">\
                                     <li><spring:message code="assignment.nodelete" javaScriptEscape="true"/></li>\
                                  </ul>', "flashMessage", "only");
                        } else {
                            refreshGrid();
                        }
                    }
                });
            }
        });

    });

    function refreshGrid() {
        var grid = dijit.byId("assignmentsGrid_grid");
        grid.setStore(assignmentStore);
    }

    function loadNextSelectForPrev(prevName, nextName, nextStore, nextStoreUrl) {
        var prevId = dijit.byId(prevName).getValue();
        if (prevId == "") {
            // clear successive select starting from 'next'
            clearFromPoint(nextName);
            return;
        }

        // if prevName is categoryOption then set the groupId parameter for assignments
        // and load the customFields
        var groupId = null;
        var locationId = dijit.byId("location").getValue();

        // reset the store with prevId parameter
        resetStore(nextName, nextStore, nextStoreUrl, prevId, groupId, locationId);
    }

    function clearFromPoint(start) {
        switch (start) {
            case "group":
                resetStore("group", groupStore, '<c:url value="/stores/groups.json"/>', '', null, null);
            // fall through
            case "category":
                resetStore("category", categoryStore, '<c:url value="/stores/categories.json"/>', '', null, null);
            // fall through
            case "categoryOption":
                resetStore("categoryOption", catOptStore, '<c:url value="/stores/catOptions.json"/>', '', null, null);
                break;
            default:
                break;
        }
    }

    function resetStore(selectName, store, storeUrl, prevId, groupId, locationId) {
        // build the store URL
        var builtURL = storeUrl + "?incEmpty=true&prevId=" + prevId;
        if (groupId != null) builtURL += "&groupId=" + groupId;
        if (locationId != null) builtURL += "&locationId=" + locationId;

        store.close();
        store = new dojo.data.ItemFileReadStore({
            url:builtURL,
            hierarchical: false
        });

        store.fetch();
        dijit.byId(selectName).store = store;
        dijit.byId(selectName).setDisplayedValue('');

        store.fetch({query:{}, onComplete:function (items, request) {
            if (items.length == 1) {
                dijit.byId(selectName).attr('value', items[0].id);
            }
        }
        });
    }

    $(function() {
        $("#deleteSelectedButton").on("click", function(event) {
            var grid = dijit.byId("assignmentsGrid_grid");
            var items = grid.selection.getSelected();
            if (!items || items.length == 0 || (items.length == 1 && !items[0])) {
                alert("<spring:message code="global.noItemsSelected" javaScriptEscape="true"/>");
                return;
            }

            if (confirm("<spring:message code="global.confirmDeleteSelectedItems" javaScriptEscape="true"/>")) {

                var assIds = new Array();
                for (var i=0; i<items.length; i++) {
                    assIds.push(items[i].i.assId);
                }

                $.ajax({
                    url: "<c:url value="/config/assignments/massDelete"/>",
                    type: "POST",
                    data: {"assignmentIds[]": assIds, "_method" : "DELETE"},
                    success: function(result) {
                        // refresh grid
                        refreshGrid();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        alert("error deleting assignments: " + errorThrown);
                    }
                });
            }
        });
    });
</script>
</head>

<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="ticketSettings.assignments"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="addNewAssignment" href="<c:url value="/config/assignments/new"/>">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="assignment.create"/>
                    </a>
                </li>
            </ul>
        </div>
        <p class="mini-infos">
            <spring:message code="assignment.explanation"/>
        </p>
        <form:form modelAttribute="assignmentFilter" class="form" method="get">
            <input type="hidden" name="filterApplied" value="true"/>
            <fieldset class="grey-bg">
                <legend>
                    <a href="#"><spring:message code="assignment.filter"/></a>
                </legend>
                <table class="full-width">
                    <tr>
                        <td>
                            <div class="float-left gutter-right">
                                <form:label path="assignment">
                                    <spring:message code="ROLE_TECHNICIAN"/>
                                </form:label>
                                <form:input path="assignment"/>
                            </div>

                            <div class="float-left gutter-right">
                                <form:label path="group"><spring:message code="global.group"/></form:label>
                                <form:input path="group"/>
                            </div>
                            <div class="float-left gutter-right">
                                <form:label path="category"><spring:message code="ticket.category"/></form:label>
                                <form:input path="category"/>
                            </div>
                            <div class="float-left gutter-right">
                                <form:label path="categoryOption"><spring:message
                                        code="ticket.categoryOption"/></form:label>
                                <form:input path="categoryOption"/>
                            </div>
                            <div class="float-left gutter-right">
                                <form:label path="location"><spring:message code="ticket.location"/></form:label>
                                <form:select path="location" cssStyle="width: 15em;"
                                             dojoType="dijit.form.FilteringSelect">
                                    <form:option value="">&nbsp;</form:option>
                                    <form:options items="${locations}" itemLabel="name" itemValue="id"/>
                                </form:select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px;">
                            <button id="applyFilter" type="submit">
                                <spring:message code="assignment.filter.apply"/>
                            </button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form:form>
        <br/>

        <button id="deleteSelectedButton"><spring:message code="global.deleteSelected"/></button>

        <br/>
        <br/>
        <div id="assignmentsGrid"></div>
    </div>
</section>
</body>
