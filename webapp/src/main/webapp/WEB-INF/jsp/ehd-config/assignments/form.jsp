<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="categories" type="java.util.List"--%>

<c:set var="action">
    <c:url value="/config/assignments/new"/>
</c:set>

<html>
<head>
    <meta name="configSidebar" content="true"/>
    <title><spring:message code="assignment.new"/></title>

    <link rel="stylesheet" href="<c:url value="/css/ticketFilterEdit.css"/>" type="text/css">

    <script id="assignment_tmpl" type="text/html">
        <li class="filter-definition">
            <input type="hidden" name="technicianId_{{row}}" value="{{techId}}"/>
            <input type="hidden" name="techName_{{row}}" value="{{tech}}"/>
            <a href="javascript:void(0)" class="no-padding"> {{tech}} </a>
            <ul class="mini-menu">
                <li>
                    <a href="javascript:void(0)" class="hideAssignLi">
                        <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                        <spring:message code="global.remove"/>
                    </a>
                </li>
            </ul>
            <ul class="extended-options">
                <li>
                    <label for="locations_{{row}}" class="required"><spring:message code="ticket.location"/></label>
                    <select name="locations_{{row}}" id="locations_{{row}}" class="chosen" multiple="multiple" style="width:20em;">
                        <option value=""></option>
                        <c:forEach var="loc" items="${locations}">
                            <option value="${loc.id}"><c:out value="${loc.name}"/></option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label for="groups_{{row}}"><spring:message code="ticket.group"/></label>
                    <select name="groups_{{row}}" id="groups_{{row}}" class="chosen" multiple="multiple" style="width:20em;">
                        <option value=""></option>
                        <c:forEach var="group" items="${groups}">
                            <option value="${group.id}"><c:out value="${group.name}"/></option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label for="categories_{{row}}"><spring:message code="ticket.category"/></label>
                    <select name="categories_{{row}}" id="categories_{{row}}" class="chosen" multiple="multiple" style="width:20em;">
                        <option value=""></option>
                        <c:forEach var="group" items="${groups}">
                            <optgroup label="${group.name}">
                                <c:forEach var="cat" items="${group.categories}">
                                    <option value="${cat.id}"><c:out value="${cat.name}"/></option>
                                </c:forEach>
                            </optgroup>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <label for="categoryOptions_{{row}}"><spring:message code="ticket.categoryOption"/></label>
                    <select name="categoryOptions_{{row}}" id="categoryOptions_{{row}}" class="chosen" multiple="multiple" style="width:20em;">
                        <option value=""></option>
                        <c:forEach var="category" items="${categories}">
                            <optgroup label="${category.name}">
                                <c:forEach var="catOpt" items="${category.categoryOptions}">
                                    <option value="${catOpt.id}"><c:out value="${catOpt.name}"/></option>
                                </c:forEach>
                            </optgroup>
                        </c:forEach>
                    </select>
                </li>
            </ul>
        </li>
    </script>

    <link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">
    <script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

    <script type="text/javascript">

        var techStore = new dojo.data.ItemFileReadStore({
            url:"<c:url value="/stores/users.json"><c:param name="tech" value="true"/><c:param name="allUrgs" value="true"/></c:url>",
            urlPreventCache:true,
            clearOnClose:true
        });

        dojo.addOnLoad(function () {
            new dijit.form.FilteringSelect({
                store:techStore,
                autoComplete:true,
                required:false,
                searchAttr:"label",
                style:"width:15em;",
                id:"technician",
                name:"technician"
            }, "technician");

            var rowCount = -1;

            dojo.connect(dojo.byId("addAssignmentLI"), "onclick", function () {
                var tekId = dijit.byId("technician").get("value");
                if (tekId !== "") {
                    var techName = dijit.byId("technician").get("displayedValue");
                    var template = _.template($("#assignment_tmpl").text());

                    rowCount++;
                    var html = dojo.trim(template({
                        tech:techName,
                        techId:tekId,
                        row: rowCount
                    }));
                    dojo.place(html, "assignmentList", "first");
                    $(".chosen").chosen();

                    // clear the technician select
                    dijit.byId("technician").set("value", null);
                }
            });

            $(".hideAssignLi").live("click", function () {
                var elementToRemove = $(this).parents(".filter-definition")[0];
                dojo.fadeOut({
                    node:elementToRemove,
                    onEnd:function () {
                        dojo.destroy(elementToRemove);
                    }
                }).play();
            });

            dojo.connect(dojo.byId("saveAssignments"), "onclick", function(){
                document.getElementById("topcount").value = rowCount;
                var form = dojo.byId("assignments");
                form.submit();
            });
        });
    </script>

</head>
<body>
<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/config/assignments"/>"><spring:message code="ticketSettings.assignments"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="assignment.new"/></a></li>
    </ul>
</content>
<section class="grid_10">
    <div id="filter-block" class="block-content form">
        <h1><spring:message code="assignment.new"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="saveAssignments" href="#">
                        <img src="<c:url value="/images/save.gif"/>" alt=""/>
                        <spring:message code="assignment.save"/>
                    </a>
                </li>
            </ul>
        </div>
        <p class="mini-infos">
            <spring:message code="assignment.new.explanation"/>
        </p>
        <div class="with-padding">
            <div class="float-left margin-right">
                <p class="inline-small-label">
                    <label for="technician"><spring:message code="ROLE_TECHNICIAN"/></label>
                    <input id="technician" name="technician"/>
                    <a class="button margin-left" style="height:21px;padding:3px 5px 0 5px;margin-top:1px"
                       href="javascript:void(0)" id="addAssignmentLI">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt=""
                             style="width:16px;height:16px">
                    </a>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
        <form:form modelAttribute="assignments" method="post" action="${action}">
            <input type="hidden" name="topcount" id="topcount">
            <ul id="assignmentList" class="extended-list no-hover"></ul>
        </form:form>
    </div>
</section>
</body>
</html>




<%--<%@ include file="/WEB-INF/jsp/include.jsp" %>--%>

<%--<c:set var="action">--%>
    <%--<c:url value="/config/assignments/new"/>--%>
<%--</c:set>--%>


<%--<head>--%>
    <%--<meta name="configSidebar" content="true"/>--%>
    <%--<title><spring:message code="assignment.new"/></title>--%>

    <%--<link rel="stylesheet" href="<c:url value="/css/ticketFilterEdit.css"/>" type="text/css">--%>
    <%--&lt;%&ndash;<link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">&ndash;%&gt;--%>

    <%--&lt;%&ndash;<script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>&ndash;%&gt;--%>

    <%--<script type="text/javascript">--%>

        <%--function groupChanged() {--%>
            <%--var groupId = dijit.byId("group").getValue();--%>
            <%--loadTechsForGroup(groupId);--%>
            <%--loadCatCatOpts(groupId);--%>
        <%--}--%>

        <%--function loadTechsForGroup(groupId) {--%>
            <%--techStore.close();--%>
            <%--techStore = new dojo.data.ItemFileReadStore({--%>
                <%--url:'<c:url value="/config/assignments/assignmentStore/group/"/>' + groupId--%>
            <%--});--%>

            <%--techStore.fetch();--%>
            <%--dijit.byId("technician").store = techStore;--%>
            <%--dijit.byId("technician").setDisplayedValue('');--%>

            <%--techStore.fetch({--%>
                <%--query:{}, onComplete:function (items, request) {--%>
                    <%--if (items.length == 1) {--%>
                        <%--dijit.byId("technician").attr('value', items[0].id);--%>
                    <%--}--%>
                <%--}--%>
            <%--});--%>
        <%--}--%>

        <%--function loadCatCatOpts(groupId) {--%>
            <%--dojo.xhrGet({--%>
                <%--url:"<c:url value="/stores/categories.json"/>?prevId=" + groupId,--%>
                <%--handleAs:"json",--%>
                <%--load:function (content) {--%>
                    <%--loadCategories(content);--%>
                    <%--loadCategoryOptions(content);--%>
                <%--}--%>
            <%--});--%>
        <%--}--%>

        <%--function loadCategories(content) {--%>
            <%--$('#category').empty();--%>
            <%--for (var i = 0; i < content.items.length; i++) {--%>
                <%--var id = content.items[i].id;--%>
                <%--var label = content.items[i].label;--%>
                <%--$('#category').append("<option value=\"" + id + "\">" + label + "</option>");--%>
            <%--}--%>
            <%--$('#category').trigger("liszt:updated");--%>
        <%--}--%>

        <%--function loadCategoryOptions(content) {--%>
            <%--$('#categoryOption').empty();--%>
            <%--for (var i = 0; i < content.items.length; i++) {--%>
                <%--var label = content.items[i].label;--%>
                <%--var catOpts = content.items[i].catOpts;--%>

                <%--var catOptSelect = $('#categoryOption');--%>
                <%--var optGroup = $('<optgroup/>');--%>
                <%--optGroup.attr('label', label);--%>

                <%--for (var j = 0; j < catOpts.length; j++) {--%>
                    <%--var cid = catOpts[j].catOptId;--%>
                    <%--var clabel = catOpts[j].catOptLabel;--%>
                    <%--optGroup.append("<option value=\"" + cid + "\">" + clabel + "</option>");--%>
                <%--}--%>
                <%--$('#categoryOption').append(optGroup);--%>
            <%--}--%>
            <%--$('#categoryOption').trigger("liszt:updated");--%>
        <%--}--%>

        <%--dojo.addOnLoad(function () {--%>
            <%--$(".chosen").chosen();--%>
        <%--});--%>

        <%--$(function() {--%>

            <%--$("#catDiv").hide();--%>
            <%--$("#catOptDiv").hide();--%>
            <%--$("#assignment-radio-group").on("click", function(e) {--%>
                <%--$("#catDiv").hide();--%>
                <%--$('#category').val('').trigger('liszt:updated');--%>
                <%--$("#catOptDiv").hide();--%>
                <%--$('#categoryOption').val('').trigger('liszt:updated');--%>

            <%--});--%>

            <%--$("#assignment-radio-cat").on("click", function(e) {--%>
                <%--$("#catDiv").show();--%>
                <%--$("#catOptDiv").hide();--%>
                <%--$('#categoryOption').val('').trigger('liszt:updated');--%>
            <%--});--%>

            <%--$("#assignment-radio-catOpt").on("click", function(e) {--%>
                <%--$("#catDiv").hide();--%>
                <%--$('#category').val('').trigger('liszt:updated');--%>
                <%--$("#catOptDiv").show();--%>
            <%--});--%>
        <%--});--%>
    <%--</script>--%>

<%--</head>--%>
<%--<body>--%>
<%--<div dojoType="dojo.data.ItemFileReadStore" jsId="groupStore" urlPreventCache="true" clearOnClose="true"--%>
     <%--url="<c:url value="/stores/groups.json"><c:param name="incEmpty" value="true"/></c:url>"></div>--%>
<%--<div dojoType="dojo.data.ItemFileReadStore" jsId="techStore" urlPreventCache="true" clearOnClose="true"--%>
     <%--url="<c:url value="/config/assignments/assignmentStore/group/"/>"></div>--%>
<%--<div dojoType="dojo.data.ItemFileReadStore" jsId="categoryStore" urlPreventCache="true" clearOnClose="true"--%>
     <%--url="<c:url value="/stores/categories.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="-1"/></c:url>"></div>--%>
<%--<div dojoType="dojo.data.ItemFileReadStore" jsId="catOptStore" urlPreventCache="true" clearOnClose="true"--%>
     <%--url="<c:url value="/stores/catOptions.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="-1"/></c:url>"></div>--%>

<%--<content tag="breadCrumb">--%>
    <%--<ul id="breadcrumb">--%>
        <%--<li><a href="<c:url value="/config/assignments"/>"><spring:message code="ticketSettings.assignments"/></a></li>--%>
        <%--<li><a href="javascript:location.reload();"><spring:message code="assignment.new"/></a></li>--%>
    <%--</ul>--%>
<%--</content>--%>
<%--<section class="grid_10">--%>
    <%--<div id="filter-block" class="block-content form">--%>
        <%--<h1><spring:message code="assignment.new"/></h1>--%>

        <%--<form:form modelAttribute="assignments" method="post" action="${action}">--%>

            <%--<h3 class="small-margin"><spring:message code="assignment.chooseTech"/></h3>--%>
            <%--<p><spring:message code="assignment.new.explanation"/></p>--%>

            <%--<p>--%>
                <%--<label for="group"><spring:message code="ticket.group"/></label>--%>

                <%--<input dojoType="dijit.form.FilteringSelect" store="groupStore" searchAttr="label"--%>
                       <%--name="group" id="group" onChange="groupChanged();"/>--%>
            <%--</p>--%>

            <%--<p>--%>
                <%--<label for="technician"><spring:message code="ROLE_TECHNICIAN"/></label>--%>

                <%--<input dojoType="dijit.form.FilteringSelect" store="techStore" searchAttr="label"--%>
                       <%--name="technician" id="technician"/>--%>
            <%--</p>--%>
            <%--<hr>--%>

            <%--<h3 class="small-margin"><spring:message code="assignment.assignmentType"/></h3>--%>
            <%--<p><spring:message code="assignment.assignmentType.explanation"/></p>--%>

            <%--<div class="columns">--%>
                <%--<div class="colx3-left">--%>
                    <%--<ul class="checkable-list">--%>
                        <%--<li>--%>
                            <%--<input id="assignment-radio-group" type="radio" name="assignmentType" value="group" checked="checked">--%>
                            <%--&nbsp;<label for="assignment-radio-group"><spring:message code="assignment.assignmentType.group"/></label>--%>
                        <%--</li>--%>
                        <%--<li>--%>
                            <%--<input id="assignment-radio-cat" type="radio" name="assignmentType" value="category">--%>
                            <%--&nbsp;<label for="assignment-radio-cat"><spring:message code="assignment.assignmentType.category"/></label>--%>
                        <%--</li>--%>
                        <%--<li>--%>
                            <%--<input id="assignment-radio-catOpt" type="radio" name="assignmentType" value="categoryOption">--%>
                            <%--&nbsp;<label for="assignment-radio-catOpt"><spring:message code="assignment.assignmentType.categoryOption"/></label>--%>
                        <%--</li>--%>
                    <%--</ul>--%>
                <%--</div>--%>
                <%--<div class="colx3-right-double">--%>
                    <%--<div id="catDiv">--%>
                        <%--<p>--%>
                            <%--<label for="category">--%>
                                <%--<spring:message code="ticket.category"/>--%>
                                <%--&lt;%&ndash;<tags:helpIcon id="catHelp" messageCode="assignment.new.category"/>&ndash;%&gt;--%>
                            <%--</label>--%>
                            <%--<select name="category" id="category" class="chosen" multiple="multiple" style="width:20em;">--%>
                            <%--</select>--%>
                        <%--</p>--%>
                    <%--</div>--%>
                    <%--<div id="catOptDiv">--%>
                        <%--<p>--%>
                            <%--<label for="categoryOption">--%>
                                <%--<spring:message code="ticket.categoryOption"/>--%>
                                <%--&lt;%&ndash;<tags:helpIcon id="catOptHelp" messageCode="assignment.new.catOpt"/>&ndash;%&gt;--%>
                            <%--</label>--%>
                            <%--<select name="categoryOption" id="categoryOption" class="chosen" multiple="multiple"--%>
                                    <%--style="width:20em;">--%>
                            <%--</select>--%>
                        <%--</p>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
            <%--<hr>--%>
            <%--<h3 class="small-margin"><spring:message code="ticket.location"/></h3>--%>
            <%--&lt;%&ndash;<p><spring:message code="assignment.location.explanation"/></p>&ndash;%&gt;--%>
            <%--<p><spring:message code="assignment.new.location"/></p>--%>
            <%--<p>--%>
                <%--<label for="location">--%>
                    <%--<spring:message code="ticket.location"/>--%>
                    <%--&lt;%&ndash;<tags:helpIcon id="locHelp" messageCode="assignment.new.location"/>&ndash;%&gt;--%>
                <%--</label>--%>
                <%--<select name="location" id="location" class="chosen" multiple="multiple" style="width:20em;">--%>
                    <%--<option value="">&nbsp;</option>--%>
                    <%--<c:forEach var="loc" items="${locations}">--%>
                        <%--<option value="${loc.id}"><c:out value="${loc.name}"/></option>--%>
                    <%--</c:forEach>--%>
                <%--</select>--%>
            <%--</p>--%>

            <%--<p class="grey-bg no-margin">--%>
                <%--<button type="submit"><spring:message code="global.save"/></button>--%>
            <%--</p>--%>
        <%--</form:form>--%>
    <%--</div>--%>
<%--</section>--%>

<%--</body>--%>
