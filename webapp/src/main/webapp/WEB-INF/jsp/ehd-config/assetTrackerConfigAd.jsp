<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>
        <spring:message code="assetTracker.configurationPage" text="Asset Tracker Configuration Page"/>
    </title>
    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>
</head>
<body>

<div id="bodyContent" style="padding: 10px;">
    <strong><spring:message code="assetTracker.configurationPage" text="Asset Tracker Configuration Page"/></strong>
    <br/><br/>

    <p>
        The Internal Asset Tracker is not available in the free edition of eReferrals. To upgrade to eReferrals&trade;'s
        Enterprise edition, or for more information, please contact a Sales Representative at
        <a href="mailto:sales@grouplink.net">sales@grouplink.net</a> or 801-335-0702.
    </p>

    <p>
        eReferrals allows you to effectively manage your hardware and software inventory and the clients they are assigned to.
        Import your assets directly into the help desk. Tie these assets to help desk tickets and create reports based
        on these trouble tickets giving you the ability to calculate warranty and lease end dates, manage downtime of
        assets and negotiate with vendors based on how frequently assets are having trouble. Use the built-in help desk
        asset tracker across multiple departments and locations.
    </p>

    <p>
        To view the capability of the robust Asset Tracker, watch the video below.
    </p>



    <p>
        <object width="500" height="405">
        <param name="movie"
                    value="http://www.youtube.com/v/L5YkIPRzq7k&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"/>
        <param name="allowFullScreen" value="true"/>
        <param name="allowscriptaccess" value="always"/>
        <embed src="http://www.youtube.com/v/L5YkIPRzq7k&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"
                    type="application/x-shockwave-flash" allowscriptaccess="always"
                    allowfullscreen="true" width="500" height="405"></embed>
        </object>
        
    </p>

</div>
</body>
</html>
