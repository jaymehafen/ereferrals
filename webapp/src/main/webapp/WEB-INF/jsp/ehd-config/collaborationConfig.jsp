<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title>
        <spring:message code="collaborationConfig.title"/>
    </title>

    <script type="text/javascript">
        function init(e) {
            function toggleGroupwiseFields(show) {
                var gwFields = dojo.byId("groupwiseFields");
                if(show) {
                    dojo.style(gwFields, "display", "block");
                } else {
                    dojo.style(gwFields, "display", "none");
                }
            }

            dojo.connect(dojo.byId("groupwiseIntegration1"), "onclick", function() {
                toggleGroupwiseFields(dojo.attr(this, "checked"));
            });

            toggleGroupwiseFields(dojo.attr(dojo.byId("groupwiseIntegration1"), "checked"));
        }

        dojo.addOnLoad(init);
    </script>
</head>
<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="collaborationConfig.title"/></h1>
        <h2><spring:message code="collaborationConfig.integrateWith"/></h2>
        <form:form modelAttribute="collaborationConfig" cssClass="form">
            <p>
                <form:checkbox path="groupwiseIntegration"/>
                <form:label path="groupwiseIntegration" cssClass="inline"><spring:message code="collaborationConfig.groupwise"/></form:label>

            </p>
            <div id="groupwiseFields">
                <p>
                    <form:label path="soapUrl"><spring:message code="collaborationConfig.url"/></form:label>
                    <form:input path="soapUrl" size="60"/>
                </p>
                <p>
                    <form:checkbox path="soapSsl">
                        <jsp:attribute name="label"><spring:message code="collaborationConfig.ssl"/></jsp:attribute>
                    </form:checkbox>
                </p>
                <p>
                    <form:label path="trustedAppName"><spring:message code="collaborationConfig.trustedAppName"/></form:label>
                    <form:input path="trustedAppName" size="60"/>
                </p>
                <p>
                    <form:label path="trustedAppKey"><spring:message code="collaborationConfig.trustedAppKey"/></form:label>
                    <form:input path="trustedAppKey" size="60"/>
                </p>
            </div>
            <br>
            <p class="grey-bg no-margin">
                <button type="submit" name="submit"><spring:message code="global.save"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
