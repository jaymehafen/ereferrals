<%@ include file="/WEB-INF/jsp/include.jsp" %>

<link href="<c:url value="/css/smoothness/jquery-ui-1.10.1.custom.min.css"/>" type="text/css" rel="stylesheet">
<link href="<c:url value="/css/jquery-ui-timepicker-addon.css"/>" type="text/css" rel="stylesheet">

<script type="text/javascript" src="<c:url value="/js/jquery-ui-1.10.1.custom.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery-ui-timepicker-addon.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/i18n/jquery.ui.datepicker-${fn:replace(rc.locale, '_', '-')}.js"/>"></script>

<script type="text/javascript">

    function loadContactComboFromHidden() {
        var contactId = dojo.byId("contact").value;
        if(contactId == "") return;

        dojo.xhrPost({
            url: '<c:url value="/stores/singleUser.json"/>',
            handleAs: "json",
            content: { contactId: contactId},
            load: function (jsonUser) {
                dijit.byId("ctctCombo").setValue(jsonUser.label);
            },
            error: function(response, ioArgs) {
                alert("XHR error (ticketTemplateEdit-js:loadContactComboFromHidden) HTTP status code: " + ioArgs.xhr.status);
                return response;
            }
        });
    }

    function loadNewContact() {
        var ctctLabel = dijit.byId("ctctCombo").getValue();
        if (ctctLabel == '<spring:message code="global.notSelected" javaScriptEscape="true" />') {
            document.getElementById("contact").value = '';
            clearCtctFields();
        } else {
            userStore.fetch({
                query: { label: ctctLabel},
                onBegin: clearCtctFields,
                onComplete: loadCtctFields,
                onError: fetchFailed,
                queryOptions: {deep:true}
            });
        }
    }

    function clearCtctFields() {
        var emptyMsg = "<spring:message code="global.notSelected" javaScriptEscape="true" />";
        document.getElementById("contact.email").innerHTML = emptyMsg;
        document.getElementById("contact.phone").innerHTML = emptyMsg;
        document.getElementById("contact.address").innerHTML = emptyMsg;
    }

    function loadCtctFields(items) {
        if (items.length == 1) return;

        var contact = items[1]; // items[0] is the empty option so items[1] is the selected option

        var id = userStore.getValue(contact, "userId");
        var cEmail = userStore.getValue(contact, "email");

        if (cEmail != "") {
            document.getElementById("contact.email").innerHTML = cEmail;
        }

        TicketService.getPrimaryPhoneForUser(id, handleGetPrimaryPhone);
        TicketService.getPrimaryAddressForUser(id, handleGetPrimaryAddress);

        if(id != ""){
            document.getElementById("contact").value = id;
        }
    }

    function fetchFailed(error) {
        alert("fetch failed: " + error);
    }

    function handleGetPrimaryPhone(phone) {
        if (phone) {
            var html = phone.number;
            if (phone.extension) {
                html += ' <spring:message code="phone.ext" javaScriptEscape="true" /> ' + phone.extension;
            }

            html += "&nbsp;(" + phone.phoneType + ")&nbsp;";

            if (phone.bestTimeToCall) {
                html += '<br/>' + phone.bestTimeToCall;
            }

            document.getElementById("contact.phone").innerHTML = html;
        }
    }

    function handleGetPrimaryAddress(address) {
        if (address) {

            var html = address.line1 + '<br/>';
            if (address.line2) {
                html += address.line2 + '<br/>';
            }

            html += address.city + ',&nbsp;' + address.state + ' ' + address.postalCode;
            if (address.country) {
                html += '<br/>' + address.country;
            }

            document.getElementById("contact.address").innerHTML = html;
        }
    }

    var customFieldIdArray = new Array();

    function resetCustomFields(groupId, locId, catId, catOptId) {
//        console.debug('called resetCustomFields');

        var templateId = dojo.byId("ttId").value;

        if (!groupId) groupId = dijit.byId("group").getValue();
        if (!locId) locId = dijit.byId("location").getValue();
        if (!catId) catId = dijit.byId("category").getValue();
        if (!catOptId) catOptId = dijit.byId("categoryOption").getValue();

        var yerl = "<c:url value="/stores/templateCustomFields.json"/>?ttId=" + templateId;
        if (groupId) yerl += "&groupId=" + groupId;
        if (locId) yerl += "&locId=" + locId;
        if (catId) yerl += "&catId=" + catId;
        if (catOptId) yerl += "&catOptId=" + catOptId;

//        console.debug("calling dojo.xhrGet");
        dojo.xhrGet({
            url: yerl,
            handleAs: "json",
            preventCache: true, 
            load: function(data) {
//                console.debug("called cf load");
                clearCustomFields();
                buildCustomFields(data, templateId);
//                console.debug("completed cf load")
            },
            error: function(response, ioArgs) {
//                console.debug("called error");
                alert("XHR error (ticketTemplateEdit-js:loadCustomFields) HTTP status code: " + ioArgs.xhr.status);
                return response;
            }
        });
    }

    function clearCustomFields() {
//    console.log("called clearCustomFields");
        // destroy existing custom field widgets
        var cfWidgets = dijit.findWidgets(dojo.byId("cfTable_0"));
        dojo.forEach(cfWidgets, function(w){
            w.destroyRecursive(true);
        });

        cfWidgets = dijit.findWidgets(dojo.byId("cfTable_1"));
        dojo.forEach(cfWidgets, function(w){
            w.destroyRecursive(true);
        });

        // clear custom field tables
        dojo.query("#cfTable_0 tr, #cfTable_1 tr").forEach(dojo.destroy);
//    console.log("finished clearCustomFields");

    }

    function connectLoadCustomFields() {
//        console.debug("called connectLoadCustomFields");

        var groupId = $("#initGroupId").val();
        var locId = $("#initLocId").val();
        var catId = $("#initCatId").val();
        var catOptId = $("#initCatOptId").val();

        resetCustomFields(groupId, locId, catId, catOptId);
    }

    function buildCustomFields(data, templateId) {
//        console.debug("called buildCustomFields");

        for (var ind in data) {
            var $table = $("#cfTable_" + ind % 2);
            var cfJson = data[ind];
            var $cfHtml = getCustomFieldHTML(cfJson, templateId);
            $table.append($cfHtml);
            dojo.parser.parse($table[0]);
        }

        setupDateTimePickers();

//        console.debug("completed buildCustomFields");
    }

    function setupDateTimePickers() {
        $('.dtpicker').datetimepicker({controlType: 'select'}); // Decorate all the datetime fields with datetimepicker
    }

    function getCustomFieldHTML(cf, tid) {
        var html;
        var cfId = "cf_" + cf.id + "_" + tid;
        var widget = dijit.byId(cfId);
        if (widget) {
            console.log("widget with id " + cfId + " already exists");
            widget.destroyRecursive(true);
        }

        var cfVal = cf.cfValue.value;
        var required = cf.required;
//        if (cfVal.trim() == "" && cfVals[cf.id]) cfVal = cfVals[cf.id];
        switch (cf.type) {
            case "text":
                var template = _.template($("#cf_text").html());
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId, cf_value: cfVal}));
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            case 'textarea':
                var template = _.template($("#cf_textarea").html());
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId, cf_value: cfVal}));
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            case 'radio':
                var template = _.template($("#cf_radio").html());
                var radioOptions = $("<div>");
                _.each(cf.options, function(cfOption) {
                    var checked = false;
                    var cfOptionId = cfId + '_' + cfOption.id;
                    if (cfOption.displayValue != "" && cfOption.displayValue == cfVal) checked = true;
                    var optionTemplate = _.template($("#cf_radio_option").html());
                    radioOptions.append($(optionTemplate({cf_dom_id: cfOptionId, cf_name: cfId, cf_value: cfOption.value, cf_label: cfOption.displayValue })).attr("checked", checked));
                    radioOptions.append("<br>");
                    customFieldIdArray[customFieldIdArray.length] = cfOptionId;
                });
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId, cf_value: "", cf_options: radioOptions.html()}));
                break;
            case 'checkbox':
                var checked = false;
                if (cfVal == 'true') {
                    checked = true;
                }
                var template = _.template($("#cf_checkbox").html());
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name: cfId}));
                html.find("input:checkbox").attr("checked", checked);
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            case 'select':
                var template = _.template($("#cf_select").html());
                var selectOptions = $("<div>");
                _.each(cf.options, function(cfOption) {
                    var selected = false;
                    if (cfOption.displayValue != "" && cfOption.displayValue == cfVal) {
                        selected = true;
                    }
                    var optionTemplate = _.template($("#cf_select_option").html());
                    var newOption = $(optionTemplate({cf_option_value: cfOption.value, cf_option_label: cfOption.displayValue}));
                    if(selected) {
                        newOption.attr("selected", "selected");
                    }
                    selectOptions.append(newOption);
                });
                html = $(template({cf_dom_id: cfId, cf_name: cfId, cf_label: cf["name"], cf_options: selectOptions.html()}));
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            case 'number':
                var template = _.template($("#cf_number").html());
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            case 'date':
                var template = _.template($("#cf_date").html());
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            case 'datetime':
                var template = _.template($("#cf_datetime").html());
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            case 'time':
                var template = _.template($("#cf_time").html());
                html = $(template({cf_dom_id: cfId, cf_label: cf["name"], cf_name:cfId, cf_value: cfVal}));
                customFieldIdArray[customFieldIdArray.length] = cfId;
                break;
            default:
                html = "";
                break;
        }

        if(required) { html.find("label").not(".inline").addClass("required"); }
        return html;
    }

    /****** attachments *******/
    var inputIndex = 1;
    function fileSelected() {
        var createNew = true;
        for (var i = 0; i < inputIndex; i++) {
            var uploadFiles = document.getElementsByName('attchmnt[' + i + ']')[0];
            if (uploadFiles && uploadFiles.value.length == 0) {
                createNew = false;
            }
        }
        if (createNew) {
            addFileSelect();
        }
    }

    function addFileSelect() {

        var newNode = document.createElement('input');
        var breakNode = document.createElement('br');
        var name = 'attchmnt[' + inputIndex + ']';
        var fileLoader = dojo.byId('fileLoader_EditDiv');

        newNode.setAttribute('type', 'file');
        newNode.setAttribute('name', name);
        newNode.setAttribute('id', name);
        newNode.setAttribute('size', '50');
        newNode.setAttribute('onchange', 'fileSelected()');
        fileLoader.appendChild(newNode);
        fileLoader.appendChild(breakNode);
        inputIndex++;

        //for IE to work
        var node = document.getElementById(name);
        if (node.attachEvent) {
            node.attachEvent('onchange', fileSelected);
            node.size = 85;
            node.style.width = '100%';
        }
    }

    /******* form button actions ******/
    function submitTemplateEdit(){
        // xhrPost didn't work for a multipart so i had to use the iframe.send instead
        // to get an ajax submit
        dojo.io.iframe.send({
            contentType: "multipart/form-data",
            handleAs: "text",
            form:     'templateForm',
            load:     function (data, ioArgs) {
//                console.debug("called load");
                $("#alertMessages").append('<ul class="alert message success"><li>' + data + '</li></ul>');
                fadeOutAlerts();
            },
            error:    function (error, ioArgs) {
//                console.debug("called error");
                console.error('Error: ', error);
                $("#alertMessages").append('<ul class="alert message error"><li>' + error + '</li></ul>');
                fadeOutAlerts();
            }
        });
    }

    function fadeOutAlerts() {
        setTimeout(function() {
            $(".alert").fadeOut("slow", function() {});
        }, 4000);
    }

    function groupChanged(){
        // reset cat
        resetCat();

        // reset cat opt
        resetCatOpt();

        // reset assignments
        resetAssignments();

        // reset custom fields
//        console.debug("groupChanged calling resetCustomFields");
        resetCustomFields();

        // reset asset tracker field
        var groupId = dijit.byId("group").getValue();
        if(groupId == "") return;

        dijit.byId("group").store.fetch({
            query: { id: groupId},
            onComplete: displayAssetTrack,
            onError: fetchFailed,
            queryOptions: {deep:true}
        });
    }

    function locationChanged(){
        // reset assignments
        resetAssignments();

        // reset custom fields
//        console.debug("locationChanged calling resetCustomFields");
        resetCustomFields();
    }

    function catChanged() {
        // reset cat opts
        resetCatOpt();

        // reset assignments
        resetAssignments();

        // reset custom fields
//        console.debug("catChanged calling resetCustomFields")
        resetCustomFields();
    }

    function catOptChanged() {
        // reset assignments
        resetAssignments();

        // reset custom fields
//        console.debug("catOptChanged calling resetCustomFields")
        resetCustomFields();
    }

    function resetCat() {
        var groupId = dijit.byId("group").getValue();

        categoryStore.close();
        categoryStore = new dojo.data.ItemFileReadStore({
            url: "<c:url value="/stores/categories.json"/>?incEmpty=true&prevId=" + groupId,
            hierarchical: false
        });
        categoryStore.fetch();
        dijit.byId("category").store = categoryStore;
        dijit.byId("category").setDisplayedValue("");
    }

    function resetCatOpt() {
        var catId = dijit.byId("category");
        catOptStore.close();
        catOptStore = new dojo.data.ItemFileReadStore({
            url: "<c:url value="/stores/catOptions.json"/>?incEmpty=true&prevId=" + catId,
            hierarchical: false
        });
        catOptStore.fetch();
        dijit.byId("categoryOption").store = catOptStore;
        dijit.byId("categoryOption").setDisplayedValue("");
    }

    function resetAssignments() {
        var groupId = dijit.byId("group").getValue();
        var locationId = dijit.byId("location").getValue();
        var catId = dijit.byId("category").getValue();
        var catOptId = dijit.byId("categoryOption").getValue();

        var url = "<c:url value="/stores/moreBetterAssignments.json"/>";
        var firstParam = true;
        if (locationId) {
            url += "?locationId=" + locationId;
            firstParam = false;
        }

        if (groupId) {
            url += (firstParam) ? "?" : "&";
            url += "groupId=" + groupId;
            firstParam = false;
        }

        if (catId) {
            url += (firstParam) ? "?" : "&";
            url += "catId=" + catId;
            firstParam = false;
        }

        if (catOptId) {
            url += (firstParam) ? "?" : "&";
            url += "catOptId=" + catOptId;
        }

        assignedToStore.close();
        assignedToStore = new dojo.data.ItemFileReadStore({
            url: url
        });
        dijit.byId("assignedTo").store = assignedToStore;
        dijit.byId("assignedTo").setDisplayedValue("");
    }

    function displayAssetTrack(jsonGroup){
        if(!jsonGroup || jsonGroup == "") return;

        var assTrackType = dijit.byId("group").store.getValue(jsonGroup[0], "assTrackType");

        var showEHD, showZEN, showLabel;
        if(assTrackType == "EHD"){
            showEHD = "block";
            showZEN = "none";
            showLabel = "block";
        } else if(assTrackType == "ZEN10"){
            showEHD = "none";
            showZEN = "block";
            showLabel = "block";
        } else {
            showEHD = "none";
            showZEN = "none";
            showLabel = "none";
        }

        document.getElementById("ehdAssetDiv").style.display = showEHD;
        document.getElementById("zenAssetDiv").style.display = showZEN;
        document.getElementById("assetLabel").style.display = showLabel;
    }

    // Asset Tracker Search Windows
    var ehdSearchWindow, zenSearchWindow;
    function assetSearch(){
        var assetNumber = document.getElementById('asset');

        // Build the url to pre-populate the search form
        var url = '<c:url value="/asset/searchAsset.glml?assetNumber=" />' + assetNumber.value;
        if (assetNumber.value == "") {
            url += "&location="+dijit.byId("location").getDisplayedValue();
            url += "&group="+dijit.byId("group").getDisplayedValue();
            url += "&category="+dijit.byId("category").getDisplayedValue();
            url += "&categoryOption="+dijit.byId("categoryOption").getDisplayedValue();
        }
        ehdSearchWindow = window.open(url, "ehdAssetSearch", "modal=yes,toolbar=no,status=yes,width=600,height=500,scrollbars=yes,resizable=yes,");
    }

    function choozAsset(assetId) {
        document.getElementById("asset").value = assetId;
        ehdSearchWindow.close();
    }

    function zenAssetSearch(){
        var contactId = dojo.byId("contact").value;
        var url = '<c:url value="/asset/searchZenAsset.glml"/>?contactUserId=' + contactId;
        zenSearchWindow = window.open(url, "zenAssetSearch", "modal=yes,toolbar=no,status=yes,width=600,height=500,scrollbars=yes,resizable=yes,");
    }

    function chooseZenAsset(zenAssetId){
        document.getElementById("zenAsset").value = zenAssetId;
        zenSearchWindow.close();
    }

    function deleteAttachment(templateId, attachmentId) {
        if (confirm('<spring:message code="ticketTemplate.deleteAttachment.confirm" javaScriptEscape="true"/>')) {
            TicketTemplateService.deleteAttachment(templateId, attachmentId);
            var attachDiv = document.getElementById('attachmentDiv_' + attachmentId);
            var attachmentsDiv = document.getElementById('attachmentsDiv');
            attachmentsDiv.removeChild(attachDiv);
        }
    }

    function ticketPaneInit() {
//        console.debug("called ticketPaneInit");
        loadContactComboFromHidden();
        connectLoadCustomFields();
    }

</script>
