<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="templateMaster" type="net.grouplink.ehelpdesk.domain.TicketTemplateMaster"--%>

<script type="text/javascript">
    function submitForm() {
        document.masterForm.submit();
    }

    function cancelForm() {
        location.href = '<c:url value="/config/templateMasterList.glml"/>';
    }

    function createTree() {
        // load up the tree model with json store
        var ttStore = new dojo.data.ItemFileWriteStore({
            url: "<c:url value="/stores/ticketTemplates.json"><c:param name="tmId" value="${templateMaster.id}"/></c:url>"
        });

        var ttModel = new dijit.tree.ForestStoreModel({
            store: ttStore,
            rootId: "root",
            rootLabel: "<spring:message code="ticketTemplate.templates"/>",
            childrenAttrs: ["children"]
        });

        new dijit.Tree({
            model: ttModel,
            onClick: loadEdit
        }, "ttTree");

        var contextMenu = createTreeMenu();
        // set the treeNodeId so the menus know which tree item was clicked on
        dojo.connect(contextMenu, "_openMyself", dijit.byId("ttTree"), function(e) {
            treeNodeId = dijit.getEnclosingWidget(e.target).item.id;

            // Disable rename and delete menus for the root node
            if (treeNodeId == "root") {
                contextMenu.getChildren().forEach(function(menuItem) {
                    if (menuItem.id != "add") menuItem.setDisabled(true);
                });
            } else {
                contextMenu.getChildren().forEach(function(menuItem) {
                    menuItem.setDisabled(false);
                });
            }
        });
    }

    function loadEdit(item) {
        if (item.id == "root") return;
        var yerl = '<c:url value="/config/ticketTemplateEdit.glml"/>?ttId=' + item.id;
        dijit.byId("ticketPane").setHref(yerl);
    }

    function createTreeMenu() {

        var ttMenu = new dijit.Menu({
            targetNodeIds: ["ttTree"]
        });

        ttMenu.addChild(new dijit.MenuItem({
            id: 'add',
            label: "<spring:message code="ticketTemplate.menu.add"/>",
            onClick: addNewTemplate
        }));
        ttMenu.addChild(new dijit.MenuItem({
            id: 'rename',
            label: "<spring:message code="ticketTemplate.menu.rename"/>",
            onClick: renameTemplate
        }));
        ttMenu.addChild(new dijit.MenuItem({
            id: 'delete',
            label: "<spring:message code="ticketTemplate.menu.delete"/>",
            onClick: deleteTemplate
        }));

        ttMenu.startup();
        return ttMenu;
    }

    function addNewTemplate() {
        document.getElementById("rename").value = "false";
        dijit.byId("treeNodeDialog").show();
    }

    <c:set var="tmId" value="${templateMaster.id}"/>
    <c:if test="${empty tmId}"><c:set var="tmId" value="-1"/></c:if>

    function deleteTemplate() {
        if (treeNodeId == "root") return;

        if (confirm('<spring:message code="global.confirmdelete"/>')) {
            dojo.xhrPost({
                url: '<c:url value="/config/templateMasterDeleteTemplate.glml"/>',
                handleAs: "text",
                content: {tmId: ${tmId}, ttId: treeNodeId},
                load: function(response, ioArgs) {
                    if (response != '') {
                        alert(response);
                    } else {
                        reloadTree();
                    }
                },
                error: function(response, ioArgs) {
                    alert("XHR error (templateMasterEdit-js:deleteTemplate) HTTP status code: " + ioArgs.xhr.status);
                    return response;
                }
            });
        }
    }

    function renameTemplate() {
        if (treeNodeId == "root") return;
        document.getElementById("rename").value = "true";
        dijit.byId("treeNodeDialog").show();
    }

    function saveName(dialogFlds) {
        var rename = document.getElementById("rename").value;

        var theName = dialogFlds.ttName;
        if (theName.length == 0) return;

        var yerl = (rename == "true") ? '<c:url value="/config/templateMasterRenameTemplate.glml"/>' :
                   '<c:url value="/config/templateMasterAddTemplate.glml"/>';

        // reset the field on the dialog to empty
        document.getElementById("ttName").value = "";

        dojo.xhrPost({
            url: yerl,
            handleAs: "text",
            content: {tmId: ${tmId}, tpId: treeNodeId, name: theName},
            load: reloadTree,
            error: function(response, ioArgs) {
                alert("XHR error (templateMasterEdit-js:saveName) HTTP status code: " + ioArgs.xhr.status);
                return response;
            }
        });
    }

    function reloadTree() {

        // destroy tree and menu items
        if (dijit.byId("ttTree")) dijit.byId("ttTree").destroy();
        if (dijit.byId("add")) dijit.byId("add").destroy();
        if (dijit.byId("rename")) dijit.byId("rename").destroy();
        if (dijit.byId("delete")) dijit.byId("delete").destroy();

        var newStore = new dojo.data.ItemFileWriteStore({
            url: "<c:url value="/stores/ticketTemplates.json"><c:param name="tmId" value="${templateMaster.id}"/></c:url>"
        });

        var newModel = new dijit.tree.ForestStoreModel({
            store: newStore,
            rootId: "root",
            rootLabel: "<spring:message code="ticketTemplate.templates"/>",
            childrenAttrs: ["children"]
        });

        var newTree = new dijit.Tree({model: newModel,id:"ttTree",onClick: loadEdit}, document.createElement("div"));
        dojo.byId("treePane").appendChild(newTree.domNode);

        var ctmenu = new dijit.Menu({
            targetNodeIds: ["ttTree"]
        });

        ctmenu.addChild(new dijit.MenuItem({
            id: 'add',
            label: "<spring:message code="ticketTemplate.menu.add"/>",
            onClick: addNewTemplate
        }));
        ctmenu.addChild(new dijit.MenuItem({
            id: 'rename',
            label: "<spring:message code="ticketTemplate.menu.rename"/>",
            onClick: renameTemplate
        }));
        ctmenu.addChild(new dijit.MenuItem({
            id: 'delete',
            label: "<spring:message code="ticketTemplate.menu.delete"/>",
            onClick: deleteTemplate
        }));

        ctmenu.startup();

        // set the treeNodeId so the menus know which tree item was clicked on
        dojo.connect(ctmenu, "_openMyself", newTree, function(e) {
            treeNodeId = dijit.getEnclosingWidget(e.target).item.id;

            // Disable rename and delete menus for the root node
            if (treeNodeId == "root") {
                ctmenu.getChildren().forEach(function(menuItem) {
                    if (menuItem.id != "add") menuItem.setDisabled(true);
                });
            } else {
                ctmenu.getChildren().forEach(function(menuItem) {
                    menuItem.setDisabled(false);
                });
            }
        });
    }

    function addWorkFlowStep(){
        document.getElementById("action").value = "addStep";
        submitWorkFlow();
    }
    
    function deleteWorkFlowStep(wsId){
        document.getElementById("action").value = "deleteStep";
        document.getElementById("wsId").value = wsId;
        submitWorkFlow();
    }

    function saveWorkflowForm(){
        submitWorkFlow();
    }

    function submitWorkFlow(){
        convertDnd2Hidden();

        // submit the form with xhrpost
        dojo.xhrPost({
            url: "<c:url value="/config/templateMasterWorkflow.glml"/>",
            form: "workflowForm",
            handleAs: "text",
            load: function(a, b){
                location.href = '<c:url value="/config/templateMasterEdit.glml"><c:param name="groupId" value="${templateMaster.group.id}"/><c:param name="tmId" value="${templateMaster.id}"/><c:param name="wfTab" value="true"/></c:url>';
            },
            error: function(a, b) {
                alert("Error saving workflow");
            }
        });
    }

    function convertDnd2Hidden(){
        var hiddenFields = document.getElementById("hiddenStepFields");
        var hfld;
        var wfstepids = document.getElementsByName("wfstepids");
        for(var ind = 0; ind<wfstepids.length; ind++){
            if(wfstepids[ind].value){
                var dndDiv = dojo.byId("dndCont_" + wfstepids[ind].value);
                var tts = dndDiv.childNodes;
                for(var i = 0; i<tts.length; i++){
                    if(tts[i].id){
                        hfld = document.createElement("input");
                        hfld.type = "hidden";
                        hfld.name = "ws_"+wfstepids[ind].value;
                        hfld.value = tts[i].id;
                        hiddenFields.appendChild(hfld);
                    }
                }
            }
        }
    }

    var treeNodeId;

    function clickFirstTreeNode(){
    <c:if test="${firstId != -1}">
        var yerl = '<c:url value="/config/ticketTemplateEdit.glml"><c:param name="ttId" value="${firstId}"/></c:url>';
        dijit.byId("ticketPane").setHref(yerl);

//        var tree = dijit.byId("ttTree");
//        var nodeMap = tree._itemNodeMap;
//        for(var n in nodeMap){
//            alert("n: " + n + " nm: " + nodeMap[n]);
//        }
    </c:if>
    }

    function tabClicked(child){
        switch(child.id){
            case "recurrTab":
                child.attr("href", '<c:url value="/config/templateMasterRecurrenceEdit.glml"><c:param name="ticketTemplateMasterId" value="${templateMaster.id}" /></c:url>');
                break;
            case "workflowTab":
                child.attr("href", '<c:url value="/config/templateMasterWorkflow.glml"><c:param name="tmId" value="${templateMaster.id}" /></c:url>');
                break;
        }
    }

    function init() {
        
        <c:if test="${not empty templateMaster.id}">createTree();</c:if>

        dojo.subscribe("mainTabContainer-selectChild", tabClicked);

        clickFirstTreeNode();

        var ticketPane = dijit.byId("ticketPane");
        ticketPane.attr("onDownloadEnd", function() {
//            console.debug("ticketPane onDownloadEnd");
            ticketPaneInit();
        });

    }

    dojo.addOnLoad(init);
</script>
