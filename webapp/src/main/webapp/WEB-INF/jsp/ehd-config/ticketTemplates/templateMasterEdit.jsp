<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="templateMaster" type="net.grouplink.ehelpdesk.domain.TicketTemplateMaster"--%>
<%--@elvariable id="selectWorkflowTab" type="java.lang.Boolean"--%>

<head>

    <title><spring:message code="ticketTemplate.title"/></title>

    <style type="text/css">
        #mainTabContainer {width:100%; height:650px;}
    </style>
    
    <script type="text/javascript" src="<c:url value="/dwr/interface/TicketService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/interface/TicketTemplateService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>

    <%-- For some stupid reason, this has to be included here at the surrounding --%>
    <%-- container for the ticketTemplateEdit.jsp to see it, even though its on the --%>
    <%-- DOM when I include in the jsp itself. --%>
    <%@ include file="/WEB-INF/jsp/ehd-config/ticketTemplates/ticketTemplateEdit-js.jsp" %>

    <%@ include file="/WEB-INF/jsp/ehd-config/ticketTemplates/templateMasterEdit-js.jsp" %>

    <%@ include file="/WEB-INF/jsp/ehd-config/ticketTemplates/templateMasterRecurrenceEdit-js.jsp" %>

</head>

<body>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/config/templateMasterList.glml"/>"><spring:message code="ticketTemplate.title"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="ticketTemplate.edit"/></a></li>
    </ul>
</content>

<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="ticketTemplate.edit"/></h1>

            <%--<div id="mainDiv" dojotype="dijit.layout.BorderContainer" design="headline">--%>
            <div id="mainDiv" >

                <%--<div dojoType="dijit.layout.ContentPane" region="top">--%>
                <div>
                    <form:form modelAttribute="templateMaster" name="masterForm" action="" method="post" class="form">
                        <spring:hasBindErrors name="templateMaster">
                            <p class="message error no-margin">
                                <form:errors path="*"/>
                            </p>
                        </spring:hasBindErrors>

                        <form:label path="name" class="required"><spring:message code="ticketTemplate.name"/></form:label>
                        <form:input path="name" size="60" maxlength="255">
                            <jsp:attribute name="cssClass">
                                <form:errors path="name">error</form:errors>
                            </jsp:attribute>
                        </form:input>

                        &nbsp;&nbsp;

                        <form:checkbox path="privified" id="privateCheck" dojotype="dijit.form.CheckBox"></form:checkbox>
                        <form:label path="privified" class="inline"><spring:message code="ticketTemplate.private"/></form:label>

                        &nbsp;&nbsp;
                        <button type="submit"><spring:message code="global.save"/></button>

                    </form:form>
                </div>

                <c:choose>
                    <c:when test="${not empty templateMaster.id}">
                        <br>
                        <c:if test="${not empty templateMaster.id}">
                            <span style="font-size:11px;padding-left:10px;">
                                <spring:message code="ticketTemplate.createMsg"/>
                            </span>
                            <br/><br/>
                        </c:if>
                    <%--<div dojoType="dijit.layout.TabContainer" id="tabs" jsId="tabs" region="center">--%>
                    <div dojoType="dijit.layout.TabContainer" id="mainTabContainer" jsId="mainTabContainer">

                            <div dojoType="dijit.layout.BorderContainer" design="sidebar"
                                 title="<spring:message code="ticketTemplate.tickets"/>">
                                <div dojoType="dijit.layout.ContentPane" id="treePane"
                                     region="leading" minSize="20" style="width:20%;" splitter="true">
                                    <div id="ttTree"> </div>
                                </div>

                                <div id="ticketPane" dojoType="dijit.layout.ContentPane" region="center">

                                </div>
                            </div>

                            <div dojoType="dijit.layout.ContentPane" id="recurrTab" title="<spring:message code="ticketTemplate.recurrence"/>">
                            </div>

                            <div dojoType="dijit.layout.ContentPane" id="workflowTab" title="<spring:message code="ticketTemplate.workFlow"/>"
                                 <c:if test="${selectWorkflowTab}">selected="true" href="<c:url value="/config/templateMasterWorkflow.glml">
                                 <c:param name="tmId" value="${templateMaster.id}" /></c:url>"</c:if>>
                            </div>

                        </div>
                    </c:when>
                    <c:otherwise>
                        <div dojotype="dijit.layout.ContentPane" region="center">

                        </div>
                    </c:otherwise>
                </c:choose>

            </div>

            <c:if test="${empty templateMaster.id}">
                <input type="hidden" name="nameOnly" value="true"/>
            </c:if>

            <div dojotype="dijit.Dialog" id="treeNodeDialog" title="<spring:message code="ticketTemplate.templateName"/>"
                 execute="saveName(arguments[0]);">
                <table>
                    <tr>
                        <td><spring:message code="ticketTemplate.name"/>:</td>
                        <td>
                            <input dojotype="dijit.form.ValidationTextBox" type="text" name="ttName" id="ttName"
                                   trim="true" required="true" maxlength="255"/>
                            <input type="hidden" name="rename" id="rename"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="saveButton" dojotype="dijit.form.Button" type="submit">
                                <spring:message code="global.save"/>
                            </button>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </section>
</body>
