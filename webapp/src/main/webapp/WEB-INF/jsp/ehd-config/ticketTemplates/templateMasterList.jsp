<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="groupList" type="java.util.List<net.grouplink.ehelpdesk.domain.Group>"--%>
<%--@elvariable id="techGroupList" type="java.util.List<net.grouplink.ehelpdesk.domain.Group>"--%>
<%--@elvariable id="pubGroupMap" type="java.util.Map<net.grouplink.ehelpdesk.domain.Group, java.util.List<net.grouplink.ehelpdesk.domain.TicketTemplateMaster>>"--%>
<%--@elvariable id="groupSlctId" type="java.lang.Integer"--%>
<head>
    <title>
        <spring:message code="ticketTemplate.title"/>
    </title>

    <script type="text/javascript">
        function viewTemplateMaster(tmId, groupId) {
            var url = '<c:url value="/config/templateMasterEdit.glml"/>?groupId=' + groupId;
            if (tmId != null) url += '&tmId=' + tmId;
            window.location.href = url;
        }

        function copyTemplateMaster(tmId) {
            if (confirm('<spring:message code="global.confirmcopy"/>')) {
                window.location.href = '<c:url value="/config/templateMasterCopy.glml"/>?tmId=' + tmId;
            }
        }

        function deleteTemplateMaster(tmId) {
            if (confirm('<spring:message code="global.confirmdelete"/>')) {
                window.location.href = '<c:url value="/config/templateMasterDelete.glml"/>?tmId=' + tmId;
            }
        }

        function launchTemplateMaster(tmId) {
            location.href = '<c:url value="/ticketTemplates/ticketTemplateLaunch.glml"/>?tmId=' + tmId;
        }

        function toggleScheduler(action, recSchedId) {
            dojo.xhrPost({
                url: '<c:url value="/config/templateMasterToggleScheduler.glml"/>',
                handleAs: "text",
                content: {action: action, recSchedId: recSchedId},
                load: function() {
                    location.href = location.href + '';
                },
                error: function(response, ioArgs) {
                    alert("XHR error (templateMasterList:toggleScheduler) HTTP status code: " + ioArgs.xhr.status);
                    return response;
                }
            });
        }

        dojo.addOnLoad(function() {
            hideLoader();
            var newTtButton = dojo.byId("newTtButton");
            var firstGroup = dijit.byId("mainTabContainer").getChildren()[0];
            var firstGroupId = firstGroup.id.replace("group_", "");
            dojo.attr(newTtButton, "href", '<c:url value="/config/templateMasterEdit.glml"/>?groupId=' + firstGroupId);
        });
    </script>
</head>

<body>
<div id="preloader"></div>
<section class="grid_12">
    <div class="block-content">
        <h1><spring:message code="ticketTemplate.title"/></h1>

        <div id="mainTabContainer" dojoType="dijit.layout.TabContainer" doLayout="false" persist="true">
            <c:forEach var="group" items="${groups}">
                <div id="group_${group.id}" dojoType="dijit.layout.ContentPane" title="${group.name}"
                    <c:if test="${groupSlctId eq group.id}">selected="true"</c:if>>

                    <ehd:authorizeGroup permission="group.ticketTemplateManagement" group="${group}">
                    <div style="padding:8px">
                        <a class="button" href="javascript:viewTemplateMaster(null, '${group.id}');" title="<spring:message code="ticketTemplate.new"/>">
                            <img alt="" src="<c:url value="/images/theme/icons/fugue/plus-circle.png" />"/>
                            <spring:message code="ticketTemplate.new"/>
                        </a>
                    </div>
                    </ehd:authorizeGroup>

                    <c:choose>
                        <c:when test="${not empty group.templateMasters}">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th><spring:message code="ticketTemplate.name"/></th>
                                    <th><spring:message code="ticketTemplate.structure"/></th>
                                    <th><spring:message code="ticketTemplate.action"/></th>
                                    <th><spring:message code="ticketTemplate.schedule"/></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="master" items="${group.templateMasters}">
                                    <%-- check if the tt master should be shown --%>
                                    <c:set var="showTT" value="false"/>
                                    <ehd:authorizeGroup permission="group.ticketTemplateManagement" group="${group}">
                                        <c:set var="showTT" value="true"/>
                                    </ehd:authorizeGroup>
                                    <ehd:authorizeGroup permission="group.launchNonPublicTicketTemplates" group="${group}">
                                        <c:set var="showTT" value="true"/>
                                    </ehd:authorizeGroup>
                                    <c:if test="${!master.privified}">
                                        <c:set var="showTT" value="true"/>
                                    </c:if>

                                    <c:if test="${showTT}">
                                    <tr>
                                        <td><c:out value="${master.name}"/></td>
                                        <td>
                                            <spring:message code="ticketTemplate.structureItem"
                                                            arguments="${fn:length(master.ticketTemplates)}"/>
                                        </td>
                                        <td>
                                            <c:if test="${fn:length(master.ticketTemplates) gt 0}">
                                                <a href="javascript://" title="<spring:message code="ticketTemplate.launch"/>"
                                                   onclick="launchTemplateMaster(<c:out value="${master.id}"/>); return false;">
                                                    <img alt="<spring:message code="ticketTemplate.launch"/>" src="<c:url value="/images/theme/icons/fugue/arrow-curve-000-left.png"/>"/>
                                                </a>
                                            </c:if>

                                            <ehd:authorizeGroup permission="group.ticketTemplateManagement" group="${group}">
                                            <a href="javascript://" title="<spring:message code="ticketTemplate.edit"/>"
                                               onclick="viewTemplateMaster(<c:out value="${master.id}"/>, ${group.id}); return false;">
                                                <img alt="<spring:message code="ticketTemplate.edit"/>" src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/>
                                            </a>
                                            <a href="javascript://" title="<spring:message code="ticketTemplate.copy"/>"
                                               onclick="copyTemplateMaster(<c:out value="${master.id}"/>); return false;">
                                                <img alt="<spring:message code="ticketTemplate.copy"/>" src="<c:url value="/images/copy_16.png"/>"/>
                                            </a>
                                            <a href="javascript://" title="<spring:message code="ticketTemplate.delete"/>"
                                               onclick="deleteTemplateMaster(<c:out value="${master.id}"/>); return false;">
                                                <img alt="<spring:message code="ticketTemplate.delete"/>" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                                            </a>
                                            </ehd:authorizeGroup>
                                        </td>
                                        <td>
                                            <ehd:authorizeGroup permission="group.ticketTemplateManagement" group="${group}">
                                            <c:if test="${empty master.recurrenceSchedule}">
                                                <spring:message code="ticketTemplate.noScheduler"/>
                                            </c:if>
                                            <c:if test="${not empty master.recurrenceSchedule}">
                                                <c:choose>
                                                    <c:when test="${master.recurrenceSchedule.running}">
                                                        <img src="<c:url value='/images/theme/icons/fugue/control-stop-square.png'/>" alt=""
                                                             title="<spring:message code="scheduler.stopProcess"/>"
                                                             onclick="toggleScheduler('stop', '${master.recurrenceSchedule.id}');"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <img src="<c:url value='/images/theme/icons/fugue/control.png'/>" alt=""
                                                             title="<spring:message code="scheduler.startProcess"/>"
                                                             onclick="toggleScheduler('start', '${master.recurrenceSchedule.id}');"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:if>
                                            &nbsp;
                                            <c:if test="${not empty master.recurrenceSchedule}">
                                                <c:choose>
                                                    <c:when test="${master.recurrenceSchedule.running}">
                                                        <img src="<c:url value="/images/circle-ball-dark-antialiased.gif"/>"
                                                             alt=""/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <img src="<c:url value="/images/circle-ball-dark-noanim.gif"/>" alt=""/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:if>
                                            </ehd:authorizeGroup>
                                        </td>
                                    </tr>
                                    </c:if>
                                    <c:remove var="showTT"/>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <p><spring:message code="ticketTemplate.noneFound"/></p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:forEach>
        </div>
    </div>
</section>
</body>
