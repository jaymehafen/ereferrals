<script type="text/javascript">
    function showDailyOptions() {
        document.getElementById('dailyOptions').style.display = '';
        document.getElementById('weeklyOptions').style.display = 'none';
        document.getElementById('monthlyOptions').style.display = 'none';
        document.getElementById('yearlyOptions').style.display = 'none';
    }

    function showWeeklyOptions() {
        document.getElementById('dailyOptions').style.display = 'none';
        document.getElementById('weeklyOptions').style.display = '';
        document.getElementById('monthlyOptions').style.display = 'none';
        document.getElementById('yearlyOptions').style.display = 'none';
    }

    function showMonthlyOptions() {
        document.getElementById('dailyOptions').style.display = 'none';
        document.getElementById('weeklyOptions').style.display = 'none';
        document.getElementById('monthlyOptions').style.display = '';
        document.getElementById('yearlyOptions').style.display = 'none';
    }

    function showYearlyOptions() {
        document.getElementById('dailyOptions').style.display = 'none';
        document.getElementById('weeklyOptions').style.display = 'none';
        document.getElementById('monthlyOptions').style.display = 'none';
        document.getElementById('yearlyOptions').style.display = '';
    }

    function saveRecurrenceScheduleForm() {

        var xhrArgs = {
            url: "<c:url value="/config/templateMasterRecurrenceEdit.glml"/>",
            form: "recurrenceScheduleForm",
//            handleAs: "text",
            load: function(data){
                dojo.byId("response").innerHTML = "<spring:message code="global.saveSuccess"/>";
            },
            error: function(error){
                //We'll 404 in the demo, but that's okay.  We don't have a 'postIt' service on the
                //docs server.
                dojo.byId("response").innerHTML = "<spring:message code="global.saveErrorMessage"/>";
            }
        };

        //Call the asynchronous xhrPost
//        dojo.byId("response").innerHTML = "Form being sent...";
        var deferred = dojo.xhrPost(xhrArgs);
    }

</script>