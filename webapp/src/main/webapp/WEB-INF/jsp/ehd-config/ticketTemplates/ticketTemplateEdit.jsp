<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="template" type="net.grouplink.ehelpdesk.domain.TicketTemplate"--%>
<%--@elvariable id="locations" type="java.util.List"--%>
<%--@elvariable id="priorities" type="java.util.List"--%>
<%--@elvariable id="ticketStatus" type="java.util.List"--%>

<style type="text/css">

    table.form-table {
        width: 100%;
    }

    table.form-table td {
        padding: 2px;
    }
</style>

<div id="ticketTemplateEdit">

    <h3><c:out value="${template.templateName}"/></h3>
    <br>

    <form:form name="templateForm" id="templateForm" commandName="template" method="post" enctype="multipart/form-data" cssClass="form">
        <input type="hidden" name="ttId" id="ttId" value="${template.id}"/>
        <input type="hidden" name="initGroupId" id="initGroupId" value="${template.group.id}"/>
        <input type="hidden" name="initLocId" id="initLocId" value="${template.location.id}"/>
        <input type="hidden" name="initCatId" id="initCatId" value="${template.category.id}"/>
        <input type="hidden" name="initCatOptId" id="initCatOptId" value="${template.categoryOption.id}"/>

        <%--Group--%>
        <div class="medium-margin">
            <form:label path="group"><spring:message code="ticket.group"/></form:label>
            <div dojoType="dojo.data.ItemFileReadStore" jsId="groupStore" urlPreventCache="true" clearOnClose="true"
                 url="<c:url value="/stores/groups.json"><c:param name="incEmpty" value="true"/><c:param name="filterByPerm" value="group.createTicket"/></c:url>"></div>
            <form:input path="group" dojoType="dijit.form.FilteringSelect" store="groupStore" searchAttr="label" onChange="groupChanged();"/>
        </div>

        <%--Submit Button--%>
        <div dojotype="dijit.layout.ContentPane" style="padding-top:3px">
            <button type="button" dojoType="dijit.form.Button" onclick="submitTemplateEdit();">
                <img src="<c:url value="/images/save.gif"/>" alt="">
                <spring:message code="ticketTemplate.saveForm"/>
            </button>
            <span id="submitResponse" style="color: green;"></span>
            <div id="alertMessages"></div>
        </div>

        <spring:bind path="template.contact">
            <input type="hidden" id="contact" name="${status.expression}" id="${status.expression}" value="${status.value}"/>
        </spring:bind>

        <%--CONTACT--%>
        <div dojotype="dijit.TitlePane" title="<spring:message code="ticket.contactInfo"/>">
            <table class="form-table">
                <tr>
                    <td>
                        <form:label path="contact">
                            <spring:message code="ticket.contact"/>
                        </form:label>
                    </td>
                    <td>
                        <div dojoType="dojox.data.QueryReadStore" jsId="userStore"
                             url="<c:url value="/stores/users.json"><c:param name="incEmpty" value="true"/></c:url>"></div>

                        <div dojoType="dijit.form.ComboBox" store="userStore" searchAttr="label"
                               name="ctctCombo" id="ctctCombo" autocomplete="false" pagesize="20" onChange="loadNewContact();">
                        </div>
                    </td>
                    <td>
                        <label>
                            <spring:message code="user.email"/>
                        </label>
                    </td>
                    <td>
                        <span id="contact.email">
                        <c:choose>
                            <c:when test="${not empty template.contact.email}">
                                <c:out value="${template.contact.email}"/>
                            </c:when>
                            <c:otherwise>
                                <spring:message code="global.notSelected"/>
                            </c:otherwise>
                        </c:choose>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>
                            <spring:message code="phone.phone"/>
                        </label>
                    </td>
                    <td>
                        <span id="contact.phone">
                            <c:choose>
                                <c:when test="${empty template.contact.phoneList}">
                                    <spring:message code="global.notSelected"/>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="phone" value="${template.contact.phoneList[0]}"/>
                                    <c:out value="${template.contact.phoneList[0].number}"/>
                                    &nbsp;(
                                    <c:out value="${template.contact.phoneList[0].phoneType}"/>
                                    )&nbsp;
                                    <c:out value="${template.contact.phoneList[0].extension}"/>
                                    <br/>
                                    <c:out value="${template.contact.phoneList[0].bestTimeToCall}"/>
                                </c:otherwise>
                            </c:choose>
                        </span>
                    </td>
                    <td>
                        <label>
                            <spring:message code="address.address"/>
                        </label>
                    </td>
                    <td>
                        <span id="contact.address">
                            <c:choose>
                                <c:when test="${empty template.contact.addressList}">
                                    <spring:message code="global.notSelected"/>
                                </c:when>
                                <c:otherwise>
                                    <c:out value="${template.contact.addressList[0].line1}"/>
                                    <br>
                                    <c:if test="${not empty template.contact.addressList[0].line2}">
                                        <c:out value="${template.contact.addressList[0].line2}"/>
                                        <br>
                                    </c:if>
                                    <c:out value="${template.contact.addressList[0].city}"/>
                                    ,&nbsp;
                                    <c:out value="${template.contact.addressList[0].state}"/>
                                    <c:out value="${template.contact.addressList[0].postalCode}"/>
                                    <br>
                                    <c:out value="${template.contact.addressList[0].country}"/>
                                </c:otherwise>
                            </c:choose>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="location">
                            <spring:message code="ticket.location"/>
                        </form:label>
                    </td>
                    <td>
                        <form:select path="location" dojoType="dijit.form.FilteringSelect" onchange="locationChanged();">
                            <form:option value=""></form:option>
                            <form:options items="${locations}" itemValue="id" itemLabel="name"/>
                        </form:select>
                    </td>

                </tr>
            </table>
        </div>

        <%--TICKET INFO--%>
        <div dojotype="dijit.TitlePane" title="<spring:message code="ticket.ticketInfo"/>">

            <div id="ticketInfoColumns" class="columns">
                <div class="colx2-left">
                    <table id="col_0" class="form-table">
                        <tr>
                            <td>
                                <form:label path="category">
                                    <spring:message code="ticket.category"/>
                                </form:label>
                            </td>
                            <td>
                                <div dojoType="dojo.data.ItemFileReadStore" jsId="categoryStore" urlPreventCache="true" clearOnClose="true"  hierarchical="false"
                                     url="<c:url value="/stores/categories.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${template.group.id}"/></c:url>"></div>
                                <form:input path="category" dojoType="dijit.form.FilteringSelect" store="categoryStore" searchAttr="label" onchange="catChanged();"/>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <form:label path="categoryOption">
                                    <spring:message code="ticket.categoryOption"/>
                                </form:label>
                            </td>
                            <td>
                                <div dojoType="dojo.data.ItemFileReadStore" jsId="catOptStore" urlPreventCache="true" clearOnClose="true"
                                     url="<c:url value="/stores/catOptions.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${template.category.id}"/></c:url>"></div>
                                <form:input path="categoryOption" dojoType="dijit.form.FilteringSelect" store="catOptStore" searchAttr="label" onchange="catOptChanged();"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <form:label path="assignedTo">
                                    <spring:message code="ticket.assignedTo"/>
                                </form:label>
                            </td>
                            <td>
                                <div dojoType="dojo.data.ItemFileReadStore" jsId="assignedToStore" urlPreventCache="true" clearOnClose="true"
                                     url="<c:url value="/stores/moreBetterAssignments.json"><c:param name="incEmpty" value="true"/><c:param name="groupId" value="${template.group.id}"/><c:param name="catId" value="${template.category.id}"/><c:param name="catOptId" value="${template.categoryOption.id}"/><c:param name="locationId" value="${template.location.id}"/></c:url>"></div>
                                <form:input path="assignedTo" dojoType="dijit.form.FilteringSelect" store="assignedToStore" searchAttr="label"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="colx2-right">
                    <table id="col_1" class="form-table">
                        <tr>
                            <td>
                                <form:label path="priority">
                                    <spring:message code="ticket.priority"/>
                                </form:label>
                            </td>
                            <td>
                                <form:select path="priority" dojoType="dijit.form.FilteringSelect">
                                    <form:option value=""/>
                                    <form:options items="${priorities}" itemValue="id" itemLabel="name"/>
                                </form:select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <form:label path="status">
                                    <spring:message code="ticket.status"/>
                                </form:label>
                            </td>
                            <td>
                                <form:select path="status" dojoType="dijit.form.FilteringSelect">
                                    <form:option value=""/>
                                    <form:options items="${ticketStatus}" itemValue="id" itemLabel="name"/>
                                </form:select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="assetLabel" <c:if test="${empty template.group.assetTrackerType}">style="display:none"</c:if>>
                                    <label>
                                        <spring:message code="ticket.asset.name"/>:
                                    </label>
                                </div>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${template.group.assetTrackerType eq 'EHD'}">
                                        <c:set var="showEHD" value="block"/>
                                        <c:set var="showZEN" value="none"/>
                                    </c:when>
                                    <c:when test="${template.group.assetTrackerType eq 'ZEN10'}">
                                        <c:set var="showEHD" value="none"/>
                                        <c:set var="showZEN" value="block"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="showEHD" value="none"/>
                                        <c:set var="showZEN" value="none"/>
                                    </c:otherwise>
                                </c:choose>

                                <div id="ehdAssetInfo">
                                    <spring:bind path="template.asset">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td nowrap="nowrap">
                                                    <div id="ehdAssetDiv" style="display:${showEHD}">
                                                        <input type="text" id="asset" name="${status.expression}"
                                                               value="<c:out value="${template.asset.assetNumber}"/>"/>
                                                        <a href="javascript:assetSearch();">
                                                            <img src="<c:url value='/images/find2.gif'/>" alt="" align="bottom">
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </spring:bind>
                                </div>

                                <div id="zenAssetInfo">
                                    <spring:bind path="template.zenAsset">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td nowrap="nowrap">
                                                    <div id="zenAssetDiv" style="display:${showZEN}">
                                                        <input type="text" id="zenAsset" name="${status.expression}"
                                                               value="<c:out value="${template.zenAsset.id}"/>"/>
                                                        <a href="javascript:zenAssetSearch();">
                                                            <img src="<c:url value='/images/find2.gif'/>" alt="" align="bottom">
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </spring:bind>
                                </div>

                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>

        <div dojotype="dijit.TitlePane" title="Custom Fields">
            <div class="columns">
                <div class="colx2-left">
                    <table id="cfTable_0" class="form-table"></table>
                </div>
                <div class="colx2-right">
                    <table id="cfTable_1" class="form-table"></table>
                </div>
            </div>
        </div>

        <%--Description--%>
        <div dojotype="dijit.TitlePane" title="<spring:message code="ticket.description"/>">
            <table class="form-table">
                <tr>
                    <td>
                        <form:label path="subject">
                            <spring:message code="ticket.subject"/>
                        </form:label>
                    </td>
                    <td>
                        <spring:bind path="template.subject">
                            <input type="text" name="${status.expression}" id="${status.expression}" style="width: 400px"
                                   value="${status.value}" dojoType="dijit.form.ValidationTextBox" trim="true"/>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="cc">
                            <spring:message code="ticket.cc"/>
                        </form:label>
                    </td>
                    <td>
                        <spring:bind path="template.cc">
                            <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                                   dojoType="dijit.form.ValidationTextBox" trim="true" style="width: 400px"
                                   regExpGen="dojox.validate.regexp.emailAddressList"
                                   invalidMessage="<spring:message code="mail.validation.address"/>"/>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="bc">
                            <spring:message code="ticket.bcc"/>
                        </form:label>
                    </td>
                    <td>
                        <spring:bind path="template.bc">
                            <input type="text" name="${status.expression}" id="${status.expression}" value="${status.value}"
                                   dojoType="dijit.form.ValidationTextBox" trim="true" style="width: 400px"
                                   regExpGen="dojox.validate.regexp.emailAddressList"
                                   invalidMessage="<spring:message code="mail.validation.address"/>"/>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="note">
                            <spring:message code="ticket.note"/>
                        </form:label>
                    </td>
                    <td>
                        <spring:bind path="template.note">
                            <textarea rows="" cols="" name="${status.expression}" id="${status.expression}" dojoType="dijit.form.Textarea"
                                      style="width: 400px; font: 11px Arial, Myriad, Tahoma, Verdana, sans-serif;">${status.value}</textarea>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td>
                        <form:label path="attachments">
                            <spring:message code="ticket.attachments"/>
                        </form:label>
                    </td>
                    <td colspan="3" style="font-weight:normal;">
                        <div id="attachmentsDiv">
                            <c:forEach var="attach" items="${template.attachments}" varStatus="status">
                                <div id="attachmentDiv_${attach.id}">
                                    <a href="<c:url value="/ticket/attachment/handleDownload.glml" ><c:param name="aid" value="${attach.id}" /></c:url>">
                                        <c:out value="${attach.fileName}"/>
                                    </a>
                                    <a href="javascript://" onclick="deleteAttachment('${template.id}', '${attach.id}');" title="<spring:message code="ticketTemplate.deleteAttachment"/>">
                                        <img alt="<spring:message code="ticketTemplate.deleteAttachment"/>" src="<c:url value="/images/delete.gif" />"/>
                                    </a>
                                </div>
                            </c:forEach>
                            <div id="fileLoader_EditDiv">
                                    <%--<span style="font-size: small;">(<spring:message code="global.fileSize"/>)</span>--%>
                                <input type="file" name="attchmnt[0]" size="50" onchange="fileSelected();"/><br/>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

    </form:form>

</div>

<%@ include file="/WEB-INF/jsp/tickets/customFieldTemplates.jsp" %>
