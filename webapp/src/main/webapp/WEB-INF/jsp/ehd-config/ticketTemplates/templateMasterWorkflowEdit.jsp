<%--@elvariable id="master" type="net.grouplink.ehelpdesk.domain.TicketTemplateMaster"--%>
<%--@elvariable id="statuses" type="java.util.List<net.grouplink.ehelpdesk.domain.Status>"--%>
<%--@elvariable id="unusedTTs" type="java.util.List"--%>
<%--@elvariable id="launchedWorkflowSteps" type="java.util.Map<net.grouplink.ehelpdesk.domain.WorkflowStep, java.lang.Boolean>"--%>

<%@ include file="/WEB-INF/jsp/include.jsp" %>

<div dojoType="dijit.layout.BorderContainer" design="sidebar">

    <div dojoType="dijit.layout.ContentPane" id="workflowTreePane"
         region="leading" minSize="20" style="width:20%;" splitter="true">
        <form class="form">
        <span class="label"><spring:message code="ticketTemplate.title"/></span>
        </form>
        <div dojotype="dojo.dnd.Source" style="border:1px solid lightgray;">
            <c:forEach var="tt" items="${unusedTTs}">
                <div id="${tt.id}" class="dojoDndItem">${tt.templateName}</div>
            </c:forEach>
            &nbsp;
        </div>
    </div>

    <div id="workflowPane" dojoType="dijit.layout.ContentPane" region="center">
        <div id="templateMasterWorkflowEdit">
            <form id="workflowForm" name="workflowForm" action="<c:url value="/config/templateMasterWorkflow.glml"/>"
                  method="post" dojotype="dijit.form.Form">

                <input type="hidden" name="tmId" id="tmId" value="${master.id}"/>
                <input type="hidden" name="action" id="action" value="save"/>
                <input type="hidden" name="wsId" id="wsId" value=""/>
                <c:forEach var="wfs" items="${master.workflowSteps}">
                    <input type="hidden" name="wfstepids" id="wfstepids" value="${wfs.id}"/>
                </c:forEach>
                <div id="hiddenStepFields" style="display:none">
                </div>

                <spring:bind path="master.enableWorkflow">
                    <input type="checkbox" dojoType="dijit.form.CheckBox" name="${status.expression}" id="enableWorkflowCheckBox" value="true"
                            <c:if test="${status.value eq true}">
                                checked="checked"
                            </c:if>
                            /><label for="enableWorkflowCheckBox">Enable Workflow</label>
                    <input type="hidden" name="_${status.expression}"/>
                </spring:bind>

                <br/><br/>

                <div style="margin-bottom:15px;">
                    <spring:message code="workflowStep.instruction"/>
                </div>

                <c:forEach var="workflowStep" items="${master.workflowSteps}" varStatus="loopStatus">
                    <div id="step${workflowStep.stepNumber}Div">
                        <table cellpadding="0" cellspacing="0">
                            <tr style="background-color:lightgray">
                                <td style="font-weight:bold; padding:3px;">
                                    <spring:message code="workflowStep.step" arguments="${workflowStep.stepNumber}"/>
                                </td>
                                <td align="right" style="padding:3px;">
                                    <c:if test="${loopStatus.last}">
                                        <a href="javascript:addWorkFlowStep();">
                                            <img alt="<spring:message code="workflowStep.new"/>"
                                                 src="<c:url value="/images/addButton.gif" />"/>
                                        </a>
                                    </c:if>

                                    <c:if test="${not loopStatus.first && empty launchedWorkflowSteps[workflowStep]}">
                                        <a href="javascript://" onclick="deleteWorkFlowStep(${workflowStep.id});"
                                           title="<spring:message code="workflowStep.delete"/>">
                                            <img alt="" src="<c:url value="/images/remove_32.gif" />"
                                                 style="width:16px;height:16px"/>
                                        </a>
                                    </c:if>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div dojotype="dojo.dnd.Source" id="dndCont_${workflowStep.id}"
                                         style="width:500px; height:100px; border:1px solid lightgray;overflow-y:auto;">
                                        <c:forEach var="tt" items="${workflowStep.ticketTemplates}">
                                            <div id="${tt.id}" class="dojoDndItem">${tt.templateName}</div>
                                        </c:forEach>
                                    </div>
                                </td>
                            </tr>
                            <c:if test="${!loopStatus.last}">
                                <tr>
                                    <td colspan="2" style="padding:3px;">
                                        <spring:message code="workflowStep.status"/> :
                                        <select name="wsStatus_${workflowStep.id}" id="wsStatus_${workflowStep.id}"
                                                dojotype="dijit.form.FilteringSelect">
                                            <c:forEach var="status" items="${statuses}">
                                                <option value="${status.id}"
                                                    <c:if test="${workflowStep.stepCompletedStatus.id eq status.id}">
                                                        selected="selected"
                                                    </c:if>>
                                                    ${status.name}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <br/>
                </c:forEach>

                <button type="submit" id="wfSaveButton" onclick="saveWorkflowForm(); return false;">
                    <spring:message code="global.save"/>
                </button>

            </form>

        </div>
    </div>

</div>
