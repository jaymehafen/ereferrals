<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="emailToTicketConfig" type="net.grouplink.ehelpdesk.domain.EmailToTicketConfig"--%>
<%--@elvariable id="saved" type="java.lang.Boolean"--%>
<%--@elvariable id="groups" type="java.util.List"--%>
<%--@elvariable id="priorities" type="java.util.List"--%>
<%--@elvariable id="locations" type="java.util.List"--%>
<body>

<c:choose>
    <c:when test="${emailToTicketConfig.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/config/emailToTicket/edit.glml"/>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/config/emailToTicket/edit.glml"><c:param name="e2tId" value="${emailToTicketConfig.id}"/></c:url>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="emailToTicketConfig" cssClass="form" method="${method}" action="${action}">
<div id="messages" class="no-margin">
<spring:hasBindErrors name="emailToTicketConfig">
    <p id="e2tErrors" class="message error no-margin">
        <form:errors path="*"/>
    </p>
</spring:hasBindErrors>
<c:if test="${not empty testSuccess}">
    <p class="message success no-margin">
        <c:out value="${testSuccess}"/>
    </p>
</c:if>
<c:if test="${not empty testError}">
    <p class="message error no-margin">
        <c:out value="${testError}"/>
    </p>
</c:if>
</div>
<br>
<div class="columns">
    <div class="colx2-left">
        <fieldset>
            <legend><spring:message code="mailconfig.serverFields"/></legend>
            <p>
                <form:label path="serverName"><spring:message code="mailconfig.host"/></form:label>
                <form:input path="serverName">
                    <jsp:attribute name="cssClass">
                        full-width <spring:bind path="emailToTicketConfig.serverName"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                    </jsp:attribute>
                </form:input>
            </p>

            <div class="columns">
                <div class="colx3-left">
                    <div>
                        <form:label path="serverType"><spring:message code="mailconfig.serverType"/></form:label>
                        <ul class="checkable-list">
                            <li>
                                <form:radiobutton path="serverType" value="1"/>
                                <form:label path="serverType" cssClass="inline"><spring:message code="mailconfig.imap"/></form:label>
                            </li>
                            <li>
                                <form:radiobutton path="serverType" value="2"/>
                                <form:label path="serverType" cssClass="inline"><spring:message code="mailconfig.pop"/></form:label>
                            </li>
                        </ul>

                    </div>
                </div>
                <div id="imapFolderDiv" class="colx2-right-double">
                    <p>
                        <form:label path="imapFolder"><spring:message code="mailconfig.imapFolder"/></form:label>
                        <form:input path="imapFolder"/>
                    </p>
                </div>
            </div>

            <p id="e2tSecurity">
                <form:label path="security"><spring:message code="mailconfig.security"/></form:label>
                <form:radiobutton path="security" value="none"/>
                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.none"/></form:label>

                <form:radiobutton path="security" value="tlsIfAvailable"/>
                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.tlsIfAvailable"/></form:label>

                <form:radiobutton path="security" value="tls"/>
                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.tls"/></form:label>

                <form:radiobutton path="security" value="ssl"/>
                <form:label path="security" cssClass="inline"><spring:message code="mailconfig.security.ssl"/></form:label>
            </p>
            <p>
                <form:label path="port"><spring:message code="mailconfig.port"/></form:label>
                <form:input path="port" id="e2tPort">
                    <jsp:attribute name="cssClass">
                        full-width <spring:bind path="emailToTicketConfig.port"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                    </jsp:attribute>
                </form:input>
            </p>
            <p>
                <form:label path="userName"><spring:message code="mailconfig.userName"/></form:label>
                <form:input path="userName">
                    <jsp:attribute name="cssClass">
                        full-width <spring:bind path="emailToTicketConfig.userName"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                    </jsp:attribute>
                </form:input>
            </p>
            <p>
                <form:label path="password"><spring:message code="mailconfig.password"/></form:label>
                <form:password path="password" showPassword="true" cssClass="full-width"/>
            </p>
            <p>
                <form:label path="replyDisplayName"><spring:message code="mailconfig.replyName"/></form:label>
                <form:input path="replyDisplayName" cssClass="full-width"/>
            </p>
            <p>
                <form:label path="replyEmailAddress"><spring:message code="mailconfig.replyAddress"/></form:label>
                <form:input path="replyEmailAddress">
                    <jsp:attribute name="cssClass">
                        full-width <spring:bind path="emailToTicketConfig.replyEmailAddress"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                    </jsp:attribute>
                </form:input>
            </p>
        </fieldset>
    </div>
    <div class="colx2-right">
        <fieldset>
            <legend><spring:message code="mailconfig.ehdFields"/></legend>
            <div class="with-margin">
                <form:label path="nonEhdUserHandlingMethod"><spring:message code="mailConfig.emailsFromNonUsers"/></form:label>
                <ul class="checkable-list">
                    <li>
                        <form:radiobutton path="nonEhdUserHandlingMethod" value="1"/>
                        <form:label path="nonEhdUserHandlingMethod" cssClass="inline"><spring:message code="mailConfig.rejectMail"/></form:label>
                    </li>
                    <li>
                        <form:radiobutton path="nonEhdUserHandlingMethod" value="2"/>
                        <form:label path="nonEhdUserHandlingMethod" cssClass="inline"><spring:message code="mailConfig.anonymousAcount"/></form:label>
                    </li>
                    <li>
                        <form:radiobutton path="nonEhdUserHandlingMethod" value="3"/>
                        <form:label path="nonEhdUserHandlingMethod" cssClass="inline"><spring:message code="mailConfig.createAccount"/></form:label>
                    </li>
                </ul>
            </div>
            <p>
                <form:label path="location"><spring:message code="mailconfig.location"/></form:label>
                <form:select path="location">
                    <jsp:attribute name="cssClass">
                        <spring:bind path="emailToTicketConfig.location"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                    </jsp:attribute>
                    <jsp:body>
                        <form:option value=""/>
                        <form:options items="${locations}" itemLabel="name" itemValue="id"/>
                    </jsp:body>
                </form:select>
            </p>
            <p>
                <form:label path="group"><spring:message code="mailconfig.group"/></form:label>
                <form:select path="group">
                    <jsp:attribute name="cssClass">
                        <spring:bind path="emailToTicketConfig.group"><c:if test="${not empty status.errorMessage}">error</c:if></spring:bind>
                    </jsp:attribute>
                    <jsp:body>
                        <form:option value=""/>
                        <form:options items="${groups}" itemLabel="name" itemValue="id"/>
                    </jsp:body>
                </form:select>
            </p>
            <p>
                <form:label path="ticketPriority"><spring:message code="mailconfig.priority"/></form:label>
                <form:select path="ticketPriority">
                    <form:option value=""/>
                    <form:options items="${priorities}" itemLabel="name" itemValue="id"/>
                </form:select>
            </p>
            <p>
                <form:label path="localeString"><spring:message code="mailconfig.localeString"/></form:label>
                <spring:bind path="emailToTicketConfig.localeString">
                    <tags:localeChooser name="${status.expression}"
                                       id="${status.expression}"
                                       selectedLocale="${status.value}">
                    </tags:localeChooser>
                </spring:bind>
            </p>
            <p>
                <form:checkbox path="sendDefaultEmail"/>
                <form:label path="sendDefaultEmail" cssClass="inline"><spring:message code="mailconfig.defaultMail"/></form:label>
            </p>
        </fieldset>
    </div>
</div>
<p id="buttons">
    <button id="testButton" type="button"><spring:message code="mailconfig.testConfig"/></button>
    <button type="submit" name="save"><spring:message code="global.save"/></button>
    <button class="cancelBtn" type="button" name="cancel"><spring:message code="global.cancel"/></button>
</p>
</form:form>
</body>
