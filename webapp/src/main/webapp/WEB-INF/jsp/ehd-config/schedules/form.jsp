<%@ include file="/WEB-INF/jsp/include.jsp" %>

<c:choose>
    <c:when test="${schedule.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/config/groups/${group.id}/schedules"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/config/groups/${group.id}/schedules/${schedule.id}"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="schedule" data-group-id="${group.id}" cssClass="form with-padding" cssStyle="width:500px;height:532px;overflow:auto" method="${method}" action="${action}">
    <spring:hasBindErrors name="schedule">
        <p id="errors" class="message error no-margin">
            <form:errors path="*"/>
        </p>
    </spring:hasBindErrors>
    <fieldset>
        <p>
            <form:label path="name"><spring:message code="scheduler.taskName"/></form:label>
            <form:input path="name" style="width:100%" dojoType="dijit.form.TextBox" maxlength="255"/>
        </p>
        <div class="columns">
            <div class="colx2-left">
                <form:label path="category"><spring:message code="ticket.category"/></form:label>
                <input name="category" id="categorySelect" value="${schedule.category.id}"/>
            </div>
            <div class="colx2-right">
                <form:label path="categoryOption"><spring:message code="ticket.categoryOption"/></form:label>
                <input name="categoryOption" id="categoryOptionSelect" value="${schedule.categoryOption.id}"/>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <div class="columns">
            <div class="colx2-left">
                <form:label path="statuses"><spring:message code="scheduler.statuses"/></form:label>
                <form:select path="statuses" multiple="true" cssClass="chosen full-width">
                    <form:options items="${statusesNonClosed}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
            <div class="colx2-right">
                <form:label path="priorities"><spring:message code="scheduler.priorities"/></form:label>
                <form:select path="priorities" multiple="true" cssClass="chosen full-width">
                    <form:options items="${priorities}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <div class="columns">
            <div class="colx2-left">
                <form:label path="elapsedTimeDigit"><spring:message code="scheduler.elapsedTime"/></form:label>
                <form:input path="elapsedTimeDigit" style="width:3em" dojoType="dijit.form.TextBox"/>
                <form:select path="elapsedTimeUnit" dojoType="dijit.form.FilteringSelect" style="width:9em">
                    <form:option value="mins"><spring:message code="scheduler.time.mins"/></form:option>
                    <form:option value="hour"><spring:message code="scheduler.time.hour"/></form:option>
                    <form:option value="days"><spring:message code="scheduler.time.days"/></form:option>
                    <form:option value="week"><spring:message code="scheduler.time.week"/></form:option>
                </form:select>
            </div>
            <div class="colx2-right">
                <form:label path="fromModified"><spring:message code="scheduler.from"/></form:label>
                <form:radiobutton id="from_1" path="fromModified" value="false" dojoType="dijit.form.RadioButton"/><label class="inline" for="from_1"><spring:message code="scheduler.time.fromCreated"/></label>
                <br><br>
                <form:radiobutton id="from_2" path="fromModified" value="true" dojoType="dijit.form.RadioButton"/><label class="inline" for="from_2"><spring:message code="scheduler.time.fromModified"/></label>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <div class="columns">
            <div class="colx2-left">
                <form:checkbox path="changeStatusAction" dojoType="dijit.form.CheckBox"/>
                <form:label path="changeStatusAction" cssClass="inline"><spring:message code="scheduler.action.changeStatus"/></form:label>
            </div>
            <div class="colx2-right">
                <form:select id="changeStatus" path="changeStatus" dojoType="dijit.form.FilteringSelect" style="width:100%">
                    <form:options items="${statusesAll}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="columns">
            <div class="colx2-left">
                <form:checkbox path="changePriorityAction" dojoType="dijit.form.CheckBox"/>
                <form:label path="changePriorityAction" cssClass="inline"><spring:message code="scheduler.action.changePriority"/></form:label>
            </div>
            <div class="colx2-right">
                <form:select id="changePriority" path="changePriority" dojoType="dijit.form.FilteringSelect" style="width:100%">
                    <form:options items="${priorities}" itemLabel="name" itemValue="id"/>
                </form:select>
            </div>
        </div>
        <div class="columns">
            <div class="colx2-left">
                <form:checkbox path="sendMail" dojoType="dijit.form.CheckBox"/>
                <form:label path="sendMail" cssClass="inline"><spring:message code="scheduler.action.sendMail"/></form:label>
            </div>
        </div>
    </fieldset>
    <div id="mailSettings" class="with-margin">
        <fieldset>
            <div class="inline-small-label with-margin">
                <p class="small-margin">
                    <label><spring:message code="collaboration.appointment.to"/></label>
                    <form:checkbox path="mailAssignedTo" dojoType="dijit.form.CheckBox"/>
                    <form:label path="mailAssignedTo" cssClass="inline"><spring:message code="scheduler.action.mail.assignedTo"/></form:label>
                    &nbsp;
                    <form:checkbox path="mailContact" dojoType="dijit.form.CheckBox"/>
                    <form:label path="mailContact" cssClass="inline"><spring:message code="scheduler.action.mail.contact"/></form:label>
                </p>
                <p class="small-margin">
                    <form:label path="mailCC"><spring:message code="mail.cc"/></form:label>
                    <form:input path="mailCC" dojoType="dijit.form.TextBox" maxlength="255" style="width:100%"/>
                </p>
                <p class="small-margin">
                    <form:label path="mailBC"><spring:message code="mail.bcc"/></form:label>
                    <form:input path="mailBC" dojoType="dijit.form.TextBox" maxlength="255" style="width:100%"/>
                </p>
                <p class="small-margin">
                    <form:label path="mailSubject"><spring:message code="mail.subject"/></form:label>
                    <form:input path="mailSubject" dojoType="dijit.form.TextBox" maxlength="255" style="width:100%"/>
                </p>
                <p class="small-margin">
                    <form:label path="mailNoteText"><spring:message code="scheduler.action.mail.message"/></form:label>
                    <form:textarea path="mailNoteText" dojoType="dijit.form.SimpleTextarea" style="width:98%;max-width:98%;"/>
                </p>
                <p>
                    
                    <a href="javascript:toggleShowTags();"><spring:message code="scheduler.showTags"/></a>

                <div id="mailTagsDiv" style="display:none;">

                    <table class="table">

                        <tr>

                            <td><spring:message code="scheduler.ticketNumber"/></td>

                            <td>{TID}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.subject"/></td>

                            <td>{TSUBJ}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.note"/></td>

                            <td>{NOTE} (also {TMSG} for backward compatibility)</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.cc"/></td>

                            <td>{CC}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="scheduler.parentTicketNumber"/></td>

                            <td>{PID}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.category"/></td>

                            <td>{CAT}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.categoryOption"/></td>

                            <td>{CATOP}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.contact"/></td>

                            <td>{CONT}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="scheduler.contactPrimaryPhoneNumber"/></td>

                            <td>{CONT#}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="scheduler.contactEmailAddress"/></td>

                            <td>{CONTEML}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.location"/></td>

                            <td>{LOC}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.priority"/></td>

                            <td>{PRI}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.status"/></td>

                            <td>{STAT}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.assignedTo"/></td>

                            <td>{ASSGN}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.group"/></td>

                            <td>{GRP}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.submittedBy"/></td>

                            <td>{SUBM}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticketFilter.created"/></td>

                            <td>{CRTDATE}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticketFilter.modified"/></td>

                            <td>{MODDATE}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.estimatedDate"/></td>

                            <td>{ESTCOMPDATE}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.workTime"/></td>

                            <td>{WRKTM}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="asset.assetNumber"/></td>

                            <td>{ASSID}</td>

                        </tr>

                        <tr>

                            <td><spring:message code="ticket.link"/></td>

                            <td>{TLINK}</td>

                        </tr>



                    </table>

                </div>
                </p>
            </div>
            <p>
                <label><spring:message code="scheduler.actionFrequency"/></label>
                <form:radiobutton id="frequency_1" path="actionFrequencyOneTime" value="true" dojoType="dijit.form.RadioButton"/>
                <label class="inline" for="frequency_1"><spring:message code="scheduler.actionFrequency.onetime"/></label>
                <br><br>
                <form:radiobutton id="frequency_2" path="actionFrequencyOneTime" value="false" dojoType="dijit.form.RadioButton"/>
                <label class="inline" for="frequency_2"><spring:message code="scheduler.actionFrequency.repeating"/></label>&nbsp;
                <label class="inline"><spring:message code="scheduler.every"/></label>
                <form:input path="actionFrequencyDigit" dojoType="dijit.form.TextBox" style="width:3em"/>
                <form:select path="actionFrequencyUnit" dojoType="dijit.form.FilteringSelect" style="width:9em">
                    <form:option value="mins"><spring:message code="scheduler.time.mins"/></form:option>
                    <form:option value="hour"><spring:message code="scheduler.time.hour"/></form:option>
                    <form:option value="days"><spring:message code="scheduler.time.days"/></form:option>
                    <form:option value="week"><spring:message code="scheduler.time.week"/></form:option>
                </form:select>
            </p>
        </fieldset>
    </div>
    <p>
        <button type="submit"><spring:message code="global.save"/></button>
        <button id="cancel" type="button" name="cancel"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
