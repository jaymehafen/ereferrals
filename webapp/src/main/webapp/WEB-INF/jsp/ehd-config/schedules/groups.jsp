<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <title><spring:message code="scheduler.title"/></title>

    <meta name="configSidebar" content="true">

    <link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">
    <script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            dojo.require("dojo.NodeList-traverse");

            var tabContainer = new dijit.layout.TabContainer({
                style: "height:100%;width:100%",
                persist: true,
                doLayout: false
            }, "mainTabContainer");

            <c:forEach items="${groupList}" var="group">
                <c:if test="${group.active}">
                    var cp = new dijit.layout.ContentPane({
                        id: "group_<c:out value="${group.id}"/>",
                        href: "<c:url value="/config/groups/${group.id}/schedules/index"/>",
                        title: "<c:out value="${group.name}"/>",
                        onDownloadEnd: function() {
                            dojo.query('.addSchedule, .editSchedule').connect("onclick", function(e) {
                                dojo.stopEvent(e);
                                var href = dojo.attr(this, "href");
                                schDlg.set("href", href);
                                schDlg.set("title", dojo.attr(this, "data-form-title"));
                                schDlg.show();
                            });
                        }
                    });
                    tabContainer.addChild(cp);
                </c:if>
            </c:forEach>
            tabContainer.startup();

            $(".delSchedule").live("click", function(e) {
                e.preventDefault();
                dojo.destroy(dojo.query("#flashMessage ul")[0]);
                var href = dojo.attr(this, "href");
                var elToRemove = dojo.query(this).parents("tr")[0];
                var destroyTable = dojo.query("tr", dojo.query(this).parents("tbody")[0]).length == 1;
                if(confirm("<spring:message code="global.confirmdelete" javaScriptEscape="true"/>")) {
                    dojo.xhrDelete({
                        url: href,
                        load: function(response) {
                            dojo.fadeOut({
                                node: elToRemove,
                                onEnd: function() {
                                    if(destroyTable) {
                                        var refNode = dojo.query(elToRemove).parents("div.no-margin")[0];
                                        dojo.create("p", {"class":"with-margin", innerHTML: "<spring:message code="scheduler.noneFound" javaScriptEscape="true"/>"}, refNode, "replace");
                                    } else {
                                        dojo.destroy(elToRemove);
                                    }
                                }
                            }).play();
                        }
                    });
                }
            });

            $(".toggleRunning").live("click", function(e) {
                e.preventDefault();
                var href = dojo.attr(this, "href");
                var link = this;
                dojo.xhrPost({
                    url: href,
                    handleAs: "json",
                    load: function(response) {
                        var controlImg = null;
                        var statusImg = null;
                        if(response["running"]) {
                            controlImg = dojo.create("img", {src: "<c:url value="/images/theme/icons/fugue/control-stop-square.png"/>"});
                            statusImg = dojo.create("img", {src: "<c:url value="/images/circle-ball-dark-antialiased.gif"/>"});
                        } else {
                            controlImg = dojo.create("img", {src: "<c:url value="/images/theme/icons/fugue/control.png"/>"});
                            statusImg = dojo.create("img", {src: "<c:url value="/images/circle-ball-dark-noanim.gif"/>"});
                        }
                        dojo.place(controlImg, link, "only");
                        dojo.place(statusImg, dojo.query(link).parent("span").next("span.statusIcon")[0], "only");
                    }
                })
            });

            function schDlgOnDownloadEnd() {
                dojo.query("#cancel").connect("onclick", function() {
                    schDlg.hide();
                });

                dojo.connect(dojo.byId('schedule'), "submit", function(e) {
                    dojo.stopEvent(e);
                    var form = dojo.byId("schedule");
                    dojo.xhrPost({
                        form: form,
                        load: function(response) {
                            var tempDiv = document.createElement("div");
                            tempDiv.innerHTML = response;
                            var errors = dojo.query("[id=errors]", tempDiv);
                            if(errors.length) {
                                schDlg.set("content", response);
                                schDlgOnDownloadEnd();
                            } else {
                                schDlg.hide();
                                location.href = "<c:url value="/config/groups/schedules"/>";
                            }
                        }
                    });
                });

                var groupId = dojo.attr(dojo.query('form', this.domNode)[0], "data-group-id");

                var categoryStore = new dojox.data.QueryReadStore({
                    url: "<c:url value="/stores/categories.json"/>?incEmpty=true&prevId=" + groupId
                });

                var categorySelectInput = dojo.byId("categorySelect");

                var categorySelect = new dijit.form.FilteringSelect({
                    id: "category",
                    style: "width:100%",
                    store: categoryStore,
                    searchAttr: "label",
                    name: dojo.attr(categorySelectInput, "name"),
                    onChange: function(category) {
                        if(category) {
                            var categoryOptionStore = new dojox.data.QueryReadStore({
                                url: "<c:url value="/stores/catOptions.json"/>?incEmpty=true&prevId=" + category
                            });
                            catOptSelect.set("store", categoryOptionStore);
                            catOptSelect.set("displayedValue", "");
                            catOptSelect.set("disabled", false);
                        } else {
                            catOptSelect.set("displayedValue", "");
                            catOptSelect.set("disabled", true);
                        }
                    }
                }, categorySelectInput);

                var categoryId = dojo.attr(categorySelectInput, "value");
                if(categoryId.length) {
                    categorySelect.store.fetch({
                        query: {id: categoryId},
                        onItem: function(item, request) {
                            categorySelect.set("value", request.store.getValue(item, "id"), false);
                        }
                    });
                }

                var defaultCatOptStore = new dojox.data.QueryReadStore({
                    url: "<c:url value="/stores/catOptions.json"/>"
                });

                var catOptSelectInput = dojo.byId("categoryOptionSelect");

                var catOptSelect = new dijit.form.FilteringSelect({
                    id: "categoryOptions",
                    store: defaultCatOptStore,
                    style: "width:100%",
                    disabled: true,
                    searchAttr: "label",
                    required: false,
                    name: dojo.attr(catOptSelectInput, "name")
                }, catOptSelectInput);

                var catOptId = dojo.attr(catOptSelectInput, "value");
                if(catOptId.length) {
                    catOptSelect.store.fetch({
                        query: {id: catOptId},
                        onItem: function(item, request) {
                            catOptSelect.set("value", request.store.getValue(item, "id"));
                            catOptSelect.set("disabled", false);
                        }
                    });
                }

                $(".chosen").chosen();

                var sendMailCbx = dojo.byId("sendMail1");
                dojo.connect(sendMailCbx, "onclick", function() {
                    toggleBasedOnCbx(sendMailCbx, "mailSettings");
                });
                toggleBasedOnCbx(sendMailCbx, "mailSettings");

                var changeStatusCbx = dojo.byId("changeStatusAction1");
                dojo.connect(changeStatusCbx, "onclick", function() {
                    toggleBasedOnCbx(changeStatusCbx, dijit.byId("changeStatus").domNode);
                });
                toggleBasedOnCbx(changeStatusCbx, dijit.byId("changeStatus").domNode);

                var changePriorityCbx = dojo.byId("changePriorityAction1");
                dojo.connect(changePriorityCbx, "onclick", function() {
                    toggleBasedOnCbx(changePriorityCbx, dijit.byId("changePriority").domNode);
                });
                toggleBasedOnCbx(changePriorityCbx, dijit.byId("changePriority").domNode);
            }

            function toggleBasedOnCbx(cbx, elm) {
                if(dojo.attr(cbx, "checked")) {
                    dojo.style(elm, "display", "block");
                } else {
                    dojo.style(elm, "display", "none");
                }
            }

            var schDlg = new dijit.Dialog({
                id: "scheduleDialog",
                onDownloadEnd: schDlgOnDownloadEnd,
                onShow: function() {
                    //disable scrolling on the body whilst the dialog is showing
                    $("html,body").css("overflow", "hidden");
                },
                onHide: function() {
                    $("html,body").css("overflow", "auto");
                }
            });
        });

    var showTags = false;

       function toggleShowTags() {

           var mailTagsDiv = dojo.byId('mailTagsDiv');

           if (showTags) {

               mailTagsDiv.style.display = 'none';

               showTags = false;

           } else {

               mailTagsDiv.style.display = '';

               showTags = true;

           }

       }
    </script>
</head>
<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="scheduler.title"/></h1>
        <div id="mainTabContainer"></div>
    </div>
</section>
</body>
