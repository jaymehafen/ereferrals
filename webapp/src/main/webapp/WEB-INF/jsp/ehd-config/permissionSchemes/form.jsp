<%@ include file="/WEB-INF/jsp/include.jsp" %>
<body>

<form:form modelAttribute="permissionScheme" cssClass="form" method="post">
    <jsp:attribute name="action"><c:url value="/config/acl/permissionSchemes"><c:param name="dialog" value="true"/></c:url></jsp:attribute>

    <jsp:body>
        <form:hidden path="id" id="schemeId"/>
        <div id="messages" class="no-margin">
            <spring:hasBindErrors name="permissionScheme">
                <p id="bindErrors" class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
        </div>

        <fieldset>
            <legend><spring:message code="acl.newModel"/></legend>
            <p>
                <form:label path="name" cssClass="required"><spring:message code="global.name"/></form:label>
                <c:set var="errMsg">
                    <form:errors path="name"/>
                </c:set>
                <form:input path="name">
                    <jsp:attribute name="cssClass">full-width<c:if test="${not empty errMsg}"> error</c:if></jsp:attribute>
                </form:input>
                <c:remove var="errMsg"/>
            </p>
        </fieldset>

        <p id="buttons">
            <button type="submit" name="save"><spring:message code="global.save"/></button>
            <button class="cancelBtn" type="button" name="cancel"><spring:message code="global.cancel"/></button>
        </p>
    </jsp:body>
</form:form>

</body>
