<%@ include file="/WEB-INF/jsp/include.jsp" %>
<body>

<form:form modelAttribute="permissionActorsForm" cssClass="form" method="post">
    <jsp:attribute name="action"><c:url value="/config/acl/permissionSchemes/${permissionScheme.id}/permissions/${permission.id}/users"><c:param name="dialog" value="true"/></c:url></jsp:attribute>
    <jsp:body>
        <div id="messages" class="no-margin">
            <spring:hasBindErrors name="permissionActorsForm">
                <p id="userPermErrors" class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
        </div>
        <br>

        <fieldset>
            <legend><spring:message code="acl.addUserPermissions"/></legend>
            <p>
                <form:label path="users"><spring:message code="acl.users"/></form:label>
                <form:select path="users" multiple="multiple" cssClass="chosen full-width" id="user-select" items="${users}" itemLabel="displayName" itemValue="id"/>
            </p>
        </fieldset>

        <p id="buttons">
            <button type="submit" name="save"><spring:message code="global.save"/></button>
            <button class="cancelBtn" type="button" name="cancel"><spring:message code="global.cancel"/></button>
        </p>
    </jsp:body>
</form:form>
</body>
