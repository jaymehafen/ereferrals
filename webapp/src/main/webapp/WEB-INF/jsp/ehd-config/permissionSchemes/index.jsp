<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title><spring:message code="acl.permissionModels"/></title>

    <script type="text/javascript">

        dojo.addOnLoad(function() {

            var dlg = new dijit.Dialog({
                title: "Create Permission Scheme",
                style: "width:400px",
                onDownloadEnd: dlgDownloadEnd
            });

            dojo.connect(dojo.byId("addSchemeBtn"), "onclick", function(e) {
                dlg.set("href", "<c:url value="/config/acl/permissionSchemes/new"><c:param name="dialog" value="true"/></c:url>");
                dlg.show();
                e.preventDefault();
            });

            function dlgDownloadEnd() {
                dojo.query("#permissionScheme").connect("submit", function(e) {
                    dojo.stopEvent(e);
                    var form = dojo.byId("permissionScheme");
                    dojo.xhrPost({
                        form: form,
                        load: function(response) {
                            //create a tmp div so we can use dojo.query on the response
                            //to check for errors and either hide the form or leave it up
                            var tempDiv = document.createElement("div");
                            tempDiv.innerHTML = response;
                            var errors = dojo.query("[id=bindErrors]", tempDiv);
                            if (errors.length) {
                                dlg.set("content", response);
                                dlgDownloadEnd();
                            } else {
                                var schemeId = dojo.query("[id=schemeId]", tempDiv)[0].value;
                                location.replace("<c:url value="/config/acl/permissionSchemes/"/>" + schemeId + "/edit");
                            }
                        }
                    });
                });
            }


            $(".cancelBtn").live("click", function() {
                dlg.hide();
            });

            dojo.query(".deleteScheme").connect("click", function(e) {
                dojo.stopEvent(e);
                var schemeId = getSchemeId(this);
                var node = dojo.byId("scheme_" + schemeId);
                dojo.xhrDelete({
                    url: "<c:url value="/config/acl/permissionSchemes/"/>" + schemeId,
                    load: function() {
                        dojo.fadeOut({
                            node: node,
                            onEnd: function() {
                                var table = $(node).parents("table")[0];
                                if(dojo.query('tr[id^="scheme_"]', table).length == 1) {
                                    dojo.fadeOut({
                                        node: table,
                                        onEnd: function() {
                                            dojo.place('<p><spring:message code="acl.noModels"/></p>', table, "replace");
                                        }
                                    }).play();
                                } else {
                                    dojo.destroy(node);
                                }
                            }
                        }).play();
                    }
                });
            });

            function getSchemeId(element) {
                return $(element).parents("tr").attr("id").replace("scheme_", "");
            }

        });
    </script>

</head>

<body>

<section class="grid_8">
    <div class="block-content">
        <h1><spring:message code="acl.permissionModels"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a href="javascript://" id="addSchemeBtn">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <div class="no-margin">
            <table class="table no-margin">
                <thead>
                <tr>
                    <th scope="col"><spring:message code="global.name"/></th>
                    <th scope="col"><spring:message code="acl.groups"/></th>
                    <th scope="col" class="table-actions"><spring:message code="acl.actions"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="scheme" items="${schemes}">
                    <tr id="scheme_${scheme.id}">
                        <td><c:out value="${scheme.name}"/></td>
                        <td>
                            <ul class="keywords">
                                <c:forEach var="group" items="${scheme.groups}">
                                    <li>
                                        <spring:url value="/config/groups/{groupId}/edit" var="groupEditUrl">
                                            <spring:param name="groupId" value="${group.id}"/>
                                        </spring:url>
                                        <a href="${fn:escapeXml(groupEditUrl)}">
                                            <c:out value="${group.name}"/>
                                        </a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </td>
                        <td>
                            <spring:url value="/config/acl/permissionSchemes/{id}/edit" var="editUrl">
                                <spring:param name="id" value="${scheme.id}"/>
                            </spring:url>

                            <a href="${fn:escapeXml(editUrl)}"
                               title="<spring:message code="global.edit"/>">
                                <img src="<c:url value="/images/theme/icons/fugue/pencil.png"/>" alt="">
                            </a>

                            <a href="javascript://" class="deleteScheme"
                               title="<spring:message code="global.delete"/>">
                                <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</section>
</body>
