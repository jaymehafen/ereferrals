<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="dashboards" type="java.util.List"--%>
<%--@elvariable id="showHeader" type="java.lang.Boolean"--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
    <title>
        <spring:message code="assetTracker.configurationPage" text="Asset Tracker Configuration Page"/>
    </title>
    <spring:theme code="css" var="cssfile"/>
    <c:if test="${not empty cssfile}">
        <link rel="stylesheet" href="<c:url value="${cssfile}"/>" type="text/css">
    </c:if>
</head>
<body>

<div id="bodyContent" style="padding: 10px;">
    <strong>ZENworks 10 Configuration Page</strong>
    <br/><br/>

    <p>
        ZENworks 10 Integration is not available in the free edition of eReferrals. To upgrade to <i>eReferrals</i>&trade;'s
        Enterprise edition, or for more information, please contact a Sales Representative at <a href="mailto:sales@grouplink.net">sales@grouplink.net</a> or
        801-335-0702.
    </p>

    <p>
        eReferrals&reg; features integration with the powerful and effective software and hardware asset
        management solution, ZENworks 10. With this integration you can search ZEN assets directly from help desk
        tickets, run and save reports grouped by ZENworks assets, and tie ZEN assets to help desk tickets. Other
        opportunities with this integration include:
    </p>
    <ul>
        <li>
            Increasing efficiency and productivity of your IT staff by automating processes, introducing consistency,
            reducing errors, and enabling redistribution of resources to deliver increased value.
        </li>
        <li>
            Eliminating the costs associated with an additional database as eReferrals integrates with current
            ZENworks databases.
        </li>
        <li>
            Creating an asset audit trail that can be used for future analysis and reports to make better decisions
            about asset purchases and the man hours needed to maintain them.
        </li>
        <li>
            Reduce support time by launching remote control sessions for ZENworks assets from your eReferrals referral.
        </li>
        <li>
            Providing troubled asset reports around faulty hardware and software so you can react before hardware and
            software warranties expire and know how frequently assets are having trouble.
        </li>
    </ul>

    <p>To view the capability of eReferrals' ZENworks 10 integration, watch the video below.</p>

    <p>
        <object width="500" height="405">
            <param name="movie"
                   value="http://www.youtube.com/v/-_Vm24yRnB4&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"/>
            <param name="allowFullScreen" value="true"/>
            <param name="allowscriptaccess" value="always"/>
            <embed src="http://www.youtube.com/v/-_Vm24yRnB4&amp;hl=en_US&amp;fs=1?rel=0&amp;border=1"
                   type="application/x-shockwave-flash" allowscriptaccess="always"
                   allowfullscreen="true" width="500" height="405"></embed>
        </object>
    </p>


</div>
</body>
</html>
