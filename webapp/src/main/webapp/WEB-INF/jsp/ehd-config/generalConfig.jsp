<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="exampleBaseUrl" type="java.lang.String"--%>
<head>
    <meta name="configSidebar" content="true">
    <title>
        <spring:message code="generalConfig.title"/>
    </title>

    <script type="text/javascript" src="<c:url value="/dwr/interface/TicketFullTextSearchService.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/dwr/engine.js"/>"></script>
    <script type="text/javascript">
        dojo.require("dijit.ProgressBar");
        dojo.require("dojo.parser");

        var timerIsOn = false;
        function doReindex() {
            if (!timerIsOn) {
                timerIsOn = true;
                dojo.attr(dojo.byId("reindexTicketsBtn"), "disabled", true);
                TicketFullTextSearchService.reindexTickets();
                reindexTicketsProgress();
            }
        }

        function reindexTicketsProgress() {
            var timer = setInterval(function() {
                TicketFullTextSearchService.getReindexStatusPercent({
                    callback: function(pct) {
                        var statusPercent = pct + "%";
                        jsProgress.update({progress: statusPercent});
                        if (statusPercent == "100%") {
                            clearInterval(timer);
                            timer = null;
                            timerIsOn = false;
                            dojo.attr(dojo.byId("reindexTicketsBtn"), "disabled", false);
                        }
                    }
                });
            }, 500);
        }

        dojo.addOnLoad(function() {
            dojo.connect(dijit.byId("reindexTicketsBtn"), "onClick", function() {
                doReindex();
            });

            TicketFullTextSearchService.getReindexStatusPercent(function(pct) {
                if(pct != 0) {
                    dojo.attr(dojo.byId("reindexTicketsBtn"), "disabled", true);
                    reindexTicketsProgress();
                }
            });
        });
    </script>
</head>
<body class="tundra">
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="config.menu.gl.general"/></h1>
        <form:form method="post" commandName="generalConfig" cssClass="form" id="generalConfigForm">
            <spring:hasBindErrors name="generalConfig">
                <p id="bindErrors" class="message error no-margin"><form:errors path="*"/></p>
            </spring:hasBindErrors>
            <p>
                <spring:bind path="generalConfig.baseUrl">
                    <label for="${status.expression}"><spring:message code="generalConfig.baseUrl"/></label>
                    <input id="${status.expression}" name="${status.expression}" type="text"
                           value="<c:out value="${status.value}" />" dojoType="dijit.form.TextBox"
                           style="width:25em;" required="true"
                           missingMessage="<spring:message code="generalConfig.baseUrl.required"/>"/>
                </spring:bind>
                (e.g. ${exampleBaseUrl})
            </p>
            <p>
                <spring:bind path="generalConfig.maxUploadSize">
                    <label for="${status.expression}"><spring:message code="generalConfig.maxUpload"/></label>
                    <input type="text" value="${status.value}" dojoType="dijit.form.NumberTextBox"
                           id="${status.expression}" name="${status.expression}" constraints="{min:1,places:0}"
                           style="width:60px;" required="true" invalidMessage="<spring:message code="generalConfig.maxUpload.range"/>"
                            rangeMessage="<spring:message code="generalConfig.maxUpload.range"/>"
                            missingMessage="<spring:message code="generalConfig.maxUpload.required"/>"/>
                </spring:bind>
                MB (<spring:message code="generalConfig.maxUpload.message"/>)
            </p>
            <p>
                <spring:bind path="generalConfig.marqueeText">
                    <label for="${status.expression}"><spring:message code="customizeApplication.marquee"/></label>
                    <input id="${status.expression}" type="text" name="${status.expression}" value="${status.value}"
                            dojoType="dijit.form.TextBox" style="width:25em;">
                </spring:bind>
            </p>
            <p>
                <spring:bind path="generalConfig.sessionTimeoutLength">
                    <label for="${status.expression}"><spring:message code="sessiontimeout.length"/></label>
                    <input id="${status.expression}" type="text" name="${status.expression}" value="${status.value}"
                           constraints="{min:2,max:480,places:0}" dojoType="dijit.form.NumberTextBox" required="true"
                           style="width:60px;" invalidMessage="<spring:message code="sessiontimeout.minimum"/>"
                           rangeMessage="<spring:message code="sessiontimeout.minimum"/>"
                           missingMessage="<spring:message code="generalConfig.timeout.required"/>">
                </spring:bind>
                <spring:message code="sessiontimeout.minutes"/>
            </p>
            <p>
                <form:label path="allowUserRegistration"><spring:message code="generalConfig.allowUserRegistration" text="Allow user registration from the login page"/></form:label>
                <form:checkbox path="allowUserRegistration" cssClass="mini-switch"></form:checkbox>
            </p>
            <p>
                <label for="reindexProgress"><spring:message code="searchConfig.title"/></label>
                <span dojoType="dijit.ProgressBar" style="width:300px" jsId="jsProgress" id="reindexProgress"></span>
                <button id="reindexTicketsBtn" type="button" dojoType="dijit.form.Button"><spring:message code="searchConfig.reindexTickets" /></button>
            </p>
            <p class="grey-bg no-margin">
                <button type="submit"><spring:message code="global.save"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
