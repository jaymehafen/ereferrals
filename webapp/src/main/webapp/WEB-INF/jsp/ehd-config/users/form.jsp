<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="ldapFieldList" type="java.util.List"--%>
<%--@elvariable id="ldapUser" type="net.grouplink.ehelpdesk.domain.LdapUser"--%>

<c:choose>
    <c:when test="${user.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action"><c:url value="/config/users"/></c:set>
        <c:set var="title"><spring:message code="userManagement.createnew"/></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action"><c:url value="/config/users/${user.id}"/></c:set>
        <c:set var="title"><spring:message code="user.edit"/></c:set>
    </c:otherwise>
</c:choose>
<html>
<head>
<meta name="configSidebar" content="true"/>
<title><c:out value="${title}"/></title>

<link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">
<script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

<script type="text/javascript">
dojo.addOnLoad(function () {

    dojo.connect(dojo.byId("userCancel"), "onclick", function () {
        location.href = '<c:url value="/config/users"/>';
    });


    <c:if test="${not empty user.id}">

    var changePasswordDlg = new dijit.Dialog({
        title:"<spring:message code="mailconfig.changePwd"/>",
        href:"<c:url value="/config/users/${user.id}/changePass"><c:param name="dialog" value="true"/></c:url>",
        onDownloadEnd:dlgChangePwdDownloadEnd
    });

    dojo.connect(dojo.byId("changePwd"), "onclick", function (evt) {
        evt.preventDefault();
        changePasswordDlg.show();
    });

    function dlgChangePwdDownloadEnd() {
        var form = dojo.byId("cpwdForm");
        dojo.connect(form, "onsubmit", function (evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form:form,
                load:function (response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        changePasswordDlg.set("content", response);
                        dlgChangePwdDownloadEnd();
                    } else {
                        changePasswordDlg.hide();
                    }
                }
            })
        });

        dojo.connect(dojo.byId("cpwdCancelBtn"), "onclick", function () {
            changePasswordDlg.hide();
        });
    }


    // Phones
    var phoneDialog = new dijit.Dialog({
        onDownloadEnd:dlgPhoneDownloadEnd
    });

    dojo.connect(dojo.byId("addPhoneBtn"), "onclick", function (evt) {
        evt.preventDefault();
        phoneDialog.set("title", "<spring:message code="phone.add"/>");
        phoneDialog.set("href", "<c:url value="/config/users/${user.id}/phones/new"><c:param name="dialog" value="true"/></c:url>");
        phoneDialog.show();
    });

    dojo.query('a[id^="edit_phone_"]').connect("onclick", function (evt) {
        var phoneId = this.id.replace("edit_phone_", "");
        phoneDialog.set("title", "<spring:message code="phone.edit"/>");
        phoneDialog.set("href", "<c:url value="/config/users/${user.id}/phones/"/>" + phoneId + "/edit?dialog=true");
        phoneDialog.show();
    });

    dojo.query('a[id^="del_phone_"]').connect("onclick", function (evt) {
        if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
            var phoneId = this.id.replace("del_phone_", "");
            var elementToRemove = this.parentNode.parentNode;
            dojo.xhrDelete({
                url:"<c:url value="/config/users/${user.id}/phones/"/>" + phoneId,
                handle:function (response, ioargs) {
                    dojo.fadeOut({
                        node:elementToRemove,
                        onEnd:function () {
                            dojo.destroy(elementToRemove);
                        }
                    }).play();
                }
            });
        }
    });

    function dlgPhoneDownloadEnd() {
        var form = dojo.byId("phone");
        dojo.connect(form, "onsubmit", function (evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form:form,
                load:function (response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        phoneDialog.set("content", response);
                        dlgPhoneDownloadEnd();
                    } else {
                        phoneDialog.hide();
                        location.reload();
                    }
                }
            });
        });

        dojo.connect(dojo.byId("phoneCancelBtn"), "onclick", function () {
            phoneDialog.hide();
        });
    }


    // Addresses
    var addressDialog = new dijit.Dialog({
        onDownloadEnd:dlgAddressDownloadEnd
    });

    dojo.connect(dojo.byId("addAddressBtn"), "onclick", function (evt) {
        evt.preventDefault();
        addressDialog.set("title", "<spring:message code="address.add"/>");
        addressDialog.set("href", "<c:url value="/config/users/${user.id}/addresses/new"><c:param name="dialog" value="true"/></c:url>");
        addressDialog.show();
    });

    dojo.query('a[id^="edit_address_"]').connect("onclick", function (evt) {
        var addressId = this.id.replace("edit_address_", "");
        addressDialog.set("title", "<spring:message code="address.edit"/>");
        addressDialog.set("href", "<c:url value="/config/users/${user.id}/addresses/"/>" + addressId + "/edit?dialog=true");
        addressDialog.show();
    });

    dojo.query('a[id^="del_address_"]').connect("onclick", function (evt) {
        if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
            var addressId = this.id.replace("del_address_", "");
            var elementToRemove = this.parentNode.parentNode;
            dojo.xhrDelete({
                url:"<c:url value="/config/users/${user.id}/addresses/"/>" + addressId,
                handle:function (response, ioargs) {
                    dojo.fadeOut({
                        node:elementToRemove,
                        onEnd:function () {
                            dojo.destroy(elementToRemove);
                        }
                    }).play();
                }
            });
        }
    });

    function dlgAddressDownloadEnd() {
        var form = dojo.byId("address");
        dojo.connect(form, "onsubmit", function (evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form:form,
                load:function (response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        addressDialog.set("content", response);
                        dlgAddressDownloadEnd();
                    } else {
                        addressDialog.hide();
                        location.reload();
                    }
                }
            });
        });

        dojo.connect(dojo.byId("addressCancelBtn"), "onclick", function () {
            addressDialog.hide();
        });
    }

    // Group Roles
    var urgDialog = new dijit.Dialog({
        onDownloadEnd:dlgURGDownloadEnd
    });

    dojo.connect(dojo.byId("addRoleBtn"), "onclick", function (evt) {
        evt.preventDefault();
        urgDialog.set("title", "<spring:message code="groupRole.add"/>");
        urgDialog.set("href", "<c:url value="/config/users/${user.id}/groupRoles/new"><c:param name="dialog" value="true"/></c:url>");
        urgDialog.show();
    });

    dojo.query('a[id^="edit_urg_"]').connect("onclick", function (evt) {
        var groupRoleId = this.id.replace("edit_urg_", "");
        urgDialog.set("title", "<spring:message code="groupRole.edit"/>");
        urgDialog.set("href", "<c:url value="/config/users/${user.id}/groupRoles/"/>" + groupRoleId + "/edit?dialog=true");
        urgDialog.show();
    });

    dojo.query('a[id^="del_urg_"]').connect("onclick", function (evt) {
        if (confirm('<spring:message code="global.confirmdelete" javaScriptEscape="true"/>')) {
            var groupRoleId = this.id.replace("del_urg_", "");
            var elementToRemove = this.parentNode.parentNode;
            dojo.xhrDelete({
                url:"<c:url value="/config/users/${user.id}/groupRoles/"/>" + groupRoleId,
                handle:function (response, ioargs) {
                    dojo.fadeOut({
                        node:elementToRemove,
                        onEnd:function () {
                            dojo.destroy(elementToRemove);
                        }
                    }).play();
                }
            });
        }
    });

    function dlgURGDownloadEnd() {
        var form = dojo.byId("urgSetter");
        dojo.connect(form, "onsubmit", function (evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form:form,
                load:function (response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        urgDialog.set("content", response);
                        dlgURGDownloadEnd();
                    } else {
                        urgDialog.hide();
                        location.reload();
                    }
                }
            });
        });

        dojo.connect(dojo.byId("urgCancelBtn"), "onclick", function () {
            urgDialog.hide();
        });
    }

    // Notification Assistant
    var noteAssDlg = new dijit.Dialog({
        title:"<spring:message code="notAssist.title"/>",
        style:"width:400px",
        onDownloadEnd:dlgNotAssistDownloadEnd
    });

    //handle button clicks for modifying notification assistant for the particular UserRoleGroup
    dojo.query('a[id^="notify_"]').connect("onclick", function (evt) {
        evt.preventDefault();
        var urgId = this.id.replace("notify_", "");
        noteAssDlg.set("href", "<c:url value="/config/users/${user.id}/groupRoles/"/>" + urgId + "/notificationAsst/edit?dialog=true");
        noteAssDlg.show();
    });

    function dlgNotAssistDownloadEnd() {
        $(".chosen").chosen();

        var form = dojo.byId("notificationAssistantForm");
        dojo.connect(form, "onsubmit", function (evt) {
            dojo.stopEvent(evt);
            dojo.xhrPost({
                form:form,
                load:function (response) {
                    // check for validation errors
                    var tempDiv = document.createElement("div");
                    tempDiv.innerHTML = response;
                    var errors = dojo.query("[id=bindErrors]", tempDiv);
                    if (errors.length) {
                        noteAssDlg.set("content", response);
                        dlgNotAssistDownloadEnd();
                    } else {
                        noteAssDlg.hide();
                    }
                }
            });
        });

        dojo.connect(dojo.byId("notAssCancelBtn"), "onclick", function () {
            noteAssDlg.hide();
        });
    }

    </c:if>

});
</script>
</head>
<body class="tundra">
<section class="grid_10">
    <div class="block-content">
        <h1><c:out value="${title}"/></h1>
        <form:form modelAttribute="user" cssClass="form" method="${method}" action="${action}">
            <div id="messages" class="no-margin">
                <spring:hasBindErrors name="user">
                    <p id="bindErrors" class="message error no-margin">
                        <form:errors path="*"/>
                    </p>
                </spring:hasBindErrors>
            </div>
            <br/>
            <fieldset>
                <div class="columns">
                    <div class="colx3-left">
                        <p>
                            <form:label path="firstName"><spring:message code="user.firstName"/></form:label>
                            <form:input path="firstName" dojoType="dijit.form.TextBox" maxlength="255"/>
                        </p>

                        <p>
                            <form:label path="lastName"><spring:message code="user.lastName"/></form:label>
                            <form:input path="lastName" dojoType="dijit.form.TextBox" maxlength="255"/>
                        </p>

                        <p>
                            <form:label path="email"><spring:message code="user.email"/></form:label>
                            <form:input path="email" dojoType="dijit.form.ValidationTextBox"
                                        regExpGen="dojox.validate.regexp.emailAddress" maxlength="255"/>
                        </p>

                        <p>
                            <form:label path="active" cssClass="float-left" cssStyle="padding-right: 15px;">
                                <spring:message code="global.active"/>
                            </form:label>
                            <form:checkbox path="active" cssClass="mini-switch with-tip">
                                <jsp:attribute name="title"><spring:message code="global.active.toggle"/></jsp:attribute>
                            </form:checkbox>
                        </p>
                    </div>
                    <div class="colx3-center">
                        <p>
                            <form:label path="loginId"><spring:message code="login.id"/></form:label>
                            <form:input path="loginId" dojoType="dijit.form.TextBox" maxlength="255"/>
                        </p>
                        <c:choose>
                            <c:when test="${user.id eq null}">
                                <p>
                                    <form:label path="changePassword"><spring:message
                                            code="login.password"/></form:label>
                                    <form:password path="changePassword" dojoType="dijit.form.TextBox" maxlength="32"/>
                                </p>

                                <p>
                                    <form:label path="retypePassword"><spring:message
                                            code="login.retypepassword"/></form:label>
                                    <form:password path="retypePassword" dojoType="dijit.form.TextBox" maxlength="32"/>
                                </p>
                            </c:when>
                            <c:otherwise>
                                <p>
                                    <a id="changePwd" class="button" href="javascript://">
                                        <spring:message code="mailconfig.changePwd"/>
                                    </a>
                                </p>
                            </c:otherwise>
                        </c:choose>
                        <ehd:authorizeGlobal permission="global.userComments">
                            <p>
                                <form:label path="comments"><spring:message code="global.comments"/></form:label>
                                <form:textarea path="comments" dojoType="dijit.form.Textarea"
                                               cssStyle="width:15em;font-size: 100%;"/>
                            </p>
                        </ehd:authorizeGlobal>
                    </div>
                    <div class="colx3-right">
                        <p>
                            <form:label path="location"><spring:message code="ticket.location"/></form:label>
                            <form:select path="location" id="userLocationSelectBox" dojoType="dijit.form.FilteringSelect"
                                         items="${locations}" itemLabel="name" itemValue="id"
                                         cssStyle="width:15em;"/>
                        </p>

                        <p>
                            <form:label path="colLoginId"><spring:message
                                    code="collaboration.groupwise.username"/></form:label>
                            <form:input path="colLoginId" dojoType="dijit.form.TextBox" maxlength="255"/>
                        </p>

                        <p>
                            <label for="tmpColPassword"><spring:message
                                    code="collaboration.groupwise.password"/></label>
                            <input name="tmpColPassword" id="tmpColPassword" type="password"
                                   dojoType="dijit.form.TextBox" maxlength="255"/>
                        </p>
                    </div>
                </div>
            </fieldset>


            <c:if test="${fn:length(ldapFieldList) > 0}">
                <fieldset class="collapse">
                    <legend><a href="#"><spring:message code="ldapconfig.information"/></a></legend>
                    <div class="full-width" style="max-height: 200px;overflow: auto;overflow-x: hidden;">
                        <table class="table">
                            <thead></thead>
                            <tbody>
                            <c:forEach items="${ldapFieldList}" var="ldapField">
                                <tr>
                                    <td>
                                        <label>
                                            <c:out value="${ldapField.label}"/>
                                            <c:if test="${empty ldapField.label}">
                                                <c:out value="${ldapField.name}"/>
                                            </c:if>
                                        </label>
                                    </td>
                                    <td>
                                        <c:out value="${ldapUser.attributeMap[ldapField.name]}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </c:if>


            <p class="grey-bg no-margin">
                <button type="submit" id="userSave" name="save"><spring:message code="global.save"/></button>
                <button type="button" id="userCancel" name="cancel"><spring:message code="global.cancel"/></button>
            </p>
        </form:form>
    </div>
</section>

<c:if test="${user.id ne null}">
<section class="grid_3">
    <div class="block-content no-padding">
        <h1><spring:message code="phone.phones"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="addPhoneBtn" href="javascript://">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <table class="table full-width">
            <c:choose>
                <c:when test="${not empty user.phoneList}">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="phone.type"/></th>
                        <th scope="col"><spring:message code="phone.number"/></th>
                        <th scope="col"><spring:message code="phone.ext"/></th>
                        <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${user.phoneList}" var="phone">

                        <tr>
                            <td><c:out value="${phone.phoneType}"/></td>
                            <td><c:out value="${phone.number}"/></td>
                            <td><c:out value="${phone.extension}"/></td>
                            <td class="table-actions">
                                <a id="edit_phone_${phone.id}" title="<spring:message code="global.edit"/>">
                                    <img alt="edit"
                                         src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/>
                                </a>
                                <a id="del_phone_${phone.id}" title="<spring:message code="global.delete"/>"
                                   href="javascript://">
                                    <img alt=""
                                         src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td>
                            <spring:message code="phone.nophones" text="No phones created"/>
                        </td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</section>
<section class="grid_7">
    <div class="block-content no-padding">
        <h1><spring:message code="address.addresses"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="addAddressBtn" href="javascript://">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <table class="table full-width">
            <c:choose>
                <c:when test="${not empty user.addressList}">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="address.type"/></th>
                        <th scope="col"><spring:message code="address.street"/></th>
                        <th scope="col"><spring:message code="address.city"/></th>
                        <th scope="col"><spring:message code="address.state"/></th>
                        <th scope="col"><spring:message code="address.postalCode"/></th>
                        <th scope="col"><spring:message code="address.country"/></th>
                        <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${user.addressList}" var="address">
                        <tr>
                            <td><c:out value="${address.addressType}"/></td>
                            <td><c:out value="${address.line1}"/></td>
                            <td><c:out value="${address.city}"/></td>
                            <td><c:out value="${address.state}"/></td>
                            <td><c:out value="${address.postalCode}"/></td>
                            <td><c:out value="${address.country}"/></td>
                            <td class="table-actions">
                                <a id="edit_address_${address.id}"
                                   title="<spring:message code="global.edit"/>">
                                    <img alt="edit"
                                         src="<c:url value="/images/theme/icons/fugue/pencil.png"/>"/>
                                </a>
                                <a id="del_address_${address.id}"
                                   title="<spring:message code="global.delete"/>"
                                   href="javascript://">
                                    <img alt=""
                                         src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    <tr></tr>
                    </tbody>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td>
                            <spring:message code="address.noaddresses" text="No addresses created"/>
                        </td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</section>

<section class="grid_10">
    <div class="block-content no-padding">
        <h1><spring:message code="group.roles"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="addRoleBtn" href="javascript://">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>

        <table class="table full-width">
            <c:set var="urgRoles" value="false"/>

            <c:forEach items="${user.userRoles}" var="userRole">
                <c:forEach items="${userRole.userRoleGroup}" var="urg">
                    <c:if test="${urg.active}">
                        <c:set var="urgRoles" value="true"/>
                    </c:if>
                </c:forEach>
            </c:forEach>
            <c:choose>
                <c:when test="${urgRoles}">
                    <thead>
                    <tr>
                        <th scope="col"><spring:message code="global.group"/></th>
                        <th scope="col"><spring:message code="user.role"/></th>
                        <th scope="col"><spring:message code="user.backupUser"/></th>
                        <th scope="col" class="table-actions"><spring:message code="global.actions"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${user.userRoles}" var="userRole">
                        <c:forEach items="${userRole.sortedURG}" var="urg">
                            <c:if test="${urg.active}">
                                <tr>
                                    <td><c:out value="${urg.group.name}"/></td>
                                    <td><spring:message code="${urg.userRole.role.name}"/></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${not empty urg.backUpTech.userRole}">
                                                <c:out value="${urg.backUpTech.userRole.user.lastName}, ${urg.backUpTech.userRole.user.firstName}"/>
                                            </c:when>
                                            <c:otherwise>
                                                <spring:message code="ticketList.ticketPool"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="table-actions">
                                        <a id="del_urg_${urg.id}" title="<spring:message code="global.delete"/>"
                                           href="javascript://">
                                            <img alt=""
                                                 src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"/>
                                        </a>
                                        <a id="notify_${urg.id}" title="<spring:message code="notAssist.title"/>"
                                           href="javascript://">
                                            <img alt="" src="<c:url value="/images/wireless.png"/>"
                                                 style="padding-left: 7px;"/>
                                        </a>
                                    </td>
                                </tr>
                            </c:if>
                        </c:forEach>
                    </c:forEach>
                    </tbody>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td>
                            <spring:message code="user.role.noroles" text="No Roles created"/>
                        </td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</section>
</c:if>

</body>
</html>
