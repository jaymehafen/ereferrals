<%@ include file="/WEB-INF/jsp/include.jsp" %>

<c:choose>
    <c:when test="${empty phone.id}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/config/users/${user.id}/phones"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/config/users/${user.id}/phones/${phone.id}"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="phone" cssClass="form" method="${method}" action="${action}">
    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="phone">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <br/>
    <fieldset>
        <p>
            <form:label path="phoneType"><spring:message code="phone.type"/></form:label>
            <form:select path="phoneType" dojoType="dijit.form.Select" cssStyle="width:15em;">
                <form:option value="Office">
                    <jsp:attribute name="label"> <spring:message code="phone.office"/> </jsp:attribute>
                </form:option>
                <form:option value="Fax">
                    <jsp:attribute name="label"> <spring:message code="phone.fax"/> </jsp:attribute>
                </form:option>
                <form:option value="Cell">
                    <jsp:attribute name="label"> <spring:message code="phone.cell"/> </jsp:attribute>
                </form:option>
                <form:option value="Home">
                    <jsp:attribute name="label"> <spring:message code="phone.home"/> </jsp:attribute>
                </form:option>
            </form:select>
        </p>

        <p>
            <form:label path="number"><spring:message code="phone.number"/></form:label>
            <form:input path="number" dojoType="dijit.form.TextBox" size="50" maxlength="255"/>
        </p>

        <p>
            <form:label path="extension"><spring:message code="phone.ext"/></form:label>
            <form:input path="extension" dojoType="dijit.form.TextBox" size="50" maxlength="255"/>
        </p>

    </fieldset>
    <p>
        <button type="submit" id="phoneSaveBtn"><spring:message code="global.save"/></button>
        <button type="button" id="phoneCancelBtn"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
