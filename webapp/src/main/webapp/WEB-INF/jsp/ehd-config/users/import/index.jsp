<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="ldapUserList" type="java.util.List"--%>
<%--@elvariable id="ldapFieldList" type="java.util.List"--%>

<head>
    <meta name="configSidebar" content="true">
    <title><spring:message code="global.importLdapUser"/></title>


    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="<c:url value="/js/jquery.dataTables.min.js"/>"></script>
    <script type="text/javascript">

        dojo.addOnLoad(function () {

            // Setup results table
            <c:if test="${not empty ldapUserList}">
            $.fn.dataTableExt.oStdClasses.sWrapper = 'no-margin last-child';
            $.fn.dataTableExt.oStdClasses.sInfo = 'message no-margin';
            $.fn.dataTableExt.oStdClasses.sLength = 'float-left';
            $.fn.dataTableExt.oStdClasses.sFilter = 'float-right';
            $.fn.dataTableExt.oStdClasses.sPaging = 'sub-hover paging_';
            $.fn.dataTableExt.oStdClasses.sPagePrevEnabled = 'control-prev';
            $.fn.dataTableExt.oStdClasses.sPagePrevDisabled = 'control-prev disabled';
            $.fn.dataTableExt.oStdClasses.sPageNextEnabled = 'control-next';
            $.fn.dataTableExt.oStdClasses.sPageNextDisabled = 'control-next disabled';
            $.fn.dataTableExt.oStdClasses.sPageFirst = 'control-first';
            $.fn.dataTableExt.oStdClasses.sPagePrevious = 'control-prev';
            $.fn.dataTableExt.oStdClasses.sPageNext = 'control-next';
            $.fn.dataTableExt.oStdClasses.sPageLast = 'control-last';

            $('.sortable').each(function (i) {
                // DataTable config
                var table = $(this),
                        oTable = table.dataTable({
                            /*
                             * We set specific options for each columns here. Some columns contain raw data to enable correct sorting, so we convert it for display
                             * @url http://www.datatables.net/usage/columns
                             */
                            aoColumns:[
                                { bSortable:false }, // No sorting for this columns, as it only contains checkboxes
                                { sType:'string' },
                                { sType:'string' },
                                { sType:'string' },
                                { sType:'string' }<c:forEach items="${ldapFieldList}" var="field">,{sType:'string'}</c:forEach>
                            ],

                            /*
                             * Set DOM structure for table controls
                             * @url http://www.datatables.net/examples/basic_init/dom.html
                             */
                            sDom:'<"block-controls"<"controls-buttons"p>>rti<"block-footer clearfix"lf>',

                            /*
                             * Callback to apply template setup
                             */
                            fnDrawCallback:function () {
                                this.parent().applyTemplateSetup();
                            },
                            fnInitComplete:function () {
                                this.parent().applyTemplateSetup();
                            }
                        });

                // Sorting arrows behaviour
                table.find('thead .sort-up').click(function (event) {
                    // Stop link behaviour
                    event.preventDefault();

                    // Find column index
                    var column = $(this).closest('th'),
                            columnIndex = column.parent().children().index(column.get(0));

                    // Send command
                    oTable.fnSort([
                        [columnIndex, 'asc']
                    ]);

                    // Prevent bubbling
                    return false;
                });
                table.find('thead .sort-down').click(function (event) {
                    // Stop link behaviour
                    event.preventDefault();

                    // Find column index
                    var column = $(this).closest('th'),
                            columnIndex = column.parent().children().index(column.get(0));

                    // Send command
                    oTable.fnSort([
                        [columnIndex, 'desc']
                    ]);

                    // Prevent bubbling
                    return false;
                });
            });

            // the datatables plugin keeps dom elements in memory through pagination so you have to use their API to get
            // all form elements to submit.
            dojo.connect(dojo.byId("table_form"), "onsubmit", function(evt){
                dojo.stopEvent(evt);
                var uTable = $('#usersTable').dataTable();
                var uData = $('input', uTable.fnGetNodes()).serialize();

                dojo.xhrPost({
                    url: "<c:url value="/config/users/import/checkedUsers"/>",
                    postData: uData,
                    load:function(response){
                        location.replace("<c:url value="/config/users/import"/>");
                    }
                });
            });

            dojo.connect(dojo.byId("selectAllBtn"), "onclick", function(){
                var cbxs = dojo.query('[name=\"ldapUserCbx\"]');
                dojo.forEach(cbxs, function(cbx){
                    dijit.getEnclosingWidget(cbx).set("checked", true);
                });
            });

            dojo.connect(dojo.byId("deselectAllBtn"), "onclick", function(){
                var cbxs = dojo.query('[name=\"ldapUserCbx\"]');
                dojo.forEach(cbxs, function(cbx){
                    dijit.getEnclosingWidget(cbx).set("checked", false);
                });
            });
            </c:if>
        });
    </script>
</head>
<body class="tundra">
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="global.importLdapUser"/></h1>
        <form:form modelAttribute="userSearch" cssClass="form" method="post">
            <div id="messages" class="no-margin">
                <spring:hasBindErrors name="user">
                    <p id="bindErrors" class="message error no-margin">
                        <form:errors path="*"/>
                    </p>
                </spring:hasBindErrors>
            </div>
            <br/>
            <fieldset>
                <div class="columns">
                    <div class="colx3-left">
                        <p>
                            <form:label path="lastName"><spring:message code="user.lastName"/></form:label>
                            <form:input path="lastName" dojoType="dijit.form.TextBox"/>
                        </p>

                    </div>
                    <div class="colx3-center">
                        <p>
                            <form:label path="firstName"><spring:message code="user.firstName"/></form:label>
                            <form:input path="firstName" dojoType="dijit.form.TextBox"/>
                        </p>
                    </div>
                    <div class="colx3-right">
                        <p>
                            <form:label path="email"><spring:message code="user.email"/></form:label>
                            <form:input path="email" dojoType="dijit.form.TextBox"/>
                        </p>
                    </div>
                </div>
            </fieldset>

            <p class="grey-bg no-margin">
                <button type="submit" id="userSave" name="save"><spring:message code="global.searchLdap"/></button>
            </p>
        </form:form>
    </div>
</section>


<c:if test="${not empty ldapUserList}">
    <section class="grid_10">
        <form class="block-content form" id="table_form" method="post" action="">
        <h1><spring:message code="userManagement.import.results"/></h1>

            <table id="usersTable" class="table sortable no-margin">
                <thead>
                <tr>
                    <th></th>
                    <th scope="col">
                            <span class="column-sort">
                                <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                            </span>
                        <spring:message code="user.lastName"/>
                    </th>
                    <th scope="col">
                            <span class="column-sort">
                                <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                            </span>
                        <spring:message code="user.firstName"/>
                    </th>
                    <th scope="col">
                            <span class="column-sort">
                                <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                            </span>
                        <spring:message code="user.userName"/>
                    </th>
                    <th scope="col">
                            <span class="column-sort">
                                <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                            </span>
                        <spring:message code="user.email"/>
                    </th>
                    <c:forEach items="${ldapFieldList}" var="field">
                        <th scope="col">
                                <span class="column-sort">
                                    <a href="#" title="<spring:message code="global.sortup"/>" class="sort-up"></a>
                                    <a href="#" title="<spring:message code="global.sortdown"/>" class="sort-down"></a>
                                </span>
                            <c:choose>
                                <c:when test="${empty field.label}"> <c:out value="${field.name}"/></c:when>
                                <c:otherwise><c:out value="${field.label}"/></c:otherwise>
                            </c:choose>
                        </th>
                    </c:forEach>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${ldapUserList}" var="user" varStatus="status">
                    <tr class="${status.index%2 ==0? 'odd':'even'}">
                        <td class="th table-check-cell">
                            <input type="checkbox" name="ldapUserCbx" dojotype="dijit.form.CheckBox"
                                   value="${user.URLEncodeDN}">
                        </td>
                        <td><c:out value="${user.sn}"/></td>
                        <td><c:out escapeXml="false" value="${user.givenName}"/></td>
                        <td><c:out escapeXml="false" value="${user.cn}"/></td>
                        <td><c:out escapeXml="false" value="${user.mail}"/></td>
                        <c:forEach items="${user.attributeMap}" var="map">
                            <td><c:out escapeXml="false" value="${map.value}"/></td>
                        </c:forEach>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

            <div class="block-footer" style="margin-top:15px;">
                <img src="<c:url value="/images/theme/icons/fugue/arrow-curve-000-left.png"/>" width="16"
                     height="16" class="picto">
                <a id="selectAllBtn" href="#" class="button"><spring:message code="userManagement.import.selectAll"/></a>
                <a id="deselectAllBtn" href="#" class="button"><spring:message code="userManagement.import.deselectAll"/></a>
                <span class="sep"></span>
                <button type="submit">
                    <spring:message code="userManagement.import.selected"/>
                </button>
            </div>

        </form>
    </section>
</c:if>
</body>
