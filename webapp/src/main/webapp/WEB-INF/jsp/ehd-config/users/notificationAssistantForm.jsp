<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="urgUserList" type="java.util.List<User>"--%>
<%--@elvariable id="backupList" type="java.util.List<net.grouplink.ehelpdesk.domain.UserRoleGroup>"--%>
<%--@elvariable id="urg" type="net.grouplink.ehelpdesk.domain.UserRoleGroup"--%>
<%--@elvariable id="assignmentLabel" type="java.lang.String"--%>

<body>
<form:form modelAttribute="notificationAssistantForm" cssClass="form" method="put">
    <jsp:attribute name="action"><c:url value="/config/users/${user.id}/groupRoles/${urg.id}/notificationAsst">
        <c:param name="dialog" value="true"/></c:url></jsp:attribute>
    <jsp:body>
        <br/>
        <c:if test="${not empty urg.userRole}">
            <h3><spring:message code="OutOfOffice.title"/></h3>
            <fieldset>
                <p>
                    <spring:message code="OutOfOffice.reassign"/> <c:out value="${urg.userRole.user.firstName}"/>
                    <c:out value="${urg.userRole.user.lastName}"/> <spring:message code="ticketSearch.to"/>:
                    <form:select path="backupTech.id" dojoType="dijit.form.Select" cssStyle="width:15em;">
                        <form:option value=""/>
                        <c:forEach items="${backupList}" var="tech">
                            <c:choose>
                                <c:when test="${not empty tech.userRole}">
                                    <form:option value="${tech.id}"
                                                 label="${tech.userRole.user.lastName}, ${tech.userRole.user.firstName}"/>
                                </c:when>
                                <c:otherwise>
                                    <form:option value="${tech.id}">
                                        <spring:message code="ticketList.ticketPool"/>
                                    </form:option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </form:select>
                </p>
            </fieldset>
        </c:if>
        <h3><spring:message code="notAssist.title"/></h3>
        <fieldset>
            <p>
                <form:label path="users">
                    <spring:message code="notAssist.notify" arguments="${assignmentLabel}"/>
                </form:label>
                <form:select path="users" items="${urgUserList}" multiple="multiple" cssClass="chosen full-width"
                             itemValue="id" itemLabel="displayName"/>
                <br/><br/><br/><br/><br/><br/>
            </p>
        </fieldset>

        <p id="buttons">
            <button type="submit" id="notAssSubmitBtn" name="save"><spring:message code="global.save"/></button>
            <button type="button" id="notAssCancelBtn" name="cancel"><spring:message code="global.cancel"/></button>
        </p>
    </jsp:body>
</form:form>
</body>
