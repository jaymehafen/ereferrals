<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%--@elvariable id="location" type="net.grouplink.ehelpdesk.domain.Location"--%>
<%--@elvariable id="customFieldTypes" type="java.util.List"--%>

<c:choose>
    <c:when test="${customField.id eq null}">
        <c:set var="method" value="post"/>
        <c:set var="action">
            <c:url value="/config/customFields"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:when>
    <c:otherwise>
        <c:set var="method" value="put"/>
        <c:set var="action">
            <c:url value="/config/customFields/${customField.id}"><c:param name="dialog" value="true"/></c:url>
        </c:set>
    </c:otherwise>
</c:choose>

<form:form modelAttribute="customField" cssClass="form" method="${method}" action="${action}">

    <div id="messages" class="no-margin">
        <spring:hasBindErrors name="customField">
            <p id="bindErrors" class="message error no-margin">
                <form:errors path="*"/>
            </p>
        </spring:hasBindErrors>
    </div>
    <br/>

    <p class="mini-infos">
        <spring:message code="customField.explanation"/>
    </p>
    <fieldset>
        <div class="columns">
            <div class="colx2-left">
                <p>
                    <form:label path="name" cssClass="required"><spring:message code="customField.name"/></form:label>
                    <form:input path="name" dojoType="dijit.form.TextBox" size="50" maxlength="255"/>
                </p>

                <p>
                    <form:label path="fieldType" cssClass="required"><spring:message code="customField.fieldType"/></form:label>
                    <form:select path="fieldType" cssStyle="width: 15em;" >
                        <form:option value="">&nbsp;</form:option>
                        <form:options items="${customFieldTypes}"/>
                    </form:select>
                </p>

                <p>
                    <form:label path="order"><spring:message code="global.order"/></form:label>
                    <form:input path="order"/>
                </p>

                <p id="required">
                    <form:label path="required"><spring:message code="customField.required"/></form:label>
                    <form:checkbox path="required" cssClass="mini-switch with-tip">
                        <jsp:attribute name="title"><spring:message code="global.active.toggle"/></jsp:attribute>
                    </form:checkbox>
                </p>
            </div>
            <div class="colx2-right">
                <p>
                    <form:label path="location"><spring:message code="ticket.location"/></form:label>
                    <form:select path="location" cssStyle="width: 15em;" dojoType="dijit.form.FilteringSelect">
                        <form:option value="">&nbsp;</form:option>
                        <form:options items="${locations}" itemLabel="name" itemValue="id"/>
                    </form:select>
                </p>

                <p>
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="groupStore" urlPreventCache="true" clearOnClose="true"
                     url="<c:url value="/stores/groups.json"><c:param name="incEmpty" value="true"/></c:url>"></div>
                    <form:label path="group"><spring:message code="ticket.group"/></form:label>
                    <form:input path="group" dojoType="dijit.form.FilteringSelect" store="groupStore" searchAttr="label"/>
                </p>

                <p>
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="categoryStore" urlPreventCache="true" clearOnClose="true" hierarchical="false"
                     url="<c:url value="/stores/categories.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${customField.group.id}"/></c:url>"></div>
                    <form:label path="category"><spring:message code="ticket.category"/></form:label>
                    <form:input path="category" dojoType="dijit.form.FilteringSelect" store="categoryStore" searchAttr="label"/>
                </p>

                <p>
                    <div dojoType="dojo.data.ItemFileReadStore" jsId="catOptStore" urlPreventCache="true" clearOnClose="true"
                     url="<c:url value="/stores/catOptions.json"><c:param name="incEmpty" value="true"/><c:param name="prevId" value="${customField.category.id}"/></c:url>"></div>
                    <form:label path="categoryOption"><spring:message code="ticket.categoryOption"/></form:label>
                    <form:input path="categoryOption" dojoType="dijit.form.FilteringSelect" store="catOptStore" searchAttr="label"/>
                </p>
            </div>
        </div>
    </fieldset>
    <div id="divCustFieldOptions">
        <fieldset>
            <legend><spring:message code="customField.options"/></legend>
            <div id="customFieldOptions">
                <c:forEach items="${customField.options}" varStatus="cfrow">
                    <div id="customFieldOption_${customField.options[cfrow.index].id == null ? 'new' : cfrow.index}" class="customFieldOption small-margin">
                        <form:input id="${cfrow.index}" path="options[${cfrow.index}].displayValue" size="30" maxlength="255"/>
                        <a class="deleteCustomFieldOption" href="javascript:void(0)">
                            <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>" alt="">
                        </a>
                    </div>
                </c:forEach>
            </div>
            <br>
            <p>
                <a id="addNewCustomFieldOption" class="button" href="javascript:void(0)">
                    <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                    <spring:message code="customField.addOption"/>
                </a>
            </p>
        </fieldset>
    </div>
    <p>
        <button type="submit" id="cfSave" name="save"><spring:message code="global.save"/></button>
        <button type="button" id="cfCancel" name="cancel"><spring:message code="global.cancel"/></button>
    </p>
</form:form>
