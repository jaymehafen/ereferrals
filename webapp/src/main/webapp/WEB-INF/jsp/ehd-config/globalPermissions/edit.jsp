<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
    <title>Global Permission Edit</title>

    <link rel="stylesheet" href="<c:url value="/js/chosen/chosen.css"/>" type="text/css">
    <script type="text/javascript" src="<c:url value="/js/chosen/chosen.jquery.js"/>"></script>

    <script type="text/javascript">

        dojo.addOnLoad(function() {
            function dlgDownloadEnd() {
                $(".chosen").chosen();
                dojo.query("#permissionActorsForm").connect("submit", function(e) {
                    dojo.stopEvent(e);
                    var form = dojo.byId("permissionActorsForm");
                    dojo.xhrPost({
                        form: form,
                        load: function(response) {
                            //create a tmp div so we can use dojo.query on the response
                            //to check for errors and either hide the form or leave it up
                            var tempDiv = document.createElement("div");
                            tempDiv.innerHTML = response;
                            var errors = dojo.query("[id=userPermErrors]", tempDiv);
                            if (errors.length) {
                                dlg.set("content", response);
                                dlgDownloadEnd();
                            } else {
                                dlg.hide();
                                location.reload(true);
                            }
                        }
                    });
                });
            }

            var dlg = new dijit.Dialog({
                title: "Add User Permissions",
                style: "width:500px;overflow:visible",
                onDownloadEnd: dlgDownloadEnd
            });


            dojo.connect(dojo.byId("addUserBtn"), "onclick", function(e) {
                dlg.set("href", "<c:url value="/config/acl/globalPermissions/${permission.id}/users/new"><c:param name="dialog" value="true"/></c:url>");
                dlg.show();
                e.preventDefault();
            });

            $(".cancelBtn").live("click", function() {
                dlg.hide();
                roleDlg.hide();
            });

            dojo.query("#users a").connect("click", function(e) {
                dojo.stopEvent(e);
                var userId = getUserId(this);
                dojo.xhrDelete({
                    url: "<c:url value="/config/acl/globalPermissions/${permission.id}/users/"/>" + userId,
                    load: function() {
                        location.reload(true);
                    }

                }).play();
            });

            function getUserId(element) {
                return $(element).parents("li").attr("id").replace("userPerm_", "");
            }

            function dlgRoleDownloadEnd() {
                $(".chosen").chosen();
                dojo.query("#permissionActorsForm").connect("submit", function(e) {
                    dojo.stopEvent(e);
                    var form = dojo.byId("permissionActorsForm");
                    dojo.xhrPost({
                        form: form,
                        load: function(response) {
                            //create a tmp div so we can use dojo.query on the response
                            //to check for errors and either hide the form or leave it up
                            var tempDiv = document.createElement("div");
                            tempDiv.innerHTML = response;
                            var errors = dojo.query("[id=rolePermErrors]", tempDiv);
                            if (errors.length) {
                                dlg.set("content", response);
                                dlgRoleDownloadEnd();
                            } else {
                                roleDlg.hide();
                                location.reload(true);
                            }
                        }
                    });
                });
            }

            var roleDlg = new dijit.Dialog({
                title: "Add User Permissions",
                style: "width:500px;overflow:visible",
                onDownloadEnd: dlgRoleDownloadEnd
            });


            dojo.connect(dojo.byId("addRoleBtn"), "onclick", function(e) {
                roleDlg.set("href", "<c:url value="/config/acl/globalPermissions/${permission.id}/roles/new"><c:param name="dialog" value="true"/></c:url>");
                roleDlg.show();
                e.preventDefault();
            });

            dojo.query("#roles a").connect("click", function(e) {
                dojo.stopEvent(e);
                var roleId = getRoleId(this);
                dojo.xhrDelete({
                    url: "<c:url value="/config/acl/globalPermissions/${permission.id}/roles/"/>" + roleId,
                    load: function() {
                        location.reload();
                    }

                }).play();
            });

            function getRoleId(element) {
                return $(element).parents("li").attr("id").replace("rolePerm_", "");
            }

        });
    </script>

</head>

<body>

<content tag="breadCrumb">
    <ul id="breadcrumb">
        <li><a href="<c:url value="/config/acl/globalPermissions"/>"><spring:message code="acl.globalPermissionsTitle"/></a></li>
        <li><a href="javascript:location.reload();"><spring:message code="acl.globalPermissionEdit"/></a></li></li>
    </ul>
</content>

<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="acl.editGlobal"/></h1>

        <h3 class="small-margin"><spring:message code="acl.permissionName"/>: <spring:message code="${permission.key}.name" text="Translation missing for ${permission.key}"/></h3>

        <div class="message">
            <spring:message code="${permission.key}.description" text="Translation missing for ${permission.key}"/>
        </div>
    </div>
</section>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="acl.users"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="addUserBtn" href="javascript://">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>


        <ul class="simple-list taller-list-items" id="users">
            <c:forEach var="user" items="${globalPermissionEntry.permissionActors.users}">
                <li id="userPerm_${user.id}">
                        ${user.lastName}, ${user.firstName} (${user.loginId})
                            <a title="<spring:message code="global.delete"/>" href="javascript://" class="float-right"><img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"></a>
                </li>
            </c:forEach>
        </ul>
        <c:if test="${empty globalPermissionEntry.permissionActors.users}">
            <spring:message code="acl.noUserPermissions"/>
        </c:if>
    </div>
</section>

<section class="grid_5">
    <div class="block-content">
        <h1><spring:message code="acl.roles"/></h1>

        <div class="block-controls">
            <ul class="controls-buttons">
                <li>
                    <a id="addRoleBtn" href="javascript://">
                        <img src="<c:url value="/images/theme/icons/fugue/plus-circle.png"/>" alt="">
                        <spring:message code="global.createNew"/>
                    </a>
                </li>
            </ul>
        </div>


        <ul class="simple-list taller-list-items" id="roles">
            <c:forEach var="role" items="${globalPermissionEntry.permissionActors.roles}">
                <li id="rolePerm_${role.id}">
                    <spring:message code ="${role.name}"/> <a title="<spring:message code="global.delete"/>" href="javascript://" class="float-right"><img alt="" src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>"></a>
                </li>

            </c:forEach>
        </ul>
    </div>
</section>
</body>
