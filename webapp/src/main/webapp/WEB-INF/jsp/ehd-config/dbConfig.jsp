<%--@elvariable id="dbConfig" type="net.grouplink.ehelpdesk.domain.DatabaseConfig"--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<head>
    <meta name="configSidebar" content="true">
<title>
    <spring:message code="config.menu.sys.db"/>
</title>

<script type="text/javascript" >
    function determineFieldVisibility() {
        if(dojo.byId("dbType").value == "HSQL") {
            dojo.style(dojo.byId("databaseNameField"), "display", "none");
            dojo.style(dojo.byId("usernameField"), "display", "none");
            dojo.style(dojo.byId("passwordField"), "display", "none");
        } else {
            dojo.style(dojo.byId("databaseNameField"), "display", "block");
            dojo.style(dojo.byId("usernameField"), "display", "block");
            dojo.style(dojo.byId("passwordField"), "display", "block");
        }
    }

    dojo.addOnLoad(function() {
        determineFieldVisibility();
        dojo.connect(dojo.byId("dbType"), "onchange", function() {
            determineFieldVisibility();
        });
    });
</script>
    
</head>

<body>
<section class="grid_10">
    <div class="block-content">
        <h1><spring:message code="config.menu.sys.db"/></h1>
        <form:form action="" method="post" commandName="dbConfig" cssClass="form">
            <spring:hasBindErrors name="dbConfig">
                <p class="message error no-margin">
                    <form:errors path="*"/>
                </p>
            </spring:hasBindErrors>
            <p>
                <form:label path="dbType"><spring:message code="databaseConfig.driver"/></form:label>
                <form:select path="dbType">
                    <form:option value="MySQL"><spring:message code="databaseConfig.mysql"/></form:option>
                    <form:option value="Oracle" ><spring:message code="databaseConfig.oracle"/></form:option>
                    <form:option value="Oracle8i" ><spring:message code="databaseConfig.oracle8i"/></form:option>
                    <form:option value="Oracle9i" ><spring:message code="databaseConfig.oracle9i"/></form:option>
                    <form:option value="Oracle10g" ><spring:message code="databaseConfig.oracle10g"/></form:option>
                    <form:option value="Oracle11g" ><spring:message code="databaseConfig.oracle11g"/></form:option>
                    <form:option value="PostgreSQL" ><spring:message code="databaseConfig.postgres"/></form:option>
                    <form:option value="SQLServer" ><spring:message code="databaseConfig.mssql"/></form:option>
                    <form:option value="Sybase" ><spring:message code="databaseConfig.sybase"/></form:option>
                    <form:option value="HSQL" ><spring:message code="databaseConfig.hsql"/></form:option>
                </form:select>
            </p>
            <p>
                <form:label path="serverName"><spring:message code="databaseConfig.serverName"/></form:label>
                <form:input path="serverName" size="60"/>
            </p>
            <p id="databaseNameField">
                <form:label path="databaseName"><spring:message code="databaseConfig.databaseName"/></form:label>
                <form:input path="databaseName" size="60"/>
            </p>
            <p id="usernameField">
                <form:label path="username"><spring:message code="databaseConfig.userName"/></form:label>
                <form:input path="username" size="60"/>
            </p>
            <p id="passwordField">
                <form:label path="password"><spring:message code="databaseConfig.password"/></form:label>
                <form:password path="password" showPassword="true" size="60"/>
            </p>
            <p class="grey-bg no-margin">
                <button type="submit" name="submit"><spring:message code="global.save"/></button>
                <button type="submit" name="testButton"><spring:message code="global.test"/></button>
            </p>
        </form:form>
    </div>
</section>
</body>
