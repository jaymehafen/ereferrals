<%@ include file="/WEB-INF/jsp/include.jsp" %>
<jsp:useBean id="kb" class="net.grouplink.ehelpdesk.domain.KnowledgeBase" scope="request"/>
<jsp:useBean id="urg" class="net.grouplink.ehelpdesk.domain.UserRoleGroup"/>
<jsp:useBean id="ticket" class="net.grouplink.ehelpdesk.domain.Ticket" scope="request"/>
<%--@elvariable id="groupList" type="java.util.List"--%>

<head>
    <title><spring:message code="kb.knowledgeBaseArticle"/></title>

    <script type="text/javascript">
        dojo.require("dijit.Editor");
        dojo.addOnLoad(function () {

            $(".attachment .close-bt-no-js").live("click", function (e) {
                var $li = $(this).parents("li.attachment");
                var url = $(this).attr("data-url");
                if (confirm("<spring:message code="global.confirmdelete"/>")) {
                    dojo.xhrGet({
                        url:url,
                        load:function () {
                            $li.fadeOut(function () {
                                $(this).remove()
                            });
                        }
                    });
                }
            });


        });

        var inputIndex = 1;
        function fileSelected() {
            var newNode = document.createElement('input');
            var breakNode = document.createElement('br');
            var name = 'attchmnt[' + inputIndex + ']';
            var fileLoader = document.getElementById('fileLoader');

            newNode.setAttribute('type', 'file');
            newNode.setAttribute('name', name);
            newNode.setAttribute('id', name);
            newNode.setAttribute('size', '85');
            newNode.setAttribute('onchange', 'fileSelected()');
            fileLoader.appendChild(newNode);
            fileLoader.appendChild(breakNode);
            inputIndex++;

            //for IE to work
            var node = document.getElementById(name);
            if (node.attachEvent) {
                node.attachEvent('onchange', fileSelected);
                node.size = 85;
            }
        }
    </script>
</head>
<body>
<section class="grid_9">
    <div class="block-content">
        <h1><spring:message code="kb.knowledgeBaseArticle"/><c:if test="${not empty kb.id}"> <c:out
                value="${kb.id}"/></c:if></h1>

        <c:if test="${not empty kb.id}">
            <div class="block-controls">
                <ul class="controls-buttons">
                    <li>
                        <a href="<c:url value="/kb/kbView.glml" ><c:param name="kbId" value="${kb.id}"/></c:url>">
                            <img src="<c:url value="/images/theme/icons/fugue/arrow-curve-000-left.png"/>">
                            <spring:message code="kb.viewThisKb"/>
                        </a>
                    </li>
                    <li>
                        <a href="<c:url value="/kbManagement/kbRemove.glml" ><c:param name="kbId" value="${kb.id}" /></c:url>">
                            <img src="<c:url value="/images/theme/icons/fugue/cross-circle.png"/>">
                            <spring:message code="kb.removeKb"/>
                        </a>
                    </li>
                </ul>
            </div>
        </c:if>

        <form action="" method="post" enctype="multipart/form-data" class="form">
            <input type="hidden" name="ticketId" value="<c:out value="${ticket.id}" />"/>

            <p>
                <spring:bind path="kb.group">
                    <label for="${status.expression}"><spring:message code="global.group"/></label>
                    <select id="${status.expression}" name="${status.expression}">
                        <c:forEach items="${groupList}" var="group">
                            <option value="<c:out value="${group.id}" />"
                                    <c:if test="${group.id == status.value or status.value eq ticket.group.id}">selected="selected"</c:if>>
                                <c:out value="${group.name}"/></option>
                        </c:forEach>
                    </select>
                </spring:bind>
                <spring:bind path="kb.isPrivate">
                    <input type="hidden" name="_<c:out value="${status.expression}"/>">
                    <input dojoType="dijit.form.CheckBox" type="checkbox" id="<c:out value="${status.expression}"/>"
                           name="<c:out value="${status.expression}"/>"
                           <c:if test="${status.value}">checked="checked"</c:if>/>
                    <label for="<c:out value="${status.expression}"/>" class="inline"><spring:message
                            code="kb.private"/></label>
                </spring:bind>
            </p>

            <p>
                <spring:bind path="kb.subject">
                    <label for="${status.expression}"><spring:message code="kb.subject"/></label>
                    <input id="${status.expression}" type="text" maxlength="255" name="<c:out value="${status.expression}" />"
                           value="<c:out value="${status.value}" /><c:out value="${ticket.subject}" />"
                           class="full-width">
                </spring:bind>
            </p>

            <p>
                <spring:bind path="kb.problem">
                    <label for="<c:out value="${status.expression}"/>"><spring:message code="kb.problem"/></label>
                    <textarea dojoType="dijit.Editor" id="<c:out value="${status.expression}"/>"
                              name="<c:out value="${status.expression}"/>">
                        <c:out value="${status.value}"/>
                        <c:if test="${not empty ticket.id}">
                            <c:out value="${ticket.note}"/>
                        </c:if>
                    </textarea>
                </spring:bind>
            </p>

            <p>
                <spring:bind path="kb.resolution">
                    <label for="<c:out value="${status.expression}"/>"><spring:message
                            code="kb.resolution"/></label>
                    <textarea dojoType="dijit.Editor" id="<c:out value="${status.expression}"/>"
                              name="<c:out value="${status.expression}"/>">
                        <c:out value="${status.value}"/>
                        <c:if test="${not empty ticket.id}">
                            <c:forEach items="${ticket.ticketHistoryList}" var="thist">
                                <div>
                                    <fmt:formatDate value="${thist.createdDate}" type="both" timeStyle="medium"
                                                    dateStyle="long"/> - <c:out value="${thist.user.firstName}"/>
                                    <c:out value="${thist.user.lastName}"/> - <c:out value="${thist.subject}"/>
                                </div>
                                <c:if test="${not empty thist.note}">
                                    <div>
                                        <c:out value="${thist.note}" escapeXml="false"/>
                                    </div>
                                </c:if>
                                <c:if test="${fn:length(thist.attachments) > 0}">
                                    <div>
                                        <c:set value="${true}" var="first"/>
                                        <c:forEach items="${thist.attachments}" var="attach">
                                            <c:if test="${not first}">- </c:if><a
                                                href="<c:url value="/ticket/attachment/handleDownload.glml" ><c:param name="aid" value="${attach.id}" /></c:url>"><c:out
                                                value="${attach.fileName}"/></a>
                                            <c:set value="${false}" var="first"/>
                                        </c:forEach>
                                    </div>
                                </c:if>
                                <br/>
                            </c:forEach>
                        </c:if>
                    </textarea>
                </spring:bind>
            </p>
            <div>
                <label for="fileLoader"><spring:message code="global.attachFile"/></label>

                <div class="columns">
                    <div class="colx2-left">
                        <div class="box">
                            <ul class="mini-blocks-list">
                                <c:forEach items="${kb.attachments}" var="attach">
                                    <li class="relative attachment">
                                        <a href="<c:url value="/ticket/attachment/handleDownload.glml" ><c:param name="aid" value="${attach.id}" /></c:url>"><c:out
                                                value="${attach.fileName}"/></a>
                                        <span class="close-bt-no-js"
                                              data-url="<c:url value="/kbManagement/kbRemoveAttachment.glml"><c:param name="attachId" value="${attach.id}" /><c:param name="kbId" value="${kb.id}" /></c:url>"></span>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    <div class="colx2-right">
                        <div id="fileLoader" class="small-margin">
                            <input type="file" name="attchmnt[0]" size="85" onchange="fileSelected()"/><br/>
                        </div>
                    </div>
                </div>
            </div>

            <p class="grey-bg no-margin">
                <button type="submit" name="submit"><spring:message code="kb.button.saveView"/></button>
                <button type="submit" name="cancel"><spring:message code="global.cancel"/></button>
            </p>
        </form>
    </div>
</section>
</body>
