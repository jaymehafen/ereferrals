<?xml version="1.0" encoding="UTF-8"?>

<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
                http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
                http://www.springframework.org/schema/context
				http://www.springframework.org/schema/context/spring-context-3.1.xsd
				http://www.springframework.org/schema/mvc
				http://www.springframework.org/schema/mvc/spring-mvc-3.1.xsd">

    <context:component-scan base-package="net.grouplink.ehelpdesk.web.controllers.config" use-default-filters="false">
        <context:include-filter expression="org.springframework.stereotype.Controller" type="annotation"/>
    </context:component-scan>

    <context:annotation-config/>

    <bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter"/>

    <mvc:annotation-driven validator="validator" conversion-service="conversionService"/>

    <mvc:interceptors>
        <ref bean="openSessionInViewInterceptor"/>
        <ref bean="globalPermissionInterceptor"/>
        <ref bean="localeChangeInterceptor"/>
        <ref bean="themeChangeInterceptor"/>
        <ref bean="securityInterceptor"/>
        <ref bean="currentTabInterceptor"/>
        <ref bean="currentUserInterceptor"/>
    </mvc:interceptors>

    <!--RESOLVER-->
    <bean id="viewResolver" class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="requestContextAttribute" value="rc"/>
        <property name="viewClass" value="org.springframework.web.servlet.view.JstlView"/>
        <property name="prefix" value="/WEB-INF/jsp/ehd-config/"/>
        <property name="suffix" value=".jsp"/>
    </bean>

    <!--URL Mappings-->
    <bean id="urlMapping" class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping">
        <property name="alwaysUseFullPath" value="true"/>
        <property name="interceptors">
            <list>
                <ref bean="openSessionInViewInterceptor"/>
                <ref bean="localeChangeInterceptor"/>
                <ref bean="themeChangeInterceptor"/>
                <ref bean="securityInterceptor"/>
                <ref bean="currentTabInterceptor"/>
                <ref bean="currentUserInterceptor"/>
            </list>
        </property>
        <property name="urlMap">
            <map>
                <entry key="/config" value-ref="configIndexController"/>
                <entry key="/config/db/dbConfig.glml" value-ref="databaseController"/>
                <entry key="/config/general/generalConfig.glml" value-ref="generalConfigController"/>
                <entry key="/config/ldap/*.glml" value-ref="ldapController"/>
                <entry key="/config/ldap/ldapConfig.glml" value-ref="ldapConfigController"/>
                <entry key="/config/surveys/management/*.glml" value-ref="surveyManagementController"/>
                <entry key="/config/dashboardList.glml" value-ref="dashboardActionController"/>
                <entry key="/config/dashboardCopy.glml" value-ref="dashboardActionController"/>
                <entry key="/config/dashboardDelete.glml" value-ref="dashboardActionController"/>
                <entry key="/config/dashboardEdit.glml" value-ref="dashboardFormController"/>
                <entry key="/config/widgetEdit.glml" value-ref="widgetFormController"/>
                <entry key="/config/widgetDelete.glml" value-ref="dashboardActionController"/>
                <entry key="/config/templateMasterList.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterEdit.glml" value-ref="templateMasterFormController"/>
                <entry key="/config/templateMasterCopy.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterDelete.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterToggleScheduler.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterAddTemplate.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterDeleteTemplate.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterRenameTemplate.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterAddWorkflowStep.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/templateMasterDeleteWorkflowStep.glml" value-ref="templateMasterActionController"/>
                <entry key="/config/ticketTemplateEdit.glml" value-ref="ticketTemplateFormController"/>
                <entry key="/config/templateMasterRecurrenceEdit.glml"
                       value-ref="templateMasterRecurrenceFormController"/>
                <entry key="/config/templateMasterWorkflow.glml" value-ref="templateMasterWorkflowFormController"/>
                <entry key="/config/reportToggleScheduler.glml" value-ref="reportActionController"/>
                <entry key="/config/search/searchConfig.glml" value-ref="searchConfigController"/>
            </map>
        </property>
    </bean>

    <!--CONTROLLERS-->
    <bean id="configIndexController" class="org.springframework.web.servlet.mvc.ParameterizableViewController">
        <property name="viewName" value="index"/>
    </bean>

    <bean id="ldapConfigController"
          class="net.grouplink.ehelpdesk.web.controllers.config.LdapConfigController">
        <property name="ldapService" ref="ldapService"/>
        <property name="applicationInitializer" ref="applicationInitializer"/>
        <property name="validators">
            <bean class="net.grouplink.ehelpdesk.web.validators.LdapConfigValidator"/>
        </property>
    </bean>

    <bean id="ldapController" class="net.grouplink.ehelpdesk.web.controllers.config.LdapController">
        <property name="ldapService" ref="ldapService"/>
        <property name="ldapFieldService" ref="ldapFieldService"/>
        <property name="methodNameResolver">
            <bean class="org.springframework.web.servlet.mvc.multiaction.PropertiesMethodNameResolver">
                <property name="mappings">
                    <props>
                        <prop key="/ldap/ldapSubFolders.glml">ldapSubFolders</prop>
                        <prop key="/ldap/addLdapField.glml">addLdapField</prop>
                        <prop key="/ldap/removeLdapField.glml">removeLdapField</prop>
                        <prop key="/ldap/modifyLdapField.glml">modifyLdapField</prop>
                        <prop key="/ldap/fetchDns.glml">fetchDns</prop>
                        <prop key="/ldap/ldapFieldSetup.glml">ldapFieldSetup</prop>
                    </props>
                </property>
            </bean>
        </property>
    </bean>

    <bean id="databaseController" class="net.grouplink.ehelpdesk.web.controllers.config.DatabaseFormController">
        <property name="databaseService" ref="databaseHandlerService"/>
    </bean>

    <bean id="generalConfigController" class="net.grouplink.ehelpdesk.web.controllers.config.GeneralConfigController">
        <property name="propertiesService" ref="propertiesService"/>
        <property name="applicationInitializer" ref="applicationInitializer"/>
        <property name="validator">
            <bean class="net.grouplink.ehelpdesk.web.validators.GeneralConfigValidator"/>
        </property>
    </bean>

    <bean id="surveyManagementController"
          class="net.grouplink.ehelpdesk.web.controllers.config.SurveyManagementController">
        <property name="categoryService" ref="categoryService"/>
        <property name="categoryOptionService" ref="categoryOptionService"/>
        <property name="groupService" ref="groupService"/>
        <property name="surveyService" ref="surveyService"/>
        <property name="surveyFrequencyService" ref="surveyFrequencyService"/>
        <property name="surveyQuestionService" ref="surveyQuestionService"/>
        <property name="surveyQuestionResponseService" ref="surveyQuestionResponseService"/>
        <property name="methodNameResolver">
            <bean class="org.springframework.web.servlet.mvc.multiaction.PropertiesMethodNameResolver">
                <property name="mappings">
                    <props>
                        <prop key="/surveys/management/surveyActivationList.glml">surveyActivationList</prop>
                        <prop key="/surveys/management/surveyActivate.glml">surveyActivate</prop>
                        <prop key="/surveys/management/surveyManage.glml">surveyManage</prop>
                    </props>
                </property>
            </bean>
        </property>
    </bean>

    <bean id="dashboardActionController"
          class="net.grouplink.ehelpdesk.web.controllers.config.DashboardActionController">
        <property name="dashboardService" ref="dashboardService"/>
        <property name="widgetService" ref="widgetService"/>
        <property name="licenseService" ref="licenseService"/>
        <property name="methodNameResolver">
            <bean class="org.springframework.web.servlet.mvc.multiaction.PropertiesMethodNameResolver">
                <property name="mappings">
                    <props>
                        <prop key="/dashboardList.glml">listDashboards</prop>
                        <prop key="/dashboardCopy.glml">copyDashboard</prop>
                        <prop key="/dashboardDelete.glml">deleteDashboard</prop>
                        <prop key="/widgetDelete.glml">deleteWidget</prop>
                    </props>
                </property>
            </bean>
        </property>
    </bean>

    <bean id="dashboardFormController" class="net.grouplink.ehelpdesk.web.controllers.config.DashboardFormController">
        <property name="dashboardService" ref="dashboardService"/>
        <property name="validator">
            <bean class="net.grouplink.ehelpdesk.web.validators.DashboardValidator"/>
        </property>
    </bean>

    <bean id="widgetFormController" class="net.grouplink.ehelpdesk.web.controllers.config.WidgetFormController">
        <property name="widgetService" ref="widgetService"/>
        <property name="dashboardService" ref="dashboardService"/>
        <property name="reportService" ref="reportService"/>
        <property name="validator">
            <bean class="net.grouplink.ehelpdesk.web.validators.WidgetValidator"/>
        </property>
    </bean>

    <bean id="templateMasterActionController"
          class="net.grouplink.ehelpdesk.web.controllers.config.TemplateMasterActionController">
        <property name="templateMasterService" ref="templateMasterService"/>
        <property name="recurrenceScheduleService" ref="recurrenceScheduleService"/>
        <property name="groupService" ref="groupService"/>
        <property name="ticketTemplateService" ref="ticketTemplateService"/>
        <property name="licenseService" ref="licenseService"/>
        <property name="permissionService" ref="permissionService"/>
        <property name="methodNameResolver">
            <bean class="org.springframework.web.servlet.mvc.multiaction.PropertiesMethodNameResolver">
                <property name="mappings">
                    <props>
                        <prop key="/templateMasterList.glml">listTemplateMasters</prop>
                        <prop key="/templateMasterCopy.glml">copyTemplateMaster</prop>
                        <prop key="/templateMasterDelete.glml">deleteTemplateMaster</prop>
                        <prop key="/templateMasterToggleScheduler.glml">toggleScheduler</prop>
                        <prop key="/templateMasterAddTemplate.glml">addTemplate</prop>
                        <prop key="/templateMasterDeleteTemplate.glml">deleteTemplate</prop>
                        <prop key="/templateMasterRenameTemplate.glml">renameTemplate</prop>
                    </props>
                </property>
            </bean>
        </property>
    </bean>

    <bean id="reportActionController" class="net.grouplink.ehelpdesk.web.controllers.config.ReportActionController">
        <property name="recurrenceScheduleService" ref="recurrenceScheduleService"/>
        <property name="methodNameResolver">
            <bean class="org.springframework.web.servlet.mvc.multiaction.PropertiesMethodNameResolver">
                <property name="mappings">
                    <props>
                        <prop key="/reportToggleScheduler.glml">toggleScheduler</prop>
                    </props>
                </property>
            </bean>
        </property>
    </bean>

    <bean id="templateMasterFormController"
          class="net.grouplink.ehelpdesk.web.controllers.config.TemplateMasterFormController">
        <property name="templateMasterService" ref="templateMasterService"/>
        <property name="groupService" ref="groupService"/>
        <property name="permissionService" ref="permissionService"/>
        <property name="validator">
            <bean class="net.grouplink.ehelpdesk.web.validators.TicketTemplateMasterValidator"/>
        </property>
    </bean>

    <bean id="ticketTemplateFormController"
          class="net.grouplink.ehelpdesk.web.controllers.config.TicketTemplateFormController">
        <property name="ticketTemplateService" ref="ticketTemplateService"/>
        <property name="ticketPriorityService" ref="ticketPriorityService"/>
        <property name="statusService" ref="statusService"/>
        <property name="locationService" ref="locationService"/>
        <property name="groupService" ref="groupService"/>
        <property name="userService" ref="userService"/>
        <property name="categoryOptionService" ref="categoryOptionService"/>
        <property name="categoryService" ref="categoryService"/>
        <property name="userRoleGroupService" ref="userRoleGroupService"/>
        <property name="assetService" ref="assetService"/>
        <property name="zenAssetService" ref="zenAssetService"/>
        <property name="customFieldsService" ref="customFieldsService"/>
    </bean>

    <bean id="templateMasterRecurrenceFormController"
          class="net.grouplink.ehelpdesk.web.controllers.config.TemplateMasterRecurrenceFormController">
        <property name="templateMasterService" ref="templateMasterService"/>
    </bean>

    <bean id="templateMasterWorkflowFormController"
          class="net.grouplink.ehelpdesk.web.controllers.config.TemplateMasterWorkflowFormController">
        <property name="templateMasterService" ref="templateMasterService"/>
        <property name="statusService" ref="statusService"/>
        <property name="workflowStepService" ref="workflowStepService"/>
        <property name="ticketTemplateService" ref="ticketTemplateService"/>
        <property name="workflowStepStatusService" ref="workflowStepStatusService"/>
    </bean>

    <bean id="searchConfigController" class="org.springframework.web.servlet.mvc.ParameterizableViewController">
        <property name="viewName" value="searchConfig"/>
    </bean>

</beans>
