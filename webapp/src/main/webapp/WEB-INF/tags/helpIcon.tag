<%@ tag language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="id" required="true" %>
<%@ attribute name="messageCode" required="true" %>

<span id="${id}"><img src="<c:url value="/images/theme/icons/fugue/information-ocre.png"/>"></span>
<span dojoType="dijit.Tooltip" connectId="${id}">
    <span style="display: block; max-width: 375px; font-size: 10px; line-height: 1.3">
        <spring:message code="${messageCode}"/>
    </span>
</span>