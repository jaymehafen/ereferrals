<%@ tag language="java" %>
<%@ attribute name="messageCode" required="true" %>
<%@ attribute name="connectId" required="true" %>
<%@ attribute name="dojoType" required="false" %>
<%@ attribute name="required" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ehd" uri="/WEB-INF/tlds/ehelpdesk.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--<%@ taglib prefix="authz" uri="http://acegisecurity.org/authz" %>--%>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<c:choose>
    <c:when test="${not empty status.errorMessage}">
        <span style="color: red">
            * <spring:message code="${messageCode}"/> :
        </span>

        <span
                <c:choose>
                    <c:when test="${dojoType eq ''}">
                        dojoType="Tooltip"
                    </c:when>
                    <c:otherwise>
                        dojoType="${dojoType}"
                    </c:otherwise>
                </c:choose>
                connectId="${connectId}" toggle="fade" toggleDuration="250" style="color:red">
                ${status.errorMessage}
        </span>
    </c:when>
    <c:otherwise>
        <c:if test="${required eq 'true'}">
            *
        </c:if>
        <spring:message code="${messageCode}"/>
        :
    </c:otherwise>
</c:choose>
