<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><decorator:title/></title>

    <%@ include file="/WEB-INF/jsp/includes/theme_styles_js.jsp"%>
    <link rel="stylesheet" href="<c:url value="/css/theme/special-pages.css"/>" type="text/css">

    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dijit/themes/tundra/tundra.css"/>" type="text/css">
    <link rel="shortcut icon" href="<c:url value="/images/favicon.ico"/>" type="image/x-icon"/>

    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/dojo/dojo.js"/>"
        djConfig="locale: '<tags:dojoLocale locale="${rc.locale}" />' ">
    </script>
    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/grouplink/includes.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>
    <%@ include file="/WEB-INF/jsp/main.js.jsp" %>

    <decorator:head/>
</head>
<body class="tundra login-bg dark">
<decorator:body/>
<footer>

</footer>

</body>
</html>
