<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="global.systemName"/> <decorator:title/></title>

    <%@ include file="/WEB-INF/jsp/includes/theme_styles_js.jsp"%>
    <script type="text/javascript" src="<c:url value="/js/underscore-min.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/js/dojo-1.6.2/dijit/themes/tundra/tundra.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="/css/full_screen.css"/>" type="text/css">

    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/dojo/dojo.js"/>"
        djConfig="parseOnLoad: false,
        locale: '<tags:dojoLocale locale="${rc.locale}" />' ">
    </script>
    <script type="text/javascript" src="<c:url value="/js/dojo-1.6.2/grouplink/includes.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/js/scripts.js"/>"></script>
    <%@ include file="/WEB-INF/jsp/main.js.jsp" %>

    <script type="text/javascript">
        dojo.addOnLoad(function() {
            _.templateSettings = {
                interpolate: /\{\{(.+?)\}\}/g
            };
        });
    </script>

    <decorator:head/>
</head>
<body class="tundra">
<decorator:body/>
</body>
</html>