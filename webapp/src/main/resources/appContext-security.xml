<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN//EN" "http://www.springframework.org/dtd/spring-beans.dtd">

<beans>

    <bean id="securityPropertyConfigurer" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="location" value="classpath:conf/security.properties"/>
        <property name="placeholderPrefix" value="$security{"/>
        <property name="properties">
            <props>
                <prop key="maximumSessions">1</prop>
            </props>
        </property>
    </bean>

    <bean id="filterChainProxy" class="org.springframework.security.util.FilterChainProxy">
        <property name="filterInvocationDefinitionSource">
            <value>
                CONVERT_URL_TO_LOWERCASE_BEFORE_COMPARISON
                PATTERN_TYPE_APACHE_ANT
                /**=httpSessionContextIntegrationFilter,logoutFilter,authenticationProcessingFilter,securityContextHolderAwareRequestFilter,rememberMeProcessingFilter,anonymousProcessingFilter,exceptionTranslationFilter,filterInvocationInterceptor,concurrentSessionFilter
            </value>
        </property>
    </bean>
    
    <bean id="httpSessionContextIntegrationFilter" class="org.springframework.security.context.HttpSessionContextIntegrationFilter"/>

    <bean id="logoutFilter" class="org.springframework.security.ui.logout.LogoutFilter">
        <constructor-arg value="/index.jsp"/> <!-- URL redirected to after logout -->
        <constructor-arg>
            <list>
                <ref bean="rememberMeServices"/>
                <bean class="net.grouplink.ehelpdesk.web.filters.CollaborationLogoutHandler">
                	<property name="collaborationService" ref="collaborationService"/>
                </bean>
                <bean class="org.springframework.security.ui.logout.SecurityContextLogoutHandler"/>
            </list>
        </constructor-arg>
        <property name="filterProcessesUrl" value="/log.out"/>
    </bean> 

    <bean id="authenticationProcessingFilter" class="net.grouplink.ehelpdesk.web.filters.GLAuthenticationProcessingFilter">
        <property name="userService" ref="userService"/>
        <property name="ldapService" ref="ldapService"/>
        <property name="userRoleService" ref="userRoleService"/>
        <property name="userRoleGroupService" ref="userRoleGroupService"/>
        <property name="authenticationManager" ref="authenticationManager"/>
        <property name="authenticationFailureUrl" value="/login.glml?login_error=1"/>
        <property name="defaultTargetUrl" value="/"/>
        <property name="filterProcessesUrl" value="/j_acegi_security_check"/>
        <property name="rememberMeServices" ref="rememberMeServices"/>
        <property name="licenseService" ref="licenseService"/>
        <property name="propertiesService" ref="propertiesService"/>
        <property name="groupService" ref="groupService"/>
    </bean>

    <bean id="securityContextHolderAwareRequestFilter" class="org.springframework.security.wrapper.SecurityContextHolderAwareRequestFilter"/>

    <bean id="rememberMeProcessingFilter" class="org.springframework.security.ui.rememberme.RememberMeProcessingFilter">
        <property name="authenticationManager" ref="authenticationManager"/>
        <property name="rememberMeServices" ref="rememberMeServices"/>
    </bean>

    <bean id="anonymousProcessingFilter" class="org.springframework.security.providers.anonymous.AnonymousProcessingFilter">
        <property name="key" value="changeThis"/>
        <property name="userAttribute" value="anonymousUser,ROLE_ANONYMOUS"/>
    </bean>

    <bean id="exceptionTranslationFilter" class="org.springframework.security.ui.ExceptionTranslationFilter">
        <property name="authenticationEntryPoint">
            <bean class="org.springframework.security.ui.webapp.AuthenticationProcessingFilterEntryPoint">
                <property name="loginFormUrl" value="/login.glml"/>
                <property name="forceHttps" value="false"/>
            </bean>
        </property>
        <property name="accessDeniedHandler">
            <bean class="org.springframework.security.ui.AccessDeniedHandlerImpl">
                <property name="errorPage" value="/accessDenied.jsp"/>
            </bean>
        </property>
    </bean>

     <bean id="accessDecisionManager" class="org.springframework.security.vote.AffirmativeBased">
        <property name="allowIfAllAbstainDecisions" value="false"/>
        <property name="decisionVoters">
            <list>
                <bean class="org.springframework.security.vote.RoleVoter"/>
                <bean class="org.springframework.security.vote.AuthenticatedVoter"/>
            </list>
        </property>
    </bean>   

    <bean id="filterInvocationInterceptor" class="org.springframework.security.intercept.web.FilterSecurityInterceptor">
        <property name="authenticationManager" ref="authenticationManager"/>
        <property name="accessDecisionManager" ref="accessDecisionManager"/>
        <property name="objectDefinitionSource">
            <value>
                CONVERT_URL_TO_LOWERCASE_BEFORE_COMPARISON
                PATTERN_TYPE_APACHE_ANT
                /login.glml=IS_AUTHENTICATED_ANONYMOUSLY
                /logout.glml=IS_AUTHENTICATED_ANONYMOUSLY
                /*.aglml=IS_AUTHENTICATED_ANONYMOUSLY
                /images/**=IS_AUTHENTICATED_ANONYMOUSLY
                /kb/**=IS_AUTHENTICATED_ANONYMOUSLY
                <!--/services/**=IS_AUTHENTICATED_ANONYMOUSLY-->
                /ticket/attachment/nonprivatehandledownload.glml=IS_AUTHENTICATED_ANONYMOUSLY
                /home.glml=ROLE_MANAGER,ROLE_TECHNICIAN,ROLE_USER
                /tickets/ticketsearch.glml=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /ticket/**=ROLE_ADMINISTRATOR,ROLE_MANAGER,ROLE_TECHNICIAN,ROLE_USER
                /tickets/**=ROLE_ADMINISTRATOR,ROLE_MANAGER,ROLE_TECHNICIAN,ROLE_USER
                /userselectlist.glml=ROLE_ADMINISTRATOR,ROLE_MANAGER,ROLE_TECHNICIAN,ROLE_USER
                /userselectuser.glml=ROLE_ADMINISTRATOR,ROLE_MANAGER,ROLE_TECHNICIAN,ROLE_USER
                /kbmanagement/**=ROLE_ADMINISTRATOR,ROLE_MANAGER,ROLE_TECHNICIAN
                /managementconsole/**=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /config/customfield/addcustomfield.glml=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /config/notification/**=ROLE_ADMINISTRATOR,ROLE_MANAGER,ROLE_TECHNICIAN
                /config/ticket/addtechtogroup.glml=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /config/ticket/**=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /config/scheduler/**=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /config/surveys/form/**=IS_AUTHENTICATED_ANONYMOUSLY
                /config/ldap/**=ROLE_ADMINISTRATOR
                /config/collaboration/**=ROLE_ADMINISTRATOR
                /config/usermanagement/usermanagement.glml=ROLE_ADMINISTRATOR
                /config/mail/**=ROLE_ADMINISTRATOR
                /config/unit/**=ROLE_ADMINISTRATOR
                /config/dashboardedit.glml=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /dashboard/**=ROLE_ADMINISTRATOR,ROLE_MANAGER
                /user/userdelete.glml=ROLE_ADMINISTRATOR
                /user/**=ROLE_ADMINISTRATOR,ROLE_MANAGER,ROLE_TECHNICIAN,ROLE_USER
                /**/*.glml=IS_AUTHENTICATED_REMEMBERED
                /help/**=IS_AUTHENTICATED_ANONYMOUSLY
            </value>
        </property>
    </bean>

    <bean id="rememberMeServices" class="org.springframework.security.ui.rememberme.TokenBasedRememberMeServices">
        <property name="userDetailsService" ref="userDetailsService"/>
        <property name="key" value="changeThis"/>
    </bean>

    <bean id="sessionRegistry" class="org.springframework.security.concurrent.SessionRegistryImpl"/>

    <bean id="concurrentSessionFilter" class="org.springframework.security.concurrent.ConcurrentSessionFilter" >
        <property name="sessionRegistry" ref="sessionRegistry" />
        <property name="expiredUrl" value="/login.glml?login_error=2" />
    </bean>

    <bean id="concurrentSessionController" class="org.springframework.security.concurrent.ConcurrentSessionControllerImpl" >
        <property name="maximumSessions" value="$security{maximumSessions}" />
        <property name="sessionRegistry" ref="sessionRegistry" />
    </bean>

    <bean id="authenticationManager" class="org.springframework.security.providers.ProviderManager">
        <property name="providers">
            <list>
                <ref local="daoAuthenticationProvider"/>
                <bean class="org.springframework.security.providers.anonymous.AnonymousAuthenticationProvider">
                    <property name="key" value="changeThis"/>
                </bean>
                <bean class="org.springframework.security.providers.rememberme.RememberMeAuthenticationProvider">
                    <property name="key" value="changeThis"/>
                </bean>
            </list>
        </property>
        <property name="sessionController" ref="concurrentSessionController" />
    </bean>

    <bean id="wsAuthenticationManager" class="org.springframework.security.providers.ProviderManager">
        <property name="providers">
            <list>
                <ref local="daoAuthenticationProvider"/>
                <bean class="org.springframework.security.providers.anonymous.AnonymousAuthenticationProvider">
                    <property name="key" value="changeThis"/>
                </bean>
                <bean class="org.springframework.security.providers.rememberme.RememberMeAuthenticationProvider">
                    <property name="key" value="changeThis"/>
                </bean>
            </list>
        </property>
    </bean>

    <bean id="daoAuthenticationProvider" class="org.springframework.security.providers.dao.DaoAuthenticationProvider">
        <property name="userDetailsService" ref="userDetailsService"/>
        <property name="passwordEncoder" >
            <bean id="passwordEncoder" class="org.springframework.security.providers.encoding.Md5PasswordEncoder" />
        </property>
        <property name="userCache">
            <bean class="org.springframework.security.providers.dao.cache.EhCacheBasedUserCache" singleton="true">
                <property name="cache">
                    <bean class="org.springframework.cache.ehcache.EhCacheFactoryBean">
                        <property name="cacheManager">
                            <bean class="org.springframework.cache.ehcache.EhCacheManagerFactoryBean"/>
                        </property>
                        <property name="cacheName" value="userCache"/>
                    </bean>
                </property>
            </bean>
        </property>
    </bean>

    <bean id="userDetailsService" class="org.springframework.security.userdetails.jdbc.JdbcDaoImpl">
        <property name="dataSource" ref="dataSource"/>
        <property name="usersByUsernameQuery">
            <value>
                SELECT loginid, password, active FROM USERS WHERE lower(loginid) = ?
            </value>
        </property>
        <property name="authoritiesByUsernameQuery">
            <value>
                SELECT LOGINID, ROLL.NAME FROM USERS
                INNER JOIN USERROLE ON USERS.ID = USERROLE.USERID
                INNER JOIN ROLL ON USERROLE.ROLEID = ROLL.ID
                WHERE lower(LOGINID) = ?
            </value>
        </property>
    </bean>

    <!-- This bean is optional; it isn't used by any other bean as it only listens and logs -->
    <bean id="loggerListener" class="org.springframework.security.event.authentication.LoggerListener"/>

</beans>