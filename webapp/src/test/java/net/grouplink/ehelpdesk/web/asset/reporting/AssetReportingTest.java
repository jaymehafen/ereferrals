package net.grouplink.ehelpdesk.web.asset.reporting;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ChartBuilderException;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.ColumnBuilderException;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.builders.StyleBuilder;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.entities.DJGroup;
import ar.com.fdvs.dj.domain.entities.DJGroupVariable;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import ar.com.fdvs.dj.util.PropertiesMap;
import net.grouplink.ehelpdesk.domain.Group;
import net.grouplink.ehelpdesk.domain.User;
import net.grouplink.ehelpdesk.domain.asset.Asset;
import net.grouplink.ehelpdesk.domain.asset.AssetType;
import net.grouplink.ehelpdesk.domain.asset.ImprovedAccountingInfo;
import net.grouplink.ehelpdesk.web.AbstractWebIntegrationTest;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * User: jaymehafen
 * Date: Sep 10, 2008
 * Time: 11:12:45 AM
 */
public class AssetReportingTest extends AbstractWebIntegrationTest {

    public void testAssetsByOwnerReport() throws JRException {
        List assets = populateAssets();
        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(assets);
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetsByOwner.jrxml");
        Map params = new HashMap();
//        params.put("net.sf.jasperreports.awt.ignore.missing.font", true);
        String destFile = JasperRunManager.runReportToPdfFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetsByOwner.jasper", params, jrDataSource);
        System.out.println("destFile = " + destFile);
    }

    public void testAssetsByGroupReport() throws JRException {
        List assets = populateAssets();
        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(assets);
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetsByGroup.jrxml");
        Map params = new HashMap();
        String destFile = JasperRunManager.runReportToPdfFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetsByGroup.jasper", params, jrDataSource);
        System.out.println("destFile = " + destFile);
    }

    public void testAssetsByTypeReport() throws JRException {
        List assets = populateAssets();
        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(assets);
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetsByType.jrxml");
        Map params = new HashMap();
        String destFile = JasperRunManager.runReportToPdfFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetsByType.jasper", params, jrDataSource);
        System.out.println("destFile = " + destFile);
    }

    public void testAssetLeaseExpirationReport() throws JRException {
        List assets = populateAssets();
        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(assets);
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetLeaseExpiration.jrxml");
        Map params = new HashMap();
        String destFile = JasperRunManager.runReportToPdfFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetLeaseExpiration.jasper", params, jrDataSource);
        System.out.println("destFile = " + destFile);
    }

    public void testAssetWarrantyReport() throws JRException {
        List assets = populateAssets();
        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(assets);
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetWarrantyExpiration.jrxml");
        Map params = new HashMap();
        String destFile = JasperRunManager.runReportToPdfFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetWarrantyExpiration.jasper", params, jrDataSource);
        System.out.println("destFile = " + destFile);
    }

    public void testAssetPurchaseReport() throws JRException {
        List assets = populateAssets();
        JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(assets);
        JasperCompileManager.compileReportToFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetPurchase.jrxml");
        Map params = new HashMap();
        String destFile = JasperRunManager.runReportToPdfFile(getBaseDir() + "src/main/webapp/WEB-INF/reports/AssetPurchase.jasper", params, jrDataSource);
        System.out.println("destFile = " + destFile);
    }

    @SuppressWarnings("unchecked")
    public void testDynamicReport() throws ColumnBuilderException, ClassNotFoundException, JRException, ChartBuilderException {
        FastReportBuilder frb = new FastReportBuilder();

        Style serifStyle = new StyleBuilder(false).setFont(new Font(10, "DejaVu Serif", false)).build();


        frb.addField("owner", User.class.getName());
        AbstractColumn columnOwner = ColumnBuilder.getInstance()
                .setTitle("Owner")
                .setCustomExpression(new CustomExpression() {
                    public Object evaluate(Map fields, Map variables, Map parameters) {
                        User u = (User) fields.get("owner");
                        if (u == null) {
                            return "None";
                        } else {
                            return u.getLastName() + ", " + u.getFirstName();
                        }
                    }

                    public String getClassName() {
                        return String.class.getName();
                    }

                    public Object evaluate(Object object) {
                        PropertiesMap pm = (PropertiesMap) object;
                        User u = (User) pm.get("owner");
                        if (u == null) {
                            return "None";
                        } else {
                            return u.getLastName() + ", " + u.getFirstName();
                        }
                    }
                })
                .setWidth(30)
                .build();

        columnOwner.setStyle(serifStyle);
//        AbstractColumn columnOwner = ColumnBuilder.getInstance()
//                .setTitle("Owner")
//                .setColumnProperty("owner.loginId", String.class.getName())
//                .setWidth(30)
//                .build();

        frb.addColumn(columnOwner);

//        frb.addColumn("Owner", "owner.loginId", String.class.getName(), 30);
        AbstractColumn columnType = ColumnBuilder.getInstance()
                .setTitle("Type")
                .setColumnProperty("assetType.name", String.class.getName())
                .setWidth(30)
                .build();
        columnType.setStyle(serifStyle);
        frb.addColumn(columnType);

//        frb.addColumn("Type", "assetType.name", String.class.getName(), 30);
        frb.addColumn("Asset #", "assetNumber", String.class.getName(), 30, serifStyle);
        frb.addColumn("Name", "name", String.class.getName(), 30, serifStyle);
        frb.addColumn("Status", "status.name", String.class.getName(), 30, serifStyle);

        frb.setTitle("Test");
        frb.setTitleStyle(serifStyle);

        frb.setUseFullPageWidth(true);
        frb.setSubtitle("Report generated at " + new Date());
//        frb.addGroups(2);

        GroupBuilder gb1 = new GroupBuilder();
        DJGroupVariable cgv = new DJGroupVariable(columnOwner, DJCalculation.COUNT);
        DJGroup g1 = gb1.setCriteriaColumn((PropertyColumn) columnOwner)
                .addFooterVariable(cgv)
                .build();

        frb.addGroup(g1);

//        DJChartBuilder cb = new DJChartBuilder();
//        DJChart chart = cb.addType(DJChart.BAR_CHART)
//                .addColumnsGroup(g1)
//                .addOperation(DJChart.CALCULATION_COUNT)
//                .addColumn(columnType)
//                .build();
//        frb.addChart(chart);

        DynamicReport dr = frb.build();
        List<Asset> assets = populateAssets();
        Collections.sort(assets, new Comparator() {
            public int compare(Object o1, Object o2) {
                Asset a1 = (Asset) o1;
                Asset a2 = (Asset) o2;
                // sort first by owner, then by type
                if (a1.getOwner() == null && a2.getOwner() == null) {
                    return 0;
                } else if (a1.getOwner() == null && a2.getOwner() != null) {
                    return 1;
                } else if (a1.getOwner() != null && a2.getOwner() == null) {
                    return -1;
                } else {
                    String u1Name = a1.getOwner().getLastName() + ", " + a1.getOwner().getFirstName();
                    String u2Name = a2.getOwner().getLastName() + ", " + a2.getOwner().getFirstName();
                    return u1Name.compareToIgnoreCase(u2Name);
                }
            }
        });


        JRDataSource ds = new JRBeanCollectionDataSource(assets);
        JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);
        JasperExportManager.exportReportToPdfFile(jp, getBaseDir() + "src/main/webapp/WEB-INF/reports/dynamic_assets.pdf");
    }

    private String  getBaseDir() {
        Properties p = new Properties();
        InputStream is = null;
        try {
            is = getClass().getClassLoader().getResourceAsStream("jaspertest.properties");
            if (is != null)
                p.load(is);

            return (StringUtils.isNotBlank(p.getProperty("basedir")) ? p.getProperty("basedir") + File.separator : "");
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<Asset> populateAssets() {
        List<Asset> assets = new ArrayList<Asset>();
        AssetType at1 = new AssetType();
        at1.setName("Asset Type 1");
        AssetType at2 = new AssetType();
        at2.setName("Asset Type 2");

        User u1 = new User();
        u1.setLastName("Smith");
        u1.setFirstName("John");
        u1.setLoginId("jsmith");

        User u2 = new User();
        u2.setLastName("Rollins");
        u2.setFirstName("Mark");
        u2.setLoginId("mrollins");

        Group g1 = new Group();
        g1.setName("Group 1");

        Date date1 = new Date();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date date2 = cal.getTime();

        for (int i = 0; i < 100; i++) {
            Asset a = new Asset();
            a.setAssetNumber("ASTNBR-" + i);
            a.setName("Asset Name " + i);

            a.setAssetType((i % 2 == 0) ? at2 : at1);
            a.setOwner((i % 2 == 0) ? u2 : null);
            a.setGroup((i % 2 == 0) ? g1 : null);
            a.setAccountingInfo(new ImprovedAccountingInfo());
            a.getAccountingInfo().setLeaseExpirationDate((i % 2 == 0) ? date1: date2);
            a.getAccountingInfo().setWarrantyDate((i % 2 == 0) ? date1: date2);
            a.setAcquisitionDate((i % 2 == 0) ? date1: date2);

            assets.add(a);
        }

        return assets;
    }
}
