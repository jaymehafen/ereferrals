package net.grouplink.ehelpdesk.web;

import org.springframework.test.AbstractTransactionalSpringContextTests;

/**
 * User: jaymehafen
 * Date: Jan 26, 2008
 * Time: 10:51:50 PM
 */
public abstract class AbstractWebIntegrationTest extends AbstractTransactionalSpringContextTests {

    protected String[] getConfigLocations() {
        return new String[]{
                "classpath:net/grouplink/ehelpdesk/common/appContext.xml",
                "classpath:net/grouplink/ehelpdesk/common/appContext-db.xml",
                "classpath:net/grouplink/ehelpdesk/service/appContext-service.xml",
                "classpath:net/grouplink/ehelpdesk/dao/hibernate/appContext-dao.xml"
        };
    }

}